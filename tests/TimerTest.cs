﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace uMod.Tests
{
    [TestClass]
    public class TimerTest : BaseTest
    {
        protected Libraries.Timer TimerLibrary => Interface.uMod.Libraries.Get<Libraries.Timer>();

        [TestMethod]
        public void TimerLibraryInitialized()
        {
            Assert.IsTrue(TimerLibrary != null);
        }

        [TestMethod]
        public void TimerOnce()
        {
            bool result = false;
            Libraries.Timer.TimerInstance timer = TimerLibrary.Once(0, delegate ()
            {
                result = true;
            });

            Assert.Timer.IsCompleted(timer);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TimerRepeat()
        {
            List<bool> result = new List<bool>();
            Libraries.Timer.TimerInstance timer = TimerLibrary.Repeat(0, 2, delegate ()
            {
                result.Add(true);
            });

            Assert.Timer.IsCompleted(timer);
            Assert.AreEqual(result.Count, 2);
        }

        [TestMethod]
        public void TimerDelay()
        {
            bool result = false;
            Libraries.Timer.TimerInstance timer = TimerLibrary.Once(2, delegate ()
            {
                result = true;
            });

            float duration = Assert.Await.Timer(timer);
            Assert.IsTrue(duration >= 2);
            Assert.IsTrue(result);
        }
    }
}
