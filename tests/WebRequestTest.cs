﻿extern alias References;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using References::Newtonsoft.Json.Linq;
using uMod.Common.Web;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    [Application("WebClient")]
    public class WebRequestTest : BaseTest
    {
        private WebRequests WebRequestLibrary => Interface.uMod.Libraries.Get<WebRequests>();

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void GetRequestLegacyCallbackV2()
        {
            Plugin mockPlugin = GetPlugin<MockPlugin>();

            bool responseTriggered = false;
            WebRequestLibrary.Enqueue("http://httpbin.org/get?test=hello", string.Empty, delegate (WebResponse response)
            {
                responseTriggered = true;
                string responseString = response.ReadAsString();
                JObject responseData = JObject.Parse(responseString);
                Assert.AreEqual(responseData["args"]["test"], "hello");
            }, mockPlugin);

            Assert.Await.IsTrue(() => responseTriggered, 100, 200);
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void GetRequestLegacyCallbackV1()
        {
            Plugin mockPlugin = GetPlugin<MockPlugin>();

            bool responseTriggered = false;
            WebRequestLibrary.Enqueue("http://httpbin.org/get?test=hello", string.Empty, delegate (int code, string response)
            {
                responseTriggered = true;
                JObject responseData = JObject.Parse(response);
                Assert.AreEqual(responseData["args"]["test"], "hello");
            }, mockPlugin);

            Assert.Await.IsTrue(() => responseTriggered, 100, 200);
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void GetRequestLegacyCallbackV1Failed()
        {
            Plugin mockPlugin = GetPlugin<MockPlugin>();

            bool responseTriggered = false;
            bool failTriggered = false;
            WebRequestLibrary.Enqueue("not_a_url", string.Empty, delegate (int code, string response)
            {
                responseTriggered = true;
            }, mockPlugin, WebRequestMethod.GET, null, 30f, delegate (string error)
            {
                failTriggered = true;
            });

            Assert.Await.IsTrue(() => failTriggered, 100, 200);
            Assert.IsFalse(responseTriggered);
        }
    }
}
