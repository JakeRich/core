using System;
using System.IO;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    [Application("Compiler")]
    public class CompilerTest : BaseTest
    {
        #region Helper Functions

        private void IsUnloaded(params string[] pluginNames)
        {
            CurrentScope.Module.LogDebug($"Checking if unloaded: {string.Join(", ", pluginNames)}");
            foreach (string pluginName in pluginNames)
            {
                Assert.Await.IsFalse(() => Utility.Plugins.Exists(pluginName), 25, 75);
            }
        }

        private void IsLoaded(params string[] pluginNames)
        {
            CurrentScope.Module.LogDebug($"Checking if loaded: {string.Join(", ", pluginNames)}");
            foreach (string pluginName in pluginNames)
            {
                Assert.Await.IsTrue(() => Utility.Plugins.Exists(pluginName), 40, 75);
            }
        }

        private void HotLoad(params string[] pluginNames)
        {
            CurrentScope.Module.LogDebug($"Hot loading: {string.Join(", ", pluginNames)}");
            foreach (string pluginName in pluginNames)
            {
                try
                {
                    Compile($"tests/umod/plugins/{pluginName}.cs");
                }
                catch (Exception exception)
                {
                    File.Delete($"tests/umod/plugins/{pluginName}.cs");
                    HotLoad(pluginName);
                }
            }
            foreach (string pluginName in pluginNames)
            {
                Assert.Await.IsTrue(() => Utility.Plugins.Exists(pluginName), 60, 85);
            }
        }

        private void HotUnload(params string[] pluginNames)
        {
            CurrentScope.Module.LogDebug($"Hot unloading: {string.Join(", ", pluginNames)}");
            foreach (string pluginName in pluginNames)
            {
                File.Delete(Path.Combine(Environment.CurrentDirectory, $"umod/plugins/{pluginName}.cs"));
            }
            foreach (string pluginName in pluginNames)
            {
                Assert.Await.IsFalse(() => Utility.Plugins.Exists(pluginName), 25, 75);
            }
        }

        #endregion Helper Functions

        /*
        [TestMethod]
        public void DatabasePlugin()
        {
            Await(() => Utility.Plugins.Exists("DatabasePlugin"), 50, 100);

            Interface.uMod.CallHook("Connect");

            Assert.Await.IsLogged("Closing data connection"));
            Assert.Await.IsLogged("Result test success"));
        }
        */

        /// <summary>
        /// Check if required depenency gets loaded automatically
        /// </summary>
        [TestMethod]
        [Compile("tests/umod/plugins/MockDynamicDependentPlugin.cs")]
        [Compile("tests/umod/plugins/MockDynamicDependencyPlugin.cs")]
        public void DependencyWhenOneSideLoadedShouldLoadDependent()
        {
            IsLoaded("MockDynamicDependentPlugin");
            IsLoaded("MockDynamicDependencyPlugin");

            Interface.uMod.Plugins.UnloadAll();

            IsUnloaded("MockDynamicDependentPlugin");
            IsUnloaded("MockDynamicDependencyPlugin");

            Interface.uMod.Plugins.Load("MockDynamicDependencyPlugin");

            IsLoaded("MockDynamicDependentPlugin");
            IsLoaded("MockDynamicDependencyPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MockDynamicDependentPlugin.cs")]
        [Compile("tests/umod/plugins/MockDynamicDependencyPlugin.cs")]
        public void DependencyWhenOtherSideLoadedShouldLoadDependent()
        {
            IsLoaded("MockDynamicDependentPlugin");
            IsLoaded("MockDynamicDependencyPlugin");

            Interface.uMod.Plugins.Unload("MockDynamicDependencyPlugin");

            IsUnloaded("MockDynamicDependencyPlugin");

            Interface.uMod.Plugins.Load("MockDynamicDependencyPlugin");

            IsLoaded("MockDynamicDependentPlugin");
            IsLoaded("MockDynamicDependencyPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/BlankPlugin.cs")]
        public void BasicCompiler()
        {
            // Will load automatically if...
            //  1. uMod.Compiler.dll is available in umod/apps
            //  2. "dotnet" is available from the PATH

            IsLoaded("BlankPlugin");

            IPlugin plugin = Utility.Plugins.Get("BlankPlugin");

            Assert.IsTrue(plugin != null);
        }

        [TestMethod]
        public void RecursiveDependencyWhenOneSideOptionalShouldSucceed()
        {
            HotLoad("BaseArenaPlugin", "ArenaEventPlugin");
            Assert.Await.IsLogged("BaseArenaPlugin.TestClass.Message");
            HotUnload("BaseArenaPlugin", "ArenaEventPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MockReferencedPlugin2.cs", "tests/umod/plugins/MockReferencedPlugin.cs", "tests/umod/plugins/MockReferencePlugin.cs")]
        public void PrecompileWhenLoadingModule()
        {
            IsLoaded("MockReferencedPlugin", "MockReferencePlugin");

            IPlugin plugin1 = Utility.Plugins.Get("MockReferencePlugin");
            IPlugin plugin2 = Utility.Plugins.Get("MockReferencedPlugin");

            Assert.IsNotNull(plugin1);
            plugin1.CallHook("TestReferenceHook");

            Assert.Await.IsLogged("ProxiedTestMethodCalled");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MockReferencedPlugin2.cs")]
        [Compile("tests/umod/plugins/MockReferencedPlugin.cs")]
        [Compile("tests/umod/plugins/MockReferencePlugin.cs")]
        public void DependencyMagicReload()
        {
            IsLoaded("MockReferencedPlugin", "MockReferencePlugin");

            IPlugin plugin1 = Utility.Plugins.Get("MockReferencePlugin");
            IPlugin plugin2 = Utility.Plugins.Get("MockReferencedPlugin");

            Assert.IsNotNull(plugin1);
            plugin1.CallHook("TestReferenceHook");

            Assert.Await.IsLogged("ProxiedTestMethodCalled");
            Interface.uMod.Plugins.Unload("MockReferencePlugin");
            IsUnloaded("MockReferencePlugin");
            Assert.Await.IsLogged("Unloaded plugin");
            Interface.uMod.Plugins.Load("MockReferencePlugin");
            IsLoaded("MockReferencePlugin");
            plugin1 = Utility.Plugins.Get("MockReferencePlugin");
            plugin1.CallHook("TestReferenceHook");

            Assert.Await.IsLogged("ProxiedTestMethodCalled");
        }

        [TestMethod]
        public void BatchCompilingTest()
        {
            HotLoad("BatchCompilePlugin1", "BatchCompilePlugin2");
            IsLoaded("BatchCompilePlugin1", "BatchCompilePlugin2");

            IPlugin plugin1 = Utility.Plugins.Get("BatchCompilePlugin1");
            IPlugin plugin2 = Utility.Plugins.Get("BatchCompilePlugin2");

            Assert.IsNotNull(plugin1, "Plugin 1 Compiled");
            Assert.IsNotNull(plugin2, "Plugin 2 Compiled");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/BrokenPlugin.cs")]
        public void BrokenPluginRollback()
        {
            //unbreak compilation
            string brokenPluginPath = Path.Combine(CurrentScope.Module.PluginDirectory, "BrokenPlugin.cs");
            string pluginText = File.ReadAllText(brokenPluginPath);
            File.WriteAllText(brokenPluginPath, pluginText.Replace("BrokenPlugin : Plugin", "BrokenPlugin : Plugin {").Replace("1.0.0", "1.0.1"));

            IsLoaded("BrokenPlugin");

            //break compilation
            File.WriteAllText(brokenPluginPath, pluginText.Replace("BrokenPlugin : Plugin {", "BrokenPlugin : Plugin").Replace("1.0.1", "1.0.2"));
            File.SetLastWriteTime(brokenPluginPath, DateTime.Now);

            // is unloaded
            IsUnloaded("BrokenPlugin");
            // is reloaded (rollback)
            IsLoaded("BrokenPlugin");
            Assert.Await.IsLogged("Rolling back");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/BrokenPlugin.cs", "tests/umod/plugins/BlankPlugin.cs")]
        public void PartialCompilation()
        {
            IsLoaded("BlankPlugin");
            HotUnload("BlankPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MismatchedPluginName.cs")]
        [Compile("tests/umod/plugins/MockReferencedPlugin.cs")]
        public void MismatchedPluginNameTest()
        {
            //We want this plugin to not compile
            IsUnloaded("MismatchedPluginName");
            IsLoaded("MockReferencedPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MagicRequiresPlugin.cs", "tests/umod/plugins/MockReferencedPlugin.cs")]
        public void MagicRequiresPluginTest()
        {
            IsLoaded("MockReferencedPlugin", "MagicRequiresPlugin");

            IPlugin basePlugin = Utility.Plugins.Get("MockReferencedPlugin");
            IPlugin magicPlugin = Utility.Plugins.Get("MagicRequiresPlugin");

            Assert.IsNotNull(basePlugin, "Referenced plugin Compiled");
            Assert.IsNotNull(magicPlugin, "Magic referencing plugin Compiled");

            magicPlugin.CallHook("TestReferenceHook");

            Assert.Await.IsLogged("ProxiedTestMethodCalled");

            HotUnload("MagicRequiresPlugin", "MockReferencedPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MagicRequiresPlugin.cs")]
        public void MagicRequiresPromise()
        {
            HotLoad("MockReferencedPlugin");
            IsLoaded("MagicRequiresPlugin");

            HotUnload("MagicRequiresPlugin", "MockReferencedPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/ExplicitRequiresPlugin.cs")]
        public void DelayedRecompileWhenFirstTypeRequiresNext()
        {
            HotUnload("ExplicitRequiresPlugin");
            Compile("tests/umod/plugins/ExplicitRequiresPlugin.cs");
            IsUnloaded("ExplicitRequiresPlugin");
            HotLoad("MockReferencedPlugin");
            IsLoaded("ExplicitRequiresPlugin");

            IPlugin magicPlugin = Utility.Plugins.Get("ExplicitRequiresPlugin");
            magicPlugin.CallHook("TestReferenceHook");

            Assert.Await.IsLogged("ProxiedTestMethodCalled");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MockReferencedPlugin.cs", "tests/umod/plugins/ExplicitRequiresPlugin.cs")]
        public void ReorderPrecompilation()
        {
            IsLoaded("MockReferencedPlugin", "ExplicitRequiresPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MockReferencedPlugin.cs", "tests/umod/plugins/ExplicitRequiresPlugin.cs")]
        public void ReorderedUnloadWhenDependantRecompiled()
        {
            IsLoaded("MockReferencedPlugin", "ExplicitRequiresPlugin");
            HotUnload("ExplicitRequiresPlugin");
            HotLoad("ExplicitRequiresPlugin");
            IsLoaded("MockReferencedPlugin");
            HotUnload("ExplicitRequiresPlugin", "MockReferencedPlugin");
        }

        private void CleanPlugin(params string[] pluginNames)
        {
            bool success = false;

            foreach (string pluginName in pluginNames)
            {
                int tries = 0;

                string path = Path.Combine(Environment.CurrentDirectory, "umod/plugins", $"{pluginName}.cs");

                if (!File.Exists(path))
                {
                    return;
                }

                while (!success && tries < 100)
                {
                    try
                    {
                        File.Delete(path);
                        return;
                    }
                    catch (IOException)
                    {
                        Thread.Sleep(100);
                        tries++;
                    }
                }

                Assert.Fail($"Unable to delete file {pluginName}.cs");
            }
        }

        [TestMethod]
        public void ReorderedUnloadWhenRemovedAndReplaced()
        {
            HotLoad("MockReferencedPlugin", "ExplicitRequiresPlugin");
            IsLoaded("MockReferencedPlugin", "ExplicitRequiresPlugin");
            HotUnload("MockReferencedPlugin");
            IsUnloaded("ExplicitRequiresPlugin");
            CleanPlugin("ExplicitRequiresPlugin", "MockReferencedPlugin");
            //File.Delete(Path.Combine(Environment.CurrentDirectory, "umod/plugins/ExplicitRequiresPlugin.cs"));
            HotLoad("MockReferencedPlugin");
            HotLoad("ExplicitRequiresPlugin");
            HotUnload("MockReferencedPlugin", "ExplicitRequiresPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/MockReferencedPlugin.cs", "tests/umod/plugins/ExplicitRequiresPlugin.cs")]
        public void ReverseDependencyJoint()
        {
            IsLoaded("MockReferencedPlugin", "ExplicitRequiresPlugin");
            HotUnload("ExplicitRequiresPlugin");
            IsLoaded("MockReferencedPlugin");
            HotLoad("ExplicitRequiresPlugin");
            HotUnload("MockReferencedPlugin", "ExplicitRequiresPlugin");
        }

        [TestMethod]
        public void PluginShouldUnloadWhenFileRemoved()
        {
            HotLoad("ExplicitRequiresPlugin", "MockReferencedPlugin");
            IsLoaded("MockReferencedPlugin", "ExplicitRequiresPlugin");
            HotUnload("MockReferencedPlugin", "ExplicitRequiresPlugin");
        }

        [TestMethod]
        [Compile("tests/umod/plugins/ParentPlugin.cs", "tests/umod/plugins/ParentChildPlugin1.cs", "tests/umod/plugins/ParentChildPlugin2.cs")]
        public void ReloadUnrelatedDependencyWhenLeafReloaded()
        {
            IsLoaded("ParentPlugin", "ParentChildPlugin1", "ParentChildPlugin2");
            Interface.uMod.Plugins.Reload("ParentChildPlugin1");
            IsUnloaded("ParentChildPlugin2");
            IsLoaded("ParentChildPlugin2");
        }
    }
}
