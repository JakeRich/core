﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Tests.Mock;

namespace uMod.Tests
{
    [TestClass]
    public class CollectionTest : BaseTest
    {
        [TestMethod]
        public void PooledCollection()
        {
            ICollection<int> originalList = new List<int>()
            {
                1,
                1,
                2
            };

            MockPooledCollection<int> collection = new MockPooledCollection<int>(ref originalList);

            int c = 0;
            foreach (int entry in collection)
            {
                c++;
            }

            Assert.AreEqual(c, 2);
        }
    }
}
