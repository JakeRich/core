﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    [Plugin(typeof(MockPlugin))]
    [Environment("auth.driver", "protobuf")]
    public class PermissionProtobufTest : PermissionTest
    {
    }

    [TestClass]
    [Application("Database")]
    [Plugin(typeof(MockPlugin))]
    [Environment("auth.driver", "database")]
    public class PermissionDatabaseTest : PermissionTest
    {
    }

    [TestClass]
    [Plugin(typeof(MockPlugin))]
    [Environment("auth.driver", "json")]
    public class PermissionJsonTest : PermissionTest
    {
    }
}
