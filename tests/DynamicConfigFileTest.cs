extern alias References;

using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.Configuration;
using uMod.IO.Schema;
using uMod.Mock;
using uMod.Plugins;
using static uMod.Libraries.Timer;

namespace uMod.Tests
{
    [TestClass]
    public class DynamicConfigFileTest : BaseTest
    {
        protected Libraries.Timer TimerLibrary => Interface.uMod.Libraries.Get<Libraries.Timer>();

        public class MockConfig
        {
            public bool Test = true;
        }

        private string ConfigDirectory => Path.Combine(AppContext.BaseDirectory, Path.Combine("umod" + Path.DirectorySeparatorChar + "config"));

        private string MockPluginPath => Path.Combine(ConfigDirectory, "MockPlugin.json");

        private string MockConfigPluginPath => Path.Combine(ConfigDirectory, "MockConfigPlugin.json");

        private void AwaitAsyncFile(string path)
        {
            int tries = 0;
            while (!File.Exists(path))
            {
                if (tries > 75)
                {
                    break;
                }

                System.Threading.Thread.Sleep(25);
                tries++;
            }
        }

        private void AwaitAsyncConfigFile(DynamicConfigFile configFile)
        {
            int tries = 0;
            while (!configFile.Exists())
            {
                if (tries > 75)
                {
                    break;
                }

                System.Threading.Thread.Sleep(25);
                tries++;
            }
        }

        private void CleanConfigs()
        {
            string[] configs =
            {
                Path.Combine(Interface.uMod.ConfigDirectory, "MockConfigPlugin.json"),
                Path.Combine(Interface.uMod.ConfigDirectory, "MockPlugin.json"),
            };

            foreach (string configPath in configs)
            {
                if (File.Exists(configPath))
                {
                    File.Delete(configPath);
                }
            }
        }

        [TestMethod]
        public void LegacyConfigFileWrite()
        {
            CleanConfigs();
            DynamicConfigFile configFile = new DynamicConfigFile(MockPluginPath);
            configFile.WriteObject(new MockConfig());

            Assert.IsTrue(File.Exists(MockPluginPath));
        }

        [TestMethod]
        public void LegacyConfigFileWriteAsync()
        {
            CleanConfigs();
            DynamicConfigFile configFile = new DynamicConfigFile(MockPluginPath);

            Assert.IsFalse(File.Exists(MockPluginPath));
            configFile.WriteObjectAsync(new MockConfig());
            AwaitAsyncConfigFile(configFile);
            Assert.IsTrue(configFile.Exists());
        }

        [TestMethod]
        public void LegacyConfigFileExists()
        {
            CleanConfigs();
            DynamicConfigFile configFile = new DynamicConfigFile(MockPluginPath);
            configFile.WriteObject(new MockConfig());
            Assert.IsTrue(configFile.Exists());
        }

        [TestMethod]
        public void LegacyConfigFileExistsAsync()
        {
            CleanConfigs();
            DynamicConfigFile configFile = new DynamicConfigFile(MockPluginPath);
            Assert.IsTrue(!configFile.Exists());
            configFile.WriteObjectAsync(new MockConfig());

            AwaitAsyncConfigFile(configFile);
            Assert.IsTrue(configFile.Exists());
        }

        [TestMethod]
        public void LegacyConfigFileRead()
        {
            CleanConfigs();
            DynamicConfigFile configFile = new DynamicConfigFile(MockPluginPath);
            MockConfig mockConfig = configFile.ReadObject<MockConfig>();
            Assert.IsTrue(mockConfig.Test);
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void PluginLegacyConfigSaved()
        {
            Plugin mockPlugin = GetPlugin<MockPlugin>();
            Assert.Await.IsTrue(delegate ()
            {
                CleanConfigs();
                return !File.Exists(MockPluginPath);
            });
            Assert.IsFalse(File.Exists(MockPluginPath));
            mockPlugin.Config.SaveAsync();

            AwaitAsyncFile(MockPluginPath);

            Assert.IsTrue(File.Exists(MockPluginPath));
        }

        [TestMethod]
        [Plugin(typeof(MockConfigPlugin))]
        public void PluginAutoConfigLoaded()
        {
            Plugin mockPlugin = GetPlugin<MockConfigPlugin>();

            AwaitAsyncFile(MockConfigPluginPath);

            Assert.IsTrue(File.Exists(MockConfigPluginPath));
        }

        [TestMethod]
        [Plugin(typeof(MockConfigPlugin))]
        public void PluginAutoConfigUpgrade()
        {
            CleanConfigs();

            MockConfigPlugin mockPlugin = GetPlugin<MockConfigPlugin>();

            Assert.Await.IsTrue(() => mockPlugin?.IsLoaded == true);

            int callbacks = 0;
            TimerInstance timer = TimerLibrary.Once(0, delegate ()
            {
                File.Delete(Path.Combine(Interface.uMod.ConfigDirectory, "MockConfigPlugin.json"));

                MockConfigPlugin.OldConfigStuff oldConfigStuff = new MockConfigPlugin.OldConfigStuff
                {
                    Feature = false
                };

                File.WriteAllText(Path.Combine(Interface.uMod.ConfigDirectory, "MockConfigPlugin.json"), JsonConvert.SerializeObject(oldConfigStuff));
                VersionStore.Instance.Update(Path.Combine("config", "MockConfigPlugin"), new VersionNumber(0, 0, 9));

                mockPlugin.HandleAddedToManager(null);
                callbacks++;
            });

            Assert.Timer.IsCompleted(timer);
            Assert.Await.IsTrue(() => callbacks == 1, 250, 150);
            Assert.Await.IsLogged("OnMockConfigPluginConfigUpgraded1Called");
            Assert.Await.IsLogged("OnMockConfigPluginConfigUpgraded2Called");
        }

        [TestMethod]
        [Plugin(typeof(MockConfigPlugin))]
        public void PluginAutoConfigSave()
        {
            Plugin mockPlugin = GetPlugin<MockConfigPlugin>();

            AwaitAsyncFile(MockConfigPluginPath);

            Assert.IsTrue(File.Exists(MockConfigPluginPath));

            string originalValue = File.ReadAllText(MockConfigPluginPath);

            TimerInstance timer = TimerLibrary.Once(0, delegate ()
            {
                mockPlugin.CallHook("UpdateConfig");

                string newValue = File.ReadAllText(MockConfigPluginPath);

                Assert.AreNotEqual(originalValue, newValue);
            });

            Assert.Timer.IsCompleted(timer);
        }

        /*
        [TestMethod]
        public void PluginAutoConfigError()
        {
            Plugin mockPlugin = GetPlugin(typeof(MockConfigPlugin));

            Assert.Await.IsTrue(() => mockPlugin.IsResolved));

            // Append some random characters to file, making it invalid
            File.WriteAllText(MockConfigPluginPath, "kjdfkgdg");

            mockPlugin.HandleAddedToManager(null);
        }
        */

        [TestMethod]
        [Plugin(typeof(MockLegacyConfigPlugin))]
        public void PluginLegacyConfigTypeLoaded()
        {
            MockLegacyConfigPlugin mockPlugin = GetPlugin<MockLegacyConfigPlugin>();

            Assert.AreEqual("Hello world", mockPlugin.ConfigValue.TestString);
            Assert.AreEqual(null, mockPlugin.ConfigValue.NullableObject);

            Assert.AreEqual(2, mockPlugin.ConfigValue.ListObject.Count);
            Assert.AreEqual("hello", mockPlugin.ConfigValue.ListObject[0]);
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void PluginLegacyConfigGetSet()
        {
            Plugin mockPlugin = GetPlugin<MockPlugin>();
            mockPlugin.Config.SaveAsync();

            AwaitAsyncFile(MockPluginPath);

            mockPlugin.Config["test", "test2"] = "hello";

            Assert.AreSame("hello", mockPlugin.Config["test", "test2"]);
        }
    }
}
