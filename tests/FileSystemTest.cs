﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.IO;

namespace uMod.Tests
{
    [TestClass]
    public class FileSystemTest : BaseTest
    {
        public string MockDataPath => Path.Combine(Interface.uMod.DataDirectory, "MockDataTest.json");
        public string MockDataPath2 => Path.Combine(Interface.uMod.DataDirectory, "MockDataTest2.json");

        public string TestDirectory
        {
            get
            {
                string testDir = Path.Combine(Environment.CurrentDirectory, "test" + Utility.Random.Range(0, 1000));
                if (!Directory.Exists(testDir))
                {
                    Directory.CreateDirectory(testDir);
                }

                return testDir;
            }
        }

        public class MockData
        {
            public bool Test = true;
        }

        private void CleanFiles()
        {
            if (File.Exists(MockDataPath))
            {
                File.Delete(MockDataPath);
            }

            if (File.Exists(MockDataPath2))
            {
                File.Delete(MockDataPath2);
            }
        }

        [TestMethod]
        public void WriteObject()
        {
            CleanFiles();
            int callbacks = 0;
            FileSystem.WriteObject(MockDataPath, new MockData
            {
                Test = false
            }).Done(delegate ()
            {
                callbacks++;
                Assert.IsTrue(File.Exists(MockDataPath));
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void WriteObjects()
        {
            CleanFiles();

            Dictionary<string, MockData> mockData = new Dictionary<string, MockData>
            {
                [MockDataPath] = new MockData
                {
                    Test = false
                },
                [MockDataPath2] = new MockData
                {
                    Test = false
                }
            };

            int callbacks = 0;
            FileSystem.WriteObjects(mockData).Done(delegate ()
            {
                callbacks++;
                Assert.IsTrue(File.Exists(MockDataPath));
                Assert.IsTrue(File.Exists(MockDataPath2));
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void ReadObject()
        {
            WriteObject();

            int callbacks = 0;
            FileSystem.ReadObject<MockData>(MockDataPath).Done(delegate (MockData data)
            {
                callbacks++;
                Assert.IsFalse(data.Test);
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void ReadObjects()
        {
            WriteObjects();

            int callbacks = 0;
            FileSystem.ReadObjects<MockData>(new[] { MockDataPath, MockDataPath2 }).Done(delegate (IEnumerable<MockData> data)
             {
                 callbacks++;
                 MockData[] mockData = data.ToArray();
                 Assert.AreEqual(2, mockData.Length);
                 Assert.IsFalse(mockData[0].Test);
                 Assert.IsFalse(mockData[1].Test);
             });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void GetDataFile()
        {
            IDataFile<object> dataFile = FileSystem.GetDataFile(typeof(IDataFile<object>), MockDataPath);
            Assert.IsInstanceOfType(dataFile, typeof(IDataFile<object>));
        }

        [TestMethod]
        public void GetDataFileGeneric()
        {
            IDataFile<MockData> dataFile = FileSystem.GetDataFile<MockData>(MockDataPath);
            Assert.IsInstanceOfType(dataFile, typeof(IDataFile<MockData>));
        }

        [TestMethod]
        public void GetDataFormat()
        {
            Assert.AreEqual(DataFormat.Json, DataSystem.GetDataFormat("json"));
            Assert.AreEqual(DataFormat.Binary, DataSystem.GetDataFormat("dat"));
            Assert.AreEqual(DataFormat.Toml, DataSystem.GetDataFormat("toml"));
            Assert.AreEqual(DataFormat.Proto, DataSystem.GetDataFormat("data"));
            Assert.AreEqual(DataFormat.Text, DataSystem.GetDataFormat("txt"));
        }

        //[TestMethod]
        public void WatcherCreated()
        {
            string testDirectory = TestDirectory;
            string testPath = Path.Combine(testDirectory, "test.txt");
            if (File.Exists(testPath))
            {
                File.Delete(testPath);
            }
            IO.FileSystemWatcher fileSystemWatcher = new IO.FileSystemWatcher(
                Interface.uMod,
                Interface.uMod.RootLogger,
                testDirectory,
                "*.txt,*.json"
            );

            int callbacks = 0;

            fileSystemWatcher.Notify.Add(delegate (string path, FileChangeType changeType)
            {
                callbacks++;
                Assert.AreEqual(testPath, path);
                Assert.AreEqual(FileChangeType.Created, changeType);
            });

            try
            {
                Interface.uMod.NextTick(() =>
                {
                    File.WriteAllText(testPath, "hello world");
                    File.SetLastWriteTime(testPath, DateTime.Now);
                    callbacks++;
                });
                
                Assert.Await.IsTrue(() => callbacks == 1);
                Assert.Await.IsTrue(() => callbacks > 1);
            }
            finally
            {
                fileSystemWatcher.Close();
                if (File.Exists(testPath))
                {
                    File.Delete(testPath);
                }
                Directory.Delete(testDirectory);
            }
        }

        [TestMethod]
        public void WatcherChanged()
        {
            string testDirectory = TestDirectory;
            string testPath = Path.Combine(testDirectory, "test.txt");
            if (File.Exists(testPath))
            {
                File.Delete(testPath);
            }
            Console.WriteLine("Creating file...");
            File.WriteAllText(testPath, "hello world");

            IO.FileSystemWatcher fileSystemWatcher = new IO.FileSystemWatcher(
                Interface.uMod,
                Interface.uMod.RootLogger,
                testDirectory,
                "*.txt"
            );

            int callbacks = 0;

            fileSystemWatcher.Notify.Add(delegate (string path, FileChangeType changeType)
            {
                Assert.AreEqual(testPath, path);
                Assert.AreEqual(FileChangeType.Changed, changeType);
                callbacks++;
            });

            try
            {
                Thread.Sleep(1500);
                Console.WriteLine("Writing changes...");
                File.WriteAllText(testPath, "hello world 2");
                Assert.Await.IsTrue(() => callbacks == 1, 100, 50);
            }
            finally
            {
                Console.WriteLine(callbacks);
                fileSystemWatcher.Close();
                if (File.Exists(testPath))
                {
                    File.Delete(testPath);
                }
                Directory.Delete(testDirectory);
            }
        }

        [TestMethod]
        public void WatcherDeleted()
        {
            string testDirectory = TestDirectory;
            string testPath = Path.Combine(testDirectory, "test.txt");
            File.WriteAllText(testPath, "hello world");

            IO.FileSystemWatcher fileSystemWatcher = new IO.FileSystemWatcher(
                Interface.uMod,
                Interface.uMod.RootLogger,
                testDirectory,
                "*.txt"
            );

            int callbacks = 0;

            fileSystemWatcher.Notify.Add(delegate (string path, FileChangeType changeType)
            {
                Assert.AreEqual(testPath, path);
                Assert.AreEqual(FileChangeType.Deleted, changeType);
                callbacks++;
            });

            try
            {
                File.Delete(testPath);
                Assert.Await.IsTrue(() => callbacks == 1);
            }
            finally
            {
                fileSystemWatcher.Close();
                if (File.Exists(testPath))
                {
                    File.Delete(testPath);
                }
                Directory.Delete(testDirectory);
            }
        }
    }
}
