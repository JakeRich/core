﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class LoggingTest : BaseTest
    {
        [TestMethod]
        public void LogLevels()
        {
            ILogger logger = Interface.uMod.RootLogger;

            logger.Notice("Notice");
            logger.Warning("Warning");
            logger.Critical("Critical");
            logger.Alert("Alert");
            logger.Emergency("Emergency");
            logger.Report(new Exception("ReportOne"));
            logger.Report("ReportMessage", new Exception("ReportTwo"));

            Assert.Await.IsLogged("Notice");
            Assert.Await.IsLogged("Warning");
            Assert.Await.IsLogged("Critical");
            Assert.Await.IsLogged("Alert");
            Assert.Await.IsLogged("Emergency");
            Assert.Await.IsLogged("ReportOne");
            Assert.Await.IsLogged("ReportMessage");
            Assert.Await.IsLogged("ReportTwo");
        }

        [TestMethod]
        public void ServerLogger()
        {
            Interface.uMod.LogInfo("hello world");

            Assert.Await.IsLogged("hello world");
        }

        [TestMethod]
        [Plugin(typeof(MockLoggingPlugin))]
        public void PluginLogger()
        {
            string path1 = Path.Combine(Interface.uMod.LogDirectory, $"mocklog_{DateTime.Now:yyyy-MM-dd}.log");
            Assert.Await.IsTrue(() => File.Exists(path1));

            string path2 = Path.Combine(Interface.uMod.LogDirectory, "mocklog.log");
            Assert.IsTrue(File.Exists(path2));

            System.Threading.Thread.Sleep(50);
            Interface.uMod.Plugins.Unload(nameof(MockLoggingPlugin));

            string log = File.ReadAllText(path1);
            Assert.IsTrue(log.Contains("MockLoggingPluginLoaded"));

            log = File.ReadAllText(path2);
            Assert.IsTrue(log.Contains("MockLoggingPluginLoaded"));

            File.Delete(path1);
            File.Delete(path2);

            //Assert.Await.IsLogged("MockLoggingPluginLoaded"));
        }
    }
}
