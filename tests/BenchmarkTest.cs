using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
#if !DEBUG
    [TestClass]
#endif
    [Plugin(typeof(MockPlugin))]
    public class BenchmarkTest : BaseTest
    {
        // If 100,000 basic hook calls exceed MAX_MS ms to call, something might need to be optimized
        // Adjusted for slow worker machine
        private const int MAX_PLUGIN_MS = 50;
        private const int MAX_GLOBAL_MS = 100;
        private const int MAX_DI_MS = 200;
        private const int MAX_IN_MS = 35;
        private const int MAX_GA_MS = 300;
        private const int MAX_CMD_MS = 550;

        private void AssertBenchmark(double[] results, int minms)
        {
            double min = results.Min();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Min: {min}ms");
            sb.AppendLine($"Avg: {results.Average()}ms");
            sb.AppendLine($"Max: {results.Max()}ms");
            Console.WriteLine(sb.ToString());
            try
            {
                Assert.IsTrue(min < minms);
            }
            finally
            {
                RestoreLog();
            }
        }

        [TestMethod]
        public void HookPluginBenchmark()
        {
            int timesToBenchmark = 6;
            double[] results = new double[timesToBenchmark];
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            ClearLog();

            for (int benchmarkIndex = 0; benchmarkIndex < timesToBenchmark; benchmarkIndex++)
            {
                int total = 100000;
                mockPlugin.count = 0;
                Stopwatch stopwatch = Stopwatch.StartNew();
                for (int i = 0; i < total; i++)
                {
                    mockPlugin.CallHook("OnBenchmark");
                }
                stopwatch.Stop();
                results[benchmarkIndex] = stopwatch.Elapsed.TotalMilliseconds;

                Assert.AreEqual(total, mockPlugin.count, "Mock Plugin calls ({0}) and total plugin calls ({1}) don't match up", total, mockPlugin.count);
            }

            AssertBenchmark(results, MAX_PLUGIN_MS);
        }

        [TestMethod]
        public void HookGlobalBenchmark()
        {
            int timesToBenchmark = 6;
            double[] results = new double[timesToBenchmark];
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            ClearLog();

            for (int benchmarkIndex = 0; benchmarkIndex < timesToBenchmark; benchmarkIndex++)
            {
                int total = 100000;
                mockPlugin.count = 0;
                Stopwatch stopwatch = Stopwatch.StartNew();
                for (int i = 0; i < total; i++)
                {
                    Interface.uMod.CallHook("OnBenchmark");
                }
                stopwatch.Stop();
                results[benchmarkIndex] = stopwatch.Elapsed.TotalMilliseconds;

                Assert.AreEqual(total, mockPlugin.count, "Mock Plugin calls ({0}) and total plugin calls ({1}) don't match up", total, mockPlugin.count);
            }

            AssertBenchmark(results, MAX_GLOBAL_MS);
        }

        [TestMethod]
        public void HookWrapperBenchmarkInjection()
        {
            int timesToBenchmark = 6;
            double[] results = new double[timesToBenchmark];
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            ClearLog();

            for (int benchmarkIndex = 0; benchmarkIndex < timesToBenchmark; benchmarkIndex++)
            {
                int total = 100000;
                mockPlugin.count = 0;
                Stopwatch stopwatch = Stopwatch.StartNew();
                for (int i = 0; i < total; i++)
                {
                    Interface.uMod.CallHook("OnBenchmarkInjection");
                }
                stopwatch.Stop();
                results[benchmarkIndex] = stopwatch.Elapsed.TotalMilliseconds;

                Assert.AreEqual(total, mockPlugin.count, "Mock Plugin calls ({0}) and total plugin calls ({1}) don't match up", total, mockPlugin.count);
            }

            AssertBenchmark(results, MAX_DI_MS);
        }

        [TestMethod]
        public void HookInternalBenchmark()
        {
            int timesToBenchmark = 6;
            double[] results = new double[timesToBenchmark];
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();

            ClearLog();

            for (int benchmarkIndex = 0; benchmarkIndex < timesToBenchmark; benchmarkIndex++)
            {
                int total = 100000;
                mockPlugin.count = 0;
                Stopwatch stopwatch = Stopwatch.StartNew();
                for (int i = 0; i < total; i++)
                {
                    Interface.uMod.CallHook("IOnInternalBenchmark", true);
                }
                stopwatch.Stop();
                results[benchmarkIndex] = stopwatch.Elapsed.TotalMilliseconds;

                Assert.AreEqual(total, mockPlugin.count, "Mock Plugin calls ({0}) and total plugin calls ({1}) don't match up", total, mockPlugin.count);
            }

            AssertBenchmark(results, MAX_IN_MS);
        }

        [TestMethod]
        public void CommandBenchmark()
        {
            int timesToBenchmark = 6;
            double[] results = new double[timesToBenchmark];
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();
            MockPlayer player = new MockPlayer(new GamePlayerIdentity());
            Universal universal = Interface.uMod.Libraries.Get<Universal>();

            ClearLog();

            for (int benchmarkIndex = 0; benchmarkIndex < timesToBenchmark; benchmarkIndex++)
            {
                int total = 100000;
                mockPlugin.commandCount = 0;
                Stopwatch stopwatch = Stopwatch.StartNew();
                for (int i = 0; i < total; i++)
                {
                    universal.CommandSystem.HandleChatMessage(player, "/benchcommand");
                }
                stopwatch.Stop();
                results[benchmarkIndex] = stopwatch.Elapsed.TotalMilliseconds;

                Assert.AreEqual(total, mockPlugin.commandCount, "Mock Plugin calls ({0}) and total plugin calls ({1}) don't match up", total, mockPlugin.count);
            }

            AssertBenchmark(results, MAX_CMD_MS);
        }

        [TestMethod]
        [Plugin(typeof(MockGatePlugin))]
        public void HookGateAllowsBenchmark()
        {
            int timesToBenchmark = 6;
            double[] results = new double[timesToBenchmark];
            MockGatePlugin mockPlugin = GetPlugin<MockGatePlugin>();

            ClearLog();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
            for (int benchmarkIndex = 0; benchmarkIndex < timesToBenchmark; benchmarkIndex++)
            {
                int total = 100000;
                mockPlugin.allowsCount = 0;
                Stopwatch stopwatch = Stopwatch.StartNew();
                for (int i = 0; i < total; i++)
                {
                    mockPlugin.CallHook("OnFly", player);
                }
                stopwatch.Stop();
                results[benchmarkIndex] = stopwatch.Elapsed.TotalMilliseconds;

                Assert.AreEqual(total, mockPlugin.allowsCount, "Mock Plugin calls ({0}) and total plugin calls ({1}) don't match up", total, mockPlugin.allowsCount);
            }

            AssertBenchmark(results, MAX_GA_MS);
        }
    }
}
