using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Auth;
using uMod.Common;
using uMod.Mock;

namespace uMod.Tests
{
    [TestClass]
    public class PlayerManagerTest : BaseTest
    {
        [TestMethod]
        public void PlayerJoin()
        {
            PlayerManager PlayerManager = new PlayerManager(Interface.uMod.Application, Interface.uMod.RootLogger);
            PlayerManager.PlayerJoin("12345", "john smith");

            IPlayer player = PlayerManager.FindPlayerById("12345");

            Assert.IsInstanceOfType(player, typeof(MockPlayer));
        }

        [TestMethod]
        public void PlayerConnected()
        {
            PlayerManager PlayerManager = new PlayerManager(Interface.uMod.Application, Interface.uMod.RootLogger);
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            PlayerManager.PlayerConnected("12345", gamePlayer);

            IPlayer player = PlayerManager.FindPlayerById("12345");

            Assert.IsInstanceOfType(player, typeof(MockPlayer));
            Assert.IsInstanceOfType(player.Object, typeof(GamePlayer));
        }

        [TestMethod]
        public void PlayerDisconnected()
        {
            PlayerManager PlayerManager = new PlayerManager(Interface.uMod.Application, Interface.uMod.RootLogger);
            PlayerManager.PlayerDisconnected("12345");

            IPlayer player = PlayerManager.FindPlayerById("12345");

            Assert.AreEqual(null, player);
        }

        [TestMethod]
        public void FindPlayerByObj()
        {
            PlayerManager PlayerManager = new PlayerManager(Interface.uMod.Application, Interface.uMod.RootLogger);
            GamePlayer gamePlayer = new GamePlayer(new GamePlayerIdentity());
            PlayerManager.PlayerConnected("12345", gamePlayer);

            IPlayer player = PlayerManager.FindPlayerByObj(gamePlayer);

            Assert.IsInstanceOfType(player, typeof(MockPlayer));
            Assert.AreSame(gamePlayer, player.Object);
        }

        [TestMethod]
        public void FindPlayerByName()
        {
            PlayerManager PlayerManager = new PlayerManager(Interface.uMod.Application, Interface.uMod.RootLogger);
            PlayerManager.PlayerJoin("12345", "john smith");

            IPlayer player = PlayerManager.FindPlayer("john smith");

            Assert.IsInstanceOfType(player, typeof(MockPlayer));
        }

        [TestMethod]
        public void FindPlayers()
        {
            PlayerManager PlayerManager = new PlayerManager(Interface.uMod.Application, Interface.uMod.RootLogger);
            PlayerManager.PlayerJoin("12345", "john smith");

            IEnumerable<IPlayer> players = PlayerManager.FindPlayers("john smith");

            Assert.AreEqual(1, players.Count());
        }

        [DataTestMethod]
        [DataRow(PlayerFilter.Admin)]
        [DataRow(PlayerFilter.Moderator)]
        [DataRow(PlayerFilter.Banned)]
        [DataRow(PlayerFilter.Alive)]
        [DataRow(PlayerFilter.Dead)]
        [DataRow(PlayerFilter.Sleeping)]
        public void FindPlayersFilter(PlayerFilter playerFilter)
        {
            PlayerManager PlayerManager = new PlayerManager(Interface.uMod.Application, Interface.uMod.RootLogger);
            PlayerManager.PlayerJoin("09876", "john smith");
            MockPlayer player1 = PlayerManager.FindPlayer("john smith") as MockPlayer;
            PlayerManager.PlayerJoin("87654", "allen smith");
            MockPlayer player3 = PlayerManager.FindPlayer("allen smith") as MockPlayer;

            switch (playerFilter)
            {
                case PlayerFilter.Admin:
                    player1.IsAdmin = true;
                    player3.IsAdmin = true;
                    break;

                case PlayerFilter.Moderator:
                    player1.IsModerator = true;
                    player3.IsModerator = true;
                    break;

                case PlayerFilter.Banned:
                    player1.IsBanned = true;
                    player3.IsBanned = true;
                    break;

                case PlayerFilter.Alive:
                    player1.IsAlive = true;
                    player3.IsAlive = true;
                    break;

                case PlayerFilter.Dead:
                    player1.IsDead = true;
                    player3.IsDead = true;
                    break;

                case PlayerFilter.Sleeping:
                    player1.IsSleeping = true;
                    player3.IsSleeping = true;
                    break;

                default:
                    Assert.Fail("Untested player filter");
                    break;
            }

            IEnumerable<IPlayer> players = PlayerManager.FindPlayers("smith", playerFilter);

            Assert.AreEqual(2, players.Count());
        }
    }
}
