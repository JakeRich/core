﻿using uMod.Collections;
using uMod.Common;
using uMod.Database;
using uMod.Mock;

namespace uMod.Plugins
{
    [Info("Mock Module Loader Plugin", "uMod", "1.0.0")]
    [Description("Fancy Module Loader Plugin")]
    internal class MockModuleLoaderPlugin : Plugin
    {
        private Hash<int, BaseModule> _modules = new Hash<int, BaseModule>();

        public void AddModule(int index, BaseModule module)
        {
            _modules.Add(index, module);
        }

        [Hook("LoadModule")]
        public void LoadModule(int index)
        {
            _modules[index].SubscribeModule();
            _modules[index].ModuleLoaded();
        }

        public abstract class BaseModule : HookDecorator
        {
            public BaseModule(Plugin plugin) : base(plugin)
            {
            }

            public abstract void ModuleLoaded();

            public void SubscribeModule() => base.Subscribe();

            public void UnsubscribeModule() => base.Unsubscribe();
        }

        [Model]
        public class TestModel
        {
            public int Id { get; set; }

            public static TestModel Resolve(int key)
            {
                return new TestModel { Id = key };
            }

            public static TestModel Resolve(GamePlayer key)
            {
                return new TestModel { Id = 1 };
            }

            public static TestModel Resolve(IPlayer key)
            {
                return new TestModel { Id = 2 };
            }
        }

        [Hook("ModuleParentTest")]
        private void ModuleParentTest(TestModel model)
        {
            Interface.uMod.LogInfo($"MockModuleLoaderPlugin.ModuleParentTest {model.Id}");
        }
    }
}
