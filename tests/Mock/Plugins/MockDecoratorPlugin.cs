﻿using uMod.Tests.Mock;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    [HookDecorator(typeof(MockDecorator))]
    internal class MockDecoratorPlugin : Plugin
    {
    }
}
