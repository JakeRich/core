﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockGatePlugin : Plugin
    {
        [Config]
        public class GateConfig
        {
            public bool JumpAllowed = true;
        }

        private class MyActionGate : Gate
        {
            private GateConfig config;
            private ILogger logger;

            public MyActionGate(MockGatePlugin plugin, GateConfig config, ILogger logger) : base(plugin)
            {
                this.config = config;
                this.logger = logger;
            }

            public bool Jumping(IPlayer player)
            {
                logger.Info("MockGatePlugin.MyActionGate.JumpingCalled");
                return config.JumpAllowed;
            }

            public bool Swimming(IPlayer player)
            {
                logger.Info("MockGatePlugin.MyActionGate.SwimmingCalled");
                return false;
            }

            public bool Falling(IPlayer player)
            {
                return false;
            }

            public bool ShootingAt(IPlayer player, IPlayer target)
            {
                logger.Info("MockGatePlugin.MyActionGate.ShootingAtCalled");
                if (target != null)
                {
                    return true;
                }
                return false;
            }

            public bool Flying(IPlayer player)
            {
                return true;
            }
        }

        [Hook("Loaded")]
        public void Loaded()
        {
            Gate.Register("test");
        }

        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(IPlayer player)
        {
            player.GrantPermission("mockgateplugin.test");
        }

        public int allowsCount = 0;

        [Hook("OnFly")]
        [Allows(typeof(MyActionGate), "flying")]
        private void OnFly(IPlayer player, string message)
        {
            allowsCount++;
        }

        [Hook("OnChat")]
        private void OnChat(IPlayer player, string message)
        {
            if (Gate.Allows("test", player))
            {
                Logger.Info("MockGatePlugin.OnChat.Allowed");
            }
            else
            {
                Logger.Info("MockGatePlugin.OnChat.Denied");
            }
        }

        [Hook("OnShoot")]
        private void OnShoot(IPlayer player, IPlayer target, MyActionGate actionGate)
        {
            if (actionGate.Allows("shootingAt", player, target))
            {
                Logger.Info("MockGatePlugin.OnShoot.Allowed");
            }
        }

        [Hook("OnJump")]
        private void OnJump(IPlayer player, string message, MyActionGate actionGate)
        {
            if (actionGate.Allows("jumping", player))
            {
                Logger.Info("MockGatePlugin.OnJump.Allowed");
            }
        }

        [Hook("OnJump2")]
        [Allows(typeof(MyActionGate), "jumping")]
        private void OnJump2(IPlayer player, string message)
        {
            Logger.Info("MockGatePlugin.OnJump2.Allowed");
        }

        [Hook("OnSwim")]
        private void OnSwim(IPlayer player, string message, MyActionGate actionGate)
        {
            if (actionGate.Denies("swimming", player))
            {
                Logger.Info("MockGatePlugin.OnSwim.Denied");
            }
        }

        [Hook("CanJumpAndFly")]
        private bool CanJumpAndFly(IPlayer player, string message, MyActionGate actionGate)
        {
            if (actionGate.All(new[] { "jumping", "flying" }, player))
            {
                return true;
            }

            return false;
        }

        [Hook("CanSwimOrFly")]
        private bool CanSwimOrFly(IPlayer player, string message, MyActionGate actionGate)
        {
            if (actionGate.Any(new[] { "swimming", "flying" }, player))
            {
                return true;
            }

            return false;
        }

        [Hook("CanNotSwimOrFall")]
        private bool CanNotSwimOrFall(IPlayer player, string message, MyActionGate actionGate)
        {
            if (actionGate.None(new[] { "swimming", "falling" }, player))
            {
                return true;
            }

            return false;
        }

        [Hook("OnSwim2")]
        [Denies(typeof(MyActionGate), "swimming")]
        private void OnSwim2(IPlayer player, string message)
        {
            Logger.Info("MockGatePlugin.OnSwim2.Denied");
        }

        [Hook("CanJumpAndFlyWithAttribute")]
        [All(typeof(MyActionGate), "jumping", "flying")]
        private bool CanJumpAndFlyWithAttribute(IPlayer player, string message, MyActionGate actionGate)
        {
            return true;
        }

        [Hook("CanSwimOrFlyWithAttribute")]
        [Any(typeof(MyActionGate), "swimming", "flying")]
        private bool CanSwimOrFlyWithAttribute(IPlayer player, string message, MyActionGate actionGate)
        {
            return true;
        }

        [Hook("CanNotSwimOrFallWithAttribute")]
        [None(typeof(MyActionGate), "swimming", "falling")]
        private bool CanNotSwimOrFallWithAttribute(IPlayer player, string message, MyActionGate actionGate)
        {
            return true;
        }
    }
}
