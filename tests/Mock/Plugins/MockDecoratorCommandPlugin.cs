﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Command Decorator Plugin", "uMod", "1.0.0")]
    [Description("Mocking a Command Decorator plugin")]
    internal class MockCommandDecoratorPlugin : Plugin
    {
        [Hook("UnsubscribeDecorator")]
        private void UnsubscribeDecorator()
        {
            Unsubscribe(nameof(TestDecorator));
        }

        [HookDecorator]
        public class TestDecorator : HookDecorator
        {
            public TestDecorator(Plugin plugin) : base(plugin)
            { }

            [Command("testcommand")]
            private void TestCommand(IPlayer player)
            {
                Interface.uMod.LogInfo("MockCommandDecoratorPlugin.TestDecorator.TestCommand");
            }

            [Command("testcommand2")]
            private void TestCommand2(IPlayer player)
            {
                Interface.uMod.LogInfo("MockCommandDecoratorPlugin.TestDecorator.TestCommand2");
            }
        }
    }
}
