﻿using System.Collections.Generic;
using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockLocalePlugin : Plugin
    {
        [Localization(AutoLoad = false, Version = "1.0.0")]
        private interface MyOldLocale : ILocale
        {
            string Hello { get; }
            string Help { get; }
        }

        [Locale]
        public class OldLocale : MyOldLocale
        {
            public string Hello { get; set; }
            public string Help { get; set; }
        }

        class HelpLocaleData
        {
            public string Message { get; set; }
        }

        [Localization(Version = "1.0.1")]
        private interface MyLocale : ILocale
        {
            string Hello { get; set; }
            HelpLocaleData Help { get; set; }
        }

        [Locale]
        private class Default : MyLocale
        {
            public string Hello { get; set; } = "Hello {server}";
            public HelpLocaleData Help { get; set; } = new HelpLocaleData
            {
                Message = "Help"
            };
        }

        [Locale("ru")]
        private class RUDefault : MyLocale
        {
            public string Hello { get; set; } = "Привет {server}";
            public HelpLocaleData Help { get; set; } = new HelpLocaleData
            {
                Message = "Помогите"
            };
        }

        [Locale("fr")]
        private class FRDefault : MyLocale
        {
            public string Hello { get; set; } = "Bonjour {server}";
            public HelpLocaleData Help { get; set; } = new HelpLocaleData
            {
                Message = "Aidez-moi"
            };
        }

        [Toml]
        [Localization("postfix")]
        private interface MyOtherLocale : ILocale
        {
            string World { get; }
        }

        [Locale]
        private class OtherDefault : MyOtherLocale
        {
            public string World { get; } = "World";
        }

        [Locale("fr")]
        private class OtherFRDefault : MyOtherLocale
        {
            public string World { get; } = "Monde";
        }

        private MyLocale __(IPlayer player = null)
        {
            return _<MyLocale>(player);
        }

        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(IPlayer player, IServer server)
        {
            var locale = __(player);
            string HelloMessage = __(player).Hello.Interpolate(new Dictionary<string, string>()
            {
                ["server"] = server.Name
            });

            Logger.Info($"ReplyTest:{HelloMessage}");

            string WorldMessage = _<MyOtherLocale>(player).World;

            Logger.Info($"OtherReplyTest:{WorldMessage}");
        }

        [Hook("Loaded")]
        private void Loaded(MyLocale serverLocale)
        {
            if (serverLocale is MyLocale)
            {
                Logger.Info("OnMockLocalePluginResolved2Called");
            }
        }

        private void OnLocaleUpgrade(MyOldLocale oldLocale, MyLocale myLocale)
        {
            Logger.Info("MockLocalePlugin.OnLocaleUpgrade.OldLocaleToMyLocale");
            myLocale.Help = new HelpLocaleData() {Message = oldLocale.Help};
        }
    }
}
