﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockChatIntegrationPlugin : Plugin
    {
        [Hook("OnChat")]
        private void OnChat(IPlayer player, string message, HookEvent e)
        {
            Logger.Info("OnMockChatPluginChatCalled");
        }
    }
}
