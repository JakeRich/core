﻿extern alias References;

using System;
using References::Newtonsoft.Json.Linq;
using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockIntegratedPlugin : Plugin
    {
        [HookDecorator]
        private class MyDecorator : HookDecorator
        {
            public MyDecorator(MockIntegratedPlugin plugin) : base(plugin)
            {
            }

            [Hook("OnPlayerConnected")]
            public void OnPlayerConnected(IPlayer player)
            {
                Interface.uMod.LogInfo("OnPlayerConnectedHookDecorator");
            }
        }

        private class MyInnerClass : IEquatable<JToken>
        {
            public bool Equals(JToken other)
            {
                return JToken.FromObject(this).Equals(other);
            }
        }

        [Reference("MyOuterClass")]
        public class MyOuterClass : IEquatable<JToken>
        {
            public bool Equals(JToken other)
            {
                return JToken.FromObject(this).Equals(other);
            }
        }

        public MyOuterClass outer = new MyOuterClass();
        private MyInnerClass inner = new MyInnerClass();

        [Hook("Loaded", EventState.Completed)]
        public void Loaded(EventArgs e)
        {
            Interface.CallHook("IntegratedLoaded", e, inner);
        }

        [Hook("GenericHookArgument")]
        public void GenericHookArgument<TArg>(TArg someType, EventArgs e) where TArg : JToken
        {
            Logger.Info("OnMockIntegrationPluginGenericHookArgumentCalled");
        }

        [Hook("GenericHookArgumentWithResult")]
        public TResult GenericHookArgumentWithResult<TArg, TResult>(TArg someType, EventArgs e)
            where TArg : JToken
            where TResult : MyOuterClass
        {
            Logger.Info("OnMockIntegrationPluginGenericHookArgumentWithResultCalled");
            return (TResult)outer;
        }

        [Hook("GenericHookArgument2WithResult")]
        public TResult GenericHookArgument2WithResult<TArg, TArg2, TResult>(TArg someType, TArg2 someOtherType, HookEvent e)
            where TArg : JToken
            where TArg2 : IPlugin
            where TResult : MyOuterClass
        {
            Logger.Info("OnMockIntegrationPluginGenericHookArgument2WithResultCalled");
            if (someOtherType.GetType().IsAssignableFrom(GetType()))
            {
                Logger.Info("OnMockIntegrationPluginGenericHookArgument2.1WithResultCalled");
            }

            if (e?.State == EventState.Started)
            {
                Logger.Info("OnMockIntegrationPluginGenericHookArgument2.2WithResultCalled");
            }
            return (TResult)outer;
        }

        [Hook("GenericHookArgument3WithResult")]
        public TResult GenericHookArgument3WithResult<TArg, TArg2, TResult>(TArg someType, bool test, TArg2 someOtherType, HookEvent e)
            where TArg : JToken
            where TArg2 : IPlugin
            where TResult : MyOuterClass
        {
            Logger.Info("OnMockIntegrationPluginGenericHookArgument3WithResultCalled");
            if (someOtherType.GetType().IsAssignableFrom(GetType()))
            {
                Logger.Info("OnMockIntegrationPluginGenericHookArgument3.1WithResultCalled");
            }

            if (e?.State == EventState.Started)
            {
                Logger.Info("OnMockIntegrationPluginGenericHookArgument3.2WithResultCalled");
            }
            return (TResult)outer;
        }

        [Hook("IntegrationPointOne")]
        public T IntegrationPointOne<T>(EventArgs e) where T : JToken
        {
            return (T)JToken.FromObject(inner);
        }

        [Hook("IntegrationPointOne")]
        public void IntegrationPointTwo<T2>(EventArgs e, T2 test) where T2 : JToken
        {
            if (test.Equals(inner))
            {
                Logger.Info("OnMockIntegratedPluginPointTwoCalled");
            }
            else
            {
                throw new Exception("Failure");
            }
        }
    }
}
