﻿namespace uMod.Plugins
{
    [Info("Mock Logging Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    //[Log] // uses uMod.Logging.Default logger (from global config)
    [Log("stack")] // uses "stack" logger (from global config)
    [Log("my_mock_logger", "mocklog.log")] // logs to logs/mocklog.log
    [LogDaily("my_mock_logger", "mocklog_{date|yyyy-MM-dd}.log")] // logs to logs/mocklog_{yyyy-MM-dd}.log
    internal class MockLoggingPlugin : Plugin
    {
        [Hook("Loaded")]
        private void Loaded()
        {
            Logger.Info("MockLoggingPluginLoaded");
        }
    }
}
