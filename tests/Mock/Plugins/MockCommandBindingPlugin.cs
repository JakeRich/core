﻿using System.Collections.Generic;
using uMod.Common;
using uMod.Database;

namespace uMod.Plugins
{
    [Info("Mock Command Binding Plugin", "uMod", "1.0.0")]
    [Description("Fancy Command Binding Plugin")]
    internal class MockCommandBindingPlugin : Plugin
    {
        public static Dictionary<IPlayer, TestPlayer> instances = new Dictionary<IPlayer, TestPlayer>();

        [Model]
        public class TestPlayer
        {
            public static TestPlayer Resolve(IPlayer player)
            {
                instances.TryGetValue(player, out TestPlayer model);
                return model;
            }
        }

        [Hook("RegisterPlayer")]
        public void RegisterPlayer(IPlayer player)
        {
            instances.Add(player, new TestPlayer());
        }

        [Command("mockcommand")]
        private void MockCommand(IPlayer player)
        {
            Logger.Info("MockChatBindingPlugin.MockCommand IPlayer");
        }

        [Command("mockbindcommand")]
        private void MockBindCommand(TestPlayer player)
        {
            Logger.Info("MockChatBindingPlugin.MockBindCommand TestPlayer");
        }
    }
}
