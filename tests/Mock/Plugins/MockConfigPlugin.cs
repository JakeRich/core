﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.IO;

namespace uMod.Plugins
{
    [Info("Mock Config Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockConfigPlugin : Plugin
    {
        [Config(AutoLoad = false, Version = "0.0.9")]
        public class OldConfigStuff
        {
            public bool Feature = true;
        }

        [Config(AutoLoad = false, Version = "1.0.0")]
        public class ConfigStuff
        {
            public bool Feature = true;
            public string Test3 = "hello world";
        }

        [Config(Version = "1.0.1")]
        public class NewConfigStuff
        {
            public class MyFeature
            {
                public bool Enabled = true;
            }

            public MyFeature Feature = new MyFeature();
            public string Test3 = "hello world";
        }

        private NewConfigStuff MyConfig;

        [Hook("Loaded")]
        private void Loaded(NewConfigStuff config)
        {
            IDataFile<object> dataFile = Files.GetDataFile<NewConfigStuff>();
            Assert.AreSame(dataFile.Object, config);
            Logger.Info("OnMockConfigPluginLoadedCalled");
            MyConfig = config;
            if (config != null)
            {
                Logger.Info("OnMockConfigPluginConfigResolvedCalled");
            }
        }

        [Hook("OnConfigUpgrade")]
        private void OnConfigUpgrade(OldConfigStuff oldConfig, ConfigStuff newConfig)
        {
            newConfig.Feature = oldConfig.Feature;
            Logger.Info("OnMockConfigPluginConfigUpgraded1Called");
        }

        [Hook("OnConfigUpgrade")]
        private void OnConfigUpgrade(ConfigStuff oldConfig, NewConfigStuff newConfig)
        {
            newConfig.Feature.Enabled = oldConfig.Feature;
            newConfig.Test3 = "hello earth";
            Logger.Info("OnMockConfigPluginConfigUpgraded2Called");
        }

        [Hook("UpdateConfig")]
        private void UpdateConfig()
        {
            MyConfig.Feature.Enabled = !MyConfig.Feature.Enabled;
            IDataFile<object> dataFile = Files.GetDataFile<NewConfigStuff>();
            dataFile.Object = MyConfig;
            dataFile.Save();
        }
    }
}
