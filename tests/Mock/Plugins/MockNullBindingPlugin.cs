﻿using uMod.Common;
using uMod.Database;

namespace uMod.Plugins
{
    [Info("Mock Binding Null Plugin", "uMod", "1.0.0")]
    [Description("Fancy Binding Plugin")]
    internal class MockNullBindingPlugin : Plugin
    {
        public class TestConverter
        { }

        [Converter]
        public TestConverter ConvertTestConverter(int key)
        {
            Interface.uMod.LogInfo("MockNullBindingPlugin.ConvertTestConverter<int>");
            return null;
        }

        [Converter]
        public TestConverter ConvertTestConverter(IPlayer key)
        {
            Interface.uMod.LogInfo("MockNullBindingPlugin.ConvertTestConverter<IPlayer>");
            return null;
        }

        [Model]
        public class TestModel
        {
            public static TestModel Resolve(int key)
            {
                Interface.uMod.LogInfo("MockNullBindingPlugin.TestModel.Resolve<int>");
                return null;
            }

            public static TestModel Resolve(IPlayer key)
            {
                Interface.uMod.LogInfo("MockNullBindingPlugin.TestModel.Resolve<IPlayer>");
                return null;
            }
        }

        [Hook("OnBindingTestModelNull")]
        public void OnBindingTestModelNull(TestModel model)
        {
            Logger.Info("MockNullBindingPlugin.OnBindingTestModelNull");
        }

        [Hook("OnBindingTestConverterNull")]
        public void OnBindingTestConverterNull(TestConverter model)
        {
            Logger.Info("MockNullBindingPlugin.OnBindingTestConverterNull");
        }
    }
}
