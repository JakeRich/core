﻿namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockDependentPlugin : Plugin
    {
        [Requires("MockDependencyPlugin", "MockDependencyAdvancedPlugin")]
        private Plugin MockDependencyPlugin;

        [Optional]
        private Plugin MockPlugin;

        [Hook("Loaded")]
        public void Loaded()
        {
            if (MockDependencyPlugin != null)
            {
                Logger.Info("MockDependentRequiredDependencyPluginFound");
            }

            if (MockPlugin != null)
            {
                Logger.Info("MockDependentOptionalDependencyPluginFound");
            }
        }
    }
}
