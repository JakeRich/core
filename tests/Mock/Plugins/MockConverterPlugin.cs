﻿using System.Collections.Generic;

namespace uMod.Plugins
{
    [Info("Mock Converter Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockConverterPlugin : Plugin
    {
        private Dictionary<int, MockData> mockData = new Dictionary<int, MockData>();

        public class MockData
        {
            public int ID = 1;
        }

        [Hook("Loaded")]
        public void Loaded()
        {
            mockData.Add(1, new MockData());
        }

        [Converter]
        public MockData ConvertMockData(int id)
        {
            mockData.TryGetValue(id, out MockData mockDataImpl);

            return mockDataImpl;
        }

        [Hook("OnInjectedHook")]
        public void OnInjectedHook(MockData mockDataImpl)
        {
            if (mockDataImpl != null)
            {
                Logger.Info("MockConverterPlugin.OnInjectedHook.Conversion");
            }
        }
    }
}
