﻿namespace uMod.Plugins
{
    [Info("Mock Conflict Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockConflictPlugin : Plugin
    {
        [Hook("OnConflictedHook")]
        public bool OnConflictedHook()
        {
            Logger.Info("ConflictedHookCalled");
            return true;
        }

        [Hook("OnConflictResolvedHook")]
        public bool OnConflictResolvedHook(HookEvent e)
        {
            Logger.Info("ConflictResolvedHookCalled");

            e.Context.Done(delegate (Plugin source, HookEvent e2)
            {
                if (e.Context.ReturnConflict)
                {
                    e.Context.FinalValue = true;
                }
            });

            return true;
        }
    }
}
