﻿namespace uMod.Plugins
{
    [Info("Mock Test Module Plugin", "uMod", "1.0.0")]
    [Description("Fancy Test Module Plugin")]
    internal class MockTestModulePlugin : Plugin
    {
        [Requires]
        private MockModuleLoaderPlugin MockModuleLoaderPlugin;

        [Hook("Loaded")]
        private void Loaded()
        {
            MockModuleLoaderPlugin.AddModule(0, new TestModule(this));
        }

        public class TestModule : MockModuleLoaderPlugin.BaseModule
        {
            public TestModule(Plugin plugin) : base(plugin)
            {
            }

            public override void ModuleLoaded()
            {
                Interface.uMod.LogInfo("MockTestModulePlugin.TestModule.ModuleLoaded");
            }

            [Hook("ModuleChildDecoratorTest")]
            private void ModuleChildDecoratorTest(MockModuleLoaderPlugin.TestModel model)
            {
                Interface.uMod.LogInfo($"MockTestModulePlugin.TestModule.ModuleChildDecoratorTest {model.Id}");
            }
        }

        [Hook("ModuleChildTest")]
        private void ModuleChildTest(MockModuleLoaderPlugin.TestModel model)
        {
            Interface.uMod.LogInfo($"MockTestModulePlugin.ModuleChildTest {model.Id}");
        }
    }
}
