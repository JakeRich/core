﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockChatAttributeStatePlugin : Plugin
    {
        [Hook("OnChat.Completed")]
        private void OnChat(IPlayer player, string message, HookEvent e)
        {
            if (e.State == EventState.Completed)
            {
                Logger.Info("OnMockChatPluginChatCompletedCalled");
            }

            Logger.Info("OnMockChatPluginChatUNCompletedCalled");
        }
    }
}
