﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockCommandLocalizedPlugin : Plugin
    {
        [Localization]
        private interface MyLocale : ILocale
        {
            string MyCommand { get; }
            string MyCommandDescription { get; }
            string MyCommandHelp { get; }
        }

        [Locale]
        private class Default : MyLocale
        {
            public string MyCommand { get; } = "my_command";
            public string MyCommandDescription { get; } = "My command description";
            public string MyCommandHelp { get; } = "/my_command";
        }

        [Locale("ru")]
        private class RuDefault : MyLocale
        {
            public string MyCommand { get; } = "моя_команда";
            public string MyCommandDescription { get; } = "Описание моей команды";
            public string MyCommandHelp { get; } = "/моя_команда";
        }

        [Locale("fr")]
        private class FrDefault : MyLocale
        {
            public string MyCommand { get; } = "ma_commande";
            public string MyCommandDescription { get; } = "Ma description de commande";
            public string MyCommandHelp { get; } = "/ma_commande";
        }

        [Command("MyCommand")]
        [Locale(typeof(MyLocale))]
        [Description("MyCommandDescription")]
        [Help("MyCommandHelp")]
        private void cmdMockCommand(IPlayer player, string cmd, ICommandInfo info)
        {
            Logger.Info("MockCommandCalled");
            Logger.Info(Commands.GetHelp(cmd, Lang.GetLanguage(player)));
            Logger.Info(Commands.GetDescription(cmd, Lang.GetLanguage(player)));
        }
    }
}
