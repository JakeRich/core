﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockChatPlugin : Plugin
    {
        [Hook("OnChat", EventState.Completed)]
        private void OnChatCompleted(IPlayer player, string message, HookEvent e)
        {
            if (e.State == EventState.Completed)
            {
                Logger.Info("OnMockChatPluginChatCompletedCalled");
            }
            else
            {
                Logger.Info("OnMockChatPluginChatUNCompletedCalled"); // SHOULD NEVER HAPPEN ONLY FOR TESTING
            }
        }

        [Hook("OnChat", EventState.Failed)]
        private void OnChatFailed(IPlayer player, string message, HookEvent e)
        {
            if (e.State == EventState.Failed)
            {
                Logger.Info("OnMockChatPluginChatFailedCalled");
            }
        }

        [Hook("OnChat", EventState.Canceled)]
        private void OnChatCanceled(IPlayer player, string message, HookEvent e)
        {
            if (e.State == EventState.Canceled)
            {
                Logger.Info("OnMockChatPluginChatCanceledCalled");
            }
        }
    }
}
