﻿extern alias References;

using System;
using System.Collections.Generic;
using System.IO;
using uMod.Common.Database;
using uMod.Database;

namespace uMod.Plugins
{
    [Info("Mock Database Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockDatabasePlugin : Plugin
    {
        private Connection sqliteConnection;

        [Model]
        public class MockData
        {
            [PrimaryKey]
            public int id;

            public string field_one;
            public string field_two;

            [OneToOne]
            public MockRelatedData MockRelated;

            [OneToMany]
            public List<MockNoteData> MockNotes;
        }

        [Model]
        public class MockRelatedData
        {
            [PrimaryKey]
            public int id;

            [OneToOne]
            public MockData Mock;
        }

        [Model]
        public class MockNoteData
        {
            [PrimaryKey]
            public int id;

            public string content;

            [OneToOne]
            public MockData Mock;
        }

        [Migration("my_migration", 1)]
        private class MyMigration : Migration
        {
            public MyMigration(Connection connection) : base(connection)
            {
            }

            protected override void Up()
            {
                Execute("CREATE TABLE IF NOT EXISTS mock_table (field_one TEXT, field_two TEXT, id INTEGER PRIMARY KEY AUTOINCREMENT);");
                Execute("CREATE TABLE IF NOT EXISTS mock_related (mock_id NUMBER, id INTEGER PRIMARY KEY AUTOINCREMENT);");
                Execute("CREATE TABLE IF NOT EXISTS mock_notes (mock_id NUMBER, content TEXT, id INTEGER PRIMARY KEY AUTOINCREMENT);");
            }

            protected override void Down()
            {
                Execute("DROP TABLE mock_table;");
                Execute("DROP TABLE mock_related;");
                Execute("DROP TABLE mock_notes;");
            }
        }

        [Hook("Loaded")]
        private void Loaded()
        {
            string path = Path.Combine(Interface.uMod.DataDirectory, "mockdb.db");
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            Database.Open(new ConnectionInfo
            {
                Type = ConnectionType.SQLite,
                Name = "MyConnection",
                ConnectionString = $"Data Source={path};Version=3;Mode=ReadWrite;",
                Persistent = true
            }).Done((connection) =>
            {
                // connection successful
                if (connection.State == ConnectionState.Open)
                {
                    sqliteConnection = connection;
                    RunMigration().Then(SeedMockTable);
                }
            }, (exception) =>
            {
                Logger.Report("Database connection failed", exception);
            });
        }

        [Hook("Unload")]
        private void Unload()
        {
        }

        private IPromise RunMigration(MigrationDirection direction = MigrationDirection.Up)
        {
            Console.WriteLine("Running migration...");
            IPromise promise = sqliteConnection.Migrate<MyMigration>(direction);
            promise.Done(delegate
            {
                Console.WriteLine("Migration complete");
            }, Logger.Report);

            return promise;
        }

        private IPromise SeedMockTable()
        {
            Console.WriteLine("Seeding table...");

            IPromise promise = sqliteConnection.Transaction()
                .Execute("INSERT INTO mock_table (field_one, field_two) VALUES (\"hello\", \"world\");")
                .Execute("INSERT INTO mock_related (mock_id) VALUES (1);")
                .Execute("INSERT INTO mock_notes (mock_id, content) VALUES (1, \"note 1\");")
                .Execute("INSERT INTO mock_notes (mock_id, content) VALUES (1, \"note 2\");")
                .Commit();

            promise.Done(delegate ()
            {
                CallHook("OnDatabaseSetup");
            }, Logger.Report);

            return promise;
        }

        [Hook("OnDatabaseSetup")]
        private void OnDatabaseSetup()
        {
            Logger.Info("MockDatabasePlugin.OnDatabaseSetup.Called");
            sqliteConnection?.Close();
        }

        public void TestOneToOne()
        {
            sqliteConnection.Query<List<MockData>>(
                "SELECT m.id, m.field_one, m.field_two, r.id FROM mock_table m LEFT JOIN mock_related r on m.id = r.mock_id;",
                new[] { "MockRelated" },
                "id"
            ).Done((results) =>
            {
                if (results == null) return;

                foreach (MockData mockData in results)
                {
                    if (mockData.MockRelated != null)
                    {
                        Logger.Info("MockDatabasePlugin.MockDataOneToOne.Mapped");
                    }
                }
                sqliteConnection?.Close();
            }, delegate (Exception exception)
            {
                sqliteConnection?.Close();
                Logger.Report(exception);
            });
        }

        public void TestOneToMany()
        {
            sqliteConnection.Query<List<MockData>>(
                "SELECT m.id, m.field_one, m.field_two, n.id, n.content FROM mock_table m INNER JOIN mock_notes n on m.id = n.mock_id;",
                new[] { "MockNotes" },
                "id"
            ).Done((results) =>
            {
                if (results == null) return;

                foreach (MockData mockData in results)
                {
                    if (mockData.MockNotes?.Count > 0)
                    {
                        Logger.Info("MockDatabasePlugin.MockDataOneToMany.Mapped");
                    }
                }
                sqliteConnection?.Close();
            }, delegate (Exception exception)
            {
                sqliteConnection?.Close();
                Logger.Report(exception);
            });
        }
    }
}
