﻿using System;
using uMod.Common;
using uMod.Libraries;
using uMod.Mock;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockPlugin : Plugin
    {
        public class TypeA
        {

        }

        public class TypeB
        {

        }

        [Hook("InjectedContext")]
        public void InjectedContext(TypeA a, TypeB b)
        {
            if (a != null && b != null)
            {
                Logger.Info("MockPlugin.InjectedContext");
            }
        }

        protected override void LoadDefaultConfig()
        {
        }

        #region Basic Hooks Testing

        [Hook("Loaded")]
        public void Loaded()
        {
            Logger.Info("MockPlugin.Loaded");
        }

        [Hook("Loaded", EventState.Started)]
        public void LoadingStarted()
        {
            Logger.Info("MockPluginLoadingStarted");
        }

        [Hook("Unload")]
        public void Unload()
        {
            Logger.Info("MockPluginUnloaded");
        }

        public int count = 0;
        public int failureCount = 0;

        [Hook("OnBenchmark")]
        public void OnBenchmark()
        {
            count++;
        }

        [Hook("IOnInternalBenchmark")]
        public void OnInternalBenchmark(bool arg1)
        {
            count++;
        }

        [Hook("OnBenchmarkInjection")]
        public void OnBenchmarkInjection(IServer server)
        {
            if (server != null)
            {
                count++;
            }
            else
            {
                failureCount++;
            }
        }

        [Hook("OnTestOutHook")]
        public void OnTestOutHook(out string @out)
        {
            @out = "hello world";
        }

        [Hook("OnBasicHook")]
        public bool OnBasicHook()
        {
            Logger.Info("MockPlugin.OnBasicHookCalled");
            return true;
        }

        [Hook("OnSlowHook")]
        public void OnSlowHook()
        {
            System.Threading.Thread.Sleep(1000);
            Logger.Info("SlowHookCalled");
        }

        [Hook("OnOverloadedHook")]
        public void OnOverloadedHook(bool arg1)
        {
            Logger.Info("OverloadedHookOne");
        }

        [Hook("OnOverloadedHook2")]
        public void OnOverloadedHookTypeB(TypeB typeB, bool arg2)
        {
            Logger.Info("OverloadedHookTwoTypeB");
        }

        [Hook("OnOverloadedHook2")]
        public void OnOverloadedHookTypeA(TypeA typeA, bool arg2)
        {
            Logger.Info("OverloadedHookTwoTypeA");
        }

        [Hook("OnOverloadedHook")]
        public void OnOverloadedHook(bool arg1, bool arg2)
        {
            Logger.Info("OverloadedHookTwo");
        }

        [Hook("OnDegradeAdditiveHook")]
        public void OnDegradeAdditiveHook(bool arg1, bool arg2)
        {
            Logger.Info("DegradeAdditiveHookCalled");
        }

        [Hook("OnDegradeSubtractiveHook")]
        public void OnDegradeSubtractiveHook(bool arg1)
        {
            Logger.Info("DegradeSubtractiveHookCalled");
        }

        [Hook("OnHookArgumentCountMismatch")]
        public void OnHookArgumentCountMismatch(string arg1)
        {
            Logger.Info("HookArgumentCountMismatchCalled");
        }

        #endregion Basic Hooks Testing

        #region Permission Testing

        [Hook("OnPermissionRegistered")]
        public void OnPermissionRegistered(string name, Plugin plugin)
        {
            Logger.Info("OnPermissionRegisteredCalled");
        }

        [Hook("OnUserGroupAdded")]
        public void OnUserGroupAdded(string id, string name)
        {
            Logger.Info("OnUserGroupAddedCalled");
        }

        [Hook("OnUserGroupRemoved")]
        public void OnUserGroupRemoved(string id, string name)
        {
            Logger.Info("OnUserGroupRemovedCalled");
        }

        [Hook("OnGroupPermissionGranted")]
        public void OnGroupPermissionGranted(string name, string perm)
        {
            Logger.Info("OnGroupPermissionGrantedCalled");
        }

        [Hook("OnGroupPermissionRevoked")]
        public void OnGroupPermissionRevoked(string name, string perm)
        {
            Logger.Info("OnGroupPermissionRevokedCalled");
        }

        [Hook("OnGroupCreated")]
        public void OnGroupCreated(string name, string perm)
        {
            Logger.Info("OnGroupCreatedCalled");
        }

        [Hook("OnGroupDeleted")]
        public void OnGroupDeleted(string name, string perm)
        {
            Logger.Info("OnGroupDeletedCalled");
        }

        [Hook("OnGroupTitleSet")]
        public void OnGroupTitleSet(string name, string perm)
        {
            Logger.Info("OnGroupTitleSetCalled");
        }

        [Hook("OnGroupRankSet")]
        public void OnGroupRankSet(string name, int rank)
        {
            Logger.Info("OnGroupRankSetCalled");
        }

        [Hook("OnGroupParentSet")]
        public void OnGroupParentSet(string name, string perm)
        {
            Logger.Info("OnGroupParentSetCalled");
        }

        [Hook("OnUserPermissionGranted")]
        public void OnUserPermissionGranted(string name, string perm)
        {
            Logger.Info("OnUserPermissionGrantedCalled");
        }

        [Hook("OnUserPermissionRevoked")]
        public void OnUserPermissionRevoked(string name, string perm)
        {
            Logger.Info("OnUserPermissionRevokedCalled");
        }

        #endregion Permission Testing

        #region Dependency Injection

        [Hook("OnDependencyInjectedHook")]
        public void OnDependencyInjectedHook(string name, Permission perm2)
        {
            if (perm2 is Permission)
            {
                Logger.Info("OnDependencyInjectedHookCalled");
            }
        }

        #endregion Dependency Injection

        #region Mediated Overrides

        [Hook("OnIPlayerOverrideOriginal")]
        public void OnIPlayerOverrideOriginal(IPlayer player)
        {
            if (player != null)
            {
                Logger.Info("OnIPlayerOverrideOriginalCalled");
            }
        }

        [Hook("OnIPlayerOverride")]
        public void OnIPlayerOverride(GamePlayer player)
        {
            if (player != null)
            {
                Logger.Info("OnIPlayerOverrideCalled");
            }
        }

        [Hook("OnGamePlayerOverrideOriginal")]
        public void OnGamePlayerOverrideOriginal(GamePlayer player)
        {
            if (player != null)
            {
                Logger.Info("OnGamePlayerOverrideOriginalCalled");
            }
        }

        [Hook("OnGamePlayerOverride")]
        public void OnGamePlayerOverride(IPlayer player)
        {
            if (player != null)
            {
                Logger.Info("OnGamePlayerOverrideCalled");
            }
        }

        #endregion Mediated Overrides

        #region Integration Testing

        [Hook("OnHookEvent")]
        public void OnHookEvent(GamePlayer player, HookEvent e)
        {
            if (e != null)
            {
                Logger.Info("MockPlugin.OnHookEventCalled");
            }
        }

        #endregion Integration Testing

        #region Command Testing

        public int commandCount = 0;

        [Command("benchcommand")]
        private void cmdBenchCommand(IPlayer player)
        {
            commandCount++;
        }

        #endregion Command Testing
    }
}
