﻿namespace uMod.Plugins
{
    [Info("Mock Deprecation Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockDeprecationPlugin : Plugin
    {
        // Deprecated OldHook
        [Hook("OnOldHook")]
        public void OnOldHook(string String)
        {
            Logger.Info("MockDeprecationPlugin.OnOldHook.StringCalled");
        }

        // New OldHook
        [Hook("OnOldHook")]
        public void OnOldHook(bool Bool, string String)
        {
            Logger.Info("MockDeprecationPlugin.OnOldHook.BoolAndStringCalled");
        }
    }
}
