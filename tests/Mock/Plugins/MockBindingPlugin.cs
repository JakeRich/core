﻿using System.Collections.Generic;
using uMod.Common;
using uMod.Database;
using uMod.Mock;

namespace uMod.Plugins
{
    [Info("Mock Binding Plugin", "uMod", "1.0.0")]
    [Description("Fancy Binding Plugin")]
    internal class MockBindingPlugin : Plugin
    {
        public static Dictionary<int, TestModel> instances = new Dictionary<int, TestModel>();

        [Model]
        public interface ITestInterface : IModel
        {
            string Description { get; }
        }

        [Model]
        public class TestModel : ITestInterface
        {
            public string Description { get; set; }

            public static ITestInterface Resolve(int key)
            {
                instances.TryGetValue(key, out TestModel model);
                return model;
            }

            public static ITestInterface Resolve(string key)
            {
                if (int.TryParse(key, out int castKey))
                {
                    instances.TryGetValue(castKey, out TestModel model);
                    return model;
                }

                return null;
            }
        }

        [Hook("Loaded")]
        public void Loaded()
        {
            instances.Clear();
            instances.Add(1, new TestModel()
            {
                Description = "Hello"
            });

            instances.Add(2, new TestModel()
            {
                Description = "World"
            });
        }

        [Hook("OnBindingInterfaceHook")]
        public void OnBindingInterfaceHook(ITestInterface model)
        {
            Logger.Info("MockBindingPlugin.OnBindingInterfaceHookCalled");

            if (model is TestModel && model.Description == "Hello")
            {
                Logger.Info("MockBindingPlugin.OnBindingInterfaceHookArgumentPassed");
            }
        }

        [Hook("OnBindingClassHook")]
        public void OnBindingClassHook(TestModel model)
        {
            Logger.Info("MockBindingPlugin.OnBindingClassHookCalled");

            if (model is TestModel && model.Description == "World")
            {
                Logger.Info("MockBindingPlugin.OnBindingClassHookArgumentPassed");
            }
        }

        [Hook("OnPlayerTest")]
        public void OnPlayerTest(GamePlayer model)
        {
            if (model != null)
            {
                Logger.Info("OnMockBindingPluginOnPlayerTestCalled");
            }
        }

        [Hook("OnIdentityTest")]
        public void OnIdentityTest(GamePlayerIdentity model)
        {
            if (model != null)
            {
                Logger.Info("OnMockBindingPluginOnIdentityTestCalled");
            }
        }
    }
}
