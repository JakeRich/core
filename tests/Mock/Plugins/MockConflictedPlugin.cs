﻿namespace uMod.Plugins
{
    [Info("Mock Conflicted Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockConflictedPlugin : Plugin
    {
        [Hook("OnConflictedHook")]
        public bool OnConflictedHook()
        {
            Logger.Info("ConflictedHookCalled");
            return false;
        }

        [Hook("OnConflictResolvedHook")]
        public bool OnConflictResolvedHook()
        {
            Logger.Info("ConflictResolvedHookCalled");

            return false;
        }
    }
}
