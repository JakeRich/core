﻿namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockSafetyPlugin : Plugin
    {
        protected object Lock = new object();
        public int count = 0;

        [Hook("OnHookCountWithLock")]
        public void OnHookCountWithLock()
        {
            lock (Lock)
            {
                count++;
            }
        }

        [Hook("OnHookCountAsync"), Async]
        public void OnHookCountAsync()
        {
            lock (Lock)
            {
                count++;
            }
        }
    }
}
