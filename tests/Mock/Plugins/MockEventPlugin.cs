﻿using uMod.Common;

namespace uMod.Plugins
{
    [Info("Mock Event Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockEventPlugin : Plugin
    {
        public enum JumpOutcome
        {
            Complete,
            Fail,
            Cancel
        }

        public JumpOutcome jumpOutcome = JumpOutcome.Complete;

        [Hook("OnJump")]
        private void OnJump(IPlayer player, HookEvent e)
        {
            switch (jumpOutcome)
            {
                case JumpOutcome.Complete:

                    break;

                case JumpOutcome.Fail:
                    throw new System.Exception("TestFail");
                case JumpOutcome.Cancel:
                    e.Context.Canceled(delegate (Plugin plugin, HookEvent canceledArgs)
                    {
                        Logger.Info("MockEventPlugin.OnJump.CanceledCallback");
                    });
                    e.Cancel();
                    break;
            }
        }

        [Hook("OnJump", EventState.Completed)]
        private void OnJumpCompleted(IPlayer player, HookEvent e)
        {
            if (e.Completed)
            {
                Logger.Info("MockEventPlugin.OnJump.Completed");
            }
            else
            {
                Logger.Info($"State Mismatch (should be Completed): {e.StateName}");
            }
        }

        [Hook("OnJump", EventState.Failed)]
        private void OnJumpFailed(IPlayer player, HookEvent e)
        {
            if (e.Failed)
            {
                Logger.Info("MockEventPlugin.OnJump.Failed");
            }
            else
            {
                Logger.Info($"State Mismatch (should be Failed): {e.StateName}");
            }
        }

        [Hook("OnJump", EventState.Canceled)]
        private void OnJumpCanceled(IPlayer player, HookEvent e)
        {
            if (e.Canceled)
            {
                Logger.Info("MockEventPlugin.OnJump.Canceled");
            }
        }
    }
}
