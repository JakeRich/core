﻿using uMod.Plugins.Decorators;

namespace uMod.Plugins
{
    [Info("Mock Game Plugin", "uMod", "1.0.0")]
    [Description("Game Plugin")]
    [HookDecorator(typeof(ServerDecorator))]
    internal class GamePlugin : Plugin
    {
        void Loaded()
        {
            Interface.uMod.CallHook("IOnServerInitialized");
        }
    }
}
