﻿namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockNamespacePlugin : Plugin
    {
        [Hook("Loaded")]
        private void Loaded()
        {
            Logger.Info("OnMockNamespacePluginLoaded1Called");
        }

        public void TestUnsubscribe()
        {
            Unsubscribe("Loaded.MyPlugin");
        }

        public void TestSubscribe()
        {
            Subscribe("Loaded.MyPlugin");
        }

        [Hook("Loaded.MyPlugin")]
        private void LoadedMyPlugin()
        {
            Logger.Info("OnMockNamespacePluginLoaded2Called");
        }

        [Hook("Unload.MyPlugin")]
        private void UnloadMyPlugin()
        {
            Logger.Info("OnMockNamespacePluginUnloadedCalled");
        }
    }
}
