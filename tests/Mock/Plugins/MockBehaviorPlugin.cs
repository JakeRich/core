namespace uMod.Plugins
{
    [Info("Mock Behavior Plugin", "uMod", "1.0.0")]
    [Description("Fancy Behavior Plugin")]
    internal class MockBehaviorPlugin : Plugin
    {
        [Behavior]
        public class TestBehavior // : MonoBehaviour
        {
            public void Update()
            {
            }
        }
    }
}
