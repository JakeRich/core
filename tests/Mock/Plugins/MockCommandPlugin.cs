﻿using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using uMod.Mock;

namespace uMod.Plugins
{
    [Info("Mock Command Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockCommandPlugin : Plugin
    {
        #region Basic Command Testing

        [Command("mockCommand")]
        private void cmdMockCommand(IPlayer player, string cmd, string[] args)
        {
            Logger.Info("MockCommandCalled");
        }

        #endregion Basic Command Testing

        #region Advanced Command Testing

        [Command("mockAdvancedCommand")]
        private void cmdMockAdvancedCommand(IPlayer player, Args args)
        {
            if (player != null && args != null)
            {
                Logger.Info("MockAdvancedCommandCalled");
            }
        }

        [Command("mockAdvancedCommand2")]
        private void cmdMockAdvancedCommand2(IPlayer player, Args args, IServer server)
        {
            if (player != null && args != null && server != null)
            {
                Logger.Info("MockAdvancedCommand2Called");
            }
        }

        [Command("mockAdvancedCommand3")]
        private void cmdMockAdvancedCommand3(GamePlayer player, Args args)
        {
            if (args != null && player != null)
            {
                Logger.Info("MockAdvancedCommand3Called");
            }
        }

        [Command("mockAdvancedCommand4")]
        private void cmdMockAdvancedCommand4(GamePlayer player, string command, string[] args)
        {
            if (command.ToLower() == "mockadvancedcommand4" && args.Length > 0)
            {
                Logger.Info("MockAdvancedCommand4Called");
            }
        }

        [Command("mockAliasCommand1"), Command("mockAliasCommand2")]
        private void cmdMockAliasedCommand(IPlayer player, IArgs args)
        {
            Logger.Info("MockAliasedCommandCalled" + args.Command);
        }

        #endregion Advanced Command Testing

        #region Command Argument Testing

        [Command("mockArgument {arg1}")]
        private void cmdMockArgumentCommand1(IPlayer player, Args args)
        {
            if (!args.IsValid)
            {
                Logger.Info("MockArgumentCommand1Invalid");
            }

            if (args.IsArgument<int>("arg1"))
            {
                Logger.Info("MockArgumentCommand1Called");
            }
        }

        [Command("mockArgument2 {arg1} {arg2=default}")]
        private void cmdMockArgumentCommand2(IPlayer player, Args args)
        {
            if (args.IsArgument<int>("arg1") && args.IsDefault("arg2"))
            {
                Logger.Info("MockArgumentCommand2Called");
            }
        }

        [Command("mockPointArgument {arg1}")]
        private void cmdMockPointArgumentCommand(IPlayer player, Args args)
        {
            if (args.IsArgument<Point>("arg1"))
            {
                Logger.Info("MockArgumentCommandPointCalled");
            }
        }

        [Command("mockPositionArgument {arg1}")]
        private void cmdMockPositionArgumentCommand(IPlayer player, Args args)
        {
            if (args.IsArgument<Position>("arg1"))
            {
                Logger.Info("MockArgumentCommandPositionCalled");
            }
        }

        [Command("mockPosition4Argument {arg1}")]
        private void cmdMockPosition4ArgumentCommand(IPlayer player, Args args)
        {
            if (args.IsArgument<Position4>("arg1"))
            {
                Logger.Info("MockArgumentCommandPosition4Called");
            }
        }

        [Command("mockMessage {message*}")]
        private void cmdMockMessageCommand(IPlayer player, Args args)
        {
            if (args.IsArgument<string[]>("message") && args.TryGetArgument("message", out string[] message) && message.Length == 3)
            {
                Logger.Info("MockMessageCommandCalled");
            }
        }

        [Command("mockMessage2|mm {message*}")]
        private void cmdMockMessageCommandWithAlias(IPlayer player, Args args)
        {
            if (args.IsArgument<string[]>("message") && args.TryGetArgument("message", out string[] message) && message.Length == 3)
            {
                Logger.Info("MockMessageCommandCalled");
            }
        }

        #endregion Command Argument Testing
    }
}
