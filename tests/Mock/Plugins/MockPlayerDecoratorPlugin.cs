﻿using uMod.Collections;
using uMod.Common;
using uMod.Mock;

namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockPlayerDecoratorPlugin : Plugin
    {
        public class PlayerDecorator
        {
            public IPlayer Player;
            public bool Special = false;
        }

        public class MockPlayerDecorator
        {
            public MockPlayer Player;
            public bool Special = false;
        }

        [OnlinePlayers]
        public Hash<IPlayer, PlayerDecorator> MyPlayers = new Hash<IPlayer, PlayerDecorator>();

        [OnlinePlayers]
        public Hash<IPlayer, MockPlayerDecorator> MyMockPlayers = new Hash<IPlayer, MockPlayerDecorator>();
    }
}
