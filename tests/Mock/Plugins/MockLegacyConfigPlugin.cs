﻿using System.Collections.Generic;

namespace uMod.Plugins
{
    [Info("Mock Legacy Config Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    internal class MockLegacyConfigPlugin : Plugin
    {
        public MyClass ConfigValue;

        public class MyClass
        {
            public string TestString;
            public object NullableObject;
            public List<string> ListObject;
        }

        [Hook("Loaded")]
        private void Loaded()
        {
            MyClass myClass = Config.Get<MyClass>("Settings", "MyClass");

            ConfigValue = myClass;
        }

        protected override void LoadDefaultConfig()
        {
            Config["Settings", "MyClass"] = new MyClass()
            {
                TestString = "Hello world",
                NullableObject = null,
                ListObject = new List<string>() { "hello", "world" }
            };
        }
    }
}
