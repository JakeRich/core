﻿using System.Collections.Generic;
using uMod.Libraries.Universal;

namespace uMod.Plugins
{
    [Info("Mock Binding Plugin", "uMod", "1.0.0")]
    [Description("Fancy Binding Plugin")]
    class ExampleBindingPlugin : UniversalPlugin
    {
        public interface IItem
        {
            string Name { get; }
            string Description { get; }
        }

        public static Dictionary<int, MyItem> instances = new Dictionary<int, MyItem>()
        {
            { 1,  new MyItem()
            {
                Name = "Hat",
                Description = "Headware"
            } },

            { 2,  new MyItem()
            {
                Name = "Gun",
                Description = "Pew Pew"
            } }
        };

        public class MyItem : IItem, IModel<IItem>
        {
            public string Description { get; set; }
            public string Name { get; set; }

            public static IItem Resolve(int key)
            {
                instances.TryGetValue(key, out MyItem model);
                return model;
            }

            public static IItem Resolve(string key)
            {
                if (int.TryParse(key, out int castKey))
                {
                    instances.TryGetValue(castKey, out MyItem model);
                    return model;
                }

                return null;
            }
        }

        [HookMethod("Loaded")]
        public void Loaded()
        {
            // these all do the same thing
            CallHook("OnMyItemHook", instances[1]);
            CallHook("OnMyItemHook", 1);
            CallHook("OnMyItemHook", 2);
            CallHook("OnMyItemHook", "1");
        }

        [HookMethod("OnMyItemHook")]
        public void OnMyItemHook(IItem item)
        {
            if (item is IItem)
            {
                switch (item.Name)
                {
                    case "Gun":
                        // do something with gun
                        break;

                    case "Hat":
                        // do something with hat
                        break;
                }
            }
        }
    }
}
