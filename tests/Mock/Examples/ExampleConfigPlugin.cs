﻿namespace uMod.Plugins
{
    [Info("Mock Plugin", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    class ExampleConfigPlugin : UniversalPlugin
    {
        [Config]
        class ConfigStuff
        {
            public bool Test1 = true;
            public string Test3 = "hello world";
        }

        private ConfigStuff MyConfig;

        private void Loaded(ConfigStuff config)
        {
            MyConfig = config;
        }
    }
}
