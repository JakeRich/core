﻿using uMod.Plugins;

namespace uMod.Tests.Mock
{
    public class BehaviorDecorator : HookDecorator
    {
        public BehaviorDecorator(Plugin plugin) : base(plugin)
        {
        }

        [Hook("Init")]
        public void Init()
        {
            Interface.uMod.LogInfo("BehaviorInitCalled");
        }
    }
}
