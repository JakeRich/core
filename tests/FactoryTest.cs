﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Utilities;

namespace uMod.Tests
{
    [TestClass]
    public class FactoryTest : BaseTest
    {
        public class MockObject
        {
            public string FieldString;
            public int FieldInt;
            public IServer Server;

            public MockObject(string arg1, IServer server)
            {
                FieldString = arg1;
                Server = server;
            }

            public MockObject(int arg1, string arg2, IServer server)
            {
                FieldInt = arg1;
                FieldString = arg2;
                Server = server;
            }
        }

        public class MockObject<T>
        {
            public T FieldGeneric;
            public string FieldString;
            public int FieldInt;
            public IServer Server;

            public MockObject(T generic, string arg1, IServer server)
            {
                FieldGeneric = generic;
                FieldString = arg1;
                Server = server;
            }

            public MockObject(T generic, int arg1, string arg2, IServer server)
            {
                FieldGeneric = generic;
                FieldInt = arg1;
                FieldString = arg2;
                Server = server;
            }
        }

        [TestMethod]
        public void FactoryShouldActivateWithMultipleConstructors()
        {
            ProviderFactory factory = new ProviderFactory();

            MockObject obj = factory.MakeParams<MockObject>("hello");

            Assert.AreEqual("hello", obj.FieldString);
            Assert.AreEqual(Interface.uMod.Server, obj.Server);

            MockObject obj2 = factory.MakeParams<MockObject>(1, "world");

            Assert.AreEqual(1, obj2.FieldInt);
            Assert.AreEqual("world", obj2.FieldString);
            Assert.AreEqual(Interface.uMod.Server, obj2.Server);
        }

        [TestMethod]
        public void FactoryShouldActivateGenericWithMultipleConstructors()
        {
            ProviderFactory factory = new ProviderFactory();

            MockObject<int> obj = factory.MakeParams<MockObject<int>>(1, "hello");

            Assert.AreEqual("hello", obj.FieldString);
            Assert.AreEqual(1, obj.FieldGeneric);
            Assert.AreEqual(Interface.uMod.Server, obj.Server);

            MockObject<int> obj2 = factory.MakeParams<MockObject<int>>(1, 1, "world");

            Assert.AreEqual(1, obj2.FieldGeneric);
            Assert.AreEqual(1, obj2.FieldInt);
            Assert.AreEqual("world", obj2.FieldString);
            Assert.AreEqual(Interface.uMod.Server, obj2.Server);
        }
    }
}
