extern alias References;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using References::Newtonsoft.Json;
using References::ProtoBuf;
using uMod.IO;

namespace uMod.Tests
{
    [TestClass]
    public class DataFileTest : BaseTest
    {
        [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
        [Serializable]
        public class MockData
        {
            public bool Test = true;
        }

        private string DataPath => Path.Combine(AppContext.BaseDirectory, "umod/data");

        private string JsonDataFile => Path.Combine(DataPath, "MockConfig.json");

        private string TomlDataFile => Path.Combine(DataPath, "MockConfig.toml");

        private string TextDataFile => Path.Combine(DataPath, "MockText.txt");

        private string BinaryDataFile => Path.Combine(DataPath, "MockData.dat");

        private string ProtoDataFile => Path.Combine(DataPath, "MockConfig.data");

        private void CleanDataFile(string file)
        {
            bool success = false;
            int tries = 0;

            if (!File.Exists(file))
            {
                return;
            }

            while (!success && tries < 100)
            {
                try
                {
                    File.Delete(file);
                    success = true;
                    return;
                }
                catch (IOException)
                {
                    Thread.Sleep(100);
                    success = false;
                    tries++;
                }
            }

            Assert.Fail($"Unable to delete file {file}");
        }

        private void CleanDataFile()
        {
            CleanDataFile(JsonDataFile);
            CleanDataFile(TomlDataFile);
            CleanDataFile(ProtoDataFile);
            CleanDataFile(BinaryDataFile);
        }

        private class JsonTestConfig : JsonFile
        {
            public string StringVariable;
            public List<string> StringList;

            public JsonTestConfig(string filename, JsonSerializerSettings settings = null) : base(filename, settings)
            {
            }
        }

        private class TomlTestConfig : TomlFile
        {
            public string StringVariable;
            public string[] StringArray;

            public TomlTestConfig(string filename) : base(filename)
            {
            }
        }

        [TestMethod]
        public void JsonSyncFileIO()
        {
            CleanDataFile();

            JsonSyncFile inputFile = new JsonSyncFile(JsonDataFile);
            inputFile["hello", "world"] = "hello world";

            bool saveDone = false;
            inputFile.SaveFile().Done(delegate (JsonSyncFile f)
            {
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            JsonSyncFile outputFile = new JsonSyncFile(JsonDataFile);

            bool loadDone = false;
            inputFile.LoadFile().Done(delegate (JsonSyncFile f)
            {
                loadDone = true;
                Assert.AreEqual("hello world", f["hello", "world"].ToString());
                Assert.AreEqual("hello world", f.Get("hello", "world"));
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void JsonObjectFileIO()
        {
            CleanDataFile();

            List<string> stringList = new List<string>()
            {
                "hello",
                "world"
            };

            JsonTestConfig inputFile = new JsonTestConfig(JsonDataFile)
            {
                StringVariable = "hello world",
                StringList = stringList
            };

            bool saveDone = false;

            DataFile.Save<JsonTestConfig>(inputFile).Done(delegate (JsonTestConfig f)
            {
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            JsonTestConfig outputFile = new JsonTestConfig(JsonDataFile);
            bool loadDone = false;
            DataFile.Load<JsonTestConfig>(inputFile).Done(delegate (JsonTestConfig f)
            {
                loadDone = true;
                Assert.AreEqual("hello world", f.StringVariable);
                Assert.AreSame(stringList, f.StringList);
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void JsonGenericObjectFileIO()
        {
            CleanDataFile();

            MockData mockData = new MockData()
            {
                Test = true
            };

            JsonFile<MockData> inputFile = new JsonFile<MockData>(JsonDataFile, mockData);

            bool saveDone = false;
            inputFile.SaveAsync().Done(delegate (MockData data)
            {
                Assert.AreSame(mockData, data);
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            JsonFile<MockData> outputFile = new JsonFile<MockData>(JsonDataFile);
            bool loadDone = false;
            outputFile.LoadAsync().Done(delegate (MockData f)
            {
                Assert.AreEqual(true, f.Test);
                loadDone = true;
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void TomlSyncFileIO()
        {
            CleanDataFile();

            TomlSyncFile inputFile = new TomlSyncFile(TomlDataFile);
            inputFile["hello"] = "hello world";

            bool saveDone = false;
            inputFile.SaveFile().Done(delegate (TomlSyncFile f)
            {
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            TomlSyncFile outputFile = new TomlSyncFile(TomlDataFile);

            bool loadDone = false;
            inputFile.LoadFile().Done(delegate (TomlSyncFile f)
            {
                loadDone = true;
                Assert.AreEqual("hello world", f["hello"].ToString());
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void TomlObjectFileIO()
        {
            CleanDataFile();

            List<string> stringList = new List<string>()
            {
                "hello",
                "world"
            };

            TomlTestConfig inputFile = new TomlTestConfig(TomlDataFile)
            {
                StringVariable = "hello world",
                StringArray = stringList.ToArray()
            };

            bool saveDone = false;
            DataFile.Save<TomlTestConfig>(inputFile).Done(delegate (TomlTestConfig f)
            {
                saveDone = true;
            }, delegate(Exception ex)
            {
                CurrentScope.Module.LogError(ex.Message);
            });

            Assert.Await.IsTrue(() => saveDone);

            TomlTestConfig outputFile = new TomlTestConfig(TomlDataFile);
            bool loadDone = false;
            DataFile.Load<TomlTestConfig>(inputFile).Done(delegate (TomlTestConfig f)
            {
                loadDone = true;
                Assert.AreEqual("hello world", f.StringVariable);
                Assert.AreSame(stringList.ToArray(), f.StringArray);
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void TomlGenericObjectFileIO()
        {
            CleanDataFile();

            MockData mockData = new MockData()
            {
                Test = true
            };

            TomlFile<MockData> inputFile = new TomlFile<MockData>(TomlDataFile, mockData);

            bool saveDone = false;
            inputFile.SaveAsync().Done(delegate (MockData data)
            {
                Assert.AreSame(mockData, data);
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            TomlFile<MockData> outputFile = new TomlFile<MockData>(TomlDataFile);
            bool loadDone = false;
            outputFile.LoadAsync().Done(delegate (MockData f)
            {
                Assert.AreEqual(true, f.Test);
                loadDone = true;
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void TextFileIO()
        {
            CleanDataFile();

            TextFile inputFile = new TextFile(TextDataFile)
            {
                Contents = "hello world"
            };

            bool saveDone = false;
            DataFile.Save<TextFile>(inputFile).Done(delegate (TextFile f)
            {
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            TextFile outputFile = new TextFile(TextDataFile);
            bool loadDone = false;
            DataFile.Load<TextFile>(inputFile).Done(delegate (TextFile f)
            {
                loadDone = true;
                Assert.AreEqual("hello world", f.Contents);
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void BinaryFileIO()
        {
            CleanDataFile();

            MockData mockData = new MockData
            {
                Test = false
            };

            BinaryFile<MockData> inputFile = new BinaryFile<MockData>(BinaryDataFile, mockData);

            bool saveDone = false;
            DataFile.Save<BinaryFile<MockData>>(inputFile).Done(delegate (BinaryFile<MockData> f)
            {
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            BinaryFile<MockData> outputFile = new BinaryFile<MockData>(BinaryDataFile);
            bool loadDone = false;
            DataFile.Load<BinaryFile<MockData>>(inputFile).Done(delegate (BinaryFile<MockData> f)
            {
                loadDone = true;
                Assert.AreEqual(false, f.Object.Test);
            });

            Assert.Await.IsTrue(() => loadDone);
        }

        [TestMethod]
        public void GenericFileSystemFileWrite()
        {
            CleanDataFile();

            DataFileSystem<MockData> dataFile = new DataFileSystem<MockData>();

            int callbacks = 0;
            dataFile.WriteObject("MockConfig", new MockData()).Done(delegate ()
            {
                Assert.IsTrue(File.Exists(JsonDataFile));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void GenericFileSystemFileRead()
        {
            GenericFileSystemFileWrite();

            DataFileSystem<MockData> dataFile = new DataFileSystem<MockData>();

            int callbacks = 0;
            dataFile.ReadObject("MockConfig").Done(delegate (MockData readData)
            {
                Assert.IsTrue(readData.Test);
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void GenericFileSystemFileReadCreate()
        {
            CleanDataFile();

            DataFileSystem<MockData> dataFile = new DataFileSystem<MockData>();

            int callbacks = 0;
            dataFile.ReadObject("MockConfig", true).Done(delegate (MockData readData)
            {
                Assert.IsInstanceOfType(readData, typeof(MockData));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void GenericFileSystemFileWriteMultiple()
        {
            string mockDataPath1 = Path.Combine(DataPath, "MockData1.json");
            string mockDataPath2 = Path.Combine(DataPath, "MockData2.json");

            if (File.Exists(mockDataPath1))
            {
                File.Delete(mockDataPath1);
            }

            if (File.Exists(mockDataPath2))
            {
                File.Delete(mockDataPath2);
            }

            DataFileSystem<MockData> dataFile = new DataFileSystem<MockData>();

            int callbacks = 0;

            dataFile.WriteObjects(new Dictionary<string, MockData>()
            {
                { "MockData1", new MockData() },
                { "MockData2", new MockData() }
            }).Done(delegate ()
            {
                Assert.IsTrue(File.Exists(mockDataPath1));
                Assert.IsTrue(File.Exists(mockDataPath2));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void GenericFileSystemFileReadMultiple()
        {
            GenericFileSystemFileWriteMultiple();

            DataFileSystem<MockData> dataFile = new DataFileSystem<MockData>();

            int callbacks = 0;

            dataFile.ReadObjects(new List<string>()
            {
                "MockData1",
                "MockData2"
            }).Done(delegate (IEnumerable<MockData> data)
            {
                Assert.AreEqual(2, data.Count());
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1, 100, 50);
        }

        [TestMethod]
        public void FileSystemFileWrite()
        {
            CleanDataFile();

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;
            dataFile.WriteObject("MockConfig", new MockData()).Done(delegate ()
            {
                Assert.IsTrue(File.Exists(JsonDataFile));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void FileSystemFileExists()
        {
            CleanDataFile();

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;
            dataFile.WriteObject("MockConfig.json", new MockData()).Done(delegate ()
            {
                Assert.IsTrue(dataFile.Exists("MockConfig.json"));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void FileSystemFileDelete()
        {
            CleanDataFile();

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;
            dataFile.WriteObject("MockConfig.json", new MockData()).Done(delegate ()
            {
                dataFile.Delete("MockConfig.json");
                Assert.IsFalse(File.Exists(JsonDataFile));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void FileSystemGetFiles()
        {
            CleanDataFile();

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;
            dataFile.WriteObject("MockConfig.json", new MockData()).Done(delegate ()
            {
                IEnumerable<string> files = dataFile.GetFiles();
                Assert.IsTrue(files.Any());
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void FileSystemFileReadCreate()
        {
            CleanDataFile();

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;
            dataFile.ReadObject(typeof(MockData), "MockConfig", true).Done(delegate (object readData)
            {
                Assert.IsInstanceOfType(readData, typeof(MockData));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void FileSystemFileRead()
        {
            FileSystemFileWrite();

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;
            dataFile.ReadObject(typeof(MockData), "MockConfig").Done(delegate (object readData)
            {
                Assert.IsInstanceOfType(readData, typeof(MockData));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void FileSystemFileWriteMultiple()
        {
            string mockDataPath1 = Path.Combine(DataPath, "MockData1.json");
            string mockDataPath2 = Path.Combine(DataPath, "MockData2.json");

            if (File.Exists(mockDataPath1))
            {
                File.Delete(mockDataPath1);
            }

            if (File.Exists(mockDataPath2))
            {
                File.Delete(mockDataPath2);
            }

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;

            dataFile.WriteObjects(new Dictionary<string, object>()
            {
                { "MockData1", new MockData() },
                { "MockData2", new MockData() }
            }).Done(delegate ()
            {
                Assert.IsTrue(File.Exists(mockDataPath1));
                Assert.IsTrue(File.Exists(mockDataPath2));
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1);
        }

        [TestMethod]
        public void FileSystemFileReadMultiple()
        {
            FileSystemFileWriteMultiple();

            DynamicFileSystem dataFile = new DynamicFileSystem();

            int callbacks = 0;

            dataFile.ReadObjects(typeof(MockData), new List<string>()
            {
                "MockData1",
                "MockData2"
            }).Done(delegate (IEnumerable<object> data)
            {
                Assert.AreEqual(2, data.Count());
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 1, 100, 50);
        }

        [TestMethod]
        public void LegacyDataFileSystemFileWrite()
        {
            CleanDataFile();

            DataFileSystem dataFile = new DataFileSystem(DataPath);
            dataFile.WriteObject("MockConfig", new MockData());

            Assert.IsTrue(File.Exists(JsonDataFile));
        }

        [TestMethod]
        public void LegacyDataFileSystemFileExists()
        {
            CleanDataFile();

            DataFileSystem dataFile = new DataFileSystem(DataPath);
            dataFile.WriteObject("MockConfig", new MockData());

            dataFile.ExistsDatafile("MockConfig");
        }

        [TestMethod]
        public void LegacyDataFileSystemFileRead()
        {
            CleanDataFile();

            DataFileSystem dataFile = new DataFileSystem(DataPath);
            dataFile.WriteObject("MockConfig", new MockData());

            MockData mockConfig = dataFile.ReadObject<MockData>("MockConfig");
            Assert.IsTrue(mockConfig.Test);
        }

        [TestMethod]
        public void ProtoGenericObjectFileIO()
        {
            CleanDataFile();

            MockData mockData = new MockData()
            {
                Test = true
            };

            ProtoFile<MockData> inputFile = new ProtoFile<MockData>(ProtoDataFile, mockData);

            bool saveDone = false;
            inputFile.SaveAsync().Done(delegate (MockData data)
            {
                Assert.AreSame(mockData, data);
                saveDone = true;
            });

            Assert.Await.IsTrue(() => saveDone);

            ProtoFile<MockData> outputFile = new ProtoFile<MockData>(ProtoDataFile);
            bool loadDone = false;
            outputFile.LoadAsync().Done(delegate (MockData f)
            {
                Assert.AreEqual(true, f.Test);
                loadDone = true;
            });

            Assert.Await.IsTrue(() => loadDone);
        }
    }
}
