extern alias References;

using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Configuration.Toml;

namespace uMod.Tests
{
    [TestClass]
    public class TomlSerializationTest : BaseTest
    {
        private class TestSubData
        {
            public int ID = 1;
        }

        private class TestData
        {
            [TomlProperty("title")]
            public string Title = "Hello world";

            [TomlProperty("object")]
            public TestSubData SubData;

            public int[] IDArray;

            [TomlProperty("table-array")]
            [TomlTableArray]
            public TestSubData[] TableArray;

            [TomlProperty("int-list")]
            public List<int> IDList;

            [TomlProperty("table-list")]
            public List<TestSubData> TableList;

            [TomlProperty("dict")]
            public Dictionary<string, TestSubData> Dict;

            [TomlAggregate]
            public Dictionary<string, TestSubData> AggregateDict;
        }

        [TestMethod]
        public void TomlDeserializeKey()
        {
            string toml = "title = \"Hello world2\"";

            TestData data = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual("Hello world2", data.Title);
        }

        [TestMethod]
        public void TomlSerializeKey()
        {
            TestData inData = new TestData()
            {
                Title = "Hello world 3"
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual("Hello world 3", outData.Title);
        }

        [TestMethod]
        public void TomlDeserializeObject()
        {
            string toml =
                @"[object]
                ID = 2";

            TestData data = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(2, data.SubData.ID);
        }

        [TestMethod]
        public void TomlSerializeObject()
        {
            TestData inData = new TestData()
            {
                SubData = new TestSubData()
                {
                    ID = 5
                }
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(5, outData.SubData.ID);
        }

        [TestMethod]
        public void TomlDeserializeArray()
        {
            string toml =
                @"IDArray = [1, 2, 3]";

            TestData data = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, data.IDArray.Length);
            Assert.AreEqual(1, data.IDArray[0]);
            Assert.AreEqual(2, data.IDArray[1]);
            Assert.AreEqual(3, data.IDArray[2]);
        }

        [TestMethod]
        public void TomlSerializeArray()
        {
            TestData inData = new TestData()
            {
                IDArray = new int[]
                {
                    1, 2, 3
                }
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, outData.IDArray.Length);
            Assert.AreEqual(1, outData.IDArray[0]);
            Assert.AreEqual(2, outData.IDArray[1]);
            Assert.AreEqual(3, outData.IDArray[2]);
        }

        [TestMethod]
        public void TomlDeserializeList()
        {
            string toml =
                @"int-list = [1, 2, 3]";

            TestData data = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, data.IDList.Count);
            Assert.AreEqual(1, data.IDList[0]);
            Assert.AreEqual(2, data.IDList[1]);
            Assert.AreEqual(3, data.IDList[2]);
        }

        [TestMethod]
        public void TomlSerializeList()
        {
            TestData inData = new TestData()
            {
                IDList = new List<int>
                {
                    1, 2, 3
                }
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, outData.IDList.Count);
            Assert.AreEqual(1, outData.IDList[0]);
            Assert.AreEqual(2, outData.IDList[1]);
            Assert.AreEqual(3, outData.IDList[2]);
        }

        [TestMethod]
        public void TomlDeserializeTableArray()
        {
            string toml = @"
[[table-array]]
ID = 1

[[table-array]]
ID = 2

[[table-array]]
ID = 3
";

            TestData data = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, data.TableArray.Length);
            Assert.AreEqual(1, data.TableArray[0].ID);
            Assert.AreEqual(2, data.TableArray[1].ID);
            Assert.AreEqual(3, data.TableArray[2].ID);
        }

        [TestMethod]
        public void TomlSerializeTableArray()
        {
            TestData inData = new TestData()
            {
                TableArray = new TestSubData[]
                {
                    new TestSubData() { ID = 1 },
                    new TestSubData() { ID = 2 },
                    new TestSubData() { ID = 3 }
                }
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, outData.TableArray.Length);
            Assert.AreEqual(1, outData.TableArray[0].ID);
            Assert.AreEqual(2, outData.TableArray[1].ID);
            Assert.AreEqual(3, outData.TableArray[2].ID);
        }

        [TestMethod]
        public void TomlSerializeTableTable()
        {
            TestData inData = new TestData()
            {
                Dict = new Dictionary<string, TestSubData>()
                {
                    ["hello"] = new TestSubData() { ID = 1 },
                    ["world"] = new TestSubData() { ID = 2 },
                    ["universe"] = new TestSubData() { ID = 3 }
                }
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, outData.Dict.Count);
            Assert.AreEqual(1, outData.Dict["hello"].ID);
            Assert.AreEqual(2, outData.Dict["world"].ID);
            Assert.AreEqual(3, outData.Dict["universe"].ID);
        }

        [TestMethod]
        public void TomlSerializeTableAggregateTable()
        {
            TestData inData = new TestData()
            {
                AggregateDict = new Dictionary<string, TestSubData>()
                {
                    ["hello"] = new TestSubData() { ID = 1 },
                    ["world"] = new TestSubData() { ID = 2 },
                    ["universe"] = new TestSubData() { ID = 3 }
                }
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, outData.AggregateDict.Count);
            Assert.AreEqual(1, outData.AggregateDict["hello"].ID);
            Assert.AreEqual(2, outData.AggregateDict["world"].ID);
            Assert.AreEqual(3, outData.AggregateDict["universe"].ID);
        }

        [TestMethod]
        public void TomlDeserializeTableList()
        {
            string toml = @"
[[table-list]]
ID = 1

[[table-list]]
ID = 2

[[table-list]]
ID = 3
";

            TestData data = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, data.TableList.Count);
            Assert.AreEqual(1, data.TableList[0].ID);
            Assert.AreEqual(2, data.TableList[1].ID);
            Assert.AreEqual(3, data.TableList[2].ID);
        }

        [TestMethod]
        public void TomlSerializeTableList()
        {
            TestData inData = new TestData()
            {
                TableList = new List<TestSubData>
                {
                    new TestSubData() { ID = 1 },
                    new TestSubData() { ID = 2 },
                    new TestSubData() { ID = 3 }
                }
            };

            string toml = TomlConvert.SerializeObject(inData);

            TestData outData = TomlConvert.DeserializeObject<TestData>(toml);

            Assert.AreEqual(3, outData.TableList.Count);
            Assert.AreEqual(1, outData.TableList[0].ID);
            Assert.AreEqual(2, outData.TableList[1].ID);
            Assert.AreEqual(3, outData.TableList[2].ID);
        }
    }
}
