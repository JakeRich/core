﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Auth;
using uMod.Common;
using uMod.Database;
using uMod.IO;
using uMod.Mock;

namespace uMod.Tests
{
    [TestClass]
    [Application("Database")]
    public class AuthSeederTest : BaseTest
    {
        private Dictionary<string, GroupData> GetMockGroupData()
        {
            return new Dictionary<string, GroupData>
            {
                {
                    "mock_group",
                    new GroupData()
                    {
                        Title = "Mock Group",
                        Perms = new HashSet<string>()
                        {
                            "somepermission"
                        }
                    }
                }
            };
        }

        private Dictionary<string, UserData> GetMockUserData()
        {
            return new Dictionary<string, UserData>
            {
                {
                    "12345",
                    new UserData()
                    {
                        LastSeenNickname = "bob",
                        Perms = new HashSet<string>()
                        {
                            "somepermission"
                        }
                    }
                },

                {
                    "54321",
                    new UserData()
                    {
                        LastSeenNickname = "john",
                        Perms = new HashSet<string>()
                        {
                            "someotherpermission"
                        }
                    }
                }
            };
        }

        private AuthTransformer MakeSeeder(DataFormat sourceFormat, DataFormat targetFormat)
        {
            Configuration.AuthDriver sourceDriver = default;
            Configuration.AuthDriver targetDriver = default;

            switch (sourceFormat)
            {
                case DataFormat.Json:
                    sourceDriver = Configuration.AuthDriver.Json;
                    break;

                case DataFormat.Proto:
                    sourceDriver = Configuration.AuthDriver.Protobuf;
                    break;

                case DataFormat.Sql:
                    sourceDriver = Configuration.AuthDriver.Database;
                    break;
            }

            switch (targetFormat)
            {
                case DataFormat.Json:
                    targetDriver = Configuration.AuthDriver.Json;
                    break;

                case DataFormat.Proto:
                    targetDriver = Configuration.AuthDriver.Protobuf;
                    break;

                case DataFormat.Sql:
                    targetDriver = Configuration.AuthDriver.Database;
                    break;
            }

            return new AuthTransformer(Interface.uMod.Database, sourceDriver, targetDriver);
        }

        private IPromise DeleteDatabase()
        {
            Promise promise = new Promise();
            Interface.uMod.Database.Client.Open().Done(delegate (Connection connection)
            {
                connection.Close().Done(delegate (Connection conn)
                {
                    File.Delete(Path.Combine(Interface.uMod.DataDirectory, "umod.db"));
                    promise.Resolve();
                }, promise.Reject);
            }, promise.Reject);
            return promise;
        }

        [DataTestMethod]
        [DataRow(DataFormat.Json, true)]
        [DataRow(DataFormat.Proto, true)]
        public void ConvertFromFileToSql(DataFormat format, bool deleteDatabase = true)
        {
            int callbacks = 0;

            Action test = delegate
            {
                Dictionary<string, UserData> userData = GetMockUserData();
                Dictionary<string, GroupData> groupData = GetMockGroupData();

                IDataFile<Dictionary<string, GroupData>> groupFile = DataSystem.MakeDataFile(Path.Combine(Interface.uMod.DataDirectory, "umod.groups"), format, groupData);
                if (groupFile.Exists)
                {
                    groupFile.Delete();
                }

                groupFile.SaveFile().Done(delegate (IDataFile<Dictionary<string, GroupData>> groupFileImpl)
                {
                    IDataFile<Dictionary<string, UserData>> userFile = DataSystem.MakeDataFile(Path.Combine(Interface.uMod.DataDirectory, "umod.users"), format, userData);
                    if (userFile.Exists)
                    {
                        userFile.Delete();
                    }

                    userFile.SaveFile().Done(delegate (IDataFile<Dictionary<string, UserData>> file)
                    {
                        AuthTransformer seeder = MakeSeeder(format, DataFormat.Sql);
                        seeder.Invoke().Done(delegate
                        {
                            Interface.uMod.Database.Client.Open().Done(delegate (Connection connection)
                            {
                                connection.Query<List<Player>>("SELECT * FROM umod_players").Done(delegate (List<Player> players)
                                {
                                    Assert.AreEqual(2, players.Count);
                                    callbacks++;
                                    groupFile.Delete();
                                    userFile.Delete();
                                }, exception => throw exception);
                            });
                        }, exception => throw exception);

                        callbacks++;
                    });
                });
            };

            ICallback callback = null;
            if (Interface.uMod.Auth.Configuration.Initialized)
            {
                callback = Interface.uMod.Auth.Configuration.OnInitialized.Add(test);
                DeleteDatabase().Done(delegate
                {
                    Interface.uMod.Auth.Configuration.Initialized = false;
                    Interface.uMod.Auth.Configuration.Initialize(Interface.uMod.Database, Interface.uMod.Dispatcher);
                }, exception => throw exception);
            }
            else
            {
                test();
            }

            Assert.Await.IsTrue(() => callbacks == 2, 200, 200);
            if (callback != null)
            {
                Event.Remove(ref callback);
            }
            if (deleteDatabase)
            {
                DeleteDatabase().Done(delegate
                {
                    callbacks++;
                });
                Assert.Await.IsTrue(() => callbacks == 3, 200, 200);
            }
        }

        [DataTestMethod]
        [DataRow(DataFormat.Json)]
        [DataRow(DataFormat.Proto)]
        public void ConvertFromSqlToFile(DataFormat targetFormat)
        {
            // Create dummy sql data from proto file
            ConvertFromFileToSql(DataFormat.Proto, false);
            int callbacks = 0;
            AuthTransformer seeder = MakeSeeder(DataFormat.Sql, targetFormat);
            seeder.Invoke().Done(delegate
            {
                IDataFile<Dictionary<string, UserData>> userFile = DataSystem.MakeDataFile<Dictionary<string, UserData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.users"), targetFormat);
                IDataFile<Dictionary<string, GroupData>> groupFile = DataSystem.MakeDataFile<Dictionary<string, GroupData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.groups"), targetFormat);
                userFile.LoadFile().Done(delegate (IDataFile<Dictionary<string, UserData>> data)
                {
                    Assert.AreEqual(2, data.Object.Count);
                    callbacks++;
                    userFile.Delete();
                    groupFile.Delete();
                });
                callbacks++;
            }, exception => throw exception);

            Assert.Await.IsTrue(() => callbacks == 2, 200, 200);
            DeleteDatabase().Done(delegate
            {
                callbacks++;
            });

            Assert.Await.IsTrue(() => callbacks == 3, 200, 200);
        }

        [DataTestMethod]
        [DataRow(DataFormat.Json, DataFormat.Proto)]
        [DataRow(DataFormat.Proto, DataFormat.Json)]
        public void ConvertFileFormat(DataFormat sourceFormat, DataFormat targetFormat)
        {
            int callbacks = 0;
            Dictionary<string, UserData> userData = GetMockUserData();
            Dictionary<string, GroupData> groupData = GetMockGroupData();

            // Create source format files
            IDataFile<Dictionary<string, UserData>> sourceUserFile = DataSystem.MakeDataFile(Path.Combine(Interface.uMod.DataDirectory, "umod.users"), sourceFormat, userData);
            IDataFile<Dictionary<string, GroupData>> sourceGroupFile = DataSystem.MakeDataFile(Path.Combine(Interface.uMod.DataDirectory, "umod.groups"), sourceFormat, groupData);

            // Delete source files if they exist
            if (sourceUserFile.Exists)
            {
                sourceUserFile.Delete();
            }

            if (sourceGroupFile.Exists)
            {
                sourceGroupFile.Delete();
            }

            // Allocate target files
            IDataFile<Dictionary<string, UserData>> targetUserFile = null;
            IDataFile<Dictionary<string, GroupData>> targetGroupFile = null;

            // Save group file
            sourceGroupFile.SaveFile().Done(delegate (IDataFile<Dictionary<string, GroupData>> fileImpl)
            {
                // Assign group file
                targetGroupFile = DataSystem.MakeDataFile<Dictionary<string, GroupData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.groups"), targetFormat);

                // Save user file
                sourceUserFile.SaveFile().Done(delegate (IDataFile<Dictionary<string, UserData>> fileImpl2)
                {
                    // Create seeder
                    AuthTransformer seeder = MakeSeeder(sourceFormat, targetFormat);

                    // Invoke seeder
                    seeder.Invoke().Done(delegate
                    {
                        // Assign user file
                        targetUserFile = DataSystem.MakeDataFile<Dictionary<string, UserData>>(Path.Combine(Interface.uMod.DataDirectory, "umod.users"), targetFormat);

                        // Load user file
                        targetUserFile.LoadFile().Done(delegate (IDataFile<Dictionary<string, UserData>> file)
                        {
                            // Verify user file contents
                            Assert.AreEqual(2, file.Object.Count);
                            Assert.AreEqual(2, targetUserFile.Object.Count);
                            callbacks++;
                        }, exception => throw exception);
                        callbacks++;
                    }, exception => throw exception);
                });
            }, exception => throw exception);

            Assert.Await.IsTrue(() => callbacks == 2, 200, 200);
            targetUserFile?.Delete();
            targetGroupFile?.Delete();
            DeleteDatabase().Done(delegate
            {
                callbacks++;
            });
            Assert.Await.IsTrue(() => callbacks == 3, 200, 200);
        }
    }
}
