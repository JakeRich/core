﻿namespace uMod.Plugins
{
    [Info("Arena Event", "uMod", "0.0.1")]
    [Description("Arena Test uMod Plugin")]
    public class ArenaEventPlugin : Plugin, BaseArenaPlugin.IMessenger
    {
        [Requires] private BaseArenaPlugin BaseArenaPlugin;

        private void OnServerInitialized()
        {
            BaseArenaPlugin?.CallHook("OnArenaLoaded", this);
        }

        public void SayHi()
        {
            BaseArenaPlugin.TestClass testClass = new BaseArenaPlugin.TestClass();
            Interface.uMod.LogInfo(testClass.Message);
        }
    }
}
