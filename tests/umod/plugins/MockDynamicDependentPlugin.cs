﻿namespace uMod.Plugins
{
    [Info("Mock Dynamic Dependent Plugin", "uMod", "1.0.0")]
    [Description("Fancy Module Loader Plugin")]
    public class MockDynamicDependentPlugin : Plugin
    {
        public interface IMockInterface
        {
            public string Test { get; }
        }
    }
}
