﻿namespace uMod.Plugins
{
    [Info("Batch Compiled Plugin #2", "uMod", "1.0.0")]
    [Description("Mocking a plugin")]
    public class BatchCompilePlugin2 : Plugin
    {
        public bool TestPluginCompiled()
        {
            return true;
        }
    }
}
