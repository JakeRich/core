#if DEBUG

using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Core.Utilities;

namespace uMod.Tests
{
    //[TestClass]
    public class ReflectionUtilityTest
    {
        private class MockObject
        {
            public MockChildObject MockChild = new MockChildObject();
        }

        private class MockChildObject
        {
            public string StringField = "string value";
            public string StringProperty { get; } = "string property";

            public string GetString()
            {
                return "string method";
            }
        }

        [TestMethod]
        public void GetChildValues()
        {
            MockChildObject obj = new MockChildObject();

            Assert.AreEqual("string value", ReflectionUtility.GetValue<string>(obj, "StringField"));
            Assert.AreEqual("string property", ReflectionUtility.GetValue<string>(obj, "StringProperty"));
            Assert.AreEqual("string method", ReflectionUtility.GetValue<string>(obj, "GetString"));
        }

        [TestMethod]
        public void FindValues()
        {
            MockObject obj = new MockObject();

            Assert.AreEqual("string value", ReflectionUtility.GetValue<string>(obj, "MockChild.StringField"));
            Assert.AreEqual("string property", ReflectionUtility.GetValue<string>(obj, "MockChild.StringProperty"));
            Assert.AreEqual("string method", ReflectionUtility.GetValue<string>(obj, "MockChild.GetString"));
        }
    }
}

#endif
