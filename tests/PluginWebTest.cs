extern alias References;

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using References::Newtonsoft.Json.Linq;
using uMod.Common.Web;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;
using uMod.Web;

namespace uMod.Tests
{
    [TestClass]
    [Application("WebClient")]
    [Plugin(typeof(MockPlugin))]
    public class PluginWebTest : BaseTest
    {
        private Client Web => new Client(GetPlugin<MockPlugin>());

        [TestMethod]
        public void GetLegacyRequestCallback()
        {
            MockPlugin plugin = GetPlugin<MockPlugin>();
            WebRequests webrequest = CurrentScope.Module.Libraries.Get<WebRequests>();
            int responseCount = 0;
            webrequest.Enqueue("http://httpbin.org/get?test=hello", null, delegate (WebResponse response)
            {
                responseCount++;
            }, plugin);

            webrequest.Enqueue("http://httpbin.org/get?test=hello", null, delegate (int code, string response)
            {
                responseCount++;
            }, plugin);

            Assert.Await.IsTrue(() => responseCount == 2, 100, 50);
        }

        [TestMethod]
        public void GetRequestCallback()
        {
            bool responseTriggered = false;
            Web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    responseTriggered = true;
                    Assert.IsInstanceOfType(e, typeof(WebResponse));
                    string responseString = e.ReadAsString();
                    JObject responseData = JObject.Parse(responseString);
                    Assert.AreEqual(responseData["args"]["test"], "hello");
                });

            Assert.Await.IsTrue(() => responseTriggered);
        }

        [TestMethod]
        public void GetMultipleRequestCallback()
        {
            int count = 0;
            Web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    count++;
                    Assert.IsInstanceOfType(e, typeof(WebResponse));
                    string responseString = e.ReadAsString();
                    JObject responseData = JObject.Parse(responseString);
                    Assert.AreEqual(responseData["args"]["test"], "hello");
                });

            Assert.Await.IsTrue(() => count == 1, 100, 200);

            Web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    count++;
                    Assert.IsInstanceOfType(e, typeof(WebResponse));
                    string responseString = e.ReadAsString();
                    JObject responseData = JObject.Parse(responseString);
                    Assert.AreEqual(responseData["args"]["test"], "hello");
                });

            Assert.Await.IsTrue(() => count == 2, 100, 200);
        }

        [TestMethod]
        public void GetRequestCallbackWithoutContext()
        {
            bool responseTriggered = false;
            Client web = new Client();
            web.Get("http://httpbin.org/get?test=hello")
                .Done(delegate (WebResponse e)
                {
                    responseTriggered = true;
                    Assert.IsInstanceOfType(e, typeof(WebResponse));
                    string responseString = e.ReadAsString();
                    JObject responseData = JObject.Parse(responseString);
                    Assert.AreEqual(responseData["args"]["test"], "hello");
                });

            Assert.Await.IsTrue(() => responseTriggered);
        }

        [TestMethod]
        public void GetRequestFailed()
        {
            bool responseTriggered = false;
            bool failTriggered = false;
            Web.Get("not_a_url")
                .Done(delegate (WebResponse e)
                {
                    responseTriggered = true;
                }, delegate (Exception ex)
                {
                    failTriggered = true;
                });

            Assert.Await.IsTrue(() => failTriggered);
            Assert.IsFalse(responseTriggered);
        }
    }
}
