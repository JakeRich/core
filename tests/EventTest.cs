﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;

namespace uMod.Tests
{
    [TestClass]
    public class EventTest : BaseTest
    {
        [TestMethod]
        public void EventInvoke()
        {
            Event @event = new Event();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Invoke();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void EventRemove()
        {
            Event @event = new Event();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Add(delegate
            {
                callbacks++;
            });

            ICallback callback3 = @event.Add(delegate
            {
                callbacks++;
            });

            Event.Remove(ref callback3);

            @event.Invoke();

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void EventArg1Invoke()
        {
            Event<int> @event = new Event<int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Invoke(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void EventArg1Remove()
        {
            Event<int> @event = new Event<int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Add(delegate
            {
                callbacks++;
            });

            ICallback<int> callback3 = @event.Add(delegate
            {
                callbacks++;
            });

            Event.Remove(ref callback3);

            @event.Invoke(1);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void EventArg2Invoke()
        {
            Event<int, int> @event = new Event<int, int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Invoke(1);
            @event.Invoke(1, 2);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void EventArg2Remove()
        {
            Event<int, int> @event = new Event<int, int>();

            int callbacks = 0;
            @event.Add(delegate
           {
               callbacks++;
           });

            @event.Add(delegate
            {
                callbacks++;
            });

            ICallback<int, int> callback3 = @event.Add(delegate
            {
                callbacks++;
            });

            Event.Remove(ref callback3);

            @event.Invoke(1, 2);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void EventArg3Invoke()
        {
            Event<int, int, int> @event = new Event<int, int, int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Invoke(1);
            @event.Invoke(1, 2);
            @event.Invoke(1, 2, 3);

            Assert.AreEqual(3, callbacks);
        }

        [TestMethod]
        public void EventArg3Remove()
        {
            Event<int, int, int> @event = new Event<int, int, int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Add(delegate
            {
                callbacks++;
            });

            ICallback<int, int, int> callback3 = @event.Add(delegate
            {
                callbacks++;
            });

            Event.Remove(ref callback3);

            @event.Invoke(1, 2, 3);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void EventArg4Invoke()
        {
            Event<int, int, int, int> @event = new Event<int, int, int, int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Invoke(1);
            @event.Invoke(1, 2);
            @event.Invoke(1, 2, 3);
            @event.Invoke(1, 2, 3, 4);

            Assert.AreEqual(4, callbacks);
        }

        [TestMethod]
        public void EventArg4Remove()
        {
            Event<int, int, int, int> @event = new Event<int, int, int, int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Add(delegate
            {
                callbacks++;
            });

            ICallback<int, int, int, int> callback3 = @event.Add(delegate
            {
                callbacks++;
            });

            Event.Remove(ref callback3);

            @event.Invoke(1, 2, 3, 4);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void EventArg5Invoke()
        {
            Event<int, int, int, int, int> @event = new Event<int, int, int, int, int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Invoke(1);
            @event.Invoke(1, 2);
            @event.Invoke(1, 2, 3);
            @event.Invoke(1, 2, 3, 4);
            @event.Invoke(1, 2, 3, 4, 5);

            Assert.AreEqual(5, callbacks);
        }

        [TestMethod]
        public void EventArg5Remove()
        {
            Event<int, int, int, int, int> @event = new Event<int, int, int, int, int>();

            int callbacks = 0;
            @event.Add(delegate
            {
                callbacks++;
            });

            @event.Add(delegate
            {
                callbacks++;
            });

            ICallback<int, int, int, int, int> callback3 = @event.Add(delegate
            {
                callbacks++;
            });

            Event.Remove(ref callback3);

            @event.Invoke(1, 2, 3, 4, 5);

            Assert.AreEqual(2, callbacks);
        }
    }
}
