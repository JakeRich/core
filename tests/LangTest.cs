﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class LangTest : BaseTest
    {
        private Libraries.Lang LangLibrary => Interface.uMod.Libraries.Get<Libraries.Lang>();

        private string ConfigDirectory => Path.Combine(AppContext.BaseDirectory, Path.Combine("umod" + Path.DirectorySeparatorChar + "lang"));

        private string GetConfigDirectory(string lang = "en")
        {
            return Path.Combine(ConfigDirectory, lang);
        }

        private string GetConfigFile(string fileName, string lang = "en")
        {
            return Path.Combine(GetConfigDirectory(lang), fileName);
        }

        [TestMethod]
        public void LangLibraryInitialized()
        {
            Assert.IsTrue(LangLibrary != null);
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void RegisterMessages()
        {
            Plugin plugin = GetPlugin<MockPlugin>();

            LangLibrary.RegisterMessages(new Dictionary<string, string>
            {
                { "MockKey", "MockValue"}
            }, plugin);

            Assert.IsTrue(File.Exists(GetConfigFile("MockPlugin.json")));
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void GetMessages()
        {
            Plugin plugin = GetPlugin<MockPlugin>();

            LangLibrary.RegisterMessages(new Dictionary<string, string>
            {
                { "MockKey", "MockValue"}
            }, plugin);

            Dictionary<string, string> messages = LangLibrary.GetMessages("en", plugin);

            Assert.AreEqual(1, messages.Count);

            string value = LangLibrary.GetMessage("MockKey", plugin);

            Assert.AreEqual("MockValue", value);
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void RegisterFRMessages()
        {
            Plugin plugin = GetPlugin<MockPlugin>();

            LangLibrary.RegisterMessages(new Dictionary<string, string>
            {
                { "bonjour", "le monde"}
            }, plugin, "fr");

            Assert.IsTrue(File.Exists(GetConfigFile("MockPlugin.json", "fr")));
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void GetFRMessages()
        {
            Plugin plugin = GetPlugin<MockPlugin>();

            LangLibrary.RegisterMessages(new Dictionary<string, string>
            {
                { "bonjour", "le monde"}
            }, plugin, "fr");

            Dictionary<string, string> messages = LangLibrary.GetMessages("fr", plugin);

            Assert.AreEqual(1, messages.Count);
        }

        [TestMethod]
        public void GetLanguages()
        {
            Assert.IsTrue(LangLibrary.GetLanguages().Length > 0);
        }

        [TestMethod]
        public void UpdateServerLanguage()
        {
            Assert.IsTrue(LangLibrary.HasLanguage("fr"));

            LangLibrary.SetServerLanguage("fr");

            Assert.AreEqual("fr", LangLibrary.GetServerLanguage());

            LangLibrary.SetServerLanguage("en");
        }
    }
}
