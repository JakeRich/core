using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Plugins;

namespace uMod.Tests
{
    public class PermissionTest : BaseTest
    {
        private Libraries.Permission PermissionLibrary => Interface.uMod.Libraries.Get<Libraries.Permission>();

        private const string SteamID = "76561197960265729";
        private Plugin plugin => GetPlugin<MockPlugin>();

        [TestMethod]
        public void PermissionLibraryInitialized()
        {
            Assert.IsTrue(PermissionLibrary != null);
        }

        [TestMethod]
        public void CreateGroup()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            Assert.IsTrue(PermissionLibrary.CreateGroup("admin", "Administrator", 1));

            Assert.IsTrue(PermissionLibrary.GroupExists("admin"));
            Assert.AreEqual(PermissionLibrary.GetGroupRank("admin"), 1);
            Assert.AreEqual(PermissionLibrary.GetGroupTitle("admin"), "Administrator");
            Assert.IsTrue(PermissionLibrary.GetGroups().Length >= 1);

            Assert.Await.IsLogged("OnGroupCreatedCalled");
        }

        [TestMethod]
        public void AddRemoveUserGroup()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.AddUserGroup(SteamID, "admin");

            Assert.IsTrue(PermissionLibrary.UserHasGroup(SteamID, "admin"));
            Assert.IsTrue(PermissionLibrary.UserHasAnyGroup(SteamID));

            Assert.Await.IsLogged("OnUserGroupAddedCalled");

            PermissionLibrary.RemoveUserGroup(SteamID, "admin");

            Assert.IsFalse(PermissionLibrary.UserHasGroup(SteamID, "admin"));

            Assert.Await.IsLogged("OnUserGroupRemovedCalled");
        }

        [TestMethod]
        public void GrantRevokeUserPermission()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            if (PermissionLibrary.GroupExists("subadmin"))
            {
                PermissionLibrary.RemoveGroup("subadmin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.CreateGroup("subadmin", "Sub-Administrator", 1, "admin");

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            PermissionLibrary.RegisterPermission("mockplugin.test_test", plugin);

            PermissionLibrary.GrantUserPermission(SteamID, "mockplugin.test", plugin);

            PermissionLibrary.GrantGroupPermission("admin", "mockplugin.test_test", plugin);

            PermissionLibrary.AddUserGroup(SteamID, "subadmin");

            Assert.IsTrue(PermissionLibrary.UserHasPermission(SteamID, "mockplugin.test"));

            Assert.IsTrue(PermissionLibrary.IsUserPermissionInherited(SteamID, "mockplugin.test_test"));

            PermissionLibrary.RevokeUserPermission(SteamID, "mockplugin.test");

            Assert.Await.IsLogged("OnUserPermissionRevokedCalled");

            Assert.IsFalse(PermissionLibrary.UserHasPermission(SteamID, "mockplugin.test"));

            PermissionLibrary.RemoveGroup("subadmin");

            PermissionLibrary.RevokeUserPermission(SteamID, "mockplugin.test");

            PermissionLibrary.RevokeGroupPermission("admin", "mockplugin.test_test");

            PermissionLibrary.UnregisterPermission("mockplugin.test_test", plugin);
        }

        [DataTestMethod]
        [DataRow("*")]
        [DataRow("test*")]
        public void RevokeUserWildcardPermissions(string perm)
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);
            PermissionLibrary.RegisterPermission("mockplugin.test2", plugin);

            PermissionLibrary.GrantUserPermission(SteamID, "mockplugin.*", plugin);
            string[] UserPermissions = PermissionLibrary.GetUserPermissions(SteamID);

            Assert.AreEqual(2, UserPermissions.Length);

            Assert.IsTrue(UserPermissions.Contains("mockplugin.test"));
            Assert.IsTrue(UserPermissions.Contains("mockplugin.test2"));

            PermissionLibrary.RevokeUserPermission(SteamID, $"mockplugin.{perm}");

            UserPermissions = PermissionLibrary.GetUserPermissions(SteamID);
            Assert.AreEqual(0, UserPermissions.Length);
        }

        [TestMethod]
        public void GetUserPermissionGroup()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            if (PermissionLibrary.UserHasPermission(SteamID, "mockplugin.test"))
            {
                PermissionLibrary.RevokeUserPermission(SteamID, "mockplugin.test");
            }

            PermissionLibrary.GrantGroupPermission("admin", "mockplugin.test", plugin);

            PermissionLibrary.AddUserGroup(SteamID, "admin");

            Assert.AreEqual("admin", PermissionLibrary.GetUserPermissionGroup(SteamID, "mockplugin.test"));
        }

        [TestMethod]
        public void GetUserGroups()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            if (PermissionLibrary.GroupExists("subadmin"))
            {
                PermissionLibrary.RemoveGroup("subadmin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            PermissionLibrary.GrantGroupPermission("admin", "mockplugin.test", plugin);

            PermissionLibrary.AddUserGroup(SteamID, "admin");
            string[] UserGroups = PermissionLibrary.GetUserGroups(SteamID);

            Assert.AreEqual(1, UserGroups.Length);

            Assert.AreEqual("admin", UserGroups[0]);
        }

        [TestMethod]
        public void GrantUserPermissions()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            PermissionLibrary.GrantGroupPermission("admin", "mockplugin.test", plugin);

            PermissionLibrary.AddUserGroup(SteamID, "admin");
            string[] UserPermissions = PermissionLibrary.GetUserPermissions(SteamID);

            Assert.AreEqual(1, UserPermissions.Length);

            Assert.AreEqual("mockplugin.test", UserPermissions[0]);
        }

        [DataTestMethod]
        [DataRow("*")]
        [DataRow("test*")]
        public void GrantUserWildcardPermissions(string perm)
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);
            PermissionLibrary.RegisterPermission("mockplugin.test2", plugin);

            PermissionLibrary.GrantGroupPermission("admin", $"mockplugin.{perm}", plugin);

            PermissionLibrary.AddUserGroup(SteamID, "admin");
            string[] UserPermissions = PermissionLibrary.GetUserPermissions(SteamID);

            Assert.AreEqual(2, UserPermissions.Length);

            Assert.AreEqual("mockplugin.test", UserPermissions[0]);
            Assert.AreEqual("mockplugin.test2", UserPermissions[1]);
        }

        [TestMethod]
        public void GetPermissionUsers()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            PermissionLibrary.GrantUserPermission(SteamID, "mockplugin.test", plugin);
            string[] PermissionUsers = PermissionLibrary.GetPermissionUsers("mockplugin.test");

            Assert.AreEqual(1, PermissionUsers.Length);
        }

        [TestMethod]
        public void GetGroupPermissions()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            PermissionLibrary.GrantGroupPermission("admin", "mockplugin.test", plugin);

            PermissionLibrary.AddUserGroup(SteamID, "admin");
            string[] UserPermissions = PermissionLibrary.GetGroupPermissions("admin");

            Assert.AreEqual(1, UserPermissions.Length);

            Assert.AreEqual("mockplugin.test", UserPermissions[0]); ;
        }

        [TestMethod]
        public void RemoveGroup()
        {
            if (PermissionLibrary.GroupExists("test"))
            {
                PermissionLibrary.RemoveGroup("test");
            }

            PermissionLibrary.CreateGroup("test", "Test", 1);

            PermissionLibrary.RemoveGroup("test");

            Assert.Await.IsLogged("OnGroupDeletedCalled");
        }

        [TestMethod]
        public void CreateChildGroup()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            if (PermissionLibrary.GroupExists("subadmin"))
            {
                PermissionLibrary.RemoveGroup("subadmin");
            }

            PermissionLibrary.CreateGroup("subadmin", "Sub-Administrator", 1, "admin");

            Assert.AreEqual(PermissionLibrary.GetGroupParent("subadmin"), "admin");

            PermissionLibrary.RemoveGroup("subadmin");
        }

        [TestMethod]
        public void GrantGroupPermissions()
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            PermissionLibrary.GrantGroupPermission("admin", "mockplugin.test", plugin);

            Assert.Await.IsLogged("OnPermissionRegisteredCalled");
            Assert.Await.IsLogged("OnGroupPermissionGrantedCalled");

            Assert.IsTrue(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test"));

            PermissionLibrary.RevokeGroupPermission("admin", "mockplugin.test");

            Assert.IsFalse(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test"));

            Assert.Await.IsLogged("OnGroupPermissionRevokedCalled");
        }

        [DataTestMethod]
        [DataRow("*")]
        [DataRow("test*")]
        public void GrantGroupWildcardPermissions(string perm)
        {
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);
            PermissionLibrary.RegisterPermission("mockplugin.test2", plugin);

            PermissionLibrary.GrantGroupPermission("admin", $"mockplugin.{perm}", plugin);

            Assert.Await.IsLogged("OnPermissionRegisteredCalled");
            Assert.Await.IsLogged("OnGroupPermissionGrantedCalled");

            Assert.IsTrue(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test"));
            Assert.IsTrue(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test2"));

            PermissionLibrary.RevokeGroupPermission("admin", "mockplugin.test");
            PermissionLibrary.RevokeGroupPermission("admin", "mockplugin.test2");

            Assert.IsFalse(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test"));
            Assert.IsFalse(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test2"));

            Assert.Await.IsLogged("OnGroupPermissionRevokedCalled");
        }

        [TestMethod]
        public void GroupNestingAndDetailsUpdate()
        {
            if (PermissionLibrary.GroupExists("test"))
            {
                PermissionLibrary.RemoveGroup("test");
            }

            if (PermissionLibrary.GroupExists("test2"))
            {
                PermissionLibrary.RemoveGroup("test2");
            }

            PermissionLibrary.CreateGroup("test", "Test", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            PermissionLibrary.GrantGroupPermission("test", "mockplugin.test", plugin);

            PermissionLibrary.CreateGroup("test2", "Test 2", 0);

            PermissionLibrary.SetGroupParent("test2", "test");

            Assert.AreEqual(PermissionLibrary.GetGroupParent("test2"), "test");

            Assert.IsTrue(PermissionLibrary.IsGroupPermissionInherited("test2", "mockplugin.test"));

            Assert.Await.IsLogged("OnGroupParentSetCalled");

            PermissionLibrary.SetGroupRank("test2", 3);

            Assert.AreEqual(PermissionLibrary.GetGroupRank("test2"), 3);

            Assert.Await.IsLogged("OnGroupRankSetCalled");

            PermissionLibrary.SetGroupTitle("test2", "Test Title");

            Assert.AreEqual(PermissionLibrary.GetGroupTitle("test2"), "Test Title");

            Assert.Await.IsLogged("OnGroupTitleSetCalled");
        }

        [TestMethod]
        public void MigrateGroup()
        {
            if (PermissionLibrary.GroupExists("test"))
            {
                PermissionLibrary.RemoveGroup("test");
            }

            PermissionLibrary.CreateGroup("test", "Test 1", 1);

            if (!PermissionLibrary.PermissionExists("mockplugin.test", plugin))
            {
                PermissionLibrary.RegisterPermission("mockplugin.test", plugin);
            }

            PermissionLibrary.GrantGroupPermission("test", "mockplugin.test", plugin);

            if (PermissionLibrary.GroupExists("test2"))
            {
                PermissionLibrary.RemoveGroup("test2");
            }

            PermissionLibrary.CreateGroup("test2", "Test 2", 1);

            PermissionLibrary.MigrateGroup("test", "test2");

            Assert.AreEqual(1, PermissionLibrary.GetGroupPermissions("test2").Length);
        }

        [TestMethod]
        public void PermissionExists()
        {
            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);

            Assert.IsTrue(PermissionLibrary.PermissionExists("mockplugin.test", plugin));
        }

        [TestMethod]
        public void GetPermissions()
        {
            PermissionLibrary.RegisterPermission("mockplugin.test", plugin);
            string[] PluginPermissions = PermissionLibrary.GetPermissions(plugin);

            Assert.AreEqual(1, PluginPermissions.Length);

            Assert.AreEqual("mockplugin.test", PluginPermissions[0]);
        }

        [TestMethod]
        public void PermissionExistsWildcard()
        {
            PermissionLibrary.RegisterPermission("mockplugin.test_test", plugin);

            Assert.IsTrue(PermissionLibrary.PermissionExists("mockplugin.test_*", plugin));

            PermissionLibrary.UnregisterPermission("mockplugin.test_test", plugin);
        }
    }
}
