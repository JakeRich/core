using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    public class PluginTest : BaseTest
    {
        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void BasicHookCall()
        {
            bool result = (bool)Interface.uMod.CallHook("OnBasicHook");

            Assert.Await.IsLogged("MockPlugin.OnBasicHookCalled");
            Assert.IsTrue(result);
        }

        /*
        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void AsyncHookCall()
        {
            Interface.uMod.CallHook("OnAsyncHook");

            Assert.Await.Log("AsyncHookCalled");

            Assert.Await.IsLogged("AsyncHookCalled");
        }

        [TestMethod]
        public void AsyncCallHookSafety()
        {
            MockSafetyPlugin mockPlugin = (MockSafetyPlugin)GetPlugin(typeof(MockSafetyPlugin));

            mockPlugin.count = 0;
            ThreadPool.QueueUserWorkItem(delegate (object context)
            {
                for (int x = 0; x < 50000; x++)
                {
                    Interface.uMod.CallHook("OnHookCountWithLock");
                }
            });

            ThreadPool.QueueUserWorkItem(delegate (object context)
            {
                for (int x = 0; x < 50000; x++)
                {
                    Interface.uMod.CallHook("OnHookCountWithLock");
                }
            });

            Await(() => mockPlugin.count == 100000, 50, 50);
            Assert.AreEqual(100000, mockPlugin.count);
        }
        */

        /*
        [TestMethod]
        public void AsyncHookSafetyCall()
        {
            MockSafetyPlugin mockPlugin = (MockSafetyPlugin)GetPlugin(typeof(MockSafetyPlugin));

            mockPlugin.count = 0;
            for (int x = 0; x < 100000; x++)
            {
                Interface.uMod.CallHook("OnHookCountAsync");
            }

            Assert.Await.IsTrue(() => mockPlugin.count == 100000));
        }

        [TestMethod]
        public void SlowHookCall()
        {
            Interface.uMod.CallHook("OnSlowHook");

            Assert.IsTrue(AwaitAsyncResponse("SlowHookCalled"));
            Assert.IsTrue(AwaitAsyncResponse("[Warning] Calling 'OnSlowHook' on 'MockPlugin v1.0.0' took"));
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin), typeof(MockChatIntegrationPlugin))]
        public void CompanionAsyncHookCall()
        {
            // This test is an example of calling multiple async hooks simultaneously, use this to check for lock conflicts
            MockPlugin mockPlugin = GetPlugin<MockPlugin>();
            MockChatIntegrationPlugin mockChatPlugin = GetPlugin<MockChatIntegrationPlugin>();

            MockPlayer gameplayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            EventArgs<Plugin> newArgs = new EventArgs<Plugin>(mockPlugin);
            int counter = 0;
            int totalEach = 1000;

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                for (int i = 1; i <= totalEach; i++)
                {
                    Interface.uMod.Dispatcher.Dispatch(delegate
                    {
                        Interface.uMod.CallHook("OnAsyncHook");
                        return true;
                    });
                    counter++;
                }

                return true;
            });

            Interface.uMod.Dispatcher.Dispatch(delegate ()
            {
                for (int i = 1; i <= totalEach; i++)
                {
                    Interface.uMod.Dispatcher.Dispatch(delegate
                    {
                        Interface.uMod.CallHook("OnAsyncHook");
                        return true;
                    });
                    counter++;
                }

                return true;
            });

            Assert.Await.IsTrue(delegate ()
            {
                return counter == (totalEach * 2);
            }, 100, 50);

            Assert.IsTrue(counter == (totalEach * 2));

            Assert.Await.IsLogged("AsyncHookCalled");
        }
        */

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void BasicHookOutCall()
        {
            Plugin mockPlugin = GetPlugin<MockPlugin>();

            int i = 0;
            while (!(bool)mockPlugin?.IsResolved)
            {
                Thread.Sleep(25);
                if (i > 60)
                {
                    break;
                }
                i++;
            }

            mockPlugin.CallHook("OnTestOutHook", out string outTest);

            //Interface.uMod.CallHook("OnTestOutHook", outTest);

            Assert.AreSame(outTest, "hello world");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void BasicOverloadedHookCall()
        {
            Interface.uMod.CallHook("OnOverloadedHook", true);

            Assert.Await.Log("OverloadedHookOne");

            Assert.Await.IsLogged("OverloadedHookOne");

            Interface.uMod.CallHook("OnOverloadedHook", true, true);

            Assert.Await.Log("OverloadedHookTwo");

            Assert.Await.IsLogged("OverloadedHookTwo");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void BasicOverloadedCastHookCall()
        {
            MockPlugin.TypeA typeA = new MockPlugin.TypeA();
            MockPlugin.TypeB typeB = new MockPlugin.TypeB();
            Interface.uMod.CallHook("OnOverloadedHook2", typeA, true);
            Interface.uMod.CallHook("OnOverloadedHook2", typeB, true);
            // Call again for cached delegate in hookmethod
            Interface.uMod.CallHook("OnOverloadedHook2", typeA, true);
            Interface.uMod.CallHook("OnOverloadedHook2", typeB, true);

            Assert.Await.IsLogged("OverloadedHookTwoTypeA");
            Assert.Await.IsLogged("OverloadedHookTwoTypeB");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void BasicDegradationHookCall()
        {
            Interface.uMod.CallHook("OnDegradeSubtractiveHook", true);

            Assert.Await.IsLogged("DegradeSubtractiveHookCalled");

            Interface.uMod.CallHook("OnDegradeAdditiveHook", true, true);

            Assert.Await.IsLogged("DegradeAdditiveHookCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void BasicDependencyInjectedHook()
        {
            Interface.uMod.CallHook("OnDependencyInjectedHook", "");

            Assert.Await.IsLogged("OnDependencyInjectedHookCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void ContextDependencyInjection()
        {
            HookEvent e = new HookEvent();
            e.Context.AddContext(new MockPlugin.TypeA());
            e.Context.AddContext(new MockPlugin.TypeB());

            GetPlugin<MockPlugin>().CallHook("InjectedContext", null, e);

            Assert.Await.IsLogged("MockPlugin.InjectedContext");
        }

        [TestMethod]
        [Plugin(typeof(MockBehaviorPlugin))]
        public void HookDecoratorBehavior()
        {
            Interface.uMod.CallHook("Init");

            Assert.Await.IsLogged("BehaviorInitCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockDecoratorPlugin))]
        public void PluginDecorator()
        {
            Assert.Await.IsLogged("MockDecoratorInitCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlayerDecoratorPlugin))]
        public void HookPlayerDecorator()
        {
            MockPlayerDecoratorPlugin mockPlugin = GetPlugin<MockPlayerDecoratorPlugin>();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.CallHook("OnPlayerConnected", player);

            Assert.AreEqual(1, mockPlugin.MyPlayers.Count);

            mockPlugin.CallHook("OnPlayerDisconnected", player);

            Assert.Await.IsTrue(() => mockPlugin.MyPlayers.Count == 0);
        }

        [TestMethod]
        [Plugin(typeof(MockPlayerDecoratorPlugin))]
        public void HookMockPlayerDecorator()
        {
            MockPlayerDecoratorPlugin mockPlugin = GetPlugin<MockPlayerDecoratorPlugin>();

            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.CallHook("OnPlayerConnected", player);

            Assert.AreEqual(1, mockPlugin.MyMockPlayers.Count);

            mockPlugin.CallHook("OnPlayerDisconnected", player);

            Assert.Await.IsTrue(() => mockPlugin.MyMockPlayers.Count == 0);
        }

        [TestMethod]
        [Plugin(typeof(MockIntegratedPlugin))]
        public void HookDecorator()
        {
            Plugin mockPlugin = GetPlugin<MockIntegratedPlugin>();

            Assert.IsInstanceOfType(mockPlugin, typeof(MockIntegratedPlugin));
            MockPlayer mockPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
            Interface.uMod.CallHook("OnPlayerConnected", mockPlayer);

            Assert.Await.IsLogged("OnPlayerConnectedHookDecorator");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void HookIPlayerOverride()
        {
            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            Interface.uMod.CallHook("OnIPlayerOverrideOriginal", player);
            Interface.uMod.CallHook("OnIPlayerOverride", player);

            Assert.Await.IsLogged("OnIPlayerOverrideOriginalCalled");
            Assert.Await.IsLogged("OnIPlayerOverrideCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void HookGamePlayerOverride()
        {
            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            Interface.uMod.CallHook("OnGamePlayerOverrideOriginal", player.Object);
            Interface.uMod.CallHook("OnGamePlayerOverride", player.Object);

            Assert.Await.IsLogged("OnGamePlayerOverrideOriginalCalled");
            Assert.Await.IsLogged("OnGamePlayerOverrideCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void HookArgumentCountMismatch()
        {
            Interface.uMod.CallHook("OnHookArgumentCountMismatch", "hello", "world");

            Assert.Await.IsLogged("HookArgumentCountMismatchCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void HookEventArgsWithIPlayerOverride()
        {
            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            Interface.uMod.CallHook("OnHookEvent", player);

            Assert.Await.IsLogged("MockPlugin.OnHookEventCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void HookEventArgs()
        {
            Interface.uMod.CallHook("OnHookEvent", new GamePlayer(new GamePlayerIdentity()));

            Assert.Await.IsLogged("MockPlugin.OnHookEventCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void PluginLoadedUnloadedHookCalls()
        {
            Plugin mockPlugin = GetPlugin<MockPlugin>();
            mockPlugin.CallHook("Loaded");
            Assert.Await.IsLogged("MockPlugin.Loaded");

            //Interface.uMod.Plugins.Unload(mockPlugin);

            //Assert.IsTrue(AwaitAsyncResponse("MockPluginUnloaded"));

            //Interface.uMod.PluginLoaded(mockPlugin);
        }

        [TestMethod]
        [Plugin(typeof(MockNamespacePlugin))]
        public void PluginNamespacedUnloadedHookCalls()
        {
            Plugin mockPlugin = GetPlugin<MockNamespacePlugin>();

            CurrentScope.PluginLoader.PluginUnloaded(mockPlugin);

            Assert.Await.IsLogged("OnMockNamespacePluginUnloadedCalled");

            bool callback = false;
            CurrentScope.PluginLoader.PluginLoaded(mockPlugin).Done(delegate () { callback = true; });
            Assert.Await.IsTrue(() => callback);
        }

        [TestMethod]
        [Plugin(typeof(MockNamespacePlugin))]
        public void PluginNamespacedCalls()
        {
            Plugin mockPlugin = GetPlugin<MockNamespacePlugin>();

            mockPlugin.CallHook("Loaded");

            Assert.Await.IsLogged("OnMockNamespacePluginLoaded2Called");

            Assert.Await.IsLogged("OnMockNamespacePluginLoaded1Called");
        }

        [TestMethod]
        [Plugin(typeof(MockNamespacePlugin))]
        public void PluginNamespacedCalls2()
        {
            Plugin mockPlugin = GetPlugin<MockNamespacePlugin>();
            ClearLog();
            Interface.uMod.CallHook("Loaded.MyPlugin");

            Assert.Await.IsNotLogged("MockPlugin.Loaded");
            Assert.Await.IsLogged("OnMockNamespacePluginLoaded2Called");
        }

        [TestMethod]
        [Plugin(typeof(MockNamespacePlugin))]
        public void PluginUnsubscribeNamespaced()
        {
            MockNamespacePlugin mockPlugin = GetPlugin<MockNamespacePlugin>();

            mockPlugin.TestUnsubscribe();

            ClearLog();

            mockPlugin.CallHook("Loaded");

            Assert.Await.IsLogged("OnMockNamespacePluginLoaded1Called");
        }

        [TestMethod]
        [Plugin(typeof(MockNamespacePlugin))]
        public void PluginResubscribeNamespaced()
        {
            MockNamespacePlugin mockPlugin = GetPlugin<MockNamespacePlugin>();

            mockPlugin.TestUnsubscribe();

            mockPlugin.CallHook("Loaded");

            mockPlugin.TestSubscribe();

            ClearLog();

            mockPlugin.CallHook("Loaded");

            Assert.Await.IsLogged("OnMockNamespacePluginLoaded1Called");
            Assert.Await.IsLogged("OnMockNamespacePluginLoaded2Called");
        }

        [TestMethod]
        [Plugin(typeof(MockChatIntegrationPlugin), typeof(MockChatPlugin))]
        public void EventArgsIntegration()
        {
            MockPlayer iPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
            Interface.uMod.CallHook("OnChat", iPlayer, "test");
            Assert.Await.IsLogged("OnMockChatPluginChatCalled");
            Assert.Await.IsLogged("OnMockChatPluginChatCompletedCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockChatPlugin))]
        public void EventArgsIntegrationDirectCompleted()
        {
            Interface.uMod.CallHook("OnChat.Completed");
            Assert.Await.IsNotLogged("OnMockChatPluginChatCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockChatIntegrationPlugin), typeof(MockChatPlugin))]
        public void EventArgsIntegrationCompleted()
        {
            MockPlayer iPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
            Interface.uMod.CallHook("OnChat", iPlayer, "test");

            Assert.Await.IsLogged("OnMockChatPluginChatCompletedCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockChatPlugin))]
        public void EventArgsFailedTest()
        {
            Plugin mockPlugin = GetPlugin<MockChatPlugin>();
            MockPlayer iPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
            HookEvent e = new HookEvent();
            e.Fail(string.Empty);
            Interface.uMod.CallHook("OnChat.Failed", iPlayer, "test", e);
            Assert.Await.IsLogged("OnMockChatPluginChatFailedCalled");
            e.Cancel();
            Interface.uMod.CallHook("OnChat.Canceled", iPlayer, "test", e);
            Assert.Await.IsLogged("OnMockChatPluginChatCanceledCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockChatIntegrationPlugin), typeof(MockChatPlugin))]
        public void EventArgsIntegrationExclusion()
        {
            MockPlayer iPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);
            Interface.uMod.CallHook("OnChat.Completed", iPlayer, "test");
            Assert.Await.IsNotLogged("OnMockChatPluginChatCalled");
            Assert.Await.IsLogged("OnMockChatPluginChatUNCompletedCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockEventPlugin))]
        public void EventArgsPassthru()
        {
            MockPlayer iPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            Interface.uMod.CallHook("OnJump", iPlayer);
            Assert.Await.IsLogged("MockEventPlugin.OnJump.Completed");
        }

        [TestMethod]
        [Plugin(typeof(MockEventPlugin))]
        public void EventArgsPassthruCancel()
        {
            MockEventPlugin mockPlugin = GetPlugin<MockEventPlugin>();

            MockPlayer iPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.jumpOutcome = MockEventPlugin.JumpOutcome.Cancel;
            Interface.uMod.CallHook("OnJump", iPlayer);
            Assert.Await.IsLogged("MockEventPlugin.OnJump.Canceled");
            Assert.Await.IsLogged("MockEventPlugin.OnJump.CanceledCallback");
        }

        [TestMethod]
        [Plugin(typeof(MockEventPlugin))]
        public void EventArgsPassthruFail()
        {
            MockEventPlugin mockPlugin = GetPlugin<MockEventPlugin>();

            MockPlayer iPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            mockPlugin.jumpOutcome = MockEventPlugin.JumpOutcome.Fail;
            Interface.uMod.CallHook("OnJump", iPlayer);
            Assert.Await.IsLogged("MockEventPlugin.OnJump.Failed");
        }

        [TestMethod]
        [Plugin(typeof(MockBindingPlugin))]
        public void ModelBinding()
        {
            Plugin mockPlugin = GetPlugin<MockBindingPlugin>();

            mockPlugin.CallHook("OnBindingInterfaceHook", 1);
            mockPlugin.CallHook("OnBindingClassHook", 2);

            Assert.Await.IsLogged("MockBindingPlugin.OnBindingInterfaceHookCalled");
            Assert.Await.IsLogged("MockBindingPlugin.OnBindingInterfaceHookArgumentPassed");
            Assert.Await.IsLogged("MockBindingPlugin.OnBindingClassHookCalled");
            Assert.Await.IsLogged("MockBindingPlugin.OnBindingClassHookArgumentPassed");

            RestoreLog();
            ClearLog();

            mockPlugin.CallHook("OnBindingInterfaceHook", "1");

            Assert.Await.IsLogged("MockBindingPlugin.OnBindingInterfaceHookCalled");
            Assert.Await.IsLogged("MockBindingPlugin.OnBindingInterfaceHookArgumentPassed");
        }

        [TestMethod]
        [Plugin(typeof(MockBindingPlugin))]
        public void ModelParameterConverter()
        {
            Plugin mockPlugin = GetPlugin<MockBindingPlugin>();

            GamePlayer player = new GamePlayer(new GamePlayerIdentity());

            mockPlugin.CallHook("OnPlayerTest", player.Identity);
            mockPlugin.CallHook("OnIdentityTest", player);

            Assert.Await.IsLogged("OnMockBindingPluginOnPlayerTestCalled");
            Assert.Await.IsLogged("OnMockBindingPluginOnIdentityTestCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockConverterPlugin))]
        public void HookWithConverterShouldConvert()
        {
            Plugin mockPlugin = GetPlugin<MockConverterPlugin>();

            mockPlugin.CallHook("OnInjectedHook", 1);

            Assert.Await.IsLogged("MockConverterPlugin.OnInjectedHook.Conversion");
        }

        /// <summary>
        /// Determine if converter with parameter overloading gets called
        /// </summary>
        [TestMethod]
        [Plugin(typeof(MockBindingPlugin))]
        public void HookWithModelBindingAndParameterOverloadingShouldConvert()
        {
            Plugin mockPlugin = GetPlugin<MockBindingPlugin>();

            mockPlugin.CallHook("OnBindingInterfaceHook", 1, 0);

            Assert.Await.IsLogged("MockBindingPlugin.OnBindingInterfaceHookCalled");
        }

        [TestMethod]
        [Plugin(typeof(MockNullBindingPlugin))]
        public void HookWithModelNullConversionShouldSkipHook()
        {
            Plugin mockPlugin = GetPlugin<MockNullBindingPlugin>();

            mockPlugin.CallHook("OnBindingTestModelNull", 1);

            Assert.Await.IsLogged("MockNullBindingPlugin.TestModel.Resolve<int>");

            Assert.Await.IsNotLogged("MockNullBindingPlugin.OnBindingTestModelNull");

            Assert.IsFalse(TestScope.Log.ToString().Contains("[Error]"));
        }

        [TestMethod]
        [Plugin(typeof(MockNullBindingPlugin))]
        public void HookWithConverterNullConversionShouldSkipHook()
        {
            Plugin mockPlugin = GetPlugin<MockNullBindingPlugin>();

            mockPlugin.CallHook("OnBindingTestConverterNull", 1);

            Assert.Await.IsLogged("MockNullBindingPlugin.ConvertTestConverter<int>");

            Assert.Await.IsNotLogged("MockNullBindingPlugin.OnBindingTestConverterNull");

            Assert.IsFalse(TestScope.Log.ToString().Contains("[Error]"));
        }

        [TestMethod]
        [Plugin(typeof(MockConflictPlugin), typeof(MockConflictedPlugin))]
        public void PluginConflictedHook()
        {
            bool result = (bool)Interface.uMod.CallHook("OnConflictedHook");

            Assert.IsFalse(result);

            Assert.Await.IsLogged("ConflictedHookCalled");
            Assert.Await.IsLogged("[Warning]");
        }

        [TestMethod]
        [Plugin(typeof(MockConflictPlugin), typeof(MockConflictedPlugin))]
        public void PluginConflictResolvedHook()
        {
            bool result = (bool)Interface.uMod.CallHook("OnConflictResolvedHook");

            Assert.IsTrue(result);

            Assert.Await.IsLogged("ConflictResolvedHookCalled");

            Assert.IsFalse(TestScope.Log.ToString().Contains("[Warning]"));
        }

        /// <summary>
        /// Determine if converter from parent plugin with primitive argument works in hook decorator of dependent/child plugin
        /// </summary>
        [TestMethod]
        [Plugin(typeof(MockModuleLoaderPlugin), typeof(MockTestModulePlugin))]
        public void HookWithDependencyConverterAndPrimitiveArgumentShouldConvert()
        {
            Plugin moduleLoader = GetPlugin<MockModuleLoaderPlugin>();

            moduleLoader.CallHook("LoadModule", 0);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleLoaded");

            Interface.Call("ModuleChildDecoratorTest", 0);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleChildDecoratorTest 0");

            Interface.Call("ModuleParentTest", 0);

            Assert.Await.IsLogged("MockModuleLoaderPlugin.ModuleParentTest 0");

            Interface.Call("ModuleChildTest", 0);

            Assert.Await.IsLogged("MockTestModulePlugin.ModuleChildTest 0");
        }

        /// <summary>
        /// Determine if converter from parent plugin with GamePlayer argument works in hook decorator of dependent/child plugin
        /// </summary>
        [TestMethod]
        [Plugin(typeof(MockModuleLoaderPlugin), typeof(MockTestModulePlugin))]
        public void HookWithDependencyConverterAndGamePlayerArgumentShouldConvert()
        {
            Plugin moduleLoader = GetPlugin<MockModuleLoaderPlugin>();

            moduleLoader.CallHook("LoadModule", 0);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleLoaded");

            GamePlayer player = new GamePlayer(new GamePlayerIdentity());

            Interface.Call("ModuleChildDecoratorTest", player);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleChildDecoratorTest 1");
        }

        /// <summary>
        /// Determine if converter from parent plugin with IPlayer argument works in hook decorator of dependent/child plugin
        /// </summary>
        [TestMethod]
        [Plugin(typeof(MockModuleLoaderPlugin), typeof(MockTestModulePlugin))]
        public void HookWithDependencyConverterAndIPlayerArgumentShouldConvert()
        {
            Plugin moduleLoader = GetPlugin<MockModuleLoaderPlugin>();

            moduleLoader.CallHook("LoadModule", 0);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleLoaded");

            MockPlayer mockPlayer = new MockPlayer(new GamePlayer(new GamePlayerIdentity()));

            Interface.Call("ModuleChildDecoratorTest", mockPlayer);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleChildDecoratorTest 2");
        }

        /// <summary>
        /// Determine if converter from parent plugin with custom module argument works in hook decorator of dependent/child plugin
        /// </summary>
        [TestMethod]
        [Plugin(typeof(MockModuleLoaderPlugin), typeof(MockTestModulePlugin))]
        public void HookWithDependencyConverterAndCustomModuleArgumentShouldConvert()
        {
            Plugin moduleLoader = GetPlugin<MockModuleLoaderPlugin>();

            moduleLoader.CallHook("LoadModule", 0);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleLoaded");

            MockModuleLoaderPlugin.TestModel testModel = new MockModuleLoaderPlugin.TestModel { Id = 3 };

            Interface.Call("ModuleChildDecoratorTest", testModel);

            Assert.Await.IsLogged("MockTestModulePlugin.TestModule.ModuleChildDecoratorTest 3");
        }
    }
}
