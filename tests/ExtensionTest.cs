﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Tests.Mock;

namespace uMod.Tests
{
    [TestClass]
    public class ExtensionTest : BaseTest
    {
        [TestMethod]
        public void InMemoryExtensionLoad()
        {
            Assert.IsTrue(Interface.uMod.Extensions.Load(typeof(MockExtension)));

            IEnumerable<IExtension> extensions = Interface.uMod.Extensions.All;

            Assert.AreEqual(1, extensions.Count());

            Assert.IsInstanceOfType(extensions.ToArray()[0], typeof(MockExtension));
        }
    }
}
