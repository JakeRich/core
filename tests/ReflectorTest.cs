﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Utilities;

namespace uMod.Tests
{
    [TestClass]
    public class ReflectorTest : BaseTest
    {
        public class MockClass
        {
            private string MockField = "hello";

            private string MockProperty { get; set; } = "world";
        }

        [TestMethod]
        public void ClassWithPrivateFieldShouldAccess()
        {
            Reflector<MockClass> reflector = new Reflector<MockClass>();
            MockClass mockObject = new MockClass();
            Assert.AreEqual("hello", reflector.GetValue<string>(mockObject, "MockField"));
        }

        [TestMethod]
        public void ClassWithPrivatePropertyShouldAccess()
        {
            Reflector<MockClass> reflector = new Reflector<MockClass>();
            MockClass mockObject = new MockClass();
            Assert.AreEqual("world", reflector.GetValue<string>(mockObject, "MockProperty"));
        }

        [TestMethod]
        public void ClassWithPrivateFieldShouldMutate()
        {
            Reflector<MockClass> reflector = new Reflector<MockClass>();
            MockClass mockObject = new MockClass();
            reflector.SetValue(mockObject, "MockField", "greeting");
            Assert.AreEqual("greeting", reflector.GetValue<string>(mockObject, "MockField"));
        }

        [TestMethod]
        public void ClassWithPrivatePropertyShouldMutate()
        {
            Reflector<MockClass> reflector = new Reflector<MockClass>();
            MockClass mockObject = new MockClass();
            reflector.SetValue(mockObject, "MockProperty", "location");
            Assert.AreEqual("location", reflector.GetValue<string>(mockObject, "MockProperty"));
        }
    }
}
