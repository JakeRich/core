﻿extern alias References;

using System.IO;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common.Database;
using uMod.Database;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    [Application("Database")]
    [Plugin(typeof(MockPlugin))]
    public class PluginDatabaseTest : BaseTest
    {
        public static int DBID;
        private Client database;

        private Client Database
        {
            get
            {
                if (database != null)
                {
                    return database;
                }

                Plugin mockPlugin = GetPlugin<MockPlugin>();
                database = new Client(Interface.uMod.Database, mockPlugin);
                return database;
            }
        }

        [TestInitialize]
        public void Initialize()
        {
            DBID = Utility.Random.Range(0, 99999);
        }

        [TestCleanup]
        public void Cleanup()
        {
            Cleanup($"{Interface.uMod.DataDirectory}/{DBID}.db");
            Cleanup($"{Interface.uMod.DataDirectory}/mockdb.db");
        }

        private void Cleanup(string path)
        {
            if (!File.Exists(path))
            {
                return;
            }
            bool success = false;
            int tries = 0;

            while (!success && tries < 100)
            {
                try
                {
                    File.Delete(path);
                    success = true;
                }
                catch (IOException)
                {
                    Thread.Sleep(100);
                    success = false;
                    tries++;
                }
            }
        }

        public ConnectionInfo GetValidConnectionInfo()
        {
            string DataSource = $"{Interface.uMod.DataDirectory}/{DBID}.db";

            return new ConnectionInfo()
            {
                Type = ConnectionType.SQLite,
                Name = "MyConnection",
                ConnectionString = $"Data Source={DataSource};Version=3;",
                Persistent = true
            };
        }

        public ConnectionInfo GetInvalidConnectionInfo()
        {
            string DataSource = $"{Interface.uMod.DataDirectory}/{DBID}.db";

            return new ConnectionInfo()
            {
                Type = ConnectionType.SQLite,
                Name = "MyConnection",
                ConnectionString = $"Data Source={DataSource};Version=3;FailIfMissing=true",
                Persistent = true
            };
        }

        [TestMethod]
        public void CreateTable()
        {
            bool responseTriggered = false;
            bool closedConnection = false;

            ConnectionState state = ConnectionState.Closed;

            Database
                .Open(GetValidConnectionInfo())
                .Done(connection =>
                {
                    state = connection.State;
                    connection
                       .Execute<int>("CREATE TABLE IF NOT EXISTS \"test_table\" (\"Number\" INTEGER, \"String\" TEXT, \"Key\" INTEGER PRIMARY KEY AUTOINCREMENT);")
                       .Done(result =>
                       {
                           if (result == 0)
                           {
                               responseTriggered = true;
                           }
                       });

                    connection.Close().Done(c =>
                    {
                        closedConnection = true;
                    });
                });

            Assert.Await.IsTrue(() => state == ConnectionState.Open, 100, 200);
            Assert.Await.IsTrue(() => responseTriggered, 100, 200);
            Assert.Await.IsTrue(() => closedConnection, 100, 200);
        }

        [TestMethod]
        public void ConnectionFailure()
        {
            bool responseTriggered = false;

            ConnectionState state = ConnectionState.Closed;

            Database
                .Open(GetInvalidConnectionInfo())
                .Fail(delegate
                {
                    responseTriggered = true;
                    return null;
                })
                .Done(connection =>
                {
                    if (connection != null)
                    {
                        state = connection.State;
                    }
                });

            Assert.Await.IsTrue(() => state == ConnectionState.Closed, 100, 200);
            Assert.Await.IsTrue(() => responseTriggered, 100, 200);
        }

        [TestMethod]
        public void RunTransaction()
        {
            bool responseTriggered = false;
            bool closedConnection = false;

            ConnectionState state = ConnectionState.Closed;

            Database
                .Open(GetValidConnectionInfo())
                .Done(connection =>
                {
                    state = connection.State;
                    connection
                        .Transaction()
                        .Execute("CREATE TABLE IF NOT EXISTS \"test_table\" (\"Number\" INTEGER, \"String\" TEXT, \"Key\" INTEGER PRIMARY KEY AUTOINCREMENT);")
                        .Execute("DROP TABLE \"test_table\"")
                        .Commit()
                        .Fail(exception =>
                        {
                            Assert.Fail(exception.Message);
                        })
                        .Done(() =>
                        {
                            responseTriggered = true;
                            connection.Close().Done(c =>
                            {
                                closedConnection = true;
                            });
                        });
                });

            Assert.Await.IsTrue(() => state == ConnectionState.Open, 100, 200);
            Assert.Await.IsTrue(() => responseTriggered, 100, 200);
            Assert.Await.IsTrue(() => closedConnection, 100, 200);
        }

        [TestMethod]
        public void FailQuery()
        {
            bool failTriggered = false;
            string errorMessage = string.Empty;
            bool closedConnection = false;

            ConnectionState state = ConnectionState.Closed;
            Database
                .Open(GetValidConnectionInfo())
                .Done(connection =>
                {
                    state = connection.State;
                    connection
                       .Execute("NOT_A_QUERY")
                       .Fail(exception =>
                       {
                           errorMessage = exception.Message;
                           failTriggered = true;
                       });

                    connection.Close().Done(c =>
                    {
                        closedConnection = true;
                    });
                });

            Assert.Await.IsTrue(() => state == ConnectionState.Open, 100, 200);
            Assert.Await.IsTrue(() => failTriggered, 100, 200);
            Assert.Await.IsTrue(() => errorMessage.StartsWith("SQL logic error"));
            Assert.Await.IsTrue(() => closedConnection, 100, 200);
        }

        [Migration("umod_test")]
        public class TestMigration : Migration
        {
            public TestMigration(Connection connection) : base(connection)
            {
            }

            protected override void Down()
            {
                Execute("DROP TABLE umod_test");
            }

            protected override void Up()
            {
                Execute(@"CREATE TABLE IF NOT EXISTS ""umod_test"" (
                    ""text"" TEXT
                );");
            }
        }

        [TestMethod]
        public void RunMigration()
        {
            Cleanup();
            ConnectionState state = ConnectionState.Closed;
            bool closedConnection = false;
            bool insert = false;

            Database
                .Open(GetValidConnectionInfo())
                .Done(connection =>
                {
                    state = connection.State;
                    Assert.AreEqual(ConnectionState.Open, connection.State);

                    connection.Migrate<TestMigration>(MigrationDirection.Up)
                    .Then(delegate ()
                    {
                        connection.Execute("INSERT INTO umod_test (text) VALUES ('hello world')").Done(delegate ()
                        {
                            insert = true;
                        });
                    })
                    .Done(delegate ()
                    {
                        connection.Close().Done(c =>
                        {
                            closedConnection = true;
                        });
                    });
                });

            Assert.Await.IsTrue(() => state == ConnectionState.Open, 100, 200);
            Assert.Await.IsTrue(() => insert, 100, 200);
            Assert.Await.IsTrue(() => closedConnection, 100, 200);
        }

        [TestMethod]
        [Plugin(typeof(MockDatabasePlugin))]
        public void PluginWhenLoadedShouldConnectAndRunMigration()
        {
            MockDatabasePlugin plugin = GetPlugin<MockDatabasePlugin>();

            Assert.Await.IsLogged("MockDatabasePlugin.OnDatabaseSetup.Called");
        }

        [TestMethod]
        [Plugin(typeof(MockDatabasePlugin))]
        public void ModelShouldMapOneToOneRelationship()
        {
            MockDatabasePlugin plugin = GetPlugin<MockDatabasePlugin>();

            Assert.Await.IsLogged("MockDatabasePlugin.OnDatabaseSetup.Called");

            plugin.TestOneToOne();

            Assert.Await.IsLogged("MockDatabasePlugin.MockDataOneToOne.Mapped");
        }

        [TestMethod]
        [Plugin(typeof(MockDatabasePlugin))]
        public void ModelShouldMapOneToManyRelationship()
        {
            MockDatabasePlugin plugin = GetPlugin<MockDatabasePlugin>();

            Assert.Await.IsLogged("MockDatabasePlugin.OnDatabaseSetup.Called");

            plugin.TestOneToMany();

            Assert.Await.IsLogged("MockDatabasePlugin.MockDataOneToMany.Mapped");
        }
    }
}
