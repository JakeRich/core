﻿extern alias References;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.IO.Schema;
using uMod.Mock;
using uMod.Plugins;
using Timer = uMod.Libraries.Timer;

namespace uMod.Tests
{
    [TestClass]
    public class LocaleTest : BaseTest
    {
        protected Libraries.Timer TimerLibrary => Interface.uMod.Libraries.Get<Libraries.Timer>();

        private Libraries.Locale LocaleLibrary => Interface.uMod.Libraries.Get<Libraries.Locale>();

        private Libraries.Lang LangLibrary => Interface.uMod.Libraries.Get<Libraries.Lang>();

        [Localization]
        public interface IMockLocale : ILocale
        {
            string Hello { get; }
        }

        [Locale]
        public class MockDefaultLocale : IMockLocale
        {
            public string Hello { get; } = "Hello";
        }

        [Locale("fr")]
        public class MockFRLocale : IMockLocale
        {
            public string Hello { get; } = "Bonjour";
        }

        [TestMethod]
        [Plugin(typeof(MockLocalePlugin))]
        public void PlayerLocaleTest()
        {
            MockLocalePlugin plugin = GetPlugin<MockLocalePlugin>();
            ClearLog();
            MockPlayer player = new MockPlayer(new GamePlayer(new GamePlayerIdentity()).Identity);

            Assert.Await.IsTrue(() => plugin.IsResolved, 100, 50);

            LangLibrary.SetLanguage("fr", player.Id);
            plugin.CallHook("OnPlayerConnected", player);

            Assert.Await.IsLogged("ReplyTest:Bonjour Mock Server");
            Assert.Await.IsLogged("OtherReplyTest:Monde");

            LangLibrary.SetLanguage("en", player.Id);
            plugin.CallHook("OnPlayerConnected", player);

            Assert.Await.IsLogged("ReplyTest:Hello Mock Server");
            Assert.Await.IsLogged("OtherReplyTest:World");
            RestoreLog();
            ClearLog();

            // Language does not exist, default to server language
            LangLibrary.SetLanguage("it", player.Id);
            plugin.CallHook("OnPlayerConnected", player);

            Assert.Await.IsLogged("ReplyTest:Hello Mock Server");
            RestoreLog();
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void DefaultGenericLocaleBehavior()
        {
            MockPlugin plugin = GetPlugin<MockPlugin>();

            LangLibrary.SetServerLanguage("en");

            LocaleLibrary.RegisterLocale(new MockDefaultLocale(), plugin);
            LocaleLibrary.RegisterLocale(new MockFRLocale(), plugin);

            MockDefaultLocale defaultLocale = LocaleLibrary.GetLocale<MockDefaultLocale>(plugin);
            Assert.AreEqual("Hello", defaultLocale.Hello);

            MockFRLocale frLocale = LocaleLibrary.GetLocale<MockFRLocale>(plugin);
            Assert.AreEqual("Bonjour", frLocale.Hello);

            IMockLocale defaultLocaleInterface = LocaleLibrary.GetLocale<IMockLocale>(plugin);
            Assert.AreEqual("Hello", defaultLocaleInterface.Hello);

            IMockLocale ENInterface = LocaleLibrary.GetLocale<IMockLocale>("en", plugin);
            Assert.AreEqual("Hello", ENInterface.Hello);

            IMockLocale FRInterface = LocaleLibrary.GetLocale<IMockLocale>("fr", plugin);
            Assert.AreEqual("Bonjour", FRInterface.Hello);

            IMockLocale gracefulDegradedInterface = LocaleLibrary.GetLocale<IMockLocale>("ru", plugin);
            Assert.AreEqual("Hello", gracefulDegradedInterface.Hello);
        }

        [TestMethod]
        [Plugin(typeof(MockPlugin))]
        public void OverideGenericLocaleBehavior()
        {
            MockPlugin plugin = GetPlugin<MockPlugin>();

            LangLibrary.SetServerLanguage("fr");

            LocaleLibrary.RegisterLocale(new MockDefaultLocale(), plugin);
            LocaleLibrary.RegisterLocale(new MockFRLocale(), plugin);

            MockDefaultLocale defaultLocale = LocaleLibrary.GetLocale<MockDefaultLocale>(plugin);
            Assert.AreEqual("Hello", defaultLocale.Hello);

            MockFRLocale frLocale = LocaleLibrary.GetLocale<MockFRLocale>(plugin);
            Assert.AreEqual("Bonjour", frLocale.Hello);

            IMockLocale defaultLocaleInterface = LocaleLibrary.GetLocale<IMockLocale>(plugin);
            Assert.AreEqual("Bonjour", defaultLocaleInterface.Hello);

            IMockLocale ENInterface = LocaleLibrary.GetLocale<IMockLocale>("en", plugin);
            Assert.AreEqual("Hello", ENInterface.Hello);

            IMockLocale FRInterface = LocaleLibrary.GetLocale<IMockLocale>("fr", plugin);
            Assert.AreEqual("Bonjour", FRInterface.Hello);

            IMockLocale gracefulDegradedInterface = LocaleLibrary.GetLocale<IMockLocale>("ru", plugin);
            Assert.AreEqual("Bonjour", gracefulDegradedInterface.Hello);
        }

        [TestMethod]
        [Plugin(typeof(MockLocalePlugin))]
        public void PluginAutoLocaleUpgrade()
        {
            File.Delete(Path.Combine(Interface.uMod.LangDirectory, "en", "MockLocalePlugin.json"));
            File.Delete(Path.Combine(Interface.uMod.LangDirectory, "fr", "MockLocalePlugin.json"));
            File.Delete(Path.Combine(Interface.uMod.LangDirectory, "ru", "MockLocalePlugin.json"));
            MockLocalePlugin mockPlugin = GetPlugin<MockLocalePlugin>();

            Assert.Await.IsTrue(() => mockPlugin?.IsLoaded == true);

            int callbacks = 0;
            Timer.TimerInstance timer = TimerLibrary.Once(0, delegate ()
            {
                File.Delete(Path.Combine(Interface.uMod.LangDirectory, "en", "MockLocalePlugin.json"));
                File.Delete(Path.Combine(Interface.uMod.LangDirectory, "fr", "MockLocalePlugin.json"));
                File.Delete(Path.Combine(Interface.uMod.LangDirectory, "ru", "MockLocalePlugin.json"));

                MockLocalePlugin.OldLocale enOldLocaleStuff = new MockLocalePlugin.OldLocale()
                {
                    Hello = "Hello {server}",
                    Help = "Help"
                };

                MockLocalePlugin.OldLocale ruOldLocaleStuff = new MockLocalePlugin.OldLocale()
                {
                    Hello = "Привет {server}",
                    Help = "Помогите"
                };

                MockLocalePlugin.OldLocale frOldLocaleStuff = new MockLocalePlugin.OldLocale()
                {
                    Hello = "Bonjour {server}",
                    Help = "Aidez-moi"
                };

                File.WriteAllText(Path.Combine(Interface.uMod.LangDirectory, "en", "MockLocalePlugin.json"), JsonConvert.SerializeObject(enOldLocaleStuff));
                File.WriteAllText(Path.Combine(Interface.uMod.LangDirectory, "ru", "MockLocalePlugin.json"), JsonConvert.SerializeObject(ruOldLocaleStuff));
                File.WriteAllText(Path.Combine(Interface.uMod.LangDirectory, "fr", "MockLocalePlugin.json"), JsonConvert.SerializeObject(frOldLocaleStuff));
                VersionStore.Instance.Update(Path.Combine("lang", "en", "MockLocalePlugin"), new VersionNumber(1, 0, 0));

                mockPlugin.HandleAddedToManager(null);
                callbacks++;
            });

            Assert.Timer.IsCompleted(timer);
            Assert.Await.IsTrue(() => callbacks == 1, 150, 75);
            Assert.Await.IsLogged("MockLocalePlugin.OnLocaleUpgrade.OldLocaleToMyLocale");
        }
    }
}
