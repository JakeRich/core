﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Pooling;

namespace uMod.Tests
{
    [TestClass]
    public class PoolTest : BaseTest
    {
        [TestMethod]
        public void PoolBenchmark()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            ListPool<List<string>> myPool = new ListPool<List<string>>();
            for (int i = 0; i < 10; i++)
            {
                List<string> myList = myPool.Get<List<string>>();
                myList.Add("hello world");
                Assert.AreEqual(myList.Count, 1);
                myPool.Free(myList);
            }
            stopwatch.Stop();

            Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds);

            System.Threading.Thread.Sleep(100);

            //Assert.AreEqual(myPool.Size, 1);
        }
    }
}
