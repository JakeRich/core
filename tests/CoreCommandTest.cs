﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using uMod.Common;
using uMod.Libraries;
using uMod.Mock;
using uMod.Plugins;

namespace uMod.Tests
{
    [TestClass]
    [Plugin(typeof(MockPlugin))]
    [Application("Compiler")]
    [Compile("tests/umod/plugins/BlankPlugin.cs")]
    public class CoreCommandTest : BaseTest
    {
        private const string SteamID = "76561197960265729";
        private Permission PermissionLibrary => CurrentScope.Module.Libraries.Get<Permission>();

        private void SetupDefaultCommands()
        {
            // Add universal commands
            Commands commands = new Commands();
            commands.Initialize(GetPlugin<MockPlugin>(), (player, args) =>
            {
                if (player.IsServer)
                {
                    player.Reply($"Test Version: {CurrentScope.Module.Server.Version}");
                    return CommandState.Completed;
                }

                return CommandState.Unrecognized;
            });
        }

        private void Setup()
        {
            SetupDefaultCommands();
            Assert.Await.IsTrue(() => Utility.Plugins.Exists("BlankPlugin"), 50, 100);
            ClearLog();
        }

        [DataTestMethod]
        [DataRow("BlankPlugin")]
        [DataRow("*")]
        public void ConsoleReload(string arg)
        {
            Setup();
            CurrentScope.Module.Server.Command($"u.reload {arg}");
            Assert.Await.IsFalse(() => Utility.Plugins.Exists("BlankPlugin"), 50, 75);
            
            Assert.Await.IsLogged("Unloaded plugin Blank Plugin");
            Assert.Await.IsTrue(() => Utility.Plugins.Exists("BlankPlugin"), 50, 75);
        }

        [DataTestMethod]
        [DataRow("BlankPlugin")]
        [DataRow("*")]
        public void ConsoleUnload(string arg)
        {
            Setup();
            CurrentScope.Module.Server.Command($"u.unload {arg}");
            Assert.Await.IsLogged("Unloaded plugin Blank Plugin");
            Assert.IsFalse(Utility.Plugins.Exists("BlankPlugin"));
        }

        [DataTestMethod]
        [DataRow("BlankPlugin")]
        [DataRow("*")]
        public void ConsoleLoad(string arg)
        {
            Setup();
            CurrentScope.Module.Server.Command($"u.unload {arg}");
            Assert.Await.IsFalse(() => Utility.Plugins.Exists("BlankPlugin"), 50, 75);

            CurrentScope.Module.Server.Command($"u.load {arg}");
            Assert.Await.IsTrue(() => Utility.Plugins.Exists("BlankPlugin"), 50, 75);
        }

        [TestMethod]
        public void ConsolePlugins()
        {
            Setup();
            CurrentScope.Module.Server.Command("u.plugins");
            Assert.Await.IsLogged("Listing 1 plugin");
            Assert.Await.IsLogged("01 \"Blank Plugin\" (1.0.0) by uMod (0.00s) - BlankPlugin.cs");
        }

        [TestMethod]
        public void ConsoleGrantGroup()
        {
            Setup();
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", GetPlugin<MockPlugin>());
            CurrentScope.Module.Server.Command("u.grant group admin mockplugin.test");
            Assert.IsTrue(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test"));
        }

        [TestMethod]
        public void ConsoleGrantPlayer()
        {
            Setup();
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            PermissionLibrary.RegisterPermission("mockplugin.test", GetPlugin<MockPlugin>());

            CurrentScope.Module.Server.Command($"u.grant player {SteamID} mockplugin.test");
            string[] UserPermissions = PermissionLibrary.GetUserPermissions(SteamID);

            Assert.AreEqual(1, UserPermissions.Length);

            Assert.IsTrue(UserPermissions.Contains("mockplugin.test"));
        }

        [TestMethod]
        public void ConsoleRevokeGroup()
        {
            ConsoleGrantGroup();
            CurrentScope.Module.Server.Command("u.revoke group admin mockplugin.test");
            Assert.IsFalse(PermissionLibrary.GroupHasPermission("admin", "mockplugin.test"));
        }

        [TestMethod]
        public void ConsoleRevokePlayer()
        {
            ConsoleGrantPlayer();
            CurrentScope.Module.Server.Command($"u.revoke player {SteamID} mockplugin.test");
            string[] UserPermissions = PermissionLibrary.GetUserPermissions(SteamID);
            Assert.AreEqual(0, UserPermissions.Length);
            Assert.IsFalse(UserPermissions.Contains("mockplugin.test"));
        }

        [TestMethod]
        public void ConsoleGroupAdd()
        {
            Setup();
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            CurrentScope.Module.Server.Command("u.group add admin");

            Assert.IsTrue(PermissionLibrary.GroupExists("admin"));
        }

        [TestMethod]
        public void ConsoleGroupRemove()
        {
            ConsoleGroupAdd();
            CurrentScope.Module.Server.Command("u.group remove admin");

            Assert.IsFalse(PermissionLibrary.GroupExists("admin"));
        }

        [TestMethod]
        public void ConsoleGroupSet()
        {
            ConsoleGroupAdd();
            CurrentScope.Module.Server.Command("u.group set admin \"Title\" 1");

            Assert.AreEqual("Title", PermissionLibrary.GetGroupTitle("admin"));
            Assert.AreEqual(1, PermissionLibrary.GetGroupRank("admin"));
        }

        [TestMethod]
        public void ConsoleGroupParent()
        {
            ConsoleGroupAdd();
            CurrentScope.Module.Server.Command("u.group add subadmin");
            CurrentScope.Module.Server.Command("u.group parent subadmin admin");

            Assert.AreEqual("admin", PermissionLibrary.GetGroupParent("subadmin"));
        }

        [TestMethod]
        public void ConsoleShowPerms()
        {
            ConsoleGrantPlayer();
            CurrentScope.Module.Server.Command("u.show perms");

            Assert.Await.IsLogged(string.Join(", ", PermissionLibrary.GetPermissions()));
        }

        [TestMethod]
        public void ConsoleShowPerm()
        {
            ConsoleGrantGroup();
            CurrentScope.Module.Server.Command("u.show perm mockplugin.test");
            Assert.Await.IsLogged("Permission 'mockplugin.test' Groups:\nadmin");
        }

        [TestMethod]
        public void ConsoleShowPlayer()
        {
            ConsoleGrantPlayer();
            CurrentScope.Module.Server.Command($"u.show player {SteamID}");
            Assert.Await.IsLogged("Player '76561197960265729' permissions:\nmockplugin.test");
        }

        [TestMethod]
        public void ConsoleShowGroup()
        {
            ConsoleGrantGroup();
            CurrentScope.Module.Server.Command("u.show group admin");
            Assert.Await.IsLogged("Group 'admin' permissions:\nmockplugin.test");
        }

        [TestMethod]
        public void ConsoleShowGroups()
        {
            ConsoleGroupAdd();
            CurrentScope.Module.Server.Command("u.show groups");
            Assert.Await.IsLogged(string.Join(", ", PermissionLibrary.GetGroups()));
        }

        [TestMethod]
        public void ConsoleUserGroupAdd()
        {
            Setup();
            if (PermissionLibrary.GroupExists("admin"))
            {
                PermissionLibrary.RemoveGroup("admin");
            }

            PermissionLibrary.CreateGroup("admin", "Administrator", 1);

            CurrentScope.Module.Server.Command($"u.usergroup add {SteamID} admin");
            Assert.Await.IsLogged("Player '76561197960265729' added to group 'admin'");
        }

        [TestMethod]
        public void ConsoleUserGroupRemove()
        {
            ConsoleUserGroupAdd();

            CurrentScope.Module.Server.Command($"u.usergroup remove {SteamID} admin");
            Assert.Await.IsLogged("Player '76561197960265729' removed from group 'admin'");
        }
    }
}
