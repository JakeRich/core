using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Database;
using uMod.Pooling;

namespace uMod.Auth.Managers
{
    internal class DatabaseAuthManager : BaseAuthProvider, IAuthManager
    {
        public bool IsLoaded { get; private set; }

        public IEvent<Player> PlayerCreated { get; } = new Event<Player>();

        public IEvent<Player> PlayerChanged { get; } = new Event<Player>();

        public IEvent<PlayerGroup> PlayerGroupGranted { get; } = new Event<PlayerGroup>();

        public IEvent<PlayerGroup> PlayerGroupRevoked { get; } = new Event<PlayerGroup>();

        public IEvent<PlayerGroup[]> PlayerGroupsGranted { get; } = new Event<PlayerGroup[]>();

        public IEvent<PlayerGroup[]> PlayerGroupsRevoked { get; } = new Event<PlayerGroup[]>();

        public IEvent<PlayerPermission> PlayerPermissionGranted { get; } = new Event<PlayerPermission>();

        public IEvent<PlayerPermission> PlayerPermissionRevoked { get; } = new Event<PlayerPermission>();

        public IEvent<PlayerPermission[]> PlayerPermissionsGranted { get; } = new Event<PlayerPermission[]>();

        public IEvent<PlayerPermission[]> PlayerPermissionsRevoked { get; } = new Event<PlayerPermission[]>();

        public IEvent<Group> GroupCreated { get; } = new Event<Group>();

        public IEvent<Group> GroupRemoved { get; } = new Event<Group>();

        public IEvent<Group> GroupChanged { get; } = new Event<Group>();

        public IEvent<GroupPermission> GroupPermissionGranted { get; } = new Event<GroupPermission>();

        public IEvent<GroupPermission> GroupPermissionRevoked { get; } = new Event<GroupPermission>();

        private Dictionary<string, Player> _userdata = new Dictionary<string, Player>();
        private Dictionary<string, Group> _groupdata = new Dictionary<string, Group>();

        private Connection _connection;
        internal Transaction PlayerTransaction;
        internal Transaction GroupTransaction;

        public DatabaseAuthManager(Provider auth, ILogger logger) : base(auth, logger)
        {
            Register();
        }

        #region Event Handling

        private void Register()
        {
            GroupTransaction = new Transaction(Interface.uMod.Database, null, "auth_groups");
            PlayerTransaction = new Transaction(Interface.uMod.Database, null, "auth_players");

            PlayerCreated.Add(PlayerCreatedCallback);
            PlayerChanged.Add(PlayerChangedCallback);
            PlayerGroupGranted.Add(PlayerGroupGrantedCallback);
            PlayerGroupRevoked.Add(PlayerGroupRevokedCallback);
            PlayerGroupsGranted.Add(PlayerGroupsGrantedCallback);
            PlayerGroupsRevoked.Add(PlayerGroupsRevokedCallback);
            PlayerPermissionGranted.Add(PlayerPermissionGrantedCallback);
            PlayerPermissionRevoked.Add(PlayerPermissionRevokedCallback);
            PlayerPermissionsGranted.Add(PlayerPermissionsGrantedCallback);
            PlayerPermissionsRevoked.Add(PlayerPermissionsRevokedCallback);

            GroupCreated.Add(GroupCreatedCallback);
            GroupRemoved.Add(GroupRemovedCallback);
            GroupChanged.Add(GroupChangedCallback);
            GroupPermissionGranted.Add(GroupPermissionGrantedCallback);
            GroupPermissionRevoked.Add(GroupPermissionRevokedCallback);
        }

        private void GroupCreatedCallback(Group group)
        {
            GroupTransaction.Execute("INSERT INTO umod_groups (Name, Title, Rank, ParentGroup) VALUES (@Name, @Title, @Rank, @ParentGroup);", group);
        }

        private void GroupRemovedCallback(Group group)
        {
            GroupTransaction.Execute("DELETE FROM umod_groups WHERE Name = @Name;", group);
        }

        private void GroupChangedCallback(Group group)
        {
            GroupTransaction.Execute("UPDATE umod_groups SET Title = @Title, Rank = @Rank, ParentGroup = @ParentGroup WHERE Name = @Name;", group);
        }

        private void GroupPermissionGrantedCallback(GroupPermission groupPermission)
        {
            GroupTransaction.Execute("INSERT INTO umod_group_permissions (GroupName, Permission, ValidUntil) VALUES (@GroupName, @Permission, @ValidUntil);", groupPermission);
        }

        private void GroupPermissionRevokedCallback(GroupPermission groupPermission)
        {
            GroupTransaction.Execute("DELETE FROM umod_group_permissions WHERE GroupName = @GroupName AND Permission = @Permission;", groupPermission);
        }

        private void PlayerCreatedCallback(Player player)
        {
            PlayerTransaction.Execute("INSERT INTO umod_players (Id, Name, Language, Created, LastLogin) VALUES (@Id, @Name, @Language, @Created, @LastLogin);", player);
        }

        private void PlayerChangedCallback(Player player)
        {
            PlayerTransaction.Execute("UPDATE umod_players SET Name = @Name, Language = @Language, LastLogin = @LastLogin WHERE Id = @Id;", player);
        }

        private void PlayerGroupGrantedCallback(PlayerGroup playerGroup)
        {
            PlayerTransaction.Execute("INSERT INTO umod_player_groups (GroupName, PlayerId, ValidUntil) VALUES (@GroupName, @PlayerId, @ValidUntil);", playerGroup);
        }

        private void PlayerGroupRevokedCallback(PlayerGroup playerGroup)
        {
            PlayerTransaction.Execute("DELETE FROM umod_player_groups WHERE GroupName = @GroupName AND PlayerId = @PlayerId;", playerGroup);
        }

        private void PlayerGroupsGrantedCallback(PlayerGroup[] playerGroups)
        {
            PlayerTransaction.Execute("INSERT INTO umod_player_groups (GroupName, PlayerId, ValidUntil) VALUES (@GroupName, @PlayerId, @ValidUntil);", playerGroups);
        }

        private void PlayerGroupsRevokedCallback(PlayerGroup[] playerGroups)
        {
            PlayerTransaction.Execute("DELETE FROM umod_player_groups WHERE GroupName = @GroupName AND PlayerId = @PlayerId;", playerGroups);
        }

        private void PlayerPermissionGrantedCallback(PlayerPermission playerPermission)
        {
            PlayerTransaction.Execute("INSERT INTO umod_player_permissions (Permission, PlayerId, ValidUntil) VALUES (@Permission, @PlayerId, @ValidUntil);", playerPermission);
        }

        private void PlayerPermissionRevokedCallback(PlayerPermission playerPermission)
        {
            PlayerTransaction.Execute("DELETE FROM umod_player_permissions WHERE Permission = @Permission AND PlayerId = @PlayerId;", playerPermission);
        }

        private void PlayerPermissionsGrantedCallback(PlayerPermission[] playerPermission)
        {
            PlayerTransaction.Execute("INSERT INTO umod_player_permissions (Permission, PlayerId, ValidUntil) VALUES (@Permission, @PlayerId, @ValidUntil);", playerPermission);
        }

        private void PlayerPermissionsRevokedCallback(PlayerPermission[] playerPermission)
        {
            PlayerTransaction.Execute("DELETE FROM umod_player_permissions WHERE Permission = @Permission AND PlayerId = @PlayerId;", playerPermission);
        }

        #endregion Event Handling

        public void CleanUp()
        {
            if (IsLoaded && validate != null)
            {
                string[] invalid = _userdata?.Keys.Where(k => !validate(k)).ToArray();
                if (invalid?.Length > 0)
                {
                    foreach (string i in invalid)
                    {
                        _userdata.Remove(i);
                    }
                }
            }
        }

        #region Data Management

        public IPromise Load(AuthData authData = AuthData.All)
        {
            Promise promise = new Promise();
            bool usersLoaded = false;
            bool groupsLoaded = false;
            Interface.uMod.Database.Client.Open().Done(delegate (Connection connection)
            {
                _connection = connection;
                PlayerTransaction.Connection = connection;
                GroupTransaction.Connection = connection;
                connection.Query<List<Group>>(
                    "SELECT g.Name, g.Title, g.Rank, g.ParentGroup, gp.GroupName, gp.Permission, gp.ValidUntil FROM umod_groups g INNER JOIN umod_group_permissions gp ON g.Name = gp.GroupName;",
                    new[]
                    {
                        "Permissions"
                    },
                    "GroupName").Done(delegate (List<Group> groups)
                {
                    if (groups != null)
                    {
                        _groupdata = groups.ToDictionary(x => x.Name, x => x);
                    }
                    groupsLoaded = true;
                    if (usersLoaded)
                    {
                        promise.Resolve();
                        IsLoaded = true;
                    }
                }, promise.Reject);

                connection.Query<List<Player>>(
                    "SELECT u.Id, u.Name, u.Language, u.Created, u.LastLogin, ug.PlayerId, ug.GroupName, ug.ValidUntil, up.PlayerId, up.Permission, up.ValidUntil FROM umod_players u INNER JOIN umod_player_groups ug ON ug.PlayerId = u.Id INNER JOIN umod_player_permissions up ON up.PlayerId = u.Id;",
                    new[] {
                        "Groups",
                        "Permissions"
                    },
                    "PlayerId").Done(delegate (List<Player> players)
                {
                    if (players != null)
                    {
                        _userdata = players.ToDictionary(x => x.Id, x => x);
                    }
                    usersLoaded = true;
                    if (groupsLoaded)
                    {
                        promise.Resolve();
                        IsLoaded = true;
                    }
                }, promise.Reject);
            }, promise.Reject);

            return promise;
        }

        /// <summary>
        /// Save auth data to database
        /// </summary>
        /// <param name="authData"></param>
        /// <returns></returns>
        public IPromise Save(AuthData authData = AuthData.All)
        {
            switch (authData)
            {
                case AuthData.Groups:
                    return SaveGroups();

                case AuthData.Users:
                    return SaveUsers();

                case AuthData.All:
                default:
                    Promise promise = new Promise();

                    Save(AuthData.Groups).Done(delegate
                    {
                        Save(AuthData.Users).Done(promise.Resolve, promise.Reject);
                    }, promise.Reject);

                    return promise;
            }
        }

        /// <summary>
        /// Save group data to database
        /// </summary>
        /// <returns></returns>
        public IPromise SaveGroups()
        {
            if (IsLoaded)
            {
                return GroupTransaction?.Commit();
            }

            return null;
        }

        /// <summary>
        /// Save user data to database
        /// </summary>
        /// <returns></returns>
        public IPromise SaveUsers()
        {
            if (IsLoaded)
            {
                return PlayerTransaction?.Commit();
            }

            return null;
        }

        #endregion Data Management

        #region Server Methods

        /// <summary>
        /// Get server language
        /// </summary>
        /// <returns></returns>
        public string GetServerLanguage()
        {
            return Auth.Configuration.Language;
        }

        /// <summary>
        /// Set server language
        /// </summary>
        /// <param name="language"></param>
        public void SetServerLanguage(string language)
        {
            if (!string.IsNullOrEmpty(language) && !language.Equals(Auth.Configuration.Language))
            {
                Auth.Configuration.Language = language;
                Auth.Configuration.SaveFile();
            }
        }

        #endregion Server Methods

        #region Player Methods

        /// <summary>
        /// Gets a dictionary of player IDs and names
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, string> GetUsers()
        {
            return _userdata.ToDictionary(x => x.Key, x => x.Value.Name);
        }

        /// <summary>
        /// Returns if the specified player exists
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public bool UserExists(string playerId)
        {
            return _userdata.ContainsKey(playerId);
        }

        /// <summary>
        /// Updates the player name
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="playerName"></param>
        /// <param name="oldName"></param>
        /// <returns></returns>
        public bool UpdateName(string playerId, string playerName, out string oldName)
        {
            if (UserExists(playerId))
            {
                Player data = GetUserData(playerId);
                oldName = data.Name;
                data.Name = playerName;

                return true;
            }

            oldName = string.Empty;
            return false;
        }

        /// <summary>
        /// Returns the data for the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public Player GetUserData(string playerId)
        {
            if (!_userdata.TryGetValue(playerId, out Player data))
            {
                _userdata.Add(playerId, data = new Player()
                {
                    Id = playerId
                });

                PlayerCreated.Invoke(data);
            }

            // Return the data
            return data;
        }

        /// <summary>
        /// Gets player language
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string GetLanguage(string playerId)
        {
            if (string.IsNullOrEmpty(playerId) || !_userdata.TryGetValue(playerId, out Player player))
            {
                return Auth.Configuration.Language;
            }

            return player.Language;
        }

        /// <summary>
        /// Sets player language
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="language"></param>
        public void SetLanguage(string playerId, string language)
        {
            if (!string.IsNullOrEmpty(language) && !string.IsNullOrEmpty(playerId))
            {
                Player player = GetUserData(playerId);
                player.Language = language.ToLower();
            }
        }

        #region Querying

        /// <summary>
        /// Returns if the specified player has the specified permission
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool UserHasPermission(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Always allow the server console
            if (playerId.Equals("server_console"))
            {
                return true;
            }

            perm = perm.ToLower();

            // First, get the player data
            Player player = GetUserData(playerId);

            // Check if they have the perm
            if (player.Permissions != null && player.Permissions.TryGetValue(perm, out PlayerPermission permission) && (permission.ValidUntil == null || permission.ValidUntil > DateTime.Now))
            {
                return true;
            }

            if (player.Groups == null)
            {
                return false;
            }

            // Check if their group has the perm
            return GroupsHavePermission(player.Groups.Keys, perm);
        }

        /// <summary>
        /// Checks if player has a group
        /// </summary>
        /// <param name="playerId"></param>
        public bool UserHasAnyGroup(string playerId)
        {
            return UserExists(playerId) && GetUserData(playerId).Groups.Count > 0;
        }

        /// <summary>
        /// Gets if the player belongs to given group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool UserHasGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            Player data = GetUserData(playerId);
            name = name.ToLower();
            if (data.Groups.TryGetValue(name, out PlayerGroup playerGroup) && (playerGroup.ValidUntil == null || playerGroup.ValidUntil > DateTime.Now))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns if the specified player permission is inherited from a group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool IsUserPermissionInherited(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            perm = perm.ToLower();

            // First, get the player data
            Player data = GetUserData(playerId);

            // Check if they have the perm
            if (data.Permissions.ContainsKey(perm))
            {
                return false;
            }

            // Check if their group has the perm
            return GroupsHavePermission(data.Groups.Keys, perm);
        }

        /// <summary>
        /// Returns the group that a specified player permission is inherited from
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string GetUserPermissionGroup(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return null;
            }

            perm = perm.ToLower();

            // First, get the player data
            Player data = GetUserData(playerId);

            // Check if they have the perm
            if (data.Permissions != null && data.Permissions.ContainsKey(perm))
            {
                return null;
            }

            if (data.Groups != null)
            {
                foreach (string group in data.Groups.Keys)
                {
                    string permissionGroup = GetGroupPermissionGroup(group, perm);
                    if (!string.IsNullOrEmpty(permissionGroup))
                    {
                        return permissionGroup;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string[] GetUserGroups(string playerId)
        {
            return GetUserData(playerId).Groups.Keys.ToArray();
        }

        /// <summary>
        /// Returns the permissions which the specified player has
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string[] GetUserPermissions(string playerId)
        {
            Player data = GetUserData(playerId);
            List<string> perms = null;

            if (data.Permissions != null)
            {
                perms = data.Permissions.Keys.ToList();
            }

            if (data.Groups != null)
            {
                if (perms == null)
                {
                    perms = new List<string>();
                }

                foreach (string group in data.Groups.Keys)
                {
                    perms.AddRange(GetGroupPermissions(group));
                }
            }

            if (perms == null)
            {
                return StringPool.Empty;
            }

            return perms.Distinct().ToArray();
        }

        /// <summary>
        /// Returns the players with given permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string[] GetPermissionUsers(string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return StringPool.Empty;
            }

            perm = perm.ToLower();
            HashSet<string> users = new HashSet<string>();
            foreach (KeyValuePair<string, Player> data in _userdata)
            {
                if (data.Value.Permissions.ContainsKey(perm))
                {
                    users.Add($"{data.Key}({data.Value.Name})");
                }
            }

            return users.ToArray();
        }

        #endregion Querying

        #region Player Permissions

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool AddUserGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            return GetUserData(playerId).GrantGroup(this, name);
        }

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool RemoveUserGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            return GetUserData(playerId).RevokeGroup(this, name);
        }

        /// <summary>
        /// Grants the specified permission to the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantUserPermission(string playerId, string perm, IPlugin plugin = null)
        {
            // Check if it is even a permission
            if (!PermissionExists(perm, plugin))
            {
                return false;
            }

            // Add the permission
            return GetUserData(playerId).GrantPermission(this, perm, plugin);
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        public bool RevokeUserPermission(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Remove the permission
            return GetUserData(playerId).RevokePermission(this, perm);
        }

        #endregion Player Permissions

        #endregion Player Methods

        #region Group Methods

        /// <summary>
        /// Migrate permissions from one group to another
        /// </summary>
        /// <param name="newGroup"></param>
        /// <param name="oldGroup"></param>
        public void MigrateGroup(string oldGroup, string newGroup)
        {
            if (IsLoaded && GroupExists(oldGroup))
            {
                foreach (string perm in GetGroupPermissions(oldGroup))
                {
                    GrantGroupPermission(newGroup, perm);
                }

                if (GetUsersInGroup(oldGroup).Length == 0)
                {
                    RemoveGroup(oldGroup);
                }
            }
        }

        #region Querying

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool GroupsHavePermission(IEnumerable<string> groups, string perm)
        {
            return groups.Any(group => GroupHasPermission(group, perm));
        }

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool GroupHasPermission(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return false;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out Group group))
            {
                return false;
            }

            // Check if the group has the perm
            return group.Permissions?.ContainsKey(perm.ToLower()) ?? GroupHasPermission(group.ParentGroup, perm);
        }

        /// <summary>
        /// Checks if specified permission belongs to group or parent group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool IsGroupPermissionInherited(string name, string perm)
        {
            if (!GroupHasPermission(name, perm))
            {
                return false;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out Group group))
            {
                return false;
            }

            if (group.Permissions == null)
            {
                return true;
            }

            return !group.Permissions.ContainsKey(perm.ToLower());
        }

        /// <summary>
        /// Returns the parent group that the specified permission belongs to
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string GetGroupPermissionGroup(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return null;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out Group group))
            {
                return null;
            }

            if (group.Permissions.ContainsKey(perm.ToLower()))
            {
                return name;
            }

            return GetGroupPermissionGroup(group.ParentGroup, perm);
        }

        /// <summary>
        /// Returns the permissions which the specified group has, with optional transversing of parent groups
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parents"></param>
        /// <returns></returns>
        public string[] GetGroupPermissions(string name, bool parents = false)
        {
            if (!GroupExists(name))
            {
                return StringPool.Empty;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out Group group))
            {
                return StringPool.Empty;
            }

            if (group.Permissions == null)
            {
                return StringPool.Empty;
            }

            List<string> perms = group.Permissions.Keys.ToList();
            if (parents)
            {
                perms.AddRange(GetGroupPermissions(group.ParentGroup));
            }

            return perms.Distinct().ToArray();
        }

        /// <summary>
        /// Returns the groups with given permission
        /// </summary>
        /// <returns></returns>
        public string[] GetPermissionGroups(string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return StringPool.Empty;
            }

            perm = perm.ToLower();
            HashSet<string> groups = new HashSet<string>();
            foreach (KeyValuePair<string, Group> data in _groupdata)
            {
                if (data.Value.Permissions.ContainsKey(perm))
                {
                    groups.Add(data.Key);
                }
            }

            return groups.ToArray();
        }

        /// <summary>
        /// Returns if the specified group exists or not
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool GroupExists(string group)
        {
            return !string.IsNullOrEmpty(group) && (group.Equals("*") || _groupdata.ContainsKey(group.ToLower()));
        }

        /// <summary>
        /// Returns existing groups
        /// </summary>
        /// <returns></returns>
        public string[] GetGroups()
        {
            return _groupdata.Keys.ToArray();
        }

        /// <summary>
        /// Returns users in specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public string[] GetUsersInGroup(string group)
        {
            if (!GroupExists(group))
            {
                return StringPool.Empty;
            }

            return _userdata.Where(u => u.Value.Groups.ContainsKey(group.ToLower())).Select(u => $"{u.Key} ({u.Value.Name})").ToArray();
        }

        /// <summary>
        /// Gets the parent of the specified group
        /// </summary>
        /// <param name="group"></param>
        public string GetGroupParent(string group)
        {
            if (!GroupExists(group))
            {
                return string.Empty;
            }

            return !_groupdata.TryGetValue(group.ToLower(), out Group data) ? string.Empty : data.ParentGroup;
        }

        /// <summary>
        /// Returns the rank of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int GetGroupRank(string group)
        {
            if (!GroupExists(group))
            {
                return 0;
            }

            // First, get the group data
            if (!_groupdata.TryGetValue(group.ToLower(), out Group data))
            {
                return 0;
            }

            // Return the group rank
            return data.Rank;
        }

        /// <summary>
        /// Returns the title of the specified group
        /// </summary>
        /// <param name="group"></param>
        public string GetGroupTitle(string group)
        {
            if (!GroupExists(group))
            {
                return string.Empty;
            }

            // First, get the group data
            if (!_groupdata.TryGetValue(group.ToLower(), out Group data))
            {
                return string.Empty;
            }

            // Return the group title
            return data.Title;
        }

        #endregion Querying

        #region Group Permissions

        /// <summary>
        /// Grant the specified permission to the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantGroupPermission(string name, string perm, IPlugin plugin = null)
        {
            // Check if it is even a permission
            if (!PermissionExists(perm, plugin) || !GroupExists(name))
            {
                return false;
            }

            // Get the group data
            if (!_groupdata.TryGetValue(name.ToLower(), out Group data))
            {
                return false;
            }

            // Add the permission
            return data.GrantPermission(this, perm, plugin);
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        public bool RevokeGroupPermission(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Get the group data
            if (!_groupdata.TryGetValue(name.ToLower(), out Group data))
            {
                return false;
            }

            // Remove the permission
            return data.RevokePermission(this, perm);
        }

        #endregion Group Permissions

        #region Group Management

        /// <summary>
        /// Creates the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="rank"></param>
        /// <param name="parent"></param>
        public bool CreateGroup(string name, string title, int rank, string parent = "")
        {
            // Check if it already exists
            if (GroupExists(name) || string.IsNullOrEmpty(name))
            {
                return false;
            }

            // Create the data
            Group group = new Group { Name = name, Title = title, Rank = rank, ParentGroup = parent };

            // Add the group
            _groupdata.Add(name.ToLower(), group);

            GroupCreated.Invoke(group);

            return true;
        }

        /// <summary>
        /// Removes the specified group
        /// </summary>
        /// <param name="name"></param>
        public bool RemoveGroup(string name)
        {
            // Check if it even exists
            if (!GroupExists(name))
            {
                return false;
            }

            name = name.ToLower();

            bool removed = false;
            // Remove the group
            if (_groupdata.TryGetValue(name, out Group group))
            {
                removed = _groupdata.Remove(name);
                GroupRemoved.Invoke(group);
            }

            // Remove group from players
            bool changed = _userdata.Values.Aggregate(false, (current, userData) => current | (userData.Groups == null || userData.Groups.Remove(name)));
            if (changed)
            {
                SaveUsers();
            }

            return removed;
        }

        /// <summary>
        /// Sets the title of the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        public bool SetGroupTitle(string name, string title)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            name = name.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(name, out Group group))
            {
                return false;
            }

            // Change the title
            if (group.Title == title)
            {
                return true;
            }

            group.Title = title;
            GroupChanged.Invoke(group);

            return true;
        }

        /// <summary>
        /// Sets the rank of the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="rank"></param>
        public bool SetGroupRank(string name, int rank)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            name = name.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(name, out Group group))
            {
                return false;
            }

            // Change the rank
            if (group.Rank == rank)
            {
                return true;
            }

            group.Rank = rank;
            GroupChanged.Invoke(group);

            return true;
        }

        /// <summary>
        /// Sets the parent of the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parent"></param>
        public bool SetGroupParent(string name, string parent)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            name = name.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(name, out Group group))
            {
                return false;
            }

            if (string.IsNullOrEmpty(parent))
            {
                group.ParentGroup = null;
                return true;
            }

            if (!GroupExists(parent) || name.Equals(parent.ToLower()))
            {
                return false;
            }

            parent = parent.ToLower();

            if (!string.IsNullOrEmpty(group.ParentGroup) && group.ParentGroup.Equals(parent))
            {
                return true;
            }

            if (HasCircularParent(name, parent))
            {
                return false;
            }

            // Change the parent group
            group.ParentGroup = parent;
            GroupChanged.Invoke(group);

            return true;
        }

        /// <summary>
        /// Determine if group relationship is circular
        /// </summary>
        /// <param name="group"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private bool HasCircularParent(string group, string parent)
        {
            // Get parent data
            if (!_groupdata.TryGetValue(parent, out Group parentData))
            {
                return false;
            }

            // Check for circular reference
            HashSet<string> groups = new HashSet<string> { group, parent };
            while (!string.IsNullOrEmpty(parentData.ParentGroup))
            {
                // Found itself?
                if (!groups.Add(parentData.ParentGroup))
                {
                    return true;
                }

                // Get next parent
                if (!_groupdata.TryGetValue(parentData.ParentGroup, out parentData))
                {
                    return false;
                }
            }

            return false;
        }

        #endregion Group Management

        #endregion Group Methods
    }
}
