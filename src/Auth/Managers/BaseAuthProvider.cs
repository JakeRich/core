﻿using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Auth.Managers
{
    internal abstract class BaseAuthProvider
    {
        /// <summary>
        /// User id validation callback
        /// </summary>
        protected Func<string, bool> validate;

        /// <summary>
        /// User id validation callback
        /// </summary>
        public Func<string, bool> Validate { get => validate; set => validate = value; }

        /// <summary>
        /// All registered permissions
        /// </summary>
        protected readonly Dictionary<IPlugin, HashSet<string>> Permset;

        public IDictionary<IPlugin, HashSet<string>> LoadedPermissions => Permset;

        protected Provider Auth;

        protected readonly ILogger Logger;

        /// <summary>
        /// Create a new base auth provider object
        /// </summary>
        protected BaseAuthProvider(Provider auth, ILogger logger)
        {
            this.Logger = logger;
            Auth = auth;
            Permset = new Dictionary<IPlugin, HashSet<string>>();
        }

        #region Permission Management

        private void owner_OnRemovedFromManager(IPlugin sender, IPluginManager manager) => Permset.Remove(sender);

        /// <summary>
        /// Registers the specified permission
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        public bool RegisterPermission(string name, IPlugin owner)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            name = name.ToLower();

            if (PermissionExists(name))
            {
                Logger.Warning($"Duplicate permission registered '{name}' (by plugin '{owner.Title}')");
                return false;
            }

            string prefix = owner.Name.ToLower() + ".";
            if (!name.StartsWith(prefix) && !owner.IsCorePlugin)
            {
                Logger.Warning($"Missing plugin name prefix '{prefix}' for permission '{name}' (by plugin '{owner.Title}')");
                return false;
            }

            if (!Permset.TryGetValue(owner, out HashSet<string> set))
            {
                set = new HashSet<string>();
                Permset.Add(owner, set);
                owner.OnRemovedFromManager.Add(owner_OnRemovedFromManager);
            }

            return set.Add(name);
        }

        /// <summary>
        /// Unregisters the specified permission
        /// </summary>
        /// <param name="name"></param>
        /// <param name="plugin"></param>
        public bool UnregisterPermission(string name, IPlugin plugin)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            name = name.ToLower();

            if (Permset.TryGetValue(plugin, out HashSet<string> set) && set.Contains(name))
            {
                return set.Remove(name);
            }
            else
            {
                Logger.Warning($"Permission not registered and cannot be unregistered '{name}' (by plugin '{plugin.Title}')");
            }

            return false;
        }

        /// <summary>
        /// Returns if the specified permission exists or not
        /// </summary>
        /// <param name="name"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool PermissionExists(string name, IPlugin plugin = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            name = name.ToLower();

            if (plugin == null)
            {
                if (Permset.Count > 0)
                {
                    if (name.Equals("*"))
                    {
                        return true;
                    }

                    if (name.EndsWith("*"))
                    {
                        name = name.TrimEnd('*');
                        return Permset.Values.SelectMany(v => v).Any(p => p.StartsWith(name));
                    }
                }
                return Permset.Values.Any(v => v.Contains(name));
            }

            if (!Permset.TryGetValue(plugin, out HashSet<string> set))
            {
                return false;
            }

            if (set.Count > 0)
            {
                if (name.Equals("*"))
                {
                    return true;
                }

                if (name.EndsWith("*"))
                {
                    name = name.TrimEnd('*');
                    return set.Any(p => p.StartsWith(name));
                }
            }
            return set.Contains(name);
        }

        /// <summary>
        /// Returns the permissions which are registered
        /// </summary>
        /// <returns></returns>
        public string[] GetPermissions()
        {
            return new HashSet<string>(Permset.Values.SelectMany(v => v)).ToArray();
        }

        /// <summary>
        /// Returns the permissions which are registered
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public string[] GetPermissions(IPlugin plugin)
        {
            if (Permset.TryGetValue(plugin, out HashSet<string> perms))
            {
                return perms.ToArray();
            }

            return StringPool.Empty;
        }

        #endregion Permission Management
    }
}
