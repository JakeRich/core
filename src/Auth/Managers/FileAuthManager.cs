using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using uMod.Common;
using uMod.IO;

namespace uMod.Auth.Managers
{
    internal class FileAuthManager : BaseAuthProvider, IAuthManager
    {
        /// <summary>
        /// User data file
        /// </summary>
        private IDataFile<Dictionary<string, UserData>> _userDataFile;

        /// <summary>
        /// User data
        /// </summary>
        private Dictionary<string, UserData> _userdata;

        /// <summary>
        /// Group data file
        /// </summary>
        private IDataFile<Dictionary<string, GroupData>> _groupDataFile;

        /// <summary>
        /// Group data
        /// </summary>
        private Dictionary<string, GroupData> _groupdata;

        /// <summary>
        /// Lang data file
        /// </summary>
        private IDataFile<LangData> _langDataFile;

        /// <summary>
        /// Lang data
        /// </summary>
        internal static LangData LangData;

        /// <summary>
        /// Expiration data
        /// </summary>
        internal static ExpirationData ExpirationData;

        /// <summary>
        /// Determine if provider is loaded
        /// </summary>
        public bool IsLoaded => (_userDataFile?.IsLoaded ?? false) && (_groupDataFile?.IsLoaded ?? false);

        public IEvent<Player> PlayerCreated => throw new NotImplementedException();

        public IEvent<Player> PlayerChanged => throw new NotImplementedException();

        public IEvent<PlayerGroup> PlayerGroupGranted => throw new NotImplementedException();

        public IEvent<PlayerGroup> PlayerGroupRevoked => throw new NotImplementedException();

        public IEvent<PlayerPermission> PlayerPermissionGranted => throw new NotImplementedException();

        public IEvent<PlayerPermission> PlayerPermissionRevoked => throw new NotImplementedException();

        public IEvent<Group> GroupCreated => throw new NotImplementedException();

        public IEvent<Group> GroupRemoved => throw new NotImplementedException();

        public IEvent<Group> GroupChanged => throw new NotImplementedException();

        public IEvent<GroupPermission> GroupPermissionGranted => throw new NotImplementedException();

        public IEvent<GroupPermission> GroupPermissionRevoked => throw new NotImplementedException();

        public IEvent<PlayerGroup[]> PlayerGroupsGranted => throw new NotImplementedException();

        public IEvent<PlayerGroup[]> PlayerGroupsRevoked => throw new NotImplementedException();

        public IEvent<PlayerPermission[]> PlayerPermissionsGranted => throw new NotImplementedException();

        public IEvent<PlayerPermission[]> PlayerPermissionsRevoked => throw new NotImplementedException();

        public FileAuthManager(Provider provider, ILogger logger) : base(provider, logger)
        {
        }

        /// <summary>
        /// Removes players with invalid player IDs
        /// </summary>
        public void CleanUp()
        {
            if (!IsLoaded || validate == null)
            {
                return;
            }

            string[] invalid = _userdata?.Keys.Where(k => !validate(k)).ToArray();
            if (!(invalid?.Length > 0))
            {
                return;
            }

            foreach (string i in invalid)
            {
                _userdata.Remove(i);
            }
        }

        #region File Management

        /// <summary>
        /// Load auth data
        /// </summary>
        /// <param name="authData"></param>
        /// <returns></returns>
        public IPromise Load(AuthData authData = AuthData.All)
        {
            Promise promise = new Promise();
            switch (authData)
            {
                case AuthData.Groups:
                    LoadGroups().Done(promise.Resolve, promise.Reject);
                    break;

                case AuthData.Users:
                    LoadUsers().Done(delegate
                    {
                        LoadLang().Done(promise.Resolve, promise.Reject);
                    }, promise.Reject);
                    break;

                default:
                    Load(AuthData.Users).Done(delegate
                    {
                        Load(AuthData.Groups).Done(promise.Resolve, promise.Reject);
                    }, promise.Reject);
                    break;
            }

            return promise;
        }

        /// <summary>
        /// Load player data
        /// </summary>
        /// <returns></returns>
        private IPromise LoadUsers()
        {
            Promise promise = new Promise();
            _userDataFile = DataSystem.MakeDataFile<Dictionary<string, UserData>>(
                Path.Combine(Interface.uMod.DataDirectory, "umod.users"),
                Auth.Configuration.Driver.ToDataFormat()
            );

            if (_userDataFile.Exists)
            {
                _userDataFile.LoadAsync().Done(delegate (Dictionary<string, UserData> data)
                {
                    _userdata = data;
                    promise.Resolve();
                }, promise.Reject);
            }
            else
            {
                _userdata = _userDataFile.Object = new Dictionary<string, UserData>();
                _userDataFile.SaveAsync().Done(promise.Resolve, promise.Reject);
            }

            return promise;
        }

        private IPromise LoadLang()
        {
            Promise promise = new Promise();
            _langDataFile = DataSystem.MakeDataFile<LangData>(
                Path.Combine(Interface.uMod.DataDirectory, "umod.lang"),
                Auth.Configuration.Driver.ToDataFormat()
            );

            if (_langDataFile.Exists)
            {
                _langDataFile.LoadAsync().Done(delegate (LangData data)
                {
                    LangData = data;
                    promise.Resolve();
                }, promise.Reject);
            }
            else
            {
                LangData = _langDataFile.Object = new LangData();
                _langDataFile.SaveAsync().Done(promise.Resolve, promise.Reject);
            }

            return promise;
        }

        /// <summary>
        /// Load groups
        /// </summary>
        /// <returns></returns>
        private IPromise LoadGroups()
        {
            Promise promise = new Promise();
            _groupDataFile = DataSystem.MakeDataFile<Dictionary<string, GroupData>>(
                Path.Combine(Interface.uMod.DataDirectory, "umod.groups"),
                Auth.Configuration.Driver.ToDataFormat()
            );

            if (_groupDataFile.Exists)
            {
                _groupDataFile.LoadAsync().Done(delegate (Dictionary<string, GroupData> data)
                {
                    _groupdata = data;
                    promise.Resolve();
                }, promise.Reject);
            }
            else
            {
                _groupdata = _groupDataFile.Object = new Dictionary<string, GroupData>();
                _groupDataFile.SaveAsync().Done(promise.Resolve, promise.Reject);
            }

            return promise;
        }

        /// <summary>
        /// Save auth data
        /// </summary>
        /// <param name="authData"></param>
        /// <returns></returns>
        public IPromise Save(AuthData authData = AuthData.All)
        {
            Promise promise = new Promise();
            switch (authData)
            {
                case AuthData.Groups:
                    SaveGroups().Done(promise.Resolve, promise.Reject);
                    break;

                case AuthData.Users:
                    SaveUsers().Done(delegate
                    {
                        SaveLang().Done(promise.Resolve, promise.Reject);
                    }, promise.Reject);
                    break;

                default:
                    Save(AuthData.Users).Done(delegate
                    {
                        Save(AuthData.Groups).Done(promise.Resolve, promise.Reject);
                    }, promise.Reject);
                    break;
            }
            return promise;
        }

        /// <summary>
        /// Save users
        /// </summary>
        /// <returns></returns>
        private IPromise SaveUsers()
        {
            if (_userDataFile?.IsLoaded ?? false)
            {
                Promise promise = new Promise();
                _userDataFile?.SaveAsync().Then(promise);
                return promise;
            }

            return null;
        }

        private IPromise SaveLang()
        {
            if (_langDataFile?.IsLoaded ?? false)
            {
                Promise promise = new Promise();
                _langDataFile?.SaveAsync().Then(promise);
                return promise;
            }

            return null;
        }

        /// <summary>
        /// Save groups
        /// </summary>
        /// <returns></returns>
        private IPromise SaveGroups()
        {
            if (_groupDataFile?.IsLoaded ?? false)
            {
                Promise promise = new Promise();
                _groupDataFile?.SaveAsync().Then(promise);
                return promise;
            }

            return null;
        }

        #endregion File Management

        #region Server Methods

        /// <summary>
        /// Gets server language
        /// </summary>
        /// <returns></returns>
        public string GetServerLanguage()
        {
            return LangData.Lang;
        }

        /// <summary>
        /// Sets server language
        /// </summary>
        /// <param name="language"></param>
        public void SetServerLanguage(string language)
        {
            if (!string.IsNullOrEmpty(language) && !language.Equals(LangData.Lang))
            {
                LangData.Lang = language;
                SaveLang();
            }
        }

        #endregion Server Methods

        #region User Methods

        /// <summary>
        /// Gets a dictionary of player IDs and names
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, string> GetUsers()
        {
            return _userdata.ToDictionary(x => x.Key, x => x.Value.LastSeenNickname);
        }

        /// <summary>
        /// Gets player language
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string GetLanguage(string playerId)
        {
            if (string.IsNullOrEmpty(playerId) || !LangData.UserData.TryGetValue(playerId, out string lang))
            {
                return LangData.Lang;
            }

            return lang;
        }

        /// <summary>
        /// Sets player language
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="language"></param>
        public void SetLanguage(string playerId, string language)
        {
            if (!string.IsNullOrEmpty(language) && !string.IsNullOrEmpty(playerId))
            {
                if (!LangData.UserData.TryGetValue(playerId, out string currentLang) || !language.Equals(currentLang))
                {
                    LangData.UserData[playerId] = language;
                    SaveLang();
                }
            }
        }

        /// <summary>
        /// Returns if the specified player exists
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public bool UserExists(string playerId)
        {
            return _userdata.ContainsKey(playerId);
        }

        /// <summary>
        /// Updates the player name
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <param name="oldName"></param>
        /// <returns></returns>
        public bool UpdateName(string playerId, string name, out string oldName)
        {
            if (UserExists(playerId))
            {
                UserData data = GetUserData(playerId);
                oldName = data.LastSeenNickname;
                data.LastSeenNickname = name;

                return true;
            }

            oldName = string.Empty;
            return false;
        }

        /// <summary>
        /// Returns the data for the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public UserData GetUserData(string playerId)
        {
            if (!_userdata.TryGetValue(playerId, out UserData data))
            {
                _userdata.Add(playerId, data = new UserData());
            }

            // Return the data
            return data;
        }

        #region Querying

        /// <summary>
        /// Returns if the specified player has the specified permission
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool UserHasPermission(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Always allow the server console
            if (playerId.Equals("server_console"))
            {
                return true;
            }

            perm = perm.ToLower();

            // First, get the player data
            UserData data = GetUserData(playerId);

            // Check if they have the perm
            if (data.Perms.Contains(perm))
            {
                return true;
            }

            // Check if their group has the perm
            return GroupsHavePermission(data.Groups, perm);
        }

        /// <summary>
        /// Check if player has a group
        /// </summary>
        /// <param name="playerId"></param>
        public bool UserHasAnyGroup(string playerId)
        {
            return UserExists(playerId) && GetUserData(playerId).Groups.Count > 0;
        }

        /// <summary>
        /// Get if the player belongs to given group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool UserHasGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            return GetUserData(playerId).Groups.Contains(name.ToLower());
        }

        /// <summary>
        /// Returns if the specified player permission is inherited from a group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool IsUserPermissionInherited(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            perm = perm.ToLower();

            // First, get the player data
            UserData data = GetUserData(playerId);

            // Check if they have the perm
            if (data.Perms.Contains(perm))
            {
                return false;
            }

            // Check if their group has the perm
            return GroupsHavePermission(data.Groups, perm);
        }

        /// <summary>
        /// Returns the group that a specified player permission is inherited from
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string GetUserPermissionGroup(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return null;
            }

            perm = perm.ToLower();

            // First, get the player data
            UserData data = GetUserData(playerId);

            // Check if they have the perm
            if (data.Perms.Contains(perm))
            {
                return null;
            }

            foreach (string group in data.Groups)
            {
                string permissionGroup = GetGroupPermissionGroup(group, perm);
                if (!string.IsNullOrEmpty(permissionGroup))
                {
                    return permissionGroup;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string[] GetUserGroups(string playerId)
        {
            return GetUserData(playerId).Groups.ToArray();
        }

        /// <summary>
        /// Returns the permissions which the specified player has
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string[] GetUserPermissions(string playerId)
        {
            UserData data = GetUserData(playerId);
            List<string> perms = data.Perms.ToList();
            foreach (string group in data.Groups)
            {
                perms.AddRange(GetGroupPermissions(group));
            }

            return perms.Distinct().ToArray();
        }

        /// <summary>
        /// Returns the players with given permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string[] GetPermissionUsers(string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return new string[0];
            }

            perm = perm.ToLower();
            HashSet<string> users = new HashSet<string>();
            foreach (KeyValuePair<string, UserData> data in _userdata)
            {
                if (data.Value.Perms.Contains(perm))
                {
                    users.Add($"{data.Key}({data.Value.LastSeenNickname})");
                }
            }

            return users.ToArray();
        }

        #endregion Querying

        #region User Permissions

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool AddUserGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            UserData data = GetUserData(playerId);
            if (!data.Groups.Add(name.ToLower()))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool RemoveUserGroup(string playerId, string name)
        {
            if (!GroupExists(name))
            {
                return false;
            }

            UserData data = GetUserData(playerId);
            if (name.Equals("*"))
            {
                if (data.Groups.Count <= 0)
                {
                    return false;
                }

                data.Groups.Clear();
                return true;
            }

            if (!data.Groups.Remove(name.ToLower()))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Grants the specified permission to the specified player
        /// </summary>
        /// <param name="id"></param>
        /// <param name="perm"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantUserPermission(string id, string perm, IPlugin plugin)
        {
            // Check if it is even a permission
            if (!PermissionExists(perm, plugin))
            {
                return false;
            }

            // Get the player data
            UserData data = GetUserData(id);

            perm = perm.ToLower();

            if (perm.EndsWith("*"))
            {
                HashSet<string> perms;
                if (plugin == null)
                {
                    perms = new HashSet<string>(Permset.Values.SelectMany(v => v));
                }
                else if (!Permset.TryGetValue(plugin, out perms))
                {
                    return false;
                }

                if (perm.Equals("*"))
                {
                    if (!perms.Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                else
                {
                    perm = perm.TrimEnd('*');
                    if (!perms.Where(s => s.StartsWith(perm)).Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                return true;
            }

            // Add the permission
            return data.Perms.Add(perm);
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        public bool RevokeUserPermission(string playerId, string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Get the player data
            UserData data = GetUserData(playerId);

            perm = perm.ToLower();

            if (perm.EndsWith("*"))
            {
                if (perm.Equals("*"))
                {
                    if (data.Perms.Count <= 0)
                    {
                        return false;
                    }

                    data.Perms.Clear();
                }
                else
                {
                    perm = perm.TrimEnd('*');
                    if (data.Perms.RemoveWhere(s => s.StartsWith(perm)) <= 0)
                    {
                        return false;
                    }
                }

                return true;
            }

            // Remove the permission
            return data.Perms.Remove(perm);
        }

        #endregion User Permissions

        #endregion User Methods

        #region Group Methods

        /// <summary>
        /// Migrate permissions from one group to another
        /// </summary>
        /// <param name="newGroup"></param>
        /// <param name="oldGroup"></param>
        public void MigrateGroup(string oldGroup, string newGroup)
        {
            if (IsLoaded && GroupExists(oldGroup))
            {
                foreach (string perm in GetGroupPermissions(oldGroup))
                {
                    GrantGroupPermission(newGroup, perm, null);
                }

                if (GetUsersInGroup(oldGroup).Length == 0)
                {
                    RemoveGroup(oldGroup);
                }
            }
        }

        #region Querying

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool GroupsHavePermission(IEnumerable<string> groups, string perm)
        {
            return groups.Any(group => GroupHasPermission(group, perm));
        }

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool GroupHasPermission(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return false;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return false;
            }

            // Check if the group has the perm
            return group.Perms.Contains(perm.ToLower()) || GroupHasPermission(group.ParentGroup, perm);
        }

        /// <summary>
        /// Checks if specified permission belongs to group or parent group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool IsGroupPermissionInherited(string name, string perm)
        {
            if (!GroupHasPermission(name, perm))
            {
                return false;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return false;
            }

            return !group.Perms.Contains(perm.ToLower());
        }

        /// <summary>
        /// Returns the parent group that the specified permission belongs to
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        public string GetGroupPermissionGroup(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return null;
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return null;
            }

            if (group.Perms.Contains(perm.ToLower()))
            {
                return name;
            }

            return GetGroupPermissionGroup(group.ParentGroup, perm);
        }

        /// <summary>
        /// Returns the permissions which the specified group has, with optional transversing of parent groups
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parents"></param>
        /// <returns></returns>
        public string[] GetGroupPermissions(string name, bool parents = false)
        {
            if (!GroupExists(name))
            {
                return new string[0];
            }

            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData group))
            {
                return new string[0];
            }

            List<string> perms = group.Perms.ToList();
            if (parents)
            {
                perms.AddRange(GetGroupPermissions(group.ParentGroup));
            }

            return perms.Distinct().ToArray();
        }

        /// <summary>
        /// Returns the groups with given permission
        /// </summary>
        /// <returns></returns>
        public string[] GetPermissionGroups(string perm)
        {
            if (string.IsNullOrEmpty(perm))
            {
                return new string[0];
            }

            perm = perm.ToLower();
            HashSet<string> groups = new HashSet<string>();
            foreach (KeyValuePair<string, GroupData> data in _groupdata)
            {
                if (data.Value.Perms.Contains(perm))
                {
                    groups.Add(data.Key);
                }
            }

            return groups.ToArray();
        }

        /// <summary>
        /// Returns if the specified group exists or not
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool GroupExists(string group)
        {
            return !string.IsNullOrEmpty(group) && (group.Equals("*") || _groupdata.ContainsKey(group.ToLower()));
        }

        /// <summary>
        /// Returns existing groups
        /// </summary>
        /// <returns></returns>
        public string[] GetGroups()
        {
            return _groupdata.Keys.ToArray();
        }

        /// <summary>
        /// Returns players in specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public string[] GetUsersInGroup(string group)
        {
            if (!GroupExists(group))
            {
                return new string[0];
            }

            return _userdata.Where(u => u.Value.Groups.Contains(group.ToLower())).Select(u => $"{u.Key} ({u.Value.LastSeenNickname})").ToArray();
        }

        /// <summary>
        /// Gets the parent of the specified group
        /// </summary>
        /// <param name="group"></param>
        public string GetGroupParent(string group)
        {
            if (!GroupExists(group))
            {
                return string.Empty;
            }

            return !_groupdata.TryGetValue(group.ToLower(), out GroupData data) ? string.Empty : data.ParentGroup;
        }

        /// <summary>
        /// Returns the title of the specified group
        /// </summary>
        /// <param name="group"></param>
        public string GetGroupTitle(string group)
        {
            if (!GroupExists(group))
            {
                return string.Empty;
            }

            // First, get the group data
            if (!_groupdata.TryGetValue(group.ToLower(), out GroupData data))
            {
                return string.Empty;
            }

            // Return the group title
            return data.Title;
        }

        /// <summary>
        /// Returns the rank of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public int GetGroupRank(string group)
        {
            if (!GroupExists(group))
            {
                return 0;
            }

            // First, get the group data
            if (!_groupdata.TryGetValue(group.ToLower(), out GroupData data))
            {
                return 0;
            }

            // Return the group rank
            return data.Rank;
        }

        #endregion Querying

        #region Group Permissions

        /// <summary>
        /// Grant the specified permission to the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantGroupPermission(string name, string perm, IPlugin plugin)
        {
            // Check if it is even a permission
            if (!PermissionExists(perm, plugin) || !GroupExists(name))
            {
                return false;
            }

            // Get the group data
            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData data))
            {
                return false;
            }

            perm = perm.ToLower();

            if (perm.EndsWith("*"))
            {
                HashSet<string> perms;
                if (plugin == null)
                {
                    perms = new HashSet<string>(Permset.Values.SelectMany(v => v));
                }
                else if (!Permset.TryGetValue(plugin, out perms))
                {
                    return false;
                }

                if (perm.Equals("*"))
                {
                    if (!perms.Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                else
                {
                    perm = perm.TrimEnd('*').ToLower();
                    if (!perms.Where(s => s.StartsWith(perm)).Aggregate(false, (c, s) => c | data.Perms.Add(s)))
                    {
                        return false;
                    }
                }
                return true;
            }

            // Add the permission
            return data.Perms.Add(perm);
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        public bool RevokeGroupPermission(string name, string perm)
        {
            if (!GroupExists(name) || string.IsNullOrEmpty(perm))
            {
                return false;
            }

            // Get the group data
            if (!_groupdata.TryGetValue(name.ToLower(), out GroupData data))
            {
                return false;
            }

            // Remove the permission
            return data.Perms.Remove(perm);
        }

        #endregion Group Permissions

        #region Group Management

        /// <summary>
        /// Creates the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="title"></param>
        /// <param name="rank"></param>
        /// <param name="parent"></param>
        public bool CreateGroup(string group, string title, int rank, string parent = "")
        {
            // Check if it already exists
            if (GroupExists(group) || string.IsNullOrEmpty(group))
            {
                return false;
            }

            // Create the data
            GroupData data = new GroupData { Title = title, Rank = rank, ParentGroup = parent };

            // Add the group
            _groupdata.Add(group.ToLower(), data);

            return true;
        }

        /// <summary>
        /// Removes the specified group
        /// </summary>
        /// <param name="group"></param>
        public bool RemoveGroup(string group)
        {
            // Check if it even exists
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // Remove the group
            bool removed = _groupdata.Remove(group);

            // Remove group from players
            bool changed = _userdata.Values.Aggregate(false, (current, userData) => current | userData.Groups.Remove(group));
            if (changed)
            {
                SaveUsers();
            }

            return removed;
        }

        /// <summary>
        /// Sets the title of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="title"></param>
        public bool SetGroupTitle(string group, string title)
        {
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(group, out GroupData data))
            {
                return false;
            }

            // Change the title
            if (data.Title == title)
            {
                return true;
            }

            data.Title = title;

            return true;
        }

        /// <summary>
        /// Sets the rank of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="rank"></param>
        public bool SetGroupRank(string group, int rank)
        {
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(group, out GroupData data))
            {
                return false;
            }

            // Change the rank
            if (data.Rank == rank)
            {
                return true;
            }

            data.Rank = rank;

            return true;
        }

        /// <summary>
        /// Sets the parent of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="parent"></param>
        public bool SetGroupParent(string group, string parent)
        {
            if (!GroupExists(group))
            {
                return false;
            }

            group = group.ToLower();

            // First, get the group data
            if (!_groupdata.TryGetValue(group, out GroupData data))
            {
                return false;
            }

            if (string.IsNullOrEmpty(parent))
            {
                data.ParentGroup = null;
                return true;
            }

            if (!GroupExists(parent) || group.Equals(parent.ToLower()))
            {
                return false;
            }

            parent = parent.ToLower();

            if (!string.IsNullOrEmpty(data.ParentGroup) && data.ParentGroup.Equals(parent))
            {
                return true;
            }

            if (HasCircularParent(group, parent))
            {
                return false;
            }

            // Change the parent group
            data.ParentGroup = parent;

            return true;
        }

        /// <summary>
        /// Determine if group relationship is circular
        /// </summary>
        /// <param name="group"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private bool HasCircularParent(string group, string parent)
        {
            // Get parent data
            if (!_groupdata.TryGetValue(parent, out GroupData parentData))
            {
                return false;
            }

            // Check for circular reference
            HashSet<string> groups = new HashSet<string> { group, parent };
            while (!string.IsNullOrEmpty(parentData.ParentGroup))
            {
                // Found itself?
                if (!groups.Add(parentData.ParentGroup))
                {
                    return true;
                }

                // Get next parent
                if (!_groupdata.TryGetValue(parentData.ParentGroup, out parentData))
                {
                    return false;
                }
            }
            return false;
        }

        #endregion Group Management

        #endregion Group Methods
    }
}
