﻿using System;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.Auth
{
    public enum AuthData
    {
        All,
        Users,
        Groups
    }

    public interface IAuthManager
    {
        bool IsLoaded { get; }

        IDictionary<IPlugin, HashSet<string>> LoadedPermissions { get; }

        IPromise Load(AuthData authData = AuthData.All);

        IPromise Save(AuthData authData = AuthData.All);

        IEvent<Player> PlayerCreated { get; }
        IEvent<Player> PlayerChanged { get; }

        IEvent<PlayerGroup> PlayerGroupGranted { get; }
        IEvent<PlayerGroup> PlayerGroupRevoked { get; }
        IEvent<PlayerGroup[]> PlayerGroupsGranted { get; }
        IEvent<PlayerGroup[]> PlayerGroupsRevoked { get; }
        IEvent<PlayerPermission> PlayerPermissionGranted { get; }
        IEvent<PlayerPermission> PlayerPermissionRevoked { get; }
        IEvent<PlayerPermission[]> PlayerPermissionsGranted { get; }
        IEvent<PlayerPermission[]> PlayerPermissionsRevoked { get; }

        IEvent<Group> GroupCreated { get; }
        IEvent<Group> GroupRemoved { get; }
        IEvent<Group> GroupChanged { get; }
        IEvent<GroupPermission> GroupPermissionGranted { get; }
        IEvent<GroupPermission> GroupPermissionRevoked { get; }

        Func<string, bool> Validate { get; set; }

        void CleanUp();

        #region User Management

        bool UserExists(string id);

        bool UpdateName(string id, string name, out string oldName);

        #endregion User Management

        #region Permission Management

        bool RegisterPermission(string name, IPlugin owner);

        bool UnregisterPermission(string name, IPlugin owner);

        bool PermissionExists(string name, IPlugin owner = null);

        string[] GetPermissions();

        string[] GetPermissions(IPlugin plugin);

        #endregion Permission Management

        #region Server Methods

        string GetServerLanguage();

        void SetServerLanguage(string language);

        #endregion Server Methods

        #region User Methods

        IDictionary<string, string> GetUsers();

        string GetLanguage(string id);

        void SetLanguage(string id, string language);

        bool UserHasPermission(string id, string perm);

        bool UserHasAnyGroup(string id);

        bool IsUserPermissionInherited(string id, string perm);

        string GetUserPermissionGroup(string id, string perm);

        string[] GetUserGroups(string id);

        string[] GetUserPermissions(string id);

        string[] GetPermissionUsers(string perm);

        bool AddUserGroup(string id, string name);

        bool RemoveUserGroup(string id, string name);

        bool UserHasGroup(string id, string name);

        bool GrantUserPermission(string id, string perm, IPlugin plugin);

        bool RevokeUserPermission(string id, string perm);

        #endregion User Methods

        #region Group Methods

        void MigrateGroup(string oldGroup, string newGroup);

        bool GroupsHavePermission(IEnumerable<string> groups, string perm);

        bool GroupHasPermission(string name, string perm);

        bool IsGroupPermissionInherited(string name, string perm);

        string GetGroupPermissionGroup(string name, string perm);

        string[] GetGroupPermissions(string name, bool parents = false);

        string[] GetPermissionGroups(string perm);

        bool GroupExists(string group);

        string[] GetGroups();

        string[] GetUsersInGroup(string group);

        string GetGroupTitle(string group);

        int GetGroupRank(string group);

        bool GrantGroupPermission(string name, string perm, IPlugin plugin);

        bool RevokeGroupPermission(string name, string perm);

        bool CreateGroup(string group, string title, int rank, string parent = "");

        bool RemoveGroup(string group);

        bool SetGroupTitle(string group, string title);

        bool SetGroupRank(string group, int rank);

        string GetGroupParent(string group);

        bool SetGroupParent(string group, string parent);

        #endregion Group Methods
    }
}
