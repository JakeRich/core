﻿using System;
using uMod.Common;
using uMod.Libraries;

namespace uMod.Auth
{
    public abstract class UniversalPlayer : IAuthorizable, IIdentity, IEquatable<IIdentity>, IEquatable<IPlayer>
    {
        #region Initialization

        private static Permission _permission;

        protected UniversalPlayer()
        {
            // Get permission library
            if (_permission == null)
            {
                _permission = Interface.uMod.Libraries.Get<Permission>();
            }
        }

        #endregion Initialization

        #region Identity

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public abstract string Id { get; }

        #endregion Identity

        #region Authorizable

        /// <summary>
        /// Gets if the player is a server administrator
        /// </summary>
        public abstract bool IsAdmin { get; }

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public abstract bool IsModerator { get; }

        #endregion Authorizable

        #region Permissions

        /// <summary>
        /// Determines if the player has the specified permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool Can(string perm) => _permission.UserHasPermission(Id, perm);

        /// <summary>
        /// Determines if the player has the specified permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool HasPermission(string perm) => _permission.UserHasPermission(Id, perm);

        /// <summary>
        /// Grants the specified permission on this player
        /// </summary>
        /// <param name="perm"></param>
        public void GrantPermission(string perm) => _permission.GrantUserPermission(Id, perm, null);

        /// <summary>
        /// Strips the specified permission from this player
        /// </summary>
        /// <param name="perm"></param>
        public void RevokePermission(string perm) => _permission.RevokeUserPermission(Id, perm);

        /// <summary>
        /// Determines if the player belongs to the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool BelongsToGroup(string group) => _permission.UserHasGroup(Id, group);

        /// <summary>
        /// Adds the player to the specified group
        /// </summary>
        /// <param name="group"></param>
        public void AddToGroup(string group) => _permission.AddUserGroup(Id, group);

        /// <summary>
        /// Removes the player from the specified group
        /// </summary>
        /// <param name="group"></param>
        public void RemoveFromGroup(string group) => _permission.RemoveUserGroup(Id, group);

        #endregion Permissions

        #region Comparisons and Overloads

        /// <summary>
        /// Determines if player's unique ID is equal to another player's unique ID
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IIdentity other) => Id == other?.Id;

        /// <summary>
        /// Determines if player's unique ID is equal to another player's unique ID
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IPlayer other) => Id == other?.Id;

        /// <summary>
        /// Determines if player's object is equal to another player's object
        /// </summary>
        /// <param name="identityObj"></param>
        /// <returns></returns>
        public override bool Equals(object identityObj) => identityObj is IIdentity identity && Id == identity.Id;

        /// <summary>
        /// Gets the hash code of the player's unique ID
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => Id.GetHashCode();

        #endregion Comparisons and Overloads
    }
}
