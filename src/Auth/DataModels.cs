extern alias References;

using System;
using System.Collections.Generic;
using System.Linq;
using References::ProtoBuf;
using uMod.Common;
using uMod.Common.Database;
using uMod.Database;

namespace uMod.Auth
{
    /// <summary>
    /// Permission object interface
    /// </summary>
    public interface IPermission
    {
        string Permission { get; }
        DateTime? ValidUntil { get; }
    }

    /// <summary>
    /// Auth context interface
    /// </summary>
    public interface IAuthContext : INameable
    {
    }

    /// <summary>
    /// Group abstraction
    /// </summary>
    [Serializable, Model]
    public class Group : IAuthContext
    {
        /// <summary>
        /// Gets or sets the group name
        /// </summary>
        [PrimaryKey]
        public string Name { get; set; }

        /// <summary>
        /// gGets or sets the group title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the group rank
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Gets or sets the group parent
        /// </summary>
        public string ParentGroup { get; set; }

        /// <summary>
        /// Group permissions
        /// </summary>
        [OneToMany]
        public Dictionary<string, GroupPermission> Permissions;

        /// <summary>
        /// Grant permission to group
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permission"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantPermission(IAuthManager manager, string permission, IPlugin plugin = null)
        {
            permission = permission.ToLower();
            if (Permissions?.ContainsKey(permission) ?? false)
            {
                return false;
            }

            if (permission.EndsWith("*"))
            {
                HashSet<string> perms;
                if (plugin == null)
                {
                    perms = new HashSet<string>(manager.LoadedPermissions.Values.SelectMany(v => v));
                }
                else if (!manager.LoadedPermissions.TryGetValue(plugin, out perms))
                {
                    return false;
                }

                if (permission.Equals("*"))
                {
                    if (!perms.Aggregate(false, (c, s) => c | GrantPermission(manager, s, plugin)))
                    {
                        return false;
                    }
                }
                else
                {
                    permission = permission.TrimEnd('*');
                    if (!perms.Where(s => s.StartsWith(permission)).Aggregate(false, (c, s) => c | GrantPermission(manager, s, plugin)))
                    {
                        return false;
                    }
                }
                return true;
            }

            GroupPermission groupPermission = new GroupPermission()
            {
                GroupName = Name,
                Permission = permission
            };

            if (Permissions == null)
            {
                Permissions = new Dictionary<string, GroupPermission>();
            }

            Permissions.Add(permission, groupPermission);

            manager.GroupPermissionGranted.Invoke(groupPermission);

            return true;
        }

        /// <summary>
        /// Revoke permission from group
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permission"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool RevokePermission(IAuthManager manager, string permission, IPlugin plugin = null)
        {
            permission = permission.ToLower();
            if (Permissions != null && Permissions.TryGetValue(permission, out GroupPermission groupPermission))
            {
                manager.GroupPermissionRevoked.Invoke(groupPermission);
                return Permissions.Remove(permission);
            }

            return false;
        }

        /// <summary>
        /// Grant multiple permissions to group
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permissions"></param>
        /// <returns></returns>
        public int GrantPermission(IAuthManager manager, IEnumerable<string> permissions)
        {
            int i = 0;
            foreach (string permission in permissions)
            {
                if (GrantPermission(manager, permission))
                {
                    i++;
                }
            }

            return i;
        }

        /// <summary>
        /// Revoke multiple permissions from group
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permissions"></param>
        /// <returns></returns>
        public int RevokePermission(IAuthManager manager, IEnumerable<string> permissions)
        {
            int i = 0;
            foreach (string permission in permissions)
            {
                if (RevokePermission(manager, permission))
                {
                    i++;
                }
            }

            return i;
        }

        /// <summary>
        /// Convert group to string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Group permission abstraction
    /// </summary>
    [Serializable, Model]
    public class GroupPermission : IPermission
    {
        /// <summary>
        /// Gets or sets the group name
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets the permission name
        /// </summary>
        [Key]
        public string Permission { get; set; }

        /// <summary>
        /// Gets or sets the permission valid until
        /// </summary>
        public DateTime? ValidUntil { get; set; }

        /// <summary>
        /// Convert permission to string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{GroupName}.{Permission}";
        }
    }

    /// <summary>
    /// Player abstraction
    /// </summary>
    [Serializable, Model]
    public class Player : IAuthContext
    {
        /// <summary>
        /// Gets or sets the player Id
        /// </summary>
        [PrimaryKey]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the player name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the player language
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the player created date
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets or sets the player last login date
        /// </summary>
        public DateTime LastLogin { get; set; }

        /// <summary>
        /// Player groups
        /// </summary>
        [OneToMany]
        public Dictionary<string, PlayerGroup> Groups;

        /// <summary>
        /// Player permissions
        /// </summary>
        [OneToMany]
        public Dictionary<string, PlayerPermission> Permissions;

        /// <summary>
        /// Player nicknames
        /// </summary>
        [OneToMany]
        public Dictionary<string, PlayerNickname> Nicknames;

        /// <summary>
        /// Grant group to player
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public bool GrantGroup(IAuthManager manager, string groupName)
        {
            groupName = groupName.ToLower();
            if (Groups?.ContainsKey(groupName) ?? false)
            {
                return false;
            }

            PlayerGroup playerGroup = new PlayerGroup
            {
                PlayerId = Id,
                GroupName = groupName,
                ValidUntil = null
            };

            if (Groups == null)
            {
                Groups = new Dictionary<string, PlayerGroup>();
            }

            Groups.Add(groupName, playerGroup);

            manager.PlayerGroupGranted.Invoke(playerGroup);

            return true;
        }

        /// <summary>
        /// Revoke group from player
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool RevokeGroup(IAuthManager manager, string group)
        {
            group = group.ToLower();

            if (group.Equals("*"))
            {
                if (Groups == null || Groups.Count <= 0)
                {
                    return false;
                }

                manager.PlayerGroupsRevoked.Invoke(Groups.Values.ToArray());

                Groups.Clear();
                return true;
            }

            if (Groups != null && Groups.TryGetValue(group, out PlayerGroup playerGroup))
            {
                manager.PlayerGroupRevoked.Invoke(playerGroup);

                return Groups.Remove(group);
            }

            return false;
        }

        /// <summary>
        /// Grant permission to player
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permission"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool GrantPermission(IAuthManager manager, string permission, IPlugin plugin = null)
        {
            permission = permission.ToLower();
            if (Permissions?.ContainsKey(permission) ?? false)
            {
                return false;
            }

            if (permission.EndsWith("*"))
            {
                HashSet<string> perms;
                if (plugin == null)
                {
                    perms = new HashSet<string>(manager.LoadedPermissions.Values.SelectMany(v => v));
                }
                else if (!manager.LoadedPermissions.TryGetValue(plugin, out perms))
                {
                    return false;
                }

                if (permission.Equals("*"))
                {
                    if (!perms.Aggregate(false, (c, s) => c | GrantPermission(manager, s, plugin)))
                    {
                        return false;
                    }
                }
                else
                {
                    permission = permission.TrimEnd('*');
                    if (!perms.Where(s => s.StartsWith(permission)).Aggregate(false, (c, s) => c | GrantPermission(manager, s, plugin)))
                    {
                        return false;
                    }
                }
                return true;
            }

            PlayerPermission playerPermission = new PlayerPermission()
            {
                PlayerId = Id,
                Permission = permission
            };

            if (Permissions == null)
            {
                Permissions = new Dictionary<string, PlayerPermission>();
            }

            Permissions.Add(permission, playerPermission);

            manager.PlayerPermissionGranted.Invoke(playerPermission);

            return true;
        }

        /// <summary>
        /// Revoke permission from player
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permission"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool RevokePermission(IAuthManager manager, string permission, IPlugin plugin = null)
        {
            permission = permission.ToLower();

            if (permission.EndsWith("*"))
            {
                if (Permissions == null || Permissions.Count <= 0)
                {
                    return false;
                }

                if (permission.Equals("*"))
                {
                    manager.PlayerPermissionsRevoked.Invoke(Permissions.Values.ToArray());

                    Permissions.Clear();
                }
                else
                {
                    permission = permission.TrimEnd('*');
                    if (RevokePermission(manager, Permissions.Keys.Where(s => s.StartsWith(permission))) <= 0)
                    {
                        return false;
                    }
                }

                return true;
            }

            if (Permissions != null && Permissions.TryGetValue(permission, out PlayerPermission playerPermission))
            {
                manager.PlayerPermissionRevoked.Invoke(playerPermission);
                return Permissions.Remove(permission);
            }

            return false;
        }

        /// <summary>
        /// Grant mutliple permissions to player
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permissions"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public int GrantPermission(IAuthManager manager, IEnumerable<string> permissions, IPlugin plugin = null)
        {
            int i = 0;
            foreach (string permission in permissions)
            {
                if (GrantPermission(manager, permission, plugin))
                {
                    i++;
                }
            }

            return i;
        }

        /// <summary>
        /// Revoke multiple permissions from player
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="permissions"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public int RevokePermission(IAuthManager manager, IEnumerable<string> permissions, IPlugin plugin = null)
        {
            int i = 0;
            foreach (string permission in permissions)
            {
                if (RevokePermission(manager, permission, plugin))
                {
                    i++;
                }
            }

            return i;
        }

        /// <summary>
        /// Convert player to string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Id}.{Name}";
        }
    }

    /// <summary>
    /// Player group abstraction
    /// </summary>
    [Serializable, Model]
    public class PlayerGroup
    {
        /// <summary>
        /// Gets or sets the player Id
        /// </summary>
        public string PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the group name
        /// </summary>
        [Key]
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets the group valid until date
        /// </summary>
        public DateTime? ValidUntil { get; set; }

        /// <summary>
        /// Convert player group to string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{PlayerId}.{GroupName}";
        }
    }

    /// <summary>
    /// Player permission abstraction
    /// </summary>
    [Serializable, Model]
    public class PlayerPermission : IPermission
    {
        /// <summary>
        /// Gets or sets the player Id
        /// </summary>
        public string PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the permission name
        /// </summary>
        [Key]
        public string Permission { get; set; }

        /// <summary>
        /// Gets or sets the permission valid until date
        /// </summary>
        public DateTime? ValidUntil { get; set; }

        /// <summary>
        /// Convert player permission to string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{PlayerId}.{Permission}";
        }
    }

    /// <summary>
    /// Player nickname abstraction
    /// </summary>
    [Serializable, Model]
    public class PlayerNickname : INameable
    {
        /// <summary>
        /// Gets or sets the player Id
        /// </summary>
        public string PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the player name
        /// </summary>
        [Key]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the player created date
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Convert player nickname to string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }
    }

    /// <summary>
    /// Contains all data for a specified player
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class UserData
    {
        /// <summary>
        /// Gets or sets the last seen nickname for this player
        /// </summary>
        public string LastSeenNickname { get; set; } = "Unnamed";

        /// <summary>
        /// Gets or sets the individual permissions for this player
        /// </summary>
        public HashSet<string> Perms { get; set; } = new HashSet<string>();

        /// <summary>
        /// Gets or sets the group for this player
        /// </summary>
        public HashSet<string> Groups { get; set; } = new HashSet<string>();
    }

    /// <summary>
    /// Contains all data for a specified group
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class GroupData
    {
        /// <summary>
        /// Gets or sets the title of this group
        /// </summary>
        public string Title { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the rank of this group
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Gets or sets the individual permissions for this group
        /// </summary>
        public HashSet<string> Perms { get; set; } = new HashSet<string>();

        /// <summary>
        /// Gets or sets the parent for this group
        /// </summary>
        public string ParentGroup { get; set; } = string.Empty;
    }

    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    internal class LangData
    {
        public string Lang = Libraries.Lang.DefaultLang;
        public readonly Dictionary<string, string> UserData = new Dictionary<string, string>();
    }

    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    internal class ExpirationData
    {
        /// <summary>
        /// Keyed by "GROUP_NAME.PERMISSION_NAME"
        /// </summary>
        public readonly Dictionary<string, DateTime?> GroupPermissions = new Dictionary<string, DateTime?>();

        /// <summary>
        /// Keyed by "PLAYER_ID.PERMISSION_NAME"
        /// </summary>
        public readonly Dictionary<string, DateTime?> PlayerPermissions = new Dictionary<string, DateTime?>();

        /// <summary>
        /// Keyed by "PLAYER_ID.GROUP_NAME"
        /// </summary>
        public readonly Dictionary<string, DateTime?> PlayerGroups = new Dictionary<string, DateTime?>();
    }
}
