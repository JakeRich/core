﻿using uMod.Auth.Managers;
using uMod.Common;
using uMod.Configuration;

namespace uMod.Auth
{
    public class Provider
    {
        /// <summary>
        /// Global auth configuration
        /// </summary>
        public Configuration.Auth Configuration { get; }

        /// <summary>
        /// Global auth provider
        /// </summary>
        public IAuthManager Manager { get; private set; }

        /// <summary>
        /// Create new auth container object
        /// </summary>
        /// <param name="auth"></param>
        public Provider(Configuration.Auth auth)
        {
            Configuration = auth;
        }

        /// <summary>
        /// Initialize auth provider
        /// </summary>
        /// <returns></returns>
        public IPromise Initialize(ILogger logger, IInitializationInfo info = null)
        {
            Promise promise = new Promise();

            Configuration.Initialize(Interface.uMod.Database, Interface.uMod.Dispatcher, info).Done(delegate
            {
                CreateProvider(logger).Load().Done(delegate
                {
                    promise.Resolve();
                }, promise.Reject);
            }, promise.Reject);

            return promise;
        }

        /// <summary>
        /// Create provider
        /// </summary>
        /// <returns></returns>
        private IAuthManager CreateProvider(ILogger logger)
        {
            switch (Configuration.Driver)
            {
                case AuthDriver.Database:
                    Manager = new DatabaseAuthManager(Interface.uMod.Auth, logger);
                    break;

                default:
                    Manager = new FileAuthManager(Interface.uMod.Auth, logger);
                    break;
            }

            return Manager;
        }
    }
}
