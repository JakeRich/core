﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using uMod.Common;
using uMod.Plugins;
using uMod.Pooling;

namespace uMod.Auth
{
    public class Gate : BaseGate
    {
        /// <summary>
        /// Exclude types from automatic policy resolver
        /// </summary>
        private readonly Type[] _excludeResolveTypes =
        {
            typeof(BaseGate),
            typeof(Gate),
            typeof(PermissionGate)
        };

        /// <summary>
        /// Dictionary of policy callbacks
        /// </summary>
        protected readonly Dictionary<string, KeyValuePair<MethodInfo, Func<object, object[], bool>>> PolicyCallbacks = new Dictionary<string, KeyValuePair<MethodInfo, Func<object, object[], bool>>>();

        /// <summary>
        /// Plugin associated with gate
        /// </summary>
        protected readonly IPlugin Plugin;

        /// <summary>
        /// List of policies
        /// </summary>
        public override IEnumerable<string> Policies => PolicyCallbacks.Keys;

        /// <summary>
        /// Create a new instance the Gate class
        /// </summary>
        /// <param name="plugin"></param>
        public Gate(IPlugin plugin)
        {
            Plugin = plugin;
            ResolveMethods();
        }

        /// <summary>
        /// Resolve all custom policy methods available to this gate
        /// </summary>
        private void ResolveMethods()
        {
            Type type = GetType();
            MethodInfo[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public);
            foreach (MethodInfo method in methods)
            {
                Type declaringType = method.DeclaringType;

                if (!_excludeResolveTypes.Contains(declaringType) && !method.IsConstructor && method.ReturnType == typeof(bool))
                {
                    Func<object, object[], bool> expressionFunc = MakeMethod(method);
                    PolicyCallbacks.Add(Utility.ReflectedName(method.Name).ToLower(), new KeyValuePair<MethodInfo, Func<object, object[], bool>>(method, expressionFunc));
                }
            }
        }

        private Func<object, object[], bool> MakeMethod(MethodInfo method)
        {
            ParameterExpression instanceParameter = Expression.Parameter(typeof(object), "target");

            ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

            MethodCallExpression call = Expression.Call(
                Expression.Convert(instanceParameter, method.DeclaringType),
                method,
                CreateParameterExpressions(method, argumentsParameter));

            Expression<Func<object, object[], bool>> lambda = Expression.Lambda<Func<object, object[], bool>>(
                Expression.Convert(call, typeof(bool)),
                instanceParameter,
                argumentsParameter);

            return lambda.Compile();
        }

        /// <summary>
        /// Get an array of expressions for parameter conversions
        /// </summary>
        /// <param name="method"></param>
        /// <param name="argumentsParameter"></param>
        /// <returns></returns>
        private UnaryExpression[] CreateParameterExpressions(MethodInfo method, Expression argumentsParameter)
        {
            return method.GetParameters().Select(delegate (ParameterInfo parameter, int index)
            {
                BinaryExpression arrayindex = Expression.ArrayIndex(argumentsParameter, Expression.Constant(index));
                return Expression.Convert(arrayindex, parameter.ParameterType);
            }).ToArray();
        }

        /// <summary>
        /// Gets the player argument from the specified arguments
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        protected T TryGet<T>(object[] arguments, T @default = default, int start = 0)
        {
            if (arguments == null)
            {
                return @default;
            }

            for (int i = start; i < arguments.Length; i++)
            {
                if (!(arguments[i] is T arg))
                {
                    continue;
                }

                return arg;
            }

            return @default;
        }

        /// <summary>
        /// Gets the player argument from the specified arguments
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        protected object TryGet(Type type, object[] arguments, object @default = null, int start = 0)
        {
            if (@default is DBNull)
            {
                @default = null;
            }

            if (arguments == null)
            {
                return @default;
            }

            for (int i = start; i < arguments.Length; i++)
            {
                if (arguments[i] == null || !(type.IsInstanceOfType(arguments[i])))
                {
                    continue;
                }

                return arguments[i];
            }

            return @default;
        }

        /// <summary>
        /// Determine if gate supports specified permission
        /// </summary>
        /// <param name="policy"></param>
        /// <returns></returns>
        public override bool Has(string policy)
        {
            return PolicyCallbacks.ContainsKey(policy);
        }

        /// <summary>
        /// Register policy by callback
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public override IGate Register(string policy, Func<bool> callback)
        {
            PolicyCallbacks.Add(policy, new KeyValuePair<MethodInfo, Func<object, object[], bool>>(callback.Method, (target, p) => callback.Invoke()));
            return this;
        }

        /// <summary>
        /// Map arguments from supplied arguments to expected callback parameters
        /// </summary>
        /// <param name="method"></param>
        /// <param name="argsPassed"></param>
        /// <param name="pooled"></param>
        /// <returns></returns>
        private object[] MapArguments(MethodInfo method, object[] argsPassed, out bool pooled)
        {
            pooled = false;
            ParameterInfo[] parameters = method.GetParameters();
            if (parameters.Length > 0)
            {
                object[] args = ArrayPool.Get(parameters.Length);
                pooled = true;
                for (int i = 0; i < parameters.Length; i++)
                {
                    args[i] = TryGet(parameters[i].ParameterType, argsPassed, parameters[i].DefaultValue, i);
                }

                return args;
            }

            return argsPassed;
        }

        /// <summary>
        /// Determine if the specified policy is allowed
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public override bool Allows(string policy, object[] arguments)
        {
            if (PolicyCallbacks.TryGetValue(policy.ToLower(), out KeyValuePair<MethodInfo, Func<object, object[], bool>> callback))
            {
                object[] mappedArgs = null;
                bool pooled = false;
                try
                {
                    mappedArgs = MapArguments(callback.Key, arguments, out pooled);
                    return callback.Value.Invoke(this, mappedArgs);
                }
                finally
                {
                    if (pooled)
                    {
                        ArrayPool.Free(mappedArgs);
                    }
                }

                //return callback.Invoke(arguments);
            }

            return false;
        }

        /// <summary>
        /// Determine if the specified policy is denied
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public override bool Denies(string policy, object[] arguments)
        {
            if (PolicyCallbacks.TryGetValue(policy.ToLower(), out KeyValuePair<MethodInfo, Func<object, object[], bool>> callback))
            {
                object[] mappedArgs = null;
                bool pooled = false;
                try
                {
                    mappedArgs = MapArguments(callback.Key, arguments, out pooled);
                    return !callback.Value.Invoke(this, mappedArgs);
                }
                finally
                {
                    if (pooled)
                    {
                        ArrayPool.Free(mappedArgs);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Determine if gate allows all of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public virtual bool All(IEnumerable<string> policies, IPlayer player)
        {
            foreach (string policy in policies)
            {
                if (!Allows(policy, player))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determine if gate allows any of the specified policies
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public virtual bool Any(IEnumerable<string> policies, IPlayer player)
        {
            foreach (string policy in policies)
            {
                if (Allows(policy, player))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
