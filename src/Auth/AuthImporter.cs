﻿using uMod.Configuration;

namespace uMod.Auth
{
    /// <summary>
    /// Represents an authentication driver data converter
    /// </summary>
    internal class AuthImporter
    {
        /// <summary>
        /// Imports data from specified driver to configured driver
        /// </summary>
        /// <param name="sourceDriver"></param>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        public IPromise Invoke(AuthDriver sourceDriver, string connectionName = null)
        {
            return Invoke(sourceDriver, Interface.uMod.Auth.Configuration.Driver, connectionName);
        }

        /// <summary>
        /// Converts data from specified driver to target driver
        /// </summary>
        /// <param name="sourceDriver"></param>
        /// <param name="targetDriver"></param>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        public IPromise Invoke(AuthDriver sourceDriver, AuthDriver targetDriver, string connectionName = null)
        {
            return MakeSeeder(sourceDriver, targetDriver, connectionName).Invoke();
        }

        /// <summary>
        /// Creates an auth transformer for the specified formats
        /// </summary>
        /// <param name="sourceDriver"></param>
        /// <param name="targetDriver"></param>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        private AuthTransformer MakeSeeder(AuthDriver sourceDriver, AuthDriver targetDriver, string connectionName = null)
        {
            return new AuthTransformer(Interface.uMod.Database, sourceDriver, targetDriver, connectionName);
        }
    }
}
