﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using uMod.Common;
using uMod.Configuration;
using uMod.Core.Utilities;
using uMod.IO;
using uMod.Pooling;
using uMod.Text;
using uMod.Utilities;

namespace uMod
{
    /// <summary>
    /// Useful extension methods which are added to base types
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Convert string to hash
        /// </summary>
        /// <param name="string"></param>
        /// <param name="algorithm"></param>
        /// <returns></returns>
        public static string ToHash(this string @string, HashAlgorithm algorithm = null)
        {
            if (algorithm == null)
            {
                algorithm = Algorithms.SHA1;
            }
            byte[] buf = Encoding.UTF8.GetBytes(@string);
            byte[] hash = algorithm.ComputeHash(buf, 0, buf.Length);
            return BitConverter.ToString(hash).Replace("-", "");
        }

        /// <summary>
        /// Interpolates specified placeholder with the specified formattable value
        /// </summary>
        /// <param name="string"></param>
        /// <param name="name"></param>
        /// <param name="formattable"></param>
        /// <returns></returns>
        public static string Interpolate(this string @string, string name, object formattable)
        {
            return Formatter.Interpolate(@string, name, formattable);
        }

        /// <summary>
        /// Interpolates specified placeholders with the specified formattable value
        /// </summary>
        /// <param name="string"></param>
        /// <param name="pairs"></param>
        /// <returns></returns>
        public static string Interpolate(this string @string, params (string name, object value)[] pairs)
        {
            return Formatter.Interpolate(@string, pairs);
        }

        /// <summary>
        /// Interpolates the specified object fields and properties
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="string"></param>
        /// <param name="object"></param>
        /// <param name="choiceAmount"></param>
        /// <returns></returns>
        public static string Interpolate<T>(this string @string, T @object, int choiceAmount = 0)
        {
            return Formatter.Interpolate(@string, @object, choiceAmount);
        }

        /// <summary>
        /// Pluralizes string from choice format using specified choice amount
        /// </summary>
        /// <param name="string"></param>
        /// <param name="choiceAmount"></param>
        /// <returns></returns>
        public static string Choice(this string @string, int choiceAmount = 0)
        {
            return Formatter.Choice(@string, choiceAmount);
        }

        /// <summary>
        /// Gets a locale field, property, or method value by reflection path
        /// </summary>
        /// <param name="locale"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetValue(this object locale, string path)
        {
            return ReflectionUtility.GetValue<string>(locale, path);
        }

        /// <summary>
        /// Convert AuthDriver to data format
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static DataFormat ToDataFormat(this AuthDriver driver)
        {
            switch (driver)
            {
                case AuthDriver.Database:
                    return DataFormat.Sql;

                case AuthDriver.Json:
                    return DataFormat.Json;

                case AuthDriver.Protobuf:
                default:
                    return DataFormat.Proto;
            }
        }

        public static IEnumerable<Type> GetNestedInterfaces<T>(this Type type, BindingFlags bindingAttr) where T : class
        {
            return type.GetNestedTypes(bindingAttr).Where(x => x.IsInterface && x.GetInterfaces().Contains(typeof(T)));
        }

        public static IEnumerable<Type> GetNestedAbstractionsOfInterface<T>(this Type type, BindingFlags bindingAttr) where T : class
        {
            return type.GetNestedTypes(bindingAttr).Where(x => x.IsAbstract && x.GetInterfaces().Contains(typeof(T)));
        }

        public static IEnumerable<Type> GetNestedTypesOfInterface<T>(this Type type, BindingFlags bindingAttr) where T : class
        {
            return type.GetNestedTypes(bindingAttr).Where(x => x.GetInterfaces().Contains(typeof(T)));
        }

        public static IEnumerable<Type> GetNestedTypesOfInterface(this Type type, Type @interface, BindingFlags bindingAttr)
        {
            return type.GetNestedTypes(bindingAttr).Where(x => x.GetInterfaces().Contains(@interface));
        }

        public static IEnumerable<Type> GetNestedTypesWithAttribute<T>(this Type type, BindingFlags bindingAttr) where T : Attribute
        {
            return type.GetNestedTypes(bindingAttr).Where(x => x.GetCustomAttribute<T>() != null);
        }

        public static IEnumerable<T> GetCustomAttributes<T>(this Type type, bool inherit = true) where T : Attribute
        {
            return type.GetCustomAttributes(typeof(T), inherit).Cast<T>();
        }

        public static T GetCustomAttribute<T>(this Type type, bool inherit = true) where T : Attribute
        {
            return type.GetCustomAttributes(typeof(T), inherit).FirstOrDefault() as T;
        }

        public static IEnumerable<MethodInfo> GetMethodsWithAttribute<T>(this Type type, BindingFlags bindingAttr) where T : Attribute
        {
            return type.GetMethods(bindingAttr).Where(x => x.GetCustomAttribute<T>() != null);
        }

        public static IEnumerable<T> GetCustomAttributes<T>(this MemberInfo member, bool inherit = true) where T : Attribute
        {
            return member.GetCustomAttributes(typeof(T), inherit).Cast<T>();
        }

        public static T GetCustomAttribute<T>(this MemberInfo member, bool inherit = true) where T : Attribute
        {
            return member.GetCustomAttributes(typeof(T), inherit).FirstOrDefault() as T;
        }

        public static IEnumerable<T> GetCustomAttributesIncludingBaseInterfaces<T>(this Type type)
        {
            Type attributeType = typeof(T);
            return type.GetCustomAttributes(attributeType, true).
                Union(type.GetInterfaces().
                SelectMany(interfaceType => interfaceType.GetCustomAttributes(attributeType, true))).
                Distinct().Cast<T>();
        }

        public static T GetCustomAttributeIncludingBaseInterfaces<T>(this Type type)
        {
            Type attributeType = typeof(T);
            return type.GetCustomAttributes(attributeType, true).
                Union(type.GetInterfaces().
                SelectMany(interfaceType => interfaceType.GetCustomAttributes(attributeType, true))).
                Distinct().Cast<T>().FirstOrDefault();
        }

        public static bool Has<T>(this Enum type, T value)
        {
            try
            {
                return ((int)(object)type & (int)(object)value) == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }

        public static bool Is<T>(this Enum type, T value)
        {
            try
            {
                return (int)(object)type == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }

        public static T Add<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type | (int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    $"Could not append value from enumerated type '{typeof(T).Name}'.", ex);
            }
        }

        public static T Remove<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type & ~(int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    $"Could not remove value from enumerated type '{typeof(T).Name}'.", ex);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <param name="typeOperand"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom(this Type typeOperand, Type[] types)
        {
            for (int i = 0; i < types.Length; i++)
            {
                if (types[i].IsAssignableFrom(typeOperand))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(1);
            types[0] = typeof(T1);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(2);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(3);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3, T4>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(4);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            types[3] = typeof(T4);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3, T4, T5>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(5);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            types[3] = typeof(T4);
            types[4] = typeof(T5);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3, T4, T5, T6>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(6);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            types[3] = typeof(T4);
            types[4] = typeof(T5);
            types[5] = typeof(T6);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3, T4, T5, T6, T7>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(7);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            types[3] = typeof(T4);
            types[4] = typeof(T5);
            types[5] = typeof(T6);
            types[6] = typeof(T7);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <typeparam name="T8"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3, T4, T5, T6, T7, T8>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(8);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            types[3] = typeof(T4);
            types[4] = typeof(T5);
            types[5] = typeof(T6);
            types[6] = typeof(T7);
            types[7] = typeof(T8);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <typeparam name="T8"></typeparam>
        /// <typeparam name="T9"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(9);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            types[3] = typeof(T4);
            types[4] = typeof(T5);
            types[5] = typeof(T6);
            types[6] = typeof(T7);
            types[7] = typeof(T8);
            types[8] = typeof(T9);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Determine if any type is assignable
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <typeparam name="T8"></typeparam>
        /// <typeparam name="T9"></typeparam>
        /// <typeparam name="T10"></typeparam>
        /// <param name="typeOperand"></param>
        /// <returns></returns>
        public static bool IsAssignableFrom<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this Type typeOperand)
        {
            Type[] types = TypePool.Get(10);
            types[0] = typeof(T1);
            types[1] = typeof(T2);
            types[2] = typeof(T3);
            types[3] = typeof(T4);
            types[4] = typeof(T5);
            types[5] = typeof(T6);
            types[6] = typeof(T7);
            types[7] = typeof(T8);
            types[8] = typeof(T9);
            types[9] = typeof(T10);
            try
            {
                return typeOperand.IsAssignableFrom(types);
            }
            finally
            {
                TypePool.Free(types);
            }
        }

        /// <summary>
        /// Convert specified object to generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public static T ToType<T>(this object @object)
        {
            return (T)@object.ToType(typeof(T));
        }

        /// <summary>
        /// Convert specified object to specified type
        /// </summary>
        /// <param name="object"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public static object ToType(this object @object, Type destinationType)
        {
            if (!destinationType.IsGenericType)
            {
                if (@object is IDictionary dict)
                {
                    return dict.ToObject(destinationType);
                }

                return Convert.ChangeType(@object, destinationType);
            }

            if (destinationType.GetGenericTypeDefinition() == typeof(List<>))
            {
                Type valueType = destinationType.GetGenericArguments()[0];
                IList list = (IList)Activator.CreateInstance(destinationType);
                foreach (object val in (IList)@object)
                {
                    list.Add(val is IDictionary customClass ? customClass.ToObject(valueType) : val.ToType(valueType));
                }

                return list;
            }

            if (destinationType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
            {
                Type keyType = destinationType.GetGenericArguments()[0];
                Type valueType = destinationType.GetGenericArguments()[1];
                IDictionary dict = (IDictionary)Activator.CreateInstance(destinationType);
                foreach (object key in ((IDictionary)@object).Keys)
                {
                    object val = ((IDictionary)@object)[key];
                    dict.Add(Convert.ChangeType(key, keyType),
                        val is IDictionary customClass ? customClass.ToObject(valueType) : val.ToType(valueType));
                }

                return dict;
            }

            throw new InvalidCastException("Generic types other than List<> and Dictionary<,> are not supported");
        }

        /// <summary>
        /// Convert a list to list of specified type
        /// </summary>
        /// <param name="source"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        public static object ToList(this IList source, Type listType)
        {
            Type valueType = listType.GetGenericArguments()[0];
            IList list = (IList)Activator.CreateInstance(listType);
            foreach (object val in source)
            {
                list.Add(val is IDictionary customClass ? ToObject(customClass, valueType) : Convert.ChangeType(val, valueType));
            }

            return list;
        }

        /// <summary>
        /// Populate arbitrary object fields and properties with dictionary key value pairs
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public static object PopulateObject(this IDictionary dictionary, object @object)
        {
            Type type = @object.GetType();

            foreach (object key in dictionary.Keys)
            {
                string keyString = key.ToString();
                Type fieldType = null;
                FieldInfo field = null;
                PropertyInfo property = type.GetProperty(keyString);
                fieldType = property?.PropertyType;

                if (fieldType == null)
                {
                    field = type.GetField(keyString);
                    fieldType = field?.FieldType;
                }

                if (fieldType == null)
                {
                    continue;
                }

                object value = dictionary[key];
                if (value is IDictionary dict)
                {
                    value = ToObject(dict, fieldType);
                }
                else if (value is IList list)
                {
                    value = ToList(list, fieldType);
                }

                if (property != null)
                {
                    property.SetValue(@object, value, null);
                }
                else
                {
                    field.SetValue(@object, value);
                }
            }

            return @object;
        }

        /// <summary>
        /// Convert dictionary to arbitrary object
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public static object ToObject(this IDictionary dictionary, Type destinationType)
        {
            return PopulateObject(dictionary, Activator.CreateInstance(destinationType));
        }

        /// <summary>
        /// Generic method to determine if struct value is default
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsDefault<T>(this T value) where T : struct
        {
            return value.Equals(default(T));
        }

        /// <summary>
        /// Returns the last portion of a path separated by slashes
        /// </summary>
        public static string Basename(this string text, string extension = null)
        {
            if (extension != null)
            {
                if (extension.Equals("*.*"))
                {
                    // Return the name excluding any extension
                    Match match = Regex.Match(text, @"([^\\/]+)\.[^\.]+$");
                    if (match.Success)
                    {
                        return match.Groups[1].Value;
                    }
                }
                else
                {
                    // Return the name excluding the given extension
                    if (extension[0] == '*')
                    {
                        extension = extension.Substring(1);
                    }
                    return Regex.Match(text, @"([^\\/]+)\" + extension + "+$").Groups[1].Value;
                }
            }
            // No extension was given or the path has no extension, return the full file name
            return Regex.Match(text, @"[^\\/]+$").Groups[0].Value;
        }

        /// <summary>
        /// Checks if an array contains a specific item
        /// </summary>
        public static bool Contains<T>(this T[] array, T value)
        {
            foreach (T item in array)
            {
                if (item.Equals(value))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the directory portion of a path separated by slashes
        /// </summary>
        public static string Dirname(this string text) => Regex.Match(text, "(.+)[\\/][^\\/]+$").Groups[1].Value;

        /// <summary>
        /// Converts PascalCase and camelCase to multiple words
        /// </summary>
        public static string Humanize(this string name) => Regex.Replace(name, @"(\B[A-Z])", " $1");

        /// <summary>
        /// Checks if a string is a valid 64-bit Steam ID
        /// </summary>
        public static bool IsSteamId(this string id)
        {
            return ulong.TryParse(id, out ulong targetId) && targetId > 76561197960265728ul;
        }

        /// <summary>
        /// Checks if an object is a valid 64-bit Steam ID
        /// </summary>
        public static bool IsSteamId(this object id) => (ulong)id > 76561197960265728ul;

        /// <summary>
        /// Checks if a ulong is a valid 64-bit Steam ID
        /// </summary>
        public static bool IsSteamId(this ulong id) => id > 76561197960265728ul;

        /// <summary>
        /// Converts a string to plain text
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Plaintext(this string text) => Formatter.ToPlaintext(text);

        /// <summary>
        /// Converts a string into a quote safe string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string QuoteSafe(this string text) => "\"" + text.Replace("\"", "\\\"").TrimEnd('\\') + "\"";

        /// <summary>
        /// Converts a string into a quote safe string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Quote(this string text) => QuoteSafe(text);

        /// <summary>
        /// Returns a random value from an array
        /// </summary>
        public static T Sample<T>(this T[] array) => array[Utility.Random.Range(0, array.Length)];

        /// <summary>
        /// Converts a string into a sanitized string for string.Format
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Sanitize(this string text) => text.Replace("{", "{{").Replace("}", "}}");

        /// <summary>
        /// Converts a string to Sentence case
        /// </summary>
        public static string SentenceCase(this string text)
        {
            Regex regex = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);
            return regex.Replace(text.ToLower(), s => s.Value.ToUpper());
        }

        /// <summary>
        /// Converts a string to Title Case
        /// </summary>
        public static string TitleCase(this string text)
        {
            return CultureInfo.InstalledUICulture.TextInfo.ToTitleCase(text.Contains('_') ? text.Replace('_', ' ') : text);
        }

        /// <summary>
        /// Converts a string to Title Case
        /// </summary>
        public static string Titleize(this string text) => TitleCase(text);

        /// <summary>
        /// Turns an array of strings into a sentence
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static string ToSentence<T>(this IEnumerable<T> items)
        {
            IEnumerator<T> enumerator = items.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                enumerator.Dispose();
                return string.Empty;
            }

            T firstItem = enumerator.Current;
            if (!enumerator.MoveNext())
            {
                enumerator.Dispose();
                return firstItem?.ToString();
            }

            StringBuilder builder = Pools.StringBuilders.Get();
            builder.Append(firstItem?.ToString());
            bool moreItems = true;
            while (moreItems)
            {
                T item = enumerator.Current;
                moreItems = enumerator.MoveNext();
                builder.Append(moreItems ? ", " : " and ");
                builder.Append(item);
            }

            enumerator.Dispose();
            string sentence = builder.ToString();
            Pools.StringBuilders.Free(builder);
            return sentence;
        }

        /// <summary>
        /// Shortens a string to the length specified
        /// </summary>
        /// <param name="text"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static string Truncate(this string text, int max) => text.Length <= max ? text : text.Substring(0, max) + " ...";
    }
}
