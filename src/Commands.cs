using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using uMod.Auth;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using uMod.Configuration;
using uMod.Libraries;
using uMod.Localization;
using uMod.Plugins;

namespace uMod
{
    /// <summary>
    /// Universal commands for all supported games
    /// </summary>
    public class Commands
    {
        // Libraries and references
        internal readonly Universal Universal = Interface.uMod.Libraries.Get<Universal>();
        internal readonly Lang Lang = Interface.uMod.Libraries.Get<Lang>();
        internal readonly Permission Permission = Interface.uMod.Libraries.Get<Permission>();
        internal readonly IPluginManager PluginManager = Interface.uMod.Plugins.Manager;
        internal Func<IPlayer, IArgs, CommandState> VersionCallback;
        internal Plugin Plugin;

        #region Command Initialization

        public void Initialize(IPlugin plugin, Func<IPlayer, IArgs, CommandState> versionCallback = null)
        {
            Plugin = plugin as Plugin;
            VersionCallback = versionCallback;
            List<string> commandList = new List<string>(); // Reduces instantiation of lists

            // Add core plugin commands
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "plugins", true), PluginsCommand, "umod.plugins");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "load", "plugin.load"), LoadCommand, "umod.load");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "reload", "plugin.reload"), ReloadCommand, "umod.reload");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "unload", "plugin.unload"), UnloadCommand, "umod.unload");

            // Add core permission commands
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "grant", "perm.grant"), GrantCommand, "umod.grant");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "group", "perm.group"), GroupCommand, "umod.group");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "revoke", "perm.revoke"), RevokeCommand, "umod.revoke");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "show", "perm.show"), ShowCommand, "umod.show");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "usergroup", "perm.usergroup"), UserGroupCommand, "umod.usergroup");

            // Add core misc commands
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "lang", true), LangCommand, string.Empty);
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "shutdown", "quit"), ShutdownCommand, "umod.shutdown");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "save", false), SaveCommand, "umod.save");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "import", false), ImportCommand, "umod.import");
            CreateCommand(plugin, CreatePrefixedCommandList(commandList, "version", false), VersionProxyCallback, string.Empty);
        }

        private void CreateCommand(IPlugin plugin, IEnumerable<string> commands, Func<IPlayer, IArgs, CommandState> callback, string permission)
        {
            ICommandDefinition commandDefinition = new CommandDefinition(commands);
            ICommandInfo command = new CommandInfo(commandDefinition, plugin.Commands.CreateCallback(commandDefinition, callback), (!string.IsNullOrEmpty(permission) ? new[] { permission } : null));
            plugin.Commands.Add(command);
        }

        private CommandState VersionProxyCallback(IPlayer player, IArgs args)
        {
            if (VersionCallback != null && VersionCallback.Invoke(player, args) == CommandState.Completed)
            {
                return CommandState.Completed;
            }

            return VersionCommand(player, args);
        }

        private IEnumerable<string> CreatePrefixedCommandList(List<string> commandList, string command, object globalCommand)
        {
            commandList.Clear();
            commandList.AddRange(new[]
            {
                $"umod.{command}",
                $"u.{command}",
                $"oxide.{command}",
                $"o.{command}",
            });

            if (globalCommand is bool b && b)
            {
                commandList.Add(command);
            }

            if (globalCommand is string)
            {
                commandList.Add(globalCommand.ToString());
            }

            return commandList;
        }

        #endregion Command Initialization

        #region Grant Command

        /// <summary>
        /// Called when the "grant" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState GrantCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 3)
            {
                player.Reply(localization.CommandUsage.UsageGrant);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args[1].Sanitize();
            string perm = args[2];

            if (!Permission.PermissionExists(perm))
            {
                player.Reply(localization.Permission.PermissionNotFound.Interpolate("permission", perm));
                return CommandState.Canceled;
            }

            if (mode.Equals("group"))
            {
                if (!Permission.GroupExists(name))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", name));
                    return CommandState.Canceled;
                }

                if (Permission.GroupHasPermission(name, perm))
                {
                    player.Reply(localization.Group.GroupAlreadyHasPermission.Interpolate(("group", name), ("permission", perm)));
                    return CommandState.Canceled;
                }

                Permission.GrantGroupPermission(name, perm, null);
                player.Reply(localization.Group.GroupPermissionGranted.Interpolate(("group", name), ("permission", perm)));
            }
            else if (mode.Equals("user") || mode.Equals("player"))
            {
                IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
                if (foundPlayers.Length > 1)
                {
                    player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                    return CommandState.Canceled;
                }

                IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
                if (target == null && !Permission.UserIdValid(name))
                {
                    player.Reply(localization.Player.PlayerNotFound.Interpolate("name", name));
                    return CommandState.Canceled;
                }

                string playerId = name;
                if (target != null)
                {
                    playerId = target.Id;
                    name = target.Name;
                    Permission.UpdateNickname(playerId, name);
                }

                if (Permission.UserHasPermission(name, perm))
                {
                    player.Reply(localization.PlayerPermission.PlayerAlreadyHasPermission.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
                    return CommandState.Canceled;
                }

                Permission.GrantUserPermission(playerId, perm, null);
                player.Reply(localization.PlayerPermission.PlayerPermissionGranted.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageGrant);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Grant Command

        // TODO: GrantAllCommand (grant all permissions from user(s)/group(s))

        #region Group Command

        /// <summary>
        /// Called when the "group" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState GroupCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 2)
            {
                player.Reply(localization.CommandUsage.UsageGroup);
                player.Reply(localization.CommandUsage.UsageGroupParent);
                player.Reply(localization.CommandUsage.UsageGroupRemove);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string group = args[1];
            string title = args.Length >= 3 ? args[2] : "";
            int rank = args.Length == 4 ? int.Parse(args[3]) : 0;

            if (mode.Equals("add"))
            {
                if (Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupAlreadyExists.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                Permission.CreateGroup(group, title, rank);
                player.Reply(localization.Group.GroupCreated.Interpolate("group", group));
            }
            else if (mode.Equals("remove"))
            {
                if (!Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                Permission.RemoveGroup(group);
                player.Reply(localization.Group.GroupDeleted.Interpolate("group", group));
            }
            else if (mode.Equals("set"))
            {
                if (!Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                Permission.SetGroupTitle(group, title);
                Permission.SetGroupRank(group, rank);
                player.Reply(localization.Group.GroupChanged.Interpolate("group", group));
            }
            else if (mode.Equals("parent"))
            {
                if (args.Length <= 2)
                {
                    player.Reply(localization.CommandUsage.UsageGroupParent);
                    return CommandState.Canceled;
                }

                if (!Permission.GroupExists(group))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                string parent = args[2];
                if (!string.IsNullOrEmpty(parent) && !Permission.GroupExists(parent))
                {
                    player.Reply(localization.Group.GroupParentNotFound.Interpolate("group", group));
                    return CommandState.Canceled;
                }

                if (Permission.SetGroupParent(group, parent))
                {
                    player.Reply(localization.Group.GroupParentChanged.Interpolate(("group", group), ("parent", parent)));
                }
                else
                {
                    player.Reply(localization.Group.GroupParentNotChanged.Interpolate("group", group));
                }
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageGroup);
                player.Reply(localization.CommandUsage.UsageGroupParent);
                player.Reply(localization.CommandUsage.UsageGroupRemove);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Group Command

        #region Lang Command

        /// <summary>
        /// Called when the "lang" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState LangCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageLang);
                return CommandState.Canceled;
            }

            if (player.IsServer)
            {
                if (!Lang.HasLanguage(args[0]))
                {
                    player.Reply(localization.Server.ServerLanguageNotFoundOrEmpty.Interpolate("language", args[0]));
                }

                Lang.SetServerLanguage(args[0]);
                player.Reply(localization.Server.ServerLanguage.Interpolate("language", Lang.GetServerLanguage()));
            }
            else
            {
                if (Lang.HasLanguage(args[0]))
                {
                    Lang.SetLanguage(args[0], player.Id);
                    player.Reply(localization.Player.PlayerLanguage.Interpolate("language", args[0]));
                }
                else
                {
                    player.Reply(localization.Player.PlayerLanguageNotFound.Interpolate("language", args[0]));
                }
            }

            return CommandState.Completed;
        }

        #endregion Lang Command

        #region Load Command

        /// <summary>
        /// Called when the "load" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState LoadCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageLoad);
                return CommandState.Canceled;
            }

            if (args[0].Equals("*") || args[0].ToLower().Equals("all"))
            {
                Interface.uMod.Plugins.LoadAll();
                return CommandState.Canceled;
            }

            Interface.uMod.Plugins.Load(args.Cast<string>());

            return CommandState.Completed;
        }

        #endregion Load Command

        #region Plugins Command

        /// <summary>
        /// Called when the "plugins" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState PluginsCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            IPlugin[] loadedPlugins = PluginManager.GetPlugins().Where(pl => !pl.IsCorePlugin).ToArray();
            HashSet<string> loadedPluginNames = new HashSet<string>(loadedPlugins.Select(pl => pl.Name));
            Dictionary<string, string> unloadedPluginErrors = new Dictionary<string, string>();
            foreach (IPluginLoader loader in Interface.uMod.Extensions.GetLoaders())
            {
                foreach (FileInfo file in loader.ScanDirectory(Interface.uMod.PluginDirectory).Where(f => !loadedPluginNames.Contains(Utility.GetFileNameWithoutExtension(f.Name))))
                {
                    string pluginName = Utility.GetFileNameWithoutExtension(file.Name);
                    unloadedPluginErrors[pluginName] = loader.PluginErrors.TryGetValue(file.Name, out string msg)
                        ? msg
                        : Interface.uMod.Strings.Plugin.Unloaded;
                }
            }

            int totalPluginCount = loadedPlugins.Length + unloadedPluginErrors.Count;
            if (totalPluginCount < 1)
            {
                player.Reply(Interface.uMod.GetStrings(player).Plugin.PluginsNotFound);
                return CommandState.Canceled;
            }

            int count = loadedPlugins.Length + unloadedPluginErrors.Count;
            string pluginNoun = localization.Plugin.Plugin.Choice(count).ToLower();
            string output = localization.Plugin.ListingHeader.Interpolate(("count", count), ("noun", pluginNoun));
            int number = 1;
            foreach (IPlugin plugin in loadedPlugins.Where(p => p.Filename != null))
            {
                string titleCard = localization.Plugin.TitleCard
                    .Interpolate(("title", plugin.Title), ("version", plugin.Version), ("author", plugin.Author));
                output += $"\n  {number++:00} {titleCard} ({plugin.Diagnostics.TotalHookTime:0.00}s) - {plugin.Filename.Basename()}";
            }

            foreach (string pluginName in unloadedPluginErrors.Keys)
            {
                output += $"\n  {number++:00} {pluginName} - {unloadedPluginErrors[pluginName]}";
            }

            player.Reply(output);

            return CommandState.Completed;
        }

        #endregion Plugins Command

        #region Reload Command

        /// <summary>
        /// Called when the "reload" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState ReloadCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageReload);
                return CommandState.Canceled;
            }

            if (args[0].Equals("*") || args[0].ToLower().Equals("all"))
            {
                Interface.uMod.Plugins.ReloadAll();
                return CommandState.Completed;
            }

            Interface.uMod.Plugins.Reload(args.Cast<string>());

            return CommandState.Completed;
        }

        #endregion Reload Command

        #region Revoke Command

        /// <summary>
        /// Called when the "revoke" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState RevokeCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 3)
            {
                player.Reply(localization.CommandUsage.UsageRevoke);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args[1].Sanitize();
            string perm = args[2];

            if (mode.Equals("group"))
            {
                if (!Permission.GroupExists(name))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", name));
                    return CommandState.Canceled;
                }

                if (!Permission.GroupHasPermission(name, perm))
                {
                    player.Reply(localization.Group.GroupDoesNotHavePermission.Interpolate(("group", name), ("permission", perm)));
                    return CommandState.Canceled;
                }

                if (Permission.IsGroupPermissionInherited(name, perm))
                {
                    string permissionGroup = Permission.GetGroupPermissionGroup(name, perm);
                    if (!string.IsNullOrEmpty(permissionGroup))
                    {
                        player.Reply(localization.Group.GroupPermissionInherited.Interpolate(("group", name), ("permission", perm), ("parentGroup", permissionGroup)));
                        return CommandState.Canceled;
                    }
                }

                Permission.RevokeGroupPermission(name, perm);
                player.Reply(localization.Group.GroupPermissionRevoked.Interpolate(("group", name), ("permission", perm)));
            }
            else if (mode.Equals("user") || mode.Equals("player"))
            {
                IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
                if (foundPlayers.Length > 1)
                {
                    player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                    return CommandState.Canceled;
                }

                IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
                if (target == null && !Permission.UserIdValid(name))
                {
                    player.Reply(localization.Player.PlayerNotFound.Interpolate("player", name));
                    return CommandState.Canceled;
                }

                string playerId = name;
                if (target != null)
                {
                    playerId = target.Id;
                    name = target.Name;
                    Permission.UpdateNickname(playerId, name);
                }

                if (!Permission.UserHasPermission(playerId, perm))
                {
                    player.Reply(localization.PlayerPermission.PlayerDoesNotHavePermission.Interpolate(("player", name), ("permission", perm)));
                    return CommandState.Canceled;
                }

                if (Permission.IsUserPermissionInherited(playerId, perm))
                {
                    string permissionGroup = Permission.GetUserPermissionGroup(playerId, perm);
                    if (!string.IsNullOrEmpty(permissionGroup))
                    {
                        player.Reply(localization.PlayerPermission.PlayerPermissionInherited.Interpolate(("player", name), ("permission", perm), ("group", permissionGroup)));
                        return CommandState.Canceled;
                    }
                }

                Permission.RevokeUserPermission(playerId, perm);
                player.Reply(localization.PlayerPermission.PlayerPermissionRevoked.Interpolate(("player", $"{name} ({playerId})"), ("permission", perm)));
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageRevoke);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Revoke Command

        // TODO: RevokeAllCommand (revoke all permissions from user(s)/group(s))

        #region Show Command

        /// <summary>
        /// Called when the "show" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState ShowCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageShow);
                player.Reply(localization.CommandUsage.UsageShowName);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args.Length == 2 ? args[1].Sanitize() : string.Empty;

            if (mode.Equals("perms"))
            {
                player.Reply(localization.Permission.Permission.Choice(2) + ":\n" + string.Join(", ", Permission.GetPermissions()));
            }
            else if (mode.Equals("perm"))
            {
                if (args.Length < 2 || string.IsNullOrEmpty(name))
                {
                    player.Reply(localization.CommandUsage.UsageShow);
                    player.Reply(localization.CommandUsage.UsageShowName);
                    return CommandState.Canceled;
                }

                string[] users = Permission.GetPermissionUsers(name);
                string[] groups = Permission.GetPermissionGroups(name);
                string result = localization.Permission.PermissionPlayers.Interpolate("permission", name) + ":\n";
                result += users.Length > 0 ? string.Join(", ", users) : localization.Permission.NoPermissionPlayers;
                result += $"\n\n{localization.Permission.PermissionGroups.Interpolate("permission", name)}:\n";
                result += groups.Length > 0 ? string.Join(", ", groups) : localization.Permission.NoPermissionGroups;
                player.Reply(result);
            }
            else if (mode.Equals("user") || mode.Equals("player"))
            {
                if (args.Length < 2 || string.IsNullOrEmpty(name))
                {
                    player.Reply(localization.CommandUsage.UsageShow);
                    player.Reply(localization.CommandUsage.UsageShowName);
                    return CommandState.Canceled;
                }

                IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
                if (foundPlayers.Length > 1)
                {
                    player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                    return CommandState.Canceled;
                }

                IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
                if (target == null && !Permission.UserIdValid(name))
                {
                    player.Reply(localization.Player.PlayerNotFound.Interpolate("player", name));
                    return CommandState.Canceled;
                }

                string playerId = name;
                if (target != null)
                {
                    playerId = target.Id;
                    name = target.Name;
                    Permission.UpdateNickname(playerId, name);
                    name += $" ({playerId})";
                }

                string[] perms = Permission.GetUserPermissions(playerId);
                string[] groups = Permission.GetUserGroups(playerId);
                string result = localization.PlayerPermission.PlayerPermissions.Interpolate("player", name) + ":\n";
                result += perms.Length > 0 ? string.Join(", ", perms) : localization.PlayerPermission.NoPlayerPermissions;
                result += $"\n\n{localization.PlayerGroup.PlayerGroups.Interpolate("player", name)}:\n";
                result += groups.Length > 0 ? string.Join(", ", groups) : localization.PlayerGroup.NoPlayerGroups;
                player.Reply(result);
            }
            else if (mode.Equals("group"))
            {
                if (args.Length < 2 || string.IsNullOrEmpty(name))
                {
                    player.Reply(localization.CommandUsage.UsageShow);
                    player.Reply(localization.CommandUsage.UsageShowName);
                    return CommandState.Canceled;
                }

                if (!Permission.GroupExists(name))
                {
                    player.Reply(localization.Group.GroupNotFound.Interpolate("group", name));
                    return CommandState.Canceled;
                }

                string[] users = Permission.GetUsersInGroup(name);
                string[] perms = Permission.GetGroupPermissions(name);

                string result = localization.Group.GroupPlayers.Interpolate("group", name) + ":\n";
                result += users.Length > 0 ? string.Join(", ", users) : localization.Group.NoPlayersInGroup;
                result += $"\n\n{localization.Group.GroupPermissions.Interpolate("group", name)}:\n";
                result += perms.Length > 0 ? string.Join(", ", perms) : localization.Group.NoGroupPermissions;

                string parent = Permission.GetGroupParent(name);
                while (Permission.GroupExists(parent))
                {
                    result += $"\n{localization.Group.ParentGroupPermissions.Interpolate("group", name)}:\n";
                    result += string.Join(", ", Permission.GetGroupPermissions(parent));
                    parent = Permission.GetGroupParent(parent);
                }
                player.Reply(result);
            }
            else if (mode.Equals("groups"))
            {
                player.Reply(localization.Group.Group.Choice(2) + ":\n" + string.Join(", ", Permission.GetGroups()));
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageShow);
                player.Reply(localization.CommandUsage.UsageShowName);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion Show Command

        #region Unload Command

        /// <summary>
        /// Called when the "unload" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState UnloadCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 1)
            {
                player.Reply(localization.CommandUsage.UsageUnload);
                return CommandState.Canceled;
            }

            if (args[0].Equals("*") || args[0].ToLower().Equals("all"))
            {
                Interface.uMod.Plugins.UnloadAll();
                return CommandState.Canceled;
            }

            Interface.uMod.Plugins.Unload(args.Cast<string>());

            return CommandState.Completed;
        }

        #endregion Unload Command

        #region User Group Command

        /// <summary>
        /// Called when the "usergroup" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState UserGroupCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length < 3)
            {
                player.Reply(localization.CommandUsage.UsageUserGroup);
                return CommandState.Canceled;
            }

            string mode = args[0];
            string name = args[1].Sanitize();
            string group = args[2];

            IPlayer[] foundPlayers = Universal.Players.FindPlayers(name).ToArray();
            if (foundPlayers.Length > 1)
            {
                player.Reply(localization.Player.PlayersFound.Interpolate("playerList", string.Join(", ", foundPlayers.Select(p => p.Name).ToArray())));
                return CommandState.Canceled;
            }

            IPlayer target = foundPlayers.Length == 1 ? foundPlayers[0] : null;
            if (target == null && !Permission.UserIdValid(name))
            {
                player.Reply(localization.Player.PlayerNotFound.Interpolate(("player", name)));
                return CommandState.Canceled;
            }

            string playerId = name;
            if (target != null)
            {
                playerId = target.Id;
                name = target.Name;
                Permission.UpdateNickname(playerId, name);
                name += $"({playerId})";
            }

            if (!Permission.GroupExists(group))
            {
                player.Reply(localization.Group.GroupNotFound.Interpolate("group", group));
                return CommandState.Canceled;
            }

            if (mode.Equals("add"))
            {
                Permission.AddUserGroup(playerId, group);
                player.Reply(localization.PlayerGroup.PlayerAddedToGroup.Interpolate(("player", name), ("group", group)));
            }
            else if (mode.Equals("remove"))
            {
                Permission.RemoveUserGroup(playerId, group);
                player.Reply(localization.PlayerGroup.PlayerRemovedFromGroup.Interpolate(("player", name), ("group", group)));
            }
            else
            {
                player.Reply(localization.CommandUsage.UsageUserGroup);
                return CommandState.Canceled;
            }

            return CommandState.Completed;
        }

        #endregion User Group Command

        // TODO: UserGroupAllCommand (add/remove all users to/from group)

        #region Version Command

        /// <summary>
        /// Called when the "version" command has been executed
        /// </summary>
        /// <param name="player"></param>
        /// <param name="args"></param>
        public CommandState VersionCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            string format = Universal.FormatText(localization.Server.Version);
            player.Reply(format.Interpolate(("version", Module.Version), ("game", Universal.Game), ("serverVersion", Universal.Server.Version), ("protocol", Universal.Server.Protocol)));
            return CommandState.Completed;
        }

        #endregion Version Command

        #region Save Command

        public CommandState SaveCommand(IPlayer player, IArgs args)
        {
            Interface.uMod.OnSave();
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            player.Reply(localization.Data.DataSaved);
            return CommandState.Completed;
        }

        #endregion Save Command

        #region Shutdown Command

        private CommandState ShutdownCommand(IPlayer player, IArgs args)
        {
            Universal.Server.Shutdown(); // TODO: Implement argument handling for optional save true/false and delay
            return CommandState.Completed;
        }

        #endregion Shutdown Command

        #region Import Command

        private CommandState ImportCommand(IPlayer player, IArgs args)
        {
            CoreLocalization localization = Interface.uMod.GetStrings(player);
            if (args.Length > 0)
            {
                string system = args[0];
                string driver = string.Empty;

                bool force = false;
                string connectionName = string.Empty;
                if (args.Length > 1)
                {
                    for (int i = 1; i < args.Length; i++)
                    {
                        switch (args[i].ToLower())
                        {
                            case "force":
                                force = true;
                                break;

                            default:
                                if (Interface.uMod.Database.Configuration.Connections.ContainsKey(args[i]))
                                {
                                    connectionName = args[i];
                                    driver = "sql";
                                }
                                else
                                {
                                    driver = args[i];
                                }

                                break;
                        }
                    }
                }
                else
                {
                    player.Reply(localization.CommandUsage.UsageImport);
                    return CommandState.Canceled;
                }

                switch (system.ToLower())
                {
                    case "auth":
                        AuthDriver authDriver;
                        try
                        {
                            authDriver = (AuthDriver)Enum.Parse(typeof(AuthDriver), driver, true);
                        }
                        catch (ArgumentException)
                        {
                            player.Reply(localization.Command.DriverInvalid.Interpolate("driver", driver));
                            return CommandState.Canceled;
                        }

                        AuthImporter importer = new AuthImporter();

                        if (Interface.uMod.Auth.Configuration.Driver != authDriver || force)
                        {
                            importer.Invoke(authDriver, connectionName).Done(delegate ()
                            {
                                player.Reply(localization.Command.ImportSuccess);
                            }, delegate (Exception exception)
                            {
                                player.Reply(localization.Command.ImportFailureErrors.Interpolate("errors", exception.Message));
                            });
                        }
                        else
                        {
                            player.Reply(localization.Command.ImportFailureErrors.Interpolate("errors", $"Metadata already in format: {authDriver}"));
                        }

                        return CommandState.Completed;
                }
            }

            player.Reply(localization.CommandUsage.UsageImport);
            return CommandState.Canceled;
        }

        #endregion Import Command
    }
}
