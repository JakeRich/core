﻿namespace uMod.Web
{
    public class Provider
    {
        internal Configuration.Web Configuration;
        internal Apps.WebClient Application;

        internal Provider(Configuration.Web configuration)
        {
            Configuration = configuration;
        }
    }
}
