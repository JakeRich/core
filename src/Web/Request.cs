using System;
using System.Collections.Generic;
using System.Net;
using uMod.Common;
using uMod.Common.Web;
using uMod.Libraries;

namespace uMod.Web
{
    public class Request : Promise<Common.Web.WebResponse>, IWebRequest
    {
        private static readonly IDictionary<string, string> DefaultHeaders = new Dictionary<string, string>()
        {
            ["User-Agent"] = $"uMod/{Module.Version} ({Environment.OSVersion}; {Environment.OSVersion.Platform}; https://umod.org)"
        };

        /// <summary>
        /// Maximum request duration
        /// </summary>
        public float Timeout { get; internal set; } = Interface.uMod?.Web?.Configuration?.DefaultTimeout ?? 30f;

        /// <summary>
        /// Request HTTP method (GET, POST, etc)
        /// </summary>
        public WebRequestMethod Method { get; internal set; } = WebRequestMethod.GET;

        /// <summary>
        /// Request Url
        /// </summary>
        public string Url { get; internal set; }

        /// <summary>
        /// Request Body
        /// </summary>
        public string Body { get; internal set; }

        /// <summary>
        /// Request cookies
        /// </summary>
        public IDictionary<string, string> Cookies { get; internal set; }

        /// <summary>
        /// Request headers
        /// </summary>
        public IDictionary<string, string> Headers { get; internal set; }

        /// <summary>
        /// Request Context
        /// </summary>
        public IContext Context;

        private Apps.WebClient _client;

        /// <summary>
        /// Create new Request
        /// </summary>
        /// <param name="client"></param>
        /// <param name="context"></param>
        internal Request(Apps.WebClient client, IContext context)
        {
            _client = client;
            Context = context;
        }

        /// <summary>
        /// Create new Request without context
        /// </summary>
        public Request()
        {
        }

        /// <summary>
        /// Reset request after pooling
        /// </summary>
        /// <param name="client"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal Request Reset(Apps.WebClient client, IContext context)
        {
            _client = client;
            Context = context;
            return this;
        }

        /// <summary>
        /// Get intermediate WebRequestData for web client
        /// </summary>
        /// <returns></returns>
        internal Common.Web.WebRequest GetData()
        {
            if (Headers == null)
            {
                Headers = DefaultHeaders;
            }
            else if (!Headers.ContainsKey("User-Agent"))
            {
                Headers.Add("User-Agent", DefaultHeaders["User-Agent"]);
            }

            return new Common.Web.WebRequest
            {
                Method = Method,
                Url = Url,
                Timeout = (int)Math.Round((Timeout.Equals(0f) ? (Interface.uMod?.Web?.Configuration?.DefaultTimeout ?? 30f) : Timeout)),
                Decompression = WebRequests.AllowDecompression ? (DecompressionMethods.GZip | DecompressionMethods.Deflate) : DecompressionMethods.None,
                Headers = Headers,
                Cookies = Cookies,
                Body = Body,
                Address = GetLocalEndpoint()
            };
        }

        /// <summary>
        /// Gets the local endpoint
        /// </summary>
        /// <returns></returns>
        private string GetLocalEndpoint()
        {
            if (!string.IsNullOrEmpty(Interface.uMod?.Web?.Configuration?.PreferredEndpoint))
            {
                return Interface.uMod.Web.Configuration.PreferredEndpoint;
            }

            return Interface.uMod.Server.LocalAddress != null
                ? Interface.uMod.Server.LocalAddress.ToString()
                : Interface.uMod.Server.Address.ToString();
        }

        /// <summary>
        /// Invoke web request
        /// </summary>
        internal virtual Request Invoke()
        {
            _client?.EnqueueRequest(GetData(), this);
            return this;
        }

        /// <summary>
        /// Dispose of Request
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            Url = null;
            Body = null;
            Cookies?.Clear();
            Cookies = null;
            Headers?.Clear();
            Headers = null;
        }
    }
}
