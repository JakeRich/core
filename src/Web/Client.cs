﻿using System;
using System.Collections.Generic;
using uMod.Common;
using uMod.Common.Web;

namespace uMod.Web
{
    public sealed class Client
    {
        private readonly Apps.WebClient _client;
        private readonly IContext _context;

        /// <summary>
        /// Create a plugin web wrapper for the specified plugin
        /// </summary>
        /// <param name="context"></param>
        public Client(IContext context = null)
        {
            _client = Interface.uMod.Web.Application;
            _context = context;
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Request(string method, string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            WebRequestMethod requestMethod = (WebRequestMethod)Enum.Parse(typeof(WebRequestMethod), method);
            return Request(requestMethod, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create web request
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Request(WebRequestMethod method, string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            Request request = Pooling.Pools.Requests.Get().Reset(_client, _context);

            request.Method = method;
            request.Url = url;
            request.Headers = headers;
            request.Timeout = timeout;
            request.Body = body;
            request.Cookies = cookies;

            return request.Invoke();
        }

        /// <summary>
        /// Create GET web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Get(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.GET, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create POST web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Post(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.POST, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create PUT web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Put(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.PUT, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create DELETE web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Delete(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.DELETE, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create PATCH web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="body"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Patch(string url, IDictionary<string, string> headers = null, string body = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.PATCH, url, headers, body, cookies, timeout);
        }

        /// <summary>
        /// Create HEAD web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="cookies"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Request Head(string url, IDictionary<string, string> headers = null, IDictionary<string, string> cookies = null, float timeout = 0f)
        {
            return Request(WebRequestMethod.HEAD, url, headers, null, cookies, timeout);
        }
    }
}
