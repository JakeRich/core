﻿using uMod.Common;

namespace uMod.Logging
{
    [Logger("native")]
    public class NativeLogger : TextLogger
    {
        private readonly NativeDebugCallback _callback;

        /// <summary>
        /// Creates a new instance of the NativeLogger class
        /// </summary>
        public NativeLogger() : base(true)
        {
            _callback = Interface.uMod.debugCallback;
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message)
        {
            if (message.Level > LogLevel)
            {
                return;
            }
            _callback?.Invoke(Format.Interpolate(message));
        }
    }
}
