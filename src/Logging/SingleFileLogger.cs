﻿using System.IO;
using uMod.Common;
using uMod.IO;

namespace uMod.Logging
{
    /// <summary>
    /// A logger that writes to a single file
    /// </summary>
    [Logger("single")]
    public class SingleFileLogger : ThreadedTextLogger, IFileLogger
    {
        /// <summary>
        /// Gets the directory to write log files to
        /// </summary>
        public string Directory { get; protected set; }

        // The active writer
        private StreamWriter _writer;

        public string Path { get; internal set; }

        /// <summary>
        /// Creates a new instance of the SingleFileLogger class
        /// </summary>
        public SingleFileLogger()
        {
            Directory = Interface.uMod.LogDirectory;

            OnConfigure.Add(delegate
            {
                string directory = Utility.GetDirectoryName(GetLogFilename());
                PathUtils.AssertValid(directory);
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
            });
        }

        /// <summary>
        /// Gets the filename for the specified date
        /// </summary>
        /// <returns></returns>
        private string GetLogFilename() => System.IO.Path.Combine(Directory, Path ?? "umod.log");

        /// <summary>
        /// Begins a batch process operation
        /// </summary>
        protected override void BeginBatchProcess()
        {
            // Open the writer
            _writer = new StreamWriter(new FileStream(PathUtils.AssertValid(GetLogFilename()), FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message)
        {
            _writer.WriteLine(Format.Interpolate(message));
        }

        /// <summary>
        /// Finishes a batch process operation
        /// </summary>
        protected override void FinishBatchProcess()
        {
            // Close the writer
            _writer?.Close();
            _writer?.Dispose();
            _writer = null;
        }
    }
}
