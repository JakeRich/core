﻿using uMod.Common;

namespace uMod.Logging
{
    /// <summary>
    /// A single file plugin logger
    /// </summary>
    [Logger("single_plugin")]
    public class SinglePluginLogger : SingleFileLogger, IPluginLogger
    {
        /// <summary>
        /// Gets the plugin
        /// </summary>
        public IPlugin Plugin { get; }

        /// <summary>
        /// Create a new single plugin logger object
        /// </summary>
        /// <param name="plugin"></param>
        public SinglePluginLogger(IPlugin plugin) : this(plugin, null)
        {
            plugin.OnRemovedFromManager.Add(delegate
            {
                Shutdown();
            });
        }

        /// <summary>
        /// Creates a new instance of the SinglePluginLogger class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="filename"></param>
        public SinglePluginLogger(IPlugin plugin, string filename = null)
        {
            Plugin = plugin;
            Format = Interface.uMod.Logging.Format;

            string extension = ".log";

            if (string.IsNullOrEmpty(filename))
            {
                Path = $"{plugin.Name.ToLower()}{extension}";
            }
            else
            {
                extension = System.IO.Path.GetExtension(filename);

                if (string.IsNullOrEmpty(extension))
                {
                    extension = ".log";
                }
                else
                {
                    filename = System.IO.Path.GetFileNameWithoutExtension(filename);
                }

                Path = $"{filename.ToLower()}{extension}";
            }

            plugin.OnAddedToManager.Add(delegate
            {
                OnAdded?.Invoke(this);
            });

            plugin.OnRemovedFromManager.Add(delegate
            {
                OnRemoved?.Invoke(this);
            });

            OnConfigure?.Invoke(this);
        }
    }
}
