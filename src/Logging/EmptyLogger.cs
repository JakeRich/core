﻿using System;
using uMod.Common;

namespace uMod.Logging
{
    /// <summary>
    /// Empty logger which discards log messages
    /// </summary>
    public class EmptyLogger : ILogger
    {
        public LogLevel LogLevel { get; } = LogLevel.Emergency;

        public IEvent<ILogger> OnConfigure { get; }

        public IEvent<ILogger> OnAdded { get; }

        public IEvent<ILogger> OnRemoved { get; }

        public void Alert(string message)
        {
        }

        public void Critical(string message)
        {
        }

        public void Debug(string message)
        {
        }

        public void Emergency(string message)
        {
        }

        public void Error(string message)
        {
        }

        public void Info(string message)
        {
        }

        public void Notice(string message)
        {
        }

        public void Report(string message, Exception exception)
        {
        }

        public void Report(Exception exception)
        {
        }

        public void Warning(string message)
        {
        }

        public void Write(LogLevel type, string message)
        {
        }

        public void Write(LogMessage message)
        {
        }
    }
}
