﻿using System;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.Logging
{
    /// <summary>
    /// Represents a set of loggers that combine info a single logger
    /// </summary>
    [Logger("stack")]
    public sealed class StackLogger : Logger
    {
        // Loggers under this stack logger
        private readonly HashSet<ILogger> _subLoggers;

        /// <summary>
        /// Creates a new instance of the StackLogger class
        /// </summary>
        public StackLogger() : base(true)
        {
            // Initialize
            _subLoggers = new HashSet<ILogger>();
            OnRemoved.Add(delegate
            {
                foreach (ILogger subLogger in _subLoggers)
                {
                    subLogger.OnRemoved?.Invoke(subLogger);
                }
                _subLoggers.Clear();
            });
        }

        /// <summary>
        /// Adds logger to stack logger
        /// </summary>
        /// <param name="logger"></param>
        public void AddLogger(ILogger logger)
        {
            // Register it
            _subLoggers.Add(logger);
            logger.OnAdded?.Invoke(logger);
        }

        /// <summary>
        /// Adds multiple loggers to stack logger
        /// </summary>
        /// <param name="loggers"></param>
        public void AddLogger(IEnumerable<ILogger> loggers)
        {
            foreach (ILogger logger in loggers)
            {
                AddLogger(logger);
            }
        }

        /// <summary>
        /// Removes a logger from stack logger
        /// </summary>
        /// <param name="logger"></param>
        public void RemoveLogger(ILogger logger)
        {
            // Unregister it
            logger.OnRemoved?.Invoke(logger);
            _subLoggers.Remove(logger);
        }

        /// <summary>
        /// Gets loggers in the stack logger
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ILogger> GetLoggers()
        {
            return _subLoggers;
        }

        /// <summary>
        /// Writes a message to all sub-loggers of this logger
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public override void Write(LogLevel level, string message)
        {
            if (level > LogLevel)
            {
                return;
            }

            // Write to all current sub-loggers
            foreach (ILogger logger in _subLoggers)
            {
                logger.Write(level, message);
            }
        }

        /// <summary>
        /// Writes a message to all the sub-loggers of this logger
        /// </summary>
        /// <param name="message"></param>
        public override void Write(LogMessage message)
        {
            // Write to all current sub-loggers
            foreach (ILogger logger in _subLoggers)
            {
                logger.Write(message);
            }
        }

        public override void Report(Exception ex)
        {
            LogMessage reportMessage = CreateReportMessage(string.Empty, ex);
            // Write to all current sub-loggers
            foreach (ILogger logger in _subLoggers)
            {
                if (logger is TextLogger)
                {
                    // Avoid creating multiple report messages for every sub-logger (if sub-logger is text logger)
                    logger.Write(reportMessage);
                }
                else
                {
                    // Not a text-logger (may be service, pass entire exception)
                    logger.Report(ex);
                }
            }
        }

        public override void Report(string message, Exception ex)
        {
            LogMessage reportMessage = CreateReportMessage(message, ex);
            // Write to all current sub-loggers
            foreach (ILogger logger in _subLoggers)
            {
                if (logger is TextLogger)
                {
                    // Avoids creating multiple report messages for every sub-logger (if sub-logger is text logger)
                    logger.Write(reportMessage);
                }
                else
                {
                    // Not a text-logger (may be service, pass entire exception)
                    logger.Report(message, ex);
                }
            }
        }
    }
}
