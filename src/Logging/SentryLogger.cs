﻿extern alias References;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.Pooling;
using uMod.Web;

namespace uMod.Logging
{
    /// <summary>
    /// Represents a logger that sends exceptions to a remote sentry service
    /// </summary>
    [Logger("sentry")]
    public class SentryLogger : Logger
    {
        #region Sentry Data

        private class SentryStackFrame
        {
            [JsonProperty("filename")]
            public string FileName;

            [JsonProperty("function")]
            public string Function;

            [JsonProperty("module")]
            public string Module;

            [JsonProperty("package")]
            public string Package;
        }

        private class SentryStackTrace : IDisposable
        {
            [JsonProperty("frames")]
            public List<SentryStackFrame> Frames;

            public SentryStackTrace()
            {
            }

            public SentryStackTrace(SentryEvent @event, Exception exception)
            {
                Reset(@event, exception);
            }

            public SentryStackTrace Reset(SentryEvent @event, Exception exception)
            {
                StackTrace stackTrace = new StackTrace(exception);
                if (stackTrace.FrameCount == 0)
                {
                    return this;
                }

                Frames = framePool.Get<List<SentryStackFrame>>();
                Populate(@event, stackTrace.GetFrames());

                if (exception.InnerException != null)
                {
                    stackTrace = new StackTrace(exception.InnerException);
                    if (stackTrace.FrameCount == 0)
                    {
                        return this;
                    }

                    Populate(@event, stackTrace.GetFrames());
                }

                return this;
            }

            private void Populate(SentryEvent @event, StackFrame[] frames)
            {
                bool isPlugin = false;
                int count = 0;
                foreach (StackFrame frame in frames)
                {
                    SentryStackFrame stackFrame = new SentryStackFrame();
                    if (frame.GetMethod() is MethodBase methodBase)
                    {
                        if (!isPlugin && typeof(IPlugin).IsAssignableFrom(methodBase.DeclaringType))
                        {
                            if (Interface.uMod.Plugins.Manager.GetPlugin(methodBase.DeclaringType.Name) is IPlugin plugin && !@event.Modules.ContainsKey(methodBase.DeclaringType.Name))
                            {
                                @event.Modules.Add(methodBase.DeclaringType.Name, plugin.Version.ToString());
                            }

                            isPlugin = true;
                        }
                        string key = $"{methodBase.DeclaringType?.FullName}.{methodBase.Name}";
                        // Skip stack trace frames if not the first frame
                        if (count > 0 && StackTraceFilter.Contains(key))
                        {
                            continue;
                        }
                        stackFrame.Module = methodBase.DeclaringType?.FullName ?? "(unknown)";
                        stackFrame.Package = methodBase.DeclaringType?.Assembly.FullName;
                        stackFrame.Function = methodBase.Name;
                    }

                    stackFrame.FileName = frame.GetFileName();

                    // Simulate stack by pushing frame to top of list
                    Frames.Insert(0, stackFrame);
                    count++;
                }
            }

            public void Dispose()
            {
                if (Frames != null)
                {
                    framePool.Free(Frames);
                }
            }
        }

        private class SentryException : IDisposable
        {
            [JsonProperty("type")]
            public string Type;

            [JsonProperty("value")]
            public string Value;

            [JsonProperty("module")]
            public string Module;

            [JsonProperty("stacktrace")]
            public SentryStackTrace Stacktrace;

            public SentryException()
            {
            }

            public SentryException(SentryEvent @event, Exception exception)
            {
                Reset(@event, exception);
            }

            public SentryException Reset(SentryEvent @event, Exception exception)
            {
                Type = exception.GetType()?.FullName;
                Module = exception.GetType()?.Assembly?.FullName;
                Value = exception.Message;
                Stacktrace = tracePool.Get<SentryStackTrace>().Reset(@event, exception);
                return this;
            }

            public void Dispose()
            {
                tracePool.Free(Stacktrace);
            }
        }

        private class SentryEvent : IDisposable
        {
            [JsonProperty("event_id")]
            public string Id = Guid.NewGuid().ToString();

            [JsonProperty("timestamp")]
            public DateTimeOffset Timestamp = DateTime.UtcNow;

            [JsonProperty("level")]
            public string Level = "error";

            [JsonProperty("platform")]
            public string Platform = "csharp";

            [JsonProperty("message")]
            public string Message;

            [JsonProperty("exception")]
            public SentryException Exception;

            [JsonProperty("release")]
            public string Release = Module.Version.ToString();

            [JsonProperty("tags")]
            public Dictionary<string, string> Tags;

            [JsonProperty("modules")]
            public Dictionary<string, string> Modules;

            public SentryEvent()
            {
            }

            public SentryEvent(string message = null, Exception exception = null)
            {
                Reset(message, exception);
            }

            public SentryEvent Reset(string message = null, Exception exception = null)
            {
                Modules = dictionaryPool.Get<Dictionary<string, string>>();
                Tags = DefaultTags;

                if (!string.IsNullOrEmpty(message))
                {
                    Message = message.Length > 1000 ? message.Substring(0, 1000) : message;
                }

                if (exception != null)
                {
                    Exception = exceptionPool.Get<SentryException>().Reset(this, exception);
                }

                return this;
            }

            public void Dispose()
            {
                if (Exception != null)
                {
                    exceptionPool.Free(Exception);
                }

                dictionaryPool.Free(Modules);
            }
        }

        #endregion Sentry Data

        public static string Filename = Utility.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.FileName);

        private static readonly Dictionary<string, string> DefaultTags = new Dictionary<string, string>
        {
            { "arch", IntPtr.Size == 8 ? "x64" : "x86" },
            { "platform", Environment.OSVersion.Platform.ToString().ToLower() },
            { "os version", Environment.OSVersion.Version.ToString().ToLower() },
            { "game", Filename.ToLower().Replace("dedicated", "").Replace("server", "").Replace("-", "").Replace("_", "") }
        };

        private static readonly List<string> StackTraceFilter = new List<string>()
        {
            "uMod.Plugins.HookMethod._Invoke",
            "uMod.Plugins.HookDispatcher.Dispatch"
        };

        private readonly Dictionary<string, string> _sentryHeaders = new Dictionary<string, string>()
        {
            {"Content-Type", "application/json"}
        };

        public static string[] MessageFilter =
        {
            "Error while compiling"
        };

        public static string[] ExceptionFilter =
        {
            "BadImageFormatException",
            "DllNotFoundException",
            "FileNotFoundException",
            "IOException",
            "KeyNotFoundException",
            "uMod.Configuration",
            "uMod.Ext.",
            "uMod.Plugins.<",
            "ReflectionTypeLoadException",
            "Sharing violation",
            "UnauthorizedAccessException",
            "WebException"
        };

        private readonly Dictionary<string, DateTime> _exceptionChecksums = new Dictionary<string, DateTime>();
        private readonly object checksumLock = new object();
        private readonly Client _client;
        private string _storePath;
        private string _sentryUri = "https://sentry.io";
        private string _projectId;
        private string _sentryKey;

        protected static DynamicPool eventPool;
        protected static DynamicPool exceptionPool;
        protected static DynamicPool tracePool;
        protected static DynamicPool framePool;
        protected static DynamicPool dictionaryPool;

        public SentryLogger() : base(true)
        {
            _client = new Client();
            eventPool = Pools.Disposable<SentryEvent>();
            exceptionPool = Pools.Disposable<SentryException>();
            tracePool = Pools.Disposable<SentryStackTrace>();
            framePool = Pools.List<List<SentryStackFrame>>();
            dictionaryPool = Pools.Dictionary<string, string>();
        }

        public static void SetTag(string name, string value) => DefaultTags[name] = value;

        public static string GetTag(string name)
        {
            return DefaultTags.TryGetValue(name, out string value) ? value : "unknown";
        }

        public void Configure(string uri, string projectId, string key)
        {
            _sentryUri = uri;
            _projectId = projectId;
            _sentryKey = key;
            _storePath = $"{_sentryUri}/api/{_projectId}/store/?sentry_key={_sentryKey}";
        }

        public override void Write(LogLevel level, string message)
        {
            if (level == LogLevel.Error)
            {
                SendSentryEvent(message);
            }
        }

        public override void Write(LogMessage message)
        {
            if (message.Level == LogLevel.Error)
            {
                SendSentryEvent(message.Message);
            }
        }

        public override void Report(Exception ex)
        {
            SendSentryEvent(null, ex);
        }

        public override void Report(string message, Exception ex)
        {
            SendSentryEvent(message, ex);
        }

        private void SendSentryEvent(string message = null, Exception exception = null)
        {
            if (exception?.StackTrace != null)
            {
                if (exception.StackTrace.Contains("uMod") || exception.StackTrace.Contains("uMod.Plugins.Compiler"))
                {
                    foreach (string filter in ExceptionFilter)
                    {
                        if (exception.StackTrace.Contains(filter))
                        {
                            return;
                        }
                    }
                }
            }

            if (message != null)
            {
                foreach (string filter in MessageFilter)
                {
                    if (message.Contains(filter))
                    {
                        return;
                    }
                }
            }

            string hash;
            if (exception != null)
            {
                hash = exception.ToString().ToHash();
            }
            else if (message != null)
            {
                hash = message.ToHash();
            }
            else
            {
                return;
            }

            lock (checksumLock)
            {
                // If we have a lot of exceptions in checksum dictionary, clear it (avoid memory leak)
                if (_exceptionChecksums.Count > 100)
                {
                    _exceptionChecksums.Clear();
                }

                if (!_exceptionChecksums.TryGetValue(hash, out DateTime lastExceptionTime))
                {
                    _exceptionChecksums.Add(hash, lastExceptionTime = DateTime.Now);
                }
                else if (DateTime.Now.Subtract(lastExceptionTime).TotalSeconds < 30)
                {
                    // Do not send same exception if occured less than X seconds ago
                    return;
                }
                else
                {
                    _exceptionChecksums[hash] = DateTime.Now;
                }
            }

            SentryEvent sentryEvent = eventPool.Get<SentryEvent>().Reset(message, exception);

            if (!Interface.uMod.IsTesting)
            {
                _client.Post(_storePath, _sentryHeaders,
                    JsonConvert.SerializeObject(new SentryEvent(message, exception))).Done(
                    delegate () { eventPool.Free(sentryEvent); },
                    delegate { eventPool.Free(sentryEvent); });
            }
            else
            {
                eventPool.Free(sentryEvent);
            }
        }
    }
}
