﻿using System;
using uMod.Common;

namespace uMod.Logging
{
    /// <summary>
    /// Cached logger to temporarily store log messages
    /// </summary>
    [Logger("pipe")]
    public class PipeLogger : Logger
    {
        private readonly string _prefix;
        protected ILogger TargetLogger;

        /// <summary>
        /// Creates a new instance of the PipeLogger class
        /// </summary>
        public PipeLogger() : base(false)
        {
        }

        /// <summary>
        /// Creates a new instance of the PipeLogger class with the specified target logger
        /// </summary>
        /// <param name="targetLogger"></param>
        /// <param name="prefix"></param>
        public PipeLogger(ILogger targetLogger, string prefix = null) : this()
        {
            TargetLogger = targetLogger;
            _prefix = prefix;
        }

        /// <summary>
        /// Gets the logger that this one pipes to
        /// </summary>
        /// <returns></returns>
        public ILogger GetTargetLogger()
        {
            return TargetLogger;
        }

        /// <summary>
        /// Pipe message to target logger or cache for later
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public override void Write(LogLevel level, string message)
        {
            if (!string.IsNullOrEmpty(_prefix))
            {
                message = $"{_prefix}{message}";
            }
            if (TargetLogger != null)
            {
                TargetLogger.Write(level, message);
                return;
            }

            base.Write(level, message);
        }

        /// <summary>
        /// Pipe message to target logger or cache for later
        /// </summary>
        /// <param name="message"></param>
        public override void Write(LogMessage message)
        {
            if (!string.IsNullOrEmpty(_prefix))
            {
                message.Message = $"{_prefix}{message.Message}";
            }

            if (TargetLogger != null)
            {
                TargetLogger.Write(message);
                return;
            }

            base.Write(message);
        }

        /// <summary>
        /// Pipes all queued messages to another logger
        /// </summary>
        /// <param name="target"></param>
        public void Pipe(ILogger target)
        {
            TargetLogger = target;
            try
            {
                do
                {
                    LogMessage message = MessageQueue.Dequeue();
                    target.Write(message);
                } while (MessageQueue.Count != 0);
            }
            catch (Exception)
            {
            }
        }
    }
}
