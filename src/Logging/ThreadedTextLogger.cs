﻿using System.Threading;
using uMod.Common;

namespace uMod.Logging
{
    /// <summary>
    /// Represents a logger that processes messages on a worker thread
    /// </summary>
    public abstract class ThreadedTextLogger : TextLogger
    {
        // Sync mechanisms
        private readonly AutoResetEvent _waitEvent;
        private readonly object _syncRoot;
        private bool _exit;

        // The worker thread
        private Thread _workerThread;

        /// <summary>
        /// Creates a new instance of the ThreadedLogger class
        /// </summary>
        protected ThreadedTextLogger(bool startImmediate = true) : base(false)
        {
            // Initialize
            _waitEvent = new AutoResetEvent(false);
            _exit = false;
            _syncRoot = new object();

            // Create the thread
            OnAdded.Add(delegate
            {
                if (_exit)
                {
                    return;
                }

                _workerThread = new Thread(Worker) { IsBackground = true };
                _workerThread.Start();
            });
        }

        ~ThreadedTextLogger()
        {
            Shutdown();
        }

        internal void Shutdown()
        {
            if (_exit)
            {
                return;
            }

            _exit = true;
            _waitEvent.Set();
            _workerThread?.Join();
        }

        /// <summary>
        /// Writes a message to the current logfile
        /// </summary>
        /// <param name="message"></param>
        public override void Write(LogMessage message)
        {
            if (message.Level > LogLevel)
            {
                return;
            }
            lock (_syncRoot)
            {
                base.Write(message);
            }

            _waitEvent.Set();
        }

        /// <summary>
        /// Begins a batch process operation
        /// </summary>
        protected abstract void BeginBatchProcess();

        /// <summary>
        /// Finishes a batch process operation
        /// </summary>
        protected abstract void FinishBatchProcess();

        /// <summary>
        /// The worker thread
        /// </summary>
        private void Worker()
        {
            // Loop until it is time to exit
            while (!_exit)
            {
                // Wait for signal
                _waitEvent.WaitOne();

                // Iterate each item in the queue
                lock (_syncRoot)
                {
                    if (MessageQueue.Count <= 0)
                    {
                        continue;
                    }

                    BeginBatchProcess();
                    try
                    {
                        while (MessageQueue.Count > 0)
                        {
                            // Dequeue
                            LogMessage message = MessageQueue.Dequeue();

                            // Process
                            ProcessMessage(message);
                        }
                    }
                    finally
                    {
                        FinishBatchProcess();
                    }
                }
            }

            FinishBatchProcess();
        }
    }
}
