﻿namespace uMod.Logging
{
    /// <summary>
    /// A logger that writes text
    /// </summary>
    [Logger("text")]
    public abstract class TextLogger : Logger
    {
        public string Format { get; internal set; } = "[uMod] {date|h:mm tt} [{level}] {message}";

        /// <summary>
        /// Creates a new instance of the TextLogger class
        /// </summary>
        protected TextLogger(bool processImmediately) : base(processImmediately)
        {
        }
    }
}
