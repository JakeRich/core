using System;
using System.IO;
using uMod.Common;
using uMod.IO;

namespace uMod.Logging
{
    /// <summary>
    /// A logger that writes to a set of files that rotate by day
    /// </summary>
    [Logger("daily")]
    public class DailyFileLogger : ThreadedTextLogger, IFileLogger
    {
        /// <summary>
        /// Gets the directory to write log files to
        /// </summary>
        public string Directory { get; protected set; }

        public string Path { get; internal set; }

        // The active writer
        private StreamWriter _writer;

        /// <summary>
        /// Creates a new instance of the DailyFileLogger class
        /// </summary>
        public DailyFileLogger()
        {
            Directory = Interface.uMod.LogDirectory;

            OnConfigure.Add(delegate
            {
                if (string.IsNullOrEmpty(Path))
                {
                    Path = "umod_{date|yyyy-MM-dd}.log";
                }

                PathUtils.AssertValid(Directory);
            });
        }

        /// <summary>
        /// Gets the filename for the specified date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string GetLogFilename(DateTime date)
        {
            string path = PathUtils.AssertValid(System.IO.Path.Combine(Directory, Path?.Interpolate("date", date) ?? $"umod_{date:yyyy-MM-dd}.log"));
            string directory = Utility.GetDirectoryName(path);
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }

            if (string.IsNullOrEmpty(path))
            {
                return path;
            }

            return path;
        }

        /// <summary>
        /// Begins a batch process operation
        /// </summary>
        protected override void BeginBatchProcess()
        {
            // Open the writer
            _writer = new StreamWriter(new FileStream(GetLogFilename(DateTime.Now), FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message)
        {
            _writer.WriteLine(Format.Interpolate(message));
        }

        /// <summary>
        /// Finishes a batch process operation
        /// </summary>
        protected override void FinishBatchProcess()
        {
            // Close the writer
            _writer?.Close();
            _writer?.Dispose();
            _writer = null;
        }
    }
}
