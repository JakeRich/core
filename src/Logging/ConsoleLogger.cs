﻿using System;
using uMod.Common;

namespace uMod.Logging
{
    /// <summary>
    /// A logger that write to the system console
    /// </summary>
    [Logger("console")]
    public class ConsoleLogger : TextLogger
    {
        /// <summary>
        /// Creates a new instance of the ConsoleLogger class
        /// </summary>
        public ConsoleLogger() : base(true)
        {
        }

        /// <summary>
        /// Processes the specified message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message)
        {
            if (message.Level > LogLevel)
            {
                return;
            }

            Console.WriteLine(Format.Interpolate(message));
        }
    }
}
