﻿using uMod.Common;

namespace uMod.Logging
{
    /// <summary>
    /// A daily file plugin logger
    /// </summary>
    [Logger("daily_plugin")]
    public class DailyPluginLogger : DailyFileLogger, IPluginLogger
    {
        /// <summary>
        /// Gets the plugin
        /// </summary>
        public IPlugin Plugin { get; }

        /// <summary>
        /// Create a new daily plugin logger object
        /// </summary>
        /// <param name="plugin"></param>
        public DailyPluginLogger(IPlugin plugin) : this(plugin, null)
        {
            plugin.OnRemovedFromManager.Add(delegate
            {
                Shutdown();
            });
        }

        /// <summary>
        /// Creates a new instance of the DailyPluginLogger class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="filename"></param>
        public DailyPluginLogger(IPlugin plugin, string filename = null)
        {
            Plugin = plugin;
            Format = Interface.uMod.Logging.Format;

            string extension = ".log";

            if (string.IsNullOrEmpty(filename))
            {
                Path = $"{plugin.Name}_{{date|yyyy-MM-dd}}{extension}";
            }
            else
            {
                extension = System.IO.Path.GetExtension(filename);

                if (string.IsNullOrEmpty(extension))
                {
                    extension = ".log";
                }
                else
                {
                    filename = System.IO.Path.GetFileNameWithoutExtension(filename);
                }

                Path = $"{filename}{extension}";
            }

            plugin.OnAddedToManager.Add(delegate
            {
                OnAdded?.Invoke(this);
            });

            plugin.OnRemovedFromManager.Add(delegate
            {
                OnRemoved?.Invoke(this);
            });

            OnConfigure?.Invoke(this);
        }
    }
}
