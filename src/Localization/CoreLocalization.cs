﻿using uMod.Common;
using uMod.Configuration.Toml;
using uMod.Plugins;

namespace uMod.Localization
{
    #region Default English Localization

    [Toml]
    public class CoreLocalization : ILocale
    {
        [TomlProperty("group")]
        public GroupLocalization Group = new GroupLocalization();

        [TomlProperty("player_group")]
        public PlayerGroupLocalization PlayerGroup = new PlayerGroupLocalization();

        [TomlProperty("permission")]
        public PermissionLocalization Permission = new PermissionLocalization();

        [TomlProperty("player_permission")]
        public PlayerPermissionLocalization PlayerPermission = new PlayerPermissionLocalization();

        [TomlProperty("database")]
        public DatabaseLocalization Database = new DatabaseLocalization();

        [TomlProperty("compiler")]
        public CompilerLocalization Compiler = new CompilerLocalization();

        [TomlProperty("webclient")]
        public WebClientLocalization WebClient = new WebClientLocalization();

        [TomlProperty("data")]
        public DataLocalization Data = new DataLocalization();

        [TomlProperty("plugin")]
        public PluginLocalization Plugin = new PluginLocalization();

        [TomlProperty("plugin_loader")]
        public PluginLoaderLocalization PluginLoader = new PluginLoaderLocalization();

        [TomlProperty("player")]
        public PlayerLocalization Player = new PlayerLocalization();

        [TomlProperty("server")]
        public ServerLocalization Server = new ServerLocalization();

        [TomlProperty("module")]
        public ModuleLocalization Module = new ModuleLocalization();

        [TomlProperty("command")]
        public CommandLocalization Command = new CommandLocalization();

        [TomlProperty("command_usage")]
        public CommandUsageLocalization CommandUsage = new CommandUsageLocalization();

        [TomlProperty("exceptions")]
        public ExceptionLocalization Exceptions = new ExceptionLocalization();

        [TomlProperty("apps")]
        public AppLocalization Apps = new AppLocalization();

        [TomlProperty("validation")]
        public ValidationLocalization Validation = new ValidationLocalization();

        [TomlProperty("locale")]
        public LocaleLocalization Locale = new LocaleLocalization();
    }

    public class LocaleLocalization
    {
        [TomlProperty("not_found_of_type")]
        public string NotFoundOfType = "Unable to find locale of type {locale} for {context} ({lang})";

        [TomlProperty("not_found_reference")]
        public string NotFoundReference = "Unable to find locale reference for type {locale} for {context} ({lang})";
    }

    public class ValidationLocalization
    {
        [TomlProperty("not_null")]
        public string NotNull = "Cannot be null";

        [TomlProperty("not_null_or_empty")]
        public string NotNullOrEmpty = "Cannot be null or empty";
    }

    public class AppLocalization
    {
        [TomlProperty("start_failure")]
        public string StartFailure = "{app} {version} could not be started";

        [TomlProperty("start_exception")]
        public string StartException = "Exception while starting {app} {version}";

        [TomlProperty("closed_unexpected")]
        public string ClosedUnexpected = "{app} {version} was closed unexpectedly";

        [TomlProperty("error")]
        public string Error = "{app} error";

        [TomlProperty("disconnected")]
        public string Disconnected = "{app} disconnected message was null";

        [TomlProperty("installing")]
        public string Installing = "Installing uMod apps...";

        [TomlProperty("install_failure")]
        public string InstallFailure = "Unable to install applications {reason}";

        [TomlProperty("validation_failure")]
        public string ValidationFailure = "Failed to validate app installation";

        [TomlProperty("start_without")]
        public string StartWithout = "Starting without {app}...";
    }

    public class CompilerLocalization
    {
        [TomlProperty("awaiting_type")]
        public string AwaitingType = "{name} awaiting type: {type}";

        [TomlProperty("compiled_unknown_assembly")]
        public string CompiledUnknownAssembly = "Compiler compiled an unknown assembly";

        [TomlProperty("premature_load")]
        public string PrematureLoad = "Load called before a compiled assembly exists: {plugin}";

        [TomlProperty("compile_assembly_failure")]
        public string CompiledAssemblyFailure = "Unable to load '{script}'. {errors}";

        [TomlProperty("missing_plugin_class")]
        public string MissingPluginClass = "Unable to find main plugin class: {plugin}";

        [TomlProperty("invalid_plugin_constructor")]
        public string InvalidPluginConstructor = "Main plugin class should not have a constructor defined: {plugin}";

        [TomlProperty("invalid_filename")]
        public string InvalidFileName = "Plugin filename {script}.cs must match the main class {class} (should be {class}.cs)";

        [TomlProperty("assembly_load_failure")]
        public string AssemblyLoadFailure = "Plugin assembly failed to load: {script}";

        [TomlProperty("annotation_missing")]
        public string AnnotationMissing = "Plugin missing necessary annotations: {script}";

        [TomlProperty("rollback_failure_missing")]
        public string RollbackFailureMissing = "No previous version to rollback plugin: {script}";

        [TomlProperty("rollback_failure_load")]
        public string RollbackFailureLoad = "Failed to load previous working version of plugin '{plugin}' while rolling back";

        [TomlProperty("rollback")]
        public string Rollback = "Rolling back plugin to last good version: {script}";

        [TomlProperty("already_loading")]
        public string AlreadyLoading = "Load requested for plugin which is already loading: {plugin}";

        [TomlProperty("reload_references")]
        public string ReloadingReferences = "Reloading {plugin} because it references updated include file: {reference}";

        [TomlProperty("requirements_pending")]
        public string RequirementsPending = "Plugin '{plugin}' is waiting for requirements to be loaded: {requirements}";

        [TomlProperty("dependency")]
        public string Dependency = "dependency|dependencies";

        [TomlProperty("requirements_missing_log")]
        public string RequirementsMissingLog = "Plugin '{plugin}' requires missing {dependency}: {requirements}";

        [TomlProperty("requirements_missing")]
        public string RequirementsMissing = "Missing {dependency}: {requirements}";

        [TomlProperty("compile_paused")]
        public string CompilePaused = "Compiling paused: {warnings}";

        [TomlProperty("compile_warnings")]
        public string CompileWarnings = "Warnings while compiling: {warnings}";

        [TomlProperty("compile_failure")]
        public string CompileFailure = "Failed to compile: {errors}";

        [TomlProperty("compile_errors")]
        public string CompileErrors = "Error while compiling {plugin}: {errors}";

        [TomlProperty("compile_success")]
        public string CompileSuccess = "{plugins} was compiled successfully in {duration}ms|{plugins} were compiled successfully in {duration}ms";

        [TomlProperty("script_empty")]
        public string ScriptEmpty = "Plugin script is empty: {plugin}";

        [TomlProperty("reference_resolve_failure")]
        public string ReferenceResolveFailure = "Exception while resolving plugin references";

        [TomlProperty("oxide_name_warning")]
        public string OxideNameWarning = "Plugin {plugin} is using Oxide naming, please update to uMod naming";

        [TomlProperty("reference_added")]
        public string ReferenceAdded = "Added '// Reference: {reference}' in plugin '{plugin}'";

        [TomlProperty("reference_ignored")]
        public string ReferenceIgnored = "Ignored unnecessary '// Reference: {reference}' in plugin '{plugin}'";

        [TomlProperty("reference_not_found")]
        public string ReferenceNotFound = "{reference} is referenced by {plugin} plugin but is not loaded!";

        [TomlProperty("reference_assembly_not_found")]
        public string ReferenceAssemblyNotFound = "Could not find assembly '{assembly}' referenced by plugin '{plugin}'";

        [TomlProperty("reference_assembly_invalid_log")]
        public string ReferenceAssemblyInvalid = "Assembly '{assembly}' referenced by plugin '{plugin}' is invalid:";

        [TomlProperty("reference_assembly_dll_not_found")]
        public string ReferenceAssemblyDllNotFound = "Reference {reference}.dll from {assembly}.dll not found";

        [TomlProperty("script_not_found_log")]
        public string ScriptNotFoundLog = "Compilation script no longer exists: {plugin}";

        [TomlProperty("script_not_found")]
        public string ScriptNotFound = "Plugin file was deleted";

        [TomlProperty("access_exception")]
        public string AccessException = "Waiting for another application to stop using script: {plugin}";

        [TomlProperty("invalid_script_error")]
        public string InvalidScriptError = "Unable to resolve script error to plugin: {line}";

        [TomlProperty("invalid_script_warning")]
        public string InvalidScriptWarning = "Unable to resolve script warning to plugin: {line}";

        [TomlProperty("compilation_error")]
        public string CompilationError = "Compilation error";
    }

    public class WebClientLocalization
    {
        [TomlProperty("invalid_response")]
        public string InvalidResponse = "Web response to unknown request";

        [TomlProperty("invalid_error")]
        public string InvalidError = "Web error to unknown request";
    }

    public class DatabaseLocalization
    {
        [TomlProperty("connection_rejected")]
        public string ConnectionRejected = "Connection was rejected";

        [TomlProperty("migration_failure")]
        public string AuthenticationMigrationFailure = "Unable to migrate authentication tables";

        [TomlProperty("authentication_connection_failure")]
        public string AuthenticationConnectionFailure = "Unable to connect to default connection, authentication driver switched to protobuf";

        [TomlProperty("connection_failure")]
        public string ConnectionFailure = "Unable to connect to default connection";
    }

    public class GroupLocalization
    {
        [TomlProperty("group")]
        public string Group = "Group|Groups";

        [TomlProperty("group_already_exists")]
        public string GroupAlreadyExists = "Group '{group}' already exists";

        [TomlProperty("group_already_has_permission")]
        public string GroupAlreadyHasPermission = "Group '{group}' already has permission '{permission'}";

        [TomlProperty("group_does_not_have_permission")]
        public string GroupDoesNotHavePermission = "Group '{group}' does not have permission '{permission}'";

        [TomlProperty("group_permission_inherited")]
        public string GroupPermissionInherited =
            "Group '{group}' permission '{permission}' is inherited from '{parentGroup}' group";

        [TomlProperty("group_changed")]
        public string GroupChanged = "Group '{group}' changed";

        [TomlProperty("group_created")]
        public string GroupCreated = "Group '{group}' created";

        [TomlProperty("group_deleted")]
        public string GroupDeleted = "Group '{group}' deleted";

        [TomlProperty("group_not_found")]
        public string GroupNotFound = "Group '{group}' does not exist";

        [TomlProperty("group_parent_changed")]
        public string GroupParentChanged = "Group '{group}' parent changed to '{parent}'";

        [TomlProperty("group_parent_not_changed")]
        public string GroupParentNotChanged = "Group '{group}' parent was not changed";

        [TomlProperty("group_parent_not_found")]
        public string GroupParentNotFound = "Group parent '{group}' does not exist";

        [TomlProperty("group_permission_granted")]
        public string GroupPermissionGranted = "Group '{group}' granted permission '{permission}'";

        [TomlProperty("group_permission_revoked")]
        public string GroupPermissionRevoked = "Group '{group}' revoked permission '{permission}'";

        [TomlProperty("group_permissions")]
        public string GroupPermissions = "Group '{group}' permissions";

        [TomlProperty("group_players")]
        public string GroupPlayers = "Group '{group}' players";

        [TomlProperty("no_group_permissions")]
        public string NoGroupPermissions = "No permissions currently granted";

        [TomlProperty("parent_group_permissions")]
        public string NoPlayerGroups = "No groups with this permission";

        [TomlProperty("no_player_groups")]
        public string NoPlayersInGroup = "No players currently in group";

        [TomlProperty("no_players_in_group")]
        public string ParentGroupPermissions = "Parent group '{group}' permissions";
    }

    public class PlayerGroupLocalization
    {
        [TomlProperty("no_player_groups")]
        public string NoPlayerGroups = "No groups currently granted";

        [TomlProperty("player_added_to_group")]
        public string PlayerAddedToGroup = "Player '{player}' added to group '{group}'";

        [TomlProperty("player_group")]
        public string PlayerGroups = "Player '{player}' groups";

        [TomlProperty("player_removed_from_group")]
        public string PlayerRemovedFromGroup = "Player '{player}' removed from group '{group}'";
    }

    public class PermissionLocalization
    {
        [TomlProperty("no_permission_groups")]
        public string NoPermissionGroups = "No groups with this permission";

        [TomlProperty("no_permission_players")]
        public string NoPermissionPlayers = "No players with this permission";

        [TomlProperty("permission_groups")]
        public string PermissionGroups = "Permission '{permission}' Groups";

        [TomlProperty("permission_players")]
        public string PermissionPlayers = "Permission '{permission}' Players";

        [TomlProperty("permission_not_found")]
        public string PermissionNotFound = "Permission '{permission}' does not exist";

        [TomlProperty("permission")]
        public string Permission = "Permission|Permissions";

        [TomlProperty("permissions_not_loaded")]
        public string PermissionsNotLoaded =
            "Unable to load permission files! Permissions will not work until resolved.\n => {message}";
    }

    public class PlayerPermissionLocalization
    {
        [TomlProperty("no_player_permissions")]
        public string NoPlayerPermissions = "No permissions currently granted";

        [TomlProperty("player_already_has_permission")]
        public string PlayerAlreadyHasPermission = "Player '{player}' already has permission '{permission}'";

        [TomlProperty("player_does_not_have_permission")]
        public string PlayerDoesNotHavePermission = "Player '{player}' does not have permission '{permission}'";

        [TomlProperty("player_permission_inherited")]
        public string PlayerPermissionInherited =
            "Player '{player}' permission '{permission}' is inherited from '{group}' group";

        [TomlProperty("player_permissions")]
        public string PlayerPermissions = "Player '{player}' permissions";

        [TomlProperty("player_permission_granted")]
        public string PlayerPermissionGranted = "Player '{player}' granted permission '{permission}'";

        [TomlProperty("player_permission_revoked")]
        public string PlayerPermissionRevoked = "Player '{player}' revoked permission '{permission}'";
    }

    public class ExceptionLocalization
    {
        [TomlProperty("win32")]
        public string Win32 = "Win32 NativeErrorCode: {NativeErrorCode} ErrorCode: {ErrorCode} HelpLink {HelpLink}";
    }

    public class DataLocalization
    {
        [TomlProperty("data_saved")]
        public string DataSaved = "Saving uMod data...";
    }

    public class PluginLoaderLocalization
    {
        [TomlProperty("plugin_load_failure")]
        public string PluginLoadFailure = "Could not load plugin {plugin}";

        [TomlProperty("core_plugin_load_failure")]
        public string CorePluginLoadFailure = "Could not load core plugin {plugin}";

        [TomlProperty("resolve_failure")]
        public string ResolveFailure = "Unable to resolve plugin {plugin}";

        [TomlProperty("script_not_found")]
        public string ScriptNotFound = "Script no longer exists: {plugin}";

        [TomlProperty("load_failure")]
        public string LoadFailure = "Failed to load plugin {plugin}";

        [TomlProperty("load_timeout")]
        public string LoadTimeout = "Loading plugins timed out: {plugins}";

        [TomlProperty("script_missing")]
        public string ScriptMissing = "Could not load plugin '{plugin}' (no plugin found with that file name)";

        [TomlProperty("script_duplicate")]
        public string ScriptDuplicate = "Could not load plugin '{plugin}' (multiple plugin with that name)";
    }

    public class PluginLocalization
    {
        [TomlProperty("plugin")]
        public string Plugin = "Plugin|Plugins";

        [TomlProperty("listing_header")]
        public string ListingHeader = "Listing {count} {noun}";

        [TomlProperty("title_card")]
        public string TitleCard = "\"{title}\" ({version}) by {author}";

        [TomlProperty("PluginsNotFound")]
        public string PluginsNotFound = "No plugins are currently available";

        [TomlProperty("plugins_not_loaded")]
        public string PluginNotLoaded = "Plugin '{plugin}' not loaded.";

        [TomlProperty("plugin_loaded")]
        public string PluginLoaded = "Loaded plugin {plugin} v{version} by {author}";

        [TomlProperty("plugin_reloaded")]
        public string PluginReloaded = "Reloaded plugin {plugin} v{version} by {author}";

        [TomlProperty("plugin_unloaded")]
        public string PluginUnloaded = "Unloaded plugin {plugin} v{version} by {author}";

        [TomlProperty("unload")]
        public string Unload = "Unload";

        [TomlProperty("unloaded")]
        public string Unloaded = "Unloaded";

        [TomlProperty("messages_not_found")]
        public string MessagesNotFound = "Plugin '{plugin}' is using the Lang API but has no messages registered";

        [TomlProperty("message_not_found")]
        public string MessageNotFound = "Missing lang message: {key}";

        [TomlProperty("unknown_plugin")]
        public string UnknownPlugin = "Unknown plugin";

        [TomlProperty("initialize_failure")]
        public string InitializeFailure = "Failed to initialize plugin '{name} v{version}'";

        [TomlProperty("hook_failure")]
        public string HookFailure = "Failed to call hook '{hook}' on plugin '{name} v{version}'";

        [TomlProperty("hook_conflict")]
        public string HookConflict = "Calling hook {hook} resulted in a conflict between the following plugins: {plugins}";

        [TomlProperty("hook_deprecation")]
        public string HookDeprecation = "'{plugin} v{version}' is using obsolete hook '{oldHook}', which will stop working on {expiration|D}. Please ask the author to update to '{newHook}'";

        [TomlProperty("hook_event_failure")]
        public string HookEventFailure = "Failed to dispatch hook state callback for '{hook}' with state '{state}'";

        [TomlProperty("invalid_version")]
        public string InvalidVersion = "Version `{version}` is invalid for {plugin}, should be `major.minor.patch`";

        [TomlProperty("info_missing")]
        public string InfoMissing = "Failed to load {name}: Info attribute missing";

        [TomlProperty("description_missing")]
        public string DescriptionMissing = "{name}: Description attribute missing";

        [TomlProperty("config_save_failure")]
        public string ConfigSaveFailure = "Failed to save config file (does the config have illegal objects in it?)";

        [TomlProperty("config_load_failure")]
        public string ConfigLoadFailure = "Failed to load config file (is the config file corrupt?)";

        [TomlProperty("metadata_version_invalid")]
        public string MetadataVersionInvalid = "Unable to parse version for {plugin}.{type}: \"{version}\"";

        [TomlProperty("config_types_missing")]
        public string ConfigTypesMissing = "No configuration types available";

        [TomlProperty("locale_types_missing")]
        public string LocaleTypesMissing = "No locale types available";

        [TomlProperty("logger_missing")]
        public string LoggerMissing = "[{plugin}] No logger named \"{name}\"";
    }

    public class PlayerLocalization
    {
        [TomlProperty("player")]
        public string Player = "Player|Players";

        [TomlProperty("not_admin")]
        public string NotAdmin = "You are not an admin";

        [TomlProperty("not_allowed")]
        public string NotAllowed = "You are not allowed to use the '{command}' command";

        [TomlProperty("player_language")]
        public string PlayerLanguage = "Player language set to {language}";

        [TomlProperty("player_language_not_found")]
        public string PlayerLanguageNotFound = "Language {language} not found";

        [TomlProperty("player_not_found")]
        public string PlayerNotFound = "Player '{player}' not found";

        [TomlProperty("players_found")]
        public string PlayersFound = "Multiple players were found, please specify: {playerList}";

        [TomlProperty("missing_player_type")]
        public string MissingPlayerType = "Unable to resolve implied player type in PlayerManager";
    }

    public class ServerLocalization
    {
        [TomlProperty("version")]
        public string Version = "Server is running [#ffb658]uMod {version}[/#] and [#ee715c]{game} {serverVersion} ({protocol})[/#]";

        [TomlProperty("version_log")]
        public string VersionLog = "uMod version {version} running on {game} server version {serverVersion}";

        [TomlProperty("server_language")]
        public string ServerLanguage = "Server language set to {language}";

        [TomlProperty("server_language_not_found_or_empty")]
        public string ServerLanguageNotFoundOrEmpty = "Server language {language} folder is empty or does not exist";
    }

    public class ModuleLocalization
    {
        [TomlProperty("universal_provider_found")]
        public string UniversalProviderFound = "Using universal provider for game '{game}'";

        [TomlProperty("universal_provider_not_found")]
        public string UniversalProviderNotFound = "Universal API not yet available for this game";

        [TomlProperty("universal_provider_found_multiple")]
        public string UniversalProviderFoundMultiple = "Multiple universal providers found! Using {name}. (Also found {extra})";

        [TomlProperty("universal_provider_failure")]
        public string UniversalProviderException = "Got exception when instantiating universal provider, will not be functional for this session";

        [TomlProperty("hook_decorator_failure")]
        public string HookDecoratorException = "Got exception when resolving hook decorator, {type} will not be functional for this session";

        [TomlProperty("player_type_not_found")]
        public string PlayerTypeNotFound = "Unable to resolve implied IPlayer instance";

        [TomlProperty("oxide_directory_obsolete")]
        public string OxideDirectoryObsolete = "oxide.directory in command-line is obsolete, please use umod.directory instead";

        [TomlProperty("nolog_warning")]
        public string NoLogWarning = "Usage of the 'nolog' variable will prevent filesystem logging";

        [TomlProperty("loading")]
        public string Loading = "Loading uMod v{version}...";

        [TomlProperty("loading_core_extensions")]
        public string LoadingCoreExtensions = "Loading core extensions...";

        [TomlProperty("loading_user_extensions")]
        public string LoadingUserExtensions = "Loading user extensions...";

        [TomlProperty("loading_plugins")]
        public string LoadingPlugins = "Loading plugins...";

        [TomlProperty("logger_failure")]
        public string LoggerFailure = "Unable to initialize logger";

        [TomlProperty("auth_failure")]
        public string AuthFailure = "Unable to initialize auth system and related libraries";

        [TomlProperty("clock_failure")]
        public string ClockFailure = "A reliable clock is not available, falling back to a clock which may be unreliable on certain hardware";

        [TomlProperty("nexttick_failure")]
        public string NextTickFailure = "Exception while calling NextTick callback";

        [TomlProperty("onframe_failure")]
        public string OnFrameFailure = "{exception} while invoke OnFrame in extensions";

        [TomlProperty("plugin_watchers_disabled")]
        public string PluginWatchersDisabled = "Automatic plugin reloading and unloading has been disabled";

        [TomlProperty("config_watchers_enabled")]
        public string ConfigWatchersEnabled = "Automatic plugin configuration file reloading is enabled";
    }

    public class CommandLocalization
    {
        [TomlProperty("command")]
        public string Command = "Command|Commands";

        [TomlProperty("unknown")]
        public string Unknown = "Unknown";

        [TomlProperty("unknown_command")]
        public string UnknownCommand = "Unknown command: {command}";

        [TomlProperty("already_exists")]
        public string CommandAlreadyExists = "{plugin} tried to register command '{command}', this command already exists and cannot be overridden!";

        [TomlProperty("help_default")]
        public string CommandHelpDefault = "See plugin documentation for command help";

        [TomlProperty("description_default")]
        public string CommandDescriptionDefault = "See plugin documentation for command description";

        [TomlProperty("alias_already_exists")]
        public string AliasAlreadyExists = "Universal command alias already exists: {command}";

        [TomlProperty("null_caller")]
        public string NullCaller = "Plugin.UniversalCommandCallback received null as the caller (bad game universal bindings?)";

        [TomlProperty("permissing_denied")]
        public string PermissionDenied = "You do not have permission to use the command '{command}'!";

        [TomlProperty("driver_invalid")]
        public string DriverInvalid = "Driver not found: {driver}";

        [TomlProperty("import_failure_errors")]
        public string ImportFailureErrors = "Import failure: {errors}";

        [TomlProperty("import_failure")]
        public string ImportFailure = "Import failed unexpectedly";

        [TomlProperty("import_success")]
        public string ImportSuccess = "Import succeeded";
    }

    public class CommandUsageLocalization
    {
        [TomlProperty("usage_ext_load")]
        public string UsageExtLoad = "Usage: umod.ext.load <extname>+";

        [TomlProperty("usage_ext_unload")]
        public string UsageExtUnload = "Usage: umod.ext.unload <extname>+";

        [TomlProperty("usage_ext_reload")]
        public string UsageExtReload = "Usage: umod.ext.reload <extname>+";

        [TomlProperty("usage_grant")]
        public string UsageGrant = "Usage: umod.grant <group|user> <name|id> <permission>";

        [TomlProperty("usage_group")]
        public string UsageGroup = "Usage: umod.group <add|set> <name> [title] [rank]";

        [TomlProperty("usage_group_parent")]
        public string UsageGroupParent = "Usage: umod.group <parent> <name> <parentName>";

        [TomlProperty("usage_group_remove")]
        public string UsageGroupRemove = "Usage: umod.group <remove> <name>";

        [TomlProperty("usage_lang")]
        public string UsageLang = "Usage: umod.lang <two-digit language code>";

        [TomlProperty("usage_load")]
        public string UsageLoad = "Usage: umod.load *|<pluginname>+";

        [TomlProperty("usage_reload")]
        public string UsageReload = "Usage: umod.reload *|<pluginname>+";

        [TomlProperty("usage_revoke")]
        public string UsageRevoke = "Usage: umod.revoke <group|user> <name|id> <permission>";

        [TomlProperty("usage_show")]
        public string UsageShow = "Usage: umod.show <groups|perms>";

        [TomlProperty("usage_show_name")]
        public string UsageShowName = "Usage: umod.show <group|user> <name>";

        [TomlProperty("usage_unload")]
        public string UsageUnload = "Usage: umod.unload *|<pluginname>+";

        [TomlProperty("usage_user_group")]
        public string UsageUserGroup = "Usage: umod.usergroup <add|remove> <username> <groupname>";

        [TomlProperty("usage_import")]
        public string UsageImport = "Usage: umod.import <meta> <driver/connection>";
    }

    #endregion Default English Localization
}
