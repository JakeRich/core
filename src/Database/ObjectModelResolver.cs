﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using uMod.Common.Database;
using uMod.Database.Exceptions;

namespace uMod.Database
{
    internal class ObjectModelResolver
    {
        internal Cache<Type, ObjectModel> CachedModels = new Cache<Type, ObjectModel>();

        /// <summary>
        /// Gets a TomlObjectContract for the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal ObjectModel ResolveModel(Type type, IDictionary<Type, ObjectModel> relatedModels = null)
        {
            if (CachedModels.TryGetValue(type, out ObjectModel contract))
            {
                return contract;
            }

            contract = MakeModel(type, relatedModels);

            CachedModels.Add(type, contract);

            return contract;
        }

        /// <summary>
        /// Make a contract that represents how data a type should be serialized
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal ObjectModel MakeModel(Type type, IDictionary<Type, ObjectModel> relatedModels = null)
        {
            ObjectModel model = new ObjectModel();
            List<MemberInfo> allMembers = new List<MemberInfo>();
            allMembers.AddRange(type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));
            allMembers.AddRange(type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance));

            Dictionary<string, ObjectModelRelation> oneToOneRelations = null;
            Dictionary<string, ObjectModelRelation> oneToManyRelations = null;

            foreach (MemberInfo member in allMembers)
            {
                FieldInfo fieldInfo = member as FieldInfo;
                PropertyInfo propertyInfo = member as PropertyInfo;
                Type memberType = fieldInfo != null ? fieldInfo.FieldType : propertyInfo?.PropertyType;

                PrimaryKeyAttribute keyAttribute = member.GetCustomAttribute<PrimaryKeyAttribute>();
                if (keyAttribute != null)
                {
                    Type keyType;
                    switch (member.MemberType)
                    {
                        case MemberTypes.Field:
                            keyType = ((FieldInfo)member).FieldType;
                            break;

                        case MemberTypes.Property:
                            keyType = ((PropertyInfo)member).PropertyType;
                            break;

                        default:
                            throw new KeyFieldInvalidException($"Model key ({member.Name}) for model ({type.Name}) must be a property or a field.");
                    }

                    if (!keyType.IsPrimitive && keyType != typeof(string))
                    {
                        throw new KeyFieldInvalidException($"Model key ({member.Name}) for model ({type.Name}) must be a primitive type.");
                    }

                    model.PrimaryKeyField = new KeyField(member.Name, keyType.ToString());
                }

                OneToOneAttribute oneToOneAttribute = member.GetCustomAttribute<OneToOneAttribute>();
                if (oneToOneAttribute != null)
                {
                    if (oneToOneRelations == null)
                    {
                        oneToOneRelations = new Dictionary<string, ObjectModelRelation>();
                    }

                    if (relatedModels == null || (!relatedModels.TryGetValue(memberType, out ObjectModel relatedModel)))
                    {
                        relatedModel = GetRelationModel(memberType, type, model);
                    }

                    oneToOneRelations.Add(member.Name, new ObjectModelRelation(relatedModel, null, null, GetRelationType(memberType)));
                }

                OneToManyAttribute oneToManyAttribute = member.GetCustomAttribute<OneToManyAttribute>();
                if (oneToManyAttribute != null)
                {
                    if (oneToManyRelations == null)
                    {
                        oneToManyRelations = new Dictionary<string, ObjectModelRelation>();
                    }

                    if (relatedModels == null || (!relatedModels.TryGetValue(memberType, out ObjectModel relatedModel)))
                    {
                        relatedModel = GetRelationModel(memberType, type, model);
                    }

                    oneToManyRelations.Add(member.Name, new ObjectModelRelation(relatedModel, null, null, GetRelationType(memberType)));
                }
            }

            if (oneToOneRelations != null)
            {
                model.OneToOneRelations = oneToOneRelations;
            }

            if (oneToManyRelations != null)
            {
                model.OneToManyRelations = oneToManyRelations;
            }

            CachedModels.Add(type, model);

            return model;
        }

        /// <summary>
        /// Gets model for relation type
        /// </summary>
        /// <param name="memberType"></param>
        /// <param name="parentType"></param>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        private ObjectModel GetRelationModel(Type memberType, Type parentType, ObjectModel parentModel, IDictionary<Type, ObjectModel> relatedModels = null)
        {
            if (typeof(IList).IsAssignableFrom(memberType))
            {
                Type[] genericArguments = memberType.GetGenericArguments();

                if (genericArguments.Length > 0)
                {
                    Type listValueType = memberType.GetGenericArguments()[0];
                    return GetModel(listValueType, parentType, parentModel);
                }

                return null;
            }
            if (typeof(IDictionary).IsAssignableFrom(memberType))
            {
                Type[] genericArguments = memberType.GetGenericArguments();

                if (genericArguments.Length > 0)
                {
                    Type dictionaryValueType = memberType.GetGenericArguments()[1];
                    return GetModel(dictionaryValueType, parentType, parentModel);
                }

                return null;
            }

            return GetModel(memberType, parentType, parentModel, relatedModels);
        }

        /// <summary>
        /// Gets model for specified type with recursion check
        /// </summary>
        /// <param name="type"></param>
        /// <param name="parentType"></param>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        private ObjectModel GetModel(Type type, Type parentType, ObjectModel parentModel, IDictionary<Type, ObjectModel> relatedModels = null)
        {
            if (type.IsClass && type.GetCustomAttribute<ModelAttribute>() != null)
            {
                // Avoid infinite loop
                if (type == parentType)
                {
                    return parentModel;
                }

                if (relatedModels == null)
                {
                    relatedModels = new Dictionary<Type, ObjectModel>();
                }

                if (!relatedModels.ContainsKey(parentType))
                {
                    relatedModels.Add(parentType, parentModel);
                }

                return ResolveModel(type, relatedModels);
            }

            return null;
        }

        /// <summary>
        /// Gets the relation type for the specified member
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        private RelationType GetRelationType(Type memberType)
        {
            if (typeof(IList).IsAssignableFrom(memberType))
            {
                return RelationType.List;
            }
            if (typeof(IDictionary).IsAssignableFrom(memberType))
            {
                return RelationType.Dictionary;
            }

            return RelationType.Assignable;
        }
    }
}
