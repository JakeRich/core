﻿namespace uMod.Database
{
    /// <summary>
    /// Wraps database context
    /// </summary>
    public class Provider
    {
        /// <summary>
        /// Global database configuration
        /// </summary>
        public Configuration.Database Configuration { get; }

        /// <summary>
        /// Global database client
        /// </summary>
        public Client Client { get; }

        /// <summary>
        /// Database app client
        /// </summary>
        internal Apps.Database Application;

        /// <summary>
        /// Create a new database container
        /// </summary>
        /// <param name="config"></param>
        internal Provider(Configuration.Database config)
        {
            Configuration = config;
            Client = new Client(this);
        }

        /// <summary>
        /// Initialize database application
        /// </summary>
        internal void Initialize()
        {
            Configuration.Initialize();
        }
    }
}
