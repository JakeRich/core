﻿using System;

namespace uMod.Database.Exceptions
{
    public class ConnectionException : PromiseException
    {
        public ConnectionException()
        {
        }

        public ConnectionException(string message) : base(message)
        {
        }

        public ConnectionException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
