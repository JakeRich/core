﻿using System;

namespace uMod.Database.Exceptions
{
    public class TransactionException : PromiseException
    {
        public TransactionException()
        {
        }

        public TransactionException(string message) : base(message)
        {
        }

        public TransactionException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
