using System;
using uMod.Common;
using uMod.Common.Database;

namespace uMod.Database
{
    /// <summary>
    /// Migration direction
    /// </summary>
    public enum MigrationDirection
    {
        Up,
        Down
    }

    /// <summary>
    /// Determines that a class is a database migration
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class MigrationAttribute : Attribute
    {
        public string Name { get; }
        public VersionNumber Version { get; }

        public MigrationAttribute(string migrationName)
        {
            Name = migrationName;
        }

        public MigrationAttribute(string migrationName, int version) : this(migrationName)
        {
            Version = new VersionNumber(version, 0, 0);
        }

        public MigrationAttribute(string migrationName, int version, int minor) : this(migrationName)
        {
            Version = new VersionNumber(version, minor, 0);
        }

        public MigrationAttribute(string migrationName, int version, int minor, int patch) : this(migrationName)
        {
            Version = new VersionNumber(version, minor, patch);
        }

        public MigrationAttribute(string migrationName, VersionNumber version) : this(migrationName)
        {
            Name = migrationName;
            Version = version;
        }

        public override string ToString()
        {
            return $"{Name}-{Version}";
        }
    }

    /// <summary>
    /// Represents a database migration
    /// </summary>
    public abstract class Migration : INameable
    {
        protected ConnectionType ConnectionType;
        public string Name => _transaction.Name;
        private readonly Transaction _transaction;

        /// <summary>
        /// Create a new Migration object
        /// </summary>
        /// <param name="connection"></param>
        protected Migration(Connection connection)
        {
            ConnectionType = connection.Info.Type;

            if (GetType().GetCustomAttribute<MigrationAttribute>() is MigrationAttribute migrationAttribute)
            {
                _transaction = new Transaction(connection.Database, connection, migrationAttribute.ToString());
            }
            else
            {
                throw new ArgumentException($"Migration ({GetType().Name}) does not specify MigrationAttribute");
            }
        }

        /// <summary>
        /// Perform migration using specified migration direction
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        internal IPromise Commit(MigrationDirection direction)
        {
            switch (direction)
            {
                case MigrationDirection.Up:
                    Up();
                    if (!(this is MigrationMigration))
                    {
                        // Upsert
                        Execute(ConnectionType == ConnectionType.MySQL
                            ? $"INSERT INTO umod_migrations (name) VALUES ('{Name}') ON DUPLICATE KEY UPDATE;"
                            : $"INSERT OR IGNORE INTO umod_migrations VALUES ('{Name}');");
                    }
                    break;

                case MigrationDirection.Down:
                    Down();
                    if (!(this is MigrationMigration))
                    {
                        Execute($"DELETE FROM umod_migrations WHERE name = '{Name}';");
                    }
                    break;
            }

            if (_transaction.Count > 0)
            {
                return _transaction.Commit();
            }

            return null;
        }

        /// <summary>
        /// Add query to migration
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        protected Migration Execute(string sql, object parameters = null, int timeout = 0)
        {
            _transaction.Execute(sql, parameters, timeout);
            return this;
        }

        /// <summary>
        /// Perform migration up
        /// </summary>
        protected abstract void Up();

        /// <summary>
        /// Perform migration down
        /// </summary>
        protected abstract void Down();

        /// <summary>
        /// Determine if migration table exists
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        internal static IPromise<bool> CheckMigrationTableExists(Connection connection)
        {
            Promise<bool> promise = new Promise<bool>();

            string sql = string.Empty;
            switch (connection.Info.Type)
            {
                case ConnectionType.MySQL:
                    sql = "SELECT count(*) FROM information_schema.TABLES (TABLE_NAME = 'umod_migrations');";
                    break;

                case ConnectionType.SQLite:
                    sql = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='umod_migrations';";
                    break;
            }

            connection.ExecuteScalar<long>(sql).Done(delegate (long result) { promise.Resolve(result > 0); }, delegate (Exception exception)
            {
                Interface.uMod.LogError($"Unable to find migration table: {exception.Message}");  // TODO: Localization
            });

            return promise;
        }

        internal static IPromise<bool> CheckTableExists(Connection connection, Migration migration)
        {
            Promise<bool> promise = new Promise<bool>();

            string sql = string.Empty;
            switch (connection.Info.Type)
            {
                case ConnectionType.MySQL:
                    sql = $"SELECT count(`name`) FROM `umod_migrations` WHERE `name` = \"{migration.Name}\";";
                    break;

                case ConnectionType.SQLite:
                    sql = $"SELECT count(\"name\") FROM \"umod_migrations\" WHERE \"name\" = \"{migration.Name}\";";
                    break;
            }

            connection.ExecuteScalar<long>(sql).Done(delegate (long result)
            {
                if (result > 0)
                {
                    promise.Resolve(true);
                }
                else
                {
                    promise.Resolve(false);
                }
            }, delegate (Exception exception)
            {
                Interface.uMod.LogError($"Unable to check migration status: {exception.Message}"); // TODO: Localization
            });

            return promise;
        }

        /// <summary>
        /// Ensure migration table exists on connection and create it if not
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        internal static IPromise EnsureMigrationTableExists(Connection connection)
        {
            Promise promise = new Promise();

            CheckMigrationTableExists(connection).Done(delegate (bool result)
            {
                if (!result)
                {
                    connection.Migrate<MigrationMigration>(MigrationDirection.Up).Done(promise.Resolve, promise.Reject);
                }
                else
                {
                    promise.Resolve();
                }
            }, promise.Reject);

            return promise;
        }
    }

    /// <summary>
    /// Represents a migration for umod_migrations
    /// </summary>
    [Migration("umod_migrations")]
    public class MigrationMigration : Migration
    {
        /// <summary>
        /// Create a new MigrationMigration object
        /// </summary>
        /// <param name="connection"></param>
        public MigrationMigration(Connection connection) : base(connection)
        {
        }

        /// <summary>
        /// Drop tables
        /// </summary>
        protected override void Down()
        {
            Execute("DROP TABLE umod_migrations;");
        }

        /// <summary>
        /// Create tables
        /// </summary>
        protected override void Up()
        {
            string sql = string.Empty;
            switch (ConnectionType)
            {
                case ConnectionType.MySQL:
                    sql = "CREATE TABLE IF NOT EXISTS `umod_migrations` (`name` VARCHAR(255) NOT NULL UNIQUE, PRIMARY KEY(`name`));";
                    break;

                case ConnectionType.SQLite:
                    sql = "CREATE TABLE IF NOT EXISTS \"umod_migrations\" (\"name\" TEXT NOT NULL UNIQUE, PRIMARY KEY(\"name\"));";
                    break;
            }

            if (!string.IsNullOrEmpty(sql))
            {
                Execute(sql);
            }
        }
    }

    /// <summary>
    /// Represents a database migration for authentication layer
    /// </summary>
    [Migration("auth_migration")]
    public class AuthMigration : Migration
    {
        /// <summary>
        /// Create a new AuthMigration object
        /// </summary>
        /// <param name="connection"></param>
        public AuthMigration(Connection connection) : base(connection)
        {
        }

        /// <summary>
        /// Drop tables
        /// </summary>
        protected override void Down()
        {
            Execute("DROP TABLE umod_group_permissions;");
            Execute("DROP TABLE umod_player_permissions;");
            Execute("DROP TABLE umod_player_groups;");
            Execute("DROP TABLE umod_player_nicknames;");
            Execute("DROP TABLE umod_groups;");
            Execute("DROP TABLE umod_players;");
        }

        /// <summary>
        /// Create tables
        /// </summary>
        protected override void Up()
        {
            string[] sqls = new string[6];
            switch (ConnectionType)
            {
                case ConnectionType.MySQL:
                    sqls[0] = "CREATE TABLE IF NOT EXISTS `umod_groups` (`Name` VARCHAR(50) NOT NULL UNIQUE, `Title` VARCHAR(255), `Rank` TINYINT UNSIGNED, `ParentGroup` VARCHAR(50), PRIMARY KEY(`Name`));";
                    sqls[1] = "CREATE TABLE IF NOT EXISTS `umod_players` (`Id` VARCHAR(50) NOT NULL, `Name` VARCHAR(255) NOT NULL, `Language` VARCHAR(3) NOT NULL, `Created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `LastLogin` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(`Id`), INDEX(`Name`));";
                    sqls[2] = "CREATE TABLE IF NOT EXISTS `umod_group_permissions` (`GroupName` VARCHAR(50) NOT NULL, `Permission` VARCHAR(255) NOT NULL, `ValidUntil` TIMESTAMP NULL DEFAULT NULL, INDEX(`GroupName`), FOREIGN KEY(`GroupName`) REFERENCES `umod_groups`(`Name`) ON UPDATE CASCADE ON DELETE CASCADE);";

                    sqls[3] = "CREATE TABLE IF NOT EXISTS `umod_player_permissions` (`PlayerId` VARCHAR(50) NOT NULL, `Permission` VARCHAR(255) NOT NULL, `ValidUntil` TIMESTAMP NULL DEFAULT NULL, INDEX(`PlayerId`), FOREIGN KEY(`PlayerId`) REFERENCES `umod_players`(`Id`) ON UPDATE CASCADE ON DELETE CASCADE);";
                    sqls[4] = "CREATE TABLE IF NOT EXISTS `umod_player_groups` (`PlayerId` VARCHAR(50) NOT NULL, `GroupName` VARCHAR(50) NOT NULL, `ValidUntil` TIMESTAMP NULL DEFAULT NULL, INDEX(`GroupName`), FOREIGN KEY(`PlayerId`) REFERENCES `umod_players`(`Id`) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY(`GroupName`) REFERENCES `umod_groups`(`Name`) ON UPDATE CASCADE ON DELETE CASCADE);";
                    sqls[5] = "CREATE TABLE IF NOT EXISTS `umod_player_nicknames` (`PlayerId` VARCHAR(50) NOT NULL, `Name` VARCHAR(255) NOT NULL, `Created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, INDEX(`PlayerId`), FOREIGN KEY(`PlayerId`) REFERENCES `umod_players`(`Id`) ON UPDATE CASCADE ON DELETE CASCADE);";
                    break;

                case ConnectionType.SQLite:
                    sqls[0] = "CREATE TABLE IF NOT EXISTS \"umod_groups\" (\"Name\" TEXT NOT NULL UNIQUE, \"Title\" TEXT, \"Rank\" INTEGER, \"ParentGroup\" TEXT, PRIMARY KEY(\"Name\"));";
                    sqls[1] = "CREATE TABLE IF NOT EXISTS \"umod_players\" (\"Id\" TEXT NOT NULL, \"Name\" TEXT NOT NULL, \"Language\" TEXT NOT NULL, \"Created\" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, \"LastLogin\" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(\"Id\"));";
                    sqls[2] = "CREATE TABLE IF NOT EXISTS \"umod_group_permissions\" (\"GroupName\" TEXT NOT NULL, \"Permission\" TEXT NOT NULL, \"ValidUntil\" DATETIME NULL DEFAULT NULL, FOREIGN KEY(\"GroupName\") REFERENCES \"umod_groups\"(\"Name\") ON UPDATE CASCADE ON DELETE CASCADE);";

                    sqls[3] = "CREATE TABLE IF NOT EXISTS \"umod_player_permissions\" (\"PlayerId\" TEXT NOT NULL, \"Permission\" TEXT NOT NULL, \"ValidUntil\" DATETIME NULL DEFAULT NULL, FOREIGN KEY(\"PlayerId\") REFERENCES \"umod_players\"(\"Id\") ON UPDATE CASCADE ON DELETE CASCADE);";
                    sqls[4] = "CREATE TABLE IF NOT EXISTS \"umod_player_groups\" (\"PlayerId\" TEXT NOT NULL, \"GroupName\" TEXT NOT NULL, \"ValidUntil\" DATETIME NULL DEFAULT NULL, FOREIGN KEY(\"PlayerId\") REFERENCES \"umod_players\"(\"Id\") ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY(\"GroupName\") REFERENCES \"umod_groups\"(\"Name\") ON UPDATE CASCADE ON DELETE CASCADE);";
                    sqls[5] = "CREATE TABLE IF NOT EXISTS \"umod_player_nicknames\" (\"PlayerId\" TEXT NOT NULL, \"Name\" TEXT NOT NULL, \"Created\" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY(\"PlayerId\") REFERENCES \"umod_players\"(\"Id\") ON UPDATE CASCADE ON DELETE CASCADE);";
                    break;
            }

            foreach (string sql in sqls)
            {
                if (!string.IsNullOrEmpty(sql))
                {
                    Execute(sql);
                }
            }
        }
    }
}
