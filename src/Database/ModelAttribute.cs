﻿using System;

namespace uMod.Database
{
    /// <summary>
    /// Specify a class as a database model
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class ModelAttribute : Attribute
    {
        /// <summary>
        /// Create a new instance of the ModelAttribute class
        /// </summary>
        public ModelAttribute()
        {
        }
    }
}
