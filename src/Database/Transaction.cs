﻿using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Common.Database;
using CommandType = uMod.Common.Database.CommandType;

namespace uMod.Database
{
    /// <summary>
    /// Represents a database transaction
    /// </summary>
    public class Transaction : IContext
    {
        /// <summary>
        /// Gets the connection of the transaction
        /// </summary>
        public Connection Connection { get; internal set; }

        /// <summary>
        /// Number of queries completed
        /// </summary>
        private int _completedCount;

        /// <summary>
        /// Gets the transaction name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the queries in the transaction
        /// </summary>
        protected readonly IList<Query> Queries = new List<Query>();

        private readonly Provider _database;

        /// <summary>
        /// Create a new transaction object
        /// </summary>
        /// <param name="database"></param>
        /// <param name="connection"></param>
        /// <param name="name"></param>
        public Transaction(Provider database, Connection connection = null, string name = null)
        {
            _database = database;
            Connection = connection;
            if (string.IsNullOrEmpty(name))
            {
                name = GetType().Name + Utility.Random.Range(0, 999999);
            }
            Name = name;
        }

        /// <summary>
        /// Gets the number of queries in the transaction
        /// </summary>
        public int Count => Queries.Count;

        /// <summary>
        /// Removes all queries in the transaction
        /// </summary>
        public void Clear()
        {
            Queries.Clear();
        }

        /// <summary>
        /// Creates a new query
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction Query(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.Query,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });

            return this;
        }

        /// <summary>
        /// Create a new query that returns multiple strongly-typed results
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction Query<T>(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.Query,
                Sql = sql,
                TypeName = Connection.GetTypeName<T>(),
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = Connection.GetTypeModel<T>()
            });

            return this;
        }

        public Transaction Query<T>(string sql, IEnumerable<string> mapping, string splitOn = "Id", object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.Query,
                Sql = sql,
                TypeName = Connection.GetTypeName<T>(),
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Mapping = mapping.ToArray(),
                SplitOn = splitOn,
                Model = Connection.GetTypeModel<T>()
            });

            return this;
        }

        /// <summary>
        /// Create a new query that returns first result
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction QueryFirst(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.QueryFirst,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });

            return this;
        }

        /// <summary>
        /// Create a new query that returns first strongly-typed result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction QueryFirst<T>(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.QueryFirst,
                Sql = sql,
                TypeName = Connection.GetTypeName<T>(),
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = Connection.GetTypeModel<T>()
            });

            return this;
        }

        /// <summary>
        /// Create a new query that returns a single result
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction QuerySingle(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.QuerySingle,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });

            return this;
        }

        /// <summary>
        /// Create a new query that returns a single strongly-typed result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction QuerySingle<T>(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.QuerySingle,
                Sql = sql,
                TypeName = Connection.GetTypeName<T>(),
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = Connection.GetTypeModel<T>()
            });

            return this;
        }

        /// <summary>
        /// Create a new non-query with no return rows
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction Execute(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.Execute,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });

            return this;
        }

        /// <summary>
        /// Create a new non-query with no return rows (but strongly-typed result)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction Execute<T>(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.Execute,
                Sql = sql,
                TypeName = Connection.GetTypeName<T>(),
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text,
                Model = Connection.GetTypeModel<T>()
            });

            return this;
        }

        /// <summary>
        /// Create a new non-query that returns a scalar value
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public Transaction ExecuteScalar(string sql, object parameters = null, int timeout = 0)
        {
            Queries.Add(new Query(_database, Connection.Client.Context)
            {
                Plugin = Connection.Client.Context?.Name,
                Connection = Connection.Info.Name,
                QueryType = QueryType.ExecuteScalar,
                Sql = sql,
                TypeName = null,
                Parameters = parameters,
                Transaction = Name,
                Buffered = true,
                Timeout = timeout,
                CommandType = CommandType.Text
            });

            return this;
        }

        /// <summary>
        /// Commit transaction
        /// </summary>
        /// <returns></returns>
        public virtual IPromise Commit()
        {
            _completedCount = 0;
            Promise promise = new Promise();

            if (Queries.Count == 0)
            {
                promise.Reject(new Exceptions.TransactionException("Transaction empty"));
                return promise;
            }

            return CommitNext(promise);
        }

        /// <summary>
        /// Commit next query in transaction
        /// </summary>
        /// <param name="promise"></param>
        /// <returns></returns>
        protected virtual IPromise CommitNext(Promise promise)
        {
            Query query = Queries[_completedCount];

            query.Context = Connection.Client.Context;
            query.Plugin = Connection.Client.Context?.Name;
            query.Connection = Connection.Info.Name;

            _database.Application.EnqueueQuery(query.GetData(), delegate (bool success, string message)
            {
                if (success)
                {
                    _completedCount++;
                    if (_completedCount == Queries.Count)
                    {
                        _database.Application.EnqueueCommit(Name, Connection.Info, delegate (bool commitSuccess, string commitMessage)
                        {
                            if (commitSuccess)
                            {
                                promise.Resolve();
                            }
                            else
                            {
                                promise.Reject(new Exceptions.TransactionException(commitMessage));
                            }
                        });
                    }
                    else
                    {
                        CommitNext(promise);
                    }
                }
                else
                {
                    promise.Reject(new Exceptions.TransactionException(message));
                }
            });

            return promise;
        }
    }
}
