﻿using System;
using System.Collections.Generic;
using System.IO;
using uMod.Common;

namespace uMod.Extensions
{
    public class ExtensionProvider : IExtensionProvider
    {
        public string Directory;
        private readonly ILogger _logger;
        private readonly IApplication _application;
        public IExtensionManager Manager { get; }

        public IEnumerable<IExtension> All => Manager.GetExtensions();

        public IExtension this[string name] => Get(name);

        public ExtensionProvider(string extensionDirectory, IApplication application, ILogger logger)
        {
            Directory = extensionDirectory;
            _logger = logger;
            _application = application;
            Manager = new ExtensionManager(logger);
        }

        /// <summary>
        /// Gets an extension with the specified type or name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T Get<T>(string name = null) where T : class, IExtension
        {
            return Manager.GetExtension<T>(name ?? typeof(T).Name);
        }

        /// <summary>
        /// Gets an extension with the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IExtension Get(string name)
        {
            return Manager.GetExtension(name);
        }

        /// <summary>
        /// Register specified plugin loader
        /// </summary>
        /// <param name="loader"></param>
        public void RegisterLoader(IPluginLoader loader)
        {
            Manager.RegisterPluginLoader(loader);
        }

        /// <summary>
        /// Unregister specified plugin loader
        /// </summary>
        /// <param name="loader"></param>
        public void UnregisterLoader(IPluginLoader loader)
        {
            Manager.UnregisterPluginLoader(loader);
        }

        /// <summary>
        /// Gets all loaded extensions
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IPluginLoader> GetLoaders()
        {
            return Manager.GetPluginLoaders();
        }

        /// <summary>
        /// Load the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool Load(IExtension extension)
        {
            return Manager.LoadExtension(extension);
        }

        /// <summary>
        /// Load the specified extension type
        /// </summary>
        /// <param name="extensionType"></param>
        /// <returns></returns>
        public bool Load(Type extensionType)
        {
            // Use factory directly to avoid context binding until extension is fully loaded
            if (_application.Make<IExtension>(extensionType) is IExtension extension)
            {
                Load(extension);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Load the specified extension type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Load<T>() where T : IExtension
        {
            return Load(typeof(T));
        }

        /// <summary>
        /// Load the specified extension assembly by file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Load(string name)
        {
            string extFileName = !name.EndsWith(".dll") ? name + ".dll" : name;
            string extPath = Path.Combine(Directory, extFileName);

            if (!File.Exists(extPath))
            {
                _logger.Error($"Could not load extension '{name}' (file not found)");
                return false;
            }

            return Load(extPath);
        }

        /// <summary>
        /// Unload the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool Unload(IExtension extension)
        {
            return Manager.UnloadExtension(extension);
        }

        /// <summary>
        /// Unload the specified extension assembly by file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Unload(string name)
        {
            string extFileName = !name.EndsWith(".dll") ? name + ".dll" : name;
            string extPath = Path.Combine(Directory, extFileName);

            if (!File.Exists(extPath))
            {
                _logger.Error($"Could not unload extension '{name}' (file not found)");
                return false;
            }

            return Manager.UnloadExtension(extPath);
        }

        /// <summary>
        /// Reload the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool Reload(IExtension extension)
        {
            return Manager.ReloadExtension(extension);
        }

        /// <summary>
        /// Reload the specified extension assembly by file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Reload(string name)
        {
            string extFileName = !name.EndsWith(".dll") ? name + ".dll" : name;
            string extPath = Path.Combine(Directory, extFileName);

            if (!File.Exists(extPath))
            {
                _logger.Error($"Could not reload extension '{name}' (file not found)");
                return false;
            }

            return Manager.ReloadExtension(extPath);
        }
    }
}
