﻿using uMod.Common;

namespace uMod.Extensions
{
    /// <summary>
    /// Represents a single binary extension
    /// </summary>
    public abstract class Extension : IExtension
    {
        /// <summary>
        /// Gets the name of this extension
        /// </summary>
        public virtual string Name { get; private set; }

        /// <summary>
        /// Gets the title of this extension
        /// </summary>
        public abstract string Title { get; }

        /// <summary>
        /// Gets the author of this extension
        /// </summary>
        public abstract string Author { get; }

        /// <summary>
        /// Gets the version of this extension
        /// </summary>
        public abstract VersionNumber Version { get; }

        /// <summary>
        /// Gets the filename of the extension
        /// </summary>
        public string Filename { get; private set; }

        /// <summary>
        /// Gets the branch of this extension
        /// </summary>
        public virtual string Branch { get; } = "public";

        /// <summary>
        /// Gets whether this extension is a core extension
        /// </summary>
        public virtual bool IsCoreExtension { get; } = false;

        /// <summary>
        /// Gets whether this extension is for a specific game
        /// </summary>
        public virtual bool IsGameExtension { get; } = false;

        /// <summary>
        /// Gets whether the extension supports extension-reloading
        /// </summary>
        public virtual bool SupportsReloading { get; } = false;

        /// <summary>
        /// Gets the extension manager responsible for this extension
        /// </summary>
        private IExtensionManager _manager;

        /// <summary>
        /// Gets the default references for plugins
        /// </summary>
        public virtual string[] DefaultReferences { get; protected set; } = new string[0];

        /// <summary>
        /// Gets the whitelisted namespaces for plugins
        /// </summary>
        public virtual string[] WhitelistedNamespaces { get; protected set; } = new string[0];

        /// <summary>
        /// Called when this extension is added to a manager
        /// </summary>
        public IContextManagerEvent<IExtension, IExtensionManager> OnAddedToManager { get; } = new ExtensionManagerEvent();

        /// <summary>
        /// Called when this extension is removed from a manager
        /// </summary>
        public IContextManagerEvent<IExtension, IExtensionManager> OnRemovedFromManager { get; } = new ExtensionManagerEvent();

        /// <summary>
        /// Resolves the name and filename for extension
        /// </summary>
        /// <param name="name"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public virtual bool Resolve(string name, string fileName)
        {
            if (!string.IsNullOrEmpty(name))
            {
                Name = name;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                Filename = fileName;
            }

            return true;
        }

        /// <summary>
        /// Loads this extension
        /// </summary>
        public virtual void Load()
        {
        }

        /// <summary>
        /// Configures this extension
        /// </summary>
        public virtual void Configure()
        {
        }

        /// <summary>
        /// Called before the extension is unloaded
        /// </summary>
        public virtual void Unload()
        {
        }

        /// <summary>
        /// Loads any plugin watchers pertinent to this extension
        /// </summary>
        /// <param name="pluginDirectory"></param>
        public virtual void LoadPluginWatchers(string pluginDirectory)
        {
        }

        /// <summary>
        /// Called after all other extensions have been loaded
        /// </summary>
        public virtual void OnModLoad()
        {
        }

        /// <summary>
        /// Called on shutdown
        /// </summary>
        public virtual void OnShutdown()
        {
        }

        /// <summary>
        /// Called when this extension has been added to the specified manager
        /// </summary>
        /// <param name="manager"></param>
        public virtual void HandleAddedToManager(IExtensionManager manager)
        {
            if (manager != null)
            {
                _manager = manager;
            }

            OnAddedToManager?.Invoke(this, manager ?? _manager);
        }

        /// <summary>
        /// Called when this plugin has been removed from the specified manager
        /// </summary>
        /// <param name="manager"></param>
        public virtual void HandleRemovedFromManager(IExtensionManager manager)
        {
            if (_manager == manager)
            {
                _manager = null;
            }

            OnRemovedFromManager?.Invoke(this, manager);
        }
    }
}
