﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using uMod.Plugins;

namespace uMod.IO
{
    /// <summary>
    /// Data category
    /// </summary>
    public enum DataCategory
    {
        Data,
        Configuration,
        Localization
    }

    /// <summary>
    /// Data format
    /// </summary>
    public enum DataFormat
    {
        Binary,
        Json,
        Toml,
        Proto,
        Sql,
        Text
    }

    /// <summary>
    /// Represents a dynamic file system
    /// </summary>
    public class DynamicFileSystem : DynamicDataSystem<string>, IDynamicFileSystem
    {
        /// <summary>
        /// Arbitrary lock object
        /// </summary>
        private readonly object fileSystemLock = new object();

        /// <summary>
        /// Data category of file system
        /// </summary>
        protected readonly DataCategory DataCategory;

        /// <summary>
        /// Directory path of file system
        /// </summary>
        protected readonly string Directory;

        /// <summary>
        /// Gets dictionary of data file systems
        /// </summary>
        protected readonly IDictionary<Type, object> DataFileSystems = new Dictionary<Type, object>();

        /// <summary>
        /// Create new dynamic file system
        /// </summary>
        /// <param name="directory"></param>
        public DynamicFileSystem(string directory)
        {
            Directory = directory;
            DataCategory = PathUtils.GetDataCategory(directory);
        }

        /// <summary>
        /// Create a new dynamic file system
        /// </summary>
        /// <param name="dataCategory"></param>
        public DynamicFileSystem(DataCategory dataCategory) : this(null, dataCategory)
        {
        }

        /// <summary>
        /// Create new dynamic file system
        /// </summary>
        /// <param name="dataCategory"></param>
        /// <param name="directory"></param>
        public DynamicFileSystem(string directory = null, DataCategory dataCategory = DataCategory.Data)
        {
            DataCategory = dataCategory;
            if (!string.IsNullOrEmpty(directory))
            {
                Directory = Path.Combine(PathUtils.GetDataDirectory(dataCategory), directory);
            }
            else
            {
                Directory = PathUtils.GetDataDirectory(dataCategory);
            }
        }

        /// <summary>
        /// Read object from specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public override IPromise<TValue> ReadObject<TValue>(string path, bool create = false)
        {
            return Load(GetDataFile<TValue>(path), create);
        }

        /// <summary>
        /// Read object of specified type from specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public override IPromise<object> ReadObject(Type type, string path, bool create = false)
        {
            return Load(GetDataFile(type, path), create);
        }

        /// <summary>
        /// Read multiple objects of specified type from specified paths
        /// </summary>
        /// <param name="type"></param>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override IPromise<IEnumerable<object>> ReadObjects(Type type, IEnumerable<string> paths, bool create = false, string prefix = null)
        {
            List<IPromise<object>> promises = new List<IPromise<object>>();

            foreach (string key in paths)
            {
                string prefixedPath = key;
                if (!string.IsNullOrEmpty(prefix))
                {
                    prefixedPath = Path.Combine(prefix, key);
                }

                promises.Add(ReadObject(type, prefixedPath, create));
            }

            return Load(promises);
        }

        /// <summary>
        /// Read objects of generic type from specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override IPromise<IEnumerable<TValue>> ReadObjects<TValue>(IEnumerable<string> paths, bool create = false, string prefix = null)
        {
            Promise<IEnumerable<TValue>> promise = new Promise<IEnumerable<TValue>>();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                List<IPromise<TValue>> promises = new List<IPromise<TValue>>();

                foreach (string path in paths)
                {
                    string prefixedPath = path;
                    if (!string.IsNullOrEmpty(prefix))
                    {
                        prefixedPath = Path.Combine(prefix, path);
                    }

                    promises.Add(ReadObject<TValue>(prefixedPath, create));
                }

                Load(promises).Done(promise.Resolve, promise.Reject);
                return true;
            });

            return promise;
        }

        /// <summary>
        /// Read objects of specified types and paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override IPromise<IEnumerable<object>> ReadObjects(IDictionary<Type, string> objects, bool create = false, string prefix = null)
        {
            Promise<IEnumerable<object>> promise = new Promise<IEnumerable<object>>();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                List<IPromise<object>> promises = new List<IPromise<object>>();

                foreach (KeyValuePair<Type, string> kvp in objects)
                {
                    string path = kvp.Value;
                    if (!string.IsNullOrEmpty(prefix))
                    {
                        path = Path.Combine(prefix, kvp.Value);
                    }

                    promises.Add(ReadObject(kvp.Key, path, create));
                }

                Load(promises).Done(promise.Resolve, promise.Reject);
                return true;
            });

            return promise;
        }

        /// <summary>
        /// Write object to the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override IPromise WriteObject(string path, object @object)
        {
            return Save(GetDataFile(@object.GetType(), path), @object);
        }

        /// <summary>
        /// Write object of generic type to the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override IPromise WriteObject<TValue>(string path, TValue @object)
        {
            return Save(GetDataFile<TValue>(path), @object);
        }

        /// <summary>
        /// Write objects to the specified paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override IPromise WriteObjects(IDictionary<string, object> objects, string prefix = null)
        {
            Promise promise = new Promise();

            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                List<IPromise> promises = new List<IPromise>();

                foreach (KeyValuePair<string, object> kvp in objects)
                {
                    string path = kvp.Key;
                    if (!string.IsNullOrEmpty(prefix))
                    {
                        path = Path.Combine(prefix, kvp.Key);
                    }

                    promises.Add(WriteObject(path, kvp.Value));
                }

                Save(promises).Done(promise.Resolve, promise.Reject);
                return true;
            });

            return promise;
        }

        /// <summary>
        /// Write objects of the generic type to the specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override IPromise WriteObjects<TValue>(IDictionary<string, TValue> objects, string prefix = null)
        {
            Promise promise = new Promise();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                List<IPromise> promises = new List<IPromise>();

                foreach (KeyValuePair<string, TValue> kvp in objects)
                {
                    string path = kvp.Key;
                    if (!string.IsNullOrEmpty(prefix))
                    {
                        path = Path.Combine(prefix, kvp.Key);
                    }

                    promises.Add(WriteObject(path, kvp.Value));
                }

                Save(promises).Done(promise.Resolve, promise.Reject);
                return true;
            });
            return promise;
        }

        /// <summary>
        /// Load object of the generic type from the specified data file
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dataFile"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        private IPromise<TValue> Load<TValue>(IDataFile<TValue> dataFile, bool create = false) where TValue : class
        {
            return GetDataFileSystem<TValue>().Load(dataFile, create);
        }

        /// <summary>
        /// Load the specified promises
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="promises"></param>
        /// <returns></returns>
        private IPromise<IEnumerable<TValue>> Load<TValue>(IEnumerable<IPromise<TValue>> promises) where TValue : class
        {
            return GetDataFileSystem<TValue>().Load(promises);
        }

        /// <summary>
        /// Save object of generic type and object to the specified data file
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dataFile"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public IPromise Save<TValue>(IDataFile<TValue> dataFile, TValue @object) where TValue : class
        {
            return GetDataFileSystem<TValue>().Save(dataFile, @object);
        }

        /// <summary>
        /// Save the specified promises
        /// </summary>
        /// <param name="promises"></param>
        /// <returns></returns>
        private IPromise Save(IEnumerable<IPromise> promises)
        {
            return GetDataFileSystem<object>().Save(promises);
        }

        /// <summary>
        /// Gets data file system of the generic type
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <returns></returns>
        protected IDataFileSystem<TValue> GetDataFileSystem<TValue>() where TValue : class
        {
            bool found;
            object dataFileSystem;
            lock (fileSystemLock)
            {
                found = DataFileSystems.TryGetValue(typeof(TValue), out dataFileSystem);
            }

            if (found)
            {
                return dataFileSystem as IDataFileSystem<TValue>;
            }

            dataFileSystem = new DataFileSystem<TValue>(Directory);

            lock (fileSystemLock)
            {
                DataFileSystems.Add(typeof(TValue), dataFileSystem);
            }

            return (IDataFileSystem<TValue>)dataFileSystem;
        }

        /// <summary>
        /// Gets data file system of the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected IDataFileSystem<object> GetDataFileSystem(Type type)
        {
            bool found;
            object dataFileSystem;

            lock (fileSystemLock)
            {
                found = DataFileSystems.TryGetValue(type, out dataFileSystem);
            }
            if (found)
            {
                return dataFileSystem as IDataFileSystem<object>;
            }

            dataFileSystem = new DataFileSystem(type, Directory);

            lock (fileSystemLock)
            {
                DataFileSystems.Add(type, dataFileSystem);
            }

            return (IDataFileSystem<object>)dataFileSystem;
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile<TValue>(string path) where TValue : class
        {
            return GetDataFileSystem<TValue>().GetDataFile(path);
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type, string path)
        {
            return GetDataFileSystem(type).GetDataFile(path);
        }

        /// <summary>
        /// Check if data file exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Exists(string path = null)
        {
            return GetDataFileSystem<object>().Exists(path);
        }

        /// <summary>
        /// Check if data file exists
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Exists<TValue>(string path = null) where TValue : class
        {
            return GetDataFileSystem<TValue>().Exists(path);
        }

        /// <summary>
        /// Check if data file exists
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Exists(Type type, string path = null)
        {
            return GetDataFileSystem(type).Exists(path);
        }

        /// <summary>
        /// Gets enumerable of files within directory
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles(string path = null)
        {
            return GetDataFileSystem<object>().GetFiles(path);
        }

        /// <summary>
        /// Gets enumerable of files within directory
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles<TValue>(string path = null) where TValue : class
        {
            return GetDataFileSystem<TValue>().GetFiles(path);
        }

        /// <summary>
        /// Gets enumerable of files within directory
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles(Type type, string path = null)
        {
            return GetDataFileSystem(type).GetFiles(path);
        }

        /// <summary>
        /// Delete file(s) matching the following path or path pattern
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public void Delete(string path)
        {
            GetDataFileSystem<object>().Delete(path);
        }

        /// <summary>
        /// Delete file(s) matching the following paths or path patterns
        /// </summary>
        /// <param name="path"></param>
        public void Delete(IEnumerable<string> paths)
        {
            IDataFileSystem<object> dataFileSystem = GetDataFileSystem<object>();
            foreach (string path in paths)
            {
                dataFileSystem.Delete(path);
            }
        }

        /// <summary>
        /// Delete file(s) matching the following path or path pattern
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public void Delete<TValue>(string path) where TValue : class
        {
            GetDataFileSystem<TValue>().Delete(path);
        }

        /// <summary>
        /// Delete file(s) matching the following path or path pattern
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public void Delete(Type type, string path)
        {
            GetDataFileSystem(type).Delete(path);
        }
    }

    /// <summary>
    /// Represents a data file system
    /// </summary>
    public class DataFileSystem : DataFileSystem<object>
    {
        /// <summary>
        /// Gets the object type associated with this file system
        /// </summary>
        public Type ObjectType { get; }

        /// <summary>
        /// Create new data file system for the specified type
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="dataCategory"></param>
        public DataFileSystem(Type objectType, DataCategory dataCategory = DataCategory.Data) : base(DataSystem.GetDataFormat(objectType), dataCategory)
        {
            ObjectType = objectType;
        }

        /// <summary>
        /// Create a new data file system for the specified type and for the specified directory
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="directory"></param>
        public DataFileSystem(Type objectType, string directory) : base(directory, DataSystem.GetDataFormat(objectType))
        {
            ObjectType = objectType;
        }

        /// <summary>
        /// Create a new data file system for the specified data category
        /// </summary>
        /// <param name="dataCategory"></param>
        public DataFileSystem(DataCategory dataCategory = DataCategory.Data) : base(dataCategory)
        {
        }

        /// <summary>
        /// Create a new data file system for the specified directory
        /// </summary>
        /// <param name="directory"></param>
        public DataFileSystem(string directory) : base(directory)
        {
        }

        /// <summary>
        /// Make a data file for the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected override IDataFile<object> MakeDataFile(string path)
        {
            return DataSystem.MakeDataFile<object>(Path.Combine(Directory, path), DataFormat, ObjectType);
        }
    }

    /// <summary>
    /// Represents a generic data file system
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class DataFileSystem<TValue> : DataSystem<string, TValue>, IDataFileSystem<TValue> where TValue : class
    {
        /// <summary>
        /// Data category of file system
        /// </summary>
        protected readonly DataCategory DataCategory;

        /// <summary>
        /// Directory path of the file system
        /// </summary>
        protected readonly string Directory;

        /// <summary>
        /// Gets dictionary of the data file systems
        /// </summary>
        protected IDictionary<string, IDataFile<TValue>> DataFiles = new Dictionary<string, IDataFile<TValue>>();

        /// <summary>
        /// Create data file system for the specified directory
        /// </summary>
        /// <param name="directory"></param>
        public DataFileSystem(string directory)
        {
            Directory = directory;
            DataCategory = PathUtils.GetDataCategory(directory);
        }

        /// <summary>
        /// Create data file system for the specified directory using the specified data format
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="dataFormat"></param>
        public DataFileSystem(string directory, DataFormat dataFormat) : base(dataFormat)
        {
            Directory = directory;
            DataCategory = PathUtils.GetDataCategory(directory);
        }

        /// <summary>
        /// Create data file system for the specified data category
        /// </summary>
        /// <param name="dataCategory"></param>
        public DataFileSystem(DataCategory dataCategory = DataCategory.Data)
        {
            DataCategory = dataCategory;
            Directory = PathUtils.GetDataDirectory(dataCategory);
        }

        /// <summary>
        /// Create data file system for the specified data format and data category
        /// </summary>
        /// <param name="dataFormat"></param>
        /// <param name="dataCategory"></param>
        public DataFileSystem(DataFormat dataFormat, DataCategory dataCategory = DataCategory.Data) : base(dataFormat)
        {
            DataCategory = dataCategory;
            Directory = PathUtils.GetDataDirectory(dataCategory);
        }

        /// <summary>
        /// Read object from the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public override IPromise<TValue> ReadObject(string path, bool create = false)
        {
            return Load(GetDataFile(path), create);
        }

        /// <summary>
        /// Read objects from the specified paths
        /// </summary>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public override IPromise<IEnumerable<TValue>> ReadObjects(IEnumerable<string> paths, bool create = false)
        {
            List<IPromise<TValue>> promises = new List<IPromise<TValue>>();

            foreach (string key in paths)
            {
                promises.Add(ReadObject(key, create));
            }

            return Load(promises);
        }

        /// <summary>
        /// Write object to the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override IPromise WriteObject(string path, TValue @object)
        {
            return Save(GetDataFile(path), @object);
        }

        /// <summary>
        /// Write objects to the specified paths
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public override IPromise WriteObjects(IDictionary<string, TValue> objects)
        {
            List<IPromise> promises = new List<IPromise>();

            foreach (KeyValuePair<string, TValue> kvp in objects)
            {
                promises.Add(WriteObject(kvp.Key, kvp.Value));
            }

            return Save(promises);
        }

        /// <summary>
        /// Load the specified data file
        /// </summary>
        /// <param name="dataFile"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<TValue> Load(IDataFile<TValue> dataFile, bool create = false)
        {
            Promise<TValue> promise = new Promise<TValue>();

            DataFile.Load<IDataFile<TValue>>(dataFile)
                .Done(delegate (IDataFile<TValue> df)
                {
                    promise.Resolve(df.Object);
                }, delegate (Exception exception)
                {
                    if (create && exception is FileNotFoundException)
                    {
                        try
                        {
                            dataFile.Object = (TValue)Activator.CreateInstance(dataFile.ObjectType ?? typeof(TValue));
                            DataFile.Save<IDataFile<TValue>>(dataFile).Done(delegate (IDataFile<TValue> result)
                            {
                                promise.Resolve(result.Object);
                            }, delegate (Exception ex2)
                            {
                                promise.Reject(ex2);
                            });
                        }
                        catch (Exception e)
                        {
                            promise.Reject(e);
                        }
                    }
                    else
                    {
                        promise.Reject(exception);
                    }
                });

            return promise;
        }

        /// <summary>
        /// Load the specified promises
        /// </summary>
        /// <param name="promises"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<TValue>> Load(IEnumerable<IPromise<TValue>> promises)
        {
            Promise<IEnumerable<TValue>> promise = new Promise<IEnumerable<TValue>>();

            List<TValue> results = new List<TValue>();

            int total = promises.Count();
            int count = 0;
            Exception lastException = null;
            foreach (IPromise<TValue> dataFilePromise in promises)
            {
                dataFilePromise.Done(delegate (TValue value)
                {
                    count++;
                    results.Add(value);
                    if (count >= total)
                    {
                        if (lastException != null)
                        {
                            promise.Reject(lastException);
                        }
                        else
                        {
                            promise.Resolve(results);
                        }
                    }
                }, delegate (Exception ex)
                {
                    count++;

                    if (count >= total)
                    {
                        promise.Reject(ex);
                    }
                    else
                    {
                        lastException = ex;
                    }
                });
            }

            return promise;
        }

        /// <summary>
        /// Save the specified data file with the specified object
        /// </summary>
        /// <param name="dataFile"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public IPromise Save(IDataFile<TValue> dataFile, TValue @object)
        {
            dataFile.Object = @object;
            return Save(dataFile);
        }

        /// <summary>
        /// Save the specified data file
        /// </summary>
        /// <param name="dataFile"></param>
        /// <returns></returns>
        public IPromise Save(IDataFile<TValue> dataFile)
        {
            Promise promise = new Promise();
            DataFile.Save<IDataFile<TValue>>(dataFile)
                .Done(delegate (IDataFile<TValue> df)
                {
                    if (df != null)
                    {
                        promise.Resolve();
                    }
                }, delegate (Exception exception)
                {
                    promise.Reject(exception);
                });

            return promise;
        }

        /// <summary>
        /// Save the specified promises
        /// </summary>
        /// <param name="promises"></param>
        /// <returns></returns>
        public IPromise Save(IEnumerable<IPromise> promises)
        {
            Promise promise = new Promise();

            int total = promises.Count();
            int count = 0;
            Exception lastException = null;
            foreach (IPromise dataFilePromise in promises)
            {
                dataFilePromise.Then(delegate
                {
                    count++;
                    if (count >= total)
                    {
                        if (lastException != null)
                        {
                            promise.Reject(lastException);
                        }
                        else
                        {
                            promise.Resolve();
                        }
                    }
                }, delegate (Exception ex)
                {
                    count++;

                    if (count >= total)
                    {
                        promise.Reject(ex);
                    }
                    else
                    {
                        lastException = ex;
                    }
                });
            }

            return promise;
        }

        /// <summary>
        /// Gets data file for the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile(string path)
        {
            if (!DataFiles.TryGetValue(path, out IDataFile<TValue> dataFile))
            {
                DataFiles.Add(path, dataFile = MakeDataFile(path));
            }

            return dataFile;
        }

        /// <summary>
        /// Make data file for the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected virtual IDataFile<TValue> MakeDataFile(string path)
        {
            return DataSystem.MakeDataFile<TValue>(Path.Combine(Directory, path), DataFormat);
        }

        /// <summary>
        /// Checks if file exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool Exists(string path = null)
        {
            if (!string.IsNullOrEmpty(path))
            {
                path = PathUtils.AssertValid(Path.Combine(Directory, path));

                string extension = Path.GetExtension(path);
                if (!string.IsNullOrEmpty(extension))
                {
                    return File.Exists(path);
                }

                extension = DataSystem.GetDataExtension(DataFormat);
                if (!string.IsNullOrEmpty(extension))
                {
                    path += $".{extension}";
                }

                return File.Exists(path);
            }

            path = PathUtils.AssertValid(Directory);
            return System.IO.Directory.Exists(path);
        }

        /// <summary>
        /// Delete file(s) matching the following path or path pattern
        /// </summary>
        /// <param name="path"></param>
        public void Delete(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("Path is missing or invalid", nameof(path));
            }

            path = PathUtils.AssertValid(Path.Combine(Directory, path));

            if (File.Exists(path))
            {
                File.Delete(path);
            }
            else
            {
                string[] paths = System.IO.Directory.GetFiles(Directory, path);
                foreach (string searchedPath in paths)
                {
                    File.Delete(searchedPath);
                }
            }
        }

        /// <summary>
        /// Gets enumerable of files within directory
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles(string path = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                path = "*.*";
            }

            return System.IO.Directory.GetFiles(Directory, path).Select(x => x.Replace(Directory, string.Empty));
        }
    }

    /// <summary>
    /// Represents a generic dynamic data system
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public abstract class DynamicDataSystem<TKey> : IDynamicDataSystem<TKey>
    {
        public abstract IPromise<TValue> ReadObject<TValue>(TKey key, bool create = false) where TValue : class;

        public abstract IPromise<object> ReadObject(Type type, TKey key, bool create = false);

        public abstract IPromise<IEnumerable<TValue>> ReadObjects<TValue>(IEnumerable<TKey> keys, bool create = false, string prefix = null) where TValue : class;

        public abstract IPromise<IEnumerable<object>> ReadObjects(Type type, IEnumerable<TKey> keys, bool create = false, string prefix = null);

        public abstract IPromise<IEnumerable<object>> ReadObjects(IDictionary<Type, TKey> objects, bool create = false, string prefix = null);

        public abstract IPromise WriteObject<TValue>(TKey key, TValue @object) where TValue : class;

        public abstract IPromise WriteObject(TKey key, object @object);

        public abstract IPromise WriteObjects<TValue>(IDictionary<TKey, TValue> objects, string prefix = null) where TValue : class;

        public abstract IPromise WriteObjects(IDictionary<TKey, object> objects, string prefix = null);
    }

    /// <summary>
    /// Represents a generic data system
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public abstract class DataSystem<TKey, TValue> : IDataSystem<TKey, TValue> where TValue : class
    {
        /// <summary>
        /// DataFormat associated with data system
        /// </summary>
        protected readonly DataFormat DataFormat;

        /// <summary>
        /// Create a new data system
        /// </summary>
        protected DataSystem()
        {
            if (typeof(TValue) != typeof(object))
            {
                DataFormat = DataSystem.GetDataFormat<TValue>();
            }
            else
            {
                DataFormat = DataFormat.Binary;
            }
        }

        /// <summary>
        /// Create a new data system for the specified data format
        /// </summary>
        /// <param name="dataFormat"></param>
        protected DataSystem(DataFormat dataFormat)
        {
            DataFormat = dataFormat;
        }

        public abstract IPromise<TValue> ReadObject(TKey key, bool create = false);

        public abstract IPromise<IEnumerable<TValue>> ReadObjects(IEnumerable<TKey> keys, bool create = false);

        public abstract IPromise WriteObject(TKey key, TValue @object);

        public abstract IPromise WriteObjects(IDictionary<TKey, TValue> objects);
    }

    /// <summary>
    /// Represents a data system
    /// </summary>
    public static class DataSystem
    {
        /// <summary>
        /// Gets data format for the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static DataFormat GetDataFormat<T>()
        {
            return GetDataFormat(typeof(T));
        }

        /// <summary>
        /// Gets data format for the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DataFormat GetDataFormat(Type type)
        {
            if (type.IsClass && type.GetCustomAttributesIncludingBaseInterfaces<DataAttribute>() is IEnumerable<DataAttribute> dataAttributes)
            {
                foreach (DataAttribute dataAttribute in dataAttributes)
                {
                    switch (dataAttribute)
                    {
                        case TomlAttribute _:
                            return DataFormat.Toml;

                        case JsonAttribute _:
                            return DataFormat.Json;

                        case BinaryAttribute _:
                            return DataFormat.Binary;

                        case ProtoAttribute _:
                            return DataFormat.Proto;
                    }

                    /*else if (dataAttribute is TableAttribute)
                    {
                        return DataFormat.Sql;
                    }*/
                }
            }

            return DataFormat.Json;
        }

        /// <summary>
        /// Gets data format for the specified extension
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static DataFormat GetDataFormat(string extension)
        {
            switch (extension.ToLower())
            {
                case "toml":
                case ".toml":
                    return DataFormat.Toml;

                case "json":
                case ".json":
                    return DataFormat.Json;

                case "dat":
                case ".dat":
                    return DataFormat.Binary;

                case "data":
                case ".data":
                    return DataFormat.Proto;

                case "txt":
                case ".txt":
                    return DataFormat.Text;

                    /*
                    case "db":
                    case ".db":
                        return DataFormat.Sql;
                    */
            }

            return default;
        }

        /// <summary>
        /// Gets file extension for the specified data format
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string GetDataExtension(DataFormat dataFormat)
        {
            switch (dataFormat)
            {
                case DataFormat.Toml:
                    return "toml";

                case DataFormat.Json:
                    return "json";

                case DataFormat.Binary:
                    return "dat";

                case DataFormat.Proto:
                    return "data";

                case DataFormat.Text:
                    return "txt";
            }

            return default;
        }

        /// <summary>
        /// Make data file for the specified path and data format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="dataFormat"></param>
        /// <returns></returns>
        public static IDataFile<T> MakeDataFile<T>(string path, DataFormat dataFormat) where T : class
        {
            return MakeDataFile<T>(path, dataFormat, null, null);
        }

        /// <summary>
        /// Make data file for the specified path and data format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="dataFormat"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public static IDataFile<T> MakeDataFile<T>(string path, DataFormat dataFormat, Type objectType) where T : class
        {
            return MakeDataFile<T>(path, dataFormat, objectType, null);
        }

        /// <summary>
        /// Make data file for the specified path and data format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="dataFormat"></param>
        /// <param name="defaultData"></param>
        /// <returns></returns>
        public static IDataFile<T> MakeDataFile<T>(string path, DataFormat dataFormat, T defaultData) where T : class
        {
            return MakeDataFile(path, dataFormat, null, defaultData);
        }

        /// <summary>
        /// Make data file for the specified path and data format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="dataFormat"></param>
        /// <param name="objectType"></param>
        /// <param name="defaultData"></param>
        /// <returns></returns>
        public static IDataFile<T> MakeDataFile<T>(string path, DataFormat dataFormat, Type objectType = null, T defaultData = null) where T : class
        {
            switch (dataFormat)
            {
                case DataFormat.Binary:
                    return new BinaryFile<T>(path, objectType, defaultData);

                case DataFormat.Json:
                    return new JsonFile<T>(path, objectType, defaultData);

                case DataFormat.Toml:
                    return new TomlFile<T>(path, objectType, defaultData);

                case DataFormat.Proto:
                    return new ProtoFile<T>(path, objectType, defaultData);
            }

            throw new InvalidOperationException($"Cannot make DataFile with format {dataFormat}");
        }

        /// <summary>
        /// Get absolute file path without file extension
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal static string GetFullPathWithoutExtension(string path)
        {
            return Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path));
        }

        /// <summary>
        /// Convert file at specified path to specified data format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static IDataFile<T> Convert<T>(string path, DataFormat format) where T : class
        {
            DataFormat fromFormat = GetDataFormat(Path.GetExtension(path));

            if (fromFormat != default)
            {
                IDataFile<T> fromFile = MakeDataFile<T>(path, fromFormat);
                fromFile.Load();
                return Convert(fromFile, format);
            }

            throw new ArgumentException($"Path {path} is not a valid data file");
        }

        /// <summary>
        /// Convert data file to specified data format
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static IDataFile<T> Convert<T>(IDataFile<T> file, DataFormat format) where T : class
        {
            IDataFile<T> toFile = MakeDataFile<T>(GetFullPathWithoutExtension(file.Path), format);
            toFile.Object = file.Object;
            return toFile;
        }
    }
}
