﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using uMod.Common;

namespace uMod.IO
{
    /// <summary>
    /// Represents possible file change states
    /// </summary>
    public enum FileChangeType
    {
        Created,
        Deleted,
        Changed
    }

    /// <summary>
    /// Represents a filesystem watcher
    /// </summary>
    public sealed class FileSystemWatcher : IDisposable
    {
        private readonly DirectoryInfo _directoryInfo;
        private readonly ILogger _logger;

        /// <summary>
        /// Root path being watched
        /// </summary>
        public string Path { get; }

        /// <summary>
        /// File filter
        /// </summary>
        public string Filter { get; }

        private readonly string[] _filters;
        private readonly Module _module;

        /// <summary>
        /// Event notified when changes occur
        /// </summary>
        public Event<string, FileChangeType> Notify { get; } = new Event<string, FileChangeType>();

        /// <summary>
        /// Current file state
        /// </summary>
        public Dictionary<string, DateTime> Files = new Dictionary<string, DateTime>();

        /// <summary>
        /// Create a new filesystem watcher
        /// </summary>
        /// <param name="module"></param>
        /// <param name="logger"></param>
        /// <param name="path"></param>
        /// <param name="filter"></param>
        public FileSystemWatcher(Module module, ILogger logger, string path, string filter = "*.*")
        {
            _logger = logger;
            Path = path;
            Filter = filter;
            _module = module;
            if (!string.IsNullOrEmpty(Filter) && Filter.IndexOf(',') > -1)
            {
                _filters = Filter.Split(',');
            }
            _directoryInfo = new DirectoryInfo(path);
            Initialize();
        }

        private readonly List<FileInfo> _fileList = new List<FileInfo>();

        /// <summary>
        /// Gets enumerable list of files
        /// </summary>
        /// <returns></returns>
        private IEnumerable<FileInfo> GetFiles()
        {
            if (_filters != null)
            {
                _fileList.Clear();
                foreach (string filter in _filters)
                {
                    _fileList.AddRange(_directoryInfo.GetFiles(filter.Trim(), SearchOption.AllDirectories));
                }

                return _fileList;
            }

            _fileList.Clear();
            _fileList.AddRange(_directoryInfo.GetFiles(Filter, SearchOption.AllDirectories));
            return _fileList;
        }

        /// <summary>
        /// Create initial file state
        /// </summary>
        private void Initialize()
        {
            IEnumerable<FileInfo> files = GetFiles();

            foreach (FileInfo fileInfo in files)
            {
                Files.Add(fileInfo.FullName, fileInfo.LastWriteTime);
            }

            ThreadPool.QueueUserWorkItem(ProcessSeperateThread);
        }

        private bool _exit;

        /// <summary>
        /// Handle separate thread processing
        /// </summary>
        /// <param name="context"></param>
        private void ProcessSeperateThread(object context)
        {
            while (true)
            {
                Thread.Sleep(100);
                if (_module.IsShuttingDown || _exit)
                {
                    return;
                }

                Handle();
            }
        }

        public void Close()
        {
            _exit = true;
        }

        /// <summary>
        /// Handle change notification
        /// </summary>
        private void Handle()
        {
            try
            {
                NotifyChanges();
            }
            catch (Exception e)
            {
                _logger.Report("Exception in FileSystemWatcher", e);
                throw;
            }
        }

        /// <summary>
        /// Notify changes
        /// </summary>
        private void NotifyChanges()
        {
            IEnumerable<FileInfo> files = GetFiles();
            List<string> foundFiles = Pooling.Pools.GetList<string>();
            List<string> deletedFiles = null;

            try
            {
                foreach (FileInfo fileInfo in files)
                {
                    if (Files.TryGetValue(fileInfo.FullName, out DateTime dateTime))
                    {
                        if (fileInfo.LastWriteTime != dateTime)
                        {
                            // file modified
                            Notify.Invoke(fileInfo.FullName, FileChangeType.Changed);

                            Files[fileInfo.FullName] = fileInfo.LastWriteTime;
                        }
                    }
                    else
                    {
                        // file created
                        Notify.Invoke(fileInfo.FullName, FileChangeType.Created);
                        Files.Add(fileInfo.FullName, fileInfo.LastWriteTime);
                    }

                    foundFiles.Add(fileInfo.FullName);
                }

                foreach (KeyValuePair<string, DateTime> kvp in Files)
                {
                    if (foundFiles.Contains(kvp.Key))
                    {
                        continue;
                    }

                    // file deleted
                    Notify.Invoke(kvp.Key, FileChangeType.Deleted);

                    if (deletedFiles == null)
                    {
                        deletedFiles = new List<string>();
                    }

                    deletedFiles.Add(kvp.Key);
                }

                if (!(deletedFiles?.Count > 0))
                {
                    return;
                }

                foreach (string deletedPath in deletedFiles)
                {
                    Files.Remove(deletedPath);
                }
            }
            finally
            {
                Pooling.Pools.FreeList(ref foundFiles);
            }
        }

        public void Dispose()
        {
            Close();
        }
    }
}
