﻿extern alias References;

using System;
using References::Newtonsoft.Json;

namespace uMod.IO
{
    /// <summary>
    /// Represents a generic json file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonFile<T> : JsonFile, IDataFile<T> where T : class
    {
        private T _object;

        /// <summary>
        /// Generic object data
        /// </summary>
        [JsonIgnore]
        public T Object
        {
            get => _object; set
            {
                _object = value;
                ObjectType = value?.GetType();
            }
        }

        /// <summary>
        /// Object type
        /// </summary>
        [JsonIgnore]
        public Type ObjectType { get; private set; }

        /// <summary>
        /// Create a new json file
        /// </summary>
        /// <param name="filename"></param>
        public JsonFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Create a new json file of the specified type with optional default data and serializer settings
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <param name="data"></param>
        /// <param name="settings"></param>
        public JsonFile(string filename, Type type, T data = null, JsonSerializerSettings settings = null) : base(filename, settings)
        {
            Object = data;
            ObjectType = type ?? data?.GetType();
        }

        /// <summary>
        /// Create a new json file with default data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        public JsonFile(string filename, T data) : this(filename, data.GetType(), data)
        {
        }

        /// <summary>
        /// Create a new json file with default data and optional serializer settings
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        /// <param name="settings"></param>
        public JsonFile(string filename, T data, JsonSerializerSettings settings = null) : this(filename, data.GetType(), data, settings)
        {
        }

        /// <summary>
        /// Serialize object to json string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(Object, Formatting.Indented, Settings);
        }

        /// <summary>
        /// Deserialize object from json string
        /// </summary>
        /// <param name="json"></param>
        public override void FromString(string json)
        {
            if (Object == null)
            {
                if (typeof(T) == typeof(object))
                {
                    Object = (T)JsonConvert.DeserializeObject(json, ObjectType, Settings);
                }
                else
                {
                    Object = JsonConvert.DeserializeObject<T>(json, Settings);
                }
            }
            else
            {
                JsonConvert.PopulateObject(json, Object, Settings);
            }
        }

        /// <summary>
        /// Convert json file to specified data format
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert(DataFormat format)
        {
            return DataSystem.Convert(this, format);
        }

        /// <summary>
        /// Converts data file to specified data format with data file cast
        /// </summary>
        /// <typeparam name="TDataFile"></typeparam>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert<TDataFile>(DataFormat format) where TDataFile : IDataFile<T>
        {
            return (TDataFile)Convert(format);
        }

        /// <summary>
        /// Load json file asynchronously
        /// </summary>
        /// <returns></returns>
        public IPromise<T> LoadAsync()
        {
            Promise<T> promise = new Promise<T>();
            LoadFile().Done(delegate (IDataFile<T> file)
            {
                promise.Resolve(Object);
            }, promise.Reject);
            return promise;
        }

        /// <summary>
        /// Save json file asynchronously
        /// </summary>
        /// <returns></returns>
        public IPromise<T> SaveAsync()
        {
            Promise<T> promise = new Promise<T>();
            SaveFile().Done(delegate (IDataFile<T> file)
            {
                promise.Resolve(Object);
            }, promise.Reject);
            return promise;
        }

        public new IPromise<IDataFile<T>> LoadFile()
        {
            return Load<IDataFile<T>>(this);
        }

        public new IPromise<IDataFile<T>> SaveFile()
        {
            return Save<IDataFile<T>>(this);
        }
    }

    /// <summary>
    /// Represents a json file
    /// </summary>
    public class JsonFile : DataFile
    {
        /// <summary>
        /// Gets default serializer settings
        /// </summary>
        /// <returns></returns>
        public static JsonSerializerSettings GetDefaultJsonSerializerSettings()
        {
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings
            {
                ObjectCreationHandling = ObjectCreationHandling.Replace
            };
            serializerSettings.Converters.Add(new JsonKeyValuesConverter());

            return serializerSettings;
        }

        /// <summary>
        /// Gets Json serialization settings
        /// </summary>
        [JsonIgnore]
        protected readonly JsonSerializerSettings Settings;

        /// <summary>
        /// Gets json file extension
        /// </summary>
        [JsonIgnore]
        public override string Extension => "json";

        /// <summary>
        /// Create new json file
        /// </summary>
        /// <param name="filename"></param>
        public JsonFile(string filename) : base(filename)
        {
            Settings = GetDefaultJsonSerializerSettings();
        }

        /// <summary>
        /// Create new json file with specified serializer settings
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="settings"></param>
        public JsonFile(string filename, JsonSerializerSettings settings = null) : base(filename)
        {
            Settings = settings ?? GetDefaultJsonSerializerSettings();
        }

        /// <summary>
        /// Serialize json file to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented, Settings);
        }

        /// <summary>
        /// Deserialize json object from string
        /// </summary>
        /// <param name="json"></param>
        public override void FromString(string json)
        {
            JsonConvert.PopulateObject(json, this, Settings);
        }
    }
}
