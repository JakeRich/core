﻿using System.Collections;
using System.Collections.Generic;
using Tommy;
using uMod.Configuration.Toml;

namespace uMod.IO
{
    /// <summary>
    /// Represents a synchronized TOML file
    /// </summary>
    public class TomlSyncFile : TomlFile, IEnumerable<KeyValuePair<string, TomlNode>>
    {
        /// <summary>
        /// Gets TOML table
        /// </summary>
        [TomlIgnore]
        protected TomlTable Table;

        /// <summary>
        /// Create new TOML synchronized file
        /// </summary>
        /// <param name="filename"></param>
        public TomlSyncFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TomlNode this[string key]
        {
            get => Table?[key];
            set
            {
                if (Table == null)
                {
                    Table = new TomlTable();
                }
                Table[key] = value;
            }
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TomlNode this[int key]
        {
            get => Table?[key];
            set
            {
                if (Table == null)
                {
                    Table = new TomlTable();
                }

                Table[key] = value;
            }
        }

        /// <summary>
        /// Load configuration file asynchronously
        /// </summary>
        /// <returns></returns>
        public new IPromise<TomlSyncFile> LoadFile()
        {
            return Load<TomlSyncFile>(this);
        }

        /// <summary>
        /// Save configuration file asynchronously
        /// </summary>
        /// <returns></returns>
        public new IPromise<TomlSyncFile> SaveFile()
        {
            return Save<TomlSyncFile>(this);
        }

        /// <summary>
        /// Serialized object as TOML string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return TomlConvert.SerializeObject(Table);
        }

        /// <summary>
        /// Deserialize object from TOML string
        /// </summary>
        /// <param name="toml"></param>
        public override void FromString(string toml)
        {
            Table = TomlConvert.DeserializeObject<TomlTable>(toml);
        }

        #region IEnumerable

        public IEnumerator<KeyValuePair<string, TomlNode>> GetEnumerator() => Table.RawTable.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => Table.RawTable.GetEnumerator();

        #endregion IEnumerable
    }
}
