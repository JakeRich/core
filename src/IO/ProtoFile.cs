﻿extern alias References;

using System;
using System.IO;
using References::ProtoBuf;

namespace uMod.IO
{
    /// <summary>
    /// Represents a protobuf file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ProtoFile<T> : DataFile<T> where T : class
    {
        private T _object;

        /// <summary>
        /// Gets or sets the object
        /// </summary>
        public override T Object
        {
            get => _object; set
            {
                _object = value;
                ObjectType = value?.GetType();
            }
        }

        /// <summary>
        /// Gets or sets the object type
        /// </summary>
        public override Type ObjectType { get; protected set; }

        /// <summary>
        /// Gets the extension associated with file type
        /// </summary>
        public override string Extension => "data";

        /// <summary>
        /// Create new binary file
        /// </summary>
        /// <param name="filename"></param>
        public ProtoFile(string filename) : base(filename)
        {
            AssertTypeSerializable();
        }

        /// <summary>
        /// Create new protobuf file of type and with optional default data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <param name="data"></param>
        public ProtoFile(string filename, Type type, T data = null) : this(filename)
        {
            Object = data;
            ObjectType = type ?? data?.GetType();
        }

        /// <summary>
        /// Create new protobuf file with optional default data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        public ProtoFile(string filename, T data) : this(filename, data.GetType(), data)
        {
        }

        /// <summary>
        /// Ensure object is serializable
        /// </summary>
        private void AssertTypeSerializable()
        {
            Utility.AssertSerializable<ProtoContractAttribute>(typeof(T));
        }

        /// <summary>
        /// Save file synchronously (not recommended)
        /// </summary>
        public override void Save()
        {
            string path = PathUtils.AssertValid(this.path);
            string dir = Utility.GetDirectoryName(path);
            if (dir != null && !Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            if (IsConfigFile && Interface.uMod?.Plugins?.Configuration != null && (Interface.uMod.Plugins?.Configuration?.Watchers?.ConfigWatchers ?? false))
            {
                Interface.uMod.ConfigChanges.Add(path);
            }

            using (FileStream file = File.Open(path, FileMode.Create))
            {
                Serializer.Serialize(file, Object);
            }

            //File.WriteAllText(path, ToString());
            OnWrite?.Invoke(this);
            IsLoaded = true;
        }

        /// <summary>
        /// Convert object to serialized string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Serializer.Serialize(stream, Object);
                stream.Flush();
                stream.Position = 0;
                return System.Convert.ToBase64String(stream.ToArray());
            }
        }

        /// <summary>
        /// Load file synchronously (not recommended)
        /// </summary>
        public override void Load()
        {
            string path = PathUtils.AssertValid(this.path);
            using (FileStream file = File.OpenRead(path))
            {
                Object = Serializer.Deserialize<T>(file);
            }
            OnRead?.Invoke(this);
            IsLoaded = true;
        }

        /// <summary>
        /// Deserialize protobuf string
        /// </summary>
        /// <param name="binary"></param>
        public override void FromString(string binary)
        {
            throw new NotImplementedException();
        }
    }
}
