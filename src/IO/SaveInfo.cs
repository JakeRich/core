﻿using System;
using System.IO;
using uMod.Common;

namespace uMod.IO
{
    /// <summary>
    /// Represents save info
    /// </summary>
    public class SaveInfo : ISaveInfo
    {
        private readonly string _fullPath;

        /// <summary>
        /// The name of the save file
        /// </summary>
        public string SaveName { get; }

        /// <summary>
        /// Get the save creation time local to the server
        /// </summary>
        public DateTime CreationTime { get; private set; }

        /// <summary>
        /// Get the save creation time in Unix format
        /// </summary>
        public uint CreationTimeUnix { get; private set; }

        /// <summary>
        /// Refresh the save creation time
        /// </summary>
        public void Refresh()
        {
            if (File.Exists(_fullPath))
            {
                CreationTime = File.GetCreationTime(_fullPath);
                CreationTimeUnix = Utility.Time.ToTimestamp(CreationTime);
            }
        }

        /// <summary>
        /// Create an empty SaveInfo object
        /// </summary>
        public SaveInfo()
        {
        }

        /// <summary>
        /// Create a SaveInfo object for a specified file
        /// </summary>
        /// <param name="filePath"></param>
        private SaveInfo(string filePath)
        {
            _fullPath = filePath;
            SaveName = Utility.GetFileNameWithoutExtension(filePath);
            Refresh();
        }

        /// <summary>
        /// Creates a new SaveInfo for a specifed file
        /// </summary>
        /// <param name="filePath">Full path to the save file</param>
        /// <returns></returns>
        public static SaveInfo Create(string filePath)
        {
            return !File.Exists(filePath) ? null : new SaveInfo(filePath);
        }
    }
}
