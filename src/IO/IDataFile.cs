﻿using System;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.IO
{
    /// <summary>
    /// Represents a data object
    /// </summary>
    public interface IDataObject
    {
        /// <summary>
        /// OnRead event
        /// </summary>
        IEvent<IDataObject> OnRead { get; }

        /// <summary>
        /// OnWrite event
        /// </summary>
        IEvent<IDataObject> OnWrite { get; }
    }

    /// <summary>
    /// Represents a data file
    /// </summary>
    public interface IDataFile : IDataObject
    {
        /// <summary>
        /// Gets the file extension
        /// </summary>
        string Extension { get; }

        /// <summary>
        /// Gets the file path
        /// </summary>
        string Path { get; }

        /// <summary>
        /// Gets the relative path
        /// </summary>
        string Filename { get; }

        /// <summary>
        /// Determines if the file was loaded
        /// </summary>
        bool IsLoaded { get; }

        /// <summary>
        /// Determines if data file exists
        /// </summary>
        bool Exists { get; }

        /// <summary>
        /// Load data file
        /// </summary>
        void Load();

        /// <summary>
        /// Save data file
        /// </summary>
        void Save();

        /// <summary>
        /// Delete data file
        /// </summary>
        void Delete();

        /// <summary>
        /// Load data from string
        /// </summary>
        /// <param name="format"></param>
        void FromString(string format);
    }

    /// <summary>
    /// Represents a generic data file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataFile<T> : IDataFile
    {
        /// <summary>
        /// Gets the generic object type
        /// </summary>
        Type ObjectType { get; }

        /// <summary>
        /// Gets the generic object
        /// </summary>
        T Object { get; set; }

        IDataFile<T> Convert(DataFormat format);

        IDataFile<T> Convert<TDataFile>(DataFormat format) where TDataFile : IDataFile<T>;

        IPromise<T> LoadAsync();

        IPromise<T> SaveAsync();

        IPromise<IDataFile<T>> LoadFile();

        IPromise<IDataFile<T>> SaveFile();
    }

    /// <summary>
    /// Represents a generic data system
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IDataSystem<TKey>
    {
        /// <summary>
        /// Read object from the specified path using the specified type with optional default creation
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<object> ReadObject(Type type, TKey path, bool create = false);

        /// <summary>
        /// Read objects from the specified paths using the specified type with option default creation
        /// </summary>
        /// <param name="type"></param>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<IEnumerable<object>> ReadObjects(Type type, IEnumerable<TKey> paths, bool create = false);

        /// <summary>
        /// Write object using the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        IPromise WriteObject(TKey path, object @object);

        /// <summary>
        /// Write objects to the specified path-object dictionary
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        IPromise WriteObjects(IDictionary<TKey, object> objects);
    }

    /// <summary>
    /// Represents a generic data system
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public interface IDataSystem<TKey, TValue> where TValue : class
    {
        /// <summary>
        /// Read object from the specified path with optional default creation
        /// </summary>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<TValue> ReadObject(TKey path, bool create = false);

        /// <summary>
        /// Read objects from the specified paths with optional default creation
        /// </summary>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<IEnumerable<TValue>> ReadObjects(IEnumerable<TKey> paths, bool create = false);

        /// <summary>
        /// Write object to the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        IPromise WriteObject(TKey path, TValue @object);

        /// <summary>
        /// Write object to the specified path-object dictionary
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        IPromise WriteObjects(IDictionary<TKey, TValue> objects);
    }

    /// <summary>
    /// Represents a data file system
    /// </summary>
    public interface IDataFileSystem : IDataSystem<string>
    {
        /// <summary>
        /// Get data file using the specified type and name
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        IDataFile<object> GetDataFile(Type type, string path);

        /// <summary>
        /// Load data file with optional creation
        /// </summary>
        /// <param name="dataFile"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<object> Load(IDataFile<object> dataFile, bool create = false);

        /// <summary>
        /// Load multiple data files
        /// </summary>
        /// <param name="promises"></param>
        /// <returns></returns>
        IPromise<IEnumerable<object>> Load(IEnumerable<IPromise<object>> promises);

        /// <summary>
        /// Save data file using specified object
        /// </summary>
        /// <param name="dataFile"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        IPromise Save(IDataFile<object> dataFile, object @object);

        /// <summary>
        /// Save data file
        /// </summary>
        /// <param name="dataFile"></param>
        /// <returns></returns>
        IPromise Save(IDataFile<object> dataFile);

        /// <summary>
        /// Save multiple data file promises
        /// </summary>
        /// <param name="promises"></param>
        /// <returns></returns>
        IPromise Save(IEnumerable<IPromise> promises);

        /// <summary>
        /// Checks if data file exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool Exists(string path = null);

        /// <summary>
        /// Delete file(s) matching the following path or path pattern
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        void Delete(string path);

        /// <summary>
        /// Gets enumerable of files within directory
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IEnumerable<string> GetFiles(string path = null);
    }

    /// <summary>
    /// Represents generic data file system
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public interface IDataFileSystem<TValue> : IDataSystem<string, TValue> where TValue : class
    {
        /// <summary>
        /// Get data file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IDataFile<TValue> GetDataFile(string path);

        /// <summary>
        /// Load data file with optional creation
        /// </summary>
        /// <param name="dataFile"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<TValue> Load(IDataFile<TValue> dataFile, bool create = false);

        /// <summary>
        /// Load multiple data files
        /// </summary>
        /// <param name="promises"></param>
        /// <returns></returns>
        IPromise<IEnumerable<TValue>> Load(IEnumerable<IPromise<TValue>> promises);

        /// <summary>
        /// Save data file using specified object
        /// </summary>
        /// <param name="dataFile"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        IPromise Save(IDataFile<TValue> dataFile, TValue @object);

        /// <summary>
        /// Save data file
        /// </summary>
        /// <param name="dataFile"></param>
        /// <returns></returns>
        IPromise Save(IDataFile<TValue> dataFile);

        /// <summary>
        /// Save multiple data file promises
        /// </summary>
        /// <param name="promises"></param>
        /// <returns></returns>
        IPromise Save(IEnumerable<IPromise> promises);

        /// <summary>
        /// Checks if data file exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        bool Exists(string path = null);

        /// <summary>
        /// Delete file(s) matching the following path or path pattern
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        void Delete(string path);

        /// <summary>
        /// Gets enumerable of files within directory
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IEnumerable<string> GetFiles(string path = null);
    }

    /// <summary>
    /// Represents a dynamic data system
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IDynamicDataSystem<TKey>
    {
        /// <summary>
        /// Read object from specified path with optional creation
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<TValue> ReadObject<TValue>(TKey path, bool create = false) where TValue : class;

        /// <summary>
        /// Read object from the specified path using the specifed type with optional creation
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        IPromise<object> ReadObject(Type type, TKey path, bool create = false);

        /// <summary>
        /// Read multiple objects from the specified paths with optional creation
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        IPromise<IEnumerable<TValue>> ReadObjects<TValue>(IEnumerable<TKey> paths, bool create = false, string prefix = null) where TValue : class;

        /// <summary>
        /// Read multiple objects from specified paths using the specified type with optional creation
        /// </summary>
        /// <param name="type"></param>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        IPromise<IEnumerable<object>> ReadObjects(Type type, IEnumerable<TKey> paths, bool create = false, string prefix = null);

        /// <summary>
        /// Read multiple objects from the specified type-path dictionary with optional cration
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        IPromise<IEnumerable<object>> ReadObjects(IDictionary<Type, TKey> objects, bool create = false, string prefix = null);

        /// <summary>
        /// Write object to the specified path with the specified object
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        IPromise WriteObject<TValue>(TKey path, TValue @object) where TValue : class;

        /// <summary>
        /// Write object to the specified path with the specified object
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        IPromise WriteObject(TKey path, object @object);

        /// <summary>
        /// Write multiple objects using the specified path-object dictionary
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        IPromise WriteObjects<TValue>(IDictionary<TKey, TValue> objects, string prefix = null) where TValue : class;

        /// <summary>
        /// Write multiple objects using the specified path-object dictionary
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        IPromise WriteObjects(IDictionary<TKey, object> objects, string prefix = null);
    }

    /// <summary>
    /// Represents a dynamic file system
    /// </summary>
    public interface IDynamicFileSystem : IDynamicDataSystem<string>
    {
        /// <summary>
        /// Get data file from the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        IDataFile<TValue> GetDataFile<TValue>(string path) where TValue : class;

        /// <summary>
        /// Get data file from the specified path using the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        IDataFile<object> GetDataFile(Type type, string path);
    }
}
