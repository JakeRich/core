﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace uMod.IO
{
    /// <summary>
    /// Represents a binary file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BinaryFile<T> : DataFile<T> where T : class
    {
        /// <summary>
        /// Gets the binary formatter
        /// </summary>
        private readonly BinaryFormatter _binaryFormatter;

        private T _object;

        /// <summary>
        /// Gets or sets the object
        /// </summary>
        public override T Object
        {
            get => _object; set
            {
                _object = value;
                ObjectType = value?.GetType();
            }
        }

        /// <summary>
        /// Gets or sets the object type
        /// </summary>
        public override Type ObjectType { get; protected set; }

        /// <summary>
        /// Gets the extension associated with file type
        /// </summary>
        public override string Extension => "dat";

        /// <summary>
        /// Create new binary file
        /// </summary>
        /// <param name="filename"></param>
        public BinaryFile(string filename) : base(filename)
        {
            AssertTypeSerializable();
            _binaryFormatter = new BinaryFormatter();
        }

        /// <summary>
        /// Create new binary file of type and with optional default data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <param name="data"></param>
        public BinaryFile(string filename, Type type, T data = null) : this(filename)
        {
            Object = data;
            ObjectType = type ?? data?.GetType();
        }

        /// <summary>
        /// Create new binary file with optional default data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        public BinaryFile(string filename, T data) : this(filename, data.GetType(), data)
        {
        }

        /// <summary>
        /// Ensure object is serializable
        /// </summary>
        private void AssertTypeSerializable()
        {
            Utility.AssertSerializable<SerializableAttribute>(typeof(T));
        }

        /// <summary>
        /// Convert object to serialized string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                _binaryFormatter.Serialize(stream, Object);
                stream.Flush();
                stream.Position = 0;
                return System.Convert.ToBase64String(stream.ToArray());
            }
        }

        /// <summary>
        /// Deserialize binary string
        /// </summary>
        /// <param name="binary"></param>
        public override void FromString(string binary)
        {
            byte[] bytes = System.Convert.FromBase64String(binary);
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                stream.Seek(0, SeekOrigin.Begin);
                Object = (T)_binaryFormatter.Deserialize(stream);
            }
        }
    }
}
