﻿using System.IO;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a version store
    /// </summary>
    public class VersionStore
    {
        /// <summary>
        /// Gets the singleton instance of the VersionStore
        /// </summary>
        public static readonly VersionStore Instance = new VersionStore();

        private readonly IDataFile<Dictionary<string, VersionNumber>> _dataFile;

        internal Dictionary<string, VersionNumber> FileVersions = new Dictionary<string, VersionNumber>();

        /// <summary>
        /// Creates a new instance of the VersionStore class
        /// </summary>
        private VersionStore()
        {
            _dataFile = FileSystem.Data.GetDataFile<Dictionary<string, VersionNumber>>("umod.versions.data");
            if (_dataFile.Exists)
            {
                _dataFile.Load();
                FileVersions = _dataFile.Object;
            }
            else
            {
                _dataFile.Object = FileVersions;
            }
        }

        /// <summary>
        /// Gets the last known version of a file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public VersionNumber GetLastKnownVersion(string file)
        {
            return FileVersions.TryGetValue(file, out VersionNumber version) ? version : VersionNumber.Empty;
        }

        /// <summary>
        /// Updates stored version for the specified file
        /// </summary>
        /// <param name="file"></param>
        /// <param name="version"></param>
        public void Update(string file, VersionNumber version)
        {
            _Update(file, version);
        }

        /// <summary>
        /// Updates stored version for the specified file
        /// </summary>
        /// <param name="file"></param>
        /// <param name="version"></param>
        /// <param name="save"></param>
        private void _Update(string file, VersionNumber version, bool save = true)
        {
            if (FileVersions.ContainsKey(file))
            {
                FileVersions[file] = version;
            }
            else
            {
                FileVersions.Add(file, version);
            }

            if (save)
            {
                _dataFile.Save();
            }
        }

        /// <summary>
        /// Updates the version store for the specified versionable files
        /// </summary>
        /// <param name="autoLoadedFiles"></param>
        /// <param name="prefix"></param>
        public void Update(IEnumerable<IVersionableFile> autoLoadedFiles, string prefix = "")
        {
            foreach (IVersionableFile versionableFile in autoLoadedFiles)
            {
                _Update(
                    !string.IsNullOrEmpty(prefix)
                        ? Path.Combine(prefix, versionableFile.Filename)
                        : versionableFile.Filename, versionableFile.Version, false);
            }

            _dataFile.Save();
        }

        /// <summary>
        /// Updates the version store using the specified file/version key-value-pair
        /// </summary>
        /// <param name="files"></param>
        public void Update(Dictionary<string, VersionNumber> files)
        {
            foreach (KeyValuePair<string, VersionNumber> kvp in files)
            {
                _Update(kvp.Key, kvp.Value, false);
            }

            _dataFile.Save();
        }
    }
}
