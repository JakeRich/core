﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Libraries;
using uMod.Plugins;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a versionable object
    /// </summary>
    public interface IVersionable
    {
        VersionNumber Version { get; }
    }

    /// <summary>
    /// Represents a versionable file
    /// </summary>
    public interface IVersionableFile : INameable, IVersionable
    {
        string Filename { get; set; }
        Type Type { get; set; }
        object Attribute { get; }
        T GetAttribute<T>();
    }

    /// <summary>
    /// Version and upgrades file schematics
    /// </summary>
    internal abstract class SchemaMigration
    {
        protected readonly IChainDispatcher Dispatcher;
        protected readonly IPlugin Plugin;
        protected readonly PluginFiles PluginFiles;
        protected readonly Lang Lang;
        protected readonly IApplication Application;
        protected readonly ILogger Logger;

        /// <summary>
        /// Create a new instance of the SchemaMigration class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="files"></param>
        /// <param name="application"></param>
        /// <param name="dispatcher"></param>
        protected SchemaMigration(IPlugin plugin, ILogger logger, PluginFiles files, IApplication application, IChainDispatcher dispatcher)
        {
            Plugin = plugin;
            PluginFiles = files;
            Dispatcher = dispatcher;
            Lang = Interface.uMod.Libraries.Get<Lang>();
            Application = application;
            Logger = logger;
        }

        /// <summary>
        /// Gets upgrade path from current configuration to latest configuration
        /// </summary>
        /// <param name="newVersion"></param>
        /// <param name="currentVersion"></param>
        /// <param name="allVersions"></param>
        /// <returns></returns>
        public Queue<KeyValuePair<IVersionable, IVersionable>> GetUpgradePath(
            IVersionable newVersion, IVersionable currentVersion, IEnumerable<IVersionable> allVersions)
        {
            IEnumerable<IVersionable> orderedVersions = allVersions.OrderByDescending(x => x.Version);
            Queue<KeyValuePair<IVersionable, IVersionable>> upgradePath = new Queue<KeyValuePair<IVersionable, IVersionable>>();

            bool currentVersionFound = false;
            IVersionable lastVersion = null;
            // Iterates through versions ordered by highest to lowest
            foreach (IVersionable version in orderedVersions)
            {
                if (currentVersion.Version == version.Version) // current version found
                {
                    currentVersionFound = true;
                    lastVersion = version;
                    continue;
                }

                if (currentVersionFound) // If we have found the current version, add this version to the queue
                {
                    upgradePath.Enqueue(new KeyValuePair<IVersionable, IVersionable>(lastVersion, version));
                    lastVersion = version;
                }

                if (version.Version == newVersion.Version) // Plugin version found
                {
                    // Stops upgrading
                    break;
                }
            }

            return upgradePath;
        }

        /// <summary>
        /// Gets upgrade path from current configuration to latest configuration
        /// </summary>
        /// <param name="newVersion"></param>
        /// <param name="currentVersion"></param>
        /// <param name="allVersions"></param>
        /// <returns></returns>
        public Queue<KeyValuePair<IVersionable, IVersionable>> GetDowngradePath(
            IVersionable newVersion, IVersionable currentVersion, IEnumerable<IVersionable> allVersions)
        {
            IEnumerable<IVersionable> orderedVersions = allVersions.OrderBy(x => x.Version);
            Queue<KeyValuePair<IVersionable, IVersionable>> upgradePath = new Queue<KeyValuePair<IVersionable, IVersionable>>();

            bool currentVersionFound = false;
            IVersionable lastVersion = null;
            // Iterate through versions ordered by lowest to highest
            foreach (IVersionable version in orderedVersions)
            {
                if (currentVersion.Version == version.Version)
                {
                    currentVersionFound = true;
                    lastVersion = version;
                    continue;
                }

                if (currentVersionFound)
                {
                    upgradePath.Enqueue(new KeyValuePair<IVersionable, IVersionable>(lastVersion, version));
                    lastVersion = version;
                }

                if (version.Version == newVersion.Version)
                {
                    break;
                }
            }

            return upgradePath;
        }

        public Dictionary<string, Queue<KeyValuePair<IVersionable, IVersionable>>> PrepareFileUpgrades(string directory, Dictionary<string, List<IVersionableFile>> autoLoadConfig, Dictionary<string, List<IVersionableFile>> allConfigs)
        {
            Dictionary<string, Queue<KeyValuePair<IVersionable, IVersionable>>> upgradePath = new Dictionary<string, Queue<KeyValuePair<IVersionable, IVersionable>>>();

            // Iterate through all autoloaded data files files
            foreach (KeyValuePair<string, List<IVersionableFile>> kvp in autoLoadConfig)
            {
                IVersionableFile versionableFile = kvp.Value.OrderByDescending(x => x.Version).FirstOrDefault();

                if (versionableFile != null)
                {
                    IDataFile<object> dataFile = PluginFiles.GetDataFile(versionableFile.Type);
                    IEnumerable<IVersionableFile> relateDataDescriptions = allConfigs[kvp.Key].Where(x => x.Name == versionableFile.Name);

                    VersionNumber currentVersion =
                        VersionStore.Instance.GetLastKnownVersion(Path.Combine(directory, versionableFile.Filename));

                    if (currentVersion != VersionNumber.Empty && currentVersion < versionableFile.Version)
                    {
                        IVersionableFile currentVersionableFile =
                            relateDataDescriptions?.FirstOrDefault(x => x.Version == currentVersion);

                        // If last known good version is less than the current version, create upgrade path
                        if (currentVersionableFile != null)
                        {
                            upgradePath.Add(kvp.Key, GetUpgradePath(versionableFile, currentVersionableFile,
                                relateDataDescriptions.Cast<IVersionable>()));
                        }
                    }
                }
            }

            return upgradePath;
        }

        /// <summary>
        /// Upgrades multiple files
        /// </summary>
        /// <param name="upgradePath"></param>
        /// <returns></returns>
        public virtual IPromise<IEnumerable<object>> TryUpgrades(Queue<KeyValuePair<IVersionable, IVersionable>> upgradePath)
        {
            return null;
        }

        /// <summary>
        /// Upgrade a single file
        /// </summary>
        /// <param name="upgradePath"></param>
        /// <returns></returns>
        public virtual IPromise<object> TryUpgrade(Queue<KeyValuePair<IVersionable, IVersionable>> upgradePath)
        {
            return null;
        }

        /// <summary>
        /// Loads a data file for the specified versionable
        /// </summary>
        /// <param name="versionable"></param>
        /// <returns></returns>
        public IDataFile<object> LoadFile(IVersionable versionable)
        {
            IDataFile<object> lastDataFile = GetDataFile(versionable);
            if (!lastDataFile.Exists)
            {
                return lastDataFile;
            }

            lastDataFile.Load();
            return lastDataFile;
        }

        /// <summary>
        /// Gets a data file for the specified versionable
        /// </summary>
        /// <param name="versionable"></param>
        /// <returns></returns>
        public abstract IDataFile<object> GetDataFile(IVersionable versionable);

        private readonly Dictionary<IVersionable, object> _originalVersions = new Dictionary<IVersionable, object>();

        /// <summary>
        /// Roll back all upgraded files to their original versions
        /// </summary>
        public void Rollback()
        {
            if (!(_originalVersions?.Count > 0))
            {
                return;
            }

            foreach (KeyValuePair<IVersionable, object> kvp in _originalVersions)
            {
                IDataFile<object> datafile = GetDataFile(kvp.Key);
                if (datafile == null)
                {
                    continue;
                }

                // if file didn't exist before, delete it
                if (kvp.Value == null && datafile.Exists)
                {
                    datafile.Delete();
                }
                else // revert to previous configuration
                {
                    datafile.Object = kvp.Value;
                    datafile.Save();
                }
            }
        }

        public abstract Type GetImpliedType(Type abstractType);

        /// <summary>
        /// Add original versions to be rolled back in case of failure
        /// </summary>
        /// <param name="originalVersions"></param>
        public void AddToTransaction(IEnumerable<IVersionable> originalVersions)
        {
            _originalVersions.Clear();
            foreach (IVersionable original in originalVersions)
            {
                if (original is IVersionableFile versionableFile)
                {
                    var type = versionableFile.Type;
                    if (type != null && (type.IsInterface || type.IsAbstract))
                    {
                        Type impliedType = GetImpliedType(type);

                        if (impliedType != null)
                        {
                            versionableFile.Type = impliedType;
                        }
                    }

                    try
                    {
                        IDataFile<object> file = LoadFile(versionableFile);
                        if (!_originalVersions.ContainsKey(versionableFile))
                        {
                            _originalVersions.Add(versionableFile, file.Object);
                        }
                    }
                    catch
                    {
                        Logger.Debug($"Unable to load file with schematic {versionableFile.Type.Name} ({versionableFile.Version})"); // TODO: Localization
                    }
                }
            }
        }
    }
}
