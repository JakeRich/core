﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Libraries;
using uMod.Plugins;
using uMod.Utilities;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a locale schematic reader
    /// </summary>
    internal class LocaleSchemaReader : SchemaReader
    {
        private readonly Lang _lang;
        

        /// <summary>
        /// Creates a new instance of the LocaleSchemaReader class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        /// <param name="typeMediator"></param>
        /// <param name="migration"></param>
        /// <param name="pluginFiles"></param>
        public LocaleSchemaReader(IPlugin plugin, ILogger logger, TypeMediator typeMediator, SchemaMigration migration, PluginFiles pluginFiles) : base(plugin, logger, typeMediator, migration, pluginFiles)
        {
            _lang = Interface.uMod.Libraries.Get<Lang>();
        }

        /// <summary>
        /// Load locale localization
        /// </summary>
        /// <param name="promise"></param>
        public void ReadSchema(Promise<IEnumerable<object>> promise)
        {
            if (Mediator.TryGetTypes("Locale*", out IEnumerable<Type> localeTypes))
            {
                GetDataFileContracts<LocalizationAttribute>(localeTypes, out Dictionary<string, List<IVersionableFile>> autoLoadLocales, out Dictionary<string, List<IVersionableFile>> locales);

                Dictionary<string, Queue<KeyValuePair<IVersionable, IVersionable>>> allUpgrades = Migration.PrepareFileUpgrades("lang", autoLoadLocales, locales);

                if (allUpgrades.Count > 0)
                {
                    Migration.AddToTransaction(allUpgrades.Select(x => x.Value.Select(y => y.Key).FirstOrDefault()));
                    foreach (KeyValuePair<string, Queue<KeyValuePair<IVersionable, IVersionable>>> kvp in allUpgrades)
                    {
                        if (kvp.Value != null)
                        {
                            Migration.TryUpgrades(kvp.Value).Done(delegate ()
                            {
                                ReadLocaleAction(autoLoadLocales, promise, kvp.Key);
                            }, delegate(Exception ex)
                            {
                                Migration.Rollback();
                                promise.Reject(ex);
                            });
                        }
                        else
                        {
                            ReadLocaleAction(autoLoadLocales, promise, kvp.Key);
                        }
                    }
                }
                else
                {
                    ReadLocaleAction(autoLoadLocales, promise);
                }
            }
            else
            {
                promise.Reject(new Exception(Interface.uMod.Strings.Plugin.LocaleTypesMissing));
            }
        }

        /// <summary>
        /// Read locale files
        /// </summary>
        /// <param name="autoLoadLocale"></param>
        /// <param name="promise"></param>
        /// <param name="name"></param>
        private void ReadLocaleAction(Dictionary<string, List<IVersionableFile>> autoLoadLocale, Promise<IEnumerable<object>> promise, string name = null)
        {
            Dictionary<string, IVersionableFile> autoload;
            if (!string.IsNullOrEmpty(name))
            {
                autoload = autoLoadLocale.Where(x => x.Key.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    .ToDictionary(x => x.Key, x => x.Value.OrderByDescending(v => v.Version).FirstOrDefault());
            }
            else
            {
                autoload = autoLoadLocale.ToDictionary(x => x.Key,
                    x => x.Value.OrderByDescending(v => v.Version).FirstOrDefault());
            }

            Dictionary<Type, string> autoLoadedLocalizationContracts = autoload.ToDictionary(x => x.Value.Type, x => x.Value.Filename);
            Dictionary<Type, string> autoLoadedFiles = new Dictionary<Type, string>();

            foreach (KeyValuePair<Type, string> kvp in autoLoadedLocalizationContracts)
            {
                IEnumerable<Type> localeTypes = Plugin.GetType()
                    .GetNestedTypesOfInterface(kvp.Key, BindingFlags.Instance | BindingFlags.NonPublic);

                var defaultLocale = localeTypes.FirstOrDefault(x =>
                    x.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute &&
                    (string.IsNullOrEmpty(localeAttribute.Locale) ||
                     localeAttribute.Locale.Equals(Lang.DefaultLang, StringComparison.InvariantCultureIgnoreCase)));

                string[] languages = _lang.GetLanguages();
                List<string> foundLanguages = new List<string>();

                foreach (string language in languages)
                {
                    string path = kvp.Value.Replace($"en{Path.DirectorySeparatorChar}", $"{language}{Path.DirectorySeparatorChar}");

                    if (File.Exists(path))
                    {
                        foundLanguages.Add(language);
                        Type localeType = localeTypes.FirstOrDefault(x =>
                                              x.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute &&
                                              localeAttribute.Locale.Equals(language)) ?? defaultLocale;

                        if (localeType != null)
                        {
                            autoLoadedFiles.Add(localeType, path);
                        }
                    }
                }

                foreach (Type localeType in localeTypes)
                {
                    if (localeType.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute && !foundLanguages.Contains(localeAttribute.Locale))
                    {
                        autoLoadedFiles.Add(localeType, kvp.Value.Replace($"en{Path.DirectorySeparatorChar}", $"{localeAttribute.Locale}{Path.DirectorySeparatorChar}"));
                    }
                }
            }

            if (autoLoadedFiles.Count == 0)
            {
                promise.Resolve(Enumerable.Empty<object>());
            }

            foreach (KeyValuePair<Type, string> kvp in autoLoadedFiles)
            {
                Files.AddFile(kvp.Key, FileSystem.Localization.GetDataFile(kvp.Key, kvp.Value));
            }

            FileSystem.Localization.ReadObjects(autoLoadedFiles, true)
                .Done(delegate (IEnumerable<object> readData)
                {
                    foreach (object data in readData)
                    {
                        if (data != null && data.GetType().IsClass)
                        {
                            Mediator.When().Needs(data.GetType()).Bind(data);
                        }
                    }

                    VersionStore.Instance.Update(autoload.Values, "lang");

                    promise.Resolve(readData);
                }, promise.Reject);
        }
    }
}
