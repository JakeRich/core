﻿using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Plugins;
using uMod.Utilities;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a configuration schematic reader
    /// </summary>
    internal class ConfigSchemaReader : SchemaReader
    {
        /// <summary>
        /// Creates a new instance of the ConfigSchemaReader class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        /// <param name="typeMediator"></param>
        /// <param name="migration"></param>
        /// <param name="pluginFiles"></param>
        public ConfigSchemaReader(IPlugin plugin, ILogger logger, TypeMediator typeMediator, SchemaMigration migration, PluginFiles pluginFiles) : base(plugin, logger, typeMediator, migration, pluginFiles)
        {
        }

        /// <summary>
        /// Load dynamic configuration
        /// </summary>
        /// <param name="promise"></param>
        public void ReadSchema(Promise promise)
        {
            if (Mediator.TryGetTypes("Config*", out IEnumerable<Type> configTypes))
            {
                GetDataFileContracts<ConfigAttribute>(configTypes, out Dictionary<string, List<IVersionableFile>> autoLoadConfig, out Dictionary<string, List<IVersionableFile>> allConfigs);

                Dictionary<string, Queue<KeyValuePair<IVersionable, IVersionable>>> allUpgrades = Migration.PrepareFileUpgrades("config", autoLoadConfig, allConfigs);

                if (allUpgrades.Count > 0)
                {
                    Migration.AddToTransaction(allUpgrades.Select(x => x.Value.Select(y => y.Key).FirstOrDefault()));
                    foreach (KeyValuePair<string, Queue<KeyValuePair<IVersionable, IVersionable>>> kvp in allUpgrades)
                    {
                        if (kvp.Value != null)
                        {
                            Migration.TryUpgrade(kvp.Value).Done(delegate ()
                            {
                                ReadConfigAction(autoLoadConfig, promise, kvp.Key);
                            }, delegate(Exception ex)
                            {
                                Migration.Rollback();
                                promise.Reject(ex);
                            });
                        }
                        else
                        {
                            ReadConfigAction(autoLoadConfig, promise, kvp.Key);
                        }
                    }
                }
                else
                {
                    ReadConfigAction(autoLoadConfig, promise);
                }
            }
            else
            {
                promise.Reject(new Exception(Interface.uMod.Strings.Plugin.ConfigTypesMissing));
            }
        }

        /// <summary>
        /// Read configuration schema
        /// </summary>
        /// <param name="autoLoadConfig"></param>
        /// <param name="promise"></param>
        /// <param name="name"></param>
        private void ReadConfigAction(Dictionary<string, List<IVersionableFile>> autoLoadConfig, Promise promise, string name = null)
        {
            Dictionary<string, IVersionableFile> autoload;
            if (!string.IsNullOrEmpty(name))
            {
                autoload = autoLoadConfig.Where(x => x.Key.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    .ToDictionary(x => x.Key, x => x.Value.OrderByDescending(v => v.Version).FirstOrDefault());
            }
            else
            {
                autoload = autoLoadConfig.ToDictionary(x => x.Key,
                    x => x.Value.OrderByDescending(v => v.Version).FirstOrDefault());
            }

            Dictionary<Type, string> autoLoadFiles = autoload.ToDictionary(x => x.Value.Type, x => x.Value.Filename);

            foreach (KeyValuePair<Type, string> kvp in autoLoadFiles)
            {
                Files.AddFile(kvp.Key, FileSystem.Configuration.GetDataFile(kvp.Key, kvp.Value));
            }

            FileSystem.Configuration.ReadObjects(autoLoadFiles, true)
                .Done(delegate (IEnumerable<object> readData)
                {
                    foreach (object data in readData)
                    {
                        if (data != null && data.GetType().IsClass)
                        {
                            Mediator.When().Needs(data.GetType()).Bind(data);
                        }
                    }

                    VersionStore.Instance.Update(autoload.Values, "config");

                    promise.Resolve();
                }, promise.Reject);
        }
    }
}
