﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Libraries;
using uMod.Plugins;

namespace uMod.IO.Schema
{
    /// <summary>
    /// Represents a locale schematic migration
    /// </summary>
    internal class LocaleSchemaMigration : SchemaMigration
    {
        /// <summary>
        /// Creates a new instance of the LocaleSchemaMigration class
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="files"></param>
        /// <param name="application"></param>
        /// <param name="dispatcher"></param>
        public LocaleSchemaMigration(IPlugin plugin, ILogger logger, PluginFiles files, IApplication application, IChainDispatcher dispatcher) : base(plugin, logger, files, application, dispatcher)
        {
        }

        /// <summary>
        /// Try to perform specified upgrade path
        /// </summary>
        /// <param name="upgradePath"></param>
        /// <returns></returns>
        public override IPromise<IEnumerable<object>> TryUpgrades(Queue<KeyValuePair<IVersionable, IVersionable>> upgradePath)
        {
            Promise<IEnumerable<object>> promise = new Promise<IEnumerable<object>>();

            Dispatcher.Dispatch(delegate
            {
                IEnumerator<KeyValuePair<IVersionable, IVersionable>> enumerator = upgradePath.GetEnumerator();

                Dictionary<string, object> writeFileObjects = null;
                List<string> deleteFileList = null;

                while (enumerator.MoveNext())
                {
                    if (!(enumerator.Current.Key is IVersionableFile leftVersionableFile))
                    {
                        continue;
                    }

                    if (enumerator.Current.Value is IVersionableFile rightVersionableFile)
                    {
                        if (!UpgradeLocale(promise, leftVersionableFile, rightVersionableFile,
                            out writeFileObjects, out deleteFileList))
                        {
                            break;
                        }
                    }
                    else
                    {
                        Logger.Warning($"Unable to find next contract version of localization files {leftVersionableFile.Attribute?.GetType().Name}"); // TODO: Localization
                    }
                }

                if (writeFileObjects?.Count > 0)
                {
                    FileSystem.Localization.WriteObjects(writeFileObjects).Done(delegate ()
                    {
                        FileSystem.Localization.Delete(deleteFileList);
                        promise.Resolve(writeFileObjects.Values);
                    }, promise.Reject);
                }
                else
                {
                    promise.Resolve(Enumerable.Empty<object>());
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Gets a localization file from the specified versionable
        /// </summary>
        /// <param name="versionable"></param>
        /// <returns></returns>
        public override IDataFile<object> GetDataFile(IVersionable versionable)
        {
            if (!(versionable is IVersionableFile versionableFile))
            {
                return default;
            }

            return PluginFiles.Localization.GetDataFile(versionableFile.Type, versionableFile.Filename);
        }

        /// <summary>
        /// Gets implied type for upgrade path schematic
        /// </summary>
        /// <param name="abstractType"></param>
        /// <returns></returns>
        public override Type GetImpliedType(Type abstractType)
        {
            return Plugin.GetType()
                .GetNestedTypes(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public)
                .FirstOrDefault(x => abstractType.IsAssignableFrom(x) &&
                                     x.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute &&
                                     (string.IsNullOrEmpty(localeAttribute.Locale) || localeAttribute.Locale.Equals(Lang.DefaultLang, StringComparison.InvariantCultureIgnoreCase)));
        }

        /// <summary>
        /// Perform locale upgrade
        /// </summary>
        /// <param name="promise"></param>
        /// <param name="leftVersionableFile"></param>
        /// <param name="rightVersionableFile"></param>
        /// <param name="writeFileObjects"></param>
        /// <param name="deleteFileList"></param>
        /// <returns></returns>
        private bool UpgradeLocale(Promise<IEnumerable<object>> promise, IVersionableFile leftVersionableFile, IVersionableFile rightVersionableFile, out Dictionary<string, object> writeFileObjects, out List<string> deleteFileList)
        {
            deleteFileList = null;
            writeFileObjects = null;

            Type leftLocaleType = GetImpliedType(leftVersionableFile.Type);

            LocalizationAttribute localizationAttribute =
                leftVersionableFile.GetAttribute<LocalizationAttribute>();

            if (leftLocaleType == null || localizationAttribute == null)
            {
                Logger.Debug($"Unable to find previous contract version of localization files {leftVersionableFile.Attribute?.GetType().Name}"); // TODO: Localization
                return true;
            }

            writeFileObjects = new Dictionary<string, object>();
            deleteFileList = new List<string>();
            string[] languages = Lang.GetLanguages(Plugin, localizationAttribute.Name);

            Type defaultTargetType = Plugin.GetType()
                .GetNestedTypes(
                    BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public)
                .FirstOrDefault(x => rightVersionableFile.Type.IsAssignableFrom(x) &&
                                     x.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute
                                         localeAttribute &&
                                     (string.IsNullOrEmpty(localeAttribute.Locale) ||
                                      localeAttribute.Locale.Equals(Lang.DefaultLang,
                                          StringComparison.InvariantCultureIgnoreCase)));

            // we found a left and right locale to upgrade
            // upgrade for all languages supported by the locale
            foreach (string language in languages)
            {
                string sourcePath = leftVersionableFile.Filename.Replace($"en{Path.DirectorySeparatorChar}", $"{language}{Path.DirectorySeparatorChar}");
                leftVersionableFile.Type = leftLocaleType;
                leftVersionableFile.Filename = sourcePath;
                IDataFile<object> lastDataFile = null;
                try
                {
                    lastDataFile = LoadFile(leftVersionableFile);
                }
                catch (Exception ex)
                {
                    return false;
                }

                string targetPath = rightVersionableFile.Filename.Replace($"en{Path.DirectorySeparatorChar}", $"{language}{Path.DirectorySeparatorChar}");

                Type rightLocaleType = null;
                if (language != Lang.DefaultLang)
                {
                    rightLocaleType = Plugin.GetType()
                        .GetNestedTypes(
                            BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public)
                        .FirstOrDefault(x => rightVersionableFile.Type.IsAssignableFrom(x) &&
                                             x.GetCustomAttribute<LocaleAttribute>() is
                                                 LocaleAttribute
                                                 localeAttribute &&
                                             localeAttribute.Locale.Equals(language,
                                                 StringComparison.InvariantCultureIgnoreCase));
                }

                if (rightLocaleType == null)
                {
                    rightLocaleType = defaultTargetType;
                }

                IDataFile<object> nextDataFile =
                    PluginFiles.Localization.GetDataFile(rightLocaleType, rightVersionableFile.Filename);

                if (nextDataFile.Object == null)
                {
                    nextDataFile.Object = Application.Make<object>(rightLocaleType);
                }

                try
                {
                    if (lastDataFile != null)
                    {
                        Plugin.CallHook("OnLocaleUpgrade", lastDataFile.Object, nextDataFile.Object);
                    }

                    writeFileObjects.Add(targetPath, nextDataFile.Object);
                }
                catch (Exception ex)
                {
                    promise.Reject(ex);
                    return false;
                }

                if (!rightVersionableFile.Filename.Equals(leftVersionableFile.Filename, StringComparison.InvariantCultureIgnoreCase))
                {
                    deleteFileList.Add(sourcePath);
                }
            }

            return true;
        }
    }
}
