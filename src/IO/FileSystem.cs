﻿using System;
using System.Collections.Generic;

namespace uMod.IO
{
    public static class FileSystem
    {
        /// <summary>
        /// Configuration dynamic file system
        /// </summary>
        internal static readonly DynamicFileSystem Configuration = new DynamicFileSystem(null, DataCategory.Configuration);

        /// <summary>
        /// Localization dynamic file system
        /// </summary>
        internal static readonly DynamicFileSystem Localization = new DynamicFileSystem(null, DataCategory.Localization);

        /// <summary>
        /// Data dynamic file system
        /// </summary>
        internal static readonly DynamicFileSystem Data = new DynamicFileSystem();

        /// <summary>
        /// Read object from specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="key"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public static IPromise<TValue> ReadObject<TValue>(string key, bool create = false) where TValue : class
        {
            return Data.ReadObject<TValue>(key, create);
        }

        /// <summary>
        /// Read object from specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public static IPromise<object> ReadObject(Type type, string path, bool create = false)
        {
            return Data.ReadObject(type, path, create);
        }

        /// <summary>
        /// Write object of generic type to the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public static IPromise WriteObject<TValue>(string path, TValue @object) where TValue : class
        {
            return Data.WriteObject(path, @object);
        }

        /// <summary>
        /// Write object to the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public static IPromise WriteObject(string path, object @object)
        {
            return Data.WriteObject(path, @object);
        }

        /// <summary>
        /// Read objects of generic type from specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static IPromise<IEnumerable<TValue>> ReadObjects<TValue>(IEnumerable<string> paths, bool create = false, string prefix = null) where TValue : class
        {
            return Data.ReadObjects<TValue>(paths, create, prefix);
        }

        /// <summary>
        /// Read objects of specified types and paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="create"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static IPromise<IEnumerable<object>> ReadObjects(IDictionary<Type, string> objects, bool create = false, string prefix = null)
        {
            return Data.ReadObjects(objects, create, prefix);
        }

        /// <summary>
        /// Write objects of the generic type to the specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static IPromise WriteObjects<TValue>(IDictionary<string, TValue> objects, string prefix = null) where TValue : class
        {
            return Data.WriteObjects(objects, prefix);
        }

        /// <summary>
        /// Write objects to the specified paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static IPromise WriteObjects(IDictionary<string, object> objects, string prefix = null)
        {
            return Data.WriteObjects(objects, prefix);
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static IDataFile<object> GetDataFile(Type type, string path)
        {
            return Data.GetDataFile(type, path);
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static IDataFile<TValue> GetDataFile<TValue>(string path) where TValue : class
        {
            return Data.GetDataFile<TValue>(path);
        }
    }
}
