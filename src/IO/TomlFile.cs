﻿using System;
using System.Collections.Generic;
using Tommy;
using uMod.Configuration.Toml;

namespace uMod.IO
{
    /// <summary>
    /// Represents a generic TOML file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TomlFile<T> : TomlFile, IDataFile<T> where T : class
    {
        private T _object;

        /// <summary>
        /// Generic object data
        /// </summary>
        [TomlIgnore]
        public T Object
        {
            get => _object; set
            {
                _object = value;
                ObjectType = value?.GetType();
            }
        }

        /// <summary>
        /// Object type
        /// </summary>
        [TomlIgnore]
        public Type ObjectType { get; private set; }

        /// <summary>
        /// Create a new TOML file
        /// </summary>
        /// <param name="filename"></param>
        public TomlFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Create a new TOML file of the specified type with optional data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <param name="data"></param>
        public TomlFile(string filename, Type type, T data = null) : base(filename)
        {
            Object = data;
            ObjectType = type ?? data?.GetType();
        }

        /// <summary>
        /// Create a new TOML file with the specified data
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        public TomlFile(string filename, T data) : this(filename, data.GetType(), data)
        {
        }

        /// <summary>
        /// Serialize object as TOML string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return TomlConvert.SerializeObject(Object);
        }

        /// <summary>
        /// Deserialize object from TOML string
        /// </summary>
        /// <param name="toml"></param>
        public override void FromString(string toml)
        {
            if (Object == null)
            {
                if (typeof(T) == typeof(object))
                {
                    Object = (T)TomlConvert.DeserializeObject(toml, ObjectType, out Exceptions);
                }
                else
                {
                    Object = TomlConvert.DeserializeObject<T>(toml, out Exceptions);
                }
            }
            else
            {
                TomlConvert.PopulateObject(toml, Object, out Exceptions);
            }
        }

        /// <summary>
        /// Convert TOML file to specified data format
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert(DataFormat format)
        {
            return DataSystem.Convert(this, format);
        }

        /// <summary>
        /// Converts data file to specified data format with data file cast
        /// </summary>
        /// <typeparam name="TDataFile"></typeparam>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert<TDataFile>(DataFormat format) where TDataFile : IDataFile<T>
        {
            return (TDataFile)Convert(format);
        }

        /// <summary>
        /// Load TOML file asynchronously
        /// </summary>
        /// <returns></returns>
        public IPromise<T> LoadAsync()
        {
            Promise<T> promise = new Promise<T>();
            LoadFile().Done(delegate (IDataFile<T> file)
            {
                promise.Resolve(Object);
            }, promise.Reject);
            return promise;
        }

        /// <summary>
        /// Save TOML file asynchronously
        /// </summary>
        /// <returns></returns>
        public IPromise<T> SaveAsync()
        {
            Promise<T> promise = new Promise<T>();
            SaveFile().Done(delegate (IDataFile<T> file)
            {
                promise.Resolve(Object);
            }, promise.Reject);
            return promise;
        }

        public new IPromise<IDataFile<T>> LoadFile()
        {
            return Load<IDataFile<T>>(this);
        }

        public new IPromise<IDataFile<T>> SaveFile()
        {
            return Save<IDataFile<T>>(this);
        }
    }

    /// <summary>
    /// Represents a TOML file
    /// </summary>
    public class TomlFile : DataFile
    {
        /// <summary>
        /// Gets toml file extension
        /// </summary>
        [TomlIgnore]
        public override string Extension => "toml";

        /// <summary>
        /// Gets toml parse exceptions generated during deserialization
        /// </summary>
        [TomlIgnore]
        public IEnumerable<TomlSyntaxException> Exceptions;

        /// <summary>
        /// Create new TOML file
        /// </summary>
        /// <param name="filename"></param>
        public TomlFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Serialize object as TOML string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return TomlConvert.SerializeObject(this);
        }

        /// <summary>
        /// Deserialize object from TOML string
        /// </summary>
        /// <param name="toml"></param>
        public override void FromString(string toml)
        {
            TomlConvert.PopulateObject(toml, this, out Exceptions);
        }
    }
}
