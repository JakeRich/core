﻿extern alias References;

using System;
using System.Collections;
using System.Collections.Generic;
using References::Newtonsoft.Json;

namespace uMod.IO
{
    /// <summary>
    /// Represents a json synchronized file
    /// </summary>
    public class JsonSyncFile : JsonFile, IEnumerable<KeyValuePair<string, object>>
    {
        /// <summary>
        /// Data dictionary
        /// </summary>
        private Dictionary<string, object> _data = new Dictionary<string, object>();

        /// <summary>
        /// Create a new json synchronized file with optional serializer settings
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="settings"></param>
        public JsonSyncFile(string filename, JsonSerializerSettings settings = null) : base(filename, settings)
        {
        }

        /// <summary>
        /// Save json file
        /// </summary>
        /// <returns></returns>
        public new IPromise<JsonSyncFile> SaveFile()
        {
            return Save<JsonSyncFile>(this);
        }

        /// <summary>
        /// Load json file
        /// </summary>
        /// <returns></returns>
        public new IPromise<JsonSyncFile> LoadFile()
        {
            return Load<JsonSyncFile>(this);
        }

        /// <summary>
        /// Serialize data to json string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(_data, Formatting.Indented, Settings);
        }

        /// <summary>
        /// Deserialize data from json string
        /// </summary>
        /// <param name="json"></param>
        public override void FromString(string json)
        {
            _data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json, Settings);
        }

        /// <summary>
        /// Clears this config
        /// </summary>
        public void Clear()
        {
            _data.Clear();
        }

        /// <summary>
        /// Removes key from config
        /// </summary>
        public bool Remove(string key)
        {
            return _data.Remove(key);
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[string key]
        {
            get => _data.TryGetValue(key, out object val) ? val : null;
            set => _data[key] = value;
        }

        /// <summary>
        /// Gets or sets a nested setting on this config by key
        /// </summary>
        /// <param name="keyLevel1"></param>
        /// <param name="keyLevel2"></param>
        /// <returns></returns>
        public object this[string keyLevel1, string keyLevel2]
        {
            get => Get(keyLevel1, keyLevel2);
            set => Set(keyLevel1, keyLevel2, value);
        }

        /// <summary>
        /// Gets or sets a nested setting on this config by key
        /// </summary>
        /// <param name="keyLevel1"></param>
        /// <param name="keyLevel2"></param>
        /// <param name="keyLevel3"></param>
        /// <returns></returns>
        public object this[string keyLevel1, string keyLevel2, string keyLevel3]
        {
            get => Get(keyLevel1, keyLevel2, keyLevel3);
            set => Set(keyLevel1, keyLevel2, keyLevel3, value);
        }

        /// <summary>
        /// Gets a configuration value at the specified path and converts it to the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public T Get<T>(params string[] path)
        {
            if (Get(path) is object val)
            {
                return val.ToType<T>();
            }

            return default;
        }

        /// <summary>
        /// Gets a configuration value at the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public object Get(params string[] path)
        {
            if (path.Length < 1)
            {
                throw new ArgumentException("Path must not be empty");
            }

            if (!_data.TryGetValue(path[0], out object val))
            {
                return null;
            }

            for (int i = 1; i < path.Length; i++)
            {
                if (!(val is Dictionary<string, object> dict) || !dict.TryGetValue(path[i], out val))
                {
                    return null;
                }
            }

            return val;
        }

        /// <summary>
        /// Sets a configuration value at the specified path
        /// </summary>
        /// <param name="pathAndTrailingValue"></param>
        public void Set(params object[] pathAndTrailingValue)
        {
            if (pathAndTrailingValue.Length < 2)
            {
                throw new ArgumentException("Path must not be empty");
            }

            string[] path = new string[pathAndTrailingValue.Length - 1];
            for (int i = 0; i < pathAndTrailingValue.Length - 1; i++)
            {
                path[i] = (string)pathAndTrailingValue[i];
            }

            object value = pathAndTrailingValue[pathAndTrailingValue.Length - 1];
            if (path.Length == 1)
            {
                _data[path[0]] = value;
                return;
            }

            if (!_data.TryGetValue(path[0], out object val))
            {
                _data[path[0]] = val = new Dictionary<string, object>();
            }

            string fullPath = path[0];
            for (int i = 1; i < path.Length - 1; i++)
            {
                fullPath += $"/{path[i]}";

                if (val is Dictionary<string, object> oldVal)
                {
                    if (!oldVal.TryGetValue(path[i], out val))
                    {
                        oldVal[path[i]] = val = new Dictionary<string, object>();
                    }
                }
                else
                {
                    throw new ArgumentException($"Path \"{fullPath}\" is invalid");
                }
            }

            ((Dictionary<string, object>)val)[path[path.Length - 1]] = value;
        }

        #region IEnumerable

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => _data.GetEnumerator();

        #endregion IEnumerable
    }
}
