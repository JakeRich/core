﻿namespace uMod.IO
{
    /// <summary>
    /// Represents a text file
    /// </summary>
    public class TextFile : DataFile
    {
        /// <summary>
        /// Gets the text contents
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// Gets text file extension
        /// </summary>
        public override string Extension => "txt";

        /// <summary>
        /// Create new text file
        /// </summary>
        /// <param name="filename"></param>
        public TextFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Gets the text content
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Contents;
        }

        /// <summary>
        /// Sets the text content
        /// </summary>
        /// <param name="text"></param>
        public override void FromString(string text)
        {
            Contents = text;
        }
    }
}
