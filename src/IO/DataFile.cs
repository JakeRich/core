﻿extern alias References;

using System;
using System.IO;
using References::Newtonsoft.Json;
using References::ProtoBuf;
using uMod.Common;
using uMod.Configuration.Toml;

namespace uMod.IO
{
    /// <summary>
    /// Represents a data file that serializes/deserializes to a generic type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DataFile<T> : DataFile, IDataFile<T> where T : class
    {
        /// <summary>
        /// Gets or sets the object type
        /// </summary>
        public abstract Type ObjectType { get; protected set; }

        /// <summary>
        /// Gets or sets the object
        /// </summary>
        public abstract T Object { get; set; }

        /// <summary>
        /// Create a new DataFile
        /// </summary>
        /// <param name="filename"></param>
        protected DataFile(string filename) : base(filename)
        {
        }

        /// <summary>
        /// Convert data file to specified data format
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert(DataFormat format)
        {
            return DataSystem.Convert(this, format);
        }

        /// <summary>
        /// Converts data file to specified data format with data file cast
        /// </summary>
        /// <typeparam name="TDataFile"></typeparam>
        /// <param name="format"></param>
        /// <returns></returns>
        public IDataFile<T> Convert<TDataFile>(DataFormat format) where TDataFile : IDataFile<T>
        {
            return (TDataFile)Convert(format);
        }

        /// <summary>
        /// Load data file asynchronously
        /// </summary>
        /// <returns></returns>
        public new IPromise<IDataFile<T>> LoadFile()
        {
            return Load<IDataFile<T>>(this);
        }

        /// <summary>
        /// Save data file asynchronously
        /// </summary>
        /// <returns></returns>
        public new IPromise<IDataFile<T>> SaveFile()
        {
            return Save<IDataFile<T>>(this);
        }

        /// <summary>
        /// Load data file asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual IPromise<T> LoadAsync()
        {
            Promise<T> promise = new Promise<T>();

            Load<IDataFile<T>>(this).Done(delegate (IDataFile<T> file)
            {
                promise.Resolve(Object);
            }, promise.Reject);

            return promise;
        }

        /// <summary>
        /// Save data file asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual IPromise<T> SaveAsync()
        {
            Promise<T> promise = new Promise<T>();

            Save<IDataFile<T>>(this).Done(delegate (IDataFile<T> file)
            {
                promise.Resolve(Object);
            }, promise.Reject);

            return promise;
        }
    }

    /// <summary>
    /// Represents a data file
    /// </summary>
    public abstract class DataFile : IDataFile
    {
        /// <summary>
        /// Gets the path
        /// </summary>
        protected readonly string path;

        /// <summary>
        /// Gets the path
        /// </summary>
        [JsonIgnore, TomlIgnore, ProtoIgnore]
        public virtual string Path => path;

        /// <summary>
        /// Gets the filename
        /// </summary>
        [JsonIgnore, TomlIgnore, ProtoIgnore]
        public virtual string Filename => System.IO.Path.GetFileNameWithoutExtension(path);

        /// <summary>
        /// Read event
        /// </summary>
        [JsonIgnore, TomlIgnore, ProtoIgnore]
        public virtual IEvent<IDataObject> OnRead { get; } = new Event<IDataObject>();

        /// <summary>
        /// Write event
        /// </summary>
        [JsonIgnore, TomlIgnore, ProtoIgnore]
        public virtual IEvent<IDataObject> OnWrite { get; } = new Event<IDataObject>();

        /// <summary>
        /// Gets whether or not file is a watched configuration file
        /// </summary>
        [JsonIgnore, TomlIgnore, ProtoIgnore]
        public virtual bool IsConfigFile { get; protected set; }

        /// <summary>
        /// Gets the file extension
        /// </summary>
        public abstract string Extension { get; }

        [JsonIgnore, TomlIgnore, ProtoIgnore]
        public virtual bool IsLoaded { get; protected set; }

        /// <summary>
        /// Create a new DataFile
        /// </summary>
        /// <param name="filename"></param>
        protected DataFile(string filename)
        {
            if (string.IsNullOrEmpty((filename)))
            {
                throw new ArgumentNullException(nameof(filename));
            }

            string extension = System.IO.Path.GetExtension(filename).ToLower();
            if (Extension != null && (!extension.EndsWith(Extension.ToLower())))
            {
                filename = $"{filename}.{Extension}";
            }
            path = PathUtils.AssertValid(filename);

            IsConfigFile = PathUtils.IsConfig(path);
        }

        /// <summary>
        /// Determines if file exists
        /// </summary>
        /// <returns></returns>
        [JsonIgnore, TomlIgnore, ProtoIgnore]
        public bool Exists => File.Exists(path);

        /// <summary>
        /// Delete file
        /// </summary>
        public void Delete()
        {
            if (Exists)
            {
                File.Delete(path);
            }
        }

        /// <summary>
        /// Deserialize data content from specified string
        /// </summary>
        /// <param name="format"></param>
        public abstract void FromString(string format);

        /// <summary>
        /// Loads data from the specified file synchronously (not recommended)
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="createDefault"></param>
        public static T Load<T>(string filename, bool createDefault = true) where T : IDataFile
        {
            T config = Interface.uMod.Application.MakeParams<T>(typeof(T), filename);
            if (File.Exists(filename))
            {
                config.Load();
            }
            else
            {
                if (createDefault)
                {
                    config.Save();
                }
                else
                {
                    string fileName = System.IO.Path.GetFileName(filename);
                    throw new FileNotFoundException($"Configuration file does not exist \"{fileName}\"", filename); // TODO: Localization
                }
            }

            return config;
        }

        /// <summary>
        /// Loads data from the specified file asynchronously
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="createDefault"></param>
        public static IPromise<T> LoadAsync<T>(string filename, bool createDefault = true) where T : IDataFile
        {
            T config = Interface.uMod.Application.MakeParams<T>(typeof(T), filename);
            if (File.Exists(filename))
            {
                return Load<T>(config);
            }

            if (createDefault)
            {
                return Save<T>(config);
            }

            throw new FileNotFoundException("Configuration file does not exist", filename); // TODO: Localization
        }

        /// <summary>
        /// Load file synchronously (not recommended)
        /// </summary>
        public virtual void Load()
        {
            string path = PathUtils.AssertValid(this.path);
            FromString(File.ReadAllText(path));
            OnRead?.Invoke(this);
            IsLoaded = true;
        }

        /// <summary>
        /// Load data file asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IPromise<T> Load<T>(IDataFile file) where T : IDataObject
        {
            Promise<T> promise = new Promise<T>();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                // Optimization to prevent try/catch
                if (!file.Exists)
                {
                    promise.Reject(new FileNotFoundException($"Could not find file '{file.Path}'.")); // TODO: Localization
                    return true;
                }

                try
                {
                    file.Load();
                    promise.Resolve((T)file);
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Save file synchronously (not recommended)
        /// </summary>
        public virtual void Save()
        {
            string path = PathUtils.AssertValid(this.path);
            string dir = Utility.GetDirectoryName(path);
            if (dir != null && !Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            if (IsConfigFile && Interface.uMod?.Plugins?.Configuration != null && (Interface.uMod.Plugins?.Configuration?.Watchers?.ConfigWatchers ?? false))
            {
                Interface.uMod.ConfigChanges.Add(path);
            }

            FileInfo file = new FileInfo(path);
            string content = ToString();
            using (FileStream stream = file.Open(FileMode.OpenOrCreate, FileAccess.Write, FileShare.Delete | FileShare.ReadWrite))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                stream.SetLength(0);
                writer.Write(content);
            }

            //File.WriteAllText(path, ToString());
            OnWrite?.Invoke(this);
            IsLoaded = true;
        }

        /// <summary>
        /// Save data file asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IPromise<T> Save<T>(IDataFile file) where T : IDataObject
        {
            Promise<T> promise = new Promise<T>();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    file.Save();
                    promise.Resolve((T)file);
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Load data file asynchronously
        /// </summary>
        /// <returns></returns>
        public virtual IPromise<IDataFile> LoadFile()
        {
            return Load<IDataFile>(this);
        }

        /// <summary>
        /// Save data file asynchronously
        /// </summary>
        /// <returns></returns>
        public virtual IPromise<IDataFile> SaveFile()
        {
            return Save<IDataFile>(this);
        }
    }
}
