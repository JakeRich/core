using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using uMod.Collections;
using uMod.Command;
using uMod.Common;
using uMod.Plugins;
using uMod.Utilities;
using uMod.Web;

namespace uMod.Pooling
{
    /// <summary>
    /// Pools helper class
    /// </summary>
    public static class Pools
    {
        private static readonly Dictionary<Type, object> DefaultPools = new Dictionary<Type, object>();
        private static readonly Dictionary<Type, object> DisposablePools = new Dictionary<Type, object>();
        private static readonly Dictionary<Type, object> ListPools = new Dictionary<Type, object>();
        private static readonly Dictionary<Type, object> QueuePools = new Dictionary<Type, object>();
        private static readonly Dictionary<Type, object> DictionaryPools = new Dictionary<Type, object>();
        private static readonly Dictionary<Type, object> DictionaryComparerPools = new Dictionary<Type, object>();
        internal static readonly StringBuilderPool StringBuilders = new StringBuilderPool();
        internal static readonly HookEventPool HookEvents = new HookEventPool();
        internal static readonly CommandArgsPool CommandArgs = new CommandArgsPool();
        internal static readonly HookContextPool HookContext = new HookContextPool();
        internal static readonly ContextReferencePool ContextReferences = new ContextReferencePool();
        internal static readonly HookMethodPool HookMethods = new HookMethodPool();
        internal static readonly RequestPool Requests = new RequestPool();

        /// <summary>
        /// Gets DynamicPool for generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static DynamicPool Default<T>()
        {
            if (!DefaultPools.TryGetValue(typeof(T), out object pool))
            {
                DefaultPools.Add(typeof(T), pool = new DynamicPool(typeof(T)));
            }

            return pool as DynamicPool;
        }

        /// <summary>
        /// Gets StringBuilderPool
        /// </summary>
        /// <returns></returns>
        public static StringBuilderPool StringBuilder()
        {
            if (!DefaultPools.TryGetValue(typeof(StringBuilder), out object pool))
            {
                DefaultPools.Add(typeof(StringBuilder), pool = new StringBuilderPool());
            }

            return pool as StringBuilderPool;
        }

        /// <summary>
        /// Gets DynamicPool for disposable type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static DynamicPool Disposable<T>() where T : IDisposable
        {
            if (!DisposablePools.TryGetValue(typeof(T), out object pool))
            {
                DisposablePools.Add(typeof(T), pool = new DisposablePool<T>());
            }

            return pool as DynamicPool;
        }

        /// <summary>
        /// Gets DynamicPool for generic list type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal static DynamicPool List<T>() where T : IList
        {
            if (!ListPools.TryGetValue(typeof(T), out object pool))
            {
                ListPools.Add(typeof(T), pool = new ListPool<T>());
            }

            return pool as DynamicPool;
        }

        /// <summary>
        /// Gets a List of the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetList<T>()
        {
            return List<List<T>>().Get<List<T>>();
        }

        /// <summary>
        /// Frees a List back to the pool
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static void FreeList<T>(ref List<T> list)
        {
            list.Clear();

            DynamicPool pool = List<List<T>>();

            pool.Free(list);

            //We set the field to null which helps prevent re-use of a pooled object accidently
            list = null;
        }

        /// <summary>
        /// Gets DynamicPool for generic queue type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal static DynamicPool Queue<T>() where T : class
        {
            if (!QueuePools.TryGetValue(typeof(T), out object pool))
            {
                QueuePools.Add(typeof(T), pool = new QueuePool<Queue<T>>());
            }

            return pool as DynamicPool;
        }

        /// <summary>
        /// Gets a Queue of the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Queue<T> GetQueue<T>() where T : class
        {
            return Queue<T>().Get<Queue<T>>();
        }

        /// <summary>
        /// Frees a Queue back to the pool
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static void FreeQueue<T>(ref Queue<T> queue) where T : class
        {
            queue.Clear();

            DynamicPool pool = Queue<T>();

            pool.Free(queue);

            //We set the field to null which helps prevent re-use of a pooled object accidently
            queue = null;
        }

        /// <summary>
        /// Gets DynamicPool for generic dictionary type
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="comparer"></param>
        /// <returns></returns>
        internal static DictionaryPool<TKey, TValue> Dictionary<TKey, TValue>(IEqualityComparer<TKey> comparer = null)
        {
            object pool;
            if (comparer != null && Utility.ReflectedName(comparer.GetType().Name) != "GenericEqualityComparer")
            {
                if (!DictionaryComparerPools.TryGetValue(typeof(Dictionary<TKey, TValue>), out pool))
                {
                    DictionaryComparerPools.Add(typeof(Dictionary<TKey, TValue>), pool = new DictionaryPool<TKey, TValue, IEqualityComparer<TKey>>(comparer));
                }

                return pool as DictionaryPool<TKey, TValue, IEqualityComparer<TKey>>;
            }

            if (!DictionaryPools.TryGetValue(typeof(Dictionary<TKey, TValue>), out pool))
            {
                DictionaryPools.Add(typeof(Dictionary<TKey, TValue>), pool = new DictionaryPool<TKey, TValue>());
            }

            return pool as DictionaryPool<TKey, TValue>;
        }

        /// <summary>
        /// Gets a Dictionary of the generic type
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> GetDictionary<TKey, TValue>(IEqualityComparer<TKey> comparer = null)
        {
            return Dictionary<TKey, TValue>(comparer).Get<Dictionary<TKey, TValue>>();
        }

        /// <summary>
        /// Frees a Dictionary back to the pool
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        public static void FreeDictionary<TKey, TValue>(ref Dictionary<TKey, TValue> dictionary)
        {
            dictionary.Clear();

            DynamicPool pool = Dictionary<TKey, TValue>(dictionary.Comparer);

            pool.Free(dictionary);

            //We set the field to null which helps prevent re-use of a pooled object accidently
            dictionary = null;
        }
    }

    /// <summary>
    /// PooledObject class
    /// Stores a reference to a disposable object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PooledObject<T> : IDisposable
    {
        public T Object { get; internal set; }
        public DynamicPool Pool { get; internal set; }
        internal bool DisposedValue;

        /// <summary>
        /// Create a blank pooled object
        /// </summary>
        public PooledObject() { }

        /// <summary>
        /// Create a pooled object for the specified object and within the specified pool
        /// </summary>
        /// <param name="object"></param>
        /// <param name="pool"></param>
        public PooledObject(T @object, DynamicPool pool) : this()
        {
            Object = @object;
            Pool = pool;
            DisposedValue = false;
        }

        /// <summary>
        /// Reset pooled object to be re-used
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        internal PooledObject<T> Reset(T @object)
        {
            Object = @object;
            DisposedValue = false;
            return this;
        }

        #region IDisposable Support

        /// <summary>
        /// Dispose of pooled object
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!DisposedValue)
            {
                if (disposing)
                {
                    Object = default;
                    Pool.Free(Object);
                }

                DisposedValue = true;
            }
        }

        /// <summary>
        /// Dispose of pooled object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        #endregion IDisposable Support
    }

    /// <summary>
    /// DynamicPool class
    /// Store pooled objects of generic type
    /// </summary>
    public class DynamicPool : PoolQueue
    {
        protected Type PoolType;
        protected readonly ActivatorFactory Factory;

        /// <summary>
        /// Create a dynamic pool object with an optional custom factory
        /// </summary>
        /// <param name="type"></param>
        /// <param name="capacity"></param>
        internal DynamicPool(Type type, ushort capacity = 255) : base(capacity)
        {
            PoolType = type;
            Factory = new ActivatorFactory(type);
        }

        /// <summary>
        /// Get (or create) a new pooled object
        /// </summary>
        /// <returns></returns>
        public virtual T Get<T>()
        {
            if (Current <= 0)
            {
                return Factory.Activate<T>();
            }

            Current -= 1;

            return (T)Nodes[Current];
        }

        /// <summary>
        /// Free a pooled object back to the pool
        /// </summary>
        /// <param name="object"></param>
        public virtual void Free<T>(T @object)
        {
            if (@object == null)
            {
                return;
            }

            Enqueue(@object);
        }
    }

    /// <summary>
    /// HookMethodPool class
    /// </summary>
    internal class HookMethodPool : ListPool<List<HookMethod>> { }

    /// <summary>
    /// HookEventPool class
    /// </summary>
    public class HookEventPool : DisposablePool<HookEvent> { }

    /// <summary>
    /// RequestPool class
    /// </summary>
    public class RequestPool : DisposablePool<Request> { }

    /// <summary>
    /// CommandArgsPool class
    /// </summary>
    public class CommandArgsPool : DisposablePool<Args> { }

    /// <summary>
    /// HookContextPool class
    /// </summary>
    public class HookContextPool : DisposablePool<HookContext> { }

    /// <summary>
    /// ContextReferencePool class
    /// </summary>
    public class ContextReferencePool : DisposablePool<ContextReference<IContext>>
    {
        /// <summary>
        /// Get (or create) a new pooled context reference
        /// </summary>
        /// <returns></returns>
        public ContextReference<IContext> Get(IContext context = null)
        {
            ContextReference<IContext> @object = base.Get();

            if (context != null)
            {
                @object.Reset(context);
            }

            return @object;
        }
    }

    /// <summary>
    /// DisposablePool class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DisposablePool<T> : DynamicPool where T : IDisposable
    {
        internal DisposablePool(ushort capacity = 255) : base(typeof(T), capacity)
        {
        }

        public T Get()
        {
            return base.Get<T>();
        }

        /// <summary>
        /// Free a pooled disposable object back to the pool
        /// </summary>
        /// <param name="object"></param>
        public void Free(T @object)
        {
            if (@object == null)
            {
                return;
            }

            @object.Dispose();
            Enqueue(@object);
        }
    }

    /// <summary>
    /// QueuePool class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QueuePool<T> : DynamicPool where T : class
    {
        internal QueuePool(ushort capacity = 255) : base(typeof(Queue<T>), capacity)
        {
        }

        public T Get()
        {
            return base.Get<T>();
        }

        /// <summary>
        /// Free a pooled queue back to the pool
        /// </summary>
        /// <param name="object"></param>
        public void Free(Queue<T> @object)
        {
            if (@object == null)
            {
                return;
            }

            @object.Clear();
            Enqueue(@object);
        }
    }

    /// <summary>
    /// ListPool class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListPool<T> : DynamicPool where T : IList
    {
        public ListPool(ushort capacity = 255) : base(typeof(T), capacity)
        {
        }

        public T Get()
        {
            return base.Get<T>();
        }

        /// <summary>
        /// Free a pooled list back to the pool
        /// </summary>
        /// <param name="object"></param>
        public void Free(T @object)
        {
            if (@object == null)
            {
                return;
            }

            @object.Clear();
            Enqueue(@object);
        }
    }

    /// <summary>
    /// DictionaryPool class
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class DictionaryPool<TKey, TValue> : DynamicPool
    {
        internal DictionaryPool(ushort capacity = 255) : base(typeof(Dictionary<TKey, TValue>), capacity)
        {
        }

        public virtual Dictionary<TKey, TValue> Get()
        {
            return base.Get<Dictionary<TKey, TValue>>();
        }

        /// <summary>
        /// Free a pooled dictionary back to the pool
        /// </summary>
        /// <param name="object"></param>
        public void Free(Dictionary<TKey, TValue> @object)
        {
            if (@object == null)
            {
                return;
            }

            @object.Clear();
            Enqueue(@object);
        }
    }

    /// <summary>
    /// DictionaryPool class (with key case comparison)
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <typeparam name="TComparer"></typeparam>
    public class DictionaryPool<TKey, TValue, TComparer> : DictionaryPool<TKey, TValue> where TComparer : IEqualityComparer<TKey>
    {
        public readonly TComparer Comparer;

        internal DictionaryPool(TComparer comparer, ushort capacity = 255) : base(capacity)
        {
            Comparer = comparer;
        }

        /// <summary>
        /// Get (or create) a new pooled object
        /// </summary>
        /// <returns></returns>
        public override Dictionary<TKey, TValue> Get()
        {
            if (Current == 0)
            {
                return new Dictionary<TKey, TValue>(Comparer);
            }

            Current -= 1;
            return (Dictionary<TKey, TValue>)Nodes[Current];
        }

        /// <summary>
        /// Free a pooled dictionary back to the pool
        /// </summary>
        /// <param name="object"></param>
        public new void Free(Dictionary<TKey, TValue> @object)
        {
            if (@object == null)
            {
                return;
            }

            @object.Clear();
            Enqueue(@object);
        }
    }

    /// <summary>
    /// StringBuilderPool class
    /// </summary>
    public class StringBuilderPool : DynamicPool
    {
        public readonly object PoolLock = new object();

        internal StringBuilderPool(ushort capacity = 255) : base(typeof(StringBuilder), capacity)
        {
        }

        public StringBuilder Get()
        {
            lock (PoolLock)
            {
                return base.Get<StringBuilder>();
            }
        }

        /// <summary>
        /// Free a pooled stringbuilder back to the pool
        /// </summary>
        /// <param name="stringBuilder"></param>
        public void Free(StringBuilder stringBuilder)
        {
            if (stringBuilder == null)
            {
                return;
            }

            lock (PoolLock)
            {
                stringBuilder.Length = 0;
                Enqueue(stringBuilder);
            }
        }
    }
}
