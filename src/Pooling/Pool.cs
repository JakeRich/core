﻿using System;
using System.Collections.Generic;
using uMod.Common.Pooling;

namespace uMod.Pooling
{
    /// <summary>
    /// StringPool class
    /// </summary>
    public static class StringPool
    {
        private static readonly ObjectPool<string> ObjectPool = new ObjectPool<string>();
        public static string[] Empty = new string[0];

        /// <summary>
        /// Get string array of specified length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string[] Get(int length)
        {
            return ObjectPool.Get(length);
        }

        /// <summary>
        /// Free string array back to pool
        /// </summary>
        /// <param name="array"></param>
        public static void Free(string[] array)
        {
            ObjectPool.Free(array);
        }
    }

    /// <summary>
    /// ArrayPool class
    /// </summary>
    public static class ArrayPool
    {
        private static readonly ObjectPool ObjectPool = new ObjectPool();
        public static object[] Empty = new object[0];

        /// <summary>
        /// Get object array of specified length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static object[] Get(int length)
        {
            return ObjectPool.Get(length);
        }

        /// <summary>
        /// Free object array back to pool
        /// </summary>
        /// <param name="array"></param>
        public static void Free(object[] array)
        {
            ObjectPool.Free(array);
        }
    }

    /// <summary>
    /// TypePool class
    /// </summary>
    public static class TypePool
    {
        private static readonly ArrayPool<Type> Pool = new ArrayPool<Type>();

        /// <summary>
        /// Get type array of specified length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static Type[] Get(int length)
        {
            return Pool.Get(length);
        }

        /// <summary>
        /// Free type array of specified length
        /// </summary>
        /// <param name="array"></param>
        public static void Free(Type[] array)
        {
            Pool.Free(array);
        }
    }

    /// <summary>
    /// PoolSettings class
    /// </summary>
    public class PoolSettings
    {
        public int MaxArrayLength = 50;
        public int MinPoolAmount = 32;
        public int MaxPoolAmount = 128;
    }

    /// <summary>
    /// ObjectPool class of generic type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ObjectPool<T> : IArrayPool<T>
    {
        private readonly ArrayPool<T> _typedPool;

        /// <summary>
        /// Create a new ObjectPool object
        /// </summary>
        /// <param name="settings"></param>
        public ObjectPool(PoolSettings settings = null)
        {
            _typedPool = new ArrayPool<T>(settings);
        }

        /// <summary>
        /// Get strongly-typed array of specified length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public T[] Get(int length)
        {
            return _typedPool.Get(length);
        }

        /// <summary>
        /// Free strongly-typed array back to pool
        /// </summary>
        /// <param name="array"></param>
        public void Free(T[] array)
        {
            _typedPool.Free(array);
        }
    }

    /// <summary>
    /// ObjectPool class
    /// </summary>
    public class ObjectPool : ArrayPool<object>
    {
        public ObjectPool(PoolSettings settings = null) : base(settings)
        {
        }
    }

    /// <summary>
    /// ArrayPool class of generic type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ArrayPool<T> : IArrayPool<T>
    {
        public readonly T[] Empty = new T[0];
        private readonly List<Queue<T[]>> _pooledArrays = new List<Queue<T[]>>();
        private readonly PoolSettings _settings;

        /// <summary>
        /// Create a new ArrayPool object
        /// </summary>
        /// <param name="settings"></param>
        public ArrayPool(PoolSettings settings = null)
        {
            if (settings == null)
            {
                settings = new PoolSettings();
            }

            _settings = settings;

            for (int i = 0; i < _settings.MaxArrayLength; i++)
            {
                _pooledArrays.Add(new Queue<T[]>());
                SetupArrays(i + 1);
            }
        }

        /// <summary>
        /// Get strongly-typed array of the specified length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public T[] Get(int length)
        {
            if (length == 0)
            {
                return Empty;
            }

            if (length > _settings.MaxArrayLength)
            {
                return new T[length];
            }

            Queue<T[]> arrays = _pooledArrays[length - 1];
            lock (arrays)
            {
                if (arrays.Count == 0)
                {
                    SetupArrays(length);
                }
                return arrays.Dequeue();
            }
        }

        /// <summary>
        /// Free strongly-typed array back to pool
        /// </summary>
        /// <param name="array"></param>
        public void Free(T[] array)
        {
            if (array != null && array.Length != 0 && array.Length <= _settings.MaxArrayLength)
            {
                // Cleanup array
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = default;
                }

                Queue<T[]> arrays = _pooledArrays[array.Length - 1];
                lock (arrays)
                {
                    if (arrays.Count > _settings.MaxPoolAmount)
                    {
                        for (int i = 0; i < _settings.MinPoolAmount; i++)
                        {
                            arrays.Dequeue();
                        }

                        return;
                    }

                    arrays.Enqueue(array);
                }
            }
        }

        /// <summary>
        /// Scales pool size depending on size of array
        /// Arrays with fewer elements are allocated more space
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private int GetPoolAmount(int length)
        {
            if (length > _settings.MaxArrayLength)
            {
                length = _settings.MaxArrayLength;
            }

            if (length < 1)
            {
                return 0;
            }

            float step = _settings.MaxPoolAmount / _settings.MaxArrayLength;

            int stepCount = length - 1; // First element has maximum pool amount

            return _settings.MaxPoolAmount - Convert.ToInt32(stepCount * step);
        }

        /// <summary>
        /// Allocates matrix of arrays for a given length
        /// Number of pooled arrays depends on array length
        /// </summary>
        /// <param name="length"></param>
        private void SetupArrays(int length)
        {
            Queue<T[]> arrays = _pooledArrays[length - 1];
            int poolAmount = GetPoolAmount(length);
            for (int i = 0; i < poolAmount; i++)
            {
                arrays.Enqueue(new T[length]);
            }
        }
    }
}
