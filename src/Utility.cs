extern alias References;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using References::Newtonsoft.Json;
using uMod.Text;
using System.Threading;
#if DEBUG
using System.Text;
#endif

namespace uMod
{
    public class Utility
    {
        public static Plugins.PluginProvider Plugins;
        public static Utilities.Random Random;
        public static Utilities.Time Time;
        public static StringInterpolater Interpolator;
        public static Formatter Formatter;
        
        public static void AssertSerializable<T>(Type type) where T : Attribute
        {
            if (type.IsPrimitive)
            {
                return;
            }

            if (type.IsValueType)
            {
                return;
            }

            if (type == typeof(string))
            {
                return;
            }

            if (typeof(IList).IsAssignableFrom(type))
            {
                AssertSerializable<T>(type.GetGenericArguments()[0]);
                return;
            }

            if (typeof(IDictionary).IsAssignableFrom(type))
            {
                AssertSerializable<T>(type.GetGenericArguments()[0]);
                AssertSerializable<T>(type.GetGenericArguments()[1]);
                return;
            }

            if (type.GetCustomAttributes(typeof(T), true).Length == 0)
            {
                throw new ArgumentException($"{type.FullName} must be marked with a {typeof(T).FullName}");
            }
        }

        /// <summary>
        /// Print the call stack to the log file
        /// </summary>
        public static void PrintCallStack() => Interface.uMod.LogDebug("CallStack: {0}{1}", Environment.NewLine, new StackTrace(1, true));

        /// <summary>
        /// Returns the formatted bytes from a double
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string FormatBytes(double bytes)
        {
            string type;
            if (bytes > 1024 * 1024)
            {
                type = "mb";
                bytes /= (1024 * 1024);
            }
            else if (bytes > 1024)
            {
                type = "kb";
                bytes /= 1024;
            }
            else
            {
                type = "b";
            }

            return $"{bytes:0}{type}";
        }

        /// <summary>
        /// Ensures a type or field name does not contain "special" characters
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string ReflectedName(string name)
        {
            int genericArgumentChar = name.IndexOf('`');
            return genericArgumentChar > 0 ? name.Substring(0, genericArgumentChar) : name;
        }

        /// <summary>
        /// Gets the path only for a directory
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetDirectoryName(string name)
        {
            try
            {
                name = name.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                return name.Substring(0, name.LastIndexOf(Path.DirectorySeparatorChar));
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the filename of a file without the extension
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetFileNameWithoutExtension(string value)
        {
            int lastIndex = value.Length - 1;
            for (int i = lastIndex; i >= 1; i--)
            {
                if (value[i] == '.')
                {
                    lastIndex = i - 1;
                    break;
                }
            }
            int firstIndex = 0;
            for (int i = lastIndex - 1; i >= 0; i--)
            {
                switch (value[i])
                {
                    case '/':
                    case '\\':
                        {
                            firstIndex = i + 1;
                            goto End;
                        }
                }
            }
        End:
            return value.Substring(firstIndex, lastIndex - firstIndex + 1);
        }

        /// <summary>
        /// Converts all path directory separaters to evironment-specific directory seperators
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string CleanPath(string path)
        {
            return path?.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// Converts a string of JSON to a JSON object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonstr"></param>
        /// <returns></returns>
        public static T ConvertFromJson<T>(string jsonstr) => JsonConvert.DeserializeObject<T>(jsonstr);

        /// <summary>
        /// Converts a JSON object to a string of JSON
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="indented"></param>
        /// <returns></returns>
        public static string ConvertToJson(object obj, bool indented = false)
        {
            return JsonConvert.SerializeObject(obj, indented ? Formatting.Indented : Formatting.None);
        }

        /// <summary>
        /// Gets the local network IP of the machine
        /// </summary>
        /// <returns></returns>
        public static IPAddress GetLocalIP()
        {
            UnicastIPAddressInformation mostSuitableIp = null;
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface network in networkInterfaces)
            {
#if DEBUG
                StringBuilder debugOutput = new StringBuilder();
                debugOutput.AppendLine(string.Empty);
#endif

                if (network.OperationalStatus == OperationalStatus.Up)
                {
                    IPInterfaceProperties properties = network.GetIPProperties();

                    if (properties.GatewayAddresses.Count == 0 || properties.GatewayAddresses[0].Address.Equals(IPAddress.Parse("0.0.0.0")))
                    {
                        continue;
                    }

                    foreach (UnicastIPAddressInformation ip in properties.UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily != AddressFamily.InterNetwork || IPAddress.IsLoopback(ip.Address))
                        {
                            continue;
                        }

#if DEBUG
                        debugOutput.AppendLine($"IP address: {ip.Address}");
                        debugOutput.AppendLine($"Is DNS eligible: {ip.IsDnsEligible}");
                        debugOutput.AppendLine($"Is lookback: {IPAddress.IsLoopback(ip.Address)}");
                        debugOutput.AppendLine($"Is using DHCP: {ip.PrefixOrigin == PrefixOrigin.Dhcp}");
                        debugOutput.AppendLine($"Address family: {ip.Address.AddressFamily}");
                        debugOutput.AppendLine($"Gateway address: {properties.GatewayAddresses[0].Address}");
#endif

                        if (!ip.IsDnsEligible)
                        {
                            if (mostSuitableIp == null)
                            {
                                mostSuitableIp = ip;
                            }

                            continue;
                        }

                        if (ip.PrefixOrigin != PrefixOrigin.Dhcp)
                        {
                            if (mostSuitableIp == null || !mostSuitableIp.IsDnsEligible)
                            {
                                mostSuitableIp = ip;
                            }

                            continue;
                        }

#if DEBUG
                        debugOutput.AppendLine($"Resulting IP address: {ip.Address}");
                        Interface.uMod.LogDebug(debugOutput.ToString());
#endif

                        return ip.Address;
                    }
                }
            }

#if DEBUG
            Interface.uMod.LogDebug($"Most suitable IP: {mostSuitableIp?.Address}");
#endif

            return mostSuitableIp?.Address;
        }

        /// <summary>
        /// Returns if the provided IP address is a local network IP
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static bool IsLocalIP(string ipAddress)
        {
            string[] split = ipAddress.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            int[] ip = { int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2]), int.Parse(split[3]) };
            return ip[0] == 0 || ip[0] == 10 || ip[0] == 100 && ip[1] == 64 || ip[0] == 127 || ip[0] == 192 && ip[1] == 168 || ip[0] == 172 && ip[1] >= 16 && ip[1] <= 31;
        }

        /// <summary>
        /// Returns if the provided IP address is a valid IPv4 address
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static bool ValidateIPv4(string ipAddress)
        {
            if (!string.IsNullOrEmpty(ipAddress.Trim()))
            {
                string[] splitValues = ipAddress.Replace("\"", string.Empty).Trim().Split('.');
                return splitValues.Length == 4 && splitValues.All(r => byte.TryParse(r, out _));
            }

            return false;
        }

        /// <summary>
        /// Gets only the numbers from a string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static int GetNumbers(string input)
        {
            int.TryParse(Regex.Replace(input, "[^.0-9]", ""), out int numbers);
            return numbers;
        }

        /// <summary>
        /// Gets file checksum
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="algorithm"></param>
        /// <returns></returns>
        internal static string GetHash(string filePath, HashAlgorithm algorithm)
        {
            using (BufferedStream stream = new BufferedStream(File.OpenRead(filePath), 100000))
            {
                byte[] hash = algorithm.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            }
        }

        /// <summary>
        /// Attempt to upgrade a file
        /// </summary>
        /// <param name="originalPath"></param>
        /// <param name="newPath"></param>
        /// <returns></returns>
        internal static bool TryUpgrade(string originalPath, string newPath)
        {
            if (!File.Exists(originalPath) || File.Exists(newPath)) // File upgraded or cannot be upgraded
            {
                return true;
            }

            try
            {
                File.Move(originalPath, newPath);
                return true;
            }
            catch (Exception)
            {
                // Ignore
            }

            return false;
        }

        /// <summary>
        /// Determine if two type signatures are equal
        /// </summary>
        public class TypeSignatureEqualityComparer : IEqualityComparer<Type[]>
        {
            /// <summary>
            /// Type signature equality comparison
            /// </summary>
            /// <param name="leftTypeArray"></param>
            /// <param name="rightTypeArray"></param>
            /// <returns></returns>
            public bool Equals(Type[] leftTypeArray, Type[] rightTypeArray)
            {
                if (rightTypeArray != null && leftTypeArray != null && leftTypeArray.Length != rightTypeArray.Length)
                {
                    return false;
                }

                bool matching = true;
                if (leftTypeArray != null)
                {
                    for (int i = 0; i < leftTypeArray.Length; i++)
                    {
                        if (rightTypeArray != null && leftTypeArray[i] != rightTypeArray[i])
                        {
                            matching = false;
                            break;
                        }
                    }
                }

                return matching;
            }

            /// <summary>
            /// Get HashCode for specified type array
            /// </summary>
            /// <param name="types"></param>
            /// <returns></returns>
            public int GetHashCode(Type[] types)
            {
                int result = 0;
                for (int i = 0; i < types.Length; i++)
                {
                    unchecked
                    {
                        result = types[i].GetHashCode();
                    }
                }
                return result;
            }
        }
    }
}
