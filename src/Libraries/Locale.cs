﻿using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Auth;
using uMod.Common;
using uMod.Exceptions;
using uMod.Plugins;

namespace uMod.Libraries
{
    /// <summary>
    /// Represents a library containing localization functions
    /// </summary>
    public class Locale : Library
    {
        /// <summary>
        /// Stores locale reference and associated plugin and language
        /// </summary>
        private struct LocaleReference : IContext
        {
            public string Name { get; }
            public INameable Plugin { get; }
            public object Locale { get; }
            public string Lang { get; }

            public LocaleReference(INameable plugin, object locale, string lang, string name)
            {
                Plugin = plugin;
                Locale = locale;
                Lang = lang;
                Name = name;
            }
        }

        private IAuthManager _authManager;
        private readonly Dictionary<string, Dictionary<string, LocaleReference>> _locales;
        private readonly Dictionary<Type, string> _localeNames;
        private readonly Dictionary<INameable, IList<Type>> _defaultLocaleProviders;
        private readonly Dictionary<IPlugin, ICallback<IPlugin, IPluginManager>> _pluginRemovedFromManager;

        /// <summary>
        /// Creates a new locale library
        /// </summary>
        public Locale(IApplication application) : base(application)
        {
            _locales = new Dictionary<string, Dictionary<string, LocaleReference>>();
            _localeNames = new Dictionary<Type, string>();
            _pluginRemovedFromManager = new Dictionary<IPlugin, ICallback<IPlugin, IPluginManager>>();
            _defaultLocaleProviders = new Dictionary<INameable, IList<Type>>();
        }

        /// <summary>
        /// Sets auth manager
        /// </summary>
        /// <param name="manager"></param>
        public void SetProvider(IAuthManager manager)
        {
            _authManager = manager;
        }

        /// <summary>
        /// Registers specified locale for specified plugin
        /// </summary>
        /// <param name="locale"></param>
        /// <param name="context"></param>
        /// <param name="lang"></param>
        [LibraryFunction("RegisterLocale")]
        public void RegisterLocale(ILocale locale, INameable context, string lang = Lang.DefaultLang)
        {
            if (locale == null)
            {
                throw new ArgumentNullException(nameof(locale), Interface.uMod.Strings.Validation.NotNull);
            }

            if (context == null)
            {
                throw new ArgumentNullException(nameof(context), Interface.uMod.Strings.Validation.NotNull);
            }

            if (locale.GetType().GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute)
            {
                lang = localeAttribute.Locale;
            }

            string name = GetLocaleName(locale.GetType(), context);
            if (!_locales.TryGetValue(name, out Dictionary<string, LocaleReference> references))
            {
                _locales.Add(name, references = new Dictionary<string, LocaleReference>());
            }

            if (!references.TryGetValue(lang, out LocaleReference _))
            {
                references.Add(lang, new LocaleReference(context, locale, lang, name));
            }

            if (context is IPlugin plugin && !_pluginRemovedFromManager.ContainsKey(plugin))
            {
                _pluginRemovedFromManager[plugin] = plugin.OnRemovedFromManager.Add(plugin_OnRemovedFromManager);
            }

            if (lang == (_authManager?.GetServerLanguage() ?? Lang.DefaultLang))
            {
                if (!_defaultLocaleProviders.TryGetValue(context, out IList<Type> defaultLang))
                {
                    _defaultLocaleProviders.Add(context, defaultLang = new List<Type>());
                }

                if (!defaultLang.Contains(locale.GetType()))
                {
                    defaultLang.Add(locale.GetType());
                }
            }
        }

        /// <summary>
        /// Gets locale of generic type for the specified plugin
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <returns></returns>
        public T GetLocale<T>(INameable context) where T : class
        {
            string lang = _authManager.GetServerLanguage();

            if (typeof(T).GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute)
            {
                lang = localeAttribute.Locale;
            }

            return GetLocale<T>(lang, context);
        }

        /// <summary>
        /// Gets locale of the generic type for the specified language and plugin
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lang"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public T GetLocale<T>(string lang, INameable context) where T : class
        {
            return GetLocale(typeof(T), lang, context) as T;
        }

        /// <summary>
        /// Gets locale of the specified type for the specified language and plugin
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="lang"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public object GetLocale(Type localeType, string lang, INameable context)
        {
            if (string.IsNullOrEmpty(lang))
            {
                throw new ArgumentNullException(nameof(lang), Interface.uMod.Strings.Validation.NotNullOrEmpty);
            }

            if (context == null)
            {
                throw new ArgumentNullException(nameof(context), Interface.uMod.Strings.Validation.NotNull);
            }

            if (!_locales.TryGetValue(GetLocaleName(localeType, context), out Dictionary<string, LocaleReference> references))
            {
                throw new LocaleException(Interface.uMod.Strings.Locale.NotFoundOfType.Interpolate(("locale", localeType.Name), ("context", context.Name), ("lang", lang)));
            }

            if (!references.TryGetValue(lang, out LocaleReference localeReference))
            {
                string serverLanguage = _authManager.GetServerLanguage();
                if (_defaultLocaleProviders.TryGetValue(context, out IList<Type> defaultLocales))
                {
                    if (lang != serverLanguage)
                    {
                        foreach (Type defaultLocaleType in defaultLocales)
                        {
                            if (localeType.IsAssignableFrom(defaultLocaleType))
                            {
                                return GetLocale(defaultLocaleType, serverLanguage, context);
                            }
                        }
                    }
                    else
                    {
                        foreach (Type defaultLocaleType in defaultLocales)
                        {
                            if (localeType.IsAssignableFrom(defaultLocaleType))
                            {
                                return GetLocale(defaultLocaleType, Lang.DefaultLang, context);
                            }
                        }
                    }
                }

                throw new LocaleException(Interface.uMod.Strings.Locale.NotFoundReference.Interpolate(("locale", localeType.Name), ("context", context.Name), ("lang", lang)));
            }

            return localeReference.Locale;
        }

        /// <summary>
        /// Gets a dictionary of locales for the specified languages and locale type
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="languages"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IDictionary<string, object> GetLocales(Type localeType, IEnumerable<string> languages, INameable context)
        {
            IDictionary<string, object> locales = new Dictionary<string, object>();
            foreach (string language in languages)
            {
                object locale = GetLocale(localeType, language, context);
                if (locale != null)
                {
                    locales.Add(language, locale);
                }
            }

            return locales;
        }

        /// <summary>
        /// Gets locale name of the specified type and plugin
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GetLocaleName(Type localeType, INameable context)
        {
            if (_localeNames.TryGetValue(localeType, out string name))
            {
                return name;
            }

            if (localeType.IsInterface)
            {
                if (localeType.GetCustomAttribute<LocalizationAttribute>() is LocalizationAttribute localizationAttribute)
                {
                    name = localizationAttribute.Name;
                }
            }
            else
            {
                Type[] interfaces = localeType.GetInterfaces();

                foreach (Type @interface in interfaces)
                {
                    if (@interface == typeof(ILocale))
                    {
                        break;
                    }

                    if (@interface.GetCustomAttribute<LocalizationAttribute>() is LocalizationAttribute localizationAttribute)
                    {
                        name = localizationAttribute.Name;
                        break;
                    }
                }
            }

            if (!string.IsNullOrEmpty(name))
            {
                name = $"{context.Name}_{name}";
            }
            else
            {
                name = context.Name;
            }

            _localeNames.Add(localeType, name);

            return name;
        }

        /// <summary>
        /// Resolve default locale providers that are currently loaded
        /// </summary>
        internal void ResolveDefaultLocaleProviders()
        {
            _defaultLocaleProviders.Clear();
            foreach (KeyValuePair<string, Dictionary<string, LocaleReference>> kvp in _locales)
            {
                foreach (KeyValuePair<string, LocaleReference> kvp2 in kvp.Value)
                {
                    Type messageType = kvp2.Value.Locale.GetType();
                    string lang;
                    if (messageType.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute)
                    {
                        lang = localeAttribute.Locale;
                    }
                    else
                    {
                        lang = Lang.DefaultLang;
                    }

                    if (lang == _authManager.GetServerLanguage())
                    {
                        if (!_defaultLocaleProviders.TryGetValue(kvp2.Value.Plugin, out IList<Type> defaultLang))
                        {
                            _defaultLocaleProviders.Add(kvp2.Value.Plugin, defaultLang = new List<Type>());
                        }

                        defaultLang.Add(messageType);
                    }
                }
            }
        }

        /// <summary>
        /// Called when a plugin is unloaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="manager"></param>
        private void plugin_OnRemovedFromManager(IPlugin sender, IPluginManager manager)
        {
            if (_pluginRemovedFromManager.TryGetValue(sender, out ICallback<IPlugin, IPluginManager> callback))
            {
                Event.Remove(ref callback);
                _pluginRemovedFromManager.Remove(sender);
            }

            Dictionary<string, Dictionary<string, LocaleReference>> pluginLocales = _locales.Where(x => x.Value.Any(y => y.Value.Plugin == sender)).ToDictionary(x => x.Key, x => x.Value);
            foreach (string key in pluginLocales.Keys)
            {
                IEnumerable<Type> localeTypes = _locales[key].Where(x => x.Value.Plugin == sender).Select(x => x.Value.Locale.GetType());
                foreach (Type type in localeTypes)
                {
                    if (_localeNames.ContainsKey(type))
                    {
                        _localeNames.Remove(type);
                    }
                }
                _locales.Remove(key);
            }

            if (_defaultLocaleProviders.ContainsKey(sender))
            {
                _defaultLocaleProviders.Remove(sender);
            }
        }
    }
}
