using System;
using System.Collections.Generic;
using System.Threading;
using uMod.Apps;
using uMod.Common;
using uMod.Common.Web;
using uMod.Plugins;

namespace uMod.Libraries
{
    /// <summary>
    /// The WebRequests library
    /// </summary>
    public class WebRequests : Library
    {
        [Obsolete("Use uMod.Common.Web.RequestMethod instead")]
        public enum RequestMethod
        {
            DELETE,
            GET,
            HEAD,
            PATCH,
            POST,
            PUT
        }

        private readonly ILogger _logger;
        private readonly AutoResetEvent _workevent = new AutoResetEvent(false);
        private readonly Queue<WebRequest> _queue = new Queue<WebRequest>();
        private readonly Thread _workerthread;
        private readonly int _maxCompletionPortThreads;
        private readonly int _maxWorkerThreads;
        private readonly object _syncRoot = new object();
        internal WebClient Client;

        private bool _shutdown;

        /// <summary>
        /// Specifies the HTTP request decompression support
        /// </summary>
        public static bool AllowDecompression = false;

        /// <summary>
        /// Represents a single WebRequest instance
        /// </summary>
        public class WebRequest : Web.Request, IDisposable
        {
            /// <summary>
            /// Get the request identifier
            /// </summary>
            public int Id { get; }

            /// <summary>
            /// Get the callback delegate
            /// </summary>
            public Action<string> FailCallback { get; internal set; }

            /// <summary>
            /// Get the callback delegate
            /// </summary>
            public Action<int, string> Callback { get; internal set; }

            /// <summary>
            /// Get the v2 callback delegate
            /// </summary>
            public Action<WebResponse> CallbackV2 { get; internal set; }

            /// <summary>
            /// Get the HTTP response text
            /// </summary>
            public string ErrorMessage { get; internal set; }

            /// <summary>
            /// Get the HTTP response object
            /// </summary>
            public WebResponse Response { get; protected set; }

            /// <summary>
            /// Get the plugin to which this web request belongs, if any
            /// </summary>
            public IPlugin Owner { get; private set; }

            /// <summary>
            /// Callback associated with unloading the owner plugin
            /// </summary>
            private ICallback<IPlugin, IPluginManager> _removedFromManager;

            /// <summary>
            /// Stores the module associated with web request
            /// </summary>
            private readonly ILogger _logger;

            /// <summary>
            /// Initializes a new instance of the WebRequest class
            /// </summary>
            /// <param name="client"></param>
            /// <param name="logger"></param>
            /// <param name="owner"></param>
            internal WebRequest(WebClient client, ILogger logger, IPlugin owner) : base(client, owner)
            {
                _logger = logger;
                Owner = owner;
                _removedFromManager = Owner?.OnRemovedFromManager.Add(owner_OnRemovedFromManager);
                Id = Utility.Random.Range(1, 999999);
            }

            /// <summary>
            /// Used by the worker thread to start the request
            /// </summary>
            internal override Web.Request Invoke()
            {
                Done(Completed, Failed);

                base.Invoke();
                return this;
            }

            public void Completed(WebResponse webResponse)
            {
                Response = webResponse;
                Event.Remove(ref _removedFromManager);

                Interface.uMod.NextTick(OnComplete);
            }

            public void Failed(Exception exception)
            {
                Failed(exception.Message);
            }

            public void Failed(string reason = null)
            {
                Event.Remove(ref _removedFromManager);

                if (!string.IsNullOrEmpty(reason))
                {
                    ErrorMessage = reason;
                }

                Interface.uMod.NextTick(OnFailed);
            }

            private void OnFailed()
            {
                int startedAt = 0;
                if (Owner != null)
                {
                    startedAt = Owner.Diagnostics.TrackStart();
                }

                try
                {
                    FailCallback?.Invoke(ErrorMessage ?? "No response");
                }
                catch (Exception ex)
                {
                    string message = "Web request callback raised an exception";
                    if (Owner != null)
                    {
                        message += $" in '{Owner.Name} v{Owner.Version}' plugin";
                    }

                    _logger.Report(message, ex);
                }

                Owner?.Diagnostics?.TrackEnd(startedAt);
                Owner = null;
            }

            private void OnComplete()
            {
                int startedAt = 0;
                if (Owner != null)
                {
                    startedAt = Owner?.Diagnostics?.TrackStart() ?? 0;
                }

                try
                {
                    if (Response != null)
                    {
                        Callback?.Invoke(Response.StatusCode, Response.ReadAsString());
                        CallbackV2?.Invoke(Response);
                    }
                    else
                    {
                        FailCallback?.Invoke(ErrorMessage ?? "No response");
                    }
                }
                catch (Exception ex)
                {
                    string message = "Web request callback raised an exception";
                    if (Owner != null)
                    {
                        message += $" in '{Owner.Name} v{Owner.Version}' plugin";
                    }

                    _logger.Report(message, ex);
                }

                Owner?.Diagnostics?.TrackEnd(startedAt);
                Owner = null;
            }

            /// <summary>
            /// Called when the owner plugin was unloaded
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="manager"></param>
            private void owner_OnRemovedFromManager(IPlugin sender, IPluginManager manager)
            {
            }

            #region IDisposable Support

            private bool _disposedValue; // To detect redundant calls

            protected virtual void Dispose(bool disposing)
            {
                if (!_disposedValue)
                {
                    if (disposing)
                    {
                        // Dispose managed state (managed objects)
                        Cookies?.Clear();
                        Headers?.Clear();
                        Response = null;
                    }

                    _disposedValue = true;
                }
            }

            void IDisposable.Dispose()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(true);
            }

            #endregion IDisposable Support
        }

        /// <summary>
        /// Initialize a new instance of the WebRequests library
        /// </summary>
        internal WebRequests(IApplication application, ILogger logger, WebClient client) : base(application)
        {
            _logger = logger;
            if (client != null)
            {
                Client = client;
            }
            else
            {
                return;
            }

            ThreadPool.GetMaxThreads(out _maxWorkerThreads, out _maxCompletionPortThreads);
            _maxCompletionPortThreads = (int)(_maxCompletionPortThreads * 0.6);
            _maxWorkerThreads = (int)(_maxWorkerThreads * 0.75);

            // Start worker thread
            _workerthread = new Thread(Worker);
            _workerthread.Start();
        }

        /// <summary>
        /// Shuts down the worker thread
        /// </summary>
        public override void Shutdown()
        {
            if (_shutdown)
            {
                return;
            }
            _shutdown = true;
            _workevent.Set();
            _workerthread.Join();
            Client?.Shutdown();
        }

        /// <summary>
        /// The worker thread method
        /// </summary>
        private void Worker()
        {
            try
            {
                while (!_shutdown)
                {
                    ThreadPool.GetAvailableThreads(out int workerThreads, out int completionPortThreads);
                    if (workerThreads <= _maxWorkerThreads || completionPortThreads <= _maxCompletionPortThreads)
                    {
                        Thread.Sleep(100);
                        continue;
                    }

                    WebRequest request = null;
                    lock (_syncRoot)
                    {
                        if (_queue.Count > 0)
                        {
                            request = _queue.Dequeue();
                        }
                    }

                    if (request != null)
                    {
                        request.Invoke();
                    }
                    else
                    {
                        _workevent.WaitOne();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Report("WebRequests worker: ", ex);
            }
        }

        /// <summary>
        /// Enqueue a DELETE, GET, PATCH, POST, HEAD, or PUT web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="body"></param>
        /// <param name="callback"></param>
        /// <param name="owner"></param>
        /// <param name="method"></param>
        /// <param name="headers"></param>
        /// <param name="timeout"></param>
        /// <param name="failCallback"></param>
        [LibraryFunction("Enqueue")]
        public void Enqueue(string url, string body, Action<int, string> callback, Plugin owner, WebRequestMethod method = WebRequestMethod.GET, Dictionary<string, string> headers = null, float timeout = 30f, Action<string> failCallback = null)
        {
            // Do not "resolve" this warning
            WebRequest request = new WebRequest(Client, _logger, owner) { Url = url, Callback = callback, Method = method, Headers = headers, Timeout = timeout, Body = body, FailCallback = failCallback };
            EnqueueRequest(request);
        }

        /// <summary>
        /// Enqueue a DELETE, GET, PATCH, POST, HEAD, or PUT web request
        /// </summary>
        /// <param name="url"></param>
        /// <param name="body"></param>
        /// <param name="callback"></param>
        /// <param name="owner"></param>
        /// <param name="method"></param>
        /// <param name="headers"></param>
        /// <param name="timeout"></param>
        /// <param name="failCallback"></param>
        [LibraryFunction("EnqueueV2")]
        public void Enqueue(string url, string body, Action<WebResponse> callback, Plugin owner, WebRequestMethod method = WebRequestMethod.GET, Dictionary<string, string> headers = null, float timeout = 30f, Action<string> failCallback = null)
        {
            // Do not "resolve" this warning
            WebRequest request = new WebRequest(Client, _logger, owner) { Url = url, CallbackV2 = callback, Method = method, Headers = headers, Timeout = timeout, Body = body, FailCallback = failCallback };
            EnqueueRequest(request);
        }

        /// <summary>
        /// Enqueue a web request
        /// </summary>
        /// <param name="request"></param>
        public void EnqueueRequest(WebRequest request)
        {
            lock (_syncRoot)
            {
                _queue.Enqueue(request);
            }
            _workevent.Set();
        }

        /// <summary>
        /// Return the current queue length
        /// </summary>
        /// <returns></returns>
        [LibraryFunction("GetQueueLength")]
        public int GetQueueLength() => _queue.Count;
    }
}
