extern alias References;

using System;
using System.Collections.Generic;
using uMod.Auth;
using uMod.Common;

namespace uMod.Libraries
{
    /// <summary>
    /// A library providing a unified permissions system
    /// </summary>
    public class Permission : Library
    {
        private IAuthManager _authManager;

        // Permission status
        public bool IsLoaded => _authManager?.IsLoaded ?? false;

        public Permission(IApplication application) : base(application)
        {
        }

        /// <summary>
        /// Set auth provider
        /// </summary>
        /// <param name="manager"></param>
        internal void SetProvider(IAuthManager manager)
        {
            _authManager = manager;
        }

        /// <summary>
        /// Exports user/group data to json
        /// </summary>
        /// <param name="prefix"></param>
        [LibraryFunction("Export")]
        public void Export(string prefix = "auth")
        {
            if (IsLoaded)
            {
                //Interface.uMod.DataFileSystem.WriteObject(prefix + ".groups", groupdata);
                //Interface.uMod.DataFileSystem.WriteObject(prefix + ".users", userdata);
            }
        }

        /// <summary>
        /// Saves all permissions data to the data files
        /// </summary>
        public IPromise SaveData()
        {
            return _authManager.Save();
        }

        /// <summary>
        /// Saves users permissions data to the data file
        /// </summary>
        public IPromise SaveUsers() => _authManager.Save(AuthData.Users);

        /// <summary>
        /// Saves groups permissions data to the data file
        /// </summary>
        public IPromise SaveGroups() => _authManager.Save(AuthData.Groups);

        /// <summary>
        /// Register user ID validation
        /// </summary>
        /// <param name="val"></param>
        public void RegisterValidate(Func<string, bool> val) => _authManager.Validate = val;

        /// <summary>
        /// Cleans invalid user ID entries
        /// </summary>
        public void CleanUp()
        {
            _authManager.CleanUp();
        }

        /// <summary>
        /// Migrate permissions from one group to another
        /// </summary>
        /// <param name="newGroup"></param>
        /// <param name="oldGroup"></param>
        public void MigrateGroup(string oldGroup, string newGroup)
        {
            _authManager.MigrateGroup(oldGroup, newGroup);
        }

        #region Permission Management

        /// <summary>
        /// Registers the specified permission
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        [LibraryFunction("RegisterPermission")]
        public void RegisterPermission(string name, IPlugin owner)
        {
            if (_authManager.RegisterPermission(name, owner))
            {
                Interface.CallHook("OnPermissionRegistered", name, owner);
            }
        }

        /// <summary>
        /// Unregisters the specified permission
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        [LibraryFunction("UnregisterPermission")]
        public void UnregisterPermission(string name, IPlugin owner)
        {
            if (_authManager.UnregisterPermission(name, owner))
            {
                Interface.CallHook("OnPermissionUnregistered", name, owner);
            }
        }

        /// <summary>
        /// Returns if the specified permission exists or not
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        [LibraryFunction("PermissionExists")]
        public bool PermissionExists(string name, IPlugin owner = null)
        {
            return _authManager.PermissionExists(name, owner);
        }

        #endregion Permission Management

        #region Querying

        /// <summary>
        /// Returns if the specified player ID is valid
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [LibraryFunction("UserIdValid")]
        public bool UserIdValid(string playerId) => _authManager.Validate == null || _authManager.Validate(playerId);

        /// <summary>
        /// Returns if the specified player exists
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [LibraryFunction("UserExists")]
        public bool UserExists(string playerId) => _authManager.UserExists(playerId);

        /// <summary>
        /// Updates the player name
        /// TODO: Move this to an abstraction under IPlayer
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="playerName"></param>
        [LibraryFunction("UpdateNickname")]
        public void UpdateNickname(string playerId, string playerName)
        {
            playerName = playerName.Sanitize();
            if (_authManager.UpdateName(playerId, playerName, out string oldName))
            {
                Interface.CallHook("OnUserNameUpdated", playerId, oldName, playerName);
            }
        }

        /// <summary>
        /// Check if user has a group
        /// </summary>
        /// <param name="playerId"></param>
        [LibraryFunction("UserHasAnyGroup")]
        public bool UserHasAnyGroup(string playerId) => _authManager.UserHasAnyGroup(playerId);

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        [LibraryFunction("GroupsHavePermission")]
        public bool GroupsHavePermission(HashSet<string> groups, string perm) => _authManager.GroupsHavePermission(groups, perm);

        /// <summary>
        /// Returns if the specified group has the specified permission or not
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        [LibraryFunction("GroupHasPermission")]
        public bool GroupHasPermission(string name, string perm)
        {
            return _authManager.GroupHasPermission(name, perm);
        }

        /// <summary>
        /// Checks if specified permission belongs to group or parent group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        [LibraryFunction("IsGroupPermissionInherited")]
        public bool IsGroupPermissionInherited(string name, string perm)
        {
            return _authManager.IsGroupPermissionInherited(name, perm);
        }

        /// <summary>
        /// Returns the parent group that the specified permission belongs to
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        [LibraryFunction("GetGroupPermissionGroup")]
        public string GetGroupPermissionGroup(string name, string perm)
        {
            return _authManager.GetGroupPermissionGroup(name, perm);
        }

        /// <summary>
        /// Returns if the specified player has the specified permission
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        [LibraryFunction("UserHasPermission")]
        public bool UserHasPermission(string playerId, string perm)
        {
            return _authManager.UserHasPermission(playerId, perm);
        }

        /// <summary>
        /// Returns if the specified player permission is inherited from a group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        [LibraryFunction("IsUserPermissionInherited")]
        public bool IsUserPermissionInherited(string playerId, string perm)
        {
            return _authManager.IsUserPermissionInherited(playerId, perm);
        }

        /// <summary>
        /// Returns the group that a specified player permission is inherited from
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        [LibraryFunction("GetUserPermissionGroup")]
        public string GetUserPermissionGroup(string playerId, string perm)
        {
            return _authManager.GetUserPermissionGroup(playerId, perm);
        }

        /// <summary>
        /// Returns the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [LibraryFunction("GetUserGroups")]
        public string[] GetUserGroups(string playerId) => _authManager.GetUserGroups(playerId);

        /// <summary>
        /// Returns the permissions which the specified player has
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        [LibraryFunction("GetUserPermissions")]
        public string[] GetUserPermissions(string playerId)
        {
            return _authManager.GetUserPermissions(playerId);
        }

        /// <summary>
        /// Returns the permissions which the specified group has, with optional transversing of parent groups
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parents"></param>
        /// <returns></returns>
        [LibraryFunction("GetGroupPermissions")]
        public string[] GetGroupPermissions(string name, bool parents = false)
        {
            return _authManager.GetGroupPermissions(name, parents);
        }

        /// <summary>
        /// Returns the permissions which are registered
        /// </summary>
        /// <returns></returns>
        [LibraryFunction("GetPermissions")]
        public string[] GetPermissions() => _authManager.GetPermissions();

        /// <summary>
        /// Returns the permissions which are registered
        /// </summary>
        /// <returns></returns>
        public string[] GetPermissions(IPlugin plugin)
        {
            return _authManager.GetPermissions(plugin);
        }

        /// <summary>
        /// Returns the players with given permission
        /// </summary>
        /// <returns></returns>
        [LibraryFunction("GetPermissionUsers")]
        public string[] GetPermissionUsers(string perm)
        {
            return _authManager.GetPermissionUsers(perm);
        }

        /// <summary>
        /// Returns the groups with given permission
        /// </summary>
        /// <returns></returns>
        [LibraryFunction("GetPermissionGroups")]
        public string[] GetPermissionGroups(string perm)
        {
            return _authManager.GetPermissionGroups(perm);
        }

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [LibraryFunction("AddUserGroup")]
        public void AddUserGroup(string playerId, string name)
        {
            if (_authManager.AddUserGroup(playerId, name))
            {
                Interface.Call("OnUserGroupAdded", playerId, name);
            }
        }

        /// <summary>
        /// Set the group to which the specified player belongs
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [LibraryFunction("RemoveUserGroup")]
        public void RemoveUserGroup(string playerId, string name)
        {
            if (_authManager.RemoveUserGroup(playerId, name))
            {
                Interface.Call("OnUserGroupRemoved", playerId, name);
            }
        }

        /// <summary>
        /// Get if the player belongs to given group
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [LibraryFunction("UserHasGroup")]
        public bool UserHasGroup(string playerId, string name)
        {
            return _authManager.UserHasGroup(playerId, name);
        }

        /// <summary>
        /// Returns if the specified group exists or not
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        [LibraryFunction("GroupExists")]
        public bool GroupExists(string group)
        {
            return _authManager.GroupExists(group);
        }

        /// <summary>
        /// Returns existing groups
        /// </summary>
        /// <returns></returns>
        [LibraryFunction("GetGroups")]
        public string[] GetGroups() => _authManager.GetGroups();

        /// <summary>
        /// Returns users in that group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        [LibraryFunction("GetUsersInGroup")]
        public string[] GetUsersInGroup(string group)
        {
            return _authManager.GetUsersInGroup(group);
        }

        /// <summary>
        /// Returns the title of the specified group
        /// </summary>
        /// <param name="group"></param>
        [LibraryFunction("GetGroupTitle")]
        public string GetGroupTitle(string group)
        {
            return _authManager.GetGroupTitle(group);
        }

        /// <summary>
        /// Returns the rank of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        [LibraryFunction("GetGroupRank")]
        public int GetGroupRank(string group)
        {
            return _authManager.GetGroupRank(group);
        }

        #endregion Querying

        #region User Permission

        /// <summary>
        /// Grants the specified permission to the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        /// <param name="owner"></param>
        [LibraryFunction("GrantUserPermission")]
        public void GrantUserPermission(string playerId, string perm, IPlugin owner)
        {
            if (_authManager.GrantUserPermission(playerId, perm, owner))
            {
                Interface.Call("OnUserPermissionGranted", playerId, perm);
            }
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="perm"></param>
        [LibraryFunction("RevokeUserPermission")]
        public void RevokeUserPermission(string playerId, string perm)
        {
            if (_authManager.RevokeUserPermission(playerId, perm))
            {
                Interface.Call("OnUserPermissionRevoked", playerId, perm);
            }
        }

        #endregion User Permission

        #region Group Permission

        /// <summary>
        /// Grant the specified permission to the specified group
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        /// <param name="owner"></param>
        [LibraryFunction("GrantGroupPermission")]
        public void GrantGroupPermission(string name, string perm, IPlugin owner)
        {
            if (_authManager.GrantGroupPermission(name, perm, owner))
            {
                Interface.Call("OnGroupPermissionGranted", name, perm);
            }
        }

        /// <summary>
        /// Revokes the specified permission from the specified player
        /// </summary>
        /// <param name="name"></param>
        /// <param name="perm"></param>
        [LibraryFunction("RevokeGroupPermission")]
        public void RevokeGroupPermission(string name, string perm)
        {
            if (_authManager.RevokeGroupPermission(name, perm))
            {
                Interface.Call("OnGroupPermissionRevoked", name, perm);
            }
        }

        #endregion Group Permission

        #region Group Management

        /// <summary>
        /// Creates the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="title"></param>
        /// <param name="rank"></param>
        /// <param name="parent"></param>
        [LibraryFunction("CreateGroup")]
        public bool CreateGroup(string group, string title, int rank, string parent = "")
        {
            if (_authManager.CreateGroup(group, title, rank, parent))
            {
                Interface.CallHook("OnGroupCreated", group, title, rank, parent);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the specified group
        /// </summary>
        /// <param name="group"></param>
        [LibraryFunction("RemoveGroup")]
        public bool RemoveGroup(string group)
        {
            if (_authManager.RemoveGroup(group))
            {
                Interface.CallHook("OnGroupDeleted", group);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the title of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="title"></param>
        [LibraryFunction("SetGroupTitle")]
        public bool SetGroupTitle(string group, string title)
        {
            if (_authManager.SetGroupTitle(group, title))
            {
                Interface.CallHook("OnGroupTitleSet", group, title);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the rank of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="rank"></param>
        [LibraryFunction("SetGroupRank")]
        public bool SetGroupRank(string group, int rank)
        {
            if (_authManager.SetGroupRank(group, rank))
            {
                Interface.CallHook("OnGroupRankSet", group, rank);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the parent of the specified group
        /// </summary>
        /// <param name="group"></param>
        [LibraryFunction("GetGroupParent")]
        public string GetGroupParent(string group)
        {
            return _authManager.GetGroupParent(group);
        }

        /// <summary>
        /// Sets the parent of the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="parent"></param>
        [LibraryFunction("SetGroupParent")]
        public bool SetGroupParent(string group, string parent)
        {
            if (_authManager.SetGroupParent(group, parent))
            {
                Interface.CallHook("OnGroupParentSet", group, parent);
                return true;
            }

            return false;
        }

        #endregion Group Management
    }
}
