using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uMod.Auth;
using uMod.Command;
using uMod.Common;

namespace uMod.Libraries.Covalence
{
    [Obsolete("Use Universal instead")]
    public class Covalence : Universal
    {
        public Covalence(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}

namespace uMod.Libraries
{
    /// <summary>
    /// The Universal library
    /// </summary>
    public class Universal : Library, INameable
    {
        internal IUniversalProvider Provider;
        private readonly ILogger _logger;

        /// <summary>
        /// Gets the name "umod"
        /// </summary>
        public string Name { get; } = "umod";

        /// <summary>
        /// Gets the command system adapter
        /// </summary>
        public ICommandSystem CommandSystem { get; private set; }

        /// <summary>
        /// Gets the server mediator
        /// </summary>
        [LibraryProperty("Server")]
        public IServer Server { get; private set; }

        internal PlayerManager playerManager;

        /// <summary>
        /// Gets the player manager mediator
        /// </summary>
        [LibraryProperty("Players")]
        public IPlayerManager Players { get; private set; }

        /// <summary>
        /// Gets the name of the current game
        /// </summary>
        [LibraryProperty("Game")]
        public string Game => Provider?.GameName ?? string.Empty;

        /// <summary>
        /// Gets the Steam app ID of the game's client, if available
        /// </summary>
        [LibraryProperty("ClientAppId")]
        public uint ClientAppId => Provider?.ClientAppId ?? 0;

        /// <summary>
        /// Gets the Steam app ID of the game's server, if available
        /// </summary>
        [LibraryProperty("ServerAppId")]
        public uint ServerAppId => Provider?.ServerAppId ?? 0;

        /// <summary>
        /// Formats the text with markup into the game-specific markup language
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string FormatText(string text) => Provider.FormatText(text);

        /// <summary>
        /// Initializes a new instance of the Universal class
        /// </summary>
        public Universal(IApplication application, ILogger logger) : base(application)
        {
            _logger = logger;
        }

        /// <summary>
        /// Initializes the Universal library
        /// </summary>
        public override void Initialize()
        {
            List<Type> candidates = Interface.uMod.Types.Resolve<IUniversalProvider>().ToList();
            Type selectedCandidate;
            if (candidates.Count == 0)
            {
                _logger.Write(LogLevel.Warning, Interface.uMod.Strings.Module.UniversalProviderNotFound);
                return;
            }

            if (candidates.Count > 1)
            {
                selectedCandidate = candidates[0];
                StringBuilder sb = new StringBuilder();
                for (int i = 1; i < candidates.Count; i++)
                {
                    if (i > 1)
                    {
                        sb.Append(',');
                    }
                    sb.Append(candidates[i].FullName);
                }

                _logger.Warning(
                    Interface.uMod.Strings.Module.UniversalProviderFoundMultiple.Interpolate(
                        ("name", selectedCandidate), ("extra", sb)));
            }
            else
            {
                selectedCandidate = candidates[0];
            }

            try
            {
                Provider = Application.Make<IUniversalProvider>(selectedCandidate);
                Application.Bind(Provider);
            }
            catch (Exception ex)
            {
                _logger.Report(Interface.uMod.Strings.Module.UniversalProviderException, ex);
                return;
            }

            List<Type> decoratorTypes = Interface.uMod.Types.Resolve<IHookDecorator>().ToList();
            foreach (Type type in decoratorTypes)
            {
                try
                {
                    if (type.IsAbstract || type.IsInterface)
                    {
                        continue;
                    }

                    string name = Utility.ReflectedName(type.Name);
                    if (name.EndsWith("Decorator") && "Decorator".Length < name.Length)
                    {
                        name = name.Substring(0, name.IndexOf("Decorator", StringComparison.Ordinal));
                    }

                    Application.Resolve(type);
                    Application.Alias(name, type);
                }
                catch (Exception ex)
                {
                    _logger.Report(Interface.uMod.Strings.Module.HookDecoratorException.Interpolate("type", type.Name), ex);
                    return;
                }
            }

            Type impliedPlayerType = null;
            IEnumerable<Type> possiblePlayerTypes = Interface.uMod.Types.Resolve<IPlayer>();
            int possiblePlayerTypeCount = possiblePlayerTypes?.Count() ?? 0;
            if (possiblePlayerTypeCount > 1)
            {
                foreach (Type possiblePlayerType in possiblePlayerTypes)
                {
                    if (!possiblePlayerType.IsSubclassOf(typeof(UniversalPlayer)))
                    {
                        continue;
                    }

                    impliedPlayerType = possiblePlayerType;
                    break;
                }
            }

            if (possiblePlayerTypeCount > 0 && impliedPlayerType == null)
            {
                impliedPlayerType = possiblePlayerTypes.FirstOrDefault();
            }

            if (impliedPlayerType != null)
            {
                Application.Resolve(impliedPlayerType);
                Application.Alias(nameof(IPlayer), impliedPlayerType);
            }
            else
            {
                _logger.Write(LogLevel.Error, Interface.uMod.Strings.Module.PlayerTypeNotFound);
            }

            Server = Application.Bind(Provider.CreateServer());
            Application.Bind(typeof(IServer), Server);

            IPlayerManager gamePlayerManager = Provider.CreatePlayerManager(Application, _logger);

            if (gamePlayerManager is PlayerManager playerManager)
            {
                this.playerManager = playerManager;
            }
            else
            {
                this.playerManager = playerManager = new PlayerManager(Application, _logger);
            }
            Players = Application.Bind(playerManager);
            Application.Bind(typeof(IPlayerManager), Players);
            Server.PlayerManager = Players;

            CommandSystem = Provider.CreateCommandSystemProvider(new CommandHandler(Interface.uMod.Plugins));

            _logger.Info(Interface.uMod.Strings.Module.UniversalProviderFound.Interpolate("game", Provider.GameName));
        }

        /// <summary>
        /// Registers a command (chat + console)
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public void RegisterCommand(string command, IPlugin plugin, CommandCallback callback)
        {
            if (CommandSystem == null)
            {
                _logger.Warning("Failed to register command due to missing command system."); // TODO: Localization
                return;
            }

            try
            {
                CommandSystem.RegisterCommand(command, plugin, callback);
            }
            catch (CommandAlreadyExistsException)
            {
                string pluginName = plugin?.Name ?? Interface.uMod.Strings.Plugin.UnknownPlugin;
                _logger.Error(Interface.uMod.Strings.Command.CommandAlreadyExists.Interpolate(("plugin", pluginName), ("command", command)));
            }
        }

        /// <summary>
        /// Unregisters a command (chat + console)
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        public void UnregisterCommand(string command, IPlugin plugin) => CommandSystem?.UnregisterCommand(command, plugin);
    }
}
