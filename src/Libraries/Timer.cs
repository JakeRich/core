using System;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.Libraries
{
    /// <summary>
    /// The timer library
    /// </summary>
    public class Timer : Library
    {
        public static int Count { get; private set; }

        internal static readonly object Lock = new object();

        public class TimeSlot
        {
            public int Count;
            public TimerInstance FirstInstance;
            public TimerInstance LastInstance;

            public void GetExpired(double now, Queue<TimerInstance> queue)
            {
                TimerInstance instance = FirstInstance;
                while (instance != null)
                {
                    if (instance.ExpiresAt > now)
                    {
                        break;
                    }

                    queue.Enqueue(instance);
                    instance = instance.NextInstance;
                }
            }

            public void InsertTimer(TimerInstance timer)
            {
                float expiresAt = timer.ExpiresAt;

                TimerInstance firstInstance = FirstInstance;
                TimerInstance lastInstance = LastInstance;

                TimerInstance nextInstance = firstInstance;
                if (firstInstance != null)
                {
                    float firstAt = firstInstance.ExpiresAt;
                    float lastAt = lastInstance.ExpiresAt;
                    if (expiresAt <= firstAt)
                    {
                        nextInstance = firstInstance;
                    }
                    else if (expiresAt >= lastAt)
                    {
                        nextInstance = null;
                    }
                    else if (lastAt - expiresAt < expiresAt - firstAt)
                    {
                        nextInstance = lastInstance;
                        TimerInstance instance = nextInstance;
                        while (instance != null)
                        {
                            if (instance.ExpiresAt <= expiresAt)
                            {
                                // We need to insert after this instance
                                break;
                            }
                            nextInstance = instance;
                            instance = instance.PreviousInstance;
                        }
                    }
                    else
                    {
                        while (nextInstance != null)
                        {
                            if (nextInstance.ExpiresAt > expiresAt)
                            {
                                break;
                            }

                            nextInstance = nextInstance.NextInstance;
                        }
                    }
                }

                if (nextInstance == null)
                {
                    timer.NextInstance = null;
                    if (lastInstance == null)
                    {
                        FirstInstance = timer;
                        LastInstance = timer;
                    }
                    else
                    {
                        lastInstance.NextInstance = timer;
                        timer.PreviousInstance = lastInstance;
                        LastInstance = timer;
                    }
                }
                else
                {
                    TimerInstance previous = nextInstance.PreviousInstance;
                    if (previous == null)
                    {
                        FirstInstance = timer;
                    }
                    else
                    {
                        previous.NextInstance = timer;
                    }
                    nextInstance.PreviousInstance = timer;
                    timer.PreviousInstance = previous;
                    timer.NextInstance = nextInstance;
                }

                timer.Added(this);
            }
        }

        /// <summary>
        /// Represents a single timer instance
        /// </summary>
        public class TimerInstance : ITimer
        {
            public const int MaxPooled = 5000;

            internal static Queue<TimerInstance> Pool = new Queue<TimerInstance>();

            /// <summary>
            /// Gets the number of repetitions left on this timer
            /// </summary>
            public int Repetitions { get; private set; }

            /// <summary>
            /// Gets the delay between each repetition
            /// </summary>
            public float Delay { get; private set; }

            /// <summary>
            /// Gets the callback delegate
            /// </summary>
            public Action Callback { get; private set; }

            /// <summary>
            /// Gets if this timer has been destroyed
            /// </summary>
            public bool Destroyed { get; private set; }

            /// <summary>
            /// Gets the plugin to which this timer belongs, if any
            /// </summary>
            public IPlugin Owner { get; private set; }

            internal float ExpiresAt;

            internal TimeSlot TimeSlot;
            internal TimerInstance NextInstance;
            internal TimerInstance PreviousInstance;

            private ICallback<IPlugin, IPluginManager> _removedFromManager;

            private Timer _timer;

            internal TimerInstance(Timer timer, int repetitions, float delay, Action callback, IPlugin owner)
            {
                Load(timer, repetitions, delay, callback, owner);
            }

            internal void Load(Timer timer, int repetitions, float delay, Action callback, IPlugin owner)
            {
                _timer = timer;
                Repetitions = repetitions;
                Delay = delay;
                Callback = callback;
                ExpiresAt = timer._module.Now + delay;
                Owner = owner;
                Destroyed = false;
                if (owner != null)
                {
                    _removedFromManager = owner.OnRemovedFromManager.Add(OnRemovedFromManager);
                }
            }

            /// <summary>
            /// Resets the timer optionally changing the delay setting a number of repetitions
            /// </summary>
            /// <param name="delay">The new delay between repetitions</param>
            /// <param name="repetitions">Number of repetitions before being destroyed</param>
            public void Reset(float delay = -1, int repetitions = 1)
            {
                lock (Lock)
                {
                    if (delay < 0)
                    {
                        delay = Delay;
                    }
                    else
                    {
                        Delay = delay;
                    }

                    Repetitions = repetitions;
                    ExpiresAt = _timer._module.Now + delay;
                    if (Destroyed)
                    {
                        Destroyed = false;
                        IPlugin owner = Owner;
                        if (owner != null)
                        {
                            _removedFromManager = owner.OnRemovedFromManager.Add(OnRemovedFromManager);
                        }
                    }
                    else
                    {
                        Remove();
                    }
                    _timer.InsertTimer(this);
                }
            }

            /// <summary>
            /// Destroys this timer
            /// </summary>
            public bool Destroy()
            {
                lock (Lock)
                {
                    if (Destroyed)
                    {
                        return false;
                    }

                    Destroyed = true;
                    Remove();
                    Event.Remove(ref _removedFromManager);
                }
                return true;
            }

            /// <summary>
            /// Destroys this timer and adds it to the pool
            /// </summary>
            public bool DestroyToPool()
            {
                lock (Lock)
                {
                    if (Destroyed)
                    {
                        return false;
                    }

                    Destroyed = true;
                    Callback = null;
                    Remove();
                    Event.Remove(ref _removedFromManager);
                    Queue<TimerInstance> pooledInstances = Pool;
                    if (pooledInstances.Count < MaxPooled)
                    {
                        pooledInstances.Enqueue(this);
                    }
                }
                return true;
            }

            internal void Added(TimeSlot timeSlot)
            {
                timeSlot.Count++;
                Count++;
                TimeSlot = timeSlot;
            }

            internal void Invoke(float now)
            {
                if (Repetitions > 0)
                {
                    if (--Repetitions == 0)
                    {
                        Destroy();
                        FireCallback();
                        return;
                    }
                }

                Remove();

                float expiresAt = ExpiresAt + Delay;
                ExpiresAt = expiresAt;
                _timer.InsertTimer(this, expiresAt < now);

                FireCallback();
            }

            internal void Remove()
            {
                TimeSlot slot = TimeSlot;
                if (slot == null)
                {
                    return;
                }

                slot.Count--;
                Count--;

                TimerInstance previous = PreviousInstance;
                TimerInstance next = NextInstance;

                if (next == null)
                {
                    slot.LastInstance = previous;
                }
                else
                {
                    next.PreviousInstance = previous;
                }

                if (previous == null)
                {
                    slot.FirstInstance = next;
                }
                else
                {
                    previous.NextInstance = next;
                }

                TimeSlot = null;
                PreviousInstance = null;
                NextInstance = null;
            }

            private void FireCallback()
            {
                int startedAt = 0;
                if (Owner != null)
                {
                    startedAt = Owner.Diagnostics.TrackStart();
                }

                try
                {
                    Callback();
                }
                catch (Exception ex)
                {
                    Destroy();
                    string errorMessage = $"Failed to run a {Delay:0.00} timer";
                    if (Owner != null)
                    {
                        errorMessage += $" in '{Owner.Name} v{Owner.Version}'";
                    }

                    if (ex.GetType().Name == "AssertFailedException")
                    {
                        throw;
                    }

                    Interface.uMod.LogException(errorMessage, ex);
                }
                finally
                {
                    Owner?.Diagnostics?.TrackEnd(startedAt);
                }
            }

            private void OnRemovedFromManager(IPlugin sender, IPluginManager manager) => Destroy();
        }

        // An even number of time slots is required. More slots means more efficient inserts with a higher number of timers but also more per-frame overhead.
        public const int TimeSlots = 512;
        public const int LastTimeSlot = TimeSlots - 1;
        public const float TickDuration = .01f;

        private readonly TimeSlot[] _timeSlots = new TimeSlot[TimeSlots];
        private readonly Queue<TimerInstance> _expiredInstanceQueue = new Queue<TimerInstance>();

        private int _currentSlot;

        // This needs to be a double in order to avoid precision errors
        private double _nextSlotAt = TickDuration;
        private readonly IModule _module;

        public Timer(IModule module, IApplication application) : base(application)
        {
            _module = module;
            for (int i = 0; i < TimeSlots; i++)
            {
                _timeSlots[i] = new TimeSlot();
            }
        }

        /// <summary>
        /// Called every server frame to process expired timers
        /// </summary>
        public void Update(float delta)
        {
            float now = _module.Now;
            TimeSlot[] timeSlots = _timeSlots;
            Queue<TimerInstance> expiredQueue = _expiredInstanceQueue;
            int checkedSlots = 0;

            lock (Lock)
            {
                int currentSlot = _currentSlot;
                double nextSlotAt = _nextSlotAt;

                while (true)
                {
                    timeSlots[currentSlot].GetExpired(nextSlotAt > now ? now : nextSlotAt, expiredQueue);

                    // Only move to the next slot once real time is out of the current slot so that the current slot is rechecked each frame
                    if (now <= nextSlotAt)
                    {
                        break;
                    }

                    checkedSlots++;
                    currentSlot = currentSlot < LastTimeSlot ? currentSlot + 1 : 0;
                    nextSlotAt += TickDuration;
                }

                if (checkedSlots > 0)
                {
                    _currentSlot = currentSlot;
                    _nextSlotAt = nextSlotAt;
                }

                int expiredCount = expiredQueue.Count;
                for (int i = 0; i < expiredCount; i++)
                {
                    TimerInstance instance = expiredQueue.Dequeue();
                    if (!instance.Destroyed)
                    {
                        instance.Invoke(now);
                    }
                }
            }
        }

        internal TimerInstance AddTimer(int repetitions, float delay, Action callback, IPlugin owner = null)
        {
            lock (Lock)
            {
                TimerInstance timer;
                Queue<TimerInstance> pooledInstances = TimerInstance.Pool;
                if (pooledInstances.Count > 0)
                {
                    timer = pooledInstances.Dequeue();
                    timer.Load(this, repetitions, delay, callback, owner);
                }
                else
                {
                    timer = new TimerInstance(this, repetitions, delay, callback, owner);
                }
                InsertTimer(timer, timer.ExpiresAt < _module.Now);
                return timer;
            }
        }

        private void InsertTimer(TimerInstance timer, bool inPast = false)
        {
            int index = inPast ? _currentSlot : (int)(timer.ExpiresAt / TickDuration) & LastTimeSlot;
            _timeSlots[index].InsertTimer(timer);
        }

        /// <summary>
        /// Creates a timer that fires once
        /// </summary>
        /// <param name="delay"></param>
        /// <param name="callback"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        [LibraryFunction("Once")]
        public TimerInstance Once(float delay, Action callback, IPlugin owner = null) => AddTimer(1, delay, callback, owner);

        /// <summary>
        /// Creates a timer that fires many times
        /// </summary>
        /// <param name="delay"></param>
        /// <param name="reps"></param>
        /// <param name="callback"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        [LibraryFunction("Repeat")]
        public TimerInstance Repeat(float delay, int reps, Action callback, IPlugin owner = null) => AddTimer(reps, delay, callback, owner);

        /// <summary>
        /// Creates a timer that fires once next frame
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        [LibraryFunction("NextFrame")]
        public TimerInstance NextFrame(Action callback) => AddTimer(1, 0.0f, callback);
    }
}
