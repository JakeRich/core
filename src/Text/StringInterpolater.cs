﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using uMod.Pooling;

namespace uMod.Text
{
    /// <summary>
    /// Specify field or property placeholder name for string interpolation
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class PlaceholderAttribute : Attribute
    {
        public string Name { get; }

        public PlaceholderAttribute(string name)
        {
            Name = name;
        }
    }

    /// <summary>
    /// Specify field or property to be ignored during string interpolation
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class PlaceholderIgnoreAttribute : Attribute
    {
    }

    /// <summary>
    /// Represents a Run-time string interpolater
    /// </summary>
    public class StringInterpolater
    {
        /// <summary>
        /// Represents a text placeholder
        /// </summary>
        private struct TextPlaceholder
        {
            public TextPlaceholder(string fullString, string name, string format)
            {
                FullString = fullString;
                Name = name;
                Format = format;
            }

            public bool HasFormat => Format != null;

            public string FullString { get; }
            public string Name { get; }
            public string Format { get; }
        }

        private readonly Regex _placeholderRegex =
            new Regex(@"{(?<name>(?:[A-Za-z0-9_.]+)\w*)(?:\|(?<format>[\w.:;\-_\/\\ ]+))?}",
                RegexOptions.Compiled | RegexOptions.CultureInvariant);

        private readonly Regex _choiceRegex = new Regex(@"\|(?![^{]*})", RegexOptions.Compiled | RegexOptions.CultureInvariant);

        private readonly Regex _choiceOptions = new Regex(@"^(?:\[(?:(?<start>\d)(?:\,(?<end>[\d\*]))?)\])?(?<format>.*)", RegexOptions.Compiled | RegexOptions.CultureInvariant);

        private readonly Cache<string, TextPlaceholder[]> _placeholderCache = new Cache<string, TextPlaceholder[]>();

        private readonly DictionaryPool<string, object> DictionaryPool = Pools.Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Interpolate using specified placeholder name and value
        /// </summary>
        /// <param name="format"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string FormatValue(string format, string name, object value)
        {
            return FormatValue(format, (name, value));
        }

        /// <summary>
        /// Interpolate using specified placeholder tuples
        /// </summary>
        /// <param name="format"></param>
        /// <param name="pairs"></param>
        /// <returns></returns>
        public string FormatValue(string format, params (string name, object value)[] pairs)
        {
            StringBuilder stringBuilder = Pools.StringBuilders.Get();
            try
            {
                stringBuilder.Append(format);

                if (_placeholderCache.TryGetValue(format, out TextPlaceholder[] placeholders))
                {
                    for (int i = 0; i < placeholders.Length; i++)
                    {
                        foreach ((string name, object value) in pairs)
                        {
                            if (placeholders[i].Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                            {
                                ReplacePlaceholder(stringBuilder, placeholders[i], value);
                            }
                        }
                    }
                }
                else
                {
                    MatchCollection matches = _placeholderRegex.Matches(format);

                    placeholders = new TextPlaceholder[matches.Count];

                    for (int i = 0; i < matches.Count; i++)
                    {
                        Match match = matches[i];

                        Group formatGroup = match.Groups["format"];

                        TextPlaceholder placeholder = new TextPlaceholder(
                            match.Value,
                            match.Groups["name"].Value,
                            formatGroup.Success ? formatGroup.Value : null);

                        placeholders[i] = placeholder;

                        foreach ((string name, object value) in pairs)
                        {
                            if (placeholder.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                            {
                                ReplacePlaceholder(stringBuilder, placeholder, value);
                            }
                        }
                    }

                    _placeholderCache.Add(format, placeholders);
                }

                return stringBuilder.ToString();
            }
            finally
            {
                Pools.StringBuilders.Free(stringBuilder);
            }
        }

        /// <summary>
        /// Format dictionary of placeholders
        /// </summary>
        /// <param name="format"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        private string FormatDictionary(string format, IDictionary values)
        {
            StringBuilder stringBuilder = Pools.StringBuilders.Get();
            try
            {
                stringBuilder.Append(format);

                if (_placeholderCache.TryGetValue(format, out TextPlaceholder[] placeholders))
                {
                    for (int i = 0; i < placeholders.Length; i++)
                    {
                        ReplacePlaceholder(stringBuilder, placeholders[i], values);
                    }
                }
                else
                {
                    MatchCollection matches = _placeholderRegex.Matches(format);

                    placeholders = new TextPlaceholder[matches.Count];

                    for (int i = 0; i < matches.Count; i++)
                    {
                        Match match = matches[i];

                        Group formatGroup = match.Groups["format"];

                        TextPlaceholder placeholder = new TextPlaceholder(
                            match.Value,
                            match.Groups["name"].Value,
                            formatGroup.Success ? formatGroup.Value : null);

                        placeholders[i] = placeholder;

                        ReplacePlaceholder(stringBuilder, placeholder, values);
                    }

                    _placeholderCache.Add(format, placeholders);
                }

                return stringBuilder.ToString();
            }
            finally
            {
                Pools.StringBuilders.Free(stringBuilder);
            }
        }

        /// <summary>
        /// Determines which format choice to use using the specified amount
        /// </summary>
        /// <param name="choices"></param>
        /// <param name="choiceAmount"></param>
        /// <returns></returns>
        private string GetFormatChoice(string[] choices, int choiceAmount)
        {
            int i = choices.Length == 2 ? 1 : 0;
            foreach (string choice in choices)
            {
                Match match = _choiceOptions.Match(choice);

                if (!int.TryParse(match.Groups["start"].Value, out int start))
                {
                    start = i;
                }

                int end = 0;
                if (match.Groups["end"].Value == "*")
                {
                    end = -1;
                }
                else if (!int.TryParse(match.Groups["end"].Value, out end))
                {
                    end = 0;
                }

                if (end == 0 && choiceAmount == start)
                {
                    return match.Groups["format"].Value;
                }
                if (end == -1 && choiceAmount >= start)
                {
                    return match.Groups["format"].Value;
                }

                if (choiceAmount >= start && choiceAmount <= end)
                {
                    return match.Groups["format"].Value;
                }

                i++;
            }

            if (choices.Length == 2 && choiceAmount == 0)
            {
                return choices[0];
            }
            if (choices.Length > 2 && choiceAmount == 1)
            {
                return choices[1];
            }

            if (choiceAmount > choices.Length)
            {
                choiceAmount = choices.Length - 1;
            }

            return choices[choiceAmount];
        }

        /// <summary>
        /// Formats a string using specified object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="format"></param>
        /// <param name="object"></param>
        /// <param name="choiceAmount"></param>
        /// <returns></returns>
        public string Format<T>(string format, T @object, int choiceAmount = 0)
        {
            if (@object == null)
            {
                return string.Empty;
            }

            if (format.IndexOf('|') > -1)
            {
                string[] choices = _choiceRegex.Split(format);
                if (choices.Length > 1)
                {
                    format = GetFormatChoice(choices, choiceAmount);
                }
            }

            if (@object is IDictionary dictionary)
            {
                return FormatDictionary(format, dictionary);
            }

            Dictionary<string, object> values = DictionaryPool.Get();

            try
            {
                PopulateValues(@object, values);

                return FormatDictionary(format, values);
            }
            finally
            {
                DictionaryPool.Free(values);
            }
        }

        /// <summary>
        /// Populate specified dictionary with available values for specified object
        /// </summary>
        /// <param name="object"></param>
        /// <param name="values"></param>
        /// <param name="namespace"></param>
        private void PopulateValues(object @object, Dictionary<string, object> values, string @namespace = "")
        {
            StringObjectContract contract = StringContractResolver.ResolveContract(@object.GetType());

            if (contract == null)
            {
                Interface.uMod.LogWarning($"Unable to resolve contract for {@object.GetType().Name}");
                return;
            }

            if (contract.Properties != null)
            {
                foreach (KeyValuePair<string, PropertyInfo> kvp in contract.Properties)
                {
                    object val;
                    values.Add(@namespace + kvp.Key, val = kvp.Value.GetValue(@object, null));

                    if (val != null && val.GetType().IsClass)
                    {
                        PopulateValues(val, values, kvp.Key + ".");
                    }
                }
            }

            if (contract.Fields != null)
            {
                foreach (KeyValuePair<string, FieldInfo> kvp in contract.Fields)
                {
                    object val;
                    if (!values.ContainsKey(@namespace + kvp.Key))
                    {
                        values.Add(@namespace + kvp.Key, val = kvp.Value.GetValue(@object));
                    }
                    else
                    {
                        values[@namespace + kvp.Key] = val = kvp.Value.GetValue(@object);
                    }

                    if (val != null && val.GetType().IsClass)
                    {
                        PopulateValues(val, values, kvp.Key + ".");
                    }
                }
            }
        }

        /// <summary>
        /// Replace placeholder from dictionary
        /// </summary>
        /// <param name="stringBuilder"></param>
        /// <param name="placeholder"></param>
        /// <param name="values"></param>
        private void ReplacePlaceholder(StringBuilder stringBuilder, TextPlaceholder placeholder, IDictionary values)
        {
            if (values?.Contains(placeholder.Name) ?? false)
            {
                ReplacePlaceholder(stringBuilder, placeholder, values[placeholder.Name]);
            }
        }

        /// <summary>
        /// Replace placeholder using specified object
        /// </summary>
        /// <param name="stringBuilder"></param>
        /// <param name="placeholder"></param>
        /// <param name="value"></param>
        private void ReplacePlaceholder(StringBuilder stringBuilder, TextPlaceholder placeholder, object value)
        {
            if (placeholder.HasFormat)
            {
                if (value is IFormattable formattableValue)
                {
                    stringBuilder.Replace(
                        placeholder.FullString,
                        formattableValue.ToString(placeholder.Format, CultureInfo.InvariantCulture));
                }
                else
                {
                    throw new ArgumentException($"Source string contains format for placeholder '{placeholder.Name}' but value does not implement {nameof(IFormattable)}");
                }
            }
            else if(value != null)
            {
                stringBuilder.Replace(placeholder.FullString, value.ToString());
            }
        }
    }
}
