﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace uMod.Text
{
    /// <summary>
    /// Represents a string contract resolver
    /// </summary>
    internal class StringContractResolver
    {
        /// <summary>
        /// Cache of string contracts
        /// </summary>
        internal static Cache<Type, StringObjectContract> CachedContracts = new Cache<Type, StringObjectContract>();

        /// <summary>
        /// Gets a TomlObjectContract for the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static StringObjectContract ResolveContract(Type type)
        {
            if (CachedContracts.TryGetValue(type, out StringObjectContract contract))
            {
                return contract;
            }

            contract = GetContract(type);

            CachedContracts.Add(type, contract);

            return contract;
        }

        /// <summary>
        /// Gets a dictionary of fields and properties to be serialized
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static StringObjectContract GetContract(Type type)
        {
            StringObjectContract contract = new StringObjectContract(type);
            Dictionary<string, PropertyInfo> properties = new Dictionary<string, PropertyInfo>();
            Dictionary<string, FieldInfo> fields = new Dictionary<string, FieldInfo>();

            FieldInfo[] allFields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            PropertyInfo[] allProperties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            List<PropertyInfo> filteredProperties = new List<PropertyInfo>();
            List<FieldInfo> filteredFields = new List<FieldInfo>();
            filteredFields.AddRange(allFields.Where(FilterField));
            filteredProperties.AddRange(allProperties.Where(FilterProperty));

            FieldInfo[] defaultFields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] defaultProperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            List<MemberInfo> defaultMembers = new List<MemberInfo>();
            defaultMembers.AddRange(defaultFields.Where(FilterField).Cast<MemberInfo>());
            defaultMembers.AddRange(defaultProperties.Where(FilterProperty).Cast<MemberInfo>());
            PlaceholderAttribute propertyAttribute = null;
            foreach (PropertyInfo member in filteredProperties)
            {
                if (defaultMembers.Contains(member))
                {
                    if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                    {
                        properties.Add(propertyAttribute.Name ?? member.Name, member);
                    }
                    else
                    {
                        properties.Add(member.Name, member);
                    }
                }
                else
                {
                    if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                    {
                        properties.Add(propertyAttribute.Name ?? member.Name, member);
                    }
                }
            }

            foreach (FieldInfo member in filteredFields)
            {
                if (defaultMembers.Contains(member))
                {
                    if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                    {
                        fields.Add(propertyAttribute.Name ?? member.Name, member);
                    }
                    else
                    {
                        fields.Add(member.Name, member);
                    }
                }
                else
                {
                    if ((propertyAttribute = member.GetCustomAttribute<PlaceholderAttribute>()) != null)
                    {
                        fields.Add(propertyAttribute.Name ?? member.Name, member);
                    }
                }
            }

            contract.Properties = properties;
            contract.Fields = fields;

            return contract;
        }

        /// <summary>
        /// Field filter
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private static bool FilterField(FieldInfo field)
        {
            return !field.IsDefined(typeof(CompilerGeneratedAttribute), true) && field.GetCustomAttribute<PlaceholderIgnoreAttribute>() == null;
        }

        /// <summary>
        /// Property filter
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private static bool FilterProperty(PropertyInfo property)
        {
            return property.GetIndexParameters().Length == 0 && !property.IsDefined(typeof(CompilerGeneratedAttribute), true) && property.GetCustomAttribute<PlaceholderIgnoreAttribute>() == null;
        }
    }
}
