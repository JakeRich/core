﻿using System;
using System.Collections.Generic;
using System.Text;
using uMod.Common;
using uMod.Common.Command;
using uMod.Plugins;
using uMod.Pooling;

namespace uMod.Command
{
    /// <summary>
    /// Represents a generic chat command handler
    /// </summary>
    public sealed class CommandHandler : ICommandHandler
    {
        /// <summary>
        /// Gets or sets the game-specific command callback
        /// </summary>
        public CommandCallback Callback { get; set; }

        /// <summary>
        /// Gets or sets the universal command filter
        /// </summary>
        public Func<string, bool> CommandFilter { get; set; }

        /// <summary>
        /// Gets plugin provider
        /// </summary>
        private readonly IPluginProvider _provider;

        private HookName OnUserCommand = HookName.Cache.Get("OnUserCommand");
        private HookName OnPlayerCommand = HookName.Cache.Get("OnPlayerCommand");

        /// <summary>
        /// Cached OnUserCommand expiration time
        /// TODO: Remove this on 04-01-2020
        /// </summary>
        private static readonly DateTime OnUserCommandExpireTime = new DateTime(2020, 04, 01);

        /// <summary>
        /// Create a new instance of the CommandHandler class
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="callback"></param>
        /// <param name="commandFilter"></param>
        public CommandHandler(PluginProvider provider, CommandCallback callback, Func<string, bool> commandFilter)
        {
            _provider = provider;
            Callback = callback;
            CommandFilter = commandFilter;
        }

        /// <summary>
        /// Create a new instance of the CommandHandler class
        /// </summary>
        /// <param name="provider"></param>
        public CommandHandler(IPluginProvider provider)
        {
            _provider = provider;
        }

        /// <summary>
        /// Handles a chat command from the specified player
        /// </summary>
        /// <param name="player"></param>
        /// <param name="command"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public CommandState HandleChatMessage(IPlayer player, string command, object[] context = null)
        {
            // Make sure the command is not empty
            if (command.Length == 0)
            {
                return CommandState.Unrecognized;
            }

            // Is it a chat command?
            if (!_provider.Configuration.Commands.ChatCommandPrefixes.Contains(command[0]))
            {
                return CommandState.Unrecognized;
            }

            // Get the command
            command = command.Substring(1);

            // Set command type for the player
            player.LastCommand = CommandType.Chat;

            // Handle the command
            return HandleCommand(player, command, context);
        }

        /// <summary>
        /// Handle console input from the specified player
        /// </summary>
        /// <param name="player"></param>
        /// <param name="command"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public CommandState HandleConsoleMessage(IPlayer player, string command, object[] context = null)
        {
            // Handle global parent for console commands
            if (command.StartsWith("global."))
            {
                command = command.Substring(7);
            }

            // Set command type for the player
            player.LastCommand = CommandType.Console;

            // Handle the command
            return HandleCommand(player, command, context);
        }

        /// <summary>
        /// Handles a command
        /// </summary>
        /// <param name="player"></param>
        /// <param name="fullCommand"></param>
        /// <param name="context"></param>
        private CommandState HandleCommand(IPlayer player, string fullCommand, object[] context = null)
        {
            ParseCommand(fullCommand, out string command, out string[] args);
            if (command == null)
            {
                return CommandState.Unrecognized;
            }

            object commandAllowed = _provider.Manager.CallHook(OnPlayerCommand.ToString(), player, command, args);
            if (commandAllowed != null)
            {
                return CommandState.Canceled;
            }

            object commandAllowedDeprecated = _provider.Manager.CallDeprecatedHook(OnUserCommand.ToString(), OnPlayerCommand.ToString(),
                OnUserCommandExpireTime, player, command, args);

            if (commandAllowedDeprecated != null)
            {
                return CommandState.Canceled;
            }

            if ((CommandFilter != null && !CommandFilter(command)) || Callback == null)
            {
                return CommandState.Unrecognized;
            }

            // Handle the command
            return Callback(player, command, fullCommand, context);
        }

        /// <summary>
        /// Parses the specified command
        /// </summary>
        /// <param name="argstr"></param>
        /// <param name="command"></param>
        /// <param name="args"></param>
        private void ParseCommand(string argstr, out string command, out string[] args)
        {
            List<string> arglist = Pools.GetList<string>();
            StringBuilder stringBuilder = Pools.StringBuilders.Get();
            bool inlongarg = false;

            try
            {
                foreach (char c in argstr)
                {
                    if (c == '"')
                    {
                        if (inlongarg)
                        {
                            string arg = stringBuilder.ToString().Trim();
                            if (!string.IsNullOrEmpty(arg))
                            {
                                arglist.Add(arg);
                            }

                            stringBuilder.Length = 0;
                            inlongarg = false;
                        }
                        else
                        {
                            inlongarg = true;
                        }
                    }
                    else if (char.IsWhiteSpace(c) && !inlongarg)
                    {
                        string arg = stringBuilder.ToString().Trim();
                        if (!string.IsNullOrEmpty(arg))
                        {
                            arglist.Add(arg);
                        }

                        stringBuilder.Length = 0;
                    }
                    else
                    {
                        stringBuilder.Append(c);
                    }
                }

                if (stringBuilder.Length > 0)
                {
                    string arg = stringBuilder.ToString().Trim();
                    if (!string.IsNullOrEmpty(arg))
                    {
                        arglist.Add(arg);
                    }
                }

                if (arglist.Count == 0)
                {
                    command = null;
                    args = null;
                    return;
                }

                command = arglist[0].ToLower();
                arglist.RemoveAt(0);
                args = arglist.ToArray();
            }
            finally
            {
                Pools.FreeList(ref arglist);
                Pools.StringBuilders.Free(stringBuilder);
            }
        }
    }
}
