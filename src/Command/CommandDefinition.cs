﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using uMod.Common.Command;

namespace uMod.Command
{
    /// <summary>
    /// Represents a command definition
    /// </summary>
    public class CommandDefinition : ICommandDefinition
    {
        /// <summary>
        /// Original definition
        /// </summary>
        private readonly string _src;

        /// <summary>
        /// Command names
        /// </summary>
        public string[] Names { get; set; }

        /// <summary>
        /// Command primary name
        /// </summary>
        public string Name => Names[0];

        /// <summary>
        /// Command aliases
        /// </summary>
        public IEnumerable<string> Aliases => Names.Skip(1);

        /// <summary>
        /// Command arguments
        /// </summary>
        public InputArgument[] Arguments { get; set; }

        /// <summary>
        /// Regular expression used to parse arguments
        /// </summary>
        private static readonly Regex ArgumentGroupRegex =
            new Regex(@"{(?<name>(?:[\w.:;\-_\/\\ \=\?\*])*)?}",
                RegexOptions.Compiled | RegexOptions.CultureInvariant);

        /// <summary>
        /// Create a new command definition object
        /// </summary>
        /// <param name="definition"></param>
        public CommandDefinition(string definition)
        {
            _src = definition;
            Resolve(_src);
        }

        public CommandDefinition(IEnumerable<string> commands, string definition = null)
        {
            string command = string.Join("|", commands.ToArray());
            if (!string.IsNullOrEmpty(definition))
            {
                _src = $"{command} {definition}";
            }
            else
            {
                _src = command;
            }

            Resolve(_src);
        }

        public CommandDefinition(List<string> commands, string definition)
        {
            string command = string.Join("|", commands.ToArray());
            _src = $"{command} {definition}";
            Resolve(_src);
        }

        /// <summary>
        /// Resolve definition arguments
        /// </summary>
        /// <param name="definition"></param>
        private void Resolve(string definition)
        {
            if (string.IsNullOrEmpty(definition))
            {
                return;
            }

            string commandName = string.Empty;
            definition = definition.Trim();
            int firstWhitespace = definition.IndexOf(' ');

            commandName = firstWhitespace > -1 ? definition.Substring(0, firstWhitespace).ToLowerInvariant().Trim() : definition.ToLowerInvariant().Trim();

            Names = commandName.IndexOf('|') > -1 ? commandName.Split('|') : new[] { commandName };

            if (firstWhitespace == -1)
            {
                return;
            }

            MatchCollection matches = ArgumentGroupRegex.Matches(definition);

            InputArgument[] arguments = new InputArgument[matches.Count];

            for (int i = 0; i < matches.Count; i++)
            {
                Match match = matches[i];
                string name = match.Groups["name"].Value;

                InputArgument argument;
                if (name.EndsWith("?*"))
                {
                    argument = new InputArgument(name.TrimEnd('?', '*'), InputArgumentType.Array);
                }
                else if (name.EndsWith("*"))
                {
                    argument = new InputArgument(name.TrimEnd('*'), InputArgumentType.Array | InputArgumentType.Required);
                }
                else if (name.EndsWith("?"))
                {
                    argument = new InputArgument(name.TrimEnd('?'));
                }
                else if (name.IndexOf('=') > -1)
                {
                    string[] parts = name.Split('=');

                    if (parts[1].StartsWith("*"))
                    {
                        parts[1] = parts[1].TrimStart('*');
                        argument = new InputArgument(parts[0], InputArgumentType.Array, null, parts[1].Split(','));
                    }
                    else
                    {
                        argument = new InputArgument(parts[0], InputArgumentType.Optional, null, parts[1]);
                    }
                }
                else
                {
                    argument = new InputArgument(name, InputArgumentType.Required);
                }

                arguments[i] = argument;
            }

            Arguments = arguments;
        }

        /// <summary>
        /// Format definition as a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _src;
        }
    }
}
