﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uMod.Common;
using uMod.Common.Command;
using uMod.Pooling;

namespace uMod.Command
{
    /// <summary>
    /// Represents a parsed command
    /// </summary>
    public class Args : IArgs
    {
        private static readonly string[] FalseEquivalents = {
            "0",
            "false",
            "no",
            "none",
            "null",
            "n"
        };

        private static readonly string[] TrueEquivalents = {
            "1",
            "true",
            "yes",
            "y"
        };

        private string _src;

        /// <summary>
        /// Gets the verb
        /// </summary>
        public string Command { get; private set; }

        /// <summary>
        /// All arguments
        /// </summary>
        private Dictionary<string, CommandArgument> _arguments;

        /// <summary>
        /// Get all arguments, named and arbitrary
        /// </summary>
        public IEnumerable<CommandArgument> Arguments => _arguments?.Values ?? Enumerable.Empty<CommandArgument>();

        /// <summary>
        /// Gets context objects
        /// </summary>
        public object[] ContextObjects { get; private set; }

        /// <summary>
        /// Gets all argument keys
        /// </summary>
        public IEnumerable<string> Keys => _arguments?.Keys;

        /// <summary>
        /// Gets all argument values
        /// </summary>
        public IEnumerable<object> Values => _arguments?.Values.Select(x => x.Value);

        private string[] _stringValues;

        /// <summary>
        /// Gets an array of argument values as string
        /// </summary>
        /// <returns></returns>
        internal string[] GetStringValues()
        {
            return _stringValues ?? (_stringValues = _arguments?.Values.Select(x => x.Value.ToString()).ToArray());
        }

        /// <summary>
        /// Gets length of arguments
        /// </summary>
        public int Length => _arguments?.Count ?? 0;

        private ICommandDefinition definition;

        /// <summary>
        /// Gets the command definition
        /// </summary>
        public ICommandDefinition Definition { get => definition; private set => definition = value; }

        /// <summary>
        /// Get Player who ran command
        /// </summary>
        public IPlayer Player { get; private set; }

        /// <summary>
        /// Determine if command is from server
        /// </summary>
        public bool IsServer => Player?.IsServer ?? false;

        /// <summary>
        /// Determine if command is from administrator
        /// </summary>
        public bool IsAdmin => Player?.IsAdmin ?? false;

        /// <summary>
        /// Determine if command is from moderator
        /// </summary>
        public bool IsModerator => Player?.IsModerator ?? false;

        /// <summary>
        /// Determine if required arguments are provided
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// Gets or sets an argument
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public object this[string name]
        {
            get => _arguments[name].Value;
            set => _arguments[name] = new CommandArgument(name, value);
        }

        /// <summary>
        /// Gets or sets an argument
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string this[int key] => _arguments.ElementAt(key).Value.Value.ToString();

        /// <summary>
        /// Create a new empty args object
        /// </summary>
        public Args()
        {
        }

        /// <summary>
        /// Create a new args object
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="player"></param>
        /// <param name="command"></param>
        /// <param name="context"></param>
        public Args(ICommandDefinition definition, IPlayer player, string command, object[] context = null)
        {
            Definition = definition;
            Player = player;
            Parse(command);
            ContextObjects = context;
        }

        /// <summary>
        /// Reset Args after pooling
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="player"></param>
        /// <param name="command"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        [Obsolete]
        public IArgs Reset(ICommandDefinition definition, IPlayer player, string command, object[] context = null)
        {
            return this;
        }

        /// <summary>
        /// Reset Args after pooling
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="player"></param>
        /// <param name="command"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal Args ResetArgs(ICommandDefinition definition, IPlayer player, string command, object[] context = null)
        {
            this.definition = definition;
            _stringValues = null;
            Player = player;
            Parse(command);
            ContextObjects = context;
            return this;
        }

        /// <summary>
        /// Dispose of Arg
        /// </summary>
        public void Dispose()
        {
            if (_arguments != null)
            {
                Pools.FreeDictionary(ref _arguments);
            }
            Player = null;
            Command = string.Empty;
            Definition = null;
            _src = null;
            _arguments?.Clear();
            IsValid = false;
        }

        public IEnumerator<object> GetEnumerator()
        {
            return Arguments.Select(x => x.Value).GetEnumerator();
        }

        /// <summary>
        /// Transform Args object to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _src;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #region Context Handling

        /// <summary>
        /// Try to get a context object of the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetContext(Type type, out object value)
        {
            value = null;
            if (ContextObjects == null)
            {
                return false;
            }

            foreach (object obj in ContextObjects)
            {
                if (obj.GetType() != type)
                {
                    continue;
                }

                value = obj;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to get a context object of the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetContext<T>(Type type, out T value)
        {
            value = default;
            if (ContextObjects == null)
            {
                return false;
            }

            foreach (object obj in ContextObjects)
            {
                if (obj.GetType() != type)
                {
                    continue;
                }

                value = (T)obj;
                return true;
            }

            return false;
        }

        #endregion Context Handling

        #region Argument Handling

        /// <summary>
        /// Determine if argument exists in command
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool HasArgument(string name)
        {
            return _arguments.ContainsKey(name);
        }

        /// <summary>
        /// Determine if argument is of type
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsArgument(string name, Type type)
        {
            if (!_arguments.ContainsKey(name))
            {
                return false;
            }

            if (type.IsArray)
            {
                return TryGetArgument(name, out object obj) && obj != null && type.IsInstanceOfType(obj);
            }

            string @string = GetString(name);
            if (@string == null)
            {
                return false;
            }

            if (type == typeof(string))
            {
                return true;
            }

            if (type == typeof(bool))
            {
                @string = @string.ToLower();

                if (FalseEquivalents.Contains(@string))
                {
                    return true;
                }

                if (TrueEquivalents.Contains(@string))
                {
                    return true;
                }

                return bool.TryParse(@string, out _);
            }

            if (type == typeof(float))
            {
                return float.TryParse(@string, out _);
            }

            if (type == typeof(double))
            {
                return double.TryParse(@string, out _);
            }

            if (type == typeof(decimal))
            {
                return decimal.TryParse(@string, out _);
            }

            if (type == typeof(short))
            {
                return short.TryParse(@string, out _);
            }

            if (type == typeof(ushort))
            {
                return ushort.TryParse(@string, out _);
            }

            if (type == typeof(byte))
            {
                return byte.TryParse(@string, out _);
            }

            if (type == typeof(sbyte))
            {
                return sbyte.TryParse(@string, out _);
            }

            if (type == typeof(int))
            {
                return int.TryParse(@string, out _);
            }

            if (type == typeof(uint))
            {
                return uint.TryParse(@string, out _);
            }

            if (type == typeof(long))
            {
                return long.TryParse(@string, out _);
            }

            if (type == typeof(ulong))
            {
                return ulong.TryParse(@string, out _);
            }

            if (type == typeof(Point))
            {
                return Point.TryParse(@string, out _);
            }

            if (type == typeof(Position))
            {
                return Position.TryParse(@string, out _);
            }

            if (type == typeof(Position4))
            {
                return Position4.TryParse(@string, out _);
            }

            if (type == typeof(TimeSpan))
            {
                return TimeSpan.TryParse(@string, out _);
            }

            if (type == typeof(DateTime))
            {
                return DateTime.TryParse(@string, out _);
            }

            return false;
        }

        /// <summary>
        /// Determine if argument is of type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsArgument<T>(string name)
        {
            return IsArgument(name, typeof(T));
        }

        /// <summary>
        /// Determine if argument is default
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsDefault(string name)
        {
            if (_arguments.TryGetValue(name, out CommandArgument result))
            {
                InputArgument argument = definition.Arguments.FirstOrDefault(x => x.Name == name);
                if (!argument.Equals(default(InputArgument)))
                {
                    return result.Value == argument.Default;
                }
            }

            return false;
        }

        /// <summary>
        /// Try to get an argument by name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetArgument(string name, out object value)
        {
            if (_arguments.TryGetValue(name, out CommandArgument result))
            {
                value = result.Value;
                return true;
            }

            value = null;
            return false;
        }

        /// <summary>
        /// Try to get an argument by name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetArgument<T>(string name, out T value)
        {
            if (_arguments.ContainsKey(name))
            {
                value = GetArgument<T>(name);
                return true;
            }

            value = default;
            return false;
        }

        /// <summary>
        /// Gets the specified case-insensitive named argument
        /// </summary>
        /// <param name="name"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        public object GetArgument(string name, object def = null)
        {
            return _arguments.TryGetValue(name, out CommandArgument result) ? result.Value : def;
        }

        /// <summary>
        /// Gets the specified case-insensitive named argument
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public T GetArgument<T>(string name, T @default = default)
        {
            if (typeof(T) == typeof(string))
            {
                return ConvertArgument<T, string>(GetString, name, @default);
            }

            if (typeof(T) == typeof(bool))
            {
                return ConvertArgument<T, bool>(GetBoolean, name, @default);
            }

            if (typeof(T) == typeof(float))
            {
                return ConvertArgument<T, float>(GetFloat, name, @default);
            }

            if (typeof(T) == typeof(double))
            {
                return ConvertArgument<T, double>(GetDouble, name, @default);
            }

            if (typeof(T) == typeof(decimal))
            {
                return ConvertArgument<T, decimal>(GetDecimal, name, @default);
            }

            if (typeof(T) == typeof(short))
            {
                return ConvertArgument<T, short>(GetShort, name, @default);
            }

            if (typeof(T) == typeof(ushort))
            {
                return ConvertArgument<T, ushort>(GetUShort, name, @default);
            }

            if (typeof(T) == typeof(byte))
            {
                return ConvertArgument<T, byte>(GetByte, name, @default);
            }

            if (typeof(T) == typeof(sbyte))
            {
                return ConvertArgument<T, sbyte>(GetSByte, name, @default);
            }

            if (typeof(T) == typeof(int))
            {
                return ConvertArgument<T, int>(GetInteger, name, @default);
            }

            if (typeof(T) == typeof(uint))
            {
                return ConvertArgument<T, uint>(GetUInt, name, @default);
            }

            if (typeof(T) == typeof(long))
            {
                return ConvertArgument<T, long>(GetInt64, name, @default);
            }

            if (typeof(T) == typeof(ulong))
            {
                return ConvertArgument<T, ulong>(GetUInt64, name, @default);
            }

            if (typeof(T) == typeof(Point))
            {
                return ConvertArgument<T, Point>(GetPoint, name, @default);
            }

            if (typeof(T) == typeof(Position))
            {
                return ConvertArgument<T, Position>(GetPosition, name, @default);
            }

            if (typeof(T) == typeof(Position4))
            {
                return ConvertArgument<T, Position4>(GetPosition4, name, @default);
            }

            if (typeof(T) == typeof(TimeSpan))
            {
                return ConvertArgument(GetTimeSpan, name, @default);
            }

            if (typeof(T) == typeof(DateTime))
            {
                return ConvertArgument(GetDateTime, name, @default);
            }

            return _arguments.TryGetValue(name, out CommandArgument result) ? (T)result.Value : @default;
        }

        /// <summary>
        /// Convert specified argument using specified delegate with specified default
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="delegateFunc"></param>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        private T ConvertArgument<T, T1>(Func<string, T1, T1> delegateFunc, string name, T @default = default)
        {
            return (T)Convert.ChangeType(delegateFunc(name, (T1)Convert.ChangeType(@default, typeof(T1))), typeof(T));
        }

        /// <summary>
        /// Convert specified argument using specified delegate with specified default
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="delegateFunc"></param>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        private T ConvertArgument<T, T1>(Func<string, T1> delegateFunc, string name, T @default = default)
        {
            return (T)Convert.ChangeType(delegateFunc(name), typeof(T));
        }

        /// <summary>
        /// Get argument as string by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public string GetString(string name, string @default = default)
        {
            return _arguments.TryGetValue(name, out CommandArgument result)
                ? result.Value?.ToString() ?? @default
                : @default;
        }

        /// <summary>
        /// Get argument as boolean by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public bool GetBool(string name, bool @default = default)
        {
            return GetBoolean(name, @default);
        }

        /// <summary>
        /// Get argument as boolean by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public bool GetBoolean(string name, bool @default = default)
        {
            string @string = GetString(name);
            if (@string == null)
            {
                return @default;
            }

            @string = @string.ToLower();

            if (FalseEquivalents.Contains(@string))
            {
                return false;
            }

            if (TrueEquivalents.Contains(@string))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(@string))
            {
                return true;
            }

            return @default;
        }

        /// <summary>
        /// Get argument as float by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public float GetFloat(string name, float @default = 0f)
        {
            string @string = GetString(name);
            if (@string == null || !float.TryParse(@string, out float result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as double by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public double GetDouble(string name, double @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !double.TryParse(@string, out double result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as decimal by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public decimal GetDecimal(string name, decimal @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !decimal.TryParse(@string, out decimal result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as short by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public short GetShort(string name, short @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !short.TryParse(@string, out short result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as unsigned short by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public ushort GetUShort(string name, ushort @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !ushort.TryParse(@string, out ushort result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as byte by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public byte GetByte(string name, byte @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !byte.TryParse(@string, out byte result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as signed byte by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public sbyte GetSByte(string name, sbyte @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !sbyte.TryParse(@string, out sbyte result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as integer by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public int GetInteger(string name, int @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !int.TryParse(@string, out int result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as integer by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public int GetInt(string name, int @default = 0)
        {
            return GetInteger(name, @default);
        }

        /// <summary>
        /// Get argument as unsigned integer by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public uint GetUInt(string name, uint @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !uint.TryParse(@string, out uint result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as long by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public long GetInt64(string name, long @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !long.TryParse(@string, out long result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as long by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public long GetLong(string name, long @default = 0)
        {
            return GetInt64(name, @default);
        }

        /// <summary>
        /// Get argument as unsigned long by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public ulong GetUInt64(string name, ulong @default = 0)
        {
            string @string = GetString(name);
            if (@string == null || !ulong.TryParse(@string, out ulong result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as unsigned long by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public ulong GetULong(string name, ulong @default = 0)
        {
            return GetUInt64(name, @default);
        }

        /// <summary>
        /// Get argument as timespan by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public TimeSpan GetTimeSpan(string name)
        {
            string @string = GetString(name);
            if (@string == null || !TimeSpan.TryParse(@string, out TimeSpan result))
            {
                return TimeSpan.FromSeconds(0);
            }

            return result;
        }

        /// <summary>
        /// Get argument as datetime by the specified key
        /// Default is returned when specified key is unavailable
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DateTime GetDateTime(string name)
        {
            string @string = GetString(name);
            if (@string == null || !DateTime.TryParse(@string, out DateTime result))
            {
                return DateTime.MinValue;
            }

            return result;
        }

        /// <summary>
        /// Get argument as position by the specified key
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public Position GetPosition(string name, Position @default = null)
        {
            string @string = GetString(name);
            if (@string == null || !Position.TryParse(@string, out Position result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as position4 by the specified key
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public Position4 GetPosition4(string name, Position4 @default = null)
        {
            string @string = GetString(name);
            if (@string == null || !Position4.TryParse(@string, out Position4 result))
            {
                return @default;
            }

            return result;
        }

        /// <summary>
        /// Get argument as point by the specified key
        /// </summary>
        /// <param name="name"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public Point GetPoint(string name, Point @default = null)
        {
            string @string = GetString(name);
            if (@string == null || !Point.TryParse(@string, out Point result))
            {
                return @default;
            }

            return result;
        }

        #endregion Argument Handling

        #region Messaging Shortcuts

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message)
        {
            Player?.Reply(message);
        }

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args)
        {
            Player?.Reply(message, prefix, args);
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message)
        {
            Player?.Message(message);
        }

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            Player?.Message(message, prefix, args);
        }

        #endregion Messaging Shortcuts

        #region Parse

        /// <summary>
        /// Represents tokens within command definition
        /// </summary>
        private enum ParseState
        {
            None,
            EscapedBasicArg,
            EscapedBasicArgEsc,
            Identifier,
            IdentifierEsc,
            IdentifierPeek
        }

        /// <summary>
        /// Parses the specified string into this command
        /// </summary>
        /// <param name="src"></param>
        private void Parse(string src)
        {
            if (string.IsNullOrEmpty(src))
            {
                return;
            }

            _src = src;
            // Iterate until we hit a whitespace
            char c;
            int i = 0;
            while (i < src.Length && !char.IsWhiteSpace(c = src[i]))
            {
                i++;
            }

            // Found the verb
            Command = src.Substring(0, i).ToLowerInvariant();

            // Setup temporary objects
            Dictionary<string, CommandArgument> commandArguments = Pools.GetDictionary<string, CommandArgument>();

            // Crude FSM
            ParseState curState = ParseState.None;
            StringBuilder sb = null;
            string namedArgKey = null;
            bool isLastArgumentArray = false;
            List<string> lastArgumentParams = null;
            string lastArgumentKey = null;

            while (i <= src.Length)
            {
                if (i == src.Length)
                {
                    if (curState == ParseState.EscapedBasicArg || curState == ParseState.IdentifierPeek)
                    {
                        break;
                    }

                    c = ' ';
                }
                else
                {
                    c = src[i];
                }

                i++;
                switch (curState)
                {
                    case ParseState.None:
                        if (!char.IsWhiteSpace(c))
                        {
                            switch (c)
                            {
                                case '"':
                                    curState = ParseState.EscapedBasicArg;
                                    if (sb != null)
                                    {
                                        Pools.StringBuilders.Free(sb);
                                    }

                                    sb = Pools.StringBuilders.Get();
                                    break;

                                default:
                                    curState = ParseState.Identifier;
                                    if (sb != null)
                                    {
                                        Pools.StringBuilders.Free(sb);
                                    }

                                    sb = Pools.StringBuilders.Get();
                                    sb.Append(c);
                                    break;
                            }
                        }

                        break;

                    case ParseState.EscapedBasicArg:
                        if (c == '"')
                        {
                            curState = ParseState.None;
                            if (namedArgKey != null)
                            {
                                string arg = sb.ToString();
                                commandArguments.Add(namedArgKey, new CommandArgument(namedArgKey, arg));
                                namedArgKey = null;
                            }
                            else
                            {
                                string arg = sb.ToString();
                                string key = string.Empty;
                                if (definition.Arguments != null && definition.Arguments.Length >= commandArguments.Count)
                                {
                                    key = definition.Arguments[commandArguments.Count].Name;
                                }
                                else
                                {
                                    key = commandArguments.Count.ToString();
                                }

                                commandArguments.Add(key, new CommandArgument(key, arg));
                            }

                            if (sb != null)
                            {
                                Pools.StringBuilders.Free(sb);
                                sb = null;
                            }
                        }
                        else if (c == '\\')
                        {
                            curState = ParseState.EscapedBasicArgEsc;
                        }
                        else
                        {
                            sb.Append(c);
                        }

                        break;

                    case ParseState.EscapedBasicArgEsc:
                        sb.Append(c);
                        curState = ParseState.EscapedBasicArg;
                        break;

                    case ParseState.Identifier:
                        if ((c == ':' || c == '=') && namedArgKey == null)
                        {
                            namedArgKey = sb.ToString();
                            if (sb != null)
                            {
                                Pools.StringBuilders.Free(sb);
                            }

                            sb = Pools.StringBuilders.Get();
                            curState = ParseState.IdentifierPeek;
                        }
                        else if (c == '\\')
                        {
                            curState = ParseState.IdentifierEsc;
                        }
                        else if (char.IsWhiteSpace(c))
                        {
                            curState = ParseState.None;
                            if (namedArgKey != null)
                            {
                                string arg = sb.ToString();
                                InputArgument inputArgument = definition.Arguments
                                    .FirstOrDefault(x => string.Equals(x.Name, namedArgKey, StringComparison.InvariantCultureIgnoreCase));

                                if (!inputArgument.Equals(default(InputArgument)) && inputArgument.IsArray)
                                {
                                    isLastArgumentArray = true;
                                }

                                if (isLastArgumentArray)
                                {
                                    lastArgumentKey = namedArgKey;
                                    if (lastArgumentParams == null)
                                    {
                                        lastArgumentParams = new List<string>();
                                    }

                                    lastArgumentParams.Add(arg);
                                }
                                else
                                {
                                    commandArguments.Add(namedArgKey, new CommandArgument(namedArgKey, arg));
                                }
                                namedArgKey = null;
                            }
                            else
                            {
                                string arg = sb.ToString();
                                string key = null;
                                if (!isLastArgumentArray)
                                {
                                    if (definition.Arguments == null ||
                                        definition.Arguments?.Length < commandArguments.Count)
                                    {
                                        key = commandArguments.Count.ToString();
                                    }
                                    else
                                    {
                                        InputArgument inputArgument = definition.Arguments[commandArguments.Count];
                                        key = inputArgument.Name;
                                        if (inputArgument.IsArray)
                                        {
                                            isLastArgumentArray = true;
                                        }
                                    }
                                }

                                if (isLastArgumentArray)
                                {
                                    if (key != null)
                                    {
                                        lastArgumentKey = key;
                                    }

                                    if (lastArgumentParams == null)
                                    {
                                        lastArgumentParams = new List<string>();
                                    }

                                    lastArgumentParams.Add(arg);
                                }
                                else
                                {
                                    commandArguments.Add(key, new CommandArgument(key, arg));
                                }
                            }

                            if (sb != null)
                            {
                                Pools.StringBuilders.Free(sb);
                                sb = null;
                            }
                        }
                        else
                        {
                            sb.Append(c);
                        }

                        break;

                    case ParseState.IdentifierEsc:
                        sb.Append($"\\{c}");
                        curState = ParseState.Identifier;
                        break;

                    case ParseState.IdentifierPeek:
                        if (c == '"')
                        {
                            curState = ParseState.EscapedBasicArg;
                        }
                        else
                        {
                            curState = ParseState.Identifier;
                            sb.Append(c);
                        }

                        break;

                    default:
                        sb.Append(c);
                        break;
                }
            }

            if (curState == ParseState.Identifier || curState == ParseState.EscapedBasicArg)
            {
                string arg = sb.ToString();
                string key = null;
                if (!isLastArgumentArray)
                {
                    if (definition.Arguments == null ||
                        definition.Arguments?.Length < commandArguments.Count)
                    {
                        key = commandArguments.Count.ToString();
                    }
                    else
                    {
                        InputArgument inputArgument = definition.Arguments[commandArguments.Count];
                        key = inputArgument.Name;
                        if (inputArgument.IsArray)
                        {
                            isLastArgumentArray = true;
                        }
                    }
                }

                if (isLastArgumentArray)
                {
                    if (key != null)
                    {
                        lastArgumentKey = key;
                    }

                    if (lastArgumentParams == null)
                    {
                        lastArgumentParams = new List<string>();
                    }

                    lastArgumentParams.Add(arg);
                }
                else
                {
                    commandArguments.Add(key, new CommandArgument(key, arg));
                }

                curState = ParseState.None;
            }

            if (curState == ParseState.IdentifierPeek && !string.IsNullOrEmpty(namedArgKey))
            {
                string arg = sb.ToString();
                commandArguments.Add(namedArgKey, new CommandArgument(namedArgKey, arg));
                curState = ParseState.None;
            }

            if (isLastArgumentArray && lastArgumentParams != null && !string.IsNullOrEmpty(lastArgumentKey))
            {
                commandArguments.Add(lastArgumentKey, new CommandArgument(lastArgumentKey, lastArgumentParams.ToArray()));
            }

            // Add additional unspecified arguments that have default values
            if (definition.Arguments?.Length > commandArguments.Count)
            {
                for (int x = commandArguments.Count; x < definition.Arguments.Length; x++)
                {
                    InputArgument argument = definition.Arguments[x];
                    if (argument.Default != null)
                    {
                        commandArguments.Add(argument.Name, new CommandArgument(argument.Name, argument.Default));
                    }
                }
            }

            // Store basic args
            _arguments = commandArguments;

            IsValid = true;

            // Check if all required arguments are provided
            if (definition.Arguments != null)
            {
                IEnumerable<string> requiredArguments = definition.Arguments.Where(x => x.IsRequired).Select(x => x.Name);

                foreach (string requiredArgument in requiredArguments)
                {
                    if (!_arguments.ContainsKey(requiredArgument))
                    {
                        IsValid = false;
                    }
                }
            }

            if (sb != null)
            {
                Pools.StringBuilders.Free(sb);
            }
        }

        #endregion Parse
    }
}
