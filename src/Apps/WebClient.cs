using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using uMod.Collections;
using uMod.Common;
using uMod.Common.Web;
using uMod.Exceptions;
using uMod.IO;
using uMod.Logging;

namespace uMod.Apps
{
    internal class WebClient : BaseApp
    {
        /// <summary>
        /// Web client process
        /// </summary>
        private Process _process;

        /// <summary>
        /// Determine if client is ready
        /// </summary>
        private bool _ready;

        /// <summary>
        /// ObjectStreamClient
        /// </summary>
        private ObjectStreamClient<WebMessage> _client;

        /// <summary>
        /// List of requests being executed
        /// </summary>
        private readonly Hash<int, Web.Request> _callbacks;

        /// <summary>
        /// Queue of pending messages
        /// </summary>
        private readonly Queue<WebMessage> _messageQueue;

        /// <summary>
        /// Current web client version
        /// </summary>
        public string Version;

        /// <summary>
        /// Last message id
        /// </summary>
        private int _lastMessageId = 1;

        /// <summary>
        /// Application logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Create new WebClient
        /// </summary>
        /// <param name="info"></param>
        /// <param name="logger"></param>
        public WebClient(IAppInfo info, ILogger logger) : base(info)
        {
            _callbacks = new Hash<int, Web.Request>();
            _messageQueue = new Queue<WebMessage>();
            _logger = logger;
        }

        /// <summary>
        /// Set version of web client
        /// </summary>
        private void SetVersion()
        {
            Version = File.Exists(Info.ApplicationPath) ? FileVersionInfo.GetVersionInfo(Info.ApplicationPath).FileVersion : "unknown";
            SentryLogger.SetTag("webclient version", Version);
        }

        /// <summary>
        /// Initialize web client
        /// </summary>
        /// <returns></returns>
        public override bool Initialize()
        {
            MakeActive();
            if (_process != null && _process.Handle != IntPtr.Zero && !_process.HasExited)
            {
                return true;
            }

            SetVersion();
            Shutdown();

            string[] args = {
                $"--logPath=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\""
            };

            try
            {
                _process = new Process
                {
                    StartInfo =
                    {
                        FileName = "dotnet",
                        Arguments =  $"{Info.ApplicationPath} {string.Join(" ", args)}",
                        WorkingDirectory = Interface.uMod.RootDirectory,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                    },
                    EnableRaisingEvents = true,
                };
                _process.Exited += OnProcessExited;
                _process.Start();
            }
            catch (Exception ex)
            {
                _process?.Dispose();
                _process = null;
                _logger.Report(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "WebClient")), ex);
                if (ex.GetBaseException() != ex)
                {
                    _logger.Report("BaseException: ", ex.GetBaseException());
                }
                if (ex is Win32Exception win32)
                {
                    _logger.Error(Interface.uMod.Strings.Exceptions.Win32.Interpolate(win32));
                }
            }

            if (_process == null)
            {
                return false;
            }

            _client = new ObjectStreamClient<WebMessage>(_process.StandardOutput.BaseStream, _process.StandardInput.BaseStream);
            _client.Message += OnMessage;
            _client.Error += OnError;
            _client.Start();

            return true;
        }

        /// <summary>
        /// Enqueue web request
        /// </summary>
        /// <param name="data"></param>
        /// <param name="request"></param>
        public void EnqueueRequest(WebRequest data, Web.Request request)
        {
            if (!Initialize())
            {
                OnWebClientFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "WebClient")));
                return;
            }

            int id = _lastMessageId++;
            _callbacks.Add(id, request);
            WebMessage message = new WebMessage { Id = id, Data = data, Type = WebMessageType.Request };
            if (_ready)
            {
                _client.PushMessage(message);
            }
            else
            {
                _messageQueue.Enqueue(message);
            }
        }

        /// <summary>
        /// Invoked when web client application disconnected unexpectedly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProcessExited(object sender, System.EventArgs e)
        {
            Interface.uMod.NextTick(() =>
            {
                OnWebClientFailed(Interface.uMod.Strings.Apps.ClosedUnexpected.Interpolate(("version", Version), ("app", "WebClient")));
                Shutdown();
            });
        }

        /// <summary>
        /// ObjectStreamClient message handler
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="message"></param>
        private void OnMessage(ObjectStreamConnection<WebMessage, WebMessage> connection, WebMessage message)
        {
            if (message == null)
            {
                Interface.uMod.NextTick(() =>
                {
                    OnWebClientFailed(Interface.uMod.Strings.Apps.Disconnected.Interpolate(("version", Version), ("app", "WebClient")));
                    Shutdown();
                });
                return;
            }

            if (_lastMessageId > 999999)
            {
                _lastMessageId = 1;
            }

            Web.Request webRequestCallback;
            switch (message.Type)
            {
                case WebMessageType.Response:
                    webRequestCallback = _callbacks[message.Id];
                    if (webRequestCallback == null)
                    {
                        _logger.Warning(Interface.uMod.Strings.WebClient.InvalidResponse);
                        return;
                    }
                    webRequestCallback.Resolve(message.Data as WebResponse);
                    if (webRequestCallback.GetType() == typeof(Web.Request))
                    {
                        Pooling.Pools.Requests.Free(webRequestCallback);
                    }
                    _callbacks.Remove(message.Id);
                    break;

                case WebMessageType.Error:
                    webRequestCallback = _callbacks[message.Id];
                    if (webRequestCallback == null)
                    {
                        _logger.Warning(Interface.uMod.Strings.WebClient.InvalidError);
                        return;
                    }
                    webRequestCallback.Reject(new WebException(message.Data.ToString()));
                    if (webRequestCallback.GetType() == typeof(Web.Request))
                    {
                        Pooling.Pools.Requests.Free(webRequestCallback);
                    }
                    _callbacks.Remove(message.Id);
                    break;

                case WebMessageType.Ready:
                    connection.PushMessage(message);
                    if (!_ready)
                    {
                        _ready = true;
                        while (_messageQueue.Count > 0)
                        {
                            connection.PushMessage(_messageQueue.Dequeue());
                        }
                    }
                    break;
            }

            CreateIdleTimer();
        }

        /// <summary>
        /// ObjectStreamClient error handler
        /// </summary>
        /// <param name="exception"></param>
        private void OnError(Exception exception)
        {
            _logger.Report(Interface.uMod.Strings.Apps.Error.Interpolate("app", "WebClient"), exception);
        }

        /// <summary>
        /// Invoked when web client fails
        /// </summary>
        /// <param name="reason"></param>
        private void OnWebClientFailed(string reason)
        {
            WebException exception = new WebException(reason);
            foreach (Promise<WebResponse> promise in _callbacks.Values)
            {
                promise.Reject(exception);
            }

            _callbacks.Clear();
        }

        /// <summary>
        /// Shutdown web client application
        /// </summary>
        public override void Shutdown()
        {
            _ready = false;
            Process endedProcess = _process;
            if (endedProcess != null)
            {
                endedProcess.Exited -= OnProcessExited;
            }

            _process = null;
            if (_client == null)
            {
                return;
            }

            _client.Message -= OnMessage;
            _client.Error -= OnError;
            _client.PushMessage(new WebMessage { Type = WebMessageType.Exit });
            _client.Stop();
            _client = null;
            if (endedProcess == null)
            {
                return;
            }

            ThreadPool.QueueUserWorkItem(_ =>
            {
                Thread.Sleep(5000);
                // Calling Close can block up to 60 seconds on certain machines
                if (!endedProcess.HasExited)
                {
                    endedProcess.Close();
                }
            });
        }
    }
}
