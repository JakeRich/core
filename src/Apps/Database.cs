extern alias References;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using uMod.Collections;
using uMod.Common;
using uMod.Common.Database;
using uMod.Database;
using uMod.Exceptions;
using uMod.IO;
using uMod.Logging;

namespace uMod.Apps
{
    internal class Database : BaseApp
    {
        /// <summary>
        /// Web client process
        /// </summary>
        private Process _process;

        /// <summary>
        /// Determine if client is ready
        /// </summary>
        private bool _ready;

        /// <summary>
        /// ObjectStreamClient
        /// </summary>
        private ObjectStreamClient<DatabaseMessage> _client;

        /// <summary>
        /// List of queries being executed
        /// </summary>
        private readonly Hash<int, object> _callbacks;

        /// <summary>
        /// Queue of pending messages
        /// </summary>
        private readonly Queue<DatabaseMessage> _messageQueue;

        /// <summary>
        /// Current web client version
        /// </summary>
        public string Version;

        /// <summary>
        /// Last message id
        /// </summary>
        private int _lastMessageId = 1;

        /// <summary>
        /// Application logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Create new WebClient
        /// </summary>
        /// <param name="info"></param>
        /// <param name="logger"></param>
        public Database(IAppInfo info, ILogger logger) : base(info)
        {
            _callbacks = new Hash<int, object>();
            _messageQueue = new Queue<DatabaseMessage>();
            _logger = logger;
        }

        /// <summary>
        /// Set version of web client
        /// </summary>
        private void SetVersion()
        {
            Version = File.Exists(Info.ApplicationPath) ? FileVersionInfo.GetVersionInfo(Info.ApplicationPath).FileVersion : "unknown";
            SentryLogger.SetTag("database version", Version);
        }

        /// <summary>
        /// Initialize web client
        /// </summary>
        /// <returns></returns>
        public override bool Initialize()
        {
            MakeActive();
            if (_process != null && _process.Handle != IntPtr.Zero && !_process.HasExited)
            {
                return true;
            }

            SetVersion();
            Shutdown();

            string[] args = {
                $"--logPath=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\"",
                $"--dataPath=\"{PathUtils.Escape(Interface.uMod.DataDirectory)}\""
            };

            // Copy uMod.Common and uMod.Promise from core to database
            string directory = Path.GetDirectoryName(Info.ApplicationPath);
            string promiseName = Path.GetFileName(typeof(IPromise).Assembly.Location);
            string destPromisePath = Path.Combine(directory, promiseName);
            string commonName = Path.GetFileName(typeof(IPlugin).Assembly.Location);
            string destCommonPath = Path.Combine(directory, commonName);

            if (File.Exists(destPromisePath))
            {
                File.Delete(destPromisePath);
            }

            if (File.Exists(destCommonPath))
            {
                File.Delete(destCommonPath);
            }

            File.Copy(typeof(IPromise).Assembly.Location, Path.Combine(directory, promiseName));
            File.Copy(typeof(IPlugin).Assembly.Location, Path.Combine(directory, commonName));

            try
            {
                _process = new Process
                {
                    StartInfo =
                    {
                        FileName = "dotnet",
                        Arguments =  $"{Info.ApplicationPath} {string.Join(" ", args)}",
                        WorkingDirectory = Interface.uMod.RootDirectory,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                    },
                    EnableRaisingEvents = true,
                };
                _process.Exited += OnProcessExited;
                _process.Start();
            }
            catch (Exception ex)
            {
                _process?.Dispose();
                _process = null;
                _logger.Report(Interface.uMod.Strings.Apps.StartException.Interpolate(("version", Version), ("app", "Database")), ex);
                if (ex.GetBaseException() != ex)
                {
                    _logger.Report("BaseException: ", ex.GetBaseException());
                }
                if (ex is Win32Exception win32)
                {
                    _logger.Error(Interface.uMod.Strings.Exceptions.Win32.Interpolate(win32));
                }
            }

            if (_process == null)
            {
                return false;
            }

            _client = new ObjectStreamClient<DatabaseMessage>(_process.StandardOutput.BaseStream, _process.StandardInput.BaseStream);
            _client.Message += OnMessage;
            _client.Error += OnError;
            _client.Start();

            return true;
        }

        /// <summary>
        /// Enqueue database query
        /// </summary>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        internal void EnqueueQuery(Common.Database.Query data, IQueryPromise callback)
        {
            if (!Initialize())
            {
                OnDatabaseFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "Database")));
                return;
            }

            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = new DatabaseMessage { Id = id, Data = data, Type = DatabaseMessageType.Query };
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database query
        /// </summary>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        internal void EnqueueQuery(Common.Database.Query data, Action<bool, string> callback)
        {
            if (!Initialize())
            {
                OnDatabaseFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "Database")));
                return;
            }

            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = new DatabaseMessage { Id = id, Data = data, Type = DatabaseMessageType.Query };
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database connect message
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueConnect(ConnectionInfo connectionInfo, Action<ConnectionState, string> callback)
        {
            if (!Initialize())
            {
                OnDatabaseFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "Database")));
                return;
            }

            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = new DatabaseMessage { Id = id, Data = connectionInfo, Type = DatabaseMessageType.Connect };
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database disconnect message
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueDisconnect(ConnectionInfo connectionInfo, Action<ConnectionState, string> callback)
        {
            if (!Initialize())
            {
                OnDatabaseFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "Database")));
                return;
            }

            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = new DatabaseMessage { Id = id, Data = connectionInfo.Name, Type = DatabaseMessageType.Disconnect };
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database status check message
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueStatusCheck(ConnectionInfo connectionInfo, Action<ConnectionState> callback)
        {
            if (!Initialize())
            {
                OnDatabaseFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "Database")));
                return;
            }

            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = new DatabaseMessage { Id = id, Data = connectionInfo.Name, Type = DatabaseMessageType.Status };
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database transaction commit message
        /// </summary>
        /// <param name="transactionName"></param>
        /// <param name="connectionInfo"></param>
        /// <param name="callback"></param>
        internal void EnqueueCommit(string transactionName, ConnectionInfo connectionInfo, Action<bool, string> callback)
        {
            if (!Initialize())
            {
                OnDatabaseFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("version", Version), ("app", "Database")));
                return;
            }

            int id = _lastMessageId++;
            _callbacks.Add(id, callback);
            DatabaseMessage message = new DatabaseMessage { Id = id, Data = transactionName, ExtraData = connectionInfo.Name, Type = DatabaseMessageType.Commit };
            EnqueueMessage(message);
        }

        /// <summary>
        /// Enqueue database client message
        /// </summary>
        /// <param name="message"></param>
        private void EnqueueMessage(DatabaseMessage message)
        {
            if (_ready)
            {
                _client.PushMessage(message);
            }
            else
            {
                _messageQueue.Enqueue(message);
            }
        }

        /// <summary>
        /// Invoked when database client application disconnected unexpectedly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProcessExited(object sender, System.EventArgs e)
        {
            Interface.uMod.NextTick(() =>
            {
                OnDatabaseFailed(Interface.uMod.Strings.Apps.ClosedUnexpected.Interpolate(("version", Version), ("app", "Database")));
                Shutdown();
            });
        }

        /// <summary>
        /// ObjectStreamClient message handler
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="message"></param>
        private void OnMessage(ObjectStreamConnection<DatabaseMessage, DatabaseMessage> connection, DatabaseMessage message)
        {
            if (message == null)
            {
                Interface.uMod.NextTick(() =>
                {
                    OnDatabaseFailed(Interface.uMod.Strings.Apps.Disconnected.Interpolate("app", "Database"));
                    Shutdown();
                });
                return;
            }

            if (_lastMessageId > 999999)
            {
                _lastMessageId = 1;
            }

            object callback = _callbacks[message.Id];
            if (callback == null)
            {
                if (message.Type != DatabaseMessageType.Ready)
                {
                    _logger.Warning($"Database {message.Type} message for unknown query");
                }
            }
            else
            {
                _callbacks.Remove(message.Id);
            }

            switch (message.Type)
            {
                case DatabaseMessageType.Commit:
                    if (callback is Action<bool, string> commitAction)
                    {
                        commitAction.Invoke(true, message.Data?.ToString());
                    }
                    break;

                case DatabaseMessageType.Result:
                    if (callback is IQueryPromise resultCallback)
                    {
                        resultCallback.Resolve(message.Data);
                    }
                    else if (callback is Action<bool, string> resultActionCallback)
                    {
                        resultActionCallback.Invoke(true, string.Empty);
                    }
                    break;

                case DatabaseMessageType.Status:
                    if (callback is Action<ConnectionState> statusAction)
                    {
                        statusAction.Invoke(message.Data is string ? ConnectionState.Closed : (ConnectionState)message.Data);
                    }
                    break;

                case DatabaseMessageType.Connect:
                case DatabaseMessageType.Disconnect:
                    if (callback is Action<ConnectionState, string> connectionAction)
                    {
                        connectionAction.Invoke((ConnectionState)message.ExtraData, message.Data?.ToString());
                    }
                    break;

                case DatabaseMessageType.Error:
                    if (callback is IQueryPromise errorCallback)
                    {
                        errorCallback.Reject(new DatabaseException(message.Data.ToString()));
                    }
                    else if (callback is Action<ConnectionState, string> errorAction)
                    {
                        errorAction.Invoke((ConnectionState)message.ExtraData, message.Data?.ToString());
                    }
                    else if (callback is Action<ConnectionState> statusErrorAction)
                    {
                        statusErrorAction.Invoke(message.Data is string ? ConnectionState.Closed : (ConnectionState)message.Data);
                    }
                    else if (callback is Action<string> messageAction)
                    {
                        messageAction.Invoke(message.Data?.ToString());
                    }
                    else if (callback is Action<bool, string> transactionErrorAction)
                    {
                        transactionErrorAction.Invoke(false, message.Data?.ToString());
                    }
                    else
                    {
                        Interface.uMod.LogError($"Unhandled database error: {message.Data}");
                    }

                    break;

                case DatabaseMessageType.Rollback:
                    if (callback is Action<bool, string> transactionRollbackAction)
                    {
                        transactionRollbackAction.Invoke(false, message.Data?.ToString());
                    }

                    break;

                case DatabaseMessageType.Ready:
                    connection.PushMessage(message);
                    if (!_ready)
                    {
                        _ready = true;
                        while (_messageQueue.Count > 0)
                        {
                            DatabaseMessage queuedMessage = _messageQueue.Dequeue();
                            connection.PushMessage(queuedMessage);
                        }
                    }
                    break;
            }

            CreateIdleTimer();
        }

        /// <summary>
        /// ObjectStreamClient error handler
        /// </summary>
        /// <param name="exception"></param>
        private void OnError(Exception exception)
        {
            _logger.Report(Interface.uMod.Strings.Apps.Error.Interpolate("app", "Database"), exception);
        }

        /// <summary>
        /// Invoked when web client fails
        /// </summary>
        /// <param name="reason"></param>
        private void OnDatabaseFailed(string reason)
        {
            DatabaseException exception = new DatabaseException(reason);
            foreach (object callback in _callbacks.Values)
            {
                if (callback is IQueryPromise queryCallback)
                {
                    queryCallback.Reject(exception);
                }
            }

            _logger.Error($"Database failure: {reason}");

            _callbacks.Clear();
        }

        /// <summary>
        /// Shutdown database client application
        /// </summary>
        public override void Shutdown()
        {
            _ready = false;
            Process endedProcess = _process;
            if (endedProcess != null)
            {
                endedProcess.Exited -= OnProcessExited;
            }

            _process = null;
            if (_client == null)
            {
                return;
            }

            _client.Message -= OnMessage;
            _client.Error -= OnError;
            _client.PushMessage(new DatabaseMessage { Type = DatabaseMessageType.Exit });
            _client.Stop();
            _client = null;
            if (endedProcess == null)
            {
                return;
            }

            ThreadPool.QueueUserWorkItem(_ =>
            {
                Thread.Sleep(5000);
                // Calling Close can block up to 60 seconds on certain machines
                if (!endedProcess.HasExited)
                {
                    endedProcess.Close();
                }
            });
        }
    }
}
