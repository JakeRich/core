﻿namespace uMod.Apps
{
    /// <summary>
    /// Represents the base of a standalone companion application
    /// </summary>
    internal abstract class BaseApp : IApp
    {
        /// <summary>
        /// Gets the application metadata
        /// </summary>
        public IAppInfo Info { get; }

        /// <summary>
        /// Timer used to track process idle
        /// </summary>
        private Libraries.Timer.TimerInstance _idleTimer;

        private Libraries.Timer _timerLib;

        /// <summary>
        /// Gets the timer library
        /// </summary>
        protected Libraries.Timer timer
        {
            get
            {
                return _timerLib ?? (_timerLib = Interface.uMod.Libraries.Get<Libraries.Timer>());
            }
        }

        /// <summary>
        /// Determines if autoshutdown is enabled
        /// </summary>
        protected bool AutoShutdown = true;

        /// <summary>
        /// Create a new instance of an application
        /// </summary>
        /// <param name="appInfo"></param>
        internal BaseApp(IAppInfo appInfo)
        {
            Info = appInfo;
        }

        /// <summary>
        /// Destroy idle timer
        /// </summary>
        protected void MakeActive()
        {
            _idleTimer?.Destroy();
        }

        /// <summary>
        /// Creates an idle timer
        /// </summary>
        protected void CreateIdleTimer()
        {
            _idleTimer?.Destroy();
            if (AutoShutdown)
            {
                Interface.uMod.NextTick(() =>
                {
                    _idleTimer?.Destroy();
                    if (AutoShutdown)
                    {
                        _idleTimer = timer.Once(60, Shutdown);
                    }
                });
            }
        }

        /// <summary>
        /// Initializes standalone application and listener
        /// </summary>
        /// <returns></returns>
        public abstract bool Initialize();

        /// <summary>
        /// Shutdown standalone application and listener
        /// </summary>
        public abstract void Shutdown();
    }
}
