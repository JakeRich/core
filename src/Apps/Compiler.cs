using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using uMod.Common;
using uMod.Common.Compiler;
using uMod.IO;
using uMod.Logging;
using uMod.Plugins;
using uMod.Plugins.Compiler;

namespace uMod.Apps
{
    internal class Compiler : BaseApp
    {
        public static string FileName = "uMod.Compiler.dll";
        public static string BinaryPath = Path.Combine(Interface.uMod.AppDirectory, Path.Combine("compiler", FileName));
        public static string CompilerVersion;

        /// <summary>
        /// Loads the compiler version
        /// </summary>
        private static void LoadCompilerVersion()
        {
            CompilerVersion = File.Exists(BinaryPath) ? FileVersionInfo.GetVersionInfo(BinaryPath).FileVersion : "unknown";
            SentryLogger.SetTag("compiler version", CompilerVersion);
        }

        /// <summary>
        /// The process associated with the compiler application
        /// </summary>
        private Process _process;

        /// <summary>
        /// The compiler stream client
        /// </summary>
        private ObjectStreamClient<CompilationMessage> _client;

        /// <summary>
        /// List of active compile requests
        /// </summary>
        private readonly List<CompilerRequest> _activeCompileRequests = new List<CompilerRequest>();

        /// <summary>
        /// List of finished compile requests
        /// </summary>
        private readonly Queue<CompilerRequest> _finishedRequests = new Queue<CompilerRequest>();

        /// <summary>
        /// Queue of messages to be sent to the compiler
        /// </summary>
        private readonly Queue<CompilationMessage> _messageQueue;

        /// <summary>
        /// Last message id
        /// </summary>
        private volatile int _lastId;

        /// <summary>
        /// Determines if compiler process is ready
        /// </summary>
        private volatile bool _ready;

        /// <summary>
        /// Root logger to send log messages
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Plugin loader used to load compiled plugins
        /// </summary>
        private readonly CompilablePluginLoader _loader;

        /// <summary>
        /// Request lock
        /// </summary>
        private readonly object _requestLock = new object();

        /// <summary>
        /// List of blacklist namespaces
        /// </summary>
        private static IEnumerable<string> BlacklistedNamespaces => new[]
        {
            "Harmony",
            "Microsoft.CSharp.RuntimeBinder",
            "Mono.CSharp",
            "Mono.Cecil",
            "ServerFileSystem",
            "System.Activator",
            "System.AppDomain",
            "System.AppDomainManager",
            "System.Diagnostics",
            "System.Dynamic",
            "System.IO",
            "System.Linq.Expressions",
            "System.Net",
            "System.Reflection",
            "System.Runtime.CompilerServices",
            "System.Runtime.InteropServices",
            "System.Runtime.Remoting",
            "System.Security",
            "System.Threading",
            "System.Timers",
            "System.Xml",
            "UnityEngine.WWW"
        };

        /// <summary>
        /// List of whitelisted namespaces
        /// </summary>
        private static IEnumerable<string> WhitelistedNamespaces => new[]
        {
            "ProtoBuf",
            "System.Collections",
            "System.Diagnostics.Stopwatch",
            "System.IO.BinaryReader",
            "System.IO.BinaryWriter",
            "System.IO.MemoryStream",
            "System.IO.Stream",
            "System.Net.Dns",
            "System.Net.Dns.GetHostEntry",
            "System.Net.IPAddress",
            "System.Net.IPEndPoint",
            "System.Net.NetworkInformation",
            "System.Net.Sockets.SocketFlags",
            "System.Security.Cryptography",
            "System.Text",
            "System.Threading.Interlocked",
            "System.Threading.Monitor"
        };

        /// <summary>
        /// Create a new instance of the Compiler class
        /// </summary>
        /// <param name="loader"></param>
        /// <param name="logger"></param>
        public Compiler(IAppInfo appInfo, CompilablePluginLoader loader, ILogger logger) : base(appInfo)
        {
            _loader = loader;
            _logger = logger;
            _messageQueue = new Queue<CompilationMessage>();
        }

        /// <summary>
        /// Compile the specified compiler request
        /// </summary>
        /// <param name="request"></param>
        internal void Compile(CompilerRequest request)
        {
            int id = _lastId++;

            request.OnQueued(id);

            _activeCompileRequests.Add(request);

            EnqueueCompilation(request);
        }

        /// <summary>
        /// Initialize compiler application
        /// </summary>
        /// <returns></returns>
        public override bool Initialize()
        {
#if !DEBUG
            string[] args = {
                $"--logPath=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\"",
                $"--extensionPath=\"{PathUtils.Escape(Interface.uMod.ExtensionDirectory)}\""
            }; // TODO: Handle exception if log path is not accessible?
#else
            string[] args = {
                $"--logPath=\"{PathUtils.Escape(Interface.uMod.LogDirectory)}\""
            };
#endif

            string relativePath = BinaryPath.Replace(AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar, string.Empty);
            try
            {
                _process = new Process
                {
                    StartInfo =
                    {
                        FileName = "dotnet",
                        Arguments =  $"{relativePath} {string.Join(" ", args)}",
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true
                    },
                    EnableRaisingEvents = true
                };
                _process.Exited += OnProcessExited;
                _process.Start();
            }
            catch (Exception ex)
            {
                _process?.Dispose();
                _process = null;
                _logger.Report(Interface.uMod.Strings.Apps.StartException.Interpolate(("app", "Compiler"), ("version", CompilerVersion)), ex);
                if (ex.GetBaseException() != ex)
                {
                    Interface.uMod.LogException("BaseException: ", ex.GetBaseException());
                }
                if (ex is Win32Exception win32)
                {
                    _logger.Error(Interface.uMod.Strings.Exceptions.Win32.Interpolate(win32));
                }
            }

            if (_process == null)
            {
                return false;
            }

            _client = new ObjectStreamClient<CompilationMessage>(_process.StandardOutput.BaseStream, _process.StandardInput.BaseStream);
            _client.Message += OnMessage;
            _client.Error += OnError;
            _client.Start();
            return true;
        }

        /// <summary>
        /// Shutdown compiler application
        /// </summary>
        public override void Shutdown()
        {
            _ready = false;
            Process endedProcess = _process;
            if (endedProcess != null)
            {
                endedProcess.Exited -= OnProcessExited;
            }

            _process = null;
            if (_client == null)
            {
                return;
            }

            _client.Message -= OnMessage;
            _client.Error -= OnError;
            _client.PushMessage(new CompilationMessage { Type = CompilationMessageType.Exit });
            _client.Stop();
            _client = null;
            if (endedProcess == null)
            {
                return;
            }

            ThreadPool.QueueUserWorkItem(_ =>
            {
                Thread.Sleep(5000);
                // Calling Close can block up to 60 seconds on certain machines
                if (!endedProcess.HasExited)
                {
                    endedProcess.Close();
                }
            });
        }

        /// <summary>
        /// Enqueue compilation to be sent to the compiler
        /// </summary>
        /// <param name="request"></param>
        private void EnqueueCompilation(CompilerRequest request)
        {
            if (request.Plugins.Count < 1)
            {
                _logger.Debug("EnqueueCompilation called for an empty compilation");
                return;
            }

            if (!CheckCompiler())
            {
                OnCompilerFailed(Interface.uMod.Strings.Apps.StartFailure.Interpolate(("app", "Compiler"), ("version", CompilerVersion)));
                return;
            }

            request.OnSentToCompiler();

            //logger.Debug("Compiling with references: {0}", compilation.references.Keys.ToSentence());

            //Pass .cs files
            CompilationFile[] scriptFiles = request.Plugins.Select(plugin => new CompilationFile($"{plugin.FileName}.cs", plugin.ScriptBytes, plugin.Directory, Encoding.UTF8)).ToArray();

            //Pass referenced DLLs
            CompilationFile[] dllFiles = request.Plugins.SelectMany(x => x.ReferencedAssemblies.Values)
                                                        .GroupBy(x => x.FullPath).Select(x => x.FirstOrDefault()).Distinct()
                                                        .Select(dll => dll.CompilationFile)
                                                        .OrderBy(x => x.Name).ToArray();

            //Interface.uMod.LogDebug("Compiling files: {0}", sourceFiles.Select(f => f.Name).ToSentence());
            Compilation data = new Compilation
            {
                AssemblyName = request.DLLName,
                SourceFiles = scriptFiles,
                ReferenceFiles = dllFiles,
                Sandbox = true,
                WhitelistedNamespaces = WhitelistedNamespaces.ToArray(),
                BlacklistedNamespaces = BlacklistedNamespaces.ToArray()
            };

            CompilationMessage message = new CompilationMessage { Id = request.RequestId, Data = data, Type = CompilationMessageType.Compile };

            if (_ready)
            {
                _client.PushMessage(message);
            }
            else
            {
                _messageQueue.Enqueue(message);
            }
        }

        /// <summary>
        /// Process a message received from the compiler
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="message"></param>
        private void OnMessage(ObjectStreamConnection<CompilationMessage, CompilationMessage> connection, CompilationMessage message)
        {
            if (message == null)
            {
                Interface.uMod.NextTick(() =>
                {
                    OnCompilerFailed(Interface.uMod.Strings.Apps.Disconnected.Interpolate("app", "Compiler"));
                    Shutdown();
                });
                return;
            }

            switch (message.Type)
            {
                case CompilationMessageType.Assembly:
                case CompilationMessageType.Error:
                    {
                        CompilerRequest request = _activeCompileRequests.FirstOrDefault(x => x.RequestId == message.Id);

                        if (request == null)
                        {
                            _logger.Warning(Interface.uMod.Strings.Compiler.CompiledUnknownAssembly);
                            return;
                        }

                        request.OnResponseFromCompiler(message);

                        _activeCompileRequests.Remove(request);

                        lock (_requestLock)
                        {
                            _finishedRequests.Enqueue(request);
                        }

                        break;
                    }

                case CompilationMessageType.Ready:
                    {
                        connection.PushMessage(message);
                        if (!_ready)
                        {
                            _ready = true;
                            while (_messageQueue.Count > 0)
                            {
                                connection.PushMessage(_messageQueue.Dequeue());
                            }
                        }
                        break;
                    }
            }

            CreateIdleTimer();
        }

        /// <summary>
        /// Attempt to dequeue finished requests
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool TryDequeueRequest(out CompilerRequest request)
        {
            lock (_requestLock)
            {
                if (_finishedRequests.Count == 0)
                {
                    request = null;
                    return false;
                }
                request = _finishedRequests.Dequeue();
            }

            return true;
        }

        /// <summary>
        /// Log error messages
        /// </summary>
        /// <param name="exception"></param>
        private void OnError(Exception exception)
        {
            _logger.Report(Interface.uMod.Strings.Compiler.CompilationError, exception);
        }

        /// <summary>
        /// Checks that compiler process is operatiing
        /// </summary>
        /// <returns></returns>
        private bool CheckCompiler()
        {
            MakeActive();

            if (BinaryPath == null)
            {
                return false;
            }

            if (_process != null && _process.Handle != IntPtr.Zero && !_process.HasExited)
            {
                return true;
            }

            LoadCompilerVersion();
            PurgeOldLogs();
            Shutdown();

            return Initialize();
        }

        /// <summary>
        /// Handles unexpected process failures
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void OnProcessExited(object sender, System.EventArgs eventArgs)
        {
            Interface.uMod.NextTick(() =>
            {
                OnCompilerFailed(Interface.uMod.Strings.Apps.ClosedUnexpected.Interpolate(
                    ("app", "Compiler"),
                    ("version", CompilerVersion)));
                Shutdown();
            });
        }

        /// <summary>
        /// Handles compilation failures in active compile requests
        /// </summary>
        /// <param name="reason"></param>
        private void OnCompilerFailed(string reason)
        {
            foreach (CompilerRequest request in _activeCompileRequests)
            {
                foreach (CompilePluginRequest plugin in request.Plugins)
                {
                    plugin.EncounteredError(reason);
                }

                request.OnCompilerFailed(reason);
            }
        }

        /// <summary>
        /// Purges stale log files
        /// </summary>
        private static void PurgeOldLogs()
        {
            try
            {
                IEnumerable<string> filePaths = Directory.GetFiles(Interface.uMod.LogDirectory, "*.txt").Where(f =>
                {
                    string fileName = Path.GetFileName(f);
                    return fileName.StartsWith("compiler_");
                });
                foreach (string filePath in filePaths)
                {
                    File.Delete(filePath);
                }
            }
            catch (Exception)
            {
                // Ignored
            }
        }
    }
}
