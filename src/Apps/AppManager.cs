using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using uMod.Common;

namespace uMod.Apps
{
    public sealed class CompilerInfo : AppInfo
    {
        public override string FileName => "uMod.Compiler.dll";
        public override string DistributablePath => Path.Combine("compiler", FileName);
    }

    public sealed class DatabaseInfo : AppInfo
    {
        public override string FileName => "uMod.Database.dll";
        public override string DistributablePath => Path.Combine("database", FileName);
    }

    public sealed class WebClientInfo : AppInfo
    {
        public override string FileName => "uMod.WebClient.dll";
        public override string DistributablePath => Path.Combine("webclient", FileName);
    }

    public class AppManager
    {
        private readonly ILogger _logger;
        private readonly Dictionary<string, IAppInfo> _appInfo = new Dictionary<string, IAppInfo>();

        /// <summary>
        /// Determine Agent path
        /// </summary>
        public string AgentPath
        {
            get
            {
                if (!Interface.uMod.CommandLine.HasVariable("umod.agent"))
                {
                    return "umod";
                }

                Interface.uMod.CommandLine.GetArgument("umod.agent", out _, out string commandLinePath);
                return File.Exists(commandLinePath) ? commandLinePath : "umod";
            }
        }

        /// <summary>
        /// Get an AppInfo from the manager
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IAppInfo this[string key]
        {
            get => _appInfo?[key];
            set
            {
                if (_appInfo != null)
                {
                    _appInfo[key] = value;
                }
            }
        }

        /// <summary>
        /// Create AppManager
        /// </summary>
        public AppManager(ILogger logger, IInitializationInfo info = null)
        {
            _logger = logger;
            if (info == null)
            {
                _appInfo.Add("Compiler", new CompilerInfo());
                _appInfo.Add("Database", new DatabaseInfo());
                _appInfo.Add("WebClient", new WebClientInfo());
            }
            else
            {
                foreach (string appName in info.Applications)
                {
                    switch (appName)
                    {
                        case "Compiler":
                            _appInfo.Add("Compiler", new CompilerInfo());
                            break;

                        case "Database":
                            _appInfo.Add("Database", new DatabaseInfo());
                            break;

                        case "WebClient":
                            _appInfo.Add("WebClient", new WebClientInfo());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Determine if standalone apps are installed
        /// </summary>
        public bool AppsInstalled
        {
            get
            {
                return _appInfo.All(kvp => File.Exists(kvp.Value.ApplicationPath));
            }
        }

        /// <summary>
        /// Determine if specified standalone app is installed
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public bool IsInstalled(string appName)
        {
            if (_appInfo.TryGetValue(appName, out IAppInfo app))
            {
                return File.Exists(app.ApplicationPath);
            }

            return false;
        }

        /// <summary>
        /// Install standalone applications
        /// </summary>
        /// <returns></returns>
        public bool Install()
        {
#if MASTER_BRANCH
            string startArguments = "update -A --filter=apps";
#else
            string startArguments = "update -A --filter=apps --prerelease";
#endif
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = AgentPath,
                Arguments = startArguments,
                WorkingDirectory = Interface.uMod.RootDirectory,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            try
            {
                Process process = Process.Start(startInfo);
                _logger.Info(Interface.uMod.Strings.Apps.Installing);
                process?.WaitForExit();
                return process?.ExitCode == 0 && AppsInstalled;
            }
            catch (Exception ex)
            {
                Interface.uMod.LogException(Interface.uMod.Strings.Apps.InstallFailure.Interpolate("reason", "due to exception"), ex);
                return false;
            }
        }

        /// <summary>
        /// Determine if installation is valid
        /// </summary>
        public void ValidateInstall()
        {
            if (AppsInstalled)
            {
                return;
            }

            if (!Install())
            {
                throw new Exception(Interface.uMod.Strings.Apps.InstallFailure.Interpolate("reason", ", ensure the uMod Agent is installed"));
            }
        }
    }
}
