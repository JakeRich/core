﻿using System;
using System.Collections;
using System.Collections.Generic;
using uMod.Common;
using uMod.Common.Database;
using uMod.Configuration.Toml;
using uMod.Database;
using uMod.IO;

namespace uMod.Configuration
{
    public enum AuthDriver
    {
        Database,
        Protobuf,
        Json
    }

    public class PlayerProvider
    {
        [TomlProperty("inactive_cleanup", Comment = "Enable/disable inactive player cleanup")]
        public bool InactiveCleanup { get; } = true;

        [TomlProperty("inactive_period", Comment = "Clean up inactive player data after period")]
        public string InactivePeriod { get; } = "3w";
    }

    public class GroupProvider : IEnumerable<string>
    {
        [TomlProperty("create_default_groups", Comment = "Enable/disable default group creation")]
        public bool CreateDefaultGroups { get; } = true;

        [TomlProperty("automatic_grouping", Comment = "Enable/disable automatic player grouping")]
        public bool AutomaticGrouping { get; } = true;

        [TomlProperty("administrators", Comment = "Default group for administrators")]
        public string Administrators { get; } = "admin";

        [TomlProperty("moderators", Comment = "Default group for moderators")]
        public string Moderators { get; } = "moderators";

        [TomlProperty("players", Comment = "Default group for all players")]
        public string Players { get; } = "default";

        public IEnumerator<string> GetEnumerator()
        {
            yield return Administrators;
            yield return Moderators;
            yield return Players;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class Auth : TomlFile
    {
        [TomlIgnore]
        public bool Initialized;

        [TomlIgnore]
        public IEvent OnInitialized = new Event();

        [TomlProperty("driver", Comment = "Driver to use for group and player data storage")]
        public AuthDriver Driver { get; private set; } = AuthDriver.Protobuf;

        [TomlProperty("language", Comment = "Default language for players and the server")]
        public string Language { get; internal set; } = "en";

        [TomlProperty("players")]
        public PlayerProvider Players;

        [TomlProperty("groups")]
        public GroupProvider Groups;

        public Auth(string filename) : base(filename)
        {
            Players = new PlayerProvider();
            Groups = new GroupProvider();
        }

        /// <summary>
        /// Initialize auth driver
        /// </summary>
        /// <param name="database"></param>
        /// <param name="dispatcher"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public IPromise Initialize(Provider database, IDispatcher dispatcher, IInitializationInfo info = null)
        {
            Promise promise = new Promise();

            if (info?.Environment != null && info.Environment.TryGetValue("auth.driver", out string authDriver) && !string.IsNullOrEmpty(authDriver))
            {
                Driver = (AuthDriver)Enum.Parse(typeof(AuthDriver), authDriver, true);
            }

            if (Driver == AuthDriver.Database)
            {
                database.Client.Open().Done(delegate (Connection connection)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        Migrate(connection).Done(delegate
                        {
                            OnInitialized.Invoke();
                            Initialized = true;
                            promise.Resolve();
                        }, delegate (Exception exception)
                        {
                            Interface.uMod.LogException(Interface.uMod.Strings.Database.AuthenticationMigrationFailure, exception);
                            promise.Reject(exception);
                        });
                    }
                    else
                    {
                        Interface.uMod.LogError(Interface.uMod.Strings.Database.AuthenticationConnectionFailure);
                        Driver = AuthDriver.Protobuf;
                        OnInitialized.Invoke();
                        Initialized = true;
                        promise.Resolve();
                    }
                }, delegate (Exception exception)
                {
                    Interface.uMod.LogException(Interface.uMod.Strings.Database.ConnectionFailure, exception);
                    promise.Reject(exception);
                });
            }
            else
            {
                OnInitialized.Invoke();
                Initialized = true;
                promise.Resolve();
            }

            return promise;
        }

        /// <summary>
        /// Perform auth migration on specified connection
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        private IPromise Migrate(Connection connection)
        {
            return connection.Migrate<AuthMigration>(MigrationDirection.Up);
        }
    }
}
