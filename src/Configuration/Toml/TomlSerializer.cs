﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Tommy;
using uMod.Common;

namespace uMod.Configuration.Toml
{
    /// <summary>
    /// Represents a TOML serializer
    /// </summary>
    internal class TomlSerializer
    {
        private readonly IApplication _application;
        private readonly TomlContractResolver _resolver;

        public TomlSerializer(IApplication application, TomlContractResolver resolver)
        {
            _application = application;
            _resolver = resolver;
        }

        /// <summary>
        /// Deserialize Toml content to an object of the specified type
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public object Deserialize(string toml, Type type, out IEnumerable<TomlSyntaxException> exceptions)
        {
            TomlTable table;
            using (StringReader reader = new StringReader(toml))
            {
                try
                {
                    table = TOML.Parse(reader);
                    exceptions = Enumerable.Empty<TomlSyntaxException>();
                }
                catch (TomlParseException ex)
                {
                    exceptions = ex.SyntaxErrors;
                    table = ex.ParsedTable;
                }
            }

            object obj = Activator.CreateInstance(type);

            if (type == typeof(TomlTable))
            {
                return table;
            }

            Map(obj, table);

            return obj;
        }

        /// <summary>
        /// Deserialize the specified Toml content and populate the specified object
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="value"></param>
        public void Populate(string toml, object value, out IEnumerable<TomlSyntaxException> exceptions)
        {
            TomlTable table;
            using (StringReader reader = new StringReader(toml))
            {
                try
                {
                    table = TOML.Parse(reader);
                    exceptions = Enumerable.Empty<TomlSyntaxException>();
                }
                catch (TomlParseException ex)
                {
                    exceptions = ex.SyntaxErrors;
                    table = ex.ParsedTable;
                }
            }

            Map(value, table);
        }

        /// <summary>
        /// Map the specified TomlNode to the specified object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="node"></param>
        private void Map(object obj, TomlNode node)
        {
            TomlObjectContract contract = _resolver.ResolveContract(obj.GetType());

            foreach (string key in node.Keys)
            {
                TomlNode val = node[key];

                if (contract.Properties.TryGetValue(key, out MemberInfo memberInfo))
                {
                    if (memberInfo is PropertyInfo propertyInfo)
                    {
                        SetProperty(propertyInfo, val, obj);
                    }
                    else if (memberInfo is FieldInfo fieldInfo)
                    {
                        fieldInfo.SetValue(obj, GetValue(val, fieldInfo.FieldType, memberInfo));
                    }
                }
                else if (val is TomlTable && contract.AggregateMember != null)
                {
                    if (contract.AggregateMember is PropertyInfo propertyInfo)
                    {
                        if (!contract.AggregateMemberReplaced)
                        {
#if NET35 || NET40
                            propertyInfo.SetValue(obj, null, null);
#else
                            propertyInfo.SetValue(obj, null);
#endif
                            contract.AggregateMemberReplaced = true;
                        }

                        SetAggregateProperty(contract, key, propertyInfo, val, obj);
                    }
                    else if (contract.AggregateMember is FieldInfo fieldInfo)
                    {
                        if (!contract.AggregateMemberReplaced)
                        {
                            fieldInfo.SetValue(obj, null);
                            contract.AggregateMemberReplaced = true;
                        }

                        fieldInfo.SetValue(obj, AggregateValue(contract, key, fieldInfo.GetValue(obj), val, fieldInfo.FieldType));
                    }
                }
            }
        }

        /// <summary>
        /// Map node to object property and auto-property with missing setter
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="val"></param>
        /// <param name="obj"></param>
        private void SetProperty(PropertyInfo propertyInfo, TomlNode val, object obj)
        {
#if NET35 || NET40
            if (propertyInfo.GetSetMethod() == null)
#else
            if (propertyInfo.SetMethod == null)
#endif
            {
                if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                {
                    backingField.SetValue(obj, GetValue(val, propertyInfo.PropertyType, propertyInfo));
                }
            }
            else
            {
                propertyInfo.SetValue(obj, GetValue(val, propertyInfo.PropertyType, propertyInfo), null);
            }
        }

        /// <summary>
        /// Aggregate extra tables to dictionary
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="key"></param>
        /// <param name="propertyInfo"></param>
        /// <param name="val"></param>
        /// <param name="obj"></param>
        private void SetAggregateProperty(TomlObjectContract contract, string key, PropertyInfo propertyInfo, TomlNode val, object obj)
        {
#if NET35 || NET40
            if (propertyInfo.GetSetMethod() == null)
#else
            if (propertyInfo.SetMethod == null)
#endif
            {
                if (propertyInfo.DeclaringType?.GetField($"<{propertyInfo.Name}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) is FieldInfo backingField)
                {
#if NET35 || NET40
                    backingField.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj, null), val, propertyInfo.PropertyType));
#else
                    backingField.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj), val, propertyInfo.PropertyType));
#endif
                }
                else
                {
                    return;
                }
            }
            else
            {
#if NET35 || NET40
                propertyInfo.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj, null), val, propertyInfo.PropertyType), null);
#else
                propertyInfo.SetValue(obj, AggregateValue(contract, key, propertyInfo.GetValue(obj), val, propertyInfo.PropertyType), null);
#endif
            }
        }

        /// <summary>
        /// Aggregate extra table value to dictionary
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="node"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public object AggregateValue(TomlObjectContract contract, string key, object obj, TomlNode node, Type type)
        {
            if (node is TomlTable)
            {
                if (obj == null)
                {
                    obj = Activator.CreateInstance(type);
                }

                if (obj is IDictionary dict)
                {
                    Type valueType = type.GetGenericArguments()[1];

                    if (valueType.IsInterface)
                    {
                        MemberInfo memberInfo = valueType.GetMembers().FirstOrDefault(x => x.GetCustomAttribute<TomlTypeAliasAttribute>() != null);

                        if (memberInfo != null)
                        {
                            TomlTypeAliasAttribute tomlTypeAlias = memberInfo.GetCustomAttribute<TomlTypeAliasAttribute>();
                            TomlPropertyAttribute tomlPropertyAttribute = memberInfo.GetCustomAttribute<TomlPropertyAttribute>();

                            if (tomlTypeAlias != null && tomlPropertyAttribute != null)
                            {
                                if (node.HasKey(tomlPropertyAttribute.PropertyName) &&
                                    node[tomlPropertyAttribute.PropertyName] is TomlString tomlString)
                                {
                                    if (_application.TryGetType($"{tomlString.Value}{tomlTypeAlias.PostFix}",
                                        out Type impliedType))
                                    {
                                        valueType = impliedType;
                                    }
                                    else
                                    {
                                        throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Missing type: {tomlString.Value}{tomlTypeAlias.PostFix}");
                                    }
                                }
                                else
                                {
                                    throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Node \"{tomlPropertyAttribute.PropertyName}\" missing or invalid");
                                }
                            }
                            else
                            {
                                throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Add a TomlPropertyAttribute");
                            }
                        }
                        else
                        {
                            throw new InvalidOperationException($"Cannot create instance of interface {valueType.Name} for {key}. Add a TomlTypeAlias");
                        }
                    }

                    object value = GetValue(node, valueType);
                    if (!dict.Contains(key))
                    {
                        dict.Add(key, value);
                    }

                    return dict;
                }
            }

            return null;
        }

        /// <summary>
        /// Convert the specified TomlNode to the specified type
        /// </summary>
        /// <param name="node"></param>
        /// <param name="type"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        public object GetValue(TomlNode node, Type type, MemberInfo member = null)
        {
            if (node.HasValue)
            {
                switch (node)
                {
                    case TomlString str:
                        if (type == typeof(char))
                        {
                            return Convert.ChangeType(str.Value, type);
                        }
                        if (type.IsEnum)
                        {
                            return Enum.Parse(type, str.Value, true);
                        }
                        return str.Value;

                    case TomlInteger i:
                        return Convert.ChangeType(i.Value, type);

                    case TomlFloat f:
                        return Convert.ChangeType(f.Value, type);

                    case TomlDateTime dt:
                        return Convert.ChangeType(dt.Value, type);

                    case TomlBoolean b:
                        return Convert.ChangeType(b.Value, type);

                    case TomlArray arr:
                        if (type.IsArray)
                        {
                            Type elementType = type.GetElementType();
                            Array obj = Array.CreateInstance(elementType, arr.ChildrenCount);

                            int x = 0;
                            foreach (TomlNode arrValue in arr.Children)
                            {
                                obj.SetValue(GetValue(arrValue, elementType), x);
                                x++;
                            }

                            return obj;
                        }
                        else if (type.IsAssignableFrom<IList>())
                        {
                            Type elementType = type.GetGenericArguments()[0];
                            IList list = (IList)Activator.CreateInstance(type);
                            foreach (TomlNode arrValue in arr.Children)
                            {
                                list.Add(GetValue(arrValue, elementType));
                            }

                            return list;
                        }
                        else if (type.IsAssignableFrom(typeof(HashSet<>)))
                        {
                            Type elementType = type.GetGenericArguments()[0];
                            ICollection list = (ICollection)Activator.CreateInstance(type);
                            MethodInfo addMethod = type.GetMethod("Add");
                            object[] parameters = { null };
                            if (addMethod != null)
                            {
                                foreach (TomlNode arrValue in arr.Children)
                                {
                                    parameters[0] = GetValue(arrValue, elementType);
                                    addMethod.Invoke(list, parameters);
                                }
                            }

                            return list;
                        }
                        break;
                }
            }

            if (node is TomlTable t)
            {
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    Type valueType = type.GetGenericArguments()[1];
                    IDictionary dict = (IDictionary)Activator.CreateInstance(type);

                    foreach (string key in node.Keys)
                    {
                        TomlNode subNode = node[key];

                        dict.Add(key, GetValue(subNode, valueType));
                    }

                    return dict;
                }

                if (type.IsInterface && member != null)
                {
                    object tomlPropertyObject = member.GetCustomAttributes(typeof(TomlPropertyAttribute), true).FirstOrDefault();
                    if (tomlPropertyObject is TomlPropertyAttribute tomlProperty && tomlProperty.DefaultType != null)
                    {
                        type = tomlProperty.DefaultType;
                    }
                }
                object obj = Activator.CreateInstance(type);

                Map(obj, t);
                return obj;
            }

            return null;
        }

        /// <summary>
        /// Serialize the specified object to a Toml compatible string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string Serialize(object value, Type type = null)
        {
            if (!(value is TomlTable table))
            {
                table = ToTable(value, type ?? value.GetType());
            }

            StringBuilder stringBuilder = Pooling.Pools.StringBuilders.Get();
            using (StringWriter stringWriter = new StringWriter(stringBuilder))
            {
                table.ToTomlString(stringWriter);
            }

            try
            {
                return stringBuilder.ToString();
            }
            finally
            {
                Pooling.Pools.StringBuilders.Free(stringBuilder);
            }
        }

        /// <summary>
        /// Convert the specified object to a TomlTable
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        private TomlTable ToTable(object value, Type type = null, string comment = null)
        {
            TomlObjectContract contract = _resolver.ResolveContract(type ?? value.GetType());

            TomlTable table = new TomlTable()
            {
                Comment = comment
            };

            foreach (KeyValuePair<string, MemberInfo> kvp in contract.Properties)
            {
                bool aggregate;
                if (kvp.Value is FieldInfo fieldInfo)
                {
                    aggregate = fieldInfo.GetCustomAttribute<TomlAggregateAttribute>() != null;
                    TomlNode node = ToNode(table, fieldInfo.GetValue(value), fieldInfo.FieldType, aggregate, fieldInfo.GetCustomAttributes(true).Cast<Attribute>());
                    if (node != null)
                    {
                        table.Add(kvp.Key, node);
                    }
                }
                else if (kvp.Value is PropertyInfo propertyInfo)
                {
                    aggregate = propertyInfo.GetCustomAttribute<TomlAggregateAttribute>() != null;
                    TomlNode node = ToNode(table, propertyInfo.GetValue(value, null), propertyInfo.PropertyType, aggregate, propertyInfo.GetCustomAttributes(true).Cast<Attribute>());
                    if (node != null)
                    {
                        table.Add(kvp.Key, node);
                    }
                }
            }

            return table;
        }

        /// <summary>
        /// Convert the specified value to a TOML type
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="value"></param>
        /// <param name="fieldType"></param>
        /// <param name="aggregate"></param>
        /// <param name="attributes"></param>
        /// <returns></returns>
        private TomlNode ToNode(TomlNode parentNode, object value, Type fieldType, bool aggregate = false, IEnumerable<Attribute> attributes = null)
        {
            if (value == null)
            {
                return null;
            }

            TomlPropertyAttribute propertyAttribute = attributes?.FirstOrDefault(x => x is TomlPropertyAttribute) as TomlPropertyAttribute;
            TomlCommentAttribute commentAttribute = fieldType.GetCustomAttribute<TomlCommentAttribute>();

            string comment = commentAttribute?.Comment ?? propertyAttribute?.Comment;

            if (!fieldType.IsValueType && !fieldType.IsPrimitive)
            {
                switch (value)
                {
                    case string _:
                        return new TomlString()
                        {
                            Value = value.ToString(),
                            IsMultiline = propertyAttribute?.MultiLine ?? false,
                            Comment = comment
                        };

                    case DateTime dt:
                        return new TomlDateTime()
                        {
                            Value = dt,
                            Comment = comment
                        };

                    default:
                        {
                            if (fieldType.IsArray)
                            {
                                TomlArray arr = new TomlArray
                                {
                                    IsTableArray = (attributes?.Where(x => x is TomlTableArrayAttribute)).Any(),
                                    Comment = comment,
                                };

                                Type elementType = fieldType.GetElementType();
                                foreach (object arrItem in (Array)value)
                                {
                                    arr.RawArray.Add(ToNode(arr, arrItem, elementType));
                                }

                                return arr;
                            }
                            if (fieldType.IsAssignableFrom<IList>())
                            {
                                Type[] genericArguments = fieldType.GetGenericArguments();
                                if (genericArguments.Length == 1)
                                {
                                    Type elementType = genericArguments[0];
                                    TomlArray arr = new TomlArray
                                    {
                                        IsTableArray = (attributes?.Where(x => x is TomlTableArrayAttribute)).Any(),
                                        Comment = comment
                                    };

                                    foreach (object arrItem in (IList)value)
                                    {
                                        arr.RawArray.Add(ToNode(arr, arrItem, elementType));
                                    }

                                    return arr;
                                }
                            }

                            Type[] interfaces = fieldType.GetInterfaces();
                            if (Array.Exists(interfaces, @interface => @interface.IsAssignableFrom(typeof(ICollection<>))))
                            {
                                Type[] genericArguments = fieldType.GetGenericArguments();
                                if (genericArguments.Length == 1)
                                {
                                    Type elementType = genericArguments[0];
                                    TomlArray arr = new TomlArray
                                    {
                                        IsTableArray = (attributes?.Where(x => x is TomlTableArrayAttribute)).Any(),
                                        Comment = comment
                                    };

                                    MethodInfo enumMethod = fieldType.GetMethod("GetEnumerator");
                                    IEnumerator enumerator = (IEnumerator)enumMethod.Invoke(value, null);

                                    while (enumerator.MoveNext())
                                    {
                                        object arrItem = enumerator.Current;
                                        arr.RawArray.Add(ToNode(arr, arrItem, elementType));
                                    }

                                    return arr;
                                }
                            }

                            break;
                        }
                }

                if (fieldType.IsGenericType && fieldType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    TomlTable table = new TomlTable
                    {
                        Comment = comment
                    };

                    if (aggregate)
                    {
                        foreach (DictionaryEntry entry in (IDictionary)value)
                        {
                            parentNode.Add(entry.Key.ToString(), ToNode(table, entry.Value, entry.Value.GetType()));
                        }

                        return null;
                    }

                    foreach (DictionaryEntry entry in (IDictionary)value)
                    {
                        table.Add(entry.Key.ToString(), ToNode(table, entry.Value, entry.Value.GetType()));
                    }

                    return table;
                }
                if (fieldType.IsClass || fieldType.IsInterface)
                {
                    return ToTable(value, fieldType, comment);
                }
            }
            else
            {
                switch (value)
                {
                    case int _:
                    case long _:
                    case short _:
                    case ushort _:
                    case uint _:
                        return new TomlInteger()
                        {
                            Value = (long)Convert.ChangeType(value, typeof(long)),
                            IntegerBase = propertyAttribute?.IntegerBase ?? TomlInteger.Base.Decimal,
                            Comment = propertyAttribute?.Comment
                        };

                    case char _:
                        return new TomlString()
                        {
                            Value = value.ToString(),
                            Comment = propertyAttribute?.Comment,
                            PreferLiteral = true
                        };

                    case float _:
                        return new TomlFloat()
                        {
                            Value = Convert.ToDouble(value),
                            Comment = propertyAttribute?.Comment
                        };

                    case decimal _:
                    case double _:
                        return new TomlFloat()
                        {
                            Value = (double)value,
                            Comment = propertyAttribute?.Comment
                        };

                    case bool boolValue:
                        return new TomlBoolean()
                        {
                            Value = boolValue,
                            Comment = propertyAttribute?.Comment
                        };

                    default:
                        {
                            if (fieldType.IsEnum)
                            {
                                return new TomlString()
                                {
                                    Value = value.ToString().ToLower(),
                                    Comment = propertyAttribute?.Comment
                                };
                            }

                            break;
                        }
                }
            }

            return null;
        }
    }
}
