﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace uMod.Configuration.Toml
{
    /// <summary>
    /// Determines which fields and properties can or should be serialized in TOML
    /// </summary>
    internal class TomlContractResolver
    {
        internal Cache<Type, TomlObjectContract> CachedContracts = new Cache<Type, TomlObjectContract>();

        /// <summary>
        /// Gets a TomlObjectContract for the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public TomlObjectContract ResolveContract(Type type)
        {
            if (CachedContracts.TryGetValue(type, out TomlObjectContract contract))
            {
                return contract;
            }

            contract = MakeContract(type);

            CachedContracts.Add(type, contract);

            return contract;
        }

        /// <summary>
        /// Make a contract that represents how data a type should be serialized
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private TomlObjectContract MakeContract(Type type)
        {
            TomlObjectContract contract = new TomlObjectContract(type);
            Dictionary<string, MemberInfo> members = new Dictionary<string, MemberInfo>();

            FieldInfo[] allFields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            PropertyInfo[] allProperties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            List<MemberInfo> allMembers = new List<MemberInfo>();
            allMembers.AddRange(allFields.Where(FilterField).Cast<MemberInfo>());
            allMembers.AddRange(allProperties.Where(FilterProperty).Cast<MemberInfo>());

            FieldInfo[] defaultFields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] defaultProperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            List<MemberInfo> defaultMembers = new List<MemberInfo>();
            defaultMembers.AddRange(defaultFields.Where(FilterField).Cast<MemberInfo>());
            defaultMembers.AddRange(defaultProperties.Where(FilterProperty).Cast<MemberInfo>());
            foreach (MemberInfo member in allMembers)
            {
                if (member.GetCustomAttribute<TomlAggregateAttribute>() != null)
                {
                    contract.AggregateMember = member;
                }

                TomlPropertyAttribute propertyAttribute;
                if (defaultMembers.Contains(member))
                {
                    if ((propertyAttribute = member.GetCustomAttribute<TomlPropertyAttribute>()) != null)
                    {
                        members.Add(propertyAttribute.PropertyName ?? member.Name, member);
                    }
                    else
                    {
                        members.Add(member.Name, member);
                    }
                }
                else
                {
                    if ((propertyAttribute = member.GetCustomAttribute<TomlPropertyAttribute>()) != null)
                    {
                        members.Add(propertyAttribute.PropertyName ?? member.Name, member);
                    }
                }
            }

            contract.Properties = members;

            return contract;
        }

        /// <summary>
        /// Field filter
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private bool FilterField(FieldInfo field)
        {
            return !field.IsDefined(typeof(CompilerGeneratedAttribute), true) && field.GetCustomAttribute<TomlIgnoreAttribute>() == null;
        }

        /// <summary>
        /// Property filter
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        private bool FilterProperty(PropertyInfo property)
        {
            return property.GetIndexParameters().Length == 0 && !property.IsDefined(typeof(CompilerGeneratedAttribute), true) && property.GetCustomAttribute<TomlIgnoreAttribute>() == null;
        }
    }
}
