﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace uMod.Configuration.Toml
{
    /// <summary>
    /// Represents a Toml object contract
    /// </summary>
    internal class TomlObjectContract
    {
        /// <summary>
        /// The type that this contract describes
        /// </summary>
        public readonly Type Type;

        /// <summary>
        /// The members of the contract to be serialized/deserialized
        /// </summary>
        public Dictionary<string, MemberInfo> Properties;

        /// <summary>
        /// Gets the member that extra tables are assigned to
        /// </summary>
        public MemberInfo AggregateMember;

        /// <summary>
        /// Gets whether or not the aggregate member is fresh
        /// </summary>
        public bool AggregateMemberReplaced = false;

        /// <summary>
        /// Create a TomlObjectContract
        /// </summary>
        /// <param name="type"></param>
        public TomlObjectContract(Type type)
        {
            Type = type;
        }
    }
}
