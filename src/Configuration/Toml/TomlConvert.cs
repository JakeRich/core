﻿using System;
using System.Collections.Generic;
using Tommy;

namespace uMod.Configuration.Toml
{
    /// <summary>
    /// Specifies a field or property should supply type for parent section
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class TomlTypeAliasAttribute : Attribute
    {
        /// <summary>
        /// The post-fix of the type alias in the service container
        /// </summary>
        public readonly string PostFix;

        /// <summary>
        /// Create a new instance of the TomlTypeAliasAttribute with an optional postfix
        /// </summary>
        /// <param name="postFix"></param>
        public TomlTypeAliasAttribute(string postFix = null)
        {
            PostFix = postFix;
        }
    }

    /// <summary>
    /// Specifies a field or property should aggregate tables
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class TomlAggregateAttribute : Attribute
    {
    }

    /// <summary>
    /// Specifies a field or property to be ignored during serialization
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class)]
    public sealed class TomlCommentAttribute : Attribute
    {
        /// <summary>
        /// The section or property command
        /// </summary>
        public string Comment;

        /// <summary>
        /// Create a new instance of the TomlCommentAttribute with the specified comment
        /// </summary>
        /// <param name="comment"></param>
        public TomlCommentAttribute(string comment)
        {
            Comment = comment;
        }
    }

    /// <summary>
    /// Specifies a field or property to be ignored during serialization
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class TomlIgnoreAttribute : Attribute
    {
    }

    /// <summary>
    /// Describes how a TOML property should be serialized
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class TomlPropertyAttribute : Attribute
    {
        /// <summary>
        /// The name of the property as it appears in the configuration file
        /// </summary>
        public string PropertyName;

        /// <summary>
        /// The property comment
        /// </summary>
        public string Comment;

        /// <summary>
        /// Determines whether or not the value is multi-line
        /// </summary>
        public bool MultiLine;

        /// <summary>
        /// The integer base (if property is an integer)
        /// </summary>
        public TomlInteger.Base IntegerBase = TomlInteger.Base.Decimal;

        /// <summary>
        /// The type to instantiate for the property value (if the property type is abstract or an interface)
        /// </summary>
        public Type DefaultType;

        /// <summary>
        /// Create a new instance of the TomlPropertyAttribute class
        /// </summary>
        public TomlPropertyAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the TomlPropertyAttribute class with the specified property name
        /// </summary>
        /// <param name="propertyName"></param>
        public TomlPropertyAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }
    }

    /// <summary>
    /// Specifies an array or list which should be a table array (not an inline array)
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class TomlTableArrayAttribute : Attribute
    {
    }

    /// <summary>
    /// Convert between objects and TOML content
    /// </summary>
    public static class TomlConvert
    {
        private static readonly TomlSerializer Serializer = new TomlSerializer(Interface.uMod.Application, new TomlContractResolver());

        /// <summary>
        /// Serialize a generic object to a TOML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SerializeObject<T>(T value)
        {
            return Serializer.Serialize(value, value.GetType());
        }

        /// <summary>
        /// Serialize an object to a TOML string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string SerializeObject(object value, Type type = null)
        {
            return Serializer.Serialize(value, type);
        }

        /// <summary>
        /// Populate the specified object with the specified TOML content
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="value"></param>
        public static void PopulateObject(string toml, object value)
        {
            Serializer.Populate(toml, value, out _);
        }

        /// <summary>
        /// Populate the specified object with the specified TOML content
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="value"></param>
        /// <param name="syntaxExceptions"></param>
        public static void PopulateObject(string toml, object value, out IEnumerable<TomlSyntaxException> syntaxExceptions)
        {
            Serializer.Populate(toml, value, out syntaxExceptions);
        }

        /// <summary>
        /// Deserialize the specified TOML string to a generic object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toml"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string toml)
        {
            return (T)Serializer.Deserialize(toml, typeof(T), out _);
        }

        /// <summary>
        /// Deserialize the specified TOML string to the specified type
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object DeserializeObject(string toml, Type type)
        {
            return Serializer.Deserialize(toml, type, out _);
        }

        /// <summary>
        /// Deserialize the specified TOML string to a generic object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toml"></param>
        /// <param name="syntaxExceptions"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string toml, out IEnumerable<TomlSyntaxException> syntaxExceptions)
        {
            return (T)Serializer.Deserialize(toml, typeof(T), out syntaxExceptions);
        }

        /// <summary>
        /// Deserialize the specified TOML string to the specified type
        /// </summary>
        /// <param name="toml"></param>
        /// <param name="type"></param>
        /// <param name="syntaxExceptions"></param>
        /// <returns></returns>
        public static object DeserializeObject(string toml, Type type, out IEnumerable<TomlSyntaxException> syntaxExceptions)
        {
            return Serializer.Deserialize(toml, type, out syntaxExceptions);
        }
    }
}
