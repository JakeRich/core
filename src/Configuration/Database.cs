﻿using System;
using System.Collections.Generic;
using System.IO;
using uMod.Common.Database;
using uMod.Configuration.Toml;
using uMod.IO;
using uMod.Utilities;

namespace uMod.Configuration
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ConnectionAttribute : Attribute
    {
        public readonly string Name;

        public ConnectionAttribute(string name)
        {
            Name = name;
        }
    }

    /// <summary>
    /// Represents a Database connection interface
    /// </summary>
    public interface IDatabaseConnection
    {
        /// <summary>
        /// Gets the connection driver ("sqlite", "mysql")
        /// </summary>
        [TomlTypeAlias("db_connection")]
        [TomlProperty("driver")]
        string Driver { get; }

        /// <summary>
        /// Gets the database name
        /// </summary>
        [TomlProperty("database")]
        string Database { get; }

        /// <summary>
        /// Gets the connection persistence
        /// </summary>
        [TomlProperty("persistent")]
        bool Persistent { get; }

        /// <summary>
        /// Gets the connection info associated with this connection configuration
        /// </summary>
        [TomlIgnore]
        ConnectionInfo Connection { get; }

        /// <summary>
        /// Configure connection using configuration
        /// </summary>
        /// <param name="db"></param>
        /// <param name="connection"></param>
        void Configure(Database db, ConnectionInfo connection);
    }

    /// <summary>
    /// Represents a Base connection configuration
    /// </summary>
    public abstract class BaseConnection : IDatabaseConnection
    {
        /// <summary>
        /// Gets the connection driver ("sqlite", "mysql")
        /// </summary>
        [TomlProperty("driver")]
        public abstract string Driver { get; }

        /// <summary>
        /// Gets the database name
        /// </summary>
        [TomlProperty("database")]
        public virtual string Database { get; protected set; }

        /// <summary>
        /// Gets the database name
        /// </summary>
        [TomlProperty("persistent")]
        public virtual bool Persistent { get; protected set; } = true;

        /// <summary>
        /// Gets the connection info associated with this connection configuration
        /// </summary>
        [TomlIgnore]
        public ConnectionInfo Connection { get; protected set; }

        /// <summary>
        /// Configure connection using configuration
        /// </summary>
        /// <param name="db"></param>
        /// <param name="connection"></param>
        public virtual void Configure(Database db, ConnectionInfo connection)
        {
            Connection = connection;
            connection.Persistent = Persistent;
        }
    }

    /// <summary>
    /// Represents a SQLite connection configuration
    /// </summary>
    [TomlComment("SQLite embedded relational database")]
    [Connection("sqlite")]
    public class SqliteConnection : BaseConnection
    {
        /// <summary>
        /// Gets the connection driver "sqlite"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "sqlite";

        /// <summary>
        /// Gets the database name
        /// </summary>
        [TomlProperty("database")]
        public override string Database { get; protected set; } = "umod.db";

        /// <summary>
        /// Configure sqlite connection using configuration
        /// </summary>
        /// <param name="db"></param>
        /// <param name="connection"></param>
        public override void Configure(Database db, ConnectionInfo connection)
        {
            base.Configure(db, connection);

            connection.Type = ConnectionType.SQLite;
            string path = Path.Combine(Interface.uMod.DataDirectory, Database);
            string dir = Utility.GetDirectoryName(path);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            connection.ConnectionString = $"Data Source={path};Version=3;";
        }
    }

    /// <summary>
    /// Represents a MySQL connection configuration
    /// </summary>
    [TomlComment("MySQL remote relational database")]
    [Connection("mysql")]
    public class MySqlConnection : BaseConnection
    {
        /// <summary>
        /// Gets the connection driver "mysql"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "mysql";

        /// <summary>
        /// Gets the database name
        /// </summary>
        [TomlProperty("database")]
        public override string Database { get; protected set; } = "umod";

        /// <summary>
        /// Gets the host
        /// </summary>
        [TomlProperty("host")]
        public string Host { get; } = "localhost";

        /// <summary>
        /// Gets the port
        /// </summary>
        [TomlProperty("port")]
        public int Port { get; } = 3306;

        /// <summary>
        /// Gets the username
        /// </summary>
        [TomlProperty("username")]
        public string Username { get; } = "user";

        /// <summary>
        /// Gets the password
        /// </summary>
        [TomlProperty("password")]
        public string Password { get; } = "password";

        /// <summary>
        /// Gets the protocol
        /// </summary>
        [TomlProperty("protocol")]
        public string Protocol { get; } = "socket";

        /// <summary>
        /// Gets the charset
        /// </summary>
        [TomlProperty("charset")]
        public string CharSet { get; } = "utf8";

        /// <summary>
        /// Configure mysql connection using configuration
        /// </summary>
        /// <param name="db"></param>
        /// <param name="connection"></param>
        public override void Configure(Database db, ConnectionInfo connection)
        {
            base.Configure(db, connection);

            connection.Type = ConnectionType.MySQL;
            connection.ConnectionString = $"Server={Host};Port={Port};Protocol={Protocol};Database={Database};User={Username};Password={Password};CharSet={CharSet};default command timeout={db.Timeout};AllowZeroDatetime=true;";
        }
    }

    /// <summary>
    /// Represents the database configuration
    /// </summary>
    public class Database : TomlFile
    {
        /// <summary>
        /// Default database driver
        /// </summary>
        [TomlProperty("default")]
        public string Default { get; }

        /// <summary>
        /// Default query timeout
        /// </summary>
        [TomlProperty("timeout")]
        public int Timeout { get; }

        /// <summary>
        /// Default connections
        /// </summary>
        [TomlProperty("connections")]
        [TomlAggregate]
        public Dictionary<string, IDatabaseConnection> Connections;

        /// <summary>
        /// Create a new instance of a database configuration
        /// </summary>
        /// <param name="filename"></param>
        public Database(string filename) : base(filename)
        {
            Default = "sqlite";
            Timeout = 30;

            Connections = new Dictionary<string, IDatabaseConnection>()
            {
                ["sqlite"] = new SqliteConnection(),
                ["mysql"] = new MySqlConnection()
            };
        }

        /// <summary>
        /// Resolve all IDatabaseConnection interfaces from core and extensions
        /// </summary>
        internal static void ResolveConnectionTypes(TypeManager types)
        {
            IEnumerable<Type> databaseConnections = types.Resolve<IDatabaseConnection>();
            foreach (Type channelType in databaseConnections)
            {
                if (channelType.IsAbstract || channelType.IsInterface)
                {
                    continue;
                }

                if (channelType.GetCustomAttribute<ConnectionAttribute>() is ConnectionAttribute loggerAttribute)
                {
                    Interface.uMod.Application.Resolve(channelType);
                    Interface.uMod.Application.Alias($"{loggerAttribute.Name}db_connection", channelType);
                }
            }
        }

        /// <summary>
        /// Configure all connections
        /// </summary>
        internal void Initialize()
        {
            foreach (KeyValuePair<string, IDatabaseConnection> kvp in Connections)
            {
                ConnectionInfo connection = new ConnectionInfo()
                {
                    Name = kvp.Key
                };

                kvp.Value.Configure(this, connection);
            }
        }
    }
}
