extern alias References;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using References::Newtonsoft.Json;
using References::Newtonsoft.Json.Linq;

namespace uMod.Configuration
{
    /// <summary>
    /// Represents a config file with a dynamic layout
    /// </summary>
    public class DynamicConfigFile : ConfigFile, IEnumerable<KeyValuePair<string, object>>
    {
        public JsonSerializerSettings Settings { get; set; } = new JsonSerializerSettings();
        private Dictionary<string, object> _keyvalues;
        private readonly JsonSerializerSettings _settings;

        /// <summary>
        /// Initializes a new instance of the DynamicConfigFile class
        /// </summary>
        public DynamicConfigFile(string filename) : base(filename)
        {
            _keyvalues = new Dictionary<string, object>();
            _settings = new JsonSerializerSettings();
            _settings.Converters.Add(new IO.JsonKeyValuesConverter());
        }

        /// <summary>
        /// Loads this config from the specified file
        /// </summary>
        /// <param name="filename"></param>
        public override void Load(string filename = null)
        {
            filename = CheckPath(filename ?? Filename);
            if (File.Exists(filename))
            {
                string source = File.ReadAllText(filename);
                _keyvalues = JsonConvert.DeserializeObject<Dictionary<string, object>>(source, _settings);
                OnRead?.Invoke(this);
            }
        }

        /// <summary>
        /// Loads this config from the specified file
        /// </summary>
        /// <param name="filename"></param>
        public override IPromise<ConfigFile> LoadAsync(string filename = null)
        {
            Promise<ConfigFile> promise = new Promise<ConfigFile>();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    filename = CheckPath(filename ?? Filename);
                    string source = File.ReadAllText(filename);
                    _keyvalues = JsonConvert.DeserializeObject<Dictionary<string, object>>(source, _settings);
                    OnRead?.Invoke(this);
                    promise.Resolve(this);
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Loads this config from the specified file
        /// </summary>
        /// <param name="filename"></param>
        public T ReadObject<T>(string filename = null)
        {
            filename = CheckPath(filename ?? Filename);
            T customObject;
            if (Exists(filename))
            {
                string source = File.ReadAllText(filename);
                customObject = JsonConvert.DeserializeObject<T>(source, Settings);
            }
            else
            {
                customObject = Interface.uMod.Application.Make<T>();
                //customObject = Activator.CreateInstance<T>();
                WriteObject(customObject, false, filename);
            }
            return customObject;
        }

        /// <summary>
        /// Loads this config from the specified file
        /// </summary>
        /// <param name="filename"></param>
        public IPromise<T> ReadObjectAsync<T>(string filename = null)
        {
            Promise<T> promise = new Promise<T>();

            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    filename = CheckPath(filename ?? Filename);

                    if (Exists(filename))
                    {
                        string source = File.ReadAllText(filename);
                        T customObject = JsonConvert.DeserializeObject<T>(source, Settings);

                        promise.Resolve(customObject);
                    }
                    else
                    {
                        T customObject = Interface.uMod.Application.Make<T>();
                        //customObject = Activator.CreateInstance<T>();
                        WriteObject(customObject, false, filename);
                        promise.Resolve(customObject);
                    }
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Loads this config from the specified file
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filename"></param>
        internal IPromise<object> ReadObjectAsync(Type type, string filename = null)
        {
            Promise<object> promise = new Promise<object>();
            filename = CheckPath(filename ?? Filename);
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    if (Exists(filename))
                    {
                        string source = File.ReadAllText(filename);
                        OnRead?.Invoke(this);
                        object customObject = JsonConvert.DeserializeObject(source, Settings);
                        if (customObject is JObject jObject)
                        {
                            promise.Resolve(jObject.ToObject(type));
                        }
                    }
                    else
                    {
                        object customObject = Interface.uMod.Application.Make<object>(type);
                        //customObject = Activator.CreateInstance(type);
                        WriteObject(customObject, false, filename);
                        OnWrite?.Invoke(this);
                        promise.Resolve(customObject);
                    }
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Saves this config to the specified file
        /// </summary>
        /// <param name="filename"></param>
        public override void Save(string filename = null)
        {
            filename = CheckPath(filename ?? Filename);
            if (Interface.uMod.Plugins.Configuration.Watchers.ConfigWatchers)
            {
                Interface.uMod.ConfigChanges.Add(filename);
            }
            string dir = Utility.GetDirectoryName(filename);
            if (dir != null && !Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            if (_keyvalues != null && _keyvalues.Count > 0)
            {
                File.WriteAllText(filename, JsonConvert.SerializeObject(_keyvalues, Formatting.Indented, _settings));
                OnWrite?.Invoke(this);
            }
        }

        /// <summary>
        /// Saves this config to the specified file asynchronously
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override IPromise<object> SaveAsync(string filename = null)
        {
            Promise<object> promise = new Promise<object>();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    filename = CheckPath(filename ?? Filename);
                    if (Interface.uMod.Plugins.Configuration.Watchers.ConfigWatchers)
                    {
                        Interface.uMod.ConfigChanges.Add(filename);
                    }
                    string dir = Utility.GetDirectoryName(filename);
                    if (dir != null && !Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }

                    File.WriteAllText(filename,
                        JsonConvert.SerializeObject(_keyvalues, Formatting.Indented, _settings));
                    OnWrite?.Invoke(this);
                    promise.Resolve(this);
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Saves the specified object to the specified file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="config"></param>
        /// <param name="syncFile"></param>
        /// <param name="filename"></param>
        public void WriteObject<T>(T config, bool syncFile = false, string filename = null)
        {
            filename = CheckPath(filename ?? Filename);

            string dir = Utility.GetDirectoryName(filename);
            if (dir != null && !Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            string json = JsonConvert.SerializeObject(config, Formatting.Indented, Settings);
            if (Interface.uMod.Plugins.Configuration.Watchers.ConfigWatchers)
            {
                Interface.uMod.ConfigChanges.Add(filename);
            }

            File.WriteAllText(filename, json);
            OnWrite?.Invoke(this);
            if (syncFile)
            {
                _keyvalues = JsonConvert.DeserializeObject<Dictionary<string, object>>(json, _settings);
            }
        }

        /// <summary>
        /// Saves the specified object to the specified file asynchronously
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="config"></param>
        /// <param name="syncFile"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public IPromise<T> WriteObjectAsync<T>(T config, bool syncFile = false, string filename = null)
        {
            Promise<T> promise = new Promise<T>();

            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    filename = CheckPath(filename ?? Filename);

                    string dir = Utility.GetDirectoryName(filename);
                    if (dir != null && !Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }

                    string json = JsonConvert.SerializeObject(config, Formatting.Indented, Settings);
                    if (Interface.uMod.Plugins.Configuration.Watchers.ConfigWatchers)
                    {
                        Interface.uMod.ConfigChanges.Add(filename);
                    }

                    File.WriteAllText(filename, json);
                    OnWrite?.Invoke(this);
                    if (syncFile)
                    {
                        _keyvalues = JsonConvert.DeserializeObject<Dictionary<string, object>>(json, _settings);
                    }
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Converts object contents to JSON
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(_keyvalues, Formatting.Indented, _settings);
        }

        /// <summary>
        /// Imports raw JSON into object
        /// </summary>
        /// <param name="json"></param>
        public void FromString(string json)
        {
            _keyvalues = JsonConvert.DeserializeObject<Dictionary<string, object>>(json, _settings);
        }

        /// <summary>
        /// Checks if the file or specified file exists
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Exists(string filename = null)
        {
            filename = CheckPath(filename ?? Filename);
            string dir = Utility.GetDirectoryName(filename);
            if (dir != null && !Directory.Exists(dir))
            {
                return false;
            }

            return File.Exists(filename);
        }

        /// <summary>
        /// Clears this config
        /// </summary>
        public void Clear()
        {
            _keyvalues.Clear();
        }

        /// <summary>
        /// Removes key from config
        /// </summary>
        public void Remove(string key)
        {
            _keyvalues.Remove(key);
        }

        /// <summary>
        /// Gets or sets a setting on this config by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[string key]
        {
            get => _keyvalues.TryGetValue(key, out object val) ? val : null;
            set => _keyvalues[key] = value;
        }

        /// <summary>
        /// Gets or sets a nested setting on this config by key
        /// </summary>
        /// <param name="keyLevel1"></param>
        /// <param name="keyLevel2"></param>
        /// <returns></returns>
        public object this[string keyLevel1, string keyLevel2]
        {
            get => Get(keyLevel1, keyLevel2);
            set => Set(keyLevel1, keyLevel2, value);
        }

        /// <summary>
        /// Gets or sets a nested setting on this config by key
        /// </summary>
        /// <param name="keyLevel1"></param>
        /// <param name="keyLevel2"></param>
        /// <param name="keyLevel3"></param>
        /// <returns></returns>
        public object this[string keyLevel1, string keyLevel2, string keyLevel3]
        {
            get => Get(keyLevel1, keyLevel2, keyLevel3);
            set => Set(keyLevel1, keyLevel2, keyLevel3, value);
        }

        /// <summary>
        /// Converts a configuration value to another type
        /// </summary>
        /// <param name="value"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        [Obsolete("Use object.ToType instead")]
        public object ConvertValue(object value, Type destinationType) => value.ToType(destinationType);

        /// <summary>
        /// Converts a configuration value to another type and returns it as that type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        [Obsolete("Use object.ToType instead")]
        public T ConvertValue<T>(object value) => (T)ConvertValue(value, typeof(T));

        /// <summary>
        /// Gets a configuration value at the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public object Get(params string[] path)
        {
            if (path.Length < 1)
            {
                throw new ArgumentException("path must not be empty");
            }

            if (!_keyvalues.TryGetValue(path[0], out object val))
            {
                return null;
            }

            for (int i = 1; i < path.Length; i++)
            {
                if (!(val is Dictionary<string, object> dict) || !dict.TryGetValue(path[i], out val))
                {
                    return null;
                }
            }

            return val;
        }

        /// <summary>
        /// Gets a configuration value at the specified path and converts it to the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public T Get<T>(params string[] path)
        {
            object val = Get(path);
            return val == null ? default : val.ToType<T>();
        }

        /// <summary>
        /// Sets a configuration value at the specified path
        /// </summary>
        /// <param name="pathAndTrailingValue"></param>
        public void Set(params object[] pathAndTrailingValue)
        {
            if (pathAndTrailingValue.Length < 2)
            {
                throw new ArgumentException("path must not be empty");
            }

            string[] path = new string[pathAndTrailingValue.Length - 1];
            for (int i = 0; i < pathAndTrailingValue.Length - 1; i++)
            {
                path[i] = (string)pathAndTrailingValue[i];
            }

            object value = pathAndTrailingValue[pathAndTrailingValue.Length - 1];
            if (path.Length == 1)
            {
                _keyvalues[path[0]] = value;
                return;
            }

            if (!_keyvalues.TryGetValue(path[0], out object val))
            {
                _keyvalues[path[0]] = val = new Dictionary<string, object>();
            }

            for (int i = 1; i < path.Length - 1; i++)
            {
                if (!(val is Dictionary<string, object>))
                {
                    throw new ArgumentException("path is not a dictionary");
                }

                Dictionary<string, object> oldVal = (Dictionary<string, object>)val;
                if (!oldVal.TryGetValue(path[i], out val))
                {
                    oldVal[path[i]] = val = new Dictionary<string, object>();
                }
            }
            ((Dictionary<string, object>)val)[path[path.Length - 1]] = value;
        }

        #region IEnumerable

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator() => _keyvalues.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => _keyvalues.GetEnumerator();

        #endregion IEnumerable
    }
}
