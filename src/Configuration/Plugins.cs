﻿using uMod.Common;
using uMod.Configuration.Toml;
using uMod.IO;

namespace uMod.Configuration
{
    public class PluginCommandConfiguration : IPluginCommandConfiguration
    {
        [TomlProperty("chat_command_prefixes", Comment = "Prefixes used to trigger chat commands")]
        public char[] ChatCommandPrefixes { get; }= { '/', '!' };

        [TomlProperty("unknown_command_replies", Comment = "Enable/disable showing if command is not found")]
        public bool UnknownCommands { get; } = true;

        [TomlProperty("denied_command_replies", Comment = "Enable/disable showing if command is denied by a gate or permission")]
        public bool DeniedCommands { get; } = true;
    }

    public class PluginSecurityConfiguration : IPluginSecurityConfiguration
    {
        [TomlProperty("sandbox_security", Comment = "Enable/disable sandboxing of plugins for security")]
        public bool Sandbox { get; } = true;
    }

    public class PluginWatcherConfiguration : IPluginWatcherConfiguration
    {
        [TomlProperty("config_watchers", Comment = "Enable/disable automatic configuration file reloading")]
        public bool ConfigWatchers { get; } = false;

        [TomlProperty("plugin_watchers", Comment = "Enable/disable automatic plugin loading/reloading")]
        public bool PluginWatchers { get; } = true;

        [TomlProperty("plugin_directories", Comment = "List of directories to load plugins from")]
        public string[] PluginDirectories { get; } = { "universal" };
    }

    public class Plugins : TomlFile, IPluginConfiguration
    {
        [TomlProperty("commands", DefaultType = typeof(PluginCommandConfiguration))]
        public IPluginCommandConfiguration Commands { get; }

        [TomlProperty("security", DefaultType = typeof(PluginSecurityConfiguration))]
        public IPluginSecurityConfiguration Security { get; }

        [TomlProperty("watchers", DefaultType = typeof(PluginWatcherConfiguration))]
        public IPluginWatcherConfiguration Watchers { get; }

        public Plugins(string filename) : base(filename)
        {
            Commands = new PluginCommandConfiguration();
            Security = new PluginSecurityConfiguration();
            Watchers = new PluginWatcherConfiguration();
        }
    }
}
