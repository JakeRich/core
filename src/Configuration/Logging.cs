﻿using System;
using System.Collections.Generic;
using uMod.Common;
using uMod.Configuration.Toml;
using uMod.IO;
using uMod.Logging;
using uMod.Utilities;

namespace uMod.Configuration
{
    /// <summary>
    /// Log channel interface
    /// </summary>
    public interface ILogChannel
    {
        /// <summary>
        /// Gets the log channel driver
        /// </summary>
        [TomlTypeAlias("log_channel")]
        [TomlProperty("driver")]
        string Driver { get; }

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        string Level { get; }

        /// <summary>
        /// Gets the logger associated with this channel
        /// </summary>
        [TomlIgnore]
        ILogger Logger { get; }

        /// <summary>
        /// Configure logger using configuration
        /// </summary>
        /// <param name="logger"></param>
        bool Configure(ILogger logger);
    }

    /// <summary>
    /// Text log channel interface
    /// </summary>
    public interface ITextLogChannel : ILogChannel
    {
        /// <summary>
        /// Gets the format of log messages
        /// </summary>
        [TomlProperty("format")]
        string Format { get; }
    }

    /// <summary>
    /// Represents a base log channel
    /// </summary>
    public abstract class BaseChannel : ILogChannel
    {
        /// <summary>
        /// Gets the log channel driver
        /// </summary>
        public abstract string Driver { get; }

        /// <summary>
        /// Gets the log level
        /// </summary>
        public abstract string Level { get; }

        /// <summary>
        /// Gets the logger associated with this channel
        /// </summary>
        [TomlIgnore]
        public ILogger Logger { get; private set; }

        /// <summary>
        /// Configure logger using configuration
        /// </summary>
        /// <param name="logger"></param>
        public virtual bool Configure(ILogger logger)
        {
            Logger = logger;
            if (logger is Logger loggerImpl)
            {
                if (Interface.uMod.IsTesting)
                {
                    loggerImpl.LogLevel = LogLevel.Debug;
                }
                else if (Level != null)
                {
                    loggerImpl.LogLevel = (LogLevel)Enum.Parse(typeof(LogLevel), Level, true);
                }
            }

            return true;
        }
    }

    /// <summary>
    /// Represents a text log channel
    /// </summary>
    public abstract class TextChannel : BaseChannel, ITextLogChannel
    {
        /// <summary>
        /// Gets the format of log messages
        /// </summary>
        public abstract string Format { get; }

        /// <summary>
        /// Configure logger using configuration
        /// </summary>
        /// <param name="logger"></param>
        public override bool Configure(ILogger logger)
        {
            base.Configure(logger);

            if (logger is TextLogger textLogger)
            {
                textLogger.Format = Format ?? Interface.uMod.Logging.Format;
            }

            return true;
        }
    }

    /// <summary>
    /// Represents a stack channel
    /// </summary>
    [TomlComment("Aggregate multiple log channels into a single channel")]
    [Logger("stack")]
    public class StackChannel : BaseChannel
    {
        /// <summary>
        /// Gets the list of channels
        /// </summary>
        [TomlProperty("channels")]
        public List<string> Channels;

        /// <summary>
        /// Gets the driver "stack"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "stack";

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        public override string Level { get; } = "info";
    }

    /// <summary>
    /// Represents a single file channel
    /// </summary>
    [TomlComment("Single file log channel")]
    [Logger("single")]
    public class SingleFileChannel : TextChannel
    {
        /// <summary>
        /// Gets the driver "single"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "single";

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        public override string Level { get; } = "info";

        /// <summary>
        /// Gets the format of log messages
        /// </summary>
        [TomlProperty("format")]
        public override string Format { get; }

        /// <summary>
        /// Gets the path of the log file
        /// </summary>
        [TomlProperty("path")]
        public string Path { get; } = "umod.log";

        /// <summary>
        /// Configure logger using configuration
        /// </summary>
        /// <param name="logger"></param>
        public override bool Configure(ILogger logger)
        {
            base.Configure(logger);
            if (logger is SingleFileLogger singleFileLogger)
            {
                singleFileLogger.Path = Path;
            }

            return true;
        }
    }

    /// <summary>
    /// Represents a daily file channel
    /// </summary>
    [TomlComment("Multiple file log channel which rotates daily")]
    [Logger("daily")]
    public class DailyFileChannel : TextChannel
    {
        /// <summary>
        /// Gets the driver "daily"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "daily";

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        public override string Level { get; } = "info";

        /// <summary>
        /// Gets the format of log message
        /// </summary>
        [TomlProperty("format")]
        public override string Format { get; }

        /// <summary>
        /// Gets the path of the log file
        /// </summary>
        [TomlProperty("path")]
        public string Path { get; } = "umod_{date|yyyy-MM-dd}.log";

        /// <summary>
        /// Configure logger using configuration
        /// </summary>
        /// <param name="logger"></param>
        public override bool Configure(ILogger logger)
        {
            base.Configure(logger);
            if (logger is DailyFileLogger rotatingFileLogger)
            {
                rotatingFileLogger.Path = Path;
            }

            return true;
        }
    }

    /// <summary>
    /// Represents a native (callback) log channel
    /// </summary>
    [TomlComment("Native log channel")]
    [Logger("native")]
    public class NativeChannel : TextChannel
    {
        /// <summary>
        /// Gets the driver "native"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "native";

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        public override string Level { get; } = "info";

        /// <summary>
        /// Gets the format of log message
        /// </summary>
        [TomlProperty("format")]
        public override string Format { get; }
    }

    /// <summary>
    /// Represents a console log channel
    /// </summary>
    [TomlComment("Console log channel")]
    [Logger("console")]
    public class ConsoleChannel : TextChannel
    {
        /// <summary>
        /// Gets the driver "console"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "console";

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        public override string Level { get; } = "info";

        /// <summary>
        /// Gets the format of log messages
        /// </summary>
        [TomlProperty("format")]
        public override string Format { get; } = "{date|h:mm tt} [{level}] {message}";
    }

    /// <summary>
    /// Represents a unity log channel
    /// </summary>
    [TomlComment("Unity console log channel")]
    [Logger("unity")]
    public class UnityChannel : ConsoleChannel
    {
        /// <summary>
        /// Gets the driver "unity"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "unity";

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        public override string Level { get; } = "info";
    }

    /// <summary>
    /// Represents a remote sentry error channel
    /// </summary>
    [TomlComment("Sentry remote error channel")]
    [Logger("sentry")]
    public class SentryChannel : BaseChannel
    {
        /// <summary>
        /// Gets the driver "native"
        /// </summary>
        [TomlProperty("driver")]
        public override string Driver { get; } = "sentry";

        /// <summary>
        /// Gets the log level
        /// </summary>
        [TomlProperty("level")]
        public override string Level { get; }

        /// <summary>
        /// Gets the project id
        /// </summary>
        [TomlProperty("project")]
        public string ProjectId { get; }

        /// <summary>
        /// Gets the sentry key
        /// </summary>
        [TomlProperty("key")]
        public string Key { get; }

        /// <summary>
        /// Gets the sentry api uri
        /// </summary>
        [TomlProperty("uri")]
        public string Uri { get; }

        public override bool Configure(ILogger logger)
        {
            base.Configure(logger);
            if (!(logger is SentryLogger sentryLogger))
            {
                return false;
            }

            if (!Interface.uMod.Telemetry.Reporting.Exceptions)
            {
                return false;
            }

#if DEBUG
            sentryLogger.Configure(
                !string.IsNullOrEmpty(Uri) ? Uri : "https://sentry.io",
                !string.IsNullOrEmpty(ProjectId) ? ProjectId : "5178213",
                !string.IsNullOrEmpty(Key) ? Key : "fbc6d153527847eabb63bc1f41a81123"
            );
#else
            sentryLogger.Configure(
                !string.IsNullOrEmpty(Uri) ? Uri : "https://sentry.io",
                !string.IsNullOrEmpty(ProjectId) ? ProjectId : "141692",
                !string.IsNullOrEmpty(Key) ? Key : "2d0162c790be4036a94d2d8326d7f900"
            );
#endif

            return true;
        }
    }

    /// <summary>
    /// Represents the logging configuration
    /// </summary>
    public class Logging : TomlFile
    {
        /// <summary>
        /// Gets the default configuration driver
        /// </summary>
        [TomlProperty("default")]
        public string Default { get; }

        /// <summary>
        /// Gets the default log message format
        /// </summary>
        [TomlProperty("format")]
        public string Format { get; } = "[uMod] {date|h:mm tt} [{level}] {message}";

        /// <summary>
        /// Gets the configured log channels
        /// </summary>
        [TomlProperty("channels")]
        [TomlAggregate]
        public Dictionary<string, ILogChannel> Channels;

        /// <summary>
        /// Runtime switch to disable file logging
        /// </summary>
        [TomlIgnore]
        public bool FileLogging = true;

        /// <summary>
        /// Determine if logging is initialized
        /// </summary>
        [TomlIgnore]
        public bool LoggingInitialized;

        private readonly IApplication _application;

        /// <summary>
        /// Create new Logging class
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="application"></param>
        public Logging(string filename, IApplication application) : base(filename)
        {
            _application = application;
            Default = "stack";

            List<string> defaultChannels = new List<string>()
            {
                "daily",
                "sentry"
            };

            if (Interface.uMod.debugCallback != null)
            {
                defaultChannels.Add("native");
            }

            switch (Interface.uMod.GameEngine)
            {
                case GameEngine.Unity:
                    defaultChannels.Add("unity");
                    break;

                default:
                    defaultChannels.Add("console");
                    break;
            }

            Channels = new Dictionary<string, ILogChannel>()
            {
                ["stack"] = new StackChannel()
                {
                    Channels = defaultChannels
                },
                ["daily"] = new DailyFileChannel(),
                ["single"] = new SingleFileChannel(),
                ["sentry"] = new SentryChannel()
            };

            if (Interface.uMod.debugCallback != null)
            {
                Channels.Add("native", new NativeChannel());
            }

            switch (Interface.uMod.GameEngine)
            {
                case GameEngine.Unity:
                    Channels.Add("unity", new UnityChannel());
                    break;

                default:
                    Channels.Add("console", new ConsoleChannel());
                    break;
            }

            Interface.uMod.OnLoggingInitialized.Add(delegate
            {
                LoggingInitialized = true;
            });
        }

        /// <summary>
        /// Resolve all ILogger and ILogChannels from core and extensions
        /// </summary>
        internal static void ResolveLoggerTypes(IApplication application, TypeManager types)
        {
            IEnumerable<Type> loggerTypes = types.Resolve<ILogger>();
            foreach (Type loggerType in loggerTypes)
            {
                if (loggerType.IsAbstract || loggerType.IsInterface)
                {
                    continue;
                }

                if (loggerType.GetCustomAttribute<LoggerAttribute>() is LoggerAttribute loggerAttribute)
                {
                    if (!application.Resolved(loggerType))
                    {
                        application.Resolve(loggerType);
                        application.Alias($"{loggerAttribute.Name}logger", loggerType);
                    }
                }
            }

            IEnumerable<Type> loggerChannelTypes = types.Resolve<ILogChannel>();
            foreach (Type channelType in loggerChannelTypes)
            {
                if (channelType.IsAbstract || channelType.IsInterface)
                {
                    continue;
                }

                if (channelType.GetCustomAttribute<LoggerAttribute>() is LoggerAttribute loggerAttribute)
                {
                    if (!application.Resolved(channelType))
                    {
                        application.Resolve(channelType);
                        application.Alias($"{loggerAttribute.Name}log_channel", channelType);
                    }
                }
            }
        }

        /// <summary>
        /// Initialize root logger
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="application"></param>
        /// <param name="fileSystemLogging"></param>
        /// <returns></returns>
        internal IPromise<ILogger> Initialize(ILogger logger, IApplication application, bool fileSystemLogging = true)
        {
            Promise<ILogger> promise = new Promise<ILogger>();
            FileLogging = fileSystemLogging;
            PipeLogger pipeLogger = logger as PipeLogger;

            if (Channels.TryGetValue(Default, out ILogChannel defaultChannel))
            {
                if (!application.TryGetType($"{defaultChannel.Driver}logger", out Type defaultLogType))
                {
                    defaultLogType = typeof(StackLogger);
                }

                ILogger defaultLogger = (ILogger)Activator.CreateInstance(defaultLogType);
                if (!FileLogging && defaultLogger is IFileLogger)
                {
                    promise.Resolve(new EmptyLogger());
                    return promise;
                }

                defaultChannel.Configure(defaultLogger);
                defaultLogger.OnConfigure?.Invoke(defaultLogger);
                if (defaultLogger is StackLogger compoundLogger && defaultChannel is StackChannel stackChannel)
                {
                    InitializeStack(compoundLogger, stackChannel.Channels);
                    if (!ValidateLogger(defaultLogger, new List<ILogger>()))
                    {
                        Interface.uMod.LogError("Unable to validate logger tree.  Stack loggers and/or pipe loggers cannot be recursive."); // TODO: Localization
                    }
                }
                else
                {
                    if (!ValidateLogger(defaultLogger, new List<ILogger>()))
                    {
                        Interface.uMod.LogError("Unable to validate logger tree.  Stack loggers and/or pipe loggers cannot be recursive."); // TODO: Localization
                    }
                    defaultLogger.OnAdded?.Invoke(defaultLogger);
                }

                // Unity logger is delayed, requires separate initialization
                if (Interface.uMod.GameEngine == GameEngine.Unity && !LoggingInitialized)
                {
                    Interface.uMod.OnLoggingInitialized.Add(delegate
                    {
                        InitializeRootLogger(defaultLogger, pipeLogger);
                        promise.Resolve(defaultLogger);
                    });
                }
                else
                {
                    InitializeRootLogger(defaultLogger, pipeLogger);
                    promise.Resolve(defaultLogger);
                }
            }
            else
            {
                promise.Resolve(new EmptyLogger());
            }

            return promise;
        }

        /// <summary>
        /// Ensures logger tree is not recursive
        /// </summary>
        /// <param name="rootLogger"></param>
        /// <param name="loggers"></param>
        /// <returns></returns>
        private bool ValidateLogger(ILogger rootLogger, ICollection<ILogger> loggers)
        {
            if (rootLogger is StackLogger stackLogger)
            {
                // Do not allow a stack logger to refer to itself in a logger tree
                if (loggers.Contains(rootLogger))
                {
                    return false;
                }

                loggers.Add(rootLogger);

                bool anyFalse = false;
                foreach (ILogger logger in stackLogger.GetLoggers())
                {
                    if (!ValidateLogger(logger, loggers))
                    {
                        anyFalse = true;
                    }
                }

                return !anyFalse;
            }
            if (rootLogger is PipeLogger pipeLogger)
            {
                // Do not allow a pipe logger to refer to itself in a logger tree
                if (loggers.Contains(pipeLogger))
                {
                    return false;
                }

                loggers.Add(rootLogger);

                return ValidateLogger(pipeLogger.GetTargetLogger(), loggers);
            }
            
            loggers.Add(rootLogger);

            return true;
        }

        /// <summary>
        /// Initialize root logger
        /// </summary>
        /// <param name="defaultLogger"></param>
        /// <param name="pipeLogger"></param>
        private void InitializeRootLogger(ILogger defaultLogger, PipeLogger pipeLogger = null)
        {
            LoggingInitialized = true;
            pipeLogger?.Pipe(defaultLogger);
        }

        /// <summary>
        /// Initialize stack logger
        /// </summary>
        /// <param name="stackLogger"></param>
        /// <param name="channels"></param>
        private void InitializeStack(StackLogger stackLogger, IEnumerable<string> channels)
        {
            foreach (string channelName in channels)
            {
                ILogger logger = CreateLogger(channelName);
                if (logger != null)
                {
                    stackLogger.AddLogger(logger);
                }
            }
        }

        /// <summary>
        /// Initialize logger using specified log chnnale
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        internal ILogger CreateLogger(string channelName)
        {
            if (Channels.TryGetValue(channelName, out ILogChannel channel) && _application.TryGetType($"{channel.Driver}logger", out Type channelType))
            {
                ILogger logger = (ILogger)Activator.CreateInstance(channelType);
                if (!FileLogging && logger is IFileLogger)
                {
                    return null;
                }

                if (channel.Configure(logger))
                {
                    logger.OnConfigure?.Invoke(logger);
                    if (logger is StackLogger subStackLogger && channel is StackChannel stackChannel)
                    {
                        InitializeStack(subStackLogger, stackChannel.Channels);
                    }

                    return logger;
                }
            }

            return null;
        }
    }
}
