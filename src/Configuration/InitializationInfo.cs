﻿using System;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.Configuration
{
    /// <summary>
    /// Represents initialization information
    /// </summary>
    public class InitializationInfo : IInitializationInfo
    {
        /// <summary>
        /// Gets or sets applications to initialize
        /// </summary>
        public string[] Applications { get; set; }

        /// <summary>
        /// Gets or sets environment variables
        /// </summary>
        public IDictionary<string, string> Environment { get; set; }

        /// <summary>
        /// Gets or sets the extension types to load initially
        /// </summary>
        public Type[] Extensions { get; set; }
    }
}
