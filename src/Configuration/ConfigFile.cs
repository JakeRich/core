extern alias References;

using System;
using System.IO;
using References::Newtonsoft.Json;
using uMod.Common;
using uMod.IO;

namespace uMod.Configuration
{
    /// <summary>
    /// Represents a config file
    /// </summary>
    public abstract class ConfigFile
    {
        [JsonIgnore]
        public string Filename { get; }

        private readonly string _chroot;

        public IEvent<ConfigFile> OnRead { get; } = new Event<ConfigFile>();

        public IEvent<ConfigFile> OnWrite { get; } = new Event<ConfigFile>();

        protected ConfigFile(string filename)
        {
            Filename = filename;
            _chroot = Interface.uMod.InstanceDirectory;
        }

        /// <summary>
        /// Loads a config from the specified file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="createDefault"></param>
        public static T Load<T>(string filename, bool createDefault = true) where T : ConfigFile
        {
            T config = Interface.uMod.Application.MakeParams<T>(typeof(T), filename);
            //T config = (T)Activator.CreateInstance(typeof(T), filename);
            if (File.Exists(filename))
            {
                config.Load();
            }
            else
            {
                if (createDefault)
                {
                    config.Save();
                }
                else
                {
                    throw new FileNotFoundException("Configuration file does not exist", filename);
                }
            }

            return config;
        }

        /// <summary>
        /// Loads this config from the specified file
        /// </summary>
        /// <param name="filename"></param>
        public virtual void Load(string filename = null)
        {
            filename = CheckPath(filename ?? Filename);
            string source = File.ReadAllText(filename);
            JsonConvert.PopulateObject(source, this);
            OnRead?.Invoke(this);
        }

        /// <summary>
        /// Loads this config from the specified file
        /// </summary>
        /// <param name="filename"></param>
        public virtual IPromise<ConfigFile> LoadAsync(string filename = null)
        {
            Promise<ConfigFile> promise = new Promise<ConfigFile>();
            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    filename = CheckPath(filename ?? Filename);
                    string source = File.ReadAllText(filename);
                    OnRead?.Invoke(this);

                    Populate(source);
                    promise.Resolve(this);
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Populate the configuration file with the data from the specified JSON string
        /// </summary>
        /// <param name="source"></param>
        internal void Populate(string source)
        {
            JsonConvert.PopulateObject(source, this);
        }

        /// <summary>
        /// Saves this config to the specified file
        /// </summary>
        /// <param name="filename"></param>
        public virtual void Save(string filename = null)
        {
            filename = CheckPath(filename ?? Filename);
            if (Interface.uMod?.Plugins?.Configuration != null && Interface.uMod.Plugins.Configuration.Watchers.ConfigWatchers)
            {
                Interface.uMod.ConfigChanges.Add(filename);
            }
            string source = JsonConvert.SerializeObject(this, Formatting.Indented);

            File.WriteAllText(filename, source);
            OnWrite?.Invoke(this);
        }

        /// <summary>
        /// Saves this config to the specified file asynchronously
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public virtual IPromise<object> SaveAsync(string filename = null)
        {
            Promise<object> promise = new Promise<object>();

            Interface.uMod.FileDispatcher.Dispatch(delegate
            {
                try
                {
                    filename = CheckPath(filename ?? Filename);
                    if (Interface.uMod?.Plugins?.Configuration != null && Interface.uMod.Plugins.Configuration.Watchers.ConfigWatchers)
                    {
                        Interface.uMod.ConfigChanges.Add(filename);
                    }

                    string source = JsonConvert.SerializeObject(this, Formatting.Indented);

                    File.WriteAllText(filename, source);
                    OnWrite?.Invoke(this);
                    promise.Resolve(this);
                }
                catch (Exception exception)
                {
                    promise.Reject(exception);
                }

                return true;
            });

            return promise;
        }

        /// <summary>
        /// Check if file path is in chroot directory
        /// </summary>
        /// <param name="filename"></param>
        internal string CheckPath(string filename)
        {
            filename = PathUtils.Sanitize(filename);
            string path = Path.GetFullPath(filename);
            if (!path.StartsWith(_chroot, StringComparison.Ordinal))
            {
                throw new Exception($"Only access to 'umod' directory!\nPath: {path}");
            }

            return path;
        }

        /// <summary>
        /// Makes the specified name safe for use in a filename
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use PathUtils.Sanitize instead")]
        public static string SanitizeName(string name)
        {
            return PathUtils.Sanitize(name);
        }
    }
}
