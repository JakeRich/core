﻿using uMod.IO;

namespace uMod.Configuration
{
    public class Web : TomlFile
    {
        public float DefaultTimeout;
        public string PreferredEndpoint;

        /// <summary>
        /// Sets defaults for the web configuration
        /// </summary>
        public Web(string filename) : base(filename)
        {
            DefaultTimeout = 30f;
            PreferredEndpoint = string.Empty;
        }
    }
}
