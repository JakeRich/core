﻿using System;
using System.Threading;
using uMod.Common;

namespace uMod.Threading
{
    /// <summary>
    /// Multi-threaded dispatcher
    /// </summary>
    internal class StackDispatcher : IChainDispatcher, ISingleton
    {
        /// <summary>
        /// Main thread
        /// </summary>
        private readonly Dispatcher _mainDispatcher;

        /// <summary>
        /// Backup thread
        /// </summary>
        private readonly Dispatcher _backupDispatcher;

        /// <summary>
        /// Create a new instance of the StackDispatcher class
        /// </summary>
        /// <param name="logger"></param>
        public StackDispatcher(ILogger logger)
        {
            _mainDispatcher = new Dispatcher(logger);
            _backupDispatcher = new Dispatcher(logger);
        }

        /// <summary>
        /// Dispatch the specified callback with specified lock
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="scopeLock"></param>
        public void Dispatch(Func<bool> callback, ref object scopeLock)
        {
            if (Thread.CurrentThread == _mainDispatcher.Thread)
            {
                _backupDispatcher.Dispatch(callback, ref scopeLock);
                return;
            }

            _mainDispatcher.Dispatch(callback, ref scopeLock);
        }

        /// <summary>
        /// Dispatch the specified callback
        /// </summary>
        /// <param name="callback"></param>
        public void Dispatch(Func<bool> callback)
        {
            if (Thread.CurrentThread == _mainDispatcher.Thread)
            {
                _backupDispatcher.Dispatch(callback);
                return;
            }

            _mainDispatcher.Dispatch(callback);
        }

        /// <summary>
        /// Stop threads
        /// </summary>
        public void Stop()
        {
            try
            {
                _mainDispatcher.Stop();
                _backupDispatcher.Stop();
            }
            catch (Exception)
            {
            }
        }
    }
}
