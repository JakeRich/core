using System;
using System.Collections.Generic;
using System.Threading;
using uMod.Common;

namespace uMod.Threading
{
    /// <summary>
    /// Threaded callback dispatcher
    /// </summary>
    public sealed class Dispatcher : IChainDispatcher, ISingleton
    {
        private readonly Queue<Func<bool>> _dispatchQueue;
        private readonly ILogger _dispatchLogger;

        // Sync mechanisms
        private readonly AutoResetEvent _waitEvent;
        private readonly object _syncRoot;
        private bool _exit;

        // The worker thread
        internal readonly Thread Thread;

        /// <summary>
        /// Gets the number of actions in queue
        /// </summary>
        public int Count
        {
            get
            {
                lock (_syncRoot)
                {
                    return _dispatchQueue?.Count ?? 0;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ThreadedLogger class
        /// </summary>
        public Dispatcher(ILogger logger)
        {
            _dispatchLogger = logger;
            _dispatchQueue = new Queue<Func<bool>>();
            // Initialize
            _waitEvent = new AutoResetEvent(false);
            _exit = false;
            _syncRoot = new object();

            // Create the thread
            Thread = new Thread(Worker) { IsBackground = true };
            Thread.Start();
        }

        /// <summary>
        /// Destructor to ensure thread is destroyed
        /// </summary>
        ~Dispatcher()
        {
            Stop();
        }

        /// <summary>
        /// Dispatches a callback asynchronously (with a lock)
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="scopeLock"></param>
        public void Dispatch(Func<bool> callback, ref object scopeLock)
        {
            if (Thread.CurrentThread == Thread)
            {
                callback.Invoke();
                return;
            }

            lock (scopeLock)
            {
                lock (_syncRoot)
                {
                    _dispatchQueue.Enqueue(callback);
                }
            }

            _waitEvent.Set();
        }

        /// <summary>
        /// Dispatches a callback asynchronously
        /// </summary>
        /// <param name="callback"></param>
        public void Dispatch(Func<bool> callback)
        {
            lock (_syncRoot)
            {
                _dispatchQueue.Enqueue(callback);
            }

            _waitEvent.Set();
        }

        public void Stop()
        {
            if (_exit)
            {
                return;
            }
            _exit = true;
            _waitEvent.Set();
            Thread.Join();
        }

        /// <summary>
        /// The worker thread
        /// </summary>
        private void Worker()
        {
            // Loop until it is time to exit
            while (!_exit)
            {
                // Wait for signal
                _waitEvent.WaitOne();

                if (Count <= 0)
                {
                    continue;
                }

                Flush();
            }
        }

        /// <summary>
        /// Flush worker queue
        /// </summary>
        public void Flush()
        {
            // Iterate each item in the queue
            while (Count > 0)
            {
                Func<bool> callback;
                lock (_syncRoot)
                {
                    // Dequeue
                    callback = _dispatchQueue.Dequeue();
                }

                // Process
                try
                {
                    callback?.Invoke();
                }
                catch (Exception ex)
                {
                    _dispatchLogger?.Report("Dispatch Failure", ex);
                }
            }
        }
    }
}
