﻿using System.Globalization;
using System.Threading;

namespace uMod.Threading
{
    public static class MainThreadState
    {
        /// <summary>
        /// Main thread
        /// </summary>
        internal static readonly Thread MainThread = Thread.CurrentThread;

        /// <summary>
        /// Determine if current thread is main thread
        /// </summary>
        public static bool IsMain => MainThread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId;

        /// <summary>
        /// Set culture settings for thread and JSON handling
        /// </summary>
        internal static void Initialize()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        }
    }
}
