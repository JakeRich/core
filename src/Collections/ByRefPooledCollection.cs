﻿using System.Collections.Generic;

namespace uMod.Collections
{
    /// <summary>
    /// A wrapper for a collection reference with additional callbacks to handle pooling
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public abstract class ByRefPooledCollection<TValue> : ByRefCollection<TValue>
    {
        /// <summary>
        /// Create a new ByRef pooled collection object (without reference)
        /// </summary>
        protected ByRefPooledCollection() { }

        /// <summary>
        /// Create a new ByRef pooled collection object with the specified collection reference
        /// </summary>
        /// <param name="collection"></param>
        protected ByRefPooledCollection(ref ICollection<TValue> collection) : base(ref collection)
        {
            _poolCallback = Pool;

            _freeCallback = delegate (ref List<TValue> c2)
            {
                Free(ref c2);
            };
        }

        private delegate List<TValue> PoolDelegate();

        private delegate void FreeDelegate(ref List<TValue> collection);

        private delegate void ScopeDelegate();

        private PoolDelegate _poolCallback;
        private FreeDelegate _freeCallback;
        private ScopeDelegate _scopeCallback;

        /// <summary>
        /// Get a pooled list
        /// </summary>
        /// <returns></returns>
        protected abstract List<TValue> Pool();

        /// <summary>
        /// Free the specified list from the associated pool
        /// </summary>
        /// <param name="val"></param>
        protected abstract void Free(ref List<TValue> val);

        /// <summary>
        /// Get an enumerator for the collection that performs additional pooling operations
        /// </summary>
        /// <returns></returns>
        public override IEnumerator<TValue> GetEnumerator()
        {
            List<TValue> poolImpl = Pool();
            _scopeCallback?.Invoke();
            Apply(ref poolImpl);
            IEnumerator<TValue> e = poolImpl?.GetEnumerator();
            while (e != null && e.MoveNext())
            {
                yield return e.Current;
            }

            Free(ref poolImpl);
        }

        protected abstract void Apply(ref List<TValue> items);
    }
}
