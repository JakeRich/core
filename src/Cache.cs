using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;

namespace uMod
{
    public struct CacheItem<TKey, TValue>
    {
        public TKey Name;
        public TValue Value;
        private DateTime _expireTime;

        /// <summary>
        /// Number of minutes until cache item expires
        /// </summary>
        public int ExpireMinutes
        {
            get
            {
                TimeSpan span = DateTime.Now - _expireTime;
                return (int)span.TotalMinutes;
            }
            set => _expireTime = value > 0 ? DateTime.Now.AddMinutes(value) : DateTime.MinValue;
        }

        /// <summary>
        /// Create cache item object
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expires"></param>
        public CacheItem(TKey key, TValue value, DateTime expires) : this(key, value)
        {
            _expireTime = expires;
        }

        /// <summary>
        /// Create cache item object
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expires"></param>
        public CacheItem(TKey key, TValue value, int expires = 0)
        {
            Name = key;
            Value = value;
            _expireTime = expires > 0 ? DateTime.Now.AddMinutes(expires) : DateTime.MinValue;
        }

        /// <summary>
        /// Determine if cache item has expired
        /// </summary>
        public bool IsExpired
        {
            get
            {
                if (_expireTime == DateTime.MinValue)
                {
                    return false;
                }

                if (DateTime.Now > _expireTime)
                {
                    return true;
                }

                return false;
            }
        }
    }

    /// <summary>
    /// Generic cache
    /// </summary>
    public class Cache<TKey, TValue> : ICache<TKey, TValue>
    {
        private readonly int _capacity;

        private object _cacheLock = new object();

        private Dictionary<TKey, CacheItem<TKey, TValue>> _baseDictionary = new Dictionary<TKey, CacheItem<TKey, TValue>>();

        public int Count => _baseDictionary.Count;

        /// <summary>
        /// Create cache object with optional limit (uses limit specified in uMod configuration if 0)
        /// </summary>
        /// <param name="maxLimit"></param>
        public Cache(int maxLimit = 0)
        {
            _capacity = maxLimit > 0 ? maxLimit : 10000;
        }

        /// <summary>
        /// Get cache item
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public TValue this[TKey name]
        {
            get
            {
                lock (_cacheLock)
                {
                    if (_baseDictionary.TryGetValue(name, out CacheItem<TKey, TValue> cachedItem) && !cachedItem.IsExpired)
                    {
                        return cachedItem.Value;
                    }
                }

                return default;
            }
            private set
            {
                _baseDictionary[name] = new CacheItem<TKey, TValue>(name, value);
            }
        }

        /// <summary>
        /// Add cache item to cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expires"></param>
        public void Add(TKey key, TValue value, int expires = 0)
        {
            lock (_cacheLock)
            {
                if (_baseDictionary.TryGetValue(key, out CacheItem<TKey, TValue> cachedItem))
                {
                    cachedItem.Value = value;
                    cachedItem.ExpireMinutes = expires;
                }
                else
                {
                    _baseDictionary.Add(key, new CacheItem<TKey, TValue>(key, value, expires));
                }

                if (_baseDictionary.Count > _capacity)
                {
                    Purge();
                }
            }
        }

#if DEBUG
        public void Add(CacheItem<TKey, TValue> cacheItem)
        {
            _baseDictionary.Add(cacheItem.Name, cacheItem);
        }
#endif

        /// <summary>
        /// Purge expired items from cache
        /// Prevents memory leak by purging cache items over maximum limit
        /// Additionally purges one of the following depending on which is higher
        /// - 10% of the Capacity
        /// - 2x the number items that the current cache is exceeding the threshold
        /// Prioritizes expired items
        /// </summary>
        internal void Purge()
        {
            int distance = _baseDictionary.Count - _capacity;

            int expiryPercent = _capacity * 10 / 100;
            int newDistance = 0;
            if ((newDistance = distance * 2) > expiryPercent)
            {
                distance = expiryPercent;
            }
            else
            {
                distance = newDistance;
            }

            if (distance > 0)
            {
                _baseDictionary = _baseDictionary.OrderByDescending(x => x.Value.IsExpired).Skip(distance).ToDictionary(x => x.Key, x => x.Value);
            }
        }

        /// <summary>
        /// Purge expired items from cache asynchronously
        /// </summary>
        internal void PurgeAsync()
        {
            Interface.uMod.Dispatcher.Dispatch(delegate
            {
                Purge();
                return true;
            }, ref _cacheLock);
        }

        /// <summary>
        /// Determine if cache contains cache item
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(TKey key)
        {
            lock (_cacheLock)
            {
                if (_baseDictionary.TryGetValue(key, out CacheItem<TKey, TValue> cachedItem) && !cachedItem.IsExpired)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Try to get cache item from cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (_cacheLock)
            {
                if (_baseDictionary.TryGetValue(key, out CacheItem<TKey, TValue> cachedItem) && !cachedItem.IsExpired)
                {
                    value = cachedItem.Value;
                    return true;
                }
            }

            value = default;
            return false;
        }

        /// <summary>
        /// Clear all items from cache
        /// </summary>
        public void Clear()
        {
            lock (_cacheLock)
            {
                _baseDictionary.Clear();
            }
        }

        public void Forget(TKey key)
        {
            lock (_cacheLock)
            {
                _baseDictionary.Remove(key);
            }
        }
    }
}
