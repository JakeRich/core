﻿using System;

namespace uMod.Exceptions
{
    public class ExtensionDirectoryNotFound : Exception
    {
        public ExtensionDirectoryNotFound()
        {
        }

        public ExtensionDirectoryNotFound(string message) : base(message)
        {
        }

        public ExtensionDirectoryNotFound(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
