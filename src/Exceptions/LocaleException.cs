﻿using System;

namespace uMod.Exceptions
{
    public class LocaleException : Exception
    {
        public LocaleException()
        {
        }

        public LocaleException(string message) : base(message)
        {
        }

        public LocaleException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
