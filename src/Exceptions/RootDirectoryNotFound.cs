﻿using System;

namespace uMod.Exceptions
{
    public class RootDirectoryNotFound : Exception
    {
        public RootDirectoryNotFound()
        {
        }

        public RootDirectoryNotFound(string message) : base(message)
        {
        }

        public RootDirectoryNotFound(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
