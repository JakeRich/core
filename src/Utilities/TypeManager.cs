﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using uMod.Common;

namespace uMod.Utilities
{
    internal class TypeManager
    {
        public TypeResolver Resolver;
        internal Assembly TestAssembly;
        private bool _resolvedState;
        private bool _testMode;

        public TypeManager(ILogger logger)
        {
            Resolver = new TypeResolver(logger);
            Resolver.ResolveCandidateAssemblies();
        }

        public IEnumerable<Type> Resolve<T>()
        {
            return Resolver.ResolveCandidateTypes<T>();
        }

        public void AddAssembly(Assembly assembly)
        {
            Resolver.AddCandidateAssembly(assembly);
        }

        /// <summary>
        /// Determine if uMod is being run from unit tests
        /// </summary>
        public bool IsTesting
        {
            get
            {
                if (_resolvedState)
                {
                    return _testMode;
                }

                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
#if !NET35
                    if (assembly.IsDynamic)
                    {
                        continue;
                    }
#endif
                    if (string.IsNullOrEmpty(assembly.Location))
                    {
                        continue;
                    }

                    string baseName = Path.GetFileName(assembly.Location);
                    if (baseName.StartsWith("uMod.Tests.dll"))
                    {
                        TestAssembly = assembly;
                        _testMode = true;
                        break;
                    }
                }

                _resolvedState = true;
                return _testMode;
            }
        }
    }
}
