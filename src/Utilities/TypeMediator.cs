using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;

namespace uMod.Utilities
{
    /// <summary>
    /// Mediates type within context
    /// </summary>
    internal class TypeMediator
    {
        private readonly IApplication _application;
        private readonly IContext _context;
        private readonly List<Type> _mediatedTypes;

        /// <summary>
        /// Create a TypeMediator object
        /// </summary>
        /// <param name="context"></param>
        /// <param name="application"></param>
        public TypeMediator(IContext context, IApplication application)
        {
            _context = context;
            _application = application;
            _mediatedTypes = new List<Type>();
        }

        /// <summary>
        /// Get mediated type array with matching prefix from mediator
        /// </summary>
        /// <param name="prefix"></param>
        public Type[] GetTypeArray(string prefix)
        {
            return _mediatedTypes.Where(x => x.FullName != null && x.FullName.StartsWith(prefix)).ToArray();
        }

        /// <summary>
        /// Get mediated type enumerable with matching prefix from mediator
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public IEnumerable<Type> GetTypes(string prefix)
        {
            return _mediatedTypes.Where(x => x.FullName != null && x.FullName.StartsWith(prefix));
        }

        /// <summary>
        /// Add generic type to mediator
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void Mediate<T>()
        {
            Mediate(typeof(T));
        }

        /// <summary>
        /// Add specified type to mediator
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        public void Mediate(Type type, string name = null)
        {
            if (!_mediatedTypes.Contains(type))
            {
                _mediatedTypes.Add(type);
                _application.Resolve(type);
            }

            if (!string.IsNullOrEmpty(name))
            {
                _application.Alias($"{_context.Name}.{name}", type);
            }
        }

        /// <summary>
        /// Add specified types to mediator
        /// </summary>
        /// <param name="types"></param>
        public void Mediate(IEnumerable<Type> types)
        {
            foreach (Type type in types)
            {
                if (_mediatedTypes.Contains(type))
                {
                    continue;
                }

                _mediatedTypes.Add(type);
                _application.Resolve(type);
            }
        }

        /// <summary>
        /// Determine if generic type is mediated
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool IsMediating<T>()
        {
            return IsMediating(typeof(T));
        }

        /// <summary>
        /// Determine if specified type is mediated
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsMediating(Type type)
        {
            return _mediatedTypes.Contains(type);
        }

        /// <summary>
        /// Unbind all types mediated by mediator
        /// </summary>
        public void Unbind()
        {
            foreach (Type type in _mediatedTypes)
            {
                _application.Unbind(type);
            }
        }

        /// <summary>
        /// Try to get types associated with plugin
        /// </summary>
        /// <param name="name"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        internal bool TryGetTypes(string name, out IEnumerable<Type> types)
        {
            return _application.TryGetTypes($"{_context.Name}.{name}", out types);
        }

        /// <summary>
        /// Try to get object associated with type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        internal bool TryGetObject(Type type, out object @object)
        {
            return _application.TryGetObject(type, out @object);
        }

        /// <summary>
        /// Create provider binding scope
        /// </summary>
        /// <returns></returns>
        internal IBindingScope When()
        {
            return _application.When(_context.GetType());
        }
    }
}
