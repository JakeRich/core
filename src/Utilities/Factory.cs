﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Utilities
{
    /// <summary>
    /// ProviderFactory class
    /// </summary>
    public class ProviderFactory : BaseFactory
    {
        /// <summary>
        /// Check if first type is equal or assignable to second type
        /// </summary>
        private class TypeComparer : IEqualityComparer<Type>
        {
            /// <summary>
            /// Check if first type is equal or assignable to second type
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            public bool Equals(Type x, Type y)
            {
                if (x == y) return true;
                if (y != null && y.IsAssignableFrom(x)) return true;

                return false;
            }

            /// <summary>
            /// Returns hash code for this instance
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public int GetHashCode(Type obj) => obj.GetHashCode();
        }

        private static readonly TypeComparer typeComparer = new TypeComparer();

        /// <summary>
        /// Make specified type using specified constructor arguments
        /// </summary>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public object Make(Type type, object[] arguments = null)
        {
            return Make<object>(type, arguments);
        }

        /// <summary>
        /// Make specified type using specified constructor arguments
        /// </summary>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public object MakeParams(Type type, params object[] arguments)
        {
            return Make<object>(type, arguments);
        }

        /// <summary>
        /// Make object of generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public override TResult Make<TResult>()
        {
            return Make<TResult>(typeof(TResult));
        }

        /// <summary>
        /// Find constructor that has the closest matching parameters that exclude injected types
        /// </summary>
        /// <param name="type"></param>
        /// <param name="argumentTypes"></param>
        /// <returns></returns>
        private ConstructorInfo FindClosestConstructor(Type type, Type[] argumentTypes = null)
        {
            if (argumentTypes == null)
            {
                return type.GetConstructor(Type.EmptyTypes);
            }

            var constructors = type.GetConstructors();

            foreach (var constructor in constructors)
            {
                List<Type> parameterList = new List<Type>(constructor.GetParameters().Select(x => x.ParameterType));

                while (parameterList.Count > 0)
                {
                    if (argumentTypes.SequenceEqual(parameterList, typeComparer))
                    {
                        return constructor;
                    }
                    parameterList.RemoveAt(parameterList.Count - 1);
                }
            }

            string typeList = string.Join(",", argumentTypes.Select(x => x.Name).ToArray());
            throw new ArgumentException($"No constructor found for type: {type.Name} ({typeList})");
        }

        /// <summary>
        /// Make object of specified type using specified constructor arguments with additional cast to generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public override TResult Make<TResult>(Type type, object[] arguments = null)
        {
            if (type.IsGenericTypeDefinition)
            {
                return (TResult)Activator.CreateInstance(GetGenericType(type, arguments), arguments);
            }

            Type[] argumentTypes = null;
            if (arguments != null)
            {
                argumentTypes = arguments.Select(x => x.GetType()).ToArray();
            }

            ConstructorInfo constructor = FindClosestConstructor(type, argumentTypes);
            ParameterInfo[] parameters = constructor?.GetParameters();

            // allows DI use in constructors
            object[] providerArgs = null;

            int argLength = 0;
            if (arguments != null)
            {
                argLength = arguments.Length;
            }

            if (parameters?.Length > 0)
            {
                if (parameters.Length != argLength)
                {
                    providerArgs = ArrayPool.Get(parameters.Length);
                    int start = argLength;

                    if (arguments != null)
                    {
                        for (int i = 0; i < start; i++)
                        {
                            providerArgs[i] = arguments[i];
                        }
                    }

                    for (int i = start; i < parameters.Length; i++)
                    {
                        object arg = null;
                        if (argLength > 0 && arguments?.Length > i)
                        {
                            arg = arguments[i];
                        }

                        ParameterInfo parameter = parameters[i];
                        if (parameter.DefaultValue != null && parameter.DefaultValue != DBNull.Value)
                        {
                            // Use the default value that was provided by the method definition
                            providerArgs[i] = parameter.DefaultValue;
                        }
                        else if (parameter.ParameterType.IsValueType)
                        {
                            // Use the default value for value types
                            providerArgs[i] = Activator.CreateInstance(parameter.ParameterType);
                        }

                        if (Interface.uMod.Application.TryGetSingleton(parameter.ParameterType, out object object2))
                        {
                            providerArgs[i] = object2;
                        }
                        else if (Interface.uMod.Application.TryInject(parameter.ParameterType, type.DeclaringType, arg, out object param))
                        {
                            providerArgs[i] = param;
                        }
                    }
                }
                else
                {
                    providerArgs = arguments;
                }
            }

            try
            {
                if (providerArgs != null)
                {
                    object @object = Activator.CreateInstance(type, providerArgs);
                    ArrayPool.Free(providerArgs);
                    return (TResult)@object;
                }

                if (constructor != null)
                {
                    return (TResult)Activator.CreateInstance(type, null);
                }

                return (TResult)FormatterServices.GetUninitializedObject(type);
            }
            catch (InvalidCastException castException)
            {
                Interface.uMod.LogException($"Unable to cast \"{type.FullName}\" to \"{typeof(TResult).FullName}\"", castException);
            }

            return default;
        }

        /// <summary>
        /// Get generic type from specified type using specified constructor arguments
        /// </summary>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        internal Type GetGenericType(Type type, object[] arguments)
        {
            if (type != null && type.IsGenericTypeDefinition)
            {
                if (type.ContainsGenericParameters)
                {
                    Type[] genericArgTypes = type.GetGenericTypeDefinition().GetGenericArguments();
                    Type[] splicedArgTypes = TypePool.Get(genericArgTypes.Length);

                    try
                    {
                        if (arguments != null)
                        {
                            Type[] argTypes = arguments.Where(x => x != null).Select(x => x.GetType()).ToArray();

                            for (int i = 0; i < genericArgTypes.Length; i++)
                            {
                                object obj;

                                if (genericArgTypes[i] == typeof(string) || genericArgTypes[i].IsValueType)
                                {
                                    continue;
                                }

                                bool skip = false;

                                // search arguments for type
                                foreach (Type argType in argTypes)
                                {
                                    if (argType == typeof(string) || argType.IsValueType)
                                    {
                                        continue;
                                    }

                                    if (genericArgTypes[i] == argType || genericArgTypes[i].IsAssignableFrom(argType))
                                    {
                                        splicedArgTypes[i] = argType;
                                        skip = true;
                                        break;
                                    }

                                    // search argument parameter constraints
                                    foreach (Type parameterConstraint in genericArgTypes[i].GetGenericParameterConstraints())
                                    {
                                        if (parameterConstraint.IsAssignableFrom(argType))
                                        {
                                            splicedArgTypes[i] = argType;
                                            skip = true;
                                            continue;
                                        }

                                        if (parameterConstraint.IsAssignableFrom(argType.DeclaringType))
                                        {
                                            splicedArgTypes[i] = argType.DeclaringType;
                                            skip = true;
                                            continue;
                                        }

                                        if (!Interface.uMod.Application.TryGetSingleton(parameterConstraint, out obj))
                                        {
                                            continue;
                                        }

                                        splicedArgTypes[i] = obj.GetType();
                                        skip = true;
                                    }
                                }

                                if (skip)
                                {
                                    continue;
                                }

                                if (genericArgTypes[i].IsAssignableFrom(type))
                                {
                                    splicedArgTypes[i] = type;
                                    continue;
                                }

                                if (genericArgTypes[i].IsAssignableFrom(type.DeclaringType))
                                {
                                    splicedArgTypes[i] = type.DeclaringType;
                                    continue;
                                }

                                if (Interface.uMod.Application.TryGetSingleton(genericArgTypes[i].GetType(), out obj))
                                {
                                    splicedArgTypes[i] = obj.GetType();
                                    continue;
                                }

                                foreach (Type parameterConstraint in genericArgTypes[i].GetGenericParameterConstraints())
                                {
                                    if (parameterConstraint.IsAssignableFrom(type))
                                    {
                                        splicedArgTypes[i] = type;
                                        continue;
                                    }

                                    if (parameterConstraint.IsAssignableFrom(type.DeclaringType))
                                    {
                                        splicedArgTypes[i] = type.DeclaringType;
                                        continue;
                                    }

                                    if (Interface.uMod.Application.TryGetSingleton(parameterConstraint, out obj))
                                    {
                                        splicedArgTypes[i] = obj.GetType();
                                    }
                                }
                            }
                        }

                        type = type.MakeGenericType(splicedArgTypes);
                    }
                    finally
                    {
                        TypePool.Free(splicedArgTypes);
                    }
                }
                else
                {
                    type = type.MakeGenericType();
                }
            }

            return type;
        }
    }

    /// <summary>
    /// ActivatorFactory class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ActivatorFactory<T> : GenericFactory<T>
    {
        /// <summary>
        /// Make object of generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override T Make<T>()
        {
            return Make<T>(typeof(T));
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public override T Make(object[] arguments = null)
        {
            return Make<T>(typeof(T), arguments);
        }

        /// <summary>
        /// Make object of specified type using specified constructor arguments with generic type cast
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public override TResult Make<TResult>(Type type, object[] arguments = null)
        {
            if (arguments == null)
            {
                return (TResult)Activator.CreateInstance(type);
            }

            return (TResult)Activator.CreateInstance(type, arguments);
        }
    }

    /// <summary>
    /// A generic object activator
    /// </summary>
    public class ActivatorFactory : BaseFactory
    {
        private readonly Type _activatedType;

        /// <summary>
        /// Create a new ActivatorFactory instance
        /// </summary>
        /// <param name="type"></param>
        public ActivatorFactory(Type type)
        {
            _activatedType = type;
        }

        /// <summary>
        /// Instantiate an object of the specified generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public TResult Activate<TResult>()
        {
            return (TResult)Activator.CreateInstance(_activatedType);
        }

        /// <summary>
        /// Instantiate an object of the specified type with the specified arguments
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public override TResult Make<TResult>(Type type, object[] arguments = null)
        {
            if (arguments == null)
            {
                return (TResult)Activator.CreateInstance(type);
            }

            return (TResult)Activator.CreateInstance(type, arguments);
        }

        /// <summary>
        /// Instantiate an object of the specified generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public override TResult Make<TResult>()
        {
            return (TResult)Activator.CreateInstance(_activatedType);
        }
    }

    /// <summary>
    /// BaseFactory class
    /// </summary>
    public abstract class BaseFactory : IFactory
    {
        /// <summary>
        /// Make an object that implements the generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public TResult Make<TResult>(object[] arguments = null)
        {
            return Make<TResult>(typeof(TResult), arguments);
        }

        /// <summary>
        /// Make an object that implements the generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public TResult MakeParams<TResult>(params object[] arguments)
        {
            return Make<TResult>(typeof(TResult), arguments);
        }

        /// <summary>
        /// Make an object that implements the specified type and cast it to the generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public TResult MakeParams<TResult>(Type type, params object[] arguments)
        {
            return Make<TResult>(type, arguments);
        }

        /// <summary>
        /// Make object of specified type using specified constructor arguments with generic type cast
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public abstract TResult Make<TResult>(Type type, object[] arguments = null);

        /// <summary>
        /// Make object of generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public abstract TResult Make<TResult>();

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <param name="arg"></param>
        /// <returns></returns>
        public TResult Make<TResult, TArg1>(TArg1 arg)
        {
            object[] args = ArrayPool.Get(1);
            args[0] = arg;

            TResult @object = Make<TResult>(typeof(TResult), args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public TResult Make<TResult, TArg1, TArg2>(TArg1 arg, TArg2 arg2)
        {
            object[] args = ArrayPool.Get(2);
            args[0] = arg;
            args[1] = arg2;

            TResult @object = Make<TResult>(typeof(TResult), args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public TResult Make<TResult, TArg1, TArg2, TArg3>(TArg1 arg, TArg2 arg2, TArg3 arg3)
        {
            object[] args = ArrayPool.Get(3);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;

            TResult @object = Make<TResult>(typeof(TResult), args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public TResult Make<TResult, TArg1, TArg2, TArg3, TArg4>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            object[] args = ArrayPool.Get(4);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;
            args[3] = arg4;

            TResult @object = Make<TResult>(typeof(TResult), args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public TResult Make<TResult, TArg1, TArg2, TArg3, TArg4, TArg5>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            object[] args = ArrayPool.Get(5);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;
            args[3] = arg4;
            args[4] = arg5;

            TResult @object = Make<TResult>(typeof(TResult), args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <typeparam name="TArg6"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <param name="arg6"></param>
        /// <returns></returns>
        public TResult Make<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6)
        {
            object[] args = ArrayPool.Get(6);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;
            args[3] = arg4;
            args[4] = arg5;
            args[5] = arg6;

            TResult @object = Make<TResult>(typeof(TResult), args);
            ArrayPool.Free(args);
            return @object;
        }
    }

    /// <summary>
    /// GenericFactory class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class GenericFactory<T> : BaseFactory, IFactory<T>
    {
        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public abstract T Make(object[] arguments = null);

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <param name="arg"></param>
        /// <returns></returns>
        public T Make<TArg1>(TArg1 arg)
        {
            object[] args = ArrayPool.Get(1);
            args[0] = arg;

            T @object = Make(args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public T Make<TArg1, TArg2>(TArg1 arg, TArg2 arg2)
        {
            object[] args = ArrayPool.Get(2);
            args[0] = arg;
            args[1] = arg2;

            T @object = Make(args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public T Make<TArg1, TArg2, TArg3>(TArg1 arg, TArg2 arg2, TArg3 arg3)
        {
            object[] args = ArrayPool.Get(3);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;

            T @object = Make(args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        public T Make<TArg1, TArg2, TArg3, TArg4>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            object[] args = ArrayPool.Get(4);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;
            args[3] = arg4;

            T @object = Make(args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <returns></returns>
        public T Make<TArg1, TArg2, TArg3, TArg4, TArg5>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            object[] args = ArrayPool.Get(5);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;
            args[3] = arg4;
            args[4] = arg5;

            T @object = Make(args);
            ArrayPool.Free(args);
            return @object;
        }

        /// <summary>
        /// Make object of generic type using specified constructor arguments
        /// </summary>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <typeparam name="TArg3"></typeparam>
        /// <typeparam name="TArg4"></typeparam>
        /// <typeparam name="TArg5"></typeparam>
        /// <typeparam name="TArg6"></typeparam>
        /// <param name="arg"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <param name="arg5"></param>
        /// <param name="arg6"></param>
        /// <returns></returns>
        public T Make<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(TArg1 arg, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5, TArg6 arg6)
        {
            object[] args = ArrayPool.Get(6);
            args[0] = arg;
            args[1] = arg2;
            args[2] = arg3;
            args[3] = arg4;
            args[4] = arg5;
            args[5] = arg6;

            T @object = Make(args);
            ArrayPool.Free(args);
            return @object;
        }
    }
}
