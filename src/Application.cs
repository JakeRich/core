using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using uMod.Common;
using uMod.Pooling;
using uMod.Utilities;

namespace uMod
{
    /// <summary>
    /// Global application container for uMod
    /// </summary>
    public sealed class Application : Container, IApplication
    {
        /// <summary>
        /// Create application object
        /// </summary>
#if DEBUG
        public Application()
#else
        internal Application()
#endif
        {
            Context = new ContextContainer(this);
            BindingManager = Context.Bindings;
            ContextMapping = Context.Mapping;
            DependencyBinder = Context.Binder;
            SingletonProvider = Context.SingletonProvider;
            ObjectProvider = Context.ObjectProvider;

            Boot();
        }

        /// <summary>
        /// Determine if application contains a binding scope
        /// </summary>
        /// <param name="contextType"></param>
        /// <returns></returns>
        public bool ContainsScope(Type contextType)
        {
            return BindingManager.BindingScopes.ContainsKey(contextType);
        }

        /// <summary>
        /// Try to get binding scopes associated with the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="scopes"></param>
        /// <returns></returns>
        internal bool TryGetScope(Type type, out List<BindingScope> scopes)
        {
            return BindingManager.BindingScopes.TryGetValue(type, out scopes);
        }

        /// <summary>
        /// Try to get binding scopes associated with the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="scopes"></param>
        /// <returns></returns>
        internal bool TryGetScope<T>(out List<BindingScope> scopes)
        {
            return TryGetScope(typeof(T), out scopes);
        }

        /// <summary>
        /// "Inject" an object associated with the generic type and context
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="in"></param>
        /// <param name="out"></param>
        /// <returns></returns>
        public bool TryInject<T>(object context, object @in, out object @out)
        {
            if (SingletonProvider == null)
            {
                @out = null;
                return false;
            }

            if (SingletonProvider.Instances.TryGetValue(typeof(T), out @out))
            {
                return true;
            }

            @out = DependencyBinder.UnpackAny<T>(context, @in);
            return @out != null;
        }

        /// <summary>
        /// "Inject" an object associated with the specified type and context
        /// </summary>
        /// <param name="type"></param>
        /// <param name="context"></param>
        /// <param name="in"></param>
        /// <param name="out"></param>
        /// <returns></returns>
        public bool TryInject(Type type, object context, object @in, out object @out)
        {
            if (SingletonProvider == null)
            {
                @out = null;
                return false;
            }

            if (SingletonProvider.Instances.TryGetValue(type, out @out))
            {
                return true;
            }

            @out = DependencyBinder.UnpackAny(type, context, @in);

            return @out != null;
        }

        /// <summary>
        /// Create provider binding scope
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IBindingScope When<T>()
        {
            return When(typeof(T));
        }

        /// <summary>
        /// Create provider binding scope
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public IBindingScope When(params Type[] types)
        {
            return BindingManager.When(types);
        }

        /// <summary>
        /// Add a type converter from the specified type to another specified type with an optional converter function
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="converterFunc"></param>
        public override void AddConverter(Type from, Type to, Func<object, object> converterFunc = null)
        {
            base.AddConverter(from, to, converterFunc);
            if (converterFunc != null)
            {
                When().Needs(to).Bind(converterFunc);
            }
        }
    }

    /// <summary>
    /// Helper class to store mappings of all provider objects for quick access
    /// </summary>
    public class ContextMapping
    {
        private readonly ContextContainer _container;
        internal Dictionary<Type, IObjectContainer> ReverseContainers = new Dictionary<Type, IObjectContainer>();
        internal Dictionary<string, Type> ReverseTypes = new Dictionary<string, Type>();
        internal Dictionary<string, Type> ReverseGenericTypes = new Dictionary<string, Type>();
        internal Dictionary<Type, List<string>> ReverseAliases = new Dictionary<Type, List<string>>();

        /// <summary>
        /// Create a new instance of the ContextMapping class
        /// </summary>
        /// <param name="provider"></param>
        public ContextMapping(ContextContainer provider)
        {
            _container = provider;
        }
    }

    /// <summary>
    /// Generic container to provide objects and singletons
    /// </summary>
    public class ContextContainer : BaseContainer, IContainer
    {
        internal readonly DependencyBinder Binder;
        internal BindingManager Bindings;

        /// <summary>
        /// Gets the singleton provider
        /// </summary>
        internal SingletonContainer SingletonProvider;

        private readonly HashSet<IContextContainer> _providers = new HashSet<IContextContainer>();

        /// <summary>
        /// Gets the object provider
        /// </summary>
        public ObjectContainer ObjectProvider { get; }

        /// <summary>
        /// Gets the context mapping
        /// </summary>
        public ContextMapping Mapping { get; }

        /// <summary>
        /// Create a new instance of the ContextContainer class
        /// </summary>
        /// <param name="application"></param>
        public ContextContainer(Application application) : base(application)
        {
            Bindings = new BindingManager(application);
            Binder = new DependencyBinder(Bindings);
            Mapping = new ContextMapping(this);
            SingletonProvider = new SingletonContainer(application);
            ObjectProvider = new ObjectContainer(application);

            _providers.Add(SingletonProvider);
            _providers.Add(ObjectProvider);
        }

        /// <summary>
        /// "Resolve" a type, adding it to the list of known types
        /// </summary>
        /// <param name="type"></param>
        public void Resolve(Type type)
        {
            string name = Utility.ReflectedName(type.FullName);
            if (type.IsGenericType)
            {
                if (!Mapping.ReverseGenericTypes.ContainsKey(name))
                {
                    Mapping.ReverseGenericTypes.Add(name, type);
                }
            }
            else
            {
                if (!Mapping.ReverseTypes.ContainsKey(name))
                {
                    Mapping.ReverseTypes.Add(name, type);
                }
            }
        }

        /// <summary>
        /// Create an alias for the specified type
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public void Alias(string name, Type type)
        {
            if (Resolved(type))
            {
                if (type.IsGenericType)
                {
                    if (Mapping.ReverseGenericTypes.ContainsKey(name))
                    {
                        Mapping.ReverseGenericTypes.Remove(name);
                    }
                    Mapping.ReverseGenericTypes.Add(name, type);
                }
                else
                {
                    if (Mapping.ReverseTypes.ContainsKey(name))
                    {
                        Mapping.ReverseTypes.Remove(name);
                    }

                    Mapping.ReverseTypes.Add(name, type);
                }

                if (Mapping.ReverseAliases.TryGetValue(type, out List<string> aliases))
                {
                    aliases.Add(name);
                }
                else
                {
                    Mapping.ReverseAliases.Add(type, new List<string> { name });
                }
            }
        }

        /// <summary>
        /// Try to get a type from the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool TryGetType(string name, out Type type)
        {
            if (Mapping.ReverseGenericTypes.TryGetValue(name, out type))
            {
                return true;
            }

            return Mapping.ReverseTypes.TryGetValue(name, out type);
        }

        /// <summary>
        /// Try to get types using the specified pattern
        /// </summary>
        /// <param name="name"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public bool TryGetTypes(string name, out IEnumerable<Type> types)
        {
            name = name.Replace(@"\", @"\\");
            name = name.Replace(".", @"\.");
            name = name.Replace("+", @"\+");
            name = name.Replace("?", ".");
            name = name.Replace("*", ".*?");
            name = name.Replace(" ", @"\s");

            List<Type> result = new List<Type>();

            Regex regex = new Regex(name, RegexOptions.IgnoreCase);

            result.AddRange(Mapping.ReverseGenericTypes.Where(x => regex.IsMatch(x.Key)).Select(x => x.Value));
            result.AddRange(Mapping.ReverseTypes.Where(x => regex.IsMatch(x.Key)).Select(x => x.Value));

            types = result;
            return result.Count > 0;
        }

        /// <summary>
        /// Determine if the generic type is resolved
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override bool Resolved<T>()
        {
            return Resolved(typeof(T));
        }

        /// <summary>
        /// Determine if the specified type is resolved
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool Resolved(Type type)
        {
            return Mapping.ReverseGenericTypes.ContainsValue(type) || Mapping.ReverseTypes.ContainsValue(type) || Mapping.ReverseContainers.ContainsKey(type);
        }

        /// <summary>
        /// Determine if the specified alias or type name is resolved
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public override bool Resolved(string typeName)
        {
            return Mapping.ReverseTypes.ContainsKey(typeName) || Mapping.ReverseGenericTypes.ContainsKey(typeName);
        }

        /// <summary>
        /// Determine if the specified type and object is resolved
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Resolved(Type type, object @object)
        {
            if (Mapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider))
            {
                return provider.Resolved(type, @object);
            }

            return false;
        }

        /// <summary>
        /// Determine if the specified object's type is resolved
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Resolved(object @object)
        {
            return Resolved(@object.GetType(), @object);
        }

        /// <summary>
        /// Bind the specified object to the container keyed by the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override object Bind(Type type, object @object)
        {
            if (Mapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider))
            {
                provider.Bind(type, @object);
                return @object;
            }

            if (@object is ISingleton)
            {
                SingletonProvider.Bind(type, @object);
            }
            else
            {
                ObjectProvider.Bind(type, @object);
            }

            return @object;
        }

        /// <summary>
        /// Bind the specified object to the container keyed by the object's type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public override T Bind<T>(T @object)
        {
            return Bind(@object.GetType(), @object);
        }

        /// <summary>
        /// Bind the specified object to the container keyed by the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public T Bind<T>(Type type, T @object)
        {
            if (Mapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider))
            {
                provider.Bind(type, @object);
                return @object;
            }

            if (@object is ISingleton)
            {
                SingletonProvider.Bind(type, @object);
            }
            else
            {
                ObjectProvider.Bind(type, @object);
            }

            return @object;
        }

        /// <summary>
        /// Bind the specified objects to the container keyed by the object's type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public override IEnumerable<T> Bind<T>(IEnumerable<T> objects)
        {
            if (Mapping.ReverseContainers.TryGetValue(typeof(T), out IObjectContainer provider))
            {
                provider.Bind(typeof(T), objects);
            }

            return objects;
        }

        /// <summary>
        /// Unbind the specified object from the container when keyed by the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind(Type type, object @object)
        {
            if (Mapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider))
            {
                return provider.Unbind(type, @object);
            }

            return false;
        }

        /// <summary>
        /// Unbind multiple objects from the container that implement the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public override bool Unbind(Type type)
        {
            // Unbind container
            if (Mapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider))
            {
                return provider.Unbind(type);
            }

            // Unbind non-generic types
            if (Mapping.ReverseTypes.ContainsValue(type))
            {
                UnbindReverseType(type, Mapping.ReverseTypes);
            }

            // Unbind generic types
            if (Mapping.ReverseGenericTypes.ContainsValue(type))
            {
                UnbindReverseType(type, Mapping.ReverseGenericTypes);
            }

            // Unbind converters
            if (Converters.ContainsKey(type))
            {
                Converters.Remove(type);
            }

            // Unbind binding scopes
            if (Bindings.BindingScopes.ContainsKey(type))
            {
                Bindings.BindingScopes.Remove(type);
            }

            return false;
        }

        /// <summary>
        /// Unbind type and type aliases
        /// </summary>
        /// <param name="type"></param>
        /// <param name="mapping"></param>
        private void UnbindReverseType(Type type, Dictionary<string, Type> mapping)
        {
            string reflectedName = Utility.ReflectedName(type.FullName);
            mapping.Remove(reflectedName);

            if (Mapping.ReverseAliases.TryGetValue(type, out List<string> aliases))
            {
                foreach (string alias in aliases)
                {
                    if (mapping.ContainsKey(alias))
                    {
                        mapping.Remove(alias);
                    }
                }

                Mapping.ReverseAliases.Remove(type);
            }
        }

        /// <summary>
        /// Unbind the specified object from the container
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind(object @object)
        {
            return Unbind(@object.GetType(), @object);
        }

        /// <summary>
        /// Unbind multiple objects from the container when keyed by the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public override bool Unbind<T>(IEnumerable<T> objects)
        {
            if (Mapping.ReverseContainers.TryGetValue(typeof(T), out IObjectContainer provider))
            {
                return provider.Unbind(objects);
            }

            return false;
        }

        /// <summary>
        /// Unbind all objects from the container that implement the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override bool Unbind<T>()
        {
            return Unbind(typeof(T));
        }

        /// <summary>
        /// Unbind the specified object from the container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public override bool Unbind<T>(T @object)
        {
            return Unbind(typeof(T), @object);
        }

        #region IDisposable Support

        /// <summary>
        /// Dispose of all providers provided by the container
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!DisposedValue)
            {
                if (disposing)
                {
                    foreach (IContextContainer provider in _providers)
                    {
                        provider.Dispose();
                    }
                }

                DisposedValue = true;
            }
        }

        /// <summary>
        /// Dispose of container
        /// </summary>
        public override void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Get all providers provided by the container
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IContextContainer> GetContainers()
        {
            return _providers;
        }

        /// <summary>
        /// Register a provider with the container
        /// </summary>
        /// <param name="provider"></param>
        public void Register(IContextContainer provider)
        {
            if (!_providers.Contains(provider))
            {
                _providers.Add(provider);
            }
        }

        /// <summary>
        /// Unregister the specified provider from the container
        /// </summary>
        /// <param name="provider"></param>
        public virtual void Unregister(IContextContainer provider)
        {
            if (_providers.Contains(provider))
            {
                _providers.Remove(provider);
                provider.Unregister();
            }
        }

        /// <summary>
        /// Unregister all providers from the container
        /// </summary>
        public override void Unregister()
        {
            List<IContextContainer> scheduleRemoval = new List<IContextContainer>();
            foreach (IContextContainer provider in _providers)
            {
                try
                {
                    scheduleRemoval.Add(provider);
                    provider.Unregister();
                }
                finally
                {
                }
            }

            if (scheduleRemoval.Count > 0)
            {
                foreach (IContextContainer provider in scheduleRemoval)
                {
                    _providers.Remove(provider);
                }
            }

            base.Unregister();
        }

        /// <summary>
        /// Boot the container
        /// </summary>
        public virtual void Boot()
        {
        }

        #endregion IDisposable Support
    }

    /// <summary>
    /// Generic container to wrap a context container which provides objects and singletons
    /// </summary>
    public class Container : BaseFactory, IContainer
    {
        internal ContextContainer Context;
        internal readonly ProviderFactory Factory = new ProviderFactory();

        /// <summary>
        /// Gets the Added event
        /// </summary>
        public IEvent<Type, object> Added { get; } = new Event<Type, object>();

        /// <summary>
        /// Gets the Removed event
        /// </summary>
        public IEvent<Type, object> Removed { get; } = new Event<Type, object>();

        /// <summary>
        /// Gets the AddedType event
        /// </summary>
        public IEvent<Type> AddedType { get; } = new Event<Type>();

        /// <summary>
        /// Gets the RemovedType event
        /// </summary>
        public IEvent<Type> RemovedType { get; } = new Event<Type>();

        private bool _disposedValue; // To detect redundant calls

        private ICallback<Type, object> _addedCallback;
        private ICallback<Type, object> _removedCallback;

        internal SingletonContainer SingletonProvider;
        internal BindingManager BindingManager;
        internal DependencyBinder DependencyBinder;
        internal ObjectContainer ObjectProvider;
        internal ContextMapping ContextMapping;

        /// <summary>
        /// Create a new instance of the Container class
        /// </summary>
        protected Container()
        {
        }

        /// <summary>
        /// Create a new instance of the Container class with the specified Application
        /// </summary>
        /// <param name="application"></param>
        public Container(Application application)
        {
            Context = new ContextContainer(application);
            SingletonProvider = Context.SingletonProvider;
            ObjectProvider = Context.ObjectProvider;
            ContextMapping = Context.Mapping;
            BindingManager = Context.Bindings;
            DependencyBinder = Context.Binder;
            Boot();
        }

        /// <summary>
        /// Mark the specified type as resolved
        /// </summary>
        /// <param name="type"></param>
        public void Resolve(Type type)
        {
            Context.Resolve(type);
        }

        /// <summary>
        /// Mark the specified types as resolved
        /// </summary>
        /// <param name="types"></param>
        public void Resolve(IEnumerable<Type> types)
        {
            foreach (Type type in types)
            {
                Context.Resolve(type);
            }
        }

        /// <summary>
        /// Alias the specified type with the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public void Alias(string name, Type type)
        {
            Context.Alias(name, type);
        }

        /// <summary>
        /// Boot the container
        /// </summary>
        public virtual void Boot()
        {
            Bind(typeof(IApplication), this);
        }

        /// <summary>
        /// Register the specified provider with the container
        /// </summary>
        /// <param name="provider"></param>
        public virtual void Register(IContextContainer provider)
        {
            Context.Register(provider);
        }

        /// <summary>
        /// Unregister the specified provider with the container
        /// </summary>
        /// <param name="provider"></param>
        public virtual void Unregister(IContextContainer provider)
        {
            Context.Unregister(provider);
        }

        /// <summary>
        /// Unregister all providers from the container
        /// </summary>
        public virtual void Unregister()
        {
            Context.Unregister();

            if (_addedCallback != null)
            {
                Event.Remove(ref _addedCallback);
            }

            if (_removedCallback != null)
            {
                Event.Remove(ref _removedCallback);
            }
        }

        /// <summary>
        /// Add a converter to the container
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="converterFunc"></param>
        public virtual void AddConverter(Type from, Type to, Func<object, object> converterFunc = null)
        {
            SingletonProvider.AddConverter(from, to, converterFunc);
        }

        /// <summary>
        /// Add a converter to the container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="converterFunc"></param>
        public void AddConverter<T, T1>(Func<T, T1> converterFunc = null)
        {
            AddConverter(typeof(T), typeof(T1), arg1 => converterFunc.Invoke((T)arg1));
        }

        /// <summary>
        /// Determine if the container providers a converter from one specified type to the other specified type
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public virtual bool Converts(Type from, Type to)
        {
            return SingletonProvider.Converts(from, to);
        }

        /// <summary>
        /// Try to get a type with the specified name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool TryGetType(string name, out Type type)
        {
            return Context.TryGetType(name, out type);
        }

        /// <summary>
        /// Try to get multiple types that match the specified search string
        /// </summary>
        /// <param name="search"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public bool TryGetTypes(string search, out IEnumerable<Type> types)
        {
            return Context.TryGetTypes(search, out types);
        }

        /// <summary>
        /// Try to get an object with the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryGetObject(Type type, out object @object)
        {
            @object = null;

            if (SingletonProvider.TryGetObject(type, out @object))
            {
                return true;
            }

            if (ContextMapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider) &&
                provider.TryGetObjects(type, out IEnumerable<object> objects))
            {
                @object = objects.First();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to get a provided object with the specified type, with an additional generic conversion
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryGetObject<T>(Type type, out T @object)
        {
            if (TryGetObject(type, out object objectValue))
            {
                @object = (T)objectValue;
                return true;
            }

            @object = default;
            return false;
        }

        /// <summary>
        /// Try to get a provided object with the specified generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryGetObject<T>(out T @object)
        {
            return TryGetObject(typeof(T), out @object);
        }

        /// <summary>
        /// Try to get all provided objects with the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public bool TryGetObject(Type type, out IEnumerable<object> objects)
        {
            objects = null;

            if (ContextMapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider) && provider.TryGetObjects(type, out objects))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to get all provided objects with the specified type, with an additional generic type cast
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public bool TryGetObjects<T>(Type type, out IEnumerable<T> objects)
        {
            if (TryGetObject(type, out IEnumerable<object> objectValues))
            {
                objects = objectValues.Cast<T>();
                return true;
            }

            objects = null;
            return false;
        }

        /// <summary>
        /// Try to get all provided objects with the specified generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public bool TryGetObjects<T>(out IEnumerable<T> objects)
        {
            return TryGetObjects(typeof(T), out objects);
        }

        /// <summary>
        /// Try to get a singleton object with the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="singleton"></param>
        /// <returns></returns>
        public bool TryGetSingleton(Type type, out object singleton)
        {
            return SingletonProvider.TryGetObject(type, out singleton);
        }

        /// <summary>
        /// Try to get a singleton object with the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="singleton"></param>
        /// <returns></returns>
        public bool TryGetSingleton<T>(Type type, out T singleton) where T : class
        {
            return SingletonProvider.TryGetObject(type, out singleton);
        }

        /// <summary>
        /// Try to get a singleton with the specified generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="singleton"></param>
        /// <returns></returns>
        public bool TryGetSingleton<T>(out T singleton) where T : class
        {
            if (SingletonProvider.TryGetObject(typeof(T), out object singletonObject))
            {
                singleton = singletonObject as T;
                return true;
            }

            singleton = default;
            return false;
        }

        /// <summary>
        /// Bind the specified object to the container keyed by the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public object Bind(Type type, object @object)
        {
            if (ContextMapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider))
            {
                provider.Bind(type, @object);
                return @object;
            }

            if (@object is ISingleton)
            {
                SingletonProvider.Bind(type, @object);
            }
            else
            {
                ObjectProvider.Bind(type, @object);
            }

            return @object;
        }

        /// <summary>
        /// Bind the specified object to the container keyed by the object's type
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public object Bind(object @object)
        {
            return Context.Bind(@object.GetType(), @object);
        }

        /// <summary>
        /// Bind the specified object to the container keyed by the object's type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public T Bind<T>(T @object)
        {
            return Context.Bind(@object);
        }

        /// <summary>
        /// Bind the specified object to the container keyed by the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public T Bind<T>(Type type, T @object)
        {
            return Context.Bind(type, @object);
        }

        /// <summary>
        /// Bind multiple objects to the container keyed by the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public IEnumerable<T> Bind<T>(IEnumerable<T> objects)
        {
            Type type = typeof(T);
            if (ContextMapping.ReverseContainers.TryGetValue(type, out IObjectContainer provider))
            {
                provider.Bind(type, objects);
            }

            return objects;
        }

        /// <summary>
        /// Get all providers provided by the container
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IContextContainer> GetContainers()
        {
            return Context.GetContainers();
        }

        /// <summary>
        /// Determine if the specified type is resolved
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool Resolved(Type type)
        {
            return Context.Resolved(type);
        }

        /// <summary>
        /// Determine if the specified type name is resolved
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public bool Resolved(string typeName)
        {
            return Context.Resolved(typeName);
        }

        /// <summary>
        /// Determine if the specified type and object is resolved
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool Resolved(Type type, object @object)
        {
            return Context.Resolved(type, @object);
        }

        /// <summary>
        /// Determine if the specified object is resolved
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool Resolved(object @object)
        {
            return Context.Resolved(@object.GetType(), @object);
        }

        /// <summary>
        /// Determine if the generic type is resolved
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Resolved<T>()
        {
            return Context.Resolved(typeof(T));
        }

        /// <summary>
        /// Unbind the specified object from the container, keyed by the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool Unbind(Type type, object @object)
        {
            return Context.Unbind(type, @object);
        }

        /// <summary>
        /// Unbind all objects from the container that implement the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool Unbind(Type type)
        {
            return Context.Unbind(type);
        }

        /// <summary>
        /// Unbind the specified object from the container
        /// </summary>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool Unbind(object @object)
        {
            return Context.Unbind(@object.GetType(), @object);
        }

        /// <summary>
        /// Unbind multiple objects from the container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public bool Unbind<T>(IEnumerable<T> objects)
        {
            return Context.Unbind(objects);
        }

        /// <summary>
        /// Unbind all objects from the container that implement the generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool Unbind<T>()
        {
            return Context.Unbind(typeof(T));
        }

        /// <summary>
        /// Unbind the specified object from the container
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool Unbind<T>(T @object)
        {
            return Context.Unbind(typeof(T), @object);
        }

        /// <summary>
        /// Make an object that implements the generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public override TResult Make<TResult>()
        {
            return Factory.Make<TResult>();
        }

        /// <summary>
        /// Make an object that implements the generic type and has the specified alias
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="alias"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public TResult Make<TResult>(string alias, object[] arguments = null)
        {
            if (ContextMapping.ReverseTypes.TryGetValue(alias, out Type type))
            {
                return Factory.Make<TResult>(type, arguments);
            }

            if (ContextMapping.ReverseGenericTypes.TryGetValue(alias, out type))
            {
                return Factory.Make<TResult>(type, arguments);
            }

            return default;
        }

        /// <summary>
        /// Make an object that implements the specified type and cast it to the generic type
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public override TResult Make<TResult>(Type type, object[] arguments = null)
        {
            return Factory.Make<TResult>(type, arguments);
        }

        /// <summary>
        /// Make an object that implements the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public object Make(Type type, object[] arguments = null)
        {
            return Factory.Make(type, arguments);
        }

        #region IDisposable Support

        /// <summary>
        /// Dispose of the container
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    Context.Dispose();
                    Context = null;
                }

                _disposedValue = true;
            }
        }

        /// <summary>
        /// Dispose of the container
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        #endregion IDisposable Support
    }

    /// <summary>
    /// Represents a generic parameter binder
    /// </summary>
    public interface IParameterBinder
    {
        /// <summary>
        /// Unpacks the specified parameter within the specified context
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        object UnpackAny<T>(object context, object parameter);
    }

    internal class DependencyBinder : IParameterBinder
    {
        private readonly BindingManager _bindings;
        private readonly IDictionary<Type, IDictionary<Type, List<BindingScope>>> _bindingCache;

        /// <summary>
        /// Create dependency binder for specified binding manager
        /// </summary>
        /// <param name="bindingManager"></param>
        public DependencyBinder(BindingManager bindingManager)
        {
            _bindings = bindingManager;
            _bindingCache = _bindings.Cache;
        }

        /// <summary>
        /// Get a list of all bindings provided by the container for the specified parameter type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="parameterType"></param>
        /// <returns></returns>
        private List<BindingScope> FilterBindings<T>(object context, Type parameterType)
        {
            return FilterBindings(typeof(T), context, parameterType);
        }

        /// <summary>
        /// Get a list of all bindings provided by the container for the specified parameter type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="context"></param>
        /// <param name="parameterType"></param>
        /// <returns></returns>
        private List<BindingScope> FilterBindings(Type type, object context, Type parameterType)
        {
            if (context == null)
            {
                return null;
            }

            if (type.DeclaringType != null && typeof(IPlugin).IsAssignableFrom(type.DeclaringType))
            {
                context = type.DeclaringType;
            }

            if (!(context is Type contextType))
            {
                contextType = context.GetType();
            }

            List<BindingScope> list = null;

            foreach (BindingScope bindingScope in _bindings.GlobalScope)
            {
                if (!ShouldBind(type, bindingScope, parameterType))
                {
                    continue;
                }

                if (list == null)
                {
                    list = new List<BindingScope>();
                }

                list.Add(bindingScope);
            }

            if (_bindings.BindingScopes.TryGetValue(contextType, out List<BindingScope> scopeList))
            {
                foreach (BindingScope bindingScope in scopeList)
                {
                    if (!ShouldBind(type, bindingScope, parameterType))
                    {
                        continue;
                    }

                    if (list == null)
                    {
                        list = new List<BindingScope>();
                    }

                    list.Add(bindingScope);
                }
            }

            return list;
        }

        /// <summary>
        /// Determine if the specified binding scope can bind the specified parameter type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="scope"></param>
        /// <param name="parameterType"></param>
        /// <returns></returns>
        protected virtual bool ShouldBind<T>(BindingScope scope, Type parameterType)
        {
            return scope.NeedsType.IsAssignableFrom(typeof(T)) && scope.BindMethod.GetParameters()[0].ParameterType == parameterType;
        }

        /// <summary>
        /// Determine if the specified binding scope can bind the specified parameter type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="scope"></param>
        /// <param name="parameterType"></param>
        /// <returns></returns>
        protected virtual bool ShouldBind(Type type, BindingScope scope, Type parameterType)
        {
            if (parameterType != null && scope.BindMethod != null)
            {
                ParameterInfo[] @params = scope.BindMethod.GetParameters();
                if (@params.Length > 0)
                {
                    return scope.NeedsType.IsAssignableFrom(type) && @params[0].ParameterType.IsAssignableFrom(parameterType);
                }
            }

            if (scope.NeedsType == null)
            {
                return false;
            }

            return type.IsAssignableFrom(scope.NeedsType);
        }

        /// <summary>
        /// Unpack the specified parameter from the specified binding context
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object UnpackAny<T>(object context, object parameter)
        {
            return UnpackAny(typeof(T), context, parameter);
        }

        private static readonly Type ObjectType = typeof(object);

        /// <summary>
        /// Unpack the specified parameter from the specified binding context
        /// </summary>
        /// <param name="type"></param>
        /// <param name="context"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object UnpackAny(Type type, object context, object parameter)
        {
            List<BindingScope> bindings;

            Type paramType = parameter?.GetType();
            Type bindingKey = paramType ?? ObjectType;

            if (!_bindingCache.TryGetValue(type, out IDictionary<Type, List<BindingScope>> bindingCache))
            {
                bindingCache = new Dictionary<Type, List<BindingScope>>
                {
                    { bindingKey, bindings = FilterBindings(type, context, paramType) }
                };

                lock (_bindings.BindingLock)
                {
                    if (!_bindingCache.ContainsKey(type))
                    {
                        _bindingCache.Add(type, bindingCache);
                    }
                }
            }
            else if (!bindingCache.TryGetValue(bindingKey, out bindings))
            {
                bindingCache.Add(bindingKey, bindings = FilterBindings(type, context, paramType));
            }

            if (bindings == null || bindings.Count <= 0)
            {
                return null;
            }

            object[] parameters = null;
            try
            {
                foreach (BindingScope bindingScope in bindings)
                {
                    if (bindingScope.boundObject != null)
                    {
                        return bindingScope.boundObject;
                    }

                    object value = null;
                    if (bindingScope.BindMethod != null)
                    {
                        if (bindingScope.BindMethodParameters.Length == 1)
                        {
                            parameters = ArrayPool.Get(1);
                            parameters[0] = parameter;
                        }

                        value = bindingScope.BindMethod.Invoke(bindingScope.BindMethod.IsStatic ? null : context, parameters);
                    }

                    if (bindingScope.BindCallback != null)
                    {
                        value = bindingScope.BindCallback.Invoke();
                    }
                    else if
                        (bindingScope.BindCallbackWithParameter != null)
                    {
                        value = bindingScope.BindCallbackWithParameter.Invoke(parameter);
                    }

                    if (value != null)
                    {
                        return value;
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.uMod.LogException($"Error when resolving {type.Name}", ex);
            }
            finally
            {
                if (parameters != null)
                {
                    ArrayPool.Free(parameters);
                }
            }

            return null;
        }
    }
}
