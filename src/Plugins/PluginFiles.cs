﻿using System;
using System.Collections.Generic;
using System.IO;
using uMod.IO;

namespace uMod.Plugins
{
    /// <summary>
    /// Handles the interaction of plugin files
    /// </summary>
    public class PluginFiles
    {
        private readonly Plugin _plugin;

        private readonly Dictionary<Type, IDataFile<object>> _dataFiles = new Dictionary<Type, IDataFile<object>>();

        /// <summary>
        /// Gets the data file system for configuration files
        /// </summary>
        public DynamicFileSystem Configuration => FileSystem.Configuration;

        /// <summary>
        /// Gets the data file system for localization files
        /// </summary>
        public DynamicFileSystem Localization => FileSystem.Localization;

        /// <summary>
        /// Gets the data file system for data files
        /// </summary>
        public DynamicFileSystem Data { get; }

        internal PluginFiles(Plugin plugin)
        {
            _plugin = plugin;
            Data = new DynamicFileSystem(plugin.Name);
        }

        internal bool HasFileOfType(Type type)
        {
            return _dataFiles.ContainsKey(type);
        }

        internal void AddFile<T>(IDataFile<object> dataFile)
        {
            AddFile(typeof(T), dataFile);
        }

        internal void AddFile(Type type, IDataFile<object> dataFile)
        {
            if (_dataFiles.ContainsKey(type))
            {
                _dataFiles[type] = dataFile;
            }
            else
            {
                _dataFiles.Add(type, dataFile);
            }
        }

        /// <summary>
        /// Gets data file for the specified plugin type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type)
        {
            return _dataFiles.TryGetValue(type, out IDataFile<object> dataFile) ? dataFile : null;
        }

        /// <summary>
        /// Gets data file of the specified plugin type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IDataFile<object> GetDataFile<T>()
        {
            return GetDataFile(typeof(T));
        }

        /// <summary>
        /// Gets data file for the specified type and specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<object> GetDataFile(Type type, string path)
        {
            return FileSystem.GetDataFile(type, path);
        }

        /// <summary>
        /// Gets data file of the generic type for the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public IDataFile<TValue> GetDataFile<TValue>(string path) where TValue : class
        {
            return FileSystem.GetDataFile<TValue>(Path.Combine(_plugin.Name, path));
        }

        /// <summary>
        /// Read object from specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<TValue> ReadObject<TValue>(string path, bool create = false) where TValue : class
        {
            return FileSystem.ReadObject<TValue>(Path.Combine(_plugin.Name, path), create);
        }

        /// <summary>
        /// Read object from specified path
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<object> ReadObject(Type type, string path, bool create = false)
        {
            return FileSystem.ReadObject(type, Path.Combine(_plugin.Name, path), create);
        }

        /// <summary>
        /// Write object of generic type to the specified path
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public IPromise WriteObject<TValue>(string path, TValue @object) where TValue : class
        {
            return FileSystem.WriteObject(Path.Combine(_plugin.Name, path), @object);
        }

        /// <summary>
        /// Write object to the specified path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public IPromise WriteObject(string path, object @object)
        {
            return FileSystem.WriteObject(Path.Combine(_plugin.Name, path), @object);
        }

        /// <summary>
        /// Read objects of generic type from specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="paths"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<TValue>> ReadObjects<TValue>(IEnumerable<string> paths, bool create = false) where TValue : class
        {
            return FileSystem.ReadObjects<TValue>(paths, create, _plugin.Name);
        }

        /// <summary>
        /// Read objects of specified types and paths
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public IPromise<IEnumerable<object>> ReadObjects(IDictionary<Type, string> objects, bool create = false)
        {
            return FileSystem.ReadObjects(objects, create, _plugin.Name);
        }

        /// <summary>
        /// Write objects of the generic type to the specified paths
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="objects"></param>
        /// <returns></returns>
        public IPromise WriteObjects<TValue>(IDictionary<string, TValue> objects) where TValue : class
        {
            return FileSystem.WriteObjects(objects, _plugin.Name);
        }

        /// <summary>
        /// Write objects to the specified paths
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public IPromise WriteObjects(IDictionary<string, object> objects)
        {
            return FileSystem.WriteObjects(objects, _plugin.Name);
        }
    }
}
