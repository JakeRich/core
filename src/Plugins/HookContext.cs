﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a hook context used to store generic state information
    /// </summary>
    public class HookContext : ContextEventCallback<HookEvent, HookContext, Plugin>, IContext
    {
        internal IPlayer player;
        private bool _validated;
        internal bool Valid;
        private string _name; // do not "auto property"

        /// <summary>
        /// The name of the hook
        /// </summary>
        public string Name { get => _name; private set => _name = value; }

        /// <summary>
        /// Determine if hook resulted in return conflict
        /// </summary>
        public bool ReturnConflict { get => _returnConflicts?.Count > 0; }

        private Dictionary<IContext, object> _returnConflicts;

        /// <summary>
        /// Dictionary of all return conflicts
        /// </summary>
        public Dictionary<IContext, object> ReturnConflicts { get => _returnConflicts; private set => _returnConflicts = value; }

        public int ReturnCount => returnValues?.Length ?? 0;

        private HookContext _parent;

        /// <summary>
        /// Parent hook context
        /// </summary>
        public HookContext Parent { get => _parent; internal set => _parent = value; }

        private bool _overrides;

        /// <summary>
        /// Determine if hook context should override default return behavior
        /// </summary>
        public bool Overrides { get => _overrides; private set => _overrides = value; }

        /// <summary>
        /// Gets or sets the final hook value
        /// </summary>
        internal object finalValue;

        /// <summary>
        /// Gets whether or not context has invoked completion callbacks
        /// </summary>
        private bool _invokedCompletion;

        /// <summary>
        /// Final value to return when hook context has override
        /// </summary>
        public object FinalValue
        {
            get => finalValue;
            set
            {
                _overrides = true;
                finalValue = value;
            }
        }

        /// <summary>
        /// Dictionary of hook subscriptions related to hook context
        /// </summary>
        private IList<Plugin> _subscriptions;

        internal object[] returnValues;

        /// <summary>
        /// Array of return values in hook context
        /// </summary>
        public object[] ReturnValues { get => returnValues; internal set => returnValues = value; }

        internal Dictionary<Type, object> argumentContext;

        /// <summary>
        /// Create empty hook context object
        /// </summary>
        public HookContext()
        {
        }

        /// <summary>
        /// Create hook context object
        /// </summary>
        public HookContext(IEnumerable signatureContext = null)
        {
            if (signatureContext == null)
            {
                return;
            }

            argumentContext = new Dictionary<Type, object>();

            foreach (object obj in signatureContext)
            {
                argumentContext.Add(obj.GetType(), obj);
            }
        }

        /// <summary>
        /// Create hook context for hook with specified name and hook subscriptions
        /// </summary>
        /// <param name="name"></param>
        /// <param name="subscriptions"></param>
        public HookContext(string name, IList<Plugin> subscriptions = null)
        {
            _name = name;
            _subscriptions = subscriptions;
        }

        /// <summary>
        /// Add signature context object
        /// </summary>
        /// <param name="object"></param>
        public HookContext AddContext(object @object)
        {
            if (@object == null)
            {
                return this;
            }

            if (argumentContext == null)
            {
                argumentContext = new Dictionary<Type, object>();
            }

            if (!argumentContext.ContainsKey(@object.GetType()))
            {
                argumentContext.Add(@object.GetType(), @object);
            }

            return this;
        }

        /// <summary>
        /// Determines if context contains type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool ContainsType(Type type)
        {
            return argumentContext?.ContainsKey(type) ?? false;
        }

        /// <summary>
        /// Gets the first context item with the specified generic type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T OfType<T>()
        {
            if (argumentContext.TryGetValue(typeof(T), out object @object))
            {
                return (T)@object;
            }

            return default;
        }

        /// <summary>
        /// Add signature context object with concrete type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        public HookContext AddContext(Type type, object @object)
        {
            if (@object == null)
            {
                return this;
            }

            if (argumentContext == null)
            {
                argumentContext = new Dictionary<Type, object>();
            }

            if (!argumentContext.ContainsKey(type))
            {
                if (type == typeof(IPlayer))
                {
                    player = @object as IPlayer;
                }
                argumentContext.Add(type, @object);
            }

            return this;
        }

        /// <summary>
        /// Try to inject signature context variables
        /// </summary>
        /// <param name="type"></param>
        /// <param name="object"></param>
        /// <returns></returns>
        public bool TryInject(Type type, out object @object)
        {
            if (argumentContext != null)
            {
                return argumentContext.TryGetValue(type, out @object);
            }
            @object = null;
            return false;
        }

        /// <summary>
        /// Gets debug information about hook state
        /// </summary>
        /// <returns></returns>
        internal string GetDebugInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Name: {_name}");
            sb.AppendLine($"Validated: {_validated}");
            sb.AppendLine($"Valid: {Valid}");
            sb.AppendLine($"ReturnConflict: {ReturnConflict}");
            sb.AppendLine($"Overrides: {Overrides}");
            sb.AppendLine($"PluginCount: {_subscriptions?.Count ?? 0}");
            sb.AppendLine($"InvokeCompleted: {_invokedCompletion}");
            sb.AppendLine($"OnCompleted: {OnCompleted != null}");
            sb.AppendLine($"OnCanceled: {OnCanceled != null}");
            sb.AppendLine($"OnFailed: {OnFailed != null}");

            return sb.ToString();
        }

        /// <summary>
        /// Validate return values to determine conflict scenario
        /// </summary>
        /// <returns></returns>
        public bool Validate()
        {
            if (_validated)
            {
                return Valid;
            }

            if (returnValues?.Length == 0)
            {
                return _validated = Valid = true;
            }

            bool result = true;
            if (_subscriptions != null)
            {
                for (int x = 0; x < _subscriptions.Count; x++)
                {
                    if (x >= returnValues.Length)
                    {
                        break;
                    }

                    object value = returnValues[x];
                    if (value == null)
                    {
                        continue;
                    }

                    if (value.GetType().IsValueType)
                    {
                        if (value.Equals(finalValue))
                        {
                            continue;
                        }

                        result = false;
                        AddConflict(_subscriptions[x], value);
                    }
                    else
                    {
                        if (value == finalValue)
                        {
                            continue;
                        }

                        result = false;
                        AddConflict(_subscriptions[x], value);
                    }
                }
            }

            _validated = true;
            return Valid = result;
        }

        /// <summary>
        /// Add hook conflict
        /// </summary>
        /// <param name="context"></param>
        /// <param name="value"></param>
        protected void AddConflict(IContext context, object value)
        {
            if (_returnConflicts == null)
            {
                _returnConflicts = new Dictionary<IContext, object>();
            }

            _returnConflicts.Add(context, value);
        }

        /// <summary>
        /// Reset hook context to be used again
        /// </summary>
        /// <param name="name"></param>
        /// <param name="subscriptions"></param>
        /// <returns></returns>
        public void Reset(string name, IList<Plugin> subscriptions = null)
        {
            _name = name;
            _subscriptions = subscriptions;
        }

        /// <summary>
        /// Dispose of hook context data
        /// </summary>
        public override void Dispose()
        {
            player = null;
            _parent = null;
            _subscriptions = null;
            returnValues = null;
            finalValue = null;
            _overrides = _invokedCompletion = _validated = Valid = false;
            _returnConflicts?.Clear();
            argumentContext?.Clear();
            Callback = null;
            OnAlways = OnAfter = OnBefore = OnCanceled = OnCompleted = OnFailed = null;
            AfterCallback = AlwaysCallback = BeforeCallback = CanceledCallback = CompletedCallback = FailedCallback = null;
        }

        /// <summary>
        /// Invoke callbacks depending on current hook state
        /// </summary>
        /// <param name="eventArgs"></param>
        /// <param name="sender"></param>
        /// <returns></returns>
        public override bool Invoke(HookEvent eventArgs, Plugin sender = null)
        {
            if (_invokedCompletion)
            {
                return true;
            }

            switch (eventArgs.state)
            {
                case EventState.Started:
                    OnBefore?.Invoke(sender, eventArgs);
                    break;

                case EventState.Canceled:
                    _invokedCompletion = true;
                    OnCanceled?.Invoke(sender, eventArgs);
                    break;

                case EventState.Completed:
                    _invokedCompletion = true;
                    OnCompleted?.Invoke(sender, eventArgs);
                    break;

                case EventState.Failed:
                    _invokedCompletion = true;
                    OnFailed?.Invoke(sender, eventArgs);
                    break;
            }

            return true;
        }
    }
}
