using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using uMod.Collections;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a loader for a certain type of plugin
    /// </summary>
    public abstract class PluginLoader : IPluginLoader
    {
        /// <summary>
        /// Stores the names of plugins which are currently loading asynchronously
        /// </summary>
        public ICollection<string> LoadingPlugins { get; } = new ConcurrentHashSet<string>();

        /// <summary>
        /// Optional loaded plugin instances used by loaders which need to be notified before a plugin is unloaded
        /// </summary>
        public IDictionary<string, IPlugin> LoadedPlugins { get; } = new Dictionary<string, IPlugin>();

        /// <summary>
        /// Stores the last error a plugin had while loading
        /// </summary>
        public IDictionary<string, string> PluginErrors { get; } = new Dictionary<string, string>();

        /// <summary>
        /// Stores the names of core plugins which should never be unloaded
        /// </summary>
        public virtual Type[] CorePlugins { get; } = Type.EmptyTypes;

        /// <summary>
        /// Stores the plugin file extension which this loader supports
        /// </summary>
        public virtual string FileExtension { get; }

        /// <summary>
        /// Check if this loader has attempted to load core plugins
        /// </summary>
        public bool HasLoadedCorePlugins { get; protected set; }

        /// <summary>
        /// Dictionary of promises that resolve when plugins finish loading
        /// </summary>
        private readonly Dictionary<string, IPromise<bool>> _loadingPromises = new Dictionary<string, IPromise<bool>>();

        private readonly ILogger logger;

        private ILogger Logger => logger ?? Interface.uMod.RootLogger;

        /// <summary>
        /// Create a new instance of the PluginLoader class
        /// </summary>
        /// <param name="logger"></param>
        protected PluginLoader(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Returns all files based on directory filter and file name pattern
        /// </summary>
        /// <param name="rootDirectory"></param>
        /// <param name="directoryFilter"></param>
        /// <param name="filePattern"></param>
        /// <returns></returns>
        public static IEnumerable<FileInfo> GetFiles(DirectoryInfo rootDirectory, string filePattern, Func<DirectoryInfo, bool> directoryFilter)
        {
            foreach (FileInfo matchedFile in rootDirectory.GetFiles(filePattern, SearchOption.TopDirectoryOnly))
            {
                if ((matchedFile.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                {
                    continue;
                }

                yield return matchedFile;
            }

            IEnumerable<DirectoryInfo> matchedDirectories = rootDirectory.GetDirectories("*.*", SearchOption.TopDirectoryOnly).Where(directoryFilter);
            foreach (DirectoryInfo directory in matchedDirectories)
            {
                if ((directory.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                {
                    continue;
                }

                foreach (FileInfo matchedFile in GetFiles(directory, filePattern, directoryFilter))
                {
                    yield return matchedFile;
                }
            }
        }

        protected readonly string[] IgnoredDirectories = { ".git", "bin", "obj", "packages" };

        /// <summary>
        /// Scans the specified directory and returns a set of plugin names for plugins that this loader can load
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public virtual IEnumerable<FileInfo> ScanDirectory(string directory)
        {
            if (FileExtension == null || !Directory.Exists(directory))
            {
                yield break;
            }

            DirectoryInfo rootDirectory = new DirectoryInfo(directory);
            string[] config = Interface.uMod.Plugins.Configuration.Watchers.PluginDirectories;
            bool Filter(DirectoryInfo d) => !IgnoredDirectories.Contains(d.Name.ToLower()) && config.Contains(d.Name.ToLower()) || config.Contains(d.Parent?.Name.ToLower());
            FileInfo[] files = GetFiles(rootDirectory, "*" + FileExtension, Filter).ToArray();
            foreach (FileInfo file in files)
            {
                yield return file;
            }
        }

        /// <summary>
        /// Loads a plugin given the specified name
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual IPlugin Load(string directory, string name)
        {
            if (LoadingPlugins.Contains(name))
            {
                Logger.Debug(Interface.uMod.Strings.Compiler.AlreadyLoading.Interpolate("plugin", name));
                return null;
            }

            string filename = Path.Combine(directory, name + FileExtension);
            IPlugin plugin = GetPlugin(filename);
            Load(plugin, true);

            return null;
        }

        /// <summary>
        /// Load specified plugin loader
        /// </summary>
        public void LoadCorePlugins()
        {
            foreach (Type type in CorePlugins)
            {
                try
                {
                    Plugin plugin = (Plugin)Activator.CreateInstance(type);
                    if (plugin.Resolve(this, plugin.Name, plugin.Filename))
                    {
                        PluginLoaded(plugin).Done(delegate (bool resolved)
                        {
                            if (!LoadedPlugins.ContainsKey(plugin.Name))
                            {
                                LoadedPlugins.Add(plugin.Name, plugin);
                            }

                            if (LoadingPlugins.Contains(plugin.Name))
                            {
                                LoadingPlugins.Remove(plugin.Name);
                            }
                        }, delegate (Exception exception)
                        {
                            Logger.Report(Interface.uMod.Strings.PluginLoader.CorePluginLoadFailure.Interpolate("plugin", type.Name), exception);
                        });
                    }
                    else
                    {
                        Logger.Warning(Interface.uMod.Strings.PluginLoader.ResolveFailure.Interpolate("plugin", plugin.Name));
                    }
                }
                catch (Exception ex)
                {
                    Logger.Report(Interface.uMod.Strings.PluginLoader.CorePluginLoadFailure.Interpolate("plugin", type.Name), ex);
                }
            }

            HasLoadedCorePlugins = true;
        }

        /// <summary>
        /// Load specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="async"></param>
        public virtual void Load(IPlugin plugin, bool async = false)
        {
            LoadingPlugins.Add(plugin.Name);

            if (async)
            {
                Interface.uMod.NextTick(() => LoadPlugin(plugin));
            }
            else
            {
                LoadPlugin(plugin);
            }
        }

        /// <summary>
        /// Gets a plugin given the specified filename
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        protected virtual IPlugin GetPlugin(string filename) => null; // TODO: Implement

        /// <summary>
        /// Loads a given plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="waitingForAccess"></param>
        protected void LoadPlugin(IPlugin plugin, bool waitingForAccess = false)
        {
            if (!string.IsNullOrEmpty(plugin.Filename) && !File.Exists(plugin.Filename))
            {
                LoadingPlugins.Remove(plugin.Name);
                Logger.Warning(Interface.uMod.Strings.PluginLoader.ScriptNotFound.Interpolate("plugin", plugin.Name));
                return;
            }

            try
            {
                Interface.uMod.Plugins.Unload(plugin.Name);
                LoadingPlugins.Remove(plugin.Name);
                if (plugin.Resolve(this, plugin.Name, plugin.Filename))
                {
                    Interface.uMod.Application.Resolve(plugin.GetType());
                    PluginLoaded(plugin);
                }
            }
            catch (IOException)
            {
                if (!waitingForAccess)
                {
                    Logger.Warning(Interface.uMod.Strings.Compiler.AccessException.Interpolate("plugin", plugin.Name));
                }

                Interface.uMod.Libraries.Get<Libraries.Timer>().Once(.5f, () => LoadPlugin(plugin, true));
            }
            catch (Exception ex)
            {
                LoadingPlugins.Remove(plugin.Name);
                Logger.Report(Interface.uMod.Strings.PluginLoader.LoadFailure.Interpolate("plugin", plugin.Name), ex);
            }
        }

        /// <summary>
        /// Reloads a plugin given the specified name, implemented by plugin loaders which support reloading plugins
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual void Reload(string directory, string name)
        {
            Interface.uMod.Plugins.Unload(name);
            Interface.uMod.Plugins.Load(name);
        }

        /// <summary>
        /// Called when a plugin which was loaded by this loader is being unloaded by the plugin manager
        /// </summary>
        /// <param name="plugin"></param>
        public virtual void Unloading(IPlugin plugin)
        {
        }

        /// <summary>
        /// Unloads the specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool PluginUnloaded(IPlugin plugin)
        {
            Unloading(plugin);

            // Unload the plugin
            Interface.uMod.Plugins.Manager.RemoveContext(plugin);

            if (plugin.IsLoaded && !plugin.IsCorePlugin)
            {
                Logger.Info(Interface.uMod.Strings.Plugin.PluginUnloaded.Interpolate(
                    ("plugin", plugin.Title),
                    ("version", plugin.Version),
                    ("author", plugin.Author)
                ));
                Interface.uMod.CallHook("OnPluginUnloaded", plugin); // Let plugins know
            }
            plugin.Unload(this);

            return true;
        }

        /// <summary>
        /// Complete plugin loading sequence
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public IPromise<bool> PluginLoaded(IPlugin plugin)
        {
            plugin.OnError.Add(plugin_OnError);
            plugin.OnException.Add(plugin_OnException);
            plugin.OnWarning.Add(plugin_OnWarning);
            plugin.OnResolved.Add(plugin_OnResolved);

            if (plugin.IsCorePlugin && !LoadingPlugins.Contains(plugin.Name))
            {
                LoadingPlugins.Add(plugin.Name);
            }

            _loadingPromises.Remove(plugin.Name);
            Promise<bool> promise;
            _loadingPromises.Add(plugin.Name, promise = new Promise<bool>());

            try
            {
                PluginErrors.Remove(plugin.Name);
                Interface.uMod.Plugins.Manager.AddContext(plugin);
            }
            catch (Exception ex)
            {
                if (plugin.Loader != null)
                {
                    plugin.Loader.PluginErrors[plugin.Name] = ex.Message;
                }

                Logger.Report(Interface.uMod.Strings.Plugin.InitializeFailure.Interpolate(("name", plugin.Name), ("version", plugin.Version)), ex);
                promise.Resolve(false);
            }

            return promise;
        }

        public void plugin_OnResolved(IPlugin plugin)
        {
            if (_loadingPromises.TryGetValue(plugin.Name, out IPromise<bool> promise))
            {
                if (promise.State == PromiseState.Pending)
                {
                    try
                    {
                        if (PluginErrors.ContainsKey(plugin.Name))
                        {
                            Interface.uMod.Plugins.Unload(plugin.Name);
                            promise.Resolve(false);
                            return;
                        }

                        plugin.Load(this);
                        if (plugin.IsCorePlugin)
                        {
                            Interface.uMod.Application.Bind(plugin);
                            promise.Resolve(true);
                            return;
                        }

                        Logger.Info(Interface.uMod.Strings.Plugin.PluginLoaded.Interpolate(
                            ("plugin", plugin.Title),
                            ("version", plugin.Version),
                            ("author", plugin.Author)
                        ));

                        Interface.uMod.Application.Bind(plugin);
                        Interface.uMod.CallHook("OnPluginLoaded", plugin); // Let plugins know
                        promise.Resolve(true);
                    }
                    catch (Exception ex)
                    {
                        if (plugin.Loader != null)
                        {
                            plugin.Loader.PluginErrors[plugin.Name] = ex.Message;
                        }

                        Logger.Report(Interface.uMod.Strings.Plugin.InitializeFailure.Interpolate(("name", plugin.Name), ("version", plugin.Version)), ex);
                        promise.Resolve(false);
                    }
                }

                _loadingPromises.Remove(plugin.Name);
            }
        }

        /// <summary>
        /// Called when a plugin has raised an error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void plugin_OnError(IPlugin sender, string message) => Logger.Error($"{sender.Name} v{sender.Version}: {message}");

        /// <summary>
        /// Called when a plugin has raised an exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        private void plugin_OnException(IPlugin sender, string message, Exception exception) => Logger.Report($"{sender.Name} v{sender.Version}: {message}", exception);

        /// <summary>
        /// Called when a plugin has raised a warning
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void plugin_OnWarning(IPlugin sender, string message) => Logger.Warning($"{sender.Name} v{sender.Version}: {message}");
    }
}
