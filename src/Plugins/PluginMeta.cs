using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Configuration;
using uMod.Exceptions;
using uMod.IO;
using uMod.IO.Schema;
using uMod.Logging;
using uMod.Pooling;
using uMod.Utilities;

#pragma warning disable 618

namespace uMod.Plugins
{
    /// <summary>
    /// Plugin configuration types
    /// </summary>
    internal enum ConfigurationType
    {
        None = 0,
        Legacy = 1,
        Dynamic = 2
    }

    /// <summary>
    /// Plugin localization types
    /// </summary>
    internal enum LocalizationType
    {
        None = 0,
        Lang = 1,
        Locale = 2
    }

    /// <summary>
    /// Represents plugin meta ifnromation
    /// </summary>
    internal class PluginMeta
    {
        
        /// <summary>
        /// Represents a versioned data file used by a plugin
        /// </summary>
        internal class PluginDataDescription : IVersionableFile
        {
            public string Name { get; set; }
            public string Filename { get; set; }
            public Type Type { get; set; }
            public object Attribute { get; set; }
            public T GetAttribute<T>()
            {
                return (T)Attribute;
            }

            public VersionNumber Version { get; set; }

            public override string ToString()
            {
                return $"{Name} ({Filename}) - {Type?.Name ?? "unknown"} ({Version})";
            }
        }

        /// <summary>
        /// Represents a reference type
        /// </summary>
        internal enum PluginReferenceType
        {
            Optional,
            Requires,
            Debug
        }

        /// <summary>
        /// Represents a plugin reference
        /// </summary>
        internal struct PluginReference
        {
            public string[] Names;
            public IPlugin Plugin;
            public FieldInfo Field;
            public PluginReferenceType Type;

            /// <summary>
            /// Create a new plugin reference object
            /// </summary>
            /// <param name="plugin"></param>
            /// <param name="names"></param>
            /// <param name="field"></param>
            /// <param name="type"></param>
            /// <param name="stronglyTyped"></param>
            public PluginReference(IPlugin plugin, string[] names, FieldInfo field, PluginReferenceType type)
            {
                Plugin = plugin;
                Names = names;
                Field = field;
                Type = type;
            }

            /// <summary>
            /// Loads plugin reference with a plugin instance
            /// </summary>
            /// <param name="val"></param>
            public void Loaded(object val)
            {
                Field.SetValue(Plugin, val);
            }

            /// <summary>
            /// Unloads plugin reference
            /// </summary>
            public void Unloaded()
            {
                Field.SetValue(Plugin, null);
            }

            /// <summary>
            /// Determine if plugin reference is loaded
            /// </summary>
            /// <returns></returns>
            public bool IsLoaded()
            {
                return Field.GetValue(Plugin) != null;
            }
        }

        // Plugin associated with meta
        private readonly Plugin _plugin;

        // Files associated with plugin
        private readonly PluginFiles _files;

        // Plugin references
        internal readonly Dictionary<string, PluginReference> PluginReferenceFields = new Dictionary<string, PluginReference>();

        // Online player fields
        internal HashSet<PluginFieldInfo> OnlinePlayerFields = new HashSet<PluginFieldInfo>();

        /// <summary>
        /// The configuration type
        /// </summary>
        public ConfigurationType Configuration = ConfigurationType.None;

        /// <summary>
        /// The localization type
        /// </summary>
        public LocalizationType Localization = LocalizationType.None;

        /// <summary>
        /// The type mediator
        /// </summary>
        public readonly TypeMediator Mediator;

        private readonly IApplication _application;
        private readonly ILogger _logger;
        private readonly Configuration.Logging _loggingConfiguration;
        private readonly IChainDispatcher _dispatcher;
        private LocaleSchemaReader _localeSchemaReader;
        private ConfigSchemaReader _configSchemaReader;
        
        /// <summary>
        /// Create a new plugin meta object
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="files"></param>
        /// <param name="application"></param>
        /// <param name="dispatcher"></param>
        /// <param name="logger"></param>
        /// <param name="loggingConfiguration"></param>
        public PluginMeta(Plugin plugin, PluginFiles files, IApplication application, IChainDispatcher dispatcher, ILogger logger, Configuration.Logging loggingConfiguration)
        {
            _application = application;
            _dispatcher = dispatcher;
            _logger = logger;
            _loggingConfiguration = loggingConfiguration;
            _plugin = plugin;
            Mediator = new TypeMediator(plugin, application);
            _files = files;
        }

        /// <summary>
        /// Save configuration
        /// </summary>
        /// <returns></returns>
        public IPromise SaveConfig()
        {
            Promise promise = new Promise();
            if (Configuration.Has(ConfigurationType.Legacy))
            {
                SaveLegacyConfiguration(promise);
            }

            if (Configuration.Has(ConfigurationType.Dynamic))
            {
                SaveDynamicConfiguration(promise);
            }
            return promise;
        }

        /// <summary>
        /// Save legacy configuration
        /// </summary>
        /// <param name="promise"></param>
        private void SaveLegacyConfiguration(Promise promise)
        {
            if (_plugin?.Config != null)
            {
                try
                {
                    _plugin.Config.Save();
                    promise.Resolve();
                }
                catch (Exception ex)
                {
                    promise.Reject(ex);
                    _plugin.OnException.Invoke(_plugin, Interface.uMod.Strings.Plugin.ConfigSaveFailure, ex);
                }
            }
        }

        /// <summary>
        /// Save dynamic configuration
        /// </summary>
        /// <param name="promise"></param>Interfa
        private void SaveDynamicConfiguration(Promise promise)
        {
            Dictionary<string, object> configs = new Dictionary<string, object>();
            Dictionary<string, VersionNumber> configVersions = new Dictionary<string, VersionNumber>();
            if (Mediator.TryGetTypes("Config*", out IEnumerable<Type> configTypes))
            {
                if(_configSchemaReader == null)
                {
                    _configSchemaReader = new ConfigSchemaReader(_plugin, _logger, Mediator, new ConfigSchemaMigration(_plugin, _logger, _files, _application, _dispatcher), _files);
                }

                foreach (Type configType in configTypes)
                {
                    string name = _plugin.Name;
                    if (configType.GetCustomAttribute<ConfigAttribute>() is ConfigAttribute configAttribute)
                    {
                        name = _configSchemaReader.GetDataFileName(configType, configAttribute);
                        if (string.IsNullOrEmpty(configAttribute.Version))
                        {
                            configVersions.Add(name, VersionNumber.Empty);
                        }
                        else if (VersionNumber.TryParse(configAttribute.Version, out VersionNumber configVersion))
                        {
                            configVersions.Add(name, configVersion);
                        }
                    }

                    if (Mediator.TryGetObject(configType, out object config))
                    {
                        configs.Add(name, config);
                    }
                }

                FileSystem.Configuration.WriteObjects(configs).Done(delegate()
                {
                    VersionStore.Instance.Update(configVersions);
                    promise.Resolve();
                }, promise.Reject);
            }
        }

        /// <summary>
        /// Load configuration
        /// </summary>
        /// <returns></returns>
        public IPromise LoadConfiguration()
        {
            Promise promise = new Promise();
            if (Configuration.Has(ConfigurationType.Legacy))
            {
                LoadLegacyConfiguration(promise);
            }

            if (Configuration.Has(ConfigurationType.Dynamic))
            {
                if(_configSchemaReader == null)
                {
                    _configSchemaReader = new ConfigSchemaReader(_plugin, _logger, Mediator, new ConfigSchemaMigration(_plugin, _logger, _files, _application, _dispatcher), _files);
                }

                _configSchemaReader.ReadSchema(promise);
            }

            return promise;
        }

        /// <summary>
        /// Load localization
        /// </summary>
        /// <returns></returns>
        public IPromise<IEnumerable<object>> LoadLocalization()
        {
            Promise<IEnumerable<object>> promise = new Promise<IEnumerable<object>>();
            if (Localization.Has(LocalizationType.Lang))
            {
                LoadLangLocalization(promise);
            }

            if (Localization.Has(LocalizationType.Locale))
            {
                if(_localeSchemaReader == null)
                {
                    _localeSchemaReader = new LocaleSchemaReader(_plugin, _logger, Mediator, new LocaleSchemaMigration(_plugin, _logger, _files, _application, _dispatcher), _files);
                }

                _localeSchemaReader.ReadSchema(promise);
            }
            else
            {
                promise.Resolve(null);
            }

            return promise;
        }

        /// <summary>
        /// Load legacy configuration
        /// </summary>
        /// <param name="promise"></param>
        private void LoadLegacyConfiguration(Promise promise)
        {
            DynamicConfigFile config;
            if (_plugin.Config != null)
            {
                config = _plugin.Config;
            }
            else
            {
                config = _plugin.Config =
                    new DynamicConfigFile(Path.Combine(Interface.uMod.ConfigDirectory, $"{_plugin.Name}.json"));
            }

            if (!config.Exists())
            {
                promise.Reject(new LegacyConfigFileNotFoundException());
                return;
            }

            config.LoadAsync().Done(promise.Resolve, delegate (Exception exception)
            {
                promise.Reject(exception);
                _plugin.OnException.Invoke(_plugin, Interface.uMod.Strings.Plugin.ConfigLoadFailure, exception);
            });
        }

        /// <summary>
        /// Load lang localization
        /// </summary>
        /// <param name="promise"></param>
        private void LoadLangLocalization(Promise<IEnumerable<object>> promise)
        {
        }

        /// <summary>
        /// Load custom logging behavior
        /// </summary>
        /// <returns></returns>
        public ILogger LoadLogger()
        {
            ILogger pluginLogger = null;
            LogAttribute[] logAttributes = _plugin.GetType().GetCustomAttributes<LogAttribute>()?.ToArray();
            if (logAttributes != null && logAttributes.Length == 1)
            {
                // Replace default logger
                if (_loggingConfiguration.Channels.TryGetValue(logAttributes[0].Name ?? _loggingConfiguration.Default, out ILogChannel logChannel))
                {
                    if (_loggingConfiguration.FileLogging && logChannel.Logger is IFileLogger || !(logChannel.Logger is IFileLogger))
                    {
                        // Use globally configured logger
                        pluginLogger = logChannel.Logger;
                    }
                }
                else
                {
                    // Create plugin-specific logger
                    if (logAttributes[0].LoggerType != null)
                    {
                        object[] parameters;
                        if (!string.IsNullOrEmpty(logAttributes[0].Filename))
                        {
                            parameters = ArrayPool.Get(2);
                            parameters[0] = _plugin;
                            parameters[1] = logAttributes[0].Filename;
                        }
                        else
                        {
                            parameters = ArrayPool.Get(1);
                            parameters[0] = _plugin;
                        }

                        try
                        {
                            bool fileLogger = typeof(IFileLogger).IsAssignableFrom(logAttributes[0].LoggerType);
                            if (_loggingConfiguration.FileLogging && fileLogger || !fileLogger)
                            {
                                pluginLogger = _application.Make<ILogger>(logAttributes[0].LoggerType, parameters);
                            }
                        }
                        finally
                        {
                            ArrayPool.Free(parameters);
                        }
                    }
                    else
                    {
                        _logger.Warning(Interface.uMod.Strings.Plugin.LoggerMissing.Interpolate(
                            ("plugin", _plugin.Title),
                            ("name", logAttributes[0].Name ?? _loggingConfiguration.Default)
                        ));
                    }
                }
            }
            else if (logAttributes != null && logAttributes.Length > 1)
            {
                // Add multiple loggers in logger stack
                List<ILogger> loggers = new List<ILogger>();

                foreach (LogAttribute logAttribute in logAttributes)
                {
                    if (_loggingConfiguration.Channels.TryGetValue(logAttribute.Name ?? _loggingConfiguration.Default, out ILogChannel logChannel))
                    {
                        if (_loggingConfiguration.FileLogging && logChannel.Logger is IFileLogger || !(logChannel.Logger is IFileLogger))
                        {
                            // Use globally configured logger
                            loggers.Add(logChannel.Logger);
                        }
                    }
                    else
                    {
                        // Create plugin-specific logger
                        if (logAttribute.LoggerType != null)
                        {
                            object[] parameters;
                            if (!string.IsNullOrEmpty(logAttribute.Filename))
                            {
                                parameters = ArrayPool.Get(2);
                                parameters[0] = _plugin;
                                parameters[1] = logAttribute.Filename;
                            }
                            else
                            {
                                parameters = ArrayPool.Get(1);
                                parameters[0] = _plugin;
                            }

                            try
                            {
                                bool fileLogger = typeof(IFileLogger).IsAssignableFrom(logAttribute.LoggerType);
                                if (_loggingConfiguration.FileLogging && fileLogger || !fileLogger)
                                {

                                    loggers.Add(Interface.uMod.Application.Make<ILogger>(logAttribute.LoggerType, parameters));
                                }
                            }
                            finally
                            {
                                ArrayPool.Free(parameters);
                            }
                        }
                        else
                        {
                            _logger.Warning(Interface.uMod.Strings.Plugin.LoggerMissing.Interpolate(
                                ("plugin", _plugin.Title),
                                ("name", logAttribute.Name ?? _loggingConfiguration.Default)
                            ));
                        }
                    }
                }

                if (loggers.Count > 0)
                {
                    StackLogger stackLogger = new StackLogger();
                    stackLogger.AddLogger(loggers);
                    pluginLogger = stackLogger;
                }
            }

            return pluginLogger ?? new PipeLogger(_logger, $"[{_plugin.Title}] ");
        }
    }
}
