﻿using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Libraries;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a plugin localization helper class
    /// Created with each plugin to simplify Lang provider handling
    /// </summary>
    public sealed class PluginLang
    {
        private readonly Plugin _plugin;
        private readonly Lang _langLib = Interface.uMod.Libraries.Get<Lang>();

        /// <summary>
        /// Create a plugin lang wrapper for the specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        public PluginLang(Plugin plugin)
        {
            _plugin = plugin;
        }

        /// <summary>
        /// Registers a language set for a plugin
        /// </summary>
        /// <param name="messages"></param>
        /// <param name="language"></param>
        public void RegisterMessages(Dictionary<string, string> messages, string language = Lang.DefaultLang)
        {
            _langLib.RegisterMessages(messages, _plugin, language);
        }

        /// <summary>
        /// Gets all available languages or only those for a single plugin
        /// </summary>
        /// <param name="suffix"></param>
        /// <returns></returns>
        public string[] GetLanguages(string suffix = null)
        {
            return _langLib.GetLanguages(_plugin, suffix);
        }

        /// <summary>
        /// Gets the language for the player, fall back to the default server language if no language is set
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string GetLanguage(string playerId)
        {
            return _langLib.GetLanguage(playerId);
        }

        /// <summary>
        /// Gets the language for the player, fall back to the default server language if no language is set
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public string GetLanguage(IPlayer player)
        {
            return _langLib.GetLanguage(player.Id);
        }

        /// <summary>
        /// Sets the language preference for the player
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="language"></param>
        public void SetLanguage(string playerId, string language)
        {
            _langLib.SetLanguage(language, playerId);
        }

        /// <summary>
        /// Sets the language preference for the player
        /// </summary>
        /// <param name="player"></param>
        /// <param name="language"></param>
        public void SetLanguage(IPlayer player, string language)
        {
            _langLib.SetLanguage(language, player.Id);
        }

        /// <summary>
        /// Checks if translations exist for the specified language
        /// </summary>
        /// <param name="language"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        public bool HasLanguage(string language, string suffix = null)
        {
            return _langLib.HasLanguage(language, _plugin, suffix);
        }

        /// <summary>
        /// Gets a message using the specified key for the specified player
        /// </summary>
        /// <param name="key"></param>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public string GetMessage(string key, string playerId = null)
        {
            return _langLib.GetMessage(key, _plugin, playerId);
        }

        /// <summary>
        /// Gets a message using the specified key for the specified language
        /// </summary>
        /// <param name="key"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetLangMessage(string key, string lang = null)
        {
            return _langLib.GetLangMessage(key, _plugin, lang);
        }

        /// <summary>
        /// Gets a message using the specified key for the specified player
        /// </summary>
        /// <param name="key"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public string GetMessage(string key, IPlayer player = null)
        {
            return _langLib.GetMessage(key, _plugin, player?.Id);
        }

        /// <summary>
        /// Gets all messages for a plugin in a language
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetMessages(string language)
        {
            return _langLib.GetMessages(language, _plugin);
        }
    }

    /// <summary>
    /// PluginLocale helper class
    /// Created with each plugin to simplify Locale provider handling
    /// </summary>
    public sealed class PluginLocale
    {
        private readonly Plugin _plugin;
        private readonly Locale _localeLib = Interface.uMod.Libraries.Get<Locale>();
        private readonly Lang _langLib = Interface.uMod.Libraries.Get<Lang>();

        /// <summary>
        /// Number of locales associated with plugin
        /// </summary>
        public int Count
        {
            get
            {
                return _plugin.Meta.Mediator.GetTypes(_plugin.Name).Count(x => x is ILocale);
            }
        }

        public PluginLocale(Plugin plugin)
        {
            _plugin = plugin;
        }

        /// <summary>
        /// Registers a locale set for the plugin
        /// </summary>
        /// <param name="provider"></param>
        public void Register(ILocale provider)
        {
            _localeLib.RegisterLocale(provider, _plugin);
        }

        /// <summary>
        /// Get the locale provider for the specified plugin
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetLocale<T>() where T : class
        {
            return _localeLib.GetLocale<T>(_plugin);
        }

        /// <summary>
        /// Get the locale provider of the specified language for the plugin
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lang"></param>
        /// <returns></returns>
        public T GetLocale<T>(string lang) where T : class
        {
            return _localeLib.GetLocale<T>(lang, _plugin);
        }

        /// <summary>
        /// Get the locale provider of the specified language for the plugin
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public object GetLocale(string lang)
        {
            return _localeLib.GetLocale<ILocale>(lang, _plugin);
        }

        /// <summary>
        /// Get the locale provider for the specified player
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="player"></param>
        /// <returns></returns>
        public T GetLocale<T>(IPlayer player) where T : class
        {
            return _localeLib.GetLocale<T>(_langLib.GetLanguage(player?.Id), _plugin);
        }

        /// <summary>
        /// Gets the locale provider for the specified language
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public object GetLocale(Type localeType, string lang)
        {
            return _localeLib.GetLocale(localeType, lang, _plugin);
        }

        /// <summary>
        /// Gets a dictionary of locales for the specified languages
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="languages"></param>
        /// <returns></returns>
        public IDictionary<string, object> GetLocales(Type localeType, IEnumerable<string> languages)
        {
            return _localeLib.GetLocales(localeType, languages, _plugin);
        }

        /// <summary>
        /// Gets the locale provider for the specified player
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public object GetLocale(Type localeType, IPlayer player)
        {
            return _localeLib.GetLocale(localeType, _langLib.GetLanguage(player?.Id), _plugin);
        }
    }
}
