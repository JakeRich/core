using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Command;
using uMod.Common;
using uMod.Database;
using uMod.Libraries;
using uMod.Plugins.Decorators;
using uMod.Pooling;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a plugin attribute resolver
    /// </summary>
    internal class PluginAttributeResolver
    {
        private const BindingFlags DefaultBindingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
        private const BindingFlags PrivateInstanceBindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;
        private const BindingFlags StaticBindingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static;

        private readonly Plugin _plugin;
        private readonly PluginLang _lang;
        private readonly PluginLocale _locale;

        private readonly IApplication _application;

        /// <summary>
        /// Creates a plugin attribute resolver
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="lang"></param>
        /// <param name="locale"></param>
        /// <param name="application"></param>
        public PluginAttributeResolver(Plugin plugin, PluginLang lang, PluginLocale locale, IApplication application)
        {
            _application = application;
            _plugin = plugin;
            _lang = lang;
            _locale = locale;
        }

        /// <summary>
        /// Resolves plugin attributes
        /// </summary>
        public void Resolve()
        {
            Type originalType = _plugin.GetType();
            Type type = originalType;
            List<Type> types = new List<Type> { type };
            while (type != typeof(Plugin))
            {
                types.Add(type = type?.BaseType);
            }

            // Add hooks and hook decorators implemented in base classes before user implemented methods
            for (int i = types.Count - 1; i >= 0; i--)
            {
                ResolveExplicitHooks(types[i]);
            }

            ResolveImplicitHooks();
            ResolveSingletons();
            ResolvePluginReferences();
            ResolveModels();
            ResolveConverters();
            ResolveReferences();
            ResolveConfiguration();
            ResolveLocalization();
            ResolveOnlinePlayerFields();
        }

        private void ResolveOnlinePlayerFields()
        {
            Type playerType = typeof(IPlayer);
            Type gamePlayerType = Interface.uMod.Universal.Provider.Types.Player;
            foreach (FieldInfo field in _plugin.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                object[] attributes = field.GetCustomAttributes(typeof(OnlinePlayersAttribute), true);
                if (attributes.Length <= 0)
                {
                    continue;
                }

                PluginFieldInfo pluginField = new PluginFieldInfo(_plugin, field);
                if (pluginField.GenericArguments.Length == 2 &&
                    pluginField.GenericArguments[0] == gamePlayerType &&
                    ValidateOnlinePlayerField(gamePlayerType, field, pluginField))
                {
                    pluginField.PlayerType = gamePlayerType;
                    _plugin.Meta.OnlinePlayerFields.Add(pluginField);
                }
                else if (pluginField.GenericArguments.Length == 2 &&
                        pluginField.GenericArguments[0] == playerType &&
                        ValidateOnlinePlayerField(playerType, field, pluginField))
                {
                    pluginField.PlayerType = playerType;
                    _plugin.Meta.OnlinePlayerFields.Add(pluginField);
                }
                else
                {
                    _plugin.logger.Warning($"The {field.Name} field is not a Hash with a {Utility.ReflectedName(gamePlayerType.Name)} or IPlayer key! (online players will not be tracked)"); // TODO: Localization
                }
            }
        }

        private bool ValidateOnlinePlayerField(Type gamePlayerType, FieldInfo field, PluginFieldInfo pluginField)
        {
            string gamePlayerName = Utility.ReflectedName(gamePlayerType.Name);
            if (pluginField.GenericArguments.Length != 2 || pluginField.GenericArguments[0] != gamePlayerType)
            {
                _plugin.logger.Warning($"The {field.Name} field is not a Hash with a {gamePlayerName} key! (online players will not be tracked)"); // TODO: Localization
                return false;
            }
            if (!pluginField.LookupMethod("Add", pluginField.GenericArguments))
            {
                _plugin.logger.Warning($"The {field.Name} field does not support adding {gamePlayerName} keys! (online players will not be tracked)"); // TODO: Localization
                return false;
            }
            if (!pluginField.LookupMethod("Remove", gamePlayerType))
            {
                _plugin.logger.Warning($"The {field.Name} field does not support removing {gamePlayerName} keys! (online players will not be tracked)"); // TODO: Localization
                return false;
            }
            if (pluginField.GenericArguments[1].GetField("Player") == null)
            {
                _plugin.logger.Warning($"The {pluginField.GenericArguments[1].Name} class does not have a public Player field! (online players will not be tracked)"); // TODO: Localization
                return false;
            }
            if (!pluginField.HasValidConstructor(gamePlayerType))
            {
                _plugin.logger.Warning($"The {field.Name} field is using a class which contains no valid constructor (online players will not be tracked)"); // TODO: Localization
                return false;
            }

            return true;
        }

        public void ResolveCommands(IContext context)
        {
            foreach (MethodInfo method in context.GetType().GetMethodsWithAttribute<CommandAttribute>(DefaultBindingFlags))
            {
                IEnumerable<CommandAttribute> commandAttributes = method.GetCustomAttributes<CommandAttribute>();
                LangAttribute langAttribute = method.GetCustomAttribute<LangAttribute>();
                LocaleAttribute localeAttribute = method.GetCustomAttribute<LocaleAttribute>();
                DescriptionAttribute descriptionAttribute = method.GetCustomAttribute<DescriptionAttribute>();
                HelpAttribute helpAttribute = method.GetCustomAttribute<HelpAttribute>();
                PermissionAttribute cmdPerm = method.GetCustomAttribute<PermissionAttribute>();
                List<string> commands = new List<string>();
                Dictionary<string, string> descriptionMessages = null;
                Dictionary<string, string> helpMessages = null;

                foreach (CommandAttribute commandAttribute in commandAttributes)
                {
                    commands.AddRange(commandAttribute.Commands);
                    if (localeAttribute?.LocaleType != null)
                    {
                        if (commandAttribute.Commands.Length == 0)
                        {
                            throw new ArgumentException("Command must define a locale field name"); // TODO: Localization
                        }

                        commands.Clear();
                        LocalizationAttribute localizationAttribute =
                            localeAttribute.LocaleType.GetCustomAttribute<LocalizationAttribute>();

                        string[] languages = _lang.GetLanguages(localizationAttribute.Name);
                        IDictionary<string, object> locales = _locale.GetLocales(localeAttribute.LocaleType, languages);
                        foreach (string localeFieldName in commandAttribute.Commands)
                        {
                            foreach (KeyValuePair<string, object> kvp in locales)
                            {
                                string commandName = kvp.Value.GetValue(localeFieldName);
                                if (!string.IsNullOrEmpty(commandName) && !commands.Contains(commandName))
                                {
                                    commands.Add(commandName);
                                }
                            }
                        }

                        if (descriptionAttribute != null)
                        {
                            if (descriptionMessages == null)
                            {
                                descriptionMessages = new Dictionary<string, string>();
                            }

                            foreach (KeyValuePair<string, object> kvp in locales)
                            {
                                descriptionMessages.Add(kvp.Key, kvp.Value.GetValue(descriptionAttribute.Description) ?? descriptionAttribute.Description);
                            }
                        }

                        if (helpAttribute != null)
                        {
                            if (helpMessages == null)
                            {
                                helpMessages = new Dictionary<string, string>();
                            }

                            foreach (KeyValuePair<string, object> kvp in locales)
                            {
                                helpMessages.Add(kvp.Key, kvp.Value.GetValue(helpAttribute.Syntax) ?? helpAttribute.Syntax);
                            }
                        }

                        if (commands.Count == 0)
                        {
                            _plugin.logger.Warning($"Unable to find localized command names: {method.Name}"); // TODO: Localization
                        }
                    }
                    else if (langAttribute != null)
                    {
                        commands.Clear();
                        string[] languages = _lang.GetLanguages();
                        foreach (string commandName in commandAttribute.Commands)
                        {
                            foreach (string language in languages)
                            {
                                string langValue = _lang.GetLangMessage(commandName, language);
                                if (!string.IsNullOrEmpty(langValue))
                                {
                                    commands.Add(langValue);
                                }
                            }
                        }

                        if (descriptionAttribute != null)
                        {
                            if (descriptionMessages == null)
                            {
                                descriptionMessages = new Dictionary<string, string>();
                            }

                            foreach (string language in languages)
                            {
                                descriptionMessages.Add(language, _lang.GetLangMessage(descriptionAttribute.Description, language) ?? descriptionAttribute.Description);
                            }
                        }

                        if (helpAttribute != null)
                        {
                            if (helpMessages == null)
                            {
                                helpMessages = new Dictionary<string, string>();
                            }

                            foreach (string language in languages)
                            {
                                helpMessages.Add(language, _lang.GetLangMessage(helpAttribute.Syntax, language) ?? helpAttribute.Syntax);
                            }
                        }

                        if (commands.Count == 0)
                        {
                            _plugin.logger.Warning($"Unable to find localized command names: {method.Name}"); // TODO: Localization
                        }
                    }
                    else
                    {
                        if (descriptionAttribute != null)
                        {
                            if (descriptionMessages == null)
                            {
                                descriptionMessages = new Dictionary<string, string>();
                            }

                            descriptionMessages.Add(Lang.DefaultLang, descriptionAttribute.Description);
                        }

                        if (helpAttribute != null)
                        {
                            if (helpMessages == null)
                            {
                                helpMessages = new Dictionary<string, string>();
                            }

                            helpMessages.Add(Lang.DefaultLang, helpAttribute.Syntax);
                        }
                    }
                }

                if (commands.Count > 0)
                {
                    CommandDefinition definition = new CommandDefinition(commands);
                    _plugin.Commands.Add(new CommandInfo(definition, _plugin.Commands.CreateCallback(definition, method.Name), cmdPerm?.Permission, descriptionMessages, helpMessages));

                    _plugin.Dispatcher.AddHookMethod(method.Name, method, false, context, method.GetCustomAttributes<GateAttribute>());
                }
            }
        }

        /// <summary>
        /// Gets the internal alias of a configuration schema
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        private string GetConfigTypeName(ConfigAttribute attribute, string prefix)
        {

            List<string> nameParts = Pools.GetList<string>();

            try
            {
                nameParts.Add(prefix);
                if (!string.IsNullOrEmpty(attribute.Name))
                {
                    nameParts.Add(attribute.Name);
                }

                if (!string.IsNullOrEmpty(attribute.Version))
                {
                    nameParts.Add(attribute.Version);
                }

                return string.Join(".", nameParts.ToArray());
            }
            finally
            {
                Pools.FreeList(ref nameParts);
            }
        }

        private void ResolveConfiguration()
        {
            Type type = _plugin.GetType();
            MethodInfo config = type.GetMethod("LoadDefaultConfig", DefaultBindingFlags);
            if (config?.DeclaringType != typeof(Plugin))
            {
                _plugin.Meta.Configuration = _plugin.Meta.Configuration.Add(ConfigurationType.Legacy);
            }

            IEnumerable<Type> configTypes = type.GetNestedTypesWithAttribute<ConfigAttribute>(DefaultBindingFlags);
            if (configTypes.Any())
            {
                _plugin.Meta.Configuration = _plugin.Meta.Configuration.Add(ConfigurationType.Dynamic);
            }

            foreach (Type configType in configTypes)
            {
                if (configType.GetCustomAttribute<ConfigAttribute>() is ConfigAttribute configAttribute)
                {
                    _plugin.Meta.Mediator.Mediate(configType, GetConfigTypeName(configAttribute, "Config"));
                }
            }
        }

        private void ResolveLocalization()
        {
            Type type = _plugin.GetType();
            MethodInfo messages = type.GetMethod("LoadDefaultMessages", DefaultBindingFlags);
            if (messages?.DeclaringType != typeof(Plugin))
            {
                _plugin.Meta.Localization = _plugin.Meta.Localization.Add(LocalizationType.Lang);
            }

            IEnumerable<Type> localeTypes = type.GetNestedTypesWithAttribute<LocalizationAttribute>(DefaultBindingFlags);
            if (localeTypes.Any())
            {
                _plugin.Meta.Localization = _plugin.Meta.Localization.Add(LocalizationType.Locale);
            }

            foreach (Type localeType in localeTypes)
            {
                if (localeType.GetCustomAttribute<LocalizationAttribute>() is LocalizationAttribute localizationAttribute)
                {
                    if (!string.IsNullOrEmpty(localizationAttribute.Name))
                    {
                        _plugin.Meta.Mediator.Mediate(localeType, $"Locale.{localizationAttribute.Name}.{localizationAttribute.Version}");
                    }
                    else
                    {
                        _plugin.Meta.Mediator.Mediate(localeType, $"Locale.{localizationAttribute.Version}");
                    }

                    foreach (Type localeImpl in type.GetNestedTypesOfInterface(localeType, DefaultBindingFlags))
                    {
                        if (localeImpl.GetCustomAttribute<LocaleAttribute>() is LocaleAttribute localeAttribute)
                        {
                            if (!string.IsNullOrEmpty(localizationAttribute.Name))
                            {
                                _plugin.Meta.Mediator.Mediate(localeImpl, $"Locale.{localizationAttribute.Name}.{localeAttribute.Locale}.{localizationAttribute.Version}");
                            }
                            else
                            {
                                _plugin.Meta.Mediator.Mediate(localeImpl, $"Locale.{localeAttribute.Locale}.{localizationAttribute.Version}");
                            }
                        }
                    }
                }
            }
        }

        private void ResolveReferences()
        {
            foreach (Type subType in _plugin.GetType().GetNestedTypesWithAttribute<ReferenceAttribute>(DefaultBindingFlags))
            {
                foreach (ReferenceAttribute refAttr in subType.GetCustomAttributes<ReferenceAttribute>())
                {
                    string typeName = !string.IsNullOrEmpty(refAttr.Name) ? refAttr.Name : subType.Name;
                    refAttr.Name = Utility.ReflectedName(typeName);
                    _plugin.Meta.Mediator.Mediate(subType, typeName);
                }
            }
        }

        /// <summary>
        /// Resolves model types for the service container
        /// </summary>
        private void ResolveModels()
        {
            Type type = _plugin.GetType();
            List<Type> modelInterfaces = new List<Type>();
            modelInterfaces.AddRange(type.GetNestedTypesWithAttribute<ModelAttribute>(DefaultBindingFlags));

            foreach (Type modelInterface in modelInterfaces)
            {
                if (modelInterface.IsInterface)
                {
                    foreach (Type subType in type.GetNestedTypesOfInterface(modelInterface, DefaultBindingFlags))
                    {
                        BindModelType(type, modelInterface, subType);
                    }
                }
                else
                {
                    BindModelType(type, modelInterface);
                }
            }
        }

        /// <summary>
        /// Resolve basic type converter methods
        /// </summary>
        private void ResolveConverters()
        {
            Type type = _plugin.GetType();
            List<MethodInfo> converterMethods = new List<MethodInfo>();
            converterMethods.AddRange(type.GetMethodsWithAttribute<ConverterAttribute>(DefaultBindingFlags));

            foreach (MethodInfo converterMethod in converterMethods)
            {
                ParameterInfo[] parameters = converterMethod.GetParameters();
                if (parameters.Length > 0)
                {
                    _application.AddConverter(parameters[0].ParameterType, converterMethod.ReturnType);
                    _application.When(type).Needs(converterMethod.ReturnType).Bind(converterMethod);

                    if (converterMethod.ReturnType.IsClass && converterMethod.ReturnType.DeclaringType == type)
                    {
                        _plugin.Meta.Mediator.Mediate(converterMethod.ReturnType);
                    }
                }
            }
        }

        /// <summary>
        /// Binds model types to the service container
        /// </summary>
        /// <param name="type"></param>
        /// <param name="modelInterface"></param>
        /// <param name="subType"></param>
        private void BindModelType(Type type, Type modelInterface, Type subType = null)
        {
            if (subType == null)
            {
                subType = modelInterface;
            }

            IEnumerable<MethodInfo> methods = subType.GetMethods(StaticBindingFlags).Where(x => x.ReturnType == modelInterface && x.Name.ToLower().StartsWith("resolve"));

            int resolveMethodCount = 0;
            List<Type> boundTypes = new List<Type>();
            foreach (MethodInfo method in methods)
            {
                ParameterInfo[] parameters = method.GetParameters();
                if (parameters.Length > 0)
                {
                    _application.AddConverter(parameters[0].ParameterType, subType);
                    _application.When(type).Needs(subType).Bind(method);
                    boundTypes.Add(subType);

                    if (subType != modelInterface)
                    {
                        _application.AddConverter(parameters[0].ParameterType, modelInterface);
                        _application.When(type).Needs(modelInterface).Bind(method);
                        boundTypes.Add(modelInterface);
                    }

                    resolveMethodCount++;
                }
            }

            foreach (Type boundType in boundTypes)
            {
                _plugin.Meta.Mediator.Mediate(boundType);
            }
        }

        private void ResolvePluginReferences()
        {
            Type type = _plugin.GetType();
            foreach (FieldInfo field in type.GetFields(PrivateInstanceBindingFlags))
            {
                if (field.DeclaringType != type ||
                    !(field.GetCustomAttribute<PluginReferenceAttribute>() is PluginReferenceAttribute pluginReference))
                {
                    continue;
                }

                PluginMeta.PluginReferenceType pluginReferenceType = PluginMeta.PluginReferenceType.Optional;

                if (pluginReference is RequiresAttribute)
                {
                    pluginReferenceType = PluginMeta.PluginReferenceType.Requires;
                }
                else if (pluginReference is DebugAttribute)
                {
                    pluginReferenceType = PluginMeta.PluginReferenceType.Debug;
                }

                PluginMeta.PluginReference pluginReferenceImpl = new PluginMeta.PluginReference(_plugin, pluginReference.Names ?? new[] { field.Name }, field, pluginReferenceType);

                if (pluginReference.Names == null)
                {
                    _plugin.Meta.PluginReferenceFields[field.Name] = pluginReferenceImpl;
                }
                else
                {
                    foreach (string name in pluginReference.Names)
                    {
                        _plugin.Meta.PluginReferenceFields[name] = pluginReferenceImpl;
                    }
                }
            }
        }

        private void ResolveImplicitHooks()
        {
            Type type = _plugin.GetType();
            foreach (MethodInfo method in type.GetMethods(PrivateInstanceBindingFlags))
            {
                if (method.GetCustomAttribute<HookAttribute>() == null && method.GetCustomAttribute<CommandAttribute>() == null)
                {
                    if (method.Name.Equals("OnFrame"))
                    {
                        _plugin.HookedOnFrame = true;
                    }

                    // Assume all private instance methods which are not explicitly hooked could be hooks
                    if (method.DeclaringType?.Name == type.Name)
                    {
                        _plugin.Dispatcher.AddHookMethod(Utility.ReflectedName(method.Name), method, null, method.GetCustomAttributes(typeof(GateAttribute), true) as IEnumerable<GateAttribute>);
                    }
                }
            }
        }

        private void ResolveExplicitHooks(Type type)
        {
            foreach (MethodInfo method in type.GetMethodsWithAttribute<HookAttribute>(DefaultBindingFlags))
            {
                if (method.GetCustomAttribute<HookAttribute>() is HookAttribute hookMethodAttribute)
                {
                    AsyncAttribute asyncAttribute = method.GetCustomAttribute<AsyncAttribute>();
                    if (string.IsNullOrEmpty(hookMethodAttribute.Name))
                    {
                        hookMethodAttribute.Name = Utility.ReflectedName(method.Name);
                    }
                    if (asyncAttribute != null)
                    {
                        _plugin.Dispatcher.AddHookAsync(hookMethodAttribute.FullName, method, null, method.GetCustomAttributes<GateAttribute>());
                    }
                    else
                    {
                        _plugin.Dispatcher.AddHookMethod(hookMethodAttribute.FullName, method, null, method.GetCustomAttributes<GateAttribute>());
                    }
                }
            }
        }

        internal void ResolveGates()
        {
            Type type = _plugin.GetType();
            foreach (Type subType in type.GetNestedTypesOfInterface<IGate>(DefaultBindingFlags))
            {
                if (!_application.Resolved(subType))
                {
                    _plugin.Meta.Mediator.Mediate(subType, $"Gate.{subType.Name}");
                }

                object[] constructorArguments = ArrayPool.Get(1);
                constructorArguments[0] = _plugin;
                try
                {
                    IGate gate = _application.Make<IGate>(subType, constructorArguments);

                    if (gate != null)
                    {
                        _application.When(type).Needs(subType).Bind(gate);
                    }
                }
                finally
                {
                    ArrayPool.Free(constructorArguments);
                }
            }
        }

        private void ResolveSingletons()
        {
            foreach (Type subType in _plugin.GetType().GetNestedTypesOfInterface<ISingleton>(DefaultBindingFlags))
            {
                if (!_application.Resolved(subType))
                {
                    _plugin.Meta.Mediator.Mediate(subType, subType.Name);
                }
            }
        }

        internal void ResolveHookDecorators()
        {
            IEnumerable<HookDecoratorAttribute> pluginDecorators = _plugin.GetType().GetCustomAttributes<HookDecoratorAttribute>();
            foreach (HookDecoratorAttribute hookDecoratorAttribute in pluginDecorators)
            {
                object[] constructorParams = null;
                foreach (Type decoratorType in hookDecoratorAttribute.DecoratorTypes)
                {
                    try
                    {
                        constructorParams = ArrayPool.Get(1);
                        constructorParams[0] = _plugin;

                        IHookDecorator decorator = _application.Make<IHookDecorator>(decoratorType, constructorParams);

                        _plugin.Dispatcher.AddHookDecorator(new HookDispatcher.HookDecorator(decorator, hookDecoratorAttribute.AutoRegister));
                    }
                    finally
                    {
                        ArrayPool.Free(constructorParams);
                    }
                }
            }

            foreach (Type subType in _plugin.GetType().GetNestedTypesWithAttribute<HookDecoratorAttribute>(DefaultBindingFlags))
            {
                AddCustomHookDecorator(subType);
            }

            if (_plugin.Meta.OnlinePlayerFields.Count > 0)
            {
                _plugin.Dispatcher.AddHookDecorator(new HookDispatcher.HookDecorator(new OnlinePlayersDecorator(_plugin)));
            }

            if (_plugin.Meta.PluginReferenceFields.Count > 0)
            {
                _plugin.Dispatcher.AddHookDecorator(new HookDispatcher.HookDecorator(new DependencyDecorator(_plugin)));
            }
        }

        private void AddCustomHookDecorator(Type subType)
        {
            HookDecoratorAttribute hookDecoratorAttribute = subType.GetCustomAttribute<HookDecoratorAttribute>();
            object[] constructorParams = null;
            try
            {
                if (!_application.Resolved(subType))
                {
                    _plugin.Meta.Mediator.Mediate(subType, subType.Name);
                }

                Type factoryType = subType;
                if (!typeof(IHookDecorator).IsAssignableFrom(subType) && !_application.TryGetType("Behavior", out factoryType))
                {
                    return;
                }
                constructorParams = ArrayPool.Get(1);
                constructorParams[0] = _plugin;

                IHookDecorator decorator = _application.Make<IHookDecorator>(factoryType, constructorParams);
                if (decorator != null)
                {
                    _plugin.Dispatcher.AddHookDecorator(new HookDispatcher.HookDecorator(decorator, subType, hookDecoratorAttribute.AutoRegister));
                }
            }
            finally
            {
                if (constructorParams != null)
                {
                    ArrayPool.Free(constructorParams);
                }
            }
        }
    }
}
