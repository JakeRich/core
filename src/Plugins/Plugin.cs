using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using uMod.Command;
using uMod.Common;
using uMod.Common.Plugins;
using uMod.Configuration;
using uMod.Exceptions;
using uMod.Libraries;

#pragma warning disable 618

namespace uMod.Plugins
{
    [Obsolete("Use Plugin instead")]
    public abstract class CSPlugin : Plugin
    {
    }

    [Obsolete("Use Plugin instead")]
    public abstract class CSharpPlugin : Plugin
    {
    }

    [Obsolete("Use Plugin instead")]
    public abstract class CovalencePlugin : Plugin
    {
    }

    [Obsolete("Use Plugin instead")]
    public abstract class UniversalPlugin : Plugin
    {
    }

    /// <summary>
    /// Represents a single plugin
    /// </summary>
    public abstract class Plugin : BasePlugin, IPlugin
    {
        public static implicit operator bool(Plugin plugin) => plugin != null;

        public static bool operator !(Plugin plugin) => !(bool)plugin;

        /// <summary>
        /// Gets the library by the specified type or name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T GetLibrary<T>(string name = null) where T : Library => Interface.uMod.Libraries.Get<T>(name);

        /// <summary>
        /// Plugin lock scope
        /// </summary>
        internal object Lock = new object();

        /// <summary>
        /// Gets the source file name, if any
        /// </summary>
        public string Filename { get; protected set; }

        /// <summary>
        /// Gets the internal name of this plugin
        /// </summary>
        private string _name;

        public string Name
        {
            get => _name;
            set => _name = GetType().Name;
        }

        /// <summary>
        /// Gets the user-friendly title of this plugin
        /// </summary>
        public string Title { get; protected set; }

        /// <summary>
        /// Gets the description of this plugin
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// Gets the author of this plugin
        /// </summary>
        public string Author { get; protected set; }

        /// <summary>
        /// Gets the version of this plugin
        /// </summary>
        public VersionNumber Version { get; protected set; }

        /// <summary>
        /// Gets the resource id of this plugin
        /// </summary>
        [Obsolete]
        public int ResourceId { get; protected set; }

        /// <summary>
        /// Gets the plugin manager responsible for this plugin
        /// </summary>
        internal IPluginManager Manager; //TODO: duplicate of "plugins"

        [Obsolete("Use universal instead")]
        protected Universal covalence = Interface.uMod.Libraries.Get<Universal>();

        protected Universal universal = Interface.uMod.Libraries.Get<Universal>();
        protected Lang lang = Interface.uMod.Libraries.Get<Lang>();
        protected Permission permission = Interface.uMod.Libraries.Get<Permission>();
        protected IPluginManager plugins = Interface.uMod.Plugins.Manager;

        [Obsolete("Use Web instead")]
        protected WebRequests webrequest = Interface.uMod.Libraries.Get<WebRequests>();

        protected PluginTimers timer;

        [Obsolete("Use Game instead")]
        protected string game = Interface.uMod.Libraries.Get<Universal>().Game;

        protected string Game = Interface.uMod.Libraries.Get<Universal>().Game;

        [Obsolete("Use Players instead")]
        protected IPlayerManager players = Interface.uMod.Libraries.Get<Universal>().Players;

        protected IPlayerManager Players = Interface.uMod.Libraries.Get<Universal>().Players;

        [Obsolete("Use Server instead")]
        protected IServer server = Interface.uMod.Libraries.Get<Universal>().Server;

        protected IServer Server = Interface.uMod.Libraries.Get<Universal>().Server;

        public bool HookedOnFrame
        {
            get; internal set;
        }

        private bool _resolved;

        internal readonly PermissionGate gate;
        protected PermissionGate Gate => gate;

        internal ILogger logger;
        protected ILogger Logger;
        protected readonly PluginLocale Locales;
        protected readonly PluginLang Lang;
        protected readonly Web.Client Web;
        protected readonly Database.Client Database;
        protected readonly PluginFiles Files;
        internal readonly HookDispatcher Dispatcher;
        internal readonly PluginAttributeResolver AttributeResolver;
        internal readonly PluginMeta Meta;
        private readonly PluginDiagnostics _diagnostics;
        public IPluginDiagnostics Diagnostics => _diagnostics;

        /// <summary>
        /// Gets if this plugin should never be unloaded
        /// </summary>
        private bool _isCorePlugin;

        public bool IsCorePlugin
        {
            get => _isCorePlugin;
            set
            {
                if (Loader == null)
                {
                    return;
                }

                if (!Loader.HasLoadedCorePlugins)
                {
                    _isCorePlugin = value;
                }
            }
        }

        /// <summary>
        /// Gets the PluginLoader which loaded this plugin
        /// </summary>
        public IPluginLoader Loader { get; set; }

        /// <summary>
        /// Gets the object associated with this plugin
        /// </summary>
        public virtual object Object => this;

        /// <summary>
        /// Gets the config file in use by this plugin
        /// </summary>
        [Obsolete("Use configuration schematics instead")]
        public DynamicConfigFile Config { get; internal set; }

        /// <summary>
        /// Called when this plugin has raised an error
        /// </summary>
        public IEvent<IPlugin, string> OnError { get; } = new Event<IPlugin, string>();

        /// <summary>
        /// Called when this plugin has raised an exception
        /// </summary>
        public IEvent<IPlugin, string, Exception> OnException { get; } = new Event<IPlugin, string, Exception>();

        /// <summary>
        /// Called when this plugin has raised an exception
        /// </summary>
        public IEvent<IPlugin, string> OnWarning { get; } = new Event<IPlugin, string>();

        /// <summary>
        /// Called when plugin files are resolved
        /// </summary>
        public IEvent<IPlugin> OnResolved { get; } = new Event<IPlugin>();

        /// <summary>
        /// Called when this plugin is added to a manager
        /// </summary>
        public IContextManagerEvent<IPlugin, IPluginManager> OnAddedToManager { get; } = new PluginManagerEvent();

        /// <summary>
        /// Called when this plugin is removed from a manager
        /// </summary>
        public IContextManagerEvent<IPlugin, IPluginManager> OnRemovedFromManager { get; } = new PluginManagerEvent();

        /// <summary>
        /// Has this plugins Init/Loaded hook been called
        /// </summary>
        public bool IsLoaded { get; internal set; }

        // Used to callback configuration status of asynchronous file operations
        private bool _configLoaded;
        private bool _langLoaded;

        public bool IsResolved
        {
            get
            {
                bool hasConfig = !Meta.Configuration.Is(ConfigurationType.None);
                bool hasMessages = !Meta.Localization.Is(LocalizationType.None);

                return (!hasConfig || (_configLoaded)) && (!hasMessages || (_langLoaded));
            }
        }

        public IPluginCommandHandler Commands => _commands;

        private readonly PluginCommands _commands;

        /// <summary>
        /// Create a new instance of the Plugin class
        /// </summary>
        protected Plugin()
        {
            Name = GetType().Name;
            Title = Name.Humanize();
            Author = "Unnamed";
            Version = new VersionNumber(1, 0, 0);

            IApplication application = Interface.uMod.Application;
            ILogger rootLogger = Interface.uMod.RootLogger;
            AttributeResolver = new PluginAttributeResolver(this, Lang = new PluginLang(this), Locales = new PluginLocale(this), application);

            gate = new PermissionGate(this);
            Web = new Web.Client(this);
            Database = new Database.Client(Interface.uMod.Database, this);
            Dispatcher = new HookDispatcher(this, rootLogger);
            _commands = new PluginCommands(this, rootLogger, Interface.uMod);
            timer = new PluginTimers(this);
            Files = new PluginFiles(this);
            Meta = new PluginMeta(this, Files, application, Interface.uMod.Dispatcher, rootLogger, Interface.uMod.Logging);
            _diagnostics = new PluginDiagnostics(this);

            Type type = GetType();

            application.When(type).Needs(typeof(PluginTimers)).Bind(timer);
            application.When(type).Needs(typeof(PermissionGate)).Bind(gate);
            application.When(type).Needs(typeof(PluginLocale)).Bind(Locales);
            application.When(type).Needs(typeof(PluginLang)).Bind(Lang);
            application.When(type).Needs(typeof(Web.Client)).Bind(Web);
            application.When(type).Needs(typeof(Database.Client)).Bind(Database);

            AttributeResolver.Resolve();
        }

        /// <summary>
        /// Determine if initial asynchronous loading is successfully resolved
        /// </summary>
        private void CheckResolved()
        {
            if (!IsResolved)
            {
                return;
            }

            if (Initialize())
            {
                OnResolved.Invoke(this);
            }
        }

        /// <summary>
        /// Checks if this plugin is subscribed to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        protected bool IsSubscribed(string hook) => Manager.IsSubscribedToHook(hook, this);

        /// <summary>
        /// Checks if this plugin is subscribed to the specified hook using a method that has any parameter from the array of supplied types
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="parameterFilter"></param>
        internal bool IsSubscribedWith(string hook, Type[] parameterFilter) => Manager.IsSubscribedToHook(hook, this, parameterFilter);

        /// <summary>
        /// Subscribes this plugin to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        protected void Subscribe(string hook)
        {
            if (Dispatcher.HookDecorators.TryGetValue(hook, out HookDispatcher.HookDecorator decorator))
            {
                IEnumerable<string> hookKeys = decorator.Decorator.Register();
                foreach (string hookKey in hookKeys)
                {
                    Manager.SubscribeToHook(hookKey, this);
                }
                return;
            }

            HookName hookName;
            lock (HookName.Cache.HookNameLock)
            {
                hookName = HookName.Cache.Get(hook);
            }

            if (hookName.IsNamespaced)
            {
                Dispatcher.AddExplicitHookByName(hookName.fullyQualifiedHookName);
                Manager.SubscribeToHook(hookName.name, this);
                return;
            }

            Manager.SubscribeToHook(hook, this);
        }

        /// <summary>
        /// Unsubscribes this plugin to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        protected void Unsubscribe(string hook)
        {
            if (Dispatcher.HookDecorators.TryGetValue(hook, out HookDispatcher.HookDecorator decorator))
            {
                decorator.Decorator.Unregister();
                return;
            }

            HookName hookName;
            lock (HookName.Cache.HookNameLock)
            {
                hookName = HookName.Cache.Get(hook);
            }

            if (hookName.IsNamespaced)
            {
                Dispatcher.RemoveHookMethod(hookName);
                return;
            }

            Manager.UnsubscribeToHook(hook, this);
        }

        /// <summary>
        /// Called when this plugin has been added to the specified manager
        /// </summary>
        /// <param name="manager"></param>
        public virtual void HandleAddedToManager(IPluginManager manager)
        {
            if (manager != null)
            {
                Manager = manager;
            }

            if (logger == null)
            {
                Logger = logger = Meta.LoadLogger();
                Interface.uMod.Application.When(GetType()).Needs(typeof(ILogger)).Bind(Logger);
            }

            OnAddedToManager?.Invoke(this, manager ?? Manager);

            bool shouldResolve = false;
            if (Meta.Configuration.Is(ConfigurationType.Legacy))
            {
                LoadConfig();
            }
            else if (Meta.Configuration.Is(ConfigurationType.Dynamic))
            {
                LoadConfigAsync();
                shouldResolve = true;
            }

            if (Meta.Localization.Is(LocalizationType.Locale))
            {
                LoadLocalization();
                shouldResolve = true;
            }
            else if (Meta.Localization.Is(LocalizationType.Lang))
            {
                LoadDefaultMessages();
                _langLoaded = true;
            }

            if (!shouldResolve)
            {
                CheckResolved();
            }
        }

        internal bool Initialize()
        {
            if (Manager != null)
            {
                // Load dependency fields
                foreach (string referenceFieldName in Meta.PluginReferenceFields.Keys)
                {
                    /*if (Interface.uMod.Plugins.Compiler.IsPending(referenceFieldName))
                    {
                        Interface.uMod.NextTick(CheckResolved);
                        return false;
                    }*/

                    IPlugin dependentPlugin = Manager.GetPlugin(referenceFieldName);
                    if (dependentPlugin != null)
                    {
                        Meta.PluginReferenceFields[referenceFieldName].Loaded(dependentPlugin);
                    }
                }

                // Validate requirements
                IEnumerable<PluginMeta.PluginReference> missingRequirements = Meta.PluginReferenceFields.Where(x =>
                {
                    return x.Value.Type == PluginMeta.PluginReferenceType.Requires && !x.Value.IsLoaded();
                }).Select(x => x.Value).Distinct();

                if (missingRequirements.Any())
                {
                    List<string> messagesList = new List<string>();
                    foreach (PluginMeta.PluginReference pluginReference in missingRequirements)
                    {
                        messagesList.Add(string.Join(" or ", pluginReference.Names));
                    }

                    string dependencyNoun = Interface.uMod.Strings.Compiler.Dependency.Choice(messagesList.Count);
                    string message = Interface.uMod.Strings.Compiler.RequirementsMissing.Interpolate(
                        ("dependency", dependencyNoun),
                        ("requirements", string.Join(", ", messagesList.ToArray()))
                    );
                    Interface.uMod.LogError(Interface.uMod.Strings.Plugin.InitializeFailure.Interpolate(
                                                ("name", Name),
                                                ("version", Version)
                                            ) + $": {message}");
                    if (Loader != null)
                    {
                        Loader.PluginErrors[Name] = message;
                    }

                    return true;
                }
            }

            AttributeResolver.ResolveCommands(this);
            AttributeResolver.ResolveHookDecorators();
            AttributeResolver.ResolveGates();
            _commands.Register();

            foreach (KeyValuePair<string, HookDispatcher.HookDecorator> kvp in Dispatcher.HookDecorators)
            {
                if (kvp.Value.AutoRegister)
                {
                    kvp.Value.Decorator.Register();
                }
            }

            // Subscribe hooks
            foreach (string hookName in Dispatcher.Hooks.Keys)
            {
                Subscribe(hookName);
            }

            try
            {
                // Let the plugin know that it is loading
                CallHook("Init");
            }
            catch (Exception ex)
            {
                Interface.uMod.LogException(Interface.uMod.Strings.Plugin.InitializeFailure.Interpolate(("name", Name), ("version", Version)), ex);
                if (Loader != null)
                {
                    Loader.PluginErrors[Name] = ex.Message;
                }

                return true;
            }

            if (Filename != null)
            {
                foreach (IChangeWatcher watcher in Interface.uMod.Extensions.Manager.GetChangeWatchers())
                {
                    watcher.AddMapping(Name);
                }
            }

            try
            {
                CallHook("Loaded");
            }
            catch (Exception ex)
            {
                Interface.uMod.LogException(Interface.uMod.Strings.Plugin.InitializeFailure.Interpolate(("name", Name), ("version", Version)), ex);
                if (Loader != null)
                {
                    Loader.PluginErrors[Name] = ex.Message;
                }
            }

            return true;
        }

        /// <summary>
        /// Called when this plugin has been removed from the specified manager
        /// </summary>
        /// <param name="manager"></param>
        public virtual void HandleRemovedFromManager(IPluginManager manager)
        {
            if (IsLoaded)
            {
                CallHook("Unload");
            }

            foreach (IChangeWatcher watcher in Interface.uMod.Extensions.Manager.GetChangeWatchers())
            {
                watcher.RemoveMapping(Name);
            }

            foreach (string name in Meta.PluginReferenceFields.Keys)
            {
                Meta.PluginReferenceFields[name].Unloaded();
            }

            _commands.Unregister();

            foreach (KeyValuePair<string, HookDispatcher.HookDecorator> kvp in Dispatcher.HookDecorators)
            {
                kvp.Value.Decorator.Unregister();
            }

            Meta.Mediator.Unbind();

            if (Manager == manager)
            {
                Manager = null;
            }

            OnRemovedFromManager?.Invoke(this, manager);

            Interface.uMod.Application.Unbind(this);
        }

        /// <summary>
        /// Called when this plugin is loaded
        /// </summary>
        public virtual void Load(IPluginLoader loader)
        {
            IsLoaded = true;
        }

        /// <summary>
        /// Called when this plugin is unloaded
        /// </summary>
        /// <param name="loader"></param>
        public virtual void Unload(IPluginLoader loader)
        {
            IsLoaded = false;
        }

        #region CallHook

        public override object CallHook(HookName hookName, object[] args = null, HookEvent e = null)
        {
            int startedAt = 0;
            if (!_isCorePlugin && _diagnostics.HookDepth == 0)
            {
                _diagnostics.PreHookGCCount = GC.CollectionCount(0);
                startedAt = Environment.TickCount;
                if (_diagnostics.AverageHookTime < 1)
                {
                    _diagnostics.AverageHookTime = startedAt;
                }
            }

            _diagnostics.HookDepth++;

            try
            {
                return Dispatcher.Dispatch(hookName, args, e);
            }
            catch (Exception ex)
            {
                Interface.uMod.LogException(Interface.uMod.Strings.Plugin.HookFailure.Interpolate(
                    ("hook", hookName),
                    ("name", Name),
                    ("version", Version)
                ), ex);
                e?.Fail(ex);

                if (!hookName.isEvent)
                {
                    if (Manager.IsSubscribedToHook(hookName.FailedHookName.fullyQualifiedHookName))
                    {
                        Manager.CallHook(hookName.FailedHookName, args, e);
                    }

                    e?.context?.Invoke(e);
                }

                return null;
            }
            finally
            {
                _diagnostics.HookDepth--;
                if (startedAt > 0)
                {
                    int duration = Environment.TickCount - startedAt;
                    _diagnostics.TotalHookTime += duration / 1000; // Approximate duration in seconds (1 tick = 1 millisecond)
                    if (duration > 100)
                    {
                        string suffix = _diagnostics.PreHookGCCount == GC.CollectionCount(0) ? string.Empty : " [GARBAGE COLLECT]";
                        Interface.uMod.LogWarning($"Calling '{hookName}' on '{Name} v{Version}' took ~{duration}ms{suffix}");
                    }

                    double total = _diagnostics.HookTimeSum + duration;
                    double endedAt = startedAt + duration;
                    if (endedAt - _diagnostics.AverageHookTime > 100)
                    {
                        total /= endedAt - _diagnostics.AverageHookTime;
                        if (total > 0.1)
                        {
                            string suffix = _diagnostics.PreHookGCCount == GC.CollectionCount(0) ? string.Empty : " [GARBAGE COLLECT]";
                            Interface.uMod.LogWarning($"Calling '{hookName}' on '{Name} v{Version}' took average ~{_diagnostics.HookTimeSum:0}ms{suffix}");
                        }
                        _diagnostics.HookTimeSum = 0;
                        _diagnostics.AverageHookTime = 0;
                    }
                    else
                    {
                        _diagnostics.HookTimeSum = total;
                    }
                }
            }
        }

        #endregion CallHook

        #region Configuration

        [Obsolete("Use configuration schematics instead")]
        protected virtual void LoadConfig()
        {
            Config = new DynamicConfigFile(Path.Combine(Interface.uMod.ConfigDirectory, $"{Name}.json"));
            if (!Config.Exists())
            {
                LoadDefaultConfig();
                SaveConfig();
                _configLoaded = true;
            }
            try
            {
                Config.Load();
                _configLoaded = true;
            }
            catch (Exception ex)
            {
                OnException.Invoke(this, $"Failed to load config file (is the config file corrupt?) ({ex.Message})", ex); // TODO: Localization
            }
        }

        /// <summary>
        /// Loads the config file for this plugin
        /// </summary>
        private void LoadConfigAsync()
        {
            Meta.LoadConfiguration().Done(delegate
            {
                _configLoaded = true;
                CheckResolved();
            }, delegate (Exception exception)
            {
                if (exception is LegacyConfigFileNotFoundException)
                {
                    try
                    {
                        LoadDefaultConfig();
                        SaveConfig();
                        Config.Load();
                        _configLoaded = true;
                        CheckResolved();
                        return;
                    }
                    catch (Exception ex2)
                    {
                        Loader.PluginErrors[Name] = ex2.Message;
                        OnException.Invoke(this, ex2.Message, ex2);
                    }
                }
                else
                {
                    Loader.PluginErrors[Name] = exception.Message;
                    OnException.Invoke(this, exception.Message, exception);
                }

                OnResolved.Invoke(this);
            });
        }

        /// <summary>
        /// Populates the config with default settings
        /// </summary>
        protected virtual void LoadDefaultConfig() => CallHook("LoadDefaultConfig", null);

        /// <summary>
        /// Saves the config file for this plugin
        /// </summary>
        private void SaveConfigAsync()
        {
            Meta.SaveConfig().Fail(delegate (Exception exception)
            {
                if (Meta.Configuration.Is(ConfigurationType.Legacy))
                {
                    OnException.Invoke(this, "Failed to save config file (does the config have illegal objects in it?)", exception); // TODO: Localization
                }
            });
        }

        [Obsolete("Use configuration schematics instead")]
        protected virtual void SaveConfig()
        {
            if (Config == null)
            {
                return;
            }

            try
            {
                Config.Save();
            }
            catch (Exception ex)
            {
                OnException.Invoke(this, $"Failed to save config file (does the config have illegal objects in it?) ({ex.Message})", ex); // TODO: Localization
            }
        }

        #endregion Configuration

        #region Localization

        private void LoadLocalization()
        {
            Meta.LoadLocalization().Done(delegate (IEnumerable<object> locales)
            {
                if (locales != null)
                {
                    foreach (object localeObj in locales)
                    {
                        Locales.Register(localeObj as ILocale);
                    }
                }

                _langLoaded = true;
                CheckResolved();
            }, delegate (Exception exception)
            {
                OnException.Invoke(this, exception.Message, exception);
            });
        }

        /// <summary>
        /// Populates the lang file(s) with default messages
        /// </summary>
        protected virtual void LoadDefaultMessages() => CallHook("LoadDefaultMessages");

        #endregion Localization

        #region Universal

        [Obsolete("Use Commands.Add instead")]
        public void AddCovalenceCommand(string command, string callback, string perm = null)
        {
            AddUniversalCommand(command, callback, perm);
        }

        [Obsolete("Use Commands.Add instead")]
        public void AddUniversalCommand(string command, string callback, string perm = null)
        {
            AddUniversalCommand(new[] { command }, callback, string.IsNullOrEmpty(perm) ? null : new[] { perm });
        }

        [Obsolete("Use Commands.Add instead")]
        public void AddCovalenceCommand(string[] commands, string callback, string perm)
        {
            AddUniversalCommand(commands, callback, perm);
        }

        [Obsolete("Use Commands.Add instead")]
        public void AddUniversalCommand(string[] commands, string callback, string perm)
        {
            AddUniversalCommand(commands, callback, string.IsNullOrEmpty(perm) ? null : new[] { perm });
        }

        [Obsolete("Use Commands.Add instead")]
        public void AddCovalenceCommand(string[] commands, string callback, string[] perms = null)
        {
            AddUniversalCommand(commands, callback, perms);
        }

        [Obsolete("Use Commands.Add instead")]
        public void AddUniversalCommand(string[] commands, string callback, string[] perms = null)
        {
            CommandDefinition commandDefinition = new CommandDefinition(commands);
            _commands.Add(new CommandInfo(commandDefinition, _commands.CreateCallback(commandDefinition, callback), perms));
        }

        #endregion Universal

        #region Plugin Info

        /// <summary>
        /// Resolves core plugin info and attributes
        /// </summary>
        /// <param name="loader"></param>
        /// <param name="name"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public virtual bool Resolve(IPluginLoader loader, string name = null, string fileName = null)
        {
            if (_resolved)
            {
                return true;
            }

            Loader = loader;
            if (loader.CorePlugins.Contains(GetType()))
            {
                IsCorePlugin = true;
            }

            if (!string.IsNullOrEmpty(name))
            {
                Name = name;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                Filename = fileName;
            }

            return _resolved = ResolveAttributes();
        }

        private bool ResolveAttributes()
        {
            if (GetType().GetCustomAttribute<InfoAttribute>() is InfoAttribute infoAttribute)
            {
                Title = infoAttribute.Title;
                Author = infoAttribute.Author;
                Version = infoAttribute.Version;
#pragma warning disable 612
                ResourceId = infoAttribute.ResourceId;
#pragma warning restore 612

                if (!string.IsNullOrEmpty(infoAttribute.InvalidVersion))
                {
                    Interface.uMod.LogWarning(Interface.uMod.Strings.Plugin.InvalidVersion.Interpolate(
                        ("version", infoAttribute.InvalidVersion),
                        ("plugin", Title)
                    ));
                }
            }
            else if (!_isCorePlugin)
            {
                Interface.uMod.LogWarning(Interface.uMod.Strings.Plugin.InfoMissing.Interpolate("name", _name));
                return false;
            }

            if (GetType().GetCustomAttribute<DescriptionAttribute>() is DescriptionAttribute descriptionAttribute)
            {
                Description = descriptionAttribute.Description;
            }
            else if (!_isCorePlugin)
            {
                Interface.uMod.LogWarning(Interface.uMod.Strings.Plugin.DescriptionMissing.Interpolate("name", _name));
            }

            return true;
        }

        #endregion Plugin Info

        #region Localization

        /// <summary>
        /// Get locale of generic type and/or for specified player
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="player"></param>
        /// <returns></returns>
        public T _<T>(IPlayer player = null) where T : class
        {
            return Locale<T>(player);
        }

        /// <summary>
        /// Get locale of generic type and/or for specified player
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="player"></param>
        /// <returns></returns>
        public T Locale<T>(IPlayer player) where T : class
        {
            return Locales.GetLocale<T>(player);
        }

        /// <summary>
        /// Get locale of generic type for the specified language
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lang"></param>
        /// <returns></returns>
        public T Locale<T>(string lang) where T : class
        {
            return Locales.GetLocale<T>(lang);
        }

        /// <summary>
        /// Get locale of the specified type for the specified player
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public object Locale(Type localeType, IPlayer player)
        {
            return Locales.GetLocale(localeType, player);
        }

        /// <summary>
        /// Get locale of the specified type for the specified language
        /// </summary>
        /// <param name="localeType"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public object Locale(Type localeType, string lang)
        {
            return Locales.GetLocale(localeType, lang);
        }

        #endregion Localization

        #region Logging

        /// <summary>
        /// Print an info message using the uMod root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected void Puts(string format, params object[] args)
        {
            Logger?.Info(string.Format("[{0}] {1}", Title, args.Length > 0 ? string.Format(format, args) : format));
        }

        /// <summary>
        /// Print a warning message using the uMod root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected void PrintWarning(string format, params object[] args)
        {
            Logger?.Warning(string.Format("[{0}] {1}", Title, args.Length > 0 ? string.Format(format, args) : format));
        }

        /// <summary>
        /// Print an error message using the uMod root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected void PrintError(string format, params object[] args)
        {
            Logger?.Error(string.Format("[{0}] {1}", Title, args.Length > 0 ? string.Format(format, args) : format));
        }

        /// <summary>
        /// Logs a string of text to a named file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="text"></param>
        /// <param name="plugin"></param>
        /// <param name="timeStamp"></param>
        [Obsolete("Use Log or LogDaily attribute instead")]
        protected void LogToFile(string filename, string text, Plugin plugin, bool timeStamp = true)
        {
            Logger?.Debug(text);
        }

        /// <summary>
        /// Print an info message using the uMod root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected void Log(string format, params object[] args)
        {
            Logger?.Info(string.Format("[{0}] {1}", Title, args.Length > 0 ? string.Format(format, args) : format));
        }

        /// <summary>
        /// Print an debug message using the uMod root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected void LogDebug(string format, params object[] args)
        {
            Logger?.Debug(string.Format("[{0}] {1}", Title, args.Length > 0 ? string.Format(format, args) : format));
        }

        /// <summary>
        /// Print a warning message using the uMod root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected void LogWarning(string format, params object[] args)
        {
            Logger?.Warning(string.Format("[{0}] {1}", Title, args.Length > 0 ? string.Format(format, args) : format));
        }

        /// <summary>
        /// Print an error message using the uMod root logger
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected void LogError(string format, params object[] args)
        {
            Logger?.Error(string.Format("[{0}] {1}", Title, args.Length > 0 ? string.Format(format, args) : format));
        }

        #endregion Logging

        #region Helper Methods

        /// <summary>
        /// Queue a callback to be called in the next server frame
        /// </summary>
        /// <param name="callback"></param>
        protected void NextFrame(Action callback) => Interface.uMod.NextTick(callback);

        /// <summary>
        /// Queue a callback to be called in the next server frame
        /// </summary>
        /// <param name="callback"></param>
        protected void NextTick(Action callback) => Interface.uMod.NextTick(callback);

        /// <summary>
        /// Queues a callback to be called from a thread pool worker thread
        /// </summary>
        /// <param name="callback"></param>
        [Obsolete]
        protected void QueueWorkerThread(Action<object> callback)
        {
            ThreadPool.QueueUserWorkItem(context =>
            {
                try
                {
                    callback(context);
                }
                catch (Exception ex)
                {
                    OnException.Invoke(this, "Exception in worker thread", ex);
                }
            });
        }

        #endregion Helper Methods
    }
}
