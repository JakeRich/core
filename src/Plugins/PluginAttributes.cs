﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using uMod.Common;
using uMod.Libraries;
using uMod.Logging;

#pragma warning disable 618

namespace uMod.Plugins
{
    /// <summary>
    /// Allows configuration of plugin info using an attribute above the plugin class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class InfoAttribute : Attribute
    {
        /// <summary>
        /// Gets the plugin title
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Gets the plugin author
        /// </summary>
        public string Author { get; }

        /// <summary>
        /// Gets the plugin version
        /// </summary>
        public VersionNumber Version { get; private set; }

        internal string InvalidVersion = string.Empty;

        /// <summary>
        /// Gets the plugin resource identifier
        /// </summary>
        [Obsolete]
        public int ResourceId { get; set; }

        /// <summary>
        /// Create a new instance of the InfoAttribute class
        /// </summary>
        /// <param name="title"></param>
        /// <param name="author"></param>
        /// <param name="version"></param>
        public InfoAttribute(string title, string author, string version)
        {
            Title = title;
            Author = author;
            SetVersion(version);
        }

        /// <summary>
        /// Create a new instance of the InfoAttribute class
        /// </summary>
        /// <param name="title"></param>
        /// <param name="author"></param>
        /// <param name="version"></param>
        public InfoAttribute(string title, string author, double version)
        {
            Title = title;
            Author = author;
            SetVersion(version.ToString(CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Sets the plugin version
        /// </summary>
        /// <param name="version"></param>
        private void SetVersion(string version)
        {
            List<ushort> versionParts = version.Split('.').Select(part =>
            {
                if (!ushort.TryParse(part, out ushort number))
                {
                    number = 0;
                }

                return number;
            }).ToList();

            while (versionParts.Count < 3)
            {
                versionParts.Add(0);
            }

            if (versionParts.Count > 3)
            {
                InvalidVersion = version;
            }

            Version = new VersionNumber(versionParts[0], versionParts[1], versionParts[2]);
        }
    }

    /// <summary>
    /// Specify plugin or command description
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DescriptionAttribute : Attribute
    {
        /// <summary>
        /// Gets the resource description
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Create a new instance of the DescriptionAttribute class
        /// </summary>
        /// <param name="description"></param>
        public DescriptionAttribute(string description)
        {
            Description = description;
        }
    }

    /// <summary>
    /// Specify command syntax help
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class HelpAttribute : Attribute
    {
        /// <summary>
        /// Gets the command syntax
        /// </summary>
        public string Syntax { get; }

        /// <summary>
        /// Create a new instance of the HelpAttribute class
        /// </summary>
        /// <param name="syntax"></param>
        public HelpAttribute(string syntax)
        {
            Syntax = syntax;
        }
    }

    /// <summary>
    /// Specify a method as a converter
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ConverterAttribute : Attribute
    {
    }

    /// <summary>
    /// Allows plugins to specify a logger
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class LogAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the logger type
        /// </summary>
        internal Type LoggerType { get; set; }

        /// <summary>
        /// Gets the logger filename
        /// </summary>
        public string Filename { get; protected set; }

        /// <summary>
        /// Gets the logger name
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Create a new instance of the LogAttribute class
        /// </summary>
        public LogAttribute()
        {
            Name = null;
        }

        /// <summary>
        /// Create a new instance of the LogAttribute class with the specified name
        /// </summary>
        /// <param name="name"></param>
        public LogAttribute(string name)
        {
            Name = name;
            LoggerType = typeof(SinglePluginLogger);
        }

        /// <summary>
        /// Create a new instance of the LogAttribute class with the specified name and filename
        /// </summary>
        /// <param name="name"></param>
        /// <param name="filename"></param>
        public LogAttribute(string name, string filename) : this(name)
        {
            Filename = filename;
        }
    }

    /// <summary>
    /// Allows plugins to specify a logger
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class LogDailyAttribute : LogAttribute
    {
        /// <summary>
        /// Create a new instance of the LogDailyAttribute class with the specified name
        /// </summary>
        /// <param name="name"></param>
        public LogDailyAttribute(string name)
        {
            Name = name;
            LoggerType = typeof(DailyPluginLogger);
        }

        /// <summary>
        /// Create a new instance of the LogDailyAttribute class with the specified name and filename
        /// </summary>
        /// <param name="name"></param>
        /// <param name="filename"></param>
        public LogDailyAttribute(string name, string filename) : this(name)
        {
            Filename = filename;
        }
    }

    /// <summary>
    /// Indicates that the specified method should be a handler for a universal command
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class CommandAttribute : Attribute
    {
        /// <summary>
        /// Gets the command definition aliases
        /// </summary>
        public string[] Commands { get; }

        /// <summary>
        /// Create a new instance of the CommandAttribute class
        /// </summary>
        /// <param name="commands"></param>
        public CommandAttribute(params string[] commands)
        {
            Commands = commands;
        }
    }

    /// <summary>
    /// Indicates that the specified method requires a specific permission
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PermissionAttribute : Attribute
    {
        /// <summary>
        /// Gets permissions
        /// </summary>
        public string[] Permission { get; }

        /// <summary>
        /// Create a new instance of the PermissionAttribute class
        /// </summary>
        /// <param name="permission"></param>
        public PermissionAttribute(string permission)
        {
            Permission = new[] { permission };
        }
    }

    /// <summary>
    /// TODO: MAKE INTERNAL
    /// Indicates that the specified field should be a reference to another plugin when it is loaded
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field, AllowMultiple = true)]
    [Obsolete("Please use Requires or Optional attribute", false)]
    public class PluginReferenceAttribute : Attribute
    {
        public string[] Names { get; protected set; }

        /// <summary>
        /// Create a new instance of the PluginReferenceAttribute class
        /// </summary>
        public PluginReferenceAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the PluginReferenceAttribute class for the specified names
        /// </summary>
        /// <param name="names"></param>
        public PluginReferenceAttribute(params string[] names)
        {
            Names = names;
        }
    }

    /// <summary>
    /// Specifies class or property as a required dependency
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field, AllowMultiple = true)]
    public class RequiresAttribute : PluginReferenceAttribute
    {
        /// <summary>
        /// Create a new instance of the RequiresAttribute class
        /// </summary>
        public RequiresAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the RequiresAttribute class for the specified names
        /// </summary>
        /// <param name="names"></param>
        public RequiresAttribute(params string[] names) : base(names)
        {
        }
    }

    /// <summary>
    /// Specifies class or field as an optional dependency
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field, AllowMultiple = true)]
    public class OptionalAttribute : PluginReferenceAttribute
    {
        /// <summary>
        /// Create a new instance of the OptionalAttribute class
        /// </summary>
        public OptionalAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the RequiresAttribute class for the specified names
        /// </summary>
        public OptionalAttribute(params string[] names) : base(names)
        {
        }
    }

    /// <summary>
    /// Specieis class or field as a debug dependency
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field, AllowMultiple = true)]
    public class DebugAttribute : PluginReferenceAttribute
    {
        /// <summary>
        /// Create a new instance of the DebugAttribute class
        /// </summary>
        public DebugAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the DebugAttribute class for the specified names
        /// </summary>
        public DebugAttribute(params string[] names) : base(names)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified method should be a handler for a console command
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    [Obsolete("Use Command instead")]
    public class ConsoleCommandAttribute : Attribute
    {
        /// <summary>
        /// Gets the command definition
        /// </summary>
        public string Command { get; }

        /// <summary>
        /// Create a new instance of the ConsoleCommandAttribute class
        /// </summary>
        /// <param name="command"></param>
        public ConsoleCommandAttribute(string command)
        {
            Command = command.Contains('.') ? command : "global." + command;
        }
    }

    /// <summary>
    /// Indicates that the specified method should be a handler for a chat command
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    [Obsolete("Use Command instead")]
    public class ChatCommandAttribute : Attribute
    {
        /// <summary>
        /// Gets the command definition
        /// </summary>
        public string Command { get; }

        /// <summary>
        /// Create a new instance of the ChatCommandAttribute class
        /// </summary>
        /// <param name="command"></param>
        public ChatCommandAttribute(string command)
        {
            Command = command;
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a handler for a hook decorator
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class HookDecoratorAttribute : Attribute
    {
        /// <summary>
        /// Gets decorator types
        /// </summary>
        public Type[] DecoratorTypes { get; }

        /// <summary>
        /// Gets or sets whether or not decorator is automatically registered
        /// </summary>
        public bool AutoRegister { get; set; } = true;

        /// <summary>
        /// Create a new instance of the HookDecoratorAttribute class
        /// </summary>
        public HookDecoratorAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the HookDecoratorAttribute class with the specified decorator types
        /// </summary>
        /// <param name="decoratorTypes"></param>
        public HookDecoratorAttribute(params Type[] decoratorTypes)
        {
            DecoratorTypes = decoratorTypes;
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a configuration type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public abstract class DataAttribute : Attribute
    {
        /// <summary>
        /// Gets the data name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Create a new instance of the DataAttribute class
        /// </summary>
        protected DataAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the DataAttribute class with the specified name
        /// </summary>
        /// <param name="name"></param>
        protected DataAttribute(string name)
        {
            Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public abstract class DataFileAttribute : DataAttribute
    {
        /// <summary>
        /// Gets or sets whether or not file class is automatically loaded
        /// </summary>
        public bool AutoLoad { get; set; } = true;

        /// <summary>
        /// Gets or sets the file version
        /// </summary>
        public string Version { get; set; } = string.Empty;

        /// <summary>
        /// Create a new instance of the DataFileAttribute class
        /// </summary>
        public DataFileAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the DataFileAttribute class with the specified name
        /// </summary>
        /// <param name="configName"></param>
        public DataFileAttribute(string fileName) : base(fileName)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a configuration type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ConfigAttribute : DataFileAttribute
    {
        /// <summary>
        /// Create a new instance of the ConfigAttribute class
        /// </summary>
        public ConfigAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the ConfigAttribute class with the specified name
        /// </summary>
        /// <param name="configName"></param>
        public ConfigAttribute(string configName) : base(configName)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a table type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : DataAttribute
    {
        /// <summary>
        /// Create a new instance of the TableAttribute class
        /// </summary>
        public TableAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the TableAttribute class
        /// </summary>
        /// <param name="tableName"></param>
        public TableAttribute(string tableName) : base(tableName)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a json data type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class JsonAttribute : DataAttribute
    {
        /// <summary>
        /// Create a new instance of the JsonAttribute class
        /// </summary>
        public JsonAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the JsonAttribute class with the specified data name
        /// </summary>
        /// <param name="dataName"></param>
        public JsonAttribute(string dataName) : base(dataName)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a json data type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class TomlAttribute : DataAttribute
    {
        /// <summary>
        /// Create a new instance of the TomlAttribute class
        /// </summary>
        public TomlAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the TomlAttribute class with the specified data name
        /// </summary>
        /// <param name="dataName"></param>
        public TomlAttribute(string dataName) : base(dataName)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a protobuf data type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class ProtoAttribute : DataAttribute
    {
        /// <summary>
        /// Create a new instance of the ProtoAttribute class
        /// </summary>
        public ProtoAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the ProtoAttribute class with the specified data name
        /// </summary>
        /// <param name="dataName"></param>
        public ProtoAttribute(string dataName) : base(dataName)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a json data type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class BinaryAttribute : DataAttribute
    {
        /// <summary>
        /// Create a new instance of the BinaryAttribute class
        /// </summary>
        public BinaryAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the BinaryAttribute class with the specified data name
        /// </summary>
        /// <param name="dataName"></param>
        public BinaryAttribute(string dataName) : base(dataName)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be the default messages
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class MessagesAttribute : Attribute
    {
        /// <summary>
        /// Create a new instance of the MessagesAttribute class
        /// </summary>
        public MessagesAttribute()
        {
        }
    }

    /// <summary>
    /// Indicates that the specified class should be a handler for a generic unity-like behavior hook decorator
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class BehaviorAttribute : HookDecoratorAttribute
    {
        /// <summary>
        /// Create a new instance of the BehaviorAttribute class
        /// </summary>
        public BehaviorAttribute()
        {
        }
    }

    /// <summary>
    /// Indicates that the specified Hash field should be used to automatically track online players
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class OnlinePlayersAttribute : Attribute
    {
    }

    /// <summary>
    /// Indiciates that the specified interface should be used to implement Locales
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface)]
    public class LocalizationAttribute : DataFileAttribute
    {
        /// <summary>
        /// Create a new instance of the LocalizationAttribute class
        /// </summary>
        public LocalizationAttribute()
        {
        }

        /// <summary>
        /// Create a new instance of the LocalizationAttribute class with the specified localization name
        /// </summary>
        /// <param name="localizationName"></param>
        public LocalizationAttribute(string localizationName) : base(localizationName)
        {
        }
    }

    /// <summary>
    /// Specify class as locale or command method locale
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class LocaleAttribute : Attribute
    {
        /// <summary>
        /// Gets the symbol of the locale
        /// </summary>
        public string Locale { get; internal set; }

        /// <summary>
        /// Gets the locale type
        /// </summary>
        public Type LocaleType { get; internal set; }

        /// <summary>
        /// Initializes a new instance of the LocaleAttribute class
        /// </summary>
        /// <param name="locale"></param>
        public LocaleAttribute(string locale = Lang.DefaultLang)
        {
            Locale = locale;
        }

        /// <summary>
        /// Initializes a new instance of the LocaleAttribute class
        /// </summary>
        /// <param name="type"></param>
        public LocaleAttribute(Type type)
        {
            LocaleType = type;
        }
    }

    /// <summary>
    /// Specify command method as localized with Lang provider
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class LangAttribute : Attribute
    {
        /// <summary>
        /// Create a new instance of the LangAttribute class
        /// </summary>
        public LangAttribute()
        {
        }
    }

    /// <summary>
    /// Indicates that the specified method should be a handler for a hook
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class HookMethodAttribute : HookAttribute
    {
        /// <summary>
        /// Create a new instance of the HookMethodAttribute class with the specified name
        /// </summary>
        /// <param name="name"></param>
        public HookMethodAttribute(string name) : base(name)
        {
        }

        /// <summary>
        /// Create a new instance of the HookMethodAttribute class with the specified name and event state
        /// </summary>
        /// <param name="name"></param>
        /// <param name="state"></param>
        public HookMethodAttribute(string name, EventState state) : base(name, state)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified method should be a handler for a hook
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class HookAttribute : Attribute
    {
        /// <summary>
        /// Gets the name of the hook
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Gets the full name of the hook
        /// </summary>
        public string FullName
        {
            get
            {
                if (string.IsNullOrEmpty(State) || State.Equals("None"))
                {
                    return Name;
                }

                return $"{Name}.{State}";
            }
        }

        /// <summary>
        /// Gets the event state filter of the hook
        /// </summary>
        public string State { get; }

        /// <summary>
        /// Initializes a new instance of the HookMethod class
        /// </summary>
        internal HookAttribute() : this("", EventState.None) { }

        /// <summary>
        /// Initializes a new instance of the HookMethod class
        /// </summary>
        /// <param name="name"></param>
        public HookAttribute(string name) : this(name, EventState.None) { }

        /// <summary>
        /// Initializes a new instance of the HookMethod class
        /// </summary>
        /// <param name="name"></param>
        /// <param name="state"></param>
        public HookAttribute(string name, EventState state)
        {
            Name = name;
            State = Enum.GetName(typeof(EventState), state);
        }

        public HookAttribute(string name, string eventStateName)
        {
            Name = name;
            State = eventStateName;
        }
    }

    /// <summary>
    /// Indicates that the specified method should be gated
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public abstract class GateAttribute : Attribute
    {
        internal Type gateType;
        internal IEnumerable<string> permissions;
        internal string permission;

        /// <summary>
        /// Permission associated with gate
        /// </summary>
        public string Permission { get; private set; }

        /// <summary>
        /// Type associated with gate
        /// </summary>
        public Type GateType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the Gate class
        /// </summary>
        /// <param name="gateType"></param>
        protected GateAttribute(Type gateType = null)
        {
            this.gateType = GateType = gateType;
        }

        /// <summary>
        /// Initializes a new instance of the Gate class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permission"></param>
        protected GateAttribute(Type gateType, string permission)
        {
            this.permission = Permission = permission;
            this.gateType = GateType = gateType;
        }

        /// <summary>
        /// Initializes a new instance of the Gate class
        /// </summary>
        /// <param name="permission"></param>
        protected GateAttribute(string permission)
        {
            this.permission = Permission = permission;
        }
    }

    /// <summary>
    ///  Indicates that the specified method should be gated by multiple gates
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class AggregateGateAttribute : GateAttribute
    {
        /// <summary>
        /// Permission associated with gate
        /// </summary>
        public IEnumerable<string> Permissions { get; private set; }

        /// <summary>
        /// Create a new instance of the AggregateGate class
        /// </summary>
        /// <param name="permissions"></param>
        /// <param name="gateType"></param>
        public AggregateGateAttribute(IEnumerable<string> permissions)
        {
            this.permissions = Permissions = permissions;
        }

        /// <summary>
        /// Create a new instance of the AggregateGate class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public AggregateGateAttribute(Type gateType, IEnumerable<string> permissions) : base(gateType)
        {
            this.permissions = Permissions = permissions;
        }

        /// <summary>
        /// Create a new instance of the AggregateGate class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public AggregateGateAttribute(Type gateType, params string[] permissions) : base(gateType)
        {
            this.permissions = Permissions = permissions;
        }
    }

    /// <summary>
    /// Determine if method is allowed if any gates allow it
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class AnyAttribute : AggregateGateAttribute
    {
        /// <summary>
        /// Create a new instance of the AnyAttribute class
        /// </summary>
        /// <param name="permissions"></param>
        public AnyAttribute(IEnumerable<string> permissions) : base(permissions)
        {
        }

        /// <summary>
        /// Create a new instance of the AnyAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public AnyAttribute(Type gateType, IEnumerable<string> permissions) : base(gateType, permissions)
        {
        }

        /// <summary>
        /// Create a new instance of the AnyAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public AnyAttribute(Type gateType, params string[] permissions) : base(gateType, permissions)
        {
        }
    }

    /// <summary>
    /// Determine if method is allowed if any gates allow it
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class AllAttribute : AggregateGateAttribute
    {
        /// <summary>
        /// Create a new instance of the AllAttribute class
        /// </summary>
        /// <param name="permissions"></param>
        public AllAttribute(IEnumerable<string> permissions) : base(permissions)
        {
        }

        /// <summary>
        /// Create a new instance of the AllAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public AllAttribute(Type gateType, IEnumerable<string> permissions) : base(gateType, permissions)
        {
        }

        /// <summary>
        /// Create a new instance of the AllAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public AllAttribute(Type gateType, params string[] permissions) : base(gateType, permissions)
        {
        }
    }

    /// <summary>
    /// Determine if method is allowed if any gates allow it
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class NoneAttribute : AggregateGateAttribute
    {
        /// <summary>
        /// Create a new instance of the NoneAttribute class
        /// </summary>
        /// <param name="permissions"></param>
        public NoneAttribute(IEnumerable<string> permissions) : base(permissions)
        {
        }

        /// <summary>
        /// Create a new instance of the NoneAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public NoneAttribute(Type gateType, IEnumerable<string> permissions) : base(gateType, permissions)
        {
        }

        /// <summary>
        /// Create a new instance of the NoneAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permissions"></param>
        public NoneAttribute(Type gateType, params string[] permissions) : base(gateType, permissions)
        {
        }
    }

    /// <summary>
    /// Determine if method is allowed
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class AllowsAttribute : GateAttribute
    {
        /// <summary>
        /// Create a new instance of the AllowsAttribute class
        /// </summary>
        /// <param name="permission"></param>
        public AllowsAttribute(string permission) : base(permission)
        {
        }

        /// <summary>
        /// Create a new instance of the AllowsAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permission"></param>
        public AllowsAttribute(Type gateType, string permission) : base(gateType, permission)
        {
        }
    }

    /// <summary>
    /// Determine if method is denied
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class DeniesAttribute : GateAttribute
    {
        /// <summary>
        /// Create a new instance of the DeniesAttribute class
        /// </summary>
        /// <param name="permission"></param>
        public DeniesAttribute(string permission) : base(permission)
        {
        }

        /// <summary>
        /// Create a new instance of the DeniesAttribute class
        /// </summary>
        /// <param name="gateType"></param>
        /// <param name="permission"></param>
        public DeniesAttribute(Type gateType, string permission) : base(gateType, permission)
        {
        }
    }

    /// <summary>
    /// Indicates that the specified method should be a started event handler for a hook
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class BeforeAttribute : HookAttribute
    {
        /// <summary>
        /// Initializes a new instance of the Before class
        /// </summary>
        /// <param name="name"></param>
        public BeforeAttribute(string name = "") : base(name, EventState.Started) { }
    }

    /// <summary>
    /// Indicates that the specified method should be an after event handler for a hook
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class CompletedAttribute : HookAttribute
    {
        /// <summary>
        /// Initializes a new instance of the After class
        /// </summary>
        /// <param name="name"></param>
        public CompletedAttribute(string name = "") : base(name, EventState.Completed) { }
    }

    /// <summary>
    /// Indicates that the specified method should be an after event handler for a hook
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class AfterAttribute : HookAttribute
    {
        /// <summary>
        /// Initializes a new instance of the After class
        /// </summary>
        /// <param name="name"></param>
        public AfterAttribute(string name = "") : base(name, "After") { }
    }

    /// <summary>
    /// Indicates that the specified method should be a cancel event handler for a hook
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class CanceledAttribute : HookAttribute
    {
        /// <summary>
        /// Initializes a new instance of the Canceled class
        /// </summary>
        /// <param name="name"></param>
        public CanceledAttribute(string name = "") : base(name, EventState.Canceled) { }
    }

    /// <summary>
    /// Indicates that the specified method should be a fail event handler for a hook
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class FailedAttribute : HookAttribute
    {
        /// <summary>
        /// Initializes a new instance of the Failed class
        /// </summary>
        /// <param name="name"></param>
        public FailedAttribute(string name = "") : base(name, EventState.Failed) { }
    }

    /// <summary>
    /// Indicates that the specified method should be executed asynchronously if possible
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class AsyncAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the Async class
        /// </summary>
        public AsyncAttribute() { }
    }

    /// <summary>
    /// Indicates that the specified class should be a mediated type
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ReferenceAttribute : Attribute
    {
        /// <summary>
        /// Gets the name of the reference
        /// </summary>
        public string Name { get; internal set; }

        public Type Interface { get; }

        /// <summary>
        /// Initializes a new instance of the Reference class
        /// </summary>
        /// <param name="name"></param>
        /// <param name="interface"></param>
        public ReferenceAttribute(string name = "", Type @interface = null)
        {
            Name = name;
            Interface = @interface;
        }
    }
}
