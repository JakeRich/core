﻿using System;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents plugin diagnostics
    /// </summary>
    public class PluginDiagnostics : IPluginDiagnostics
    {
        /// <summary>
        /// Gets a plugin object
        /// </summary>
        public readonly IPlugin Plugin;

        /// <summary>
        /// The current hook depth
        /// </summary>
        internal int HookDepth;

        /// <summary>
        /// Gets the total hook time
        /// </summary>
        public double TotalHookTime { get; internal set; }

        internal float AverageHookTime;
        internal double HookTimeSum;
        internal int PreHookGCCount;

        /// <summary>
        /// Create a new plugin diagnostic object
        /// </summary>
        /// <param name="plugin"></param>
        internal PluginDiagnostics(IPlugin plugin)
        {
            Plugin = plugin;
        }

        /// <summary>
        /// Track the start of an important method
        /// </summary>
        /// <returns></returns>
        public int TrackStart()
        {
            if (!Plugin.IsCorePlugin && HookDepth <= 0)
            {
                return Environment.TickCount;
            }

            return 0;
        }

        /// <summary>
        /// Track the end of an important method
        /// </summary>
        /// <param name="startedAt"></param>
        public void TrackEnd(int startedAt)
        {
            if (startedAt > 0 && !Plugin.IsCorePlugin && HookDepth <= 0)
            {
                TotalHookTime += (Environment.TickCount - startedAt) / 1000;
            }
        }
    }
}
