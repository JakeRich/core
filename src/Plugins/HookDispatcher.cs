using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Command;
using uMod.Common;
using uMod.Exceptions;
using uMod.Pooling;

namespace uMod.Plugins
{
    internal class HookDispatcher
    {
        internal struct HookDecorator
        {
            public IHookDecorator Decorator;
            public Type Type;
            public bool AutoRegister;

            public HookDecorator(IHookDecorator decorator, bool autoRegister = true)
            {
                Decorator = decorator;
                Type = decorator.GetType();
                AutoRegister = autoRegister;
            }

            public HookDecorator(IHookDecorator decorator, Type type, bool autoRegister = true)
            {
                Decorator = decorator;
                Type = type;
                AutoRegister = autoRegister;
            }
        }

        private readonly Plugin _plugin;

        // All matched hooked methods
        protected HookCache HooksCache = new HookCache();

        // All hooked methods
        internal Dictionary<string, List<HookMethod>> Hooks = new Dictionary<string, List<HookMethod>>();

        private Dictionary<HookName, HookMethod> _internalHooks;

        // All hook parameter substitution types
        internal Dictionary<string, List<Type>> HookSubstitutions = new Dictionary<string, List<Type>>();

        // All hook decorators
        internal Dictionary<string, HookDecorator> HookDecorators = new Dictionary<string, HookDecorator>();

        private readonly ILogger _logger;

        /// <summary>
        /// Create a hook dispatcher object for the specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        public HookDispatcher(Plugin plugin, ILogger logger)
        {
            _logger = logger;
            _plugin = plugin;
            plugin.OnAddedToManager.Add(owner_OnAddedFromManager);
            plugin.OnRemovedFromManager.Add(owner_OnRemovedFromManager);
        }

        private void owner_OnAddedFromManager(IPlugin sender, IPluginManager manager)
        {
            if (_internalHooks == null)
            {
                return;
            }

            if (_internalHooks.Count == 0)
            {
                return;
            }

            foreach (KeyValuePair<HookName, HookMethod> kvp in _internalHooks)
            {
                if (Interface.uMod.BaseHookMethods.ContainsKey(kvp.Key.fullyQualifiedHookName))
                {
                    _logger.Warning($"Cannot register base hook {kvp.Key.fullyQualifiedHookName} because it is already registered.");
                }
                else
                {
                    Interface.uMod.BaseHookMethods.Add(kvp.Key.fullyQualifiedHookName, new KeyValuePair<IPlugin, HookMethod>(_plugin, kvp.Value));
                }
            }
        }

        private void owner_OnRemovedFromManager(IPlugin sender, IPluginManager manager)
        {
            if (_internalHooks == null)
            {
                return;
            }

            if (_internalHooks?.Count == 0)
            {
                return;
            }

            foreach (KeyValuePair<HookName, HookMethod> kvp in _internalHooks)
            {
                if (Interface.uMod.BaseHookMethods.ContainsKey(kvp.Key.fullyQualifiedHookName))
                {
                    Interface.uMod.BaseHookMethods.Remove(kvp.Key.fullyQualifiedHookName);
                }
            }
        }

        /// <summary>
        /// Dispatch the specified hook using the specified arguments and context
        /// </summary>
        /// <param name="name"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public object Dispatch(string name, object[] args = null, HookEvent e = null)
        {
            HookName hookName = HookName.Cache.Get(name);
            return Dispatch(hookName, args, e);
        }

        /// <summary>
        /// Dispatch the specified hook using the specified arguments and context
        /// </summary>
        /// <param name="hookName"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public object Dispatch(HookName hookName, object[] args = null, HookEvent e = null)
        {
            object returnValue = null;
            bool pooledEventArgs = false;
            Args commandArg = null;
            int received = args?.Length ?? 0;

            if (e != null)
            {
                if (e.state == EventState.None)
                {
                    e.Start();
                }
            }
            else
            {
                // Pool event args if none exist
                e = Pools.HookEvents.Get<HookEvent>();
                e.context.Reset(hookName.fullyQualifiedHookName);
                e.state = hookName.isEvent ? hookName.eventState : EventState.Started;
                pooledEventArgs = true;
            }

            // Find all hooks that match the signature
            HookMethod[] hooks = FindHooks(hookName.name, args, e);

            // Invoke all matching hooks
            foreach (HookMethod hookMethod in hooks)
            {
                bool pooledArray = false;
                if (hookName.isNamespaced && (!hookMethod.IsNamespaced || (hookMethod.IsNamespaced && hookMethod.Namespace != hookName.@namespace)))
                {
                    continue;
                }

                object[] hookArgs;
                if (received != hookMethod.parameters.Length)
                {
                    pooledArray = true;
                    // The call argument count is different to the declared callback methods argument count
                    hookArgs = ArrayPool.Get(hookMethod.parameters.Length);

                    if (received > 0 && hookArgs.Length > 0)
                    {
                        // Remove any additional arguments which the callback method does not declare
                        Array.Copy(args, hookArgs, Math.Min(received, hookArgs.Length));
                    }

                    if (!hookMethod.TryMapParameters(hookArgs, received, e))
                    {
                        continue;
                    }
                }
                else
                {
                    // In case parameter needs to use a contract implementation
                    hookArgs = args;
                    if (hookArgs != null)
                    {
                        if (!hookMethod.TryMapParameters(hookArgs, received, e))
                        {
                            continue;
                        }
                    }
                }

                // Authorize gates
                if (hookMethod.isGated)
                {
                    try
                    {
                        if (!hookMethod.Authorize(hookArgs))
                        {
                            continue;
                        }
                    }
                    catch (AuthorizationException)
                    {
                        continue;
                    }
                    catch (Exception ex)
                    {
                        Interface.uMod.LogException($"Unable to authorize hook: {_plugin.Name}.{hookName}", ex);
                        continue;
                    }
                }

                try
                {
                    returnValue = hookMethod.Invoke(_plugin, hookArgs);
                }
                catch (Exception ex)
                {
                    if (pooledArray)
                    {
                        ArrayPool.Free(hookArgs);
                    }

                    if (pooledEventArgs)
                    {
                        Pools.HookEvents.Free(e);
                    }

                    if (commandArg != null)
                    {
                        Pools.CommandArgs.Free(commandArg);
                    }

                    throw;
                }

                // Copy output values for out and by reference arguments back to the calling args
                if (hookMethod.outParameters?.Length > 0)
                {
                    foreach (int outPosition in hookMethod.outParameters)
                    {
                        if (args.Length >= outPosition && hookArgs.Length >= outPosition)
                        {
                            args[outPosition] = hookArgs[outPosition];
                        }
                    }
                }

                if (pooledArray)
                {
                    ArrayPool.Free(hookArgs);
                }
            }

            if (pooledEventArgs)
            {
                Pools.HookEvents.Free(e);
            }

            if (commandArg != null)
            {
                Pools.CommandArgs.Free(commandArg);
            }

            return returnValue;
        }

        protected HookMethod[] FindHooks(string name, object[] args, HookEvent e = null)
        {
            // Check the cache if we already found a match for this hook
            HookMethod[] methods = HooksCache.GetHookMethod(name, args, out HookCache cache);
            if (methods != null)
            {
                return methods;
            }

            // Get all hook methods that could match, return an empty list if none match
            if (Hooks.TryGetValue(name, out List<HookMethod> hookMethods))
            {
                methods = hookMethods.ToArray();
            }
            else
            {
                return HookMethod.Empty;
            }

            List<HookMethod> matches = Pools.HookMethods.Get<List<HookMethod>>();

            // Find matching hooks
            bool isExactMatch = false;
            int x;
            //HookMethod exactMatch = null;
            //HookMethod overloadedMatch = null;

            for (x = 0; x < methods.Length; x++)
            {
                // A base hook should always have a matching signature either directly or through inheritance
                // and should always be called as core functionality depends on it.
                if (methods[x].IsBaseHook)
                {
                    matches.Add(methods[x]);
                    continue;
                }

                // Check if this method matches the hook arguments passed if it is not a base hook
                //object[] hookArgs;
                //int received = args?.Length ?? 0;

                //bool pooledArray = false;

                /*
                if (received != hookMethod.Parameters.Length)
                {
                    // The call argument count is different to the declared callback methods argument count
                    hookArgs = ArrayPool.Get(hookMethod.Parameters.Length);
                    pooledArray = true;

                    if (received > 0 && hookArgs.Length > 0)
                    {
                        // Remove any additional arguments which the callback method does not declare
                        Array.Copy(args, hookArgs, Math.Min(received, hookArgs.Length));
                    }

                    MapParameters(hookMethod, hookArgs, received, e);
                }
                else
                {
                    //hookArgs = args;
                }
                */

                if (methods[x].HasMatchingSignature(args, out isExactMatch))
                {
                    if (isExactMatch)
                    {
                        matches.Add(methods[x]);
                        break;
                    }

                    // Should we determine the level and call the closest overloaded match? Performance impact?
                    matches.Add(methods[x]);
                }

                /*
                if (pooledArray)
                {
                    ArrayPool.Free(hookArgs);
                }
                */
            }

            if (isExactMatch && methods.Length > 1)
            {
                foreach (HookMethod hookMethod in methods)
                {
                    if (!string.IsNullOrEmpty(hookMethod.Namespace) && hookMethod.HasMatchingSignature(args, out isExactMatch))
                    {
                        matches.Add(hookMethod);
                    }
                }
            }

            try
            {
                return cache.SetupMethods(matches.ToArray());
            }
            finally
            {
                Pools.HookMethods.Free(matches);
            }
        }

        /*
        protected List<HookMethod> FindHooks(string name, object[] args, EventArgs e = null)
        {
            // Check the cache if we already found a match for this hook
            List<HookMethod> methods = HooksCache.GetHookMethod(name, args, out HookCache cache);
            if (methods != null)
            {
                return methods;
            }

            List<HookMethod> matches = new List<HookMethod>();
            // Get all hook methods that could match, return an empty list if none match
            if (!Hooks.TryGetValue(name, out methods))
            {
                return matches;
            }

            // Find matching hooks
            foreach (HookMethod hookMethod in methods)
            {
                // A base hook should always have a matching signature either directly or through inheritance
                // and should always be called as core functionality depends on it.
                if (hookMethod.IsBaseHook)
                {
                    matches.Add(hookMethod);
                    continue;
                }

                // Check if this method matches the hook arguments passed if it is not a base hook
                object[] hookArgs;
                int received = args?.Length ?? 0;

                bool pooledArray = false;

                if (permissive && received != hookMethod.Parameters.Length)
                {
                    // The call argument count is different to the declared callback methods argument count
                    hookArgs = ArrayPool.Get(hookMethod.Parameters.Length);
                    pooledArray = true;

                    if (received > 0 && hookArgs.Length > 0)
                    {
                        // Remove any additional arguments which the callback method does not declare
                        Array.Copy(args, hookArgs, Math.Min(received, hookArgs.Length));
                    }

                    MapParameters(hookMethod, hookArgs, received, e);
                }
                else
                {
                    hookArgs = args;
                }

                if (hookMethod.HasMatchingSignature(hookArgs, out bool isExactMatch))
                {
                    matches.Add(hookMethod);
                }

                if (pooledArray)
                {
                    ArrayPool.Free(hookArgs);
                }
            }

            cache.SetupMethods(matches);

            return matches;
        }
        */

        internal void AddHookDecorator(HookDecorator decorator)
        {
            if (!HookDecorators.TryGetValue(decorator.Decorator.Name, out _))
            {
                HookDecorators.Add(decorator.Decorator.Name, decorator);
            }
            else
            {
                HookDecorators[decorator.Decorator.Name] = decorator;
            }
        }

        internal void AddHookAsync(string name, MethodInfo method, IContext target = null, IEnumerable<GateAttribute> gates = null)
        {
            AddHookMethod(name, method, true, target, gates);
        }

        internal void AddHookMethod(string name, MethodInfo method, IContext target = null, IEnumerable<GateAttribute> gates = null)
        {
            AddHookMethod(name, method, false, target, gates);
        }

        internal void AddHookMethod(string name, MethodInfo method, bool async = false, IContext target = null, IEnumerable<GateAttribute> gates = null)
        {
            HookName hookName;
            lock (HookName.Cache.HookNameLock)
            {
                hookName = HookName.Cache.Get(name);
            }

            AddHookMethod(hookName, method, async, target, gates);
        }

        internal void AddHookMethod(HookName hookName, MethodInfo method, bool async = false, IContext target = null, IEnumerable<GateAttribute> gates = null)
        {
            if (!Hooks.TryGetValue(hookName.name, out List<HookMethod> hookMethods))
            {
                hookMethods = new List<HookMethod>();
                Hooks[hookName.name] = hookMethods;
            }

            HookMethod hookMethod;

            if (async)
            {
                hookMethods.Add(hookMethod = new AsyncHookMethod(Interface.uMod.Application, Interface.uMod.Universal.Provider, _plugin, method, hookName.name, gates, target, hookName.@namespace));
            }
            else
            {
                hookMethods.Add(hookMethod = new HookMethod(Interface.uMod.Application, Interface.uMod.Universal.Provider, _plugin, method, hookName.name, gates, target, hookName.@namespace));
            }

            if (hookName.IsInternal)
            {
                if (_internalHooks == null)
                {
                    _internalHooks = new Dictionary<HookName, HookMethod>();
                }

                if (!_internalHooks.ContainsKey(hookName))
                {
                    _internalHooks.Add(hookName, hookMethod);
                }

                return;
            }

            if (hookMethod.Signature != null && hookMethod.Signature.Length > 0)
            {
                if (!HookSubstitutions.TryGetValue(hookName.name, out List<Type> hookSubstitutions))
                {
                    HookSubstitutions.Add(hookName.name, hookSubstitutions = new List<Type>());
                }

                for (int x = 0; x < hookMethod.Signature.Length; x++)
                {
                    if (!hookSubstitutions.Contains(hookMethod.Signature[x]))
                    {
                        hookSubstitutions.Add(hookMethod.Signature[x]);
                    }
                }
            }
        }

        internal void AddExplicitHookByName(string name)
        {
            bool hooksChanged = false;
            string cachedHookName = string.Empty;
            foreach (MethodInfo method in _plugin.GetType().GetMethodsWithAttribute<HookAttribute>(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                if (method.GetCustomAttribute<HookAttribute>() is HookAttribute hookMethodAttribute && hookMethodAttribute.Name == name)
                {
                    HookName hookName;
                    lock (HookName.Cache.HookNameLock)
                    {
                        hookName = HookName.Cache.Get(name);
                    }

                    AsyncAttribute asyncAttribute = method.GetCustomAttribute<AsyncAttribute>();
                    if (string.IsNullOrEmpty(hookMethodAttribute.Name))
                    {
                        hookMethodAttribute.Name = Utility.ReflectedName(method.Name);
                    }
                    if (asyncAttribute != null)
                    {
                        AddHookAsync(hookMethodAttribute.FullName, method, null, method.GetCustomAttributes<GateAttribute>());
                    }
                    else
                    {
                        AddHookMethod(hookMethodAttribute.FullName, method, null, method.GetCustomAttributes<GateAttribute>());
                    }

                    cachedHookName = hookName.name;
                    hooksChanged = true;
                    break;
                }
            }

            if (hooksChanged)
            {
                HooksCache.Clear(cachedHookName);
            }
        }

        internal void RemoveHookMethod(string name, MethodInfo method = null)
        {
            HookName hookName;
            lock (HookName.Cache.HookNameLock)
            {
                hookName = HookName.Cache.Get(name);
            }

            RemoveHookMethod(hookName, method);
        }

        internal void RemoveHookMethod(HookName hookName, MethodInfo method = null)
        {
            // Unsubscribe and remove method
            if (method == null)
            {
                if (hookName.isNamespaced)
                {
                    if (Hooks.TryGetValue(hookName.name, out List<HookMethod> methods))
                    {
                        methods.RemoveAll(x => x.Name == hookName.name && x.Namespace == hookName.@namespace);
                        if (methods.Count == 0)
                        {
                            _plugin.Manager.UnsubscribeToHook(hookName.name, _plugin);
                            Hooks.Remove(hookName.name);
                        }

                        HooksCache.Clear(hookName.name);
                    }
                }
                else
                {
                    _plugin.Manager.UnsubscribeToHook(hookName.name, _plugin);
                    Hooks.Remove(hookName.name);
                }
            }
            else // Only remove method
            {
                if (Hooks.TryGetValue(hookName.name, out List<HookMethod> hookMethods))
                {
                    // Only one method, so unsubscribe and remove
                    if (hookMethods.Count == 1)
                    {
                        _plugin.Manager.UnsubscribeToHook(hookName.name, _plugin);
                        Hooks.Remove(hookName.name);
                    }
                    else
                    {
                        // Find matching method and remove it (but stay subscribed)
                        HookMethod matchingMethod = hookMethods.FirstOrDefault(x => x.Method == method);
                        if (matchingMethod != null)
                        {
                            Hooks.Remove(hookName.name);
                        }
                    }
                }
            }
        }

        internal bool SubstitutesAnyType(string name, Type[] types)
        {
            if (HookSubstitutions.TryGetValue(name, out List<Type> hookSubstitutions))
            {
                for (int x = 0; x < types.Length; x++)
                {
                    if (hookSubstitutions.Contains(types[x]))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
