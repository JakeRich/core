using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common;
using uMod.Plugins.Compiler;
using uMod.Pooling;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a plugin loader that handles live compilation
    /// </summary>
    public class CompilablePluginLoader : PluginLoader
    {
        /// <summary>
        /// Assemblies that always will be referenced when compiling
        /// </summary>
        public static string[] DefaultReferences = { "System", "System.Core", "System.Data", "uMod.Core", "uMod.Promise", "uMod.Tommy" };

        /// <summary>
        /// Default plugin references
        /// </summary>
        public static HashSet<string> PluginReferences = new HashSet<string>(DefaultReferences);

        /// <summary>
        /// Forbidden assemblies
        /// </summary>
        private static readonly string[] AssemblyBlacklist = { "Newtonsoft.Json", "protobuf-net" };

        /// <summary>
        /// Gets the plugin file extension
        /// </summary>
        public override string FileExtension => ".cs";

        /// <summary>
        /// The plugin provider
        /// </summary>
        private readonly PluginProvider _provider;

        /// <summary>
        /// Create a new instance of the CompilablePluginLoader class
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="provider"></param>
        public CompilablePluginLoader(ILogger logger, PluginProvider provider) : base(logger)
        {
            _provider = provider;
        }

        /// <summary>
        /// Include references to all loaded game extensions and any assemblies they reference
        /// </summary>
        public void AddReferences()
        {
            foreach (IExtension extension in Interface.uMod.Extensions.All)
            {
                if (extension != null && (extension.IsCoreExtension || extension.IsGameExtension))
                {
                    Assembly assembly = extension.GetType().Assembly;
                    string assemblyName = assembly.GetName().Name;
                    if (!AssemblyBlacklist.Contains(assemblyName)) // TODO: Utilize option for plugin sandbox
                    {
                        PluginReferences.Add(assemblyName);
                        foreach (AssemblyName reference in assembly.GetReferencedAssemblies())
                        {
                            PluginReferences.Add(reference.Name);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Enumerate over specified directory
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public override IEnumerable<FileInfo> ScanDirectory(string directory)
        {
            if (Apps.Compiler.BinaryPath == null)
            {
                yield break;
            }

            IEnumerable<FileInfo> enumerable = base.ScanDirectory(directory);
            foreach (FileInfo file in enumerable)
            {
                yield return file;
            }
        }

        /// <summary>
        /// Creates a FileInfo from a possible plugin candidate
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public FileInfo GetLoadableFile(string directory, string path)
        {
            DirectoryInfo d = new DirectoryInfo(directory);
            string[] config = Interface.uMod.Plugins.Configuration.Watchers.PluginDirectories;

            if (!IgnoredDirectories.Contains(d.Name.ToLower()) && config.Contains(d.Name.ToLower()) ||
                config.Contains(d.Parent?.Name.ToLower()))
            {
                if (!path.EndsWith(FileExtension))
                {
                    path += FileExtension;
                }

                return new FileInfo(Path.Combine(directory, path));
            }

            return null;
        }

        /// <summary>
        /// Attempt to asynchronously compile and load plugin
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public override IPlugin Load(string directory, string name)
        {
            _provider.Compiler.OnScriptAdded(directory, name);

            return null;
        }

        /// <summary>
        /// Attempt to asynchronously compile plugin and only reload if successful
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="name"></param>
        public override void Reload(string directory, string name)
        {
            _provider.Compiler.OnScriptModified(directory, name);
        }

        /// <summary>
        /// Called when the plugin manager is unloading a plugin that was loaded by this plugin loader
        /// </summary>
        /// <param name="pluginBase"></param>
        public override void Unloading(IPlugin pluginBase)
        {
            string pluginName = pluginBase?.Name;

            if (string.IsNullOrEmpty(pluginName))
            {
                return;
            }

            LoadedPlugins.Remove(pluginName);

            CompiledPlugin[] allPlugins = _provider.Compiler.GetAllCompiledPlugins();

            List<string> unloadPlugins = Pools.GetList<string>();
            List<string> reloadPlugins = Pools.GetList<string>();

            try
            {
                // Unload plugins which require this plugin
                foreach (CompiledPlugin pluginInfo in allPlugins)
                {
                    if (pluginInfo?.PluginReferences?.Any(x => x.PluginName == pluginName && x.Optional == false) == true)
                    {
                        unloadPlugins.Add(pluginInfo.PluginName);
                    }

                    if (pluginInfo?.PluginReferences?.Any(x =>
                        x.PluginName == pluginName && x.ExplicitOptional && LoadedPlugins.ContainsKey(pluginName)) == true)
                    {
                        reloadPlugins.Add(pluginInfo.PluginName);
                    }
                }

                if (unloadPlugins?.Count > 0)
                {
                    Interface.uMod.Plugins.Unload(unloadPlugins);
                }

                if (reloadPlugins?.Count > 0)
                {
                    Interface.uMod.Plugins.Reload(reloadPlugins);
                }
            }
            finally
            {
                Pools.FreeList(ref unloadPlugins);
                Pools.FreeList(ref reloadPlugins);
            }
        }
    }
}
