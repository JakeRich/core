using System;
using System.Collections.Generic;

namespace uMod.Plugins
{
    internal static class OxideDeprecation
    {
        /// <summary>
        /// List of replacements
        /// </summary>
        private static readonly Dictionary<string, string> ReplaceStrings = new Dictionary<string, string>()
        {
            ["Oxide.Core"] = "uMod",
            ["Oxide.Game"] = "uMod.Game",
            ["OxideMod"] = "uMod",
            ["Oxide"] = "uMod",
            ["Core.Interface."] = "Interface.",
            ["Core.Libraries.RequestMethod"] = "WebRequestMethod",
            ["Core.Random."] = "Utility.Random.",
            ["Interface.GetMod()"] = "Interface.uMod",
            ["Libraries.Covalence"] = "Libraries",
            ["Libraries.Universal"] = "Libraries",
            ["PluginManager.GetPlugin"] = "Plugins.Find",
            ["uMod.VersionNumber"] = "VersionNumber"
        };

        /// <summary>
        /// List of namespaces to be automatically inserted
        /// </summary>
        private static readonly string[] AutoNamespaces =
        {
            "uMod.Collections",
            "uMod.Common",
            "uMod.Common.Web",
            "uMod.Pooling",
            "uMod.Text"
        };

        /// <summary>
        /// Perform deprecation
        /// </summary>
        /// <param name="lines"></param>
        internal static void Deprecate(List<string> lines)
        {
            bool[] imported = new bool[AutoNamespaces.Length];

            int startLine = -1;
            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i]?.Length == 0) continue;
                // Set start line to insert auto namespaces
                if (startLine == -1 && lines[i].StartsWith("using "))
                {
                    startLine = i;
                }

                if (startLine == -1 && lines[i].StartsWith("namespace "))
                {
                    startLine = i;
                }

                // Check for auto namespace existence
                for (int x = 0; x < AutoNamespaces.Length; x++)
                {
                    if (lines[i].StartsWith($"using {AutoNamespaces[x]};"))
                    {
                        imported[x] = true;
                    }
                }

                // Replace strings
                foreach (KeyValuePair<string, string> kvp in ReplaceStrings)
                {
                    lines[i] = lines[i].Replace(kvp.Key, kvp.Value);
                }
            }

            // Insert non-existent auto namespaces
            for (int x = 0; x < AutoNamespaces.Length; x++)
            {
                if (!imported[x])
                {
                    lines.Insert(startLine, $"using {AutoNamespaces[x]};");
                }
            }
        }
    }
}
