﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents an asynchronous hook method
    /// </summary>
    internal class AsyncHookMethod : HookMethod
    {
        /// <summary>
        /// Create an asynchronous hook method object
        /// </summary>
        /// <param name="application"></param>
        /// <param name="provider"></param>
        /// <param name="plugin"></param>
        /// <param name="method"></param>
        /// <param name="name"></param>
        /// <param name="gates"></param>
        /// <param name="target"></param>
        /// <param name="namespace"></param>
        public AsyncHookMethod(IApplication application, IUniversalProvider provider, Plugin plugin, MethodInfo method, string name = "", IEnumerable<GateAttribute> gates = null, IContext target = null, string @namespace = "")
            : base(application, provider, plugin, method, name, gates, target, @namespace)
        {
            _isAsync = true;
            PluginLock = plugin.Lock;
        }
    }

    /// <summary>
    /// Represents a hook method
    /// </summary>
    internal class HookMethod
    {
        public static HookMethod[] Empty = new HookMethod[0];

        internal Plugin plugin;
        public Plugin Plugin => plugin;

        private readonly IContext _target;

        public IContext Target => _target;

        public Type TargetType { get; }

        public string Name { get; }

        public string Namespace { get; }

        public bool IsNamespaced { get; }

        private readonly MethodInfo _method;

        public MethodInfo Method => _method;

        internal ParameterInfo[] parameters;
        public ParameterInfo[] Parameters => parameters;

        internal int[] outParameters;
        public int[] OutParameters { get => outParameters; }

        public Type[] Signature { get; }

        public bool IsBaseHook { get; }

        protected bool _isAsync;

        public bool IsAsync { get => _isAsync; protected set => _isAsync = value; }

        protected object PluginLock;

        internal bool isGated = false;
        private IEnumerator<GateAttribute> gateEnumerator;
        public IEnumerable<GateAttribute> Gates { get; }

        private MethodInfo _genericMethod;

        private Func<object, object[], object> _funcDelegate;
        private Action<object, object[]> _actionDelegate;
        private readonly IApplication _application;
        private IUniversalProvider _provider;
        private readonly IGameTypes _gameTypes;

        /// <summary>
        /// Create a hook method object
        /// </summary>
        /// <param name="application"></param>
        /// <param name="provider"></param>
        /// <param name="plugin"></param>
        /// <param name="method"></param>
        /// <param name="name"></param>
        /// <param name="gates"></param>
        /// <param name="target"></param>
        /// <param name="namespace"></param>
        public HookMethod(IApplication application, IUniversalProvider provider, Plugin plugin, MethodInfo method, string name = "", IEnumerable<GateAttribute> gates = null, IContext target = null, string @namespace = "")
        {
            _application = application;
            _provider = provider;
            _gameTypes = provider.Types;
            this.plugin = plugin;
            _method = method;
            Name = string.IsNullOrEmpty(name) ? method.Name : name;
            Namespace = @namespace;
            IsNamespaced = !string.IsNullOrEmpty(@namespace);
            _target = target;
            TargetType = target?.GetType();
            if (gates != null && gates.Any())
            {
                Gates = gates;
                isGated = true;
                gateEnumerator = gates.GetEnumerator();
            }

            parameters = _method.GetParameters();

            if (parameters.Length > 0)
            {
                Signature = new Type[parameters.Length];

                List<int> outPositions = new List<int>();
                for (int n = 0; n < parameters.Length; n++)
                {
                    // Copy output values for out and by reference arguments back to the calling args
                    if (parameters[n].IsOut || parameters[n].ParameterType.IsByRef)
                    {
                        outPositions.Add(n);
                    }
                }

                if (outPositions.Count > 0)
                {
                    outParameters = outPositions.ToArray();
                }
            }

            for (int i = 0; i < parameters.Length; i++)
            {
                if (!parameters[i].IsOptional)
                {
                    Signature[i] = parameters[i].ParameterType;
                }
            }

            if (parameters.Length > 0)
            {
                Name += $"({string.Join(", ", parameters.Select(x => x.ParameterType.ToString()).ToArray())})";
            }

            IsBaseHook = Name.StartsWith("base_");

            _isAsync = false;
        }

        /// <summary>
        /// Invoke method with reflection
        /// </summary>
        /// <param name="target"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private object __Invoke(IContext target, object[] args)
        {
            if (_method != null && _method.IsGenericMethodDefinition)
            {
                if (_genericMethod == null)
                {
                    if (_method.ContainsGenericParameters)
                    {
                        _genericMethod = MakeGeneric(target, _method, args);
                    }
                    else
                    {
                        _genericMethod = _method.MakeGenericMethod();
                    }
                }

                return _genericMethod?.Invoke(target, args);
            }

            return _method?.Invoke(target, args);
        }

        /// <summary>
        /// Determine if hook is authorized depending on specified arguments
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        internal bool Authorize(object[] arguments)
        {
            if (!isGated)
            {
                return true;
            }

            gateEnumerator.Reset();
            while (gateEnumerator.MoveNext())
            {
                switch (gateEnumerator.Current)
                {
                    // Use permission gate
                    case AllowsAttribute _ when gateEnumerator.Current.gateType == null:
                        {
                            if (!plugin.gate.Allows(gateEnumerator.Current.permission, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case AllowsAttribute _:
                        {
                            if (Interface.uMod.Application.TryInject(gateEnumerator.Current.gateType, plugin.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.Allows(gateEnumerator.Current.permission, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (_application.TryGetSingleton(gateEnumerator.Current.gateType, out customGate))
                            {
                                if (!customGate.Allows(gateEnumerator.Current.permission, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case DeniesAttribute _ when gateEnumerator.Current.gateType == null:
                        {
                            if (!plugin.gate.Denies(gateEnumerator.Current.permission, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case DeniesAttribute _:
                        {
                            if (Interface.uMod.Application.TryInject(gateEnumerator.Current.gateType, plugin.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.Denies(gateEnumerator.Current.permission, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (_application.TryGetSingleton(gateEnumerator.Current.gateType, out customGate))
                            {
                                if (!customGate.Denies(gateEnumerator.Current.permission, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case AnyAttribute _ when gateEnumerator.Current.gateType == null:
                        {
                            if (!plugin.gate.Any(gateEnumerator.Current.permissions, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case AnyAttribute _:
                        {
                            if (Interface.uMod.Application.TryInject(gateEnumerator.Current.gateType, plugin.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.Any(gateEnumerator.Current.permissions, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (_application.TryGetSingleton(gateEnumerator.Current.gateType, out customGate))
                            {
                                if (!customGate.Any(gateEnumerator.Current.permissions, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case AllAttribute _ when gateEnumerator.Current.gateType == null:
                        {
                            if (!plugin.gate.All(gateEnumerator.Current.permissions, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case AllAttribute _:
                        {
                            if (Interface.uMod.Application.TryInject(gateEnumerator.Current.gateType, plugin.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.All(gateEnumerator.Current.permissions, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (_application.TryGetSingleton(gateEnumerator.Current.gateType, out customGate))
                            {
                                if (!customGate.All(gateEnumerator.Current.permissions, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                    // Use permission gate
                    case NoneAttribute _ when gateEnumerator.Current.gateType == null:
                        {
                            if (!plugin.gate.None(gateEnumerator.Current.permissions, arguments))
                            {
                                return false;
                            }

                            break;
                        }
                    // Use custom gate
                    case NoneAttribute _:
                        {
                            if (Interface.uMod.Application.TryInject(gateEnumerator.Current.gateType, plugin.GetType(), null, out object param) && param is IGate customGate)
                            {
                                if (!customGate.None(gateEnumerator.Current.permissions, arguments))
                                {
                                    return false;
                                }
                            }
                            else if (_application.TryGetSingleton(gateEnumerator.Current.gateType, out customGate))
                            {
                                if (!customGate.None(gateEnumerator.Current.permissions, arguments))
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                }
            }

            return true;
        }

        /// <summary>
        /// Invoke hook method in specified context with specified arguments
        /// </summary>
        /// <param name="target"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        internal object Invoke(IContext target, object[] arguments)
        {
            if (_method == null)
            {
                return null;
            }

            if (_actionDelegate != null)
            {
                if (_isAsync)
                {
                    Interface.uMod.Dispatcher.Dispatch(delegate
                    {
                        _actionDelegate(target, arguments);
                        return false;
                    }, ref PluginLock);
                    return null;
                }

                _actionDelegate(_target ?? target, arguments);
                return null;
            }

            if (_funcDelegate != null)
            {
                if (_isAsync)
                {
                    Interface.uMod.Dispatcher.Dispatch(delegate
                    {
                        _funcDelegate(target, arguments);
                        return false;
                    }, ref PluginLock);
                    return null;
                }

                return _funcDelegate(_target ?? target, arguments);
            }

            if (_method.IsStatic)
            {
                return _Invoke(null, arguments);
            }

            return _Invoke(_target ?? target, arguments);
        }

        /// <summary>
        /// Invoke hook within specified context using specified arguments
        /// </summary>
        /// <param name="target"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private object _Invoke(IContext target, object[] arguments)
        {
            if (OutParameters?.Length > 0)
            {
                return __Invoke(target, arguments);
            }

            /*
            if (!IsBaseHook)
            {
                if (args != null && args.Length > 0)
                {
                    ParameterInfo[] parameters = Parameters;
                    for (int i = 0; i < args.Length; i++)
                    {
                        object value = args[i];
                        if (value != null)
                        {
                            Type parameterType = parameters[i].ParameterType;
                            if (parameterType.IsValueType)
                            {
                                Type argumentType = value.GetType();
                                if (parameterType != typeof(object) && argumentType != parameterType)
                                {
                                    args[i] = Convert.ChangeType(value, parameterType);
                                }
                            }
                        }
                    }
                }
            }
            */

            if (TryGetDelegate(target, arguments, out Action<object, object[]> @delegate))
            {
                if (_isAsync)
                {
                    Interface.uMod.Dispatcher.Dispatch(delegate
                    {
                        @delegate(target, arguments);
                        return false;
                    }, ref PluginLock);
                    return null;
                }

                @delegate(target, arguments);
                return null;
            }

            if (TryGetDelegate(target, arguments, out Func<object, object[], object> delegate2))
            {
                if (_isAsync)
                {
                    Interface.uMod.Dispatcher.Dispatch(() =>
                    {
                        delegate2(target, arguments);
                        return false;
                    }, ref PluginLock);
                    return null;
                }

                return delegate2(target, arguments);
            }

            return null;
        }

        /// <summary>
        /// Try to get the late bound action or create one if a return value is not expected
        /// </summary>
        /// <param name="method"></param>
        /// <param name="delegate"></param>
        /// <returns></returns>
        private bool TryGetAction(MethodInfo method, out Action<object, object[]> @delegate)
        {
            if (_actionDelegate != null)
            {
                @delegate = _actionDelegate;
                return true;
            }

            @delegate = null;
            if (method.ReturnType == typeof(void))
            {
                ParameterExpression instanceParameter = Expression.Parameter(typeof(object), "target");

                ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

                MethodCallExpression call = Expression.Call(
                        Expression.Convert(instanceParameter, method.DeclaringType),
                        method,
                        CreateParameterExpressions(method, argumentsParameter));

                Expression<Action<object, object[]>> lambda = Expression.Lambda<Action<object, object[]>>(
                    call,
                    instanceParameter,
                    argumentsParameter);

                _actionDelegate = @delegate = lambda.Compile();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to get the late bound function or create one if a return value is expected
        /// </summary>
        /// <param name="method"></param>
        /// <param name="delegate"></param>
        /// <returns></returns>
        private bool TryGetFunc(MethodInfo method, out Func<object, object[], object> @delegate)
        {
            if (_funcDelegate != null)
            {
                @delegate = _funcDelegate;
                return true;
            }

            @delegate = null;
            if (method.ReturnType != typeof(void))
            {
                ParameterExpression instanceParameter = Expression.Parameter(typeof(object), "target");

                ParameterExpression argumentsParameter = Expression.Parameter(typeof(object[]), "arguments");

                MethodCallExpression call = Expression.Call(
                    Expression.Convert(instanceParameter, method.DeclaringType),
                    method,
                    CreateParameterExpressions(method, argumentsParameter));

                Expression<Func<object, object[], object>> lambda = Expression.Lambda<Func<object, object[], object>>(
                    Expression.Convert(call, typeof(object)),
                    instanceParameter,
                    argumentsParameter);

                _funcDelegate = @delegate = lambda.Compile();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to get a late bound function (with return value)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <param name="delegate"></param>
        /// <returns></returns>
        private bool TryGetDelegate(IContext context, object[] args, out Func<object, object[], object> @delegate)
        {
            if (_funcDelegate != null)
            {
                @delegate = _funcDelegate;
                return true;
            }

            if (_method.IsGenericMethodDefinition)
            {
                if (_genericMethod == null)
                {
                    if (_method.ContainsGenericParameters)
                    {
                        _genericMethod = MakeGeneric(context, _method, args);
                    }
                    else
                    {
                        _genericMethod = _method.MakeGenericMethod();
                    }
                }

                return TryGetFunc(_genericMethod, out @delegate);
            }

            return TryGetFunc(_method, out @delegate);
        }

        /// <summary>
        /// Try to get a late bound action (without return value)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="args"></param>
        /// <param name="delegate"></param>
        /// <returns></returns>
        private bool TryGetDelegate(IContext context, object[] args, out Action<object, object[]> @delegate)
        {
            if (_actionDelegate != null)
            {
                @delegate = _actionDelegate;
                return true;
            }

            if (_method.IsGenericMethodDefinition)
            {
                if (_genericMethod == null)
                {
                    if (_method.ContainsGenericParameters)
                    {
                        _genericMethod = MakeGeneric(context, _method, args);
                    }
                    else
                    {
                        _genericMethod = _method.MakeGenericMethod();
                    }
                }

                return TryGetAction(_genericMethod, out @delegate);
            }

            return TryGetAction(_method, out @delegate);
        }

        /// <summary>
        /// Get an array of expressions for parameter conversions
        /// </summary>
        /// <param name="method"></param>
        /// <param name="argumentsParameter"></param>
        /// <returns></returns>
        private Expression[] CreateParameterExpressions(MethodInfo method, Expression argumentsParameter)
        {
            return method.GetParameters().Select((parameter, index) =>
                Expression.Convert(Expression.ArrayIndex(argumentsParameter, Expression.Constant(index)),
                    parameter.ParameterType)).ToArray();
        }

        /// <summary>
        /// Create a generic method with support for late bound types and dependency injection
        /// </summary>
        /// <param name="context"></param>
        /// <param name="method"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private MethodInfo MakeGeneric(IContext context, MethodInfo method, object[] args)
        {
            if (method != null && method.IsGenericMethodDefinition)
            {
                if (method.ContainsGenericParameters)
                {
                    Type[] genericArgTypes = method.GetGenericMethodDefinition().GetGenericArguments();
                    Type[] splicedArgTypes = TypePool.Get(genericArgTypes.Length);

                    try
                    {
                        for (int i = 0; i < genericArgTypes.Length; i++)
                        {
                            if (genericArgTypes[i] == method.ReturnType)
                            {
                                if (_application.TryGetType($"{context.Name}.{method.ReturnType.BaseType?.Name}", out Type genericType))
                                {
                                    splicedArgTypes[i] = genericType;
                                }
                                else
                                {
                                    throw new Exception($"{method.ReturnType.Name} must be mediated with a Reference attribute to be used as generic hook result");
                                }
                            }
                            else
                            {
                                Type objType = typeof(object);
                                bool found = false;
                                for (int x = 0; x < args.Length; x++)
                                {
                                    if (genericArgTypes[i] == args[x].GetType())
                                    {
                                        splicedArgTypes[i] = args[x].GetType();
                                        found = true;
                                        break;
                                    }
                                    if (genericArgTypes[i].IsAssignableFrom(args[x].GetType()))
                                    {
                                        splicedArgTypes[i] = args[x].GetType();
                                        found = true;
                                        break;
                                    }
                                    if (genericArgTypes[i].BaseType != null && genericArgTypes[i].BaseType != objType && genericArgTypes[i].BaseType.IsAssignableFrom(args[x].GetType()))
                                    {
                                        splicedArgTypes[i] = args[x].GetType();
                                        found = true;
                                        break;
                                    }
                                    if (genericArgTypes[i].IsGenericParameter)
                                    {
                                        Type[] constraints = genericArgTypes[i].GetGenericParameterConstraints();
                                        foreach (Type constraint in constraints)
                                        {
                                            if (constraint.IsAssignableFrom(args[x].GetType()))
                                            {
                                                splicedArgTypes[i] = args[x].GetType();
                                                found = true;
                                                break;
                                            }
                                        }
                                    }

                                    if (found)
                                    {
                                        break;
                                    }
                                }

                                if (!found)
                                {
                                    splicedArgTypes[i] = objType;
                                }
                            }
                        }

                        method = method.MakeGenericMethod(splicedArgTypes);
                    }
                    finally
                    {
                        TypePool.Free(splicedArgTypes);
                    }
                }
                else
                {
                    method = method.MakeGenericMethod();
                }
            }

            return method;
        }

        private static readonly Type ObjectType = typeof(object);
        private static readonly Type StringType = typeof(string);
        private static readonly Type StringArrayType = typeof(string[]);
        private static readonly Type PlayerType = typeof(IPlayer);
        private static readonly Type CommandArgType = typeof(Command.Args);

        /// <summary>
        /// Determine if specified arguments match the hook signature
        /// </summary>
        /// <param name="args"></param>
        /// <param name="exact"></param>
        /// <returns></returns>
        public bool HasMatchingSignature(object[] args, out bool exact)
        {
            exact = true;

            if (parameters.Length == 0 && (args == null || args.Length == 0))
            {
                return true;
            }

            if (parameters.Length > 0 && args == null)
            {
                exact = false;
            }
            else if (parameters.Length != args.Length)
            {
                exact = false;
            }

            if (args == null)
            {
                return true;
            }

            for (int i = 0; i < args.Length; i++)
            {
                Type parameterType = null;
                if (i < parameters.Length)
                {
                    parameterType = parameters[i].ParameterType;
                }

                if (args[i] == null)
                {
                    if (parameterType != null)
                    {
                        return !CanAssignNull(parameterType) || (parameters[i].IsOut || parameterType.IsByRef);
                    }
                }

                Type argType = args[i]?.GetType();

                if (exact)
                {
                    // match IPlayer -> GamePlayer
                    if (argType != null && PlayerType.IsAssignableFrom(argType))
                    {
                        if (_gameTypes.Player.IsAssignableFrom(parameterType))
                        {
                            exact = true;
                        }
                        else
                        {
                            exact = _application.Converts(parameterType, PlayerType);
                        }

                        if (exact)
                        {
                            continue;
                        }
                    }

                    // match GamePlayer -> IPlayer
                    if (argType != null && _gameTypes.Player.IsAssignableFrom(argType))
                    {
                        if (PlayerType.IsAssignableFrom(parameterType))
                        {
                            exact = true;
                        }
                        else
                        {
                            exact = _application.Converts(parameterType, argType);
                        }

                        if (exact)
                        {
                            continue;
                        }
                    }

                    if (_method.IsGenericMethod) // matching generic interfaces
                    {
                        Type[] genericArguments = _method.GetGenericArguments();
                        if (i < genericArguments.Length)
                        {
                            Type[] typeDef = genericArguments[i].GetGenericParameterConstraints();
                            foreach (Type tD in typeDef)
                            {
                                if (!tD.IsAssignableFrom(argType))
                                {
                                    exact = false;
                                }
                            }
                        }
                    }
                    else if (parameterType == CommandArgType && (argType == StringType || argType == StringArrayType))
                    {
                        exact = true;
                        continue;
                    }
                    else if (parameterType != null && _application.Converts(parameterType, argType))
                    {
                        exact = true;
                        continue;
                    }
                    else if (parameterType != null && argType != parameterType && argType.MakeByRefType() != parameterType &&
                             !CanConvertNumber(args[i], parameterType))
                    {
                        exact = false;
                    }
                }

                if (!exact && i < args.Length && i < parameters.Length)
                {
                    if (argType != null && PlayerType.IsAssignableFrom(argType))
                    {
                        if (_gameTypes.Player.IsAssignableFrom(parameterType))
                        {
                            exact = true;
                            continue;
                        }

                        if (parameterType.IsAssignableFrom(PlayerType))
                        {
                            exact = true;
                            continue;
                        }

                        exact = false;
                    }

                    if (argType != null && _gameTypes.Player.IsAssignableFrom(argType))
                    {
                        if (PlayerType.IsAssignableFrom(parameterType))
                        {
                            exact = true;
                            continue;
                        }

                        exact = parameterType.IsAssignableFrom(_gameTypes.Player);
                        if (exact)
                        {
                            continue;
                        }
                    }

                    if (_application.Converts(parameterType, argType))
                    {
                        exact = true;
                        continue;
                    }

                    if (argType == parameterType || argType.MakeByRefType() == parameterType || parameterType == ObjectType)
                    {
                        continue;
                    }

                    if (argType.IsValueType)
                    {
                        if (!TypeDescriptor.GetConverter(parameterType).CanConvertFrom(argType) && !CanConvertNumber(args[i], parameterType))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (parameterType.IsGenericParameter && parameterType.BaseType != null)
                        {
                            return parameterType.BaseType.IsAssignableFrom(argType);
                        }

                        return parameterType.IsInstanceOfType(args[i]);
                    }
                }
            }

            return true;
        }

        internal bool TryMapParameters(object[] hookArgs, int received, HookEvent e)
        {
            object @object;
            object hookArg;
            Type hookArgType;
            ParameterInfo parameter;
            int total = received > parameters.Length ? parameters.Length : received;
            for (int n = 0; n < total; n++)
            {
                parameter = parameters[n];
                if (parameter.IsOut || parameter.ParameterType.IsByRef)
                {
                    continue;
                }

                hookArg = hookArgs[n];
                hookArgType = hookArg?.GetType();
                if (hookArg != null && hookArgType == parameter.ParameterType)
                {
                    continue;
                }
                if (_gameTypes.Player.IsAssignableFrom(parameter.ParameterType) && (e.Context.ContainsType(PlayerType) || (PlayerType.IsAssignableFrom(hookArgType) && hookArgType != null)))
                {
                    // Substitute GamePlayer from IPlayer
                    if (e.Context.ContainsType(PlayerType))
                    {
                        hookArgs[n] = e.Context.player.Object;
                    }
                    else
                    {
                        hookArgs[n] = ((IPlayer)hookArg)?.Object;
                    }
                }
                else if (hookArgType != null && _gameTypes.Player.IsAssignableFrom(hookArgType) && PlayerType.IsAssignableFrom(parameter.ParameterType))
                {
                    // Substitute IPlayer from GamePlayer
                    hookArgs[n] = _gameTypes.GetPlayer(hookArg);
                }
                else if (e.context.TryInject(parameter.ParameterType, out @object))
                {
                    // Inject object from hook context container
                    hookArgs[n] = @object;
                }
                else if (_application.TryInject(parameter.ParameterType, plugin, hookArg, out @object))
                {
                    // Inject object from application service container
                    hookArgs[n] = @object;
                }
                else if (_application.Converts(parameter.ParameterType, hookArgType))
                {
                    // Cancel hook call when converter exists but returns null
                    return false;
                }
            }

            if (hookArgs.Length <= received)
            {
                return true;
            }

            // Create additional parameters for arguments excluded in this hook call
            for (int n = received; n < hookArgs.Length; n++)
            {
                hookArg = hookArgs[n];
                hookArgType = hookArg?.GetType();
                parameter = parameters[n];
                if (parameter.DefaultValue != null && parameter.DefaultValue != DBNull.Value)
                {
                    // Use the default value that was provided by the method definition
                    hookArgs[n] = parameter.DefaultValue;
                }
                else if (parameter.ParameterType.IsValueType)
                {
                    // Use the default value for value types
                    hookArgs[n] = Activator.CreateInstance(parameter.ParameterType);
                }
                else if (typeof(HookEvent).IsAssignableFrom(parameter.ParameterType))
                {
                    // Use hook HookEvent if hook requires it
                    hookArgs[n] = e;
                }
                if (parameter.ParameterType == _gameTypes.Player && (e.Context.ContainsType(PlayerType) || (PlayerType.IsAssignableFrom(hookArgType) && hookArgType != null)))
                {
                    // Substitute GamePlayer from IPlayer
                    if (e.Context.ContainsType(PlayerType))
                    {
                        hookArgs[n] = e.Context.player.Object;
                    }
                    else
                    {
                        hookArgs[n] = ((IPlayer)hookArg)?.Object;
                    }
                }
                else if (hookArgType != null && hookArgType.IsAssignableFrom(_gameTypes.Player) && parameter.ParameterType == PlayerType)
                {
                    // Substitute IPlayer from GamePlayer
                    hookArgs[n] = _gameTypes.GetPlayer(hookArg);
                }
                else if (e.context.TryInject(parameter.ParameterType, out @object))
                {
                    // Inject object from hook context container
                    hookArgs[n] = @object;
                }
                else if (_application.TryInject(parameter.ParameterType, plugin, hookArg, out @object))
                {
                    // Inject object from application service container
                    hookArgs[n] = @object;
                }
            }
            return true;
        }

        /// <summary>
        /// Determine if the specified type can be assigned a null value
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool CanAssignNull(Type type)
        {
            return !type.IsValueType || Nullable.GetUnderlyingType(type) != null;
        }

        /// <summary>
        /// Determine if the specified object is a number
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool IsNumber(object obj)
        {
            return obj != null && IsNumber(Nullable.GetUnderlyingType(obj.GetType()) ?? obj.GetType());
        }

        /// <summary>
        /// Determine if specified type is a number
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool IsNumber(Type type)
        {
            if (type.IsPrimitive)
            {
                return type != typeof(bool) && type != typeof(char) && type != typeof(IntPtr) && type != typeof(UIntPtr);
            }

            return type == typeof(decimal);
        }

        /// <summary>
        /// Determine if specified value can be converted to a number
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private bool CanConvertNumber(object value, Type type)
        {
            return IsNumber(value) && IsNumber(type) && TypeDescriptor.GetConverter(type).IsValid(value);
        }
    }
}
