﻿using System;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Plugins
{
    public abstract class BasePlugin
    {
        #region CallHook

        /// <summary>
        /// Calls a hook on this plugin
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallHook(string hook, params object[] args)
        {
            return CallHook(hook, args, null);
        }

        /*
        public object CallHook(string hook)
        {
            return CallHook(hook, null, null);
        }
        */

        public object CallHook<TArg1>(string hook, TArg1 obj1)
        {
            object[] array = ArrayPool.Get(1);
            array[0] = obj1;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2>(string hook, TArg1 obj1, TArg2 obj2)
        {
            object[] array = ArrayPool.Get(2);
            array[0] = obj1;
            array[1] = obj2;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3)
        {
            object[] array = ArrayPool.Get(3);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, TArg4 obj4, TArg5 obj5)
        {
            object[] array = ArrayPool.Get(5);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, TArg4 obj4, TArg5 obj5, TArg6 obj6)
        {
            object[] array = ArrayPool.Get(6);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            array[5] = obj6;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook(string hook)
        {
            HookName hookName = HookName.Cache.Get(hook);
            return CallHook(hookName);
        }

        public object CallHook(string hook, object[] args, IEventArgs e)
        {
            HookName hookName = HookName.Cache.Get(hook);
            return CallHook(hookName, args, e as HookEvent);
        }

        /*
        public object CallHook(string hook, object[] args = null, HookEvent e = null)
        {
            HookName hookName = HookName.Cache.Get(hook);
            return CallHook(hookName, args, e);
        }
        */

        public object CallHook(IHookName hookName, object[] args = null, IEventArgs e = null)
        {
            return CallHook(hookName as HookName, args, e as HookEvent);
        }

        public abstract object CallHook(HookName hookName, object[] args = null, HookEvent e = null);

        #endregion CallHook

        #region Call

        /// <summary>
        /// Calls a hook on this plugin
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object Call(string hook, params object[] args) => CallHook(hook, args);

        /// <summary>
        /// Calls a hook on this plugin and converts the return value to the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="hook"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public T Call<T>(string hook, params object[] args) => (T)Convert.ChangeType(CallHook(hook, args), typeof(T));

        #endregion Call

        #region CallHook Out/ByRef Parameter Combinations

        public object CallHook<TArg1>(string hook, out TArg1 obj1)
        {
            object[] array = ArrayPool.Get(1);
            object ret = CallHook(hook, array, null);
            obj1 = (TArg1)array[0];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2>(string hook, TArg1 obj1, out TArg2 obj2)
        {
            object[] array = ArrayPool.Get(2);
            array[0] = obj1;
            object ret = CallHook(hook, array, null);
            obj2 = (TArg2)array[1];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2>(string hook, out TArg1 obj1, out TArg2 obj2)
        {
            object[] array = ArrayPool.Get(2);
            object ret = CallHook(hook, array, null);
            obj1 = (TArg1)array[0];
            obj2 = (TArg2)array[1];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3>(string hook, TArg1 obj1, TArg2 obj2, out TArg3 obj3)
        {
            object[] array = ArrayPool.Get(3);
            array[0] = obj1;
            array[1] = obj2;
            object ret = CallHook(hook, array, null);
            obj3 = (TArg3)array[2];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3>(string hook, TArg1 obj1, out TArg2 obj2, out TArg3 obj3)
        {
            object[] array = ArrayPool.Get(3);
            array[0] = obj1;
            object ret = CallHook(hook, array, null);
            obj2 = (TArg2)array[1];
            obj3 = (TArg3)array[2];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3>(string hook, out TArg1 obj1, out TArg2 obj2, out TArg3 obj3)
        {
            object[] array = ArrayPool.Get(3);
            object ret = CallHook(hook, array, null);
            obj1 = (TArg1)array[0];
            obj2 = (TArg2)array[1];
            obj3 = (TArg3)array[2];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3>(string hook, TArg1 obj1, out TArg2 obj2, TArg3 obj3)
        {
            object[] array = ArrayPool.Get(3);
            array[0] = obj1;
            array[2] = obj3;
            object ret = CallHook(hook, array, null);
            obj2 = (TArg2)array[1];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, out TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            object ret = CallHook(hook, array, null);
            obj4 = (TArg4)array[3];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, TArg2 obj2, out TArg3 obj3, out TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = obj1;
            array[1] = obj2;
            object ret = CallHook(hook, array, null);
            obj3 = (TArg3)array[2];
            obj4 = (TArg4)array[3];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, TArg2 obj2, out TArg3 obj3, TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = obj1;
            array[1] = obj2;
            array[3] = obj4;
            object ret = CallHook(hook, array, null);
            obj3 = (TArg3)array[2];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, out TArg2 obj2, out TArg3 obj3, out TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = obj1;
            object ret = CallHook(hook, array, null);
            obj2 = (TArg2)array[1];
            obj3 = (TArg3)array[2];
            obj4 = (TArg4)array[3];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, out TArg1 obj1, out TArg2 obj2, out TArg3 obj3, out TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            object ret = CallHook(hook, array, null);
            obj1 = (TArg1)array[0];
            obj2 = (TArg2)array[1];
            obj3 = (TArg3)array[2];
            obj4 = (TArg4)array[3];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, out TArg2 obj2, out TArg3 obj3, TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = obj1;
            array[3] = obj4;
            object ret = CallHook(hook, array, null);
            obj2 = (TArg2)array[1];
            obj3 = (TArg3)array[2];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4>(string hook, TArg1 obj1, out TArg2 obj2, TArg3 obj3, TArg4 obj4)
        {
            object[] array = ArrayPool.Get(4);
            array[0] = obj1;
            array[3] = obj4;
            array[2] = obj3;
            object ret = CallHook(hook, array, null);
            obj2 = (TArg2)array[1];
            ArrayPool.Free(array);
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, TArg4 obj4, out TArg5 obj5)
        {
            object[] array = ArrayPool.Get(5);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            obj5 = (TArg5)array[4];
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, out TArg4 obj4, out TArg5 obj5)
        {
            object[] array = ArrayPool.Get(5);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            obj4 = (TArg4)array[3];
            obj5 = (TArg5)array[4];
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, TArg1 obj1, TArg2 obj2, TArg3 obj3, out TArg4 obj4, TArg5 obj5)
        {
            object[] array = ArrayPool.Get(5);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[4] = obj5;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            obj4 = (TArg4)array[3];
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, TArg1 obj1, TArg2 obj2, out TArg3 obj3, out TArg4 obj4, out TArg5 obj5)
        {
            object[] array = ArrayPool.Get(5);
            array[0] = obj1;
            array[1] = obj2;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            obj3 = (TArg3)array[2];
            obj4 = (TArg4)array[3];
            obj5 = (TArg5)array[4];
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, TArg1 obj1, out TArg2 obj2, out TArg3 obj3, out TArg4 obj4, out TArg5 obj5)
        {
            object[] array = ArrayPool.Get(5);
            array[0] = obj1;
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            obj2 = (TArg2)array[1];
            obj3 = (TArg3)array[2];
            obj4 = (TArg4)array[3];
            obj5 = (TArg5)array[4];
            return ret;
        }

        public object CallHook<TArg1, TArg2, TArg3, TArg4, TArg5>(string hook, out TArg1 obj1, out TArg2 obj2, out TArg3 obj3, out TArg4 obj4, out TArg5 obj5)
        {
            object[] array = ArrayPool.Get(5);
            object ret = CallHook(hook, array, null);
            ArrayPool.Free(array);
            obj1 = (TArg1)array[0];
            obj2 = (TArg2)array[1];
            obj3 = (TArg3)array[2];
            obj4 = (TArg4)array[3];
            obj5 = (TArg5)array[4];
            return ret;
        }

        #endregion CallHook Out/ByRef Parameter Combinations
    }
}
