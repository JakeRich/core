﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using uMod.Apps;
using uMod.Common;
using uMod.Localization;
using uMod.Plugins.Compiler;

namespace uMod.Plugins
{
    public class PluginProvider : IPluginProvider
    {
        /// <summary>
        /// Enumerable containing all loaded plugins
        /// </summary>
        public IEnumerable<IPlugin> All { get; }

        /// <summary>
        /// Compiler client
        /// </summary>
        internal Client Compiler;

        /// <summary>
        /// Compiler application
        /// </summary>
        internal Apps.Compiler Application;

        /// <summary>
        /// Plugins configuration
        /// </summary>
        public IPluginConfiguration Configuration { get; }

        /// <summary>
        /// Plugin manager (handles hook dispatching)
        /// </summary>
        public IPluginManager Manager { get; private set; }

        /// <summary>
        /// The .cs file loader
        /// </summary>
        internal CompilablePluginLoader Loader;

        internal IExtensionProvider ExtensionProvider;

        private readonly ILogger _logger;

        private readonly CoreLocalization _strings;

        public string Directory;

        private readonly IModule _module;

        internal bool IsInitialized;

        /// <summary>
        /// Create a new instance of the Provider class
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        public PluginProvider(IModule module, CoreLocalization strings, IExtensionProvider extensionProvider, IAppInfo compiler, Configuration.Plugins configuration, ILogger logger)
        {
            _module = module;
            Directory = module.PluginDirectory;
            _strings = strings;
            _logger = logger;
            ExtensionProvider = extensionProvider;
            Configuration = configuration;
            Loader = new CompilablePluginLoader(logger, this);
            Application = new Apps.Compiler(compiler, Loader, logger);
            Compiler = new Client(this, Loader);
        }

        internal void CreateManager()
        {
            Manager = new PluginManager(_logger);
        }

        /// <summary>
        /// Returns if a plugin has been loaded by the specified name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public bool Exists(string pluginName) => Manager.GetPlugin(pluginName) != null;

        /// <summary>
        /// Returns the object of a loaded plugin with the specified name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public IPlugin Get(string pluginName) => Manager.GetPlugin(pluginName);

        /// <summary>
        /// Returns the object of a loaded plugin with the specified name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public T Get<T>(string pluginName = null) where T : IPlugin => Manager.GetPlugin<T>(pluginName);

        /// <summary>
        /// Returns an enumerable list of plugins that implement the specified type or interface
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> Find<T>() where T : IPlugin
        {
            return Manager.GetPlugins<T>();
        }

        public void LoadAll(bool init = false)
        {
            if(init)
            {
                Compiler.Initialize();
            }
            IEnumerable<IPluginLoader> loaders = ExtensionProvider.GetLoaders().ToArray();
            // Load all core plugins first
            LoadOnce(loaders);

            // Scan the plugin directory and load all reported plugins
            foreach (IPluginLoader loader in loaders)
            {
                Compiler.Paused = true;
                foreach (FileInfo file in loader.ScanDirectory(Directory))
                {
                    Load(Utility.GetFileNameWithoutExtension(file.Name));
                }
            }

            Compiler.Paused = false;

            if (init)
            {
                float lastCall = _module.Now;
                int attempts = 0;
                foreach (IPluginLoader loader in loaders)
                {
#if DEBUG
                    int maxAttempts = 1500;
#else
                    int maxAttempts = 500;
#endif

                    // Wait until all async plugins have finished loading
                    while (loader.LoadingPlugins.Count > 0)
                    {
                        Thread.Sleep(25);
                        Compiler.ProcessMainThread();
                        _module.OnFrame(_module.Now - lastCall);
                        lastCall = _module.Now;
                        attempts++;

                        
                        if (attempts > maxAttempts)
                        {
                            _logger?.Error(_strings.PluginLoader.LoadTimeout.Interpolate("plugins", string.Join(", ", loader.LoadingPlugins.ToArray())));
                            loader.LoadingPlugins.Clear();
                            break;
                        }
                    }
                }

                IsInitialized = true;
            }
        }

        /// <summary>
        /// Unloads all plugins
        /// </summary>
        /// <param name="skip"></param>
        public void UnloadAll(IEnumerable<string> skip = null)
        {
            foreach (IPlugin plugin in Manager.GetPlugins().Where(p => !p.IsCorePlugin && (skip == null || !skip.Contains(p.Name))).ToArray())
            {
                Unload(plugin.Name);
            }
        }

        /// <summary>
        /// Reloads all plugins
        /// </summary>
        /// <param name="skip"></param>
        public void ReloadAll(IEnumerable<string> skip = null)
        {
            foreach (IPlugin plugin in Manager.GetPlugins().Where(p => !p.IsCorePlugin && (skip == null || !skip.Contains(p.Name))).ToArray())
            {
                Reload(plugin.Name);
            }
        }

        /// <summary>
        /// Reloads specified plugins
        /// </summary>
        /// <param name="pluginNames"></param>
        public void Reload(IEnumerable<string> pluginNames)
        {
            foreach (string name in pluginNames)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    Reload(name);
                }
            }
        }

        /// <summary>
        /// Load core plugins from specified loaders
        /// </summary>
        /// <param name="loaders"></param>
        internal void LoadOnce(IEnumerable<IPluginLoader> loaders)
        {
            foreach (IPluginLoader loader in loaders)
            {
                if (!loader.HasLoadedCorePlugins)
                {
                    loader.LoadCorePlugins();
                }
            }
        }

        /// <summary>
        /// Load specified plugin loaders
        /// </summary>
        /// <param name="loaders"></param>
        internal void Load(IEnumerable<IPluginLoader> loaders)
        {
            foreach (IPluginLoader loader in loaders)
            {
                loader.LoadCorePlugins();
            }
        }

        /// <summary>
        /// Loads specified plugins
        /// </summary>
        /// <param name="pluginNames"></param>
        public void Load(IEnumerable<string> pluginNames)
        {
            Compiler.Paused = true;
            foreach (string name in pluginNames)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    Load(name);
                }
            }

            Compiler.Paused = false;
        }

        /// <summary>
        /// Unloads specified plugins
        /// </summary>
        /// <param name="pluginNames"></param>
        public void Unload(IEnumerable<string> pluginNames)
        {
            foreach (string name in pluginNames)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    Unload(name);
                }
            }
        }

        /// <summary>
        /// Loads a plugin by the given name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public bool Load(string pluginName)
        {
            // Check if the plugin is already loaded
            // TODO: This requires the full path right now, probably should register without
            if (Manager.GetPlugin(pluginName) != null)
            {
                return false;
            }

            pluginName = Utility.GetFileNameWithoutExtension(pluginName); // Necessary to make sure path is removed from plugin name

            // Find all plugin loaders that lay claim to the name
            HashSet<IPluginLoader> loaders = new HashSet<IPluginLoader>(ExtensionProvider.GetLoaders()
                .Where(l => l.ScanDirectory(Directory).Any(f => f.Name.StartsWith(pluginName))));
            if (loaders.Count == 0)
            {
                _logger?.Error(_strings.PluginLoader.ScriptMissing.Interpolate("plugin", pluginName));
                return false;
            }

            if (loaders.Count > 1)
            {
                _logger?.Error(_strings.PluginLoader.ScriptDuplicate.Interpolate("plugin", pluginName));
                return false;
            }

            // Load it and watch for errors
            IPluginLoader loader = loaders.First();

            // Get all plugin file info to load
            FileInfo pluginFileInfo = loader.ScanDirectory(Directory).First(f => f.Name.StartsWith(pluginName));

            // Try to load plugin file
            try
            {
                loader.Load(pluginFileInfo.DirectoryName, pluginName);
                return true;
            }
            catch (Exception ex)
            {
                _logger?.Report(_strings.PluginLoader.PluginLoadFailure.Interpolate("plugin", pluginName), ex);
                return false;
            }
        }

        /// <summary>
        /// Unloads the plugin by the given name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public bool Unload(string pluginName)
        {
            // Get the plugin
            IPlugin plugin = Manager.GetPlugin(pluginName);
            if (plugin != null)
            {
                IPluginLoader loader = ExtensionProvider.GetLoaders().SingleOrDefault(l => l.LoadedPlugins.ContainsKey(plugin.Name));

                loader?.PluginUnloaded(plugin);
            }

            // Let the plugin loader know that this plugin is being unloaded
            return false;
        }

        /// <summary>
        /// Reloads the plugin by the given name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public bool Reload(string pluginName)
        {
            bool isNested = false;
            string directory = Directory;
            if (pluginName.Contains("\\"))
            {
                isNested = true;
                string subPath = Path.GetDirectoryName(pluginName);
                if (subPath != null)
                {
                    directory = Path.Combine(directory, subPath);
                    pluginName = Utility.GetFileNameWithoutExtension(pluginName);
                }
            }

            IPluginLoader loader = ExtensionProvider.GetLoaders().FirstOrDefault(l => l.ScanDirectory(directory).Any(f => f.Name.StartsWith(pluginName)));
            if (loader != null)
            {
                loader.Reload(directory, pluginName);
                return true;
            }

            if (isNested)
            {
                return false;
            }

            Unload(pluginName);
            Load(pluginName);
            return true;
        }

        

        /// <summary>
        /// Calls the specified hook
        /// </summary>
        /// <param name="hookName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallHook(string hookName, params object[] args) => Interface.Call(hookName, args);

        /// <summary>
        /// Gets an array of all currently loaded plugins
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Plugin> GetAll() => Manager.GetPlugins().Cast<Plugin>();
    }
}
