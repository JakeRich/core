﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Auth;
using uMod.Common;
using uMod.Libraries;
using uMod.Pooling;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a permission gate class
    /// </summary>
    public class PermissionGate : Gate, IPermissionGate
    {
        private readonly string _prefix;
        private readonly Permission _permissionLib = Interface.uMod.Libraries.Get<Permission>();

        /// <summary>
        /// List of all permissions supported by this gate
        /// </summary>
        public override IEnumerable<string> Policies => PolicyCallbacks.Keys.Union(_permissionLib.GetPermissions(Plugin));

        /// <summary>
        /// List of all permissions supported by this gate
        /// </summary>
        public IEnumerable<string> Permissions => _permissionLib.GetPermissions(Plugin);

        /// <summary>
        /// Create a permission gate object for the specified plugin
        /// </summary>
        /// <param name="plugin"></param>
        public PermissionGate(IPlugin plugin) : base(plugin)
        {
            _prefix = plugin?.Name.ToLower();
        }

        /// <summary>
        /// Determine if player is allowed to use permission
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public bool Allows(string permission, IPlayer player)
        {
            if (PolicyCallbacks.TryGetValue(permission.ToLower(), out KeyValuePair<MethodInfo, Func<object, object[], bool>> callback))
            {
                object[] args = ArrayPool.Get(1);
                args[0] = player;

                try
                {
                    return callback.Value.Invoke(this, args);
                }
                finally
                {
                    ArrayPool.Free(args);
                }
            }

            return _permissionLib.UserHasPermission(player.Id, $"{_prefix}.{permission.ToLower()}");
        }

        /// <summary>
        /// Determine if player is not allowed to use permission
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public bool Denies(string permission, IPlayer player)
        {
            if (PolicyCallbacks.TryGetValue(permission.ToLower(), out KeyValuePair<MethodInfo, Func<object, object[], bool>> callback))
            {
                object[] args = ArrayPool.Get(1);
                args[0] = player;

                try
                {
                    return !callback.Value.Invoke(this, args);
                }
                finally
                {
                    ArrayPool.Free(args);
                }
            }

            return !_permissionLib.UserHasPermission(player.Id, $"{_prefix}.{permission.ToLower()}");
        }

        /// <summary>
        /// Determine if gate supports specified permission
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public override bool Has(string permission)
        {
            return base.Has(permission) || _permissionLib.PermissionExists($"{_prefix}.{permission}", Plugin);
        }

        /// <summary>
        /// Register permission policy with optional custom callback
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public override IGate Register(string permission, Func<bool> callback)
        {
            if (callback != null)
            {
                return base.Register(permission, callback);
            }

            return Register(permission);
        }

        /// <summary>
        /// Register permission policy
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public IGate Register(string permission)
        {
            _permissionLib.RegisterPermission($"{_prefix}.{permission}", Plugin);
            return this;
        }

        /// <summary>
        /// Register permission policy with optional custom callback
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IGate Register(string permission, Func<object, object[], bool> callback)
        {
            if (callback != null)
            {
                PolicyCallbacks.Add(permission, new KeyValuePair<MethodInfo, Func<object, object[], bool>>(callback.Method, callback));
            }
            else
            {
                _permissionLib.RegisterPermission($"{_prefix}.{permission}", Plugin);
            }
            return this;
        }
    }
}
