﻿using System;
using System.Collections.Generic;
using System.Reflection;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Wrapper for dynamically managed plugin fields
    /// </summary>
    public class PluginFieldInfo
    {
        public IPlugin Plugin;
        public FieldInfo Field;
        public Type FieldType;
        public Type PlayerType;
        public Type[] GenericArguments;
        public Dictionary<string, MethodInfo> Methods = new Dictionary<string, MethodInfo>();

        public PluginFieldInfo(IPlugin plugin, FieldInfo field)
        {
            Plugin = plugin;
            Field = field;
            FieldType = field.FieldType;
            GenericArguments = FieldType.GetGenericArguments();
        }

        public bool HasValidConstructor(params Type[] argumentTypes)
        {
            Type type = GenericArguments[1];
            return type.GetConstructor(new Type[0]) != null || type.GetConstructor(argumentTypes) != null;
        }

        public object Value => Field.GetValue(Plugin);

        public bool LookupMethod(string methodName, params Type[] argumentTypes)
        {
            MethodInfo method = FieldType.GetMethod(methodName, argumentTypes);
            if (method != null)
            {
                Methods[methodName] = method;
                return true;
            }

            return false;
        }

        public object Call(string methodName, params object[] args)
        {
            if (!Methods.TryGetValue(methodName, out MethodInfo method))
            {
                method = FieldType.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public);
                Methods[methodName] = method;
            }
            if (method == null)
            {
                throw new MissingMethodException(FieldType.Name, methodName);
            }

            return method.Invoke(Value, args);
        }
    }
}
