﻿using uMod.IO;

namespace uMod.Plugins.Watchers
{
    /// <summary>
    /// Represents a file system watcher
    /// </summary>
    public sealed class ConfigWatcher : AbstractWatcher
    {
        /// <summary>
        /// Initializes a new instance of the SourceWatcher class
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="filter"></param>
        public ConfigWatcher(string directory, string filter) : base(directory, filter)
        {
        }

        /// <summary>
        /// Called when the watcher has registered a file system change
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileChangeType"></param>
        protected override void watcher_Changed(string path, FileChangeType fileChangeType)
        {
            if (Interface.uMod.ConfigChanges.Contains(path))
            {
                Interface.uMod.ConfigChanges.Remove(path);
            }
            else
            {
                base.watcher_Changed(path, fileChangeType);
            }
        }
    }
}
