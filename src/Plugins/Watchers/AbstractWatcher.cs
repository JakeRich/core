using System.Collections.Generic;
using System.IO;
#if !NETSTANDARD
using System.Security.Permissions;
#endif
using System.Text.RegularExpressions;
using uMod.IO;
using FileSystemWatcher = uMod.IO.FileSystemWatcher;

namespace uMod.Plugins.Watchers
{
    /// <summary>
    /// Represents a filesystem watcher for a specific directory and set of files
    /// </summary>
    public abstract class AbstractWatcher : ChangeWatcher
    {
        /// <summary>
        /// Represents a filesystem change before being processed
        /// </summary>
        protected class QueuedChange
        {
            internal FileChangeType Type;
            internal Libraries.Timer.TimerInstance Timer;
        }

        /// <summary>
        /// The file system watcher
        /// </summary>
        protected FileSystemWatcher Watcher;

        /// <summary>
        /// Changes are buffered briefly to avoid duplicate events
        /// </summary>
        protected Dictionary<string, QueuedChange> ChangeQueue;

        /// <summary>
        /// Gets the timer library
        /// </summary>
        protected Libraries.Timer Timers;

        /// <summary>
        /// Initializes a new instance of the AbstractWatcher class
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="filter"></param>
        protected AbstractWatcher(string directory, string filter)
        {
            WatchedFiles = new HashSet<string>();
            ChangeQueue = new Dictionary<string, QueuedChange>();
            Timers = Interface.uMod.Libraries.Get<Libraries.Timer>();

            LoadWatcher(directory, filter);
        }

        /// <summary>
        /// Loads the file system watcher
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="filter"></param>
#if !NETSTANDARD
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
#endif
        protected void LoadWatcher(string directory, string filter)
        {
            // Create the watcher
            Watcher = new FileSystemWatcher(Interface.uMod, Interface.uMod.RootLogger, directory, filter);
            Watcher.Notify.Add(watcher_Changed);
        }

        /// <summary>
        /// Called when the watcher has registered a file system change
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileChangeType"></param>
        protected virtual void watcher_Changed(string path, FileChangeType fileChangeType)
        {
            int length = path.Length - Watcher.Path.Length - (Path.GetExtension(path)?.Length ?? 0) - 1;
            string subPath = path.Substring(Watcher.Path.Length + 1, length);

            if (!ChangeQueue.TryGetValue(subPath, out QueuedChange change))
            {
                change = new QueuedChange
                {
                    Type = fileChangeType
                };
                ChangeQueue[subPath] = change;
            }
            change.Timer?.Destroy();
            change.Timer = null;

            switch (fileChangeType)
            {
                case FileChangeType.Changed:
                    if (change.Type != FileChangeType.Created)
                    {
                        change.Type = FileChangeType.Changed;
                    }

                    break;

                case FileChangeType.Created:
                    if (change.Type == FileChangeType.Deleted)
                    {
                        change.Type = FileChangeType.Changed;
                    }
                    else
                    {
                        change.Type = FileChangeType.Created;
                    }

                    break;

                case FileChangeType.Deleted:
                    if (change.Type == FileChangeType.Created)
                    {
                        ChangeQueue.Remove(subPath);
                        return;
                    }

                    change.Type = FileChangeType.Deleted;
                    break;
            }

            Interface.uMod.NextTick(() =>
            {
                change.Timer?.Destroy();
                change.Timer = Timers.Once(.2f, () =>
                {
                    change.Timer = null;
                    ChangeQueue.Remove(subPath);
                    if (Regex.Match(subPath, @"include\\", RegexOptions.IgnoreCase).Success)
                    {
                        if (change.Type == FileChangeType.Created || change.Type == FileChangeType.Changed)
                        {
                            FireChanged(subPath);
                        }

                        return;
                    }

                    switch (change.Type)
                    {
                        case FileChangeType.Changed:
                            if (WatchedFiles.Contains(subPath))
                            {
                                FireChanged(subPath);
                            }
                            else
                            {
                                FireAdded(subPath);
                            }

                            break;

                        case FileChangeType.Created:
                            FireAdded(subPath);
                            break;

                        case FileChangeType.Deleted:
                            if (WatchedFiles.Contains(subPath))
                            {
                                FireRemoved(subPath);
                            }

                            break;
                    }
                });
            });
        }
    }
}
