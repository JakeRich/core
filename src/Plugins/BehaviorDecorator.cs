﻿namespace uMod.Plugins
{
    /// <summary>
    /// Behavior decorator
    /// TODO: MOVE TO uMod.Unity
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    internal abstract class BehaviorDecorator<T1, T2> : HookDecorator
    {
        protected BehaviorDecorator(Plugin plugin) : base(plugin)
        {
        }

        [Hook("Init")]
        public abstract void Init();

        [Hook("Unload")]
        public abstract void Unload();

        /*
        [HookMethod("Unload")]
        public void Unload()
        {
            Plugin.Subscribe(nameof(OnEntitySpawned));

            // TODO: ADD OBJECTS
            foreach (var entity in BaseNetworkable.serverEntities)
            {
                if (entity is T)
                {
                    entity.gameObject.AddComponent<T2>();
                }
            }
        }

        [HookMethod("OnEntitySpawned")]
        void OnEntitySpawned(BaseNetworkable entity)
        {
            if (entity is T)
            {
                entity.gameObject.AddComponent<T2>();
            }
        }

        [HookMethod("OnEntityDestroyed")]
        void OnEntityDestroyed(BaseNetworkable entity)
        {
            if (entity is T)
            {
                entity.gameObject.AddComponent<T2>();
            }
        }
        */
    }
}
