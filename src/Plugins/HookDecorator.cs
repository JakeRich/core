using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a hook decorator
    /// </summary>
    public class HookDecorator : IHookDecorator
    {
        /// <summary>
        /// Plugin associated with hook decorator
        /// </summary>
        public Plugin Plugin { get; }

        /// <summary>
        /// Name of hook decorator
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Determine if decorator is currently registerd
        /// </summary>
        public bool IsRegistered { get; private set; }

        /// <summary>
        /// Create a new instance of the HookDecorator class
        /// </summary>
        /// <param name="plugin"></param>
        public HookDecorator(Plugin plugin)
        {
            Name = Utility.ReflectedName(GetType().Name);
            Plugin = plugin;
        }

        /// <summary>
        /// Register decorated hooks to plugin
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Register()
        {
            if (IsRegistered)
            {
                return Enumerable.Empty<string>();
            }

            List<string> hooks = new List<string>();
            Type type = GetType();
            foreach (MethodInfo method in type.GetMethodsWithAttribute<HookAttribute>(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                HookAttribute hookmethod = method.GetCustomAttribute<HookAttribute>();
                HookName hookName;
                lock (HookName.Cache.HookNameLock)
                {
                    hookName = HookName.Cache.Get(hookmethod.FullName);
                }

                AsyncAttribute asyncAttribute = method.GetCustomAttribute<AsyncAttribute>();
                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    hookmethod.Name = Utility.ReflectedName(method.Name);
                }

                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    continue;
                }

                try
                {
                    if (asyncAttribute != null)
                    {
                        Plugin.Dispatcher.AddHookAsync(hookmethod.FullName, method, this);
                    }
                    else
                    {
                        Plugin.Dispatcher.AddHookMethod(hookmethod.FullName, method, this);
                    }
                    hooks.Add(hookName.name);
                }
                catch (Exception ex)
                {
                    Plugin.logger.Report($"Unable to decorate hook {type.Name}.{hookmethod.Name}", ex);
                }
            }

            Plugin.AttributeResolver.ResolveCommands(this);

            IsRegistered = true;

            return hooks;
        }

        /// <summary>
        /// Unregister decorated hooks from plugin
        /// </summary>
        public void Unregister()
        {
            if (!IsRegistered)
            {
                return;
            }

            Type type = GetType();
            foreach (MethodInfo method in type.GetMethodsWithAttribute<HookAttribute>(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                HookAttribute hookmethod = method.GetCustomAttribute<HookAttribute>();
                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    hookmethod.Name = Utility.ReflectedName(method.Name);
                }

                if (string.IsNullOrEmpty(hookmethod.Name))
                {
                    continue;
                }

                try
                {
                    Plugin.Dispatcher.RemoveHookMethod(hookmethod.FullName, method);
                }
                catch (Exception ex)
                {
                    Plugin.logger.Report($"Unable to remove decorated hook {type.Name}.{hookmethod.Name}", ex);
                }
            }

            foreach (MethodInfo method in GetType().GetMethodsWithAttribute<CommandAttribute>(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                IEnumerable<CommandAttribute> commandAttributes = method.GetCustomAttributes<CommandAttribute>();
                foreach (CommandAttribute commandAttribute in commandAttributes)
                {
                    foreach (string command in commandAttribute.Commands)
                    {
                        Plugin.Commands.Remove(command);
                    }
                }
            }

            IsRegistered = false;
        }

        /// <summary>
        /// Subscribe decorated hook methods
        /// </summary>
        protected virtual void Subscribe()
        {
            IEnumerable<string> hookKeys = Register();
            foreach (string hookKey in hookKeys)
            {
                Plugin.Manager.SubscribeToHook(hookKey, Plugin);
            }
        }

        /// <summary>
        /// Unsubscribe decorated hook methods
        /// </summary>
        protected virtual void Unsubscribe()
        {
            Unregister();
        }
    }
}
