using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Represents a batch plugin loader
    /// </summary>
    public class BatchPluginLoader : PluginLoader, IBatchPluginLoader
    {
        private readonly List<Type> _defaultPlugins = new List<Type>();
        public override Type[] CorePlugins => _corePlugins.ToArray();
        private readonly List<Type> _corePlugins = new List<Type>();

        /// <summary>
        /// Create a new instance of the BatchPluginLoader class
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="defaultPluginTypes"></param>
        public BatchPluginLoader(ILogger logger, params Type[] defaultPluginTypes) : base(logger)
        {
            if (defaultPluginTypes.Length > 0)
            {
                _defaultPlugins.AddRange(defaultPluginTypes);
                _corePlugins.AddRange(defaultPluginTypes);
            }
        }

        /// <summary>
        /// Unload and clear core plugins
        /// </summary>
        public void Reset()
        {
            UnloadCorePlugins();
            ClearPlugins();
        }

        /// <summary>
        /// Register core plugin type to load
        /// </summary>
        /// <param name="pluginTypes"></param>
        public void RegisterPlugin(params Type[] pluginTypes)
        {
            _corePlugins.AddRange(pluginTypes);
        }

        /// <summary>
        /// Unregister core plugin type and unload it
        /// </summary>
        /// <param name="pluginTypes"></param>
        public void UnregisterPlugin(params Type[] pluginTypes)
        {
            foreach (Type pluginType in pluginTypes)
            {
                KeyValuePair<string, IPlugin> plugin = LoadedPlugins
                    .FirstOrDefault(x => pluginType == x.Value?.GetType());

                if (!plugin.IsDefault())
                {
                    try
                    {
                        Interface.uMod.Plugins.Unload(plugin.Key);
                    }
                    catch (Exception ex)
                    {
                        Interface.uMod.LogException("Unable to unregister plugin", ex);
                    }
                }

                if (_corePlugins.Contains(pluginType))
                {
                    _corePlugins.Remove(pluginType);
                }
            }
        }

        /// <summary>
        /// Unload core plugins
        /// </summary>
        private void UnloadCorePlugins()
        {
            foreach (KeyValuePair<string, IPlugin> kvp in LoadedPlugins)
            {
                try
                {
                    Interface.uMod.Plugins.Unload(kvp.Key);
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogException("Unable to unload plugin", ex);
                }
            }

            HasLoadedCorePlugins = false;
        }

        /// <summary>
        /// Reset loading plugins to default plugins
        /// </summary>
        private void ClearPlugins()
        {
            _corePlugins.Clear();
            _corePlugins.AddRange(_defaultPlugins);
        }
    }
}
