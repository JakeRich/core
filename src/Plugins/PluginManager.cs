using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Plugins
{
    public class PluginManagerEvent : Event<IPlugin, IPluginManager>, IContextManagerEvent<IPlugin, IPluginManager> { }

    /// <summary>
    /// Manages a set of plugins
    /// </summary>
    public sealed class PluginManager : IPluginManager
    {
        /// <summary>
        /// Gets the logger to which this plugin manager writes
        /// </summary>
        public ILogger Logger { get; }

        /// <summary>
        /// Called when a plugin has been added
        /// </summary>
        public IEvent<IPlugin> OnContextAdded { get; } = new Event<IPlugin>();

        /// <summary>
        /// Called when a plugin has been removed
        /// </summary>
        public IEvent<IPlugin> OnContextRemoved { get; } = new Event<IPlugin>();

        // All loaded plugins
        private readonly IDictionary<string, IPlugin> _loadedPlugins;

        // All hook subscriptions
        private readonly IDictionary<string, IList<Plugin>> _hookSubscriptions;

        // Stores the last time a deprecation warning was printed for a specific hook
        private readonly Dictionary<string, float> _lastDeprecatedWarningAt = new Dictionary<string, float>();

        // Re-usable conflict list used for hook calls
        private readonly List<string> _hookConflicts = new List<string>();

        private readonly object _hookSubscriptionLock = new object();

        /// <summary>
        /// Initializes a new instance of the PluginManager class
        /// </summary>
        public PluginManager(ILogger logger)
        {
            // Initialize
            _loadedPlugins = new Dictionary<string, IPlugin>();
            _hookSubscriptions = new Dictionary<string, IList<Plugin>>();
            Logger = logger;
        }

        /// <summary>
        /// Adds a plugin to this manager
        /// </summary>
        /// <param name="plugin"></param>
        public bool AddContext(IPlugin plugin)
        {
            if (_loadedPlugins.ContainsKey(plugin.Name))
            {
                return false;
            }
            _loadedPlugins.Add(plugin.Name, plugin);
            plugin.HandleAddedToManager(this);
            OnContextAdded?.Invoke(plugin);
            return true;
        }

        /// <summary>
        /// Consistency method
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool AddPlugin(IPlugin plugin)
        {
            return AddContext(plugin);
        }

        /// <summary>
        /// Removes a plugin from this manager
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool RemoveContext(IPlugin plugin)
        {
            if (plugin is Plugin pluginImpl && _loadedPlugins.ContainsKey(plugin.Name))
            {
                _loadedPlugins.Remove(plugin.Name);
                foreach (IList<Plugin> list in _hookSubscriptions.Values)
                {
                    if (list.Contains(pluginImpl))
                    {
                        list.Remove(pluginImpl);
                    }
                }
                plugin.HandleRemovedFromManager(this);
                OnContextRemoved?.Invoke(plugin);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Consistency method
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool RemovePlugin(IPlugin plugin)
        {
            return RemoveContext(plugin);
        }

        /// <summary>
        /// Removes all plugins from context manager
        /// </summary>
        public void RemoveAll()
        {
            foreach (KeyValuePair<string, IPlugin> kvp in _loadedPlugins)
            {
                RemoveContext(kvp.Value);
            }
        }

        /// <summary>
        /// Gets a plugin by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IPlugin GetPlugin(string name)
        {
            return _loadedPlugins.TryGetValue(name, out IPlugin plugin) ? plugin : null;
        }

        /// <summary>
        /// Gets all plugins managed by this manager
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IPlugin> GetPlugins() => _loadedPlugins.Values;

        /// <summary>
        /// Gets plugin that implement the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetPlugin<T>(string name = null) where T : IPlugin
        {
            return _loadedPlugins.Where(x => x.Value is T && (string.IsNullOrEmpty(name) || (!string.IsNullOrEmpty(name) && x.Key.Equals(name, StringComparison.InvariantCultureIgnoreCase)))).Select(x => (T)x.Value).FirstOrDefault();
        }

        /// <summary>
        /// Gets all plugins that implement the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> GetPlugins<T>() where T : IPlugin
        {
            return _loadedPlugins.Where(x => x.Value is T).Select(x => (T)x.Value);
        }

        /// <summary>
        /// Gets all plugins managed by this manager
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use GetPlugins instead")]
        public IEnumerable<IPlugin> GetAll() => _loadedPlugins.Values;

        /// <summary>
        /// Check if the specified plugin subscribes to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="plugin"></param>
        /// <returns></returns>
        public bool IsSubscribedToHook(string hook, IPlugin plugin)
        {
            return IsSubscribedToHook(hook, plugin, null);
        }

        /// <summary>
        /// Checks if any plugin subscribes to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        /// <returns></returns>
        public bool IsSubscribedToHook(string hook)
        {
            return _hookSubscriptions.ContainsKey(hook);
        }

        /// <summary>
        /// Try to get subscriptions to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="subscriptions"></param>
        /// <returns></returns>
        public bool TryGetSubscriptions(string hook, out IList<Plugin> subscriptions)
        {
            return _hookSubscriptions.TryGetValue(hook, out subscriptions);
        }

        /// <summary>
        /// Check if the specified plugin subscribes to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="plugin"></param>
        /// <param name="parameterFilter"></param>
        /// <returns></returns>
        public bool IsSubscribedToHook(string hook, IPlugin plugin, Type[] parameterFilter)
        {
            if (plugin is Plugin pluginImpl &&
                _loadedPlugins.ContainsKey(plugin.Name) &&
                (plugin.IsCorePlugin || !(hook.StartsWith("I") && char.IsUpper(hook[1]))) &&
                _hookSubscriptions.TryGetValue(hook, out IList<Plugin> sublist))
            {
                if (parameterFilter != null)
                {
                    foreach (Plugin pluginItem in sublist)
                    {
                        if (pluginItem is Plugin pluginItemImpl && pluginItemImpl.Dispatcher.SubstitutesAnyType(hook, parameterFilter))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return sublist.Contains(pluginImpl);
                }
            }

            return false;
        }

        /// <summary>
        /// Subscribes the specified plugin to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="plugin"></param>
        public void SubscribeToHook(string hook, IPlugin plugin)
        {
            bool isInternalHook = hook.StartsWith("I") && char.IsUpper(hook[1]);
            if (plugin is Plugin pluginImpl &&
                _loadedPlugins.ContainsKey(plugin.Name) && (plugin.IsCorePlugin || !isInternalHook))
            {
                lock (_hookSubscriptionLock)
                {
                    if (!_hookSubscriptions.TryGetValue(hook, out IList<Plugin> sublist))
                    {
                        sublist = new List<Plugin>();
                        _hookSubscriptions.Add(hook, sublist);
                    }

                    if (!sublist.Contains(pluginImpl))
                    {
                        sublist.Add(pluginImpl);
                    }
                }
            }
        }

        /// <summary>
        /// Unsubscribes the specified plugin to the specified hook
        /// </summary>
        /// <param name="hook"></param>
        /// <param name="plugin"></param>
        public void UnsubscribeToHook(string hook, IPlugin plugin)
        {
            if (plugin is Plugin pluginImpl &&
                _loadedPlugins.ContainsKey(plugin.Name) && (plugin.IsCorePlugin || !(hook.StartsWith("I") && char.IsUpper(hook[1]))))
            {
                if (_hookSubscriptions.TryGetValue(hook, out IList<Plugin> sublist) && sublist.Contains(pluginImpl))
                {
                    sublist.Remove(pluginImpl);
                }
            }
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager
        /// </summary>
        /// <param name="name"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallHook(string name, params object[] args)
        {
            return CallHook(HookName.Cache.Get(name), args);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager
        /// </summary>
        /// <param name="name"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallHook(HookName name, params object[] args)
        {
            return CallHook(name, args, null);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager
        /// </summary>
        /// <param name="name"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public object CallHook(string name, object[] args, IEventArgs e = null)
        {
            return CallHook(HookName.Cache.Get(name), args, e as HookEvent);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager
        /// </summary>
        /// <param name="name"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public object CallHook(string name, object[] args, HookEvent e = null)
        {
            return CallHook(HookName.Cache.Get(name), args, e);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager
        /// </summary>
        /// <param name="hookName"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public object CallHook(IHookName hookName, object[] args, IEventArgs e = null)
        {
            return CallHook(hookName as HookName, args, e as HookEvent);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager
        /// </summary>
        /// <param name="hookName"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        internal object CallHook(HookName hookName, object[] args, HookEvent e = null)
        {
            if (!_hookSubscriptions.TryGetValue(hookName.name, out IList<Plugin> plugins))
            {
                if (hookName.isEvent)
                {
                    return null;
                }
                if (!_hookSubscriptions.TryGetValue(hookName.AfterHookName.name, out plugins))
                {
                    return null;
                }
            }
            // Locate the sublist
            if (plugins?.Count == 0)
            {
                return null;
            }

            // Create event context
            bool nested = e != null;
            bool pooledEventArgs = false;
            bool pooledHookContext = false;

            HookContext hookContext;

            if (nested)
            {
                // Nest existing hook context
                hookContext = Pools.HookContext.Get();
                pooledHookContext = true;
                hookContext.Parent = e.context;
                e.Bind(hookContext);
            }
            else
            {
                e = Pools.HookEvents.Get();
                pooledEventArgs = true;
                e.context.Reset(hookName.fullyQualifiedHookName, plugins);
                hookContext = e.context;
                e.Start();
            }

            if (!nested && !hookName.isEvent)
            {
                if (_hookSubscriptions.ContainsKey(hookName.StartedHookName.fullyQualifiedHookName))
                {
                    CallHook(hookName.StartedHookName, args, e);
                }

                try
                {
                    hookContext.Invoke(e);
                }
                catch (Exception ex)
                {
                    Logger.Report(Interface.uMod.Strings.Plugin.HookEventFailure.Interpolate(("hook", hookName), ("state", e.State)), ex);
                }
            }

            // Loop each item
            hookContext.returnValues = ArrayPool.Get(plugins.Count);
            object finalValue = null;
            IPlugin finalPlugin = null;
            for (int i = 0; i < plugins.Count; i++)
            {
                // Call the hook
                object returnValue = plugins[i].CallHook(hookName, args, e);
                if (returnValue != null)
                {
                    hookContext.returnValues[i] = returnValue;
                    finalValue = hookContext.finalValue = returnValue;
                    finalPlugin = plugins[i];
                }

                if (!nested && !hookName.isEvent && e.state == EventState.Canceled)
                {
                    if (!hookName.isEvent && _hookSubscriptions.ContainsKey(hookName.CanceledHookName.fullyQualifiedHookName))
                    {
                        CallHook(hookName.CanceledHookName, args, e);
                    }

                    try
                    {
                        hookContext.Invoke(e);
                    }
                    catch (Exception ex)
                    {
                        Logger.Report(Interface.uMod.Strings.Plugin.HookEventFailure.Interpolate(("hook", hookName), ("state", e.State)), ex);
                    }
                }
            }

            if (!nested && !hookName.isEvent)
            {
                hookContext.Validate();

                if (e.state == EventState.Failed)
                {
                    e.Fail();
                    if (_hookSubscriptions.ContainsKey(hookName.FailedHookName.fullyQualifiedHookName))
                    {
                        CallHook(hookName.FailedHookName, args, e);
                    }

                    try
                    {
                        hookContext.Invoke(e);
                    }
                    catch (Exception ex)
                    {
                        Logger.Report(Interface.uMod.Strings.Plugin.HookEventFailure.Interpolate(("hook", hookName), ("state", e.State)), ex);
                    }
                }
                else if (e.state != EventState.Failed && e.state != EventState.Canceled)
                {
                    e.Complete();
                    if (_hookSubscriptions.ContainsKey(hookName.CompletedHookName.fullyQualifiedHookName))
                    {
                        CallHook(hookName.CompletedHookName, args, e);
                    }

                    try
                    {
                        hookContext.Invoke(e);
                    }
                    catch (Exception ex)
                    {
                        Logger.Report(Interface.uMod.Strings.Plugin.HookEventFailure.Interpolate(("hook", hookName), ("state", e.State)), ex);
                    }
                }

                if (_hookSubscriptions.ContainsKey(hookName.AfterHookName.fullyQualifiedHookName))
                {
                    CallHook(hookName.AfterHookName, args, e);
                }
            }

            // Return null if no return value
            if (hookContext.returnValues?.Length == 0)
            {
                if (pooledEventArgs)
                {
                    Pools.HookEvents.Free(e);
                }

                if (pooledHookContext)
                {
                    // Pop nested hook context
                    if (hookContext.Parent != null)
                    {
                        e.Bind(hookContext.Parent);
                    }
                    Pools.HookContext.Free(hookContext);
                }

                return null;
            }

            if (hookContext.Overrides)
            {
                finalValue = hookContext.finalValue;
            }
            else if (hookContext.returnValues?.Length > 1 && !hookContext.Valid)
            {
                // Notify log of hook conflict
                if (hookContext.ReturnConflicts != null)
                {
                    _hookConflicts.Clear();
                    foreach (KeyValuePair<IContext, object> kvp in hookContext.ReturnConflicts)
                    {
                        _hookConflicts.Add($"{kvp.Key.Name} - {kvp.Value} ({kvp.Value.GetType().Name})");
                    }

                    if (_hookConflicts.Count > 0)
                    {
                        if (finalPlugin != null)
                        {
                            _hookConflicts.Add($"{finalPlugin.Name} ({finalValue} ({finalValue.GetType().Name}))");
                        }

                        Logger.Warning(Interface.uMod.Strings.Plugin.HookConflict.Interpolate(
                            ("hook", hookName),
                            ("plugins", string.Join(", ", _hookConflicts.ToArray()))
                        ));
                    }
                }
            }

            if (pooledEventArgs)
            {
                Pools.HookEvents.Free(e);
            }

            if (pooledHookContext)
            {
                if (hookContext?.Parent != null)
                {
                    e.Bind(hookContext.Parent);
                }
                Pools.HookContext.Free(hookContext);
            }
            else
            {
                ArrayPool.Free(hookContext.returnValues);
            }

            return finalValue;
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager and prints a deprecation warning
        /// </summary>
        /// <param name="oldHook"></param>
        /// <param name="newHook"></param>
        /// <param name="expireDate"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallDeprecatedHook(string oldHook, string newHook, DateTime expireDate, params object[] args)
        {
            return CallDeprecatedHook(HookName.Cache.Get(oldHook), HookName.Cache.Get(newHook), expireDate, args, null);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager and prints a deprecation warning
        /// </summary>
        /// <param name="oldHook"></param>
        /// <param name="newHook"></param>
        /// <param name="expireDate"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object CallDeprecatedHook(IHookName oldHook, IHookName newHook, DateTime expireDate, params object[] args)
        {
            return CallDeprecatedHook(oldHook, newHook, expireDate, args, null);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager and prints a deprecation warning
        /// </summary>
        /// <param name="oldHook"></param>
        /// <param name="newHook"></param>
        /// <param name="expireDate"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public object CallDeprecatedHook(string oldHook, string newHook, DateTime expireDate, object[] args, IEventArgs e = null)
        {
            return CallDeprecatedHook(HookName.Cache.Get(oldHook), HookName.Cache.Get(newHook), expireDate, args, e);
        }

        /// <summary>
        /// Calls a hook on all plugins of this manager and prints a deprecation warning
        /// </summary>
        /// <param name="oldHook"></param>
        /// <param name="newHook"></param>
        /// <param name="expireDate"></param>
        /// <param name="args"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        internal object CallDeprecatedHook(IHookName oldHook, IHookName newHook, DateTime expireDate, object[] args, IEventArgs e)
        {
            if (!_hookSubscriptions.TryGetValue(oldHook.FullyQualifiedHookName, out IList<Plugin> plugins) || plugins.Count == 0 || expireDate < DateTime.Now)
            {
                return null;
            }

            float now = Interface.uMod.Now;
            if (!_lastDeprecatedWarningAt.TryGetValue(oldHook.FullyQualifiedHookName, out float lastWarningAt) || now - lastWarningAt > 3600f)
            {
                foreach (IPlugin plugin in plugins)
                {
                    _lastDeprecatedWarningAt[oldHook.FullyQualifiedHookName] = now;

                    Logger.Warning(Interface.uMod.Strings.Plugin.HookDeprecation.Interpolate(
                        ("plugin", plugin.Name),
                        ("version", plugin.Version),
                        ("oldHook", oldHook),
                        ("expiration", expireDate),
                        ("newHook", newHook)
                    ));
                }
            }

            return CallHook(oldHook, args, e);
        }
    }
}
