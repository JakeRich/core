﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using uMod.Common.Compiler;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents an assembly reference
    /// </summary>
    internal class AssemblyReference
    {
        /// <summary>
        /// Gets the assembly name
        /// </summary>
        internal string Name { get; private set; }

        /// <summary>
        /// Gets the assembly directory location
        /// </summary>
        internal string Directory { get; private set; }

        private string _extension = ".dll";

        /// <summary>
        /// Gets the assembly filename
        /// </summary>
        internal string FileName { get { return Name + _extension; } }

        /// <summary>
        /// Gets the assembly full path
        /// </summary>
        internal string FullPath { get; private set; }

        /// <summary>
        /// Gets the last error
        /// </summary>
        internal string Error { get; private set; }

        /// <summary>
        /// Determines if reference is valid
        /// </summary>
        internal bool IsValid { get; private set; }

        /// <summary>
        /// Determines if reference is cached
        /// </summary>
        internal bool IsCached { get; set; }

        /// <summary>
        /// Serializable model sent to application
        /// </summary>
        internal CompilationFile CompilationFile;

        /// <summary>
        /// Create a new instance of the AssemblyReference class
        /// </summary>
        /// <param name="fullPath"></param>
        internal AssemblyReference(string fullPath)
        {
            _extension = Path.GetExtension(fullPath);
            if (string.IsNullOrEmpty(_extension))
            {
                _extension = ".dll";
            }
            Name = Path.GetFileNameWithoutExtension(fullPath);
            Directory = Path.GetDirectoryName(fullPath);
            FullPath = fullPath;
            CompilationFile = new CompilationFile(Directory, FileName);
        }

        /// <summary>
        /// Ensure reference assembly can be loaded and was loaded at least once
        /// </summary>
        /// <param name="pluginName"></param>
        internal bool Validate(string pluginName)
        {
            #region Check if DLL has already been loaded

            //Doesn't matter if the reference is from uMod: any assembly already loaded shall be considered validated at some other point
            //We can do blacklisting elsewhere, or here later
            /*
            if (IsReferenceIncluded(Name))
            {
                // This may eventually handle blacklisting too
            }

            Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            Assembly assembly = loadedAssemblies.FirstOrDefault(x => x.GetName().Name == Name);

            if (assembly != null)
            {
                // Anything already loaded will have it's path already set
                FullPath = assembly.Location;
                // Assume that any loaded assembly is already validated if doing blacklisting here
            }
            */

            #endregion Check if DLL has already been loaded

            #region Look for DLL on disk

            if (!File.Exists(FullPath))
            {
                Error = Interface.uMod.Strings.Compiler.ReferenceAssemblyNotFound.Interpolate(("assembly", FullPath), ("plugin", pluginName));
                return IsValid = false;
            }

            #endregion Look for DLL on disk

            // Later on we could load by bytes to prevent file locking (would allow DLL references to be hot reloaded later on)
            // However that opens up the nightmare of resolving references manually instead of Assembly.Load() doing it for us

            // The compiler only needs the path because it reads the DLL for symbols and compiles against that

            return IsValid = true;
        }

        /// <summary>
        /// Determine if reference is included by default
        /// </summary>
        /// <param name="dllName"></param>
        /// <returns></returns>
        private bool IsReferenceIncluded(string dllName)
        {
            return dllName.StartsWith("uMod.") || dllName.StartsWith("Newtonsoft.Json") || dllName.StartsWith("protobuf-net") || dllName.StartsWith("Rust.");
        }
    }
}
