﻿using System;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents the types of dependency relationships relevant to the compiler
    /// </summary>
    [Flags]
    public enum PluginReferenceType
    {
        None = 0,
        ImplicitOptional = 1,
        ExplicitRequires = 2,
        ExplicitOptional = 4,
        ImplicitRequires = 8,
    }

    /// <summary>
    /// Represents a reference from one plugin to another
    /// </summary>
    internal class PluginReference
    {
        /// <summary>
        /// The name of the plugin being referenced
        /// </summary>
        internal string PluginName { get; private set; }

        /// <summary>
        /// Gets or sets whether or not the reference is valid
        /// </summary>
        internal bool Valid { get; set; }

        /// <summary>
        /// Gets whether or not the reference is optional
        /// </summary>
        internal bool Optional { get => ImplicitOptional || ExplicitOptional; }

        /// <summary>
        /// Gets whether or not the reference is optional
        /// </summary>
        internal bool Required { get => ExplicitRequires || ImplicitRequires; }

        /// <summary>
        /// Gets whether or not the reference is implicit and optional
        /// </summary>
        internal bool ImplicitOptional { get { return (ReferenceTypes & PluginReferenceType.ImplicitOptional) != 0; } }

        /// <summary>
        /// Gets whether or not the reference is explicit and required
        /// </summary>
        internal bool ExplicitRequires { get { return (ReferenceTypes & PluginReferenceType.ExplicitRequires) != 0; } }

        /// <summary>
        /// Gets whether or not the reference is explicit and optional
        /// </summary>
        internal bool ExplicitOptional { get { return (ReferenceTypes & PluginReferenceType.ExplicitOptional) != 0; } }

        /// <summary>
        /// Gets whether or not the reference is implicit and required
        /// </summary>
        internal bool ImplicitRequires { get { return (ReferenceTypes & PluginReferenceType.ImplicitRequires) != 0; } }

        /// <summary>
        /// Gets the reference type
        /// </summary>
        private PluginReferenceType ReferenceTypes { get; set; }

        /// <summary>
        /// Creates a new instance of the PluginReference class
        /// </summary>
        /// <param name="name"></param>
        public PluginReference(string name)
        {
            PluginName = name;
        }

        /// <summary>
        /// Adds a reason that might describe the reference type
        /// </summary>
        /// <param name="types"></param>
        public void AddReason(PluginReferenceType types)
        {
            ReferenceTypes |= types;
        }

        /// <summary>
        /// Determines if the plugin reference is already loaded or ready to be compiled
        /// </summary>
        /// <param name="workQueue"></param>
        /// <param name="compiledPlugins"></param>
        /// <returns></returns>
        public bool IsReady(CompilationStack workQueue, CompiledPluginList compiledPlugins)
        {
            CompiledPlugin loadedPlugin = compiledPlugins.Find(PluginName);
            if (loadedPlugin != null && loadedPlugin.LastGoodPlugin.IsLoaded)
            {
                return true;
            }

            CompilePluginRequest loadingPlugin = workQueue.Find(PluginName);
            if (loadingPlugin != null && loadingPlugin.CurrentStep == PluginCompileStage.ReadyToCompile)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Converts PluginReference to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return PluginName;
        }
    }
}
