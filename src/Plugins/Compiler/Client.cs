using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Plugins.Compiler
{
    internal class Client
    {
        /// <summary>
        /// List of references specific to the core testing environment
        /// </summary>
        private static List<AssemblyReference> TestReferences;

        private int _compileOrderIndex = 1;

        /// <summary>
        /// List of most recent compilation requests keyed by plugin name.
        /// </summary>
        internal Dictionary<string, CompilePluginRequest> RecentRequests = new Dictionary<string, CompilePluginRequest>();

        /// <summary>
        /// Pauses compilation, ensuring plugins in a sequence are always completely loaded
        /// </summary>
        internal bool Paused = true;

        /// <summary>
        /// Pushed from the main thread and taken off by the compile thread
        /// </summary>
        private readonly List<CompileAction> _pendingActions = new List<CompileAction>();

        /// <summary>
        /// Thread safe because it is only touched by the compile thread
        /// </summary>
        private readonly CompilationStack _workQueue = new CompilationStack();

        /// <summary>
        /// Taken off the work queue then loaded on the main thread into the game
        /// </summary>
        private readonly CompilationStack _completedQueue = new CompilationStack();

        /// <summary>
        /// Any compile data for compiled plugins so we can reference them later on when compiling dependancies
        /// </summary>
        private readonly CompiledPluginList _compiledPlugins = new CompiledPluginList();

        private readonly Apps.Compiler _compiler;

        private readonly AssemblyCache _assemblyCache;

        internal CompilablePluginLoader Loader;

        private readonly ILogger _logger;

        /// <summary>
        /// Determines if compiler thread is running
        /// </summary>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// Create a new instance of the Compiler.Client class
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="loader"></param>
        internal Client(PluginProvider provider, CompilablePluginLoader loader)
        {
            _assemblyCache = new AssemblyCache();

            _logger = Interface.uMod.RootLogger;
            Loader = loader;

            _compiler = provider.Application;
        }

        /// <summary>
        /// Initialize the compiler client
        /// </summary>
        internal void Initialize()
        {
            if (IsInitialized)
            {
                return;
            }

            SetupExtensionNames();

            Interface.uMod.Libraries.Get<Libraries.Timer>().Repeat(0.1f, -1, ProcessMainThread);

            ThreadPool.QueueUserWorkItem(ProcessSeperateThread);

            IsInitialized = true;
        }

        /// <summary>
        /// Gets a pending compilation request by the specified plugin name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal CompilePluginRequest GetPendingPlugin(string pluginName)
        {
            lock (_workQueue)
            {
                return _workQueue.Find(pluginName);
            }
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool IsPending(params string[] pluginNames)
        {
            return _IsPending(pluginNames);
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool IsPending(IEnumerable<string> pluginNames)
        {
            return _IsPending(pluginNames);
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        private bool _IsPending(IEnumerable<string> pluginNames)
        {
            lock (_workQueue)
            {
                foreach (string pluginName in pluginNames)
                {
                    if (RecentRequests.TryGetValue(pluginName, out CompilePluginRequest cpr))
                    {
                        if (!cpr.IsFinished)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool AnyPending(params string[] pluginNames)
        {
            return _AnyPending(pluginNames);
        }

        /// <summary>
        /// Check if the specified plugin names are pending compilations
        /// </summary>
        /// <param name="pluginNames"></param>
        /// <returns></returns>
        internal bool AnyPending(IEnumerable<string> pluginNames)
        {
            return _AnyPending(pluginNames);
        }

        private bool _AnyPending(IEnumerable<string> pluginNames)
        {
            lock (_workQueue)
            {
                foreach (string pluginName in pluginNames)
                {
                    if (RecentRequests.TryGetValue(pluginName, out CompilePluginRequest cpr))
                    {
                        if (cpr.IsFinished)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets a successfully compiled plugin by the specified plugin name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal CompiledPlugin GetCompiledPlugin(string pluginName)
        {
            lock (_compiledPlugins)
            {
                return _compiledPlugins.Find(pluginName);
            }
        }

        /// <summary>
        /// Gets a list of all successfully compiled plugins
        /// </summary>
        /// <returns></returns>
        internal CompiledPlugin[] GetAllCompiledPlugins()
        {
            lock (_compiledPlugins)
            {
                return _compiledPlugins.All.ToArray();
            }
        }

        #region Extension Names

        private string[] _extensionNames;
        private string _includePath;
        private string _gameExtensionName;
        private string _gameExtensionBranch;

        /// <summary>
        /// Loads extension details
        /// </summary>
        private void SetupExtensionNames()
        {
            _includePath = Path.Combine(Interface.uMod.PluginDirectory, "include");
            _extensionNames = Interface.uMod.Extensions.All.Select(ext => ext.Name).ToArray();
            IExtension gameExtension = Interface.uMod.Extensions.All.SingleOrDefault(ext => ext.IsGameExtension);
            _gameExtensionName = gameExtension?.Name.Replace("uMod.", string.Empty).Replace("Oxide.", string.Empty).Replace(".", "_").ToUpper();
            _gameExtensionBranch = gameExtension?.Branch?.ToUpper();
        }

        #endregion Extension Names

        #region Recompilation

        /// <summary>
        /// Recompile the specified compilation request at the specified stage
        /// </summary>
        /// <param name="request"></param>
        /// <param name="stage"></param>
        internal void Recompile(CompilePluginRequest request, PluginCompileStage stage = PluginCompileStage.WaitingOnDependencies)
        {
            request.CompiledAssembly = null;
            request.CurrentStep = stage;
            request.Errors.Clear();
            lock (_workQueue)
            {
                _workQueue.Add(request);
            }
        }

        /// <summary>
        /// Recompile the specified compilation requests
        /// </summary>
        /// <param name="requests"></param>
        internal void Recompile(CompilePluginRequest[] requests)
        {
            RecompileAll(PluginCompileStage.WaitingOnDependencies, requests);
        }

        /// <summary>
        /// Recompile the specified requests from the specified stage onward
        /// </summary>
        /// <param name="stage"></param>
        /// <param name="requests"></param>
        internal void RecompileAll(PluginCompileStage stage = PluginCompileStage.WaitingOnDependencies, CompilePluginRequest[] requests = null)
        {
            bool all = (requests == null);
            if (all)
            {
                requests = _workQueue.All.ToArray();
            }

#if DEBUG
            _logger.Debug($"Marking for recompilation {string.Join(", ", requests.Select(x => x.Name).ToArray())}");
#endif
            lock (_workQueue)
            {
                if (all)
                {
                    _workQueue.Clear();
                }

                foreach (CompilePluginRequest request in requests)
                {
                    request.CurrentStep = stage;
                    request.CompiledAssembly = null;
                    request.Errors.Clear();
                    request.TimeoutTime = DateTime.UtcNow.AddSeconds(60);

                    if (!all)
                    {
                        _workQueue.Add(request);
                    }
                }

                if (all)
                {
                    _workQueue.AddRange(requests);
                }
            }
        }

        #endregion Recompilation

        #region Compile

        internal void Compile(PluginReference pluginReference)
        {
            Compile(pluginReference.PluginName);
        }

        internal void Compile(string pluginName)
        {
            OnScriptAdded(Interface.uMod.PluginDirectory, pluginName);
        }

        #endregion

        #region File watcher events

        /// <summary>
        /// Notified when a script at the specified path is added
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        internal void OnScriptAdded(string directory, string fileName)
        {
            string pluginName = FormatPluginName(fileName);

            if (Loader.LoadingPlugins.Contains(pluginName))
            {
#if DEBUG
                _logger.Debug("Pre-mature recompilation due to script add during compilation process");
#endif
                RecompileAll(PluginCompileStage.AwaitingUnload);
                return;
            }

            Loader.LoadingPlugins.Add(pluginName);

            lock (_pendingActions)
            {
                _pendingActions.Add(new CompileAction()
                {
                    Action = CompileActionType.Load,
                    Directory = directory,
                    FileName = fileName,
                    PluginName = pluginName,
                });
            }
        }

        /// <summary>
        /// Notified when a script at the specified path is removed
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        internal void OnScriptRemove(string directory, string fileName)
        {
            // Remove cached CompilePluginRequests when file is removed
            if (RecentRequests.ContainsKey(fileName))
            {
                RecentRequests.Remove(fileName);
            }

            lock (_workQueue)
            {
                if (_workQueue.Count == 0)
                {
                    return;
                }
            }

            string pluginName = FormatPluginName(fileName);

            lock (_workQueue)
            {
                if (_workQueue.Contains(pluginName))
                {
                    _workQueue.Remove(pluginName);
                }
            }
#if DEBUG
            _logger.Debug("Recompiling due to removed script");
#endif
            RecompileAll(PluginCompileStage.PreParseScript);
        }

        /// <summary>
        /// Notified when a script with the specified path is modified
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        internal void OnScriptModified(string directory, string fileName)
        {
            string pluginName = FormatPluginName(fileName);

            if (Loader.LoadingPlugins.Contains(pluginName))
            {
#if DEBUG
                _logger.Debug("Pre-mature recompilation due to script change during compilation process");
#endif
                RecompileAll(PluginCompileStage.PreParseScript);
                return;
            }

            Loader.LoadingPlugins.Add(pluginName);

            lock (_pendingActions)
            {
                _pendingActions.Add(new CompileAction()
                {
                    Action = CompileActionType.Load,
                    Directory = directory,
                    FileName = fileName,
                    PluginName = pluginName,
                });
            }
        }

        #endregion File watcher events

        #region Main Thread

        /// <summary>
        /// Handles main thread
        /// </summary>
        internal void ProcessMainThread()
        {
            ProcessFinishedPlugins();
        }

        #region Plugin handling

        /// <summary>
        /// Process plugin finishes in the compilation stack
        /// </summary>
        private void ProcessFinishedPlugins()
        {
            CompilePluginRequest[] finishedArray = null;

            lock (_completedQueue)
            {
                if (_completedQueue.Count > 0)
                {
                    finishedArray = _completedQueue.All.ToArray();
                }
            }

            if (finishedArray == null)
            {
                return;
            }

            //Now its safe to work on the main thread and load the plugins

            foreach (CompilePluginRequest plugin in finishedArray)
            {
                if (FinishPlugin(plugin))
                {
                    lock (_completedQueue)
                    {
                        //Remove the plugin when we finished processing it
                        _completedQueue.Remove(plugin.PluginName);
                    }

                    Loader.LoadingPlugins.Remove(plugin.PluginName);
                }
            }
        }

        /// <summary>
        /// Finish the compilation process for the specified plugin compilation request
        /// </summary>
        /// <param name="plugin"></param>
        private bool FinishPlugin(CompilePluginRequest plugin)
        {
            if (plugin.HasFailed)
            {
                //What do we do when a plugin failed to compile

                _logger.Error(Interface.uMod.Strings.Compiler.CompileErrors.Interpolate(
                        ("plugin", plugin.PluginName),
                        ("errors", string.Join(Environment.NewLine, plugin.Errors.ToArray()))));

                TryRollback(plugin);
                return true;
            }
            else
            {
                // This calls unload then load, etc
                bool loadingPluginWorked = plugin.LoadRetries > 0 ? plugin.Plugin.IsLoaded : TryLoadPlugin(plugin.Plugin);

                if (loadingPluginWorked == false)
                {
                    if (plugin.LoadRetries > 50)
                    {
                        TryRollback(plugin);
                        return true;
                    }
                    else
                    {
                        plugin.LoadRetries++;
                        return false;
                    }
                }
                else
                {
                    OnPluginFinished(plugin, true);
                }
            }

            
            return true;
        }

        /// <summary>
        /// Tries to rollback the specified compilation request to the last previously loaded plugin
        /// </summary>
        /// <param name="plugin"></param>
        private void TryRollback(CompilePluginRequest plugin)
        {
            plugin.LoadRetries = 0;
            CompiledPlugin oldPlugin = _compiledPlugins.Find(plugin.PluginName);

            if (oldPlugin == null)
            {
                //No plugin was loaded before so just print fail message
                _logger.Info(Interface.uMod.Strings.Compiler.RollbackFailureMissing.Interpolate("script", plugin.FileName));
                OnPluginFinished(plugin, false);
            }
            else
            {
                _logger.Info(Interface.uMod.Strings.Compiler.Rollback.Interpolate("script", plugin.FileName));

                // This calls unload then load, etc
                bool loadingPluginWorked = oldPlugin.LastGoodPlugin != null && TryLoadPlugin(oldPlugin.LastGoodPlugin);

                if (loadingPluginWorked)
                {
                    //The end result was calling Unload() then Load()

                    OnPluginFinished(plugin, true);
                }
                else
                {
                    _logger.Info(Interface.uMod.Strings.Compiler.RollbackFailureLoad.Interpolate("plugin", plugin.FileName));

                    //We have failed to call Load(), call Unload() it to be safe
                    if (oldPlugin.IsLoaded && plugin.CanBeUnloaded)
                    {
                        Loader.PluginUnloaded(oldPlugin.LastGoodPlugin);
                    }

                    OnPluginFinished(plugin, false);
                }
            }
        }

        /// <summary>
        /// Tries to load the specified plugin
        /// </summary>
        /// <param name="pluginClass"></param>
        /// <returns></returns>
        private bool TryLoadPlugin(IPlugin pluginClass)
        {
            if (pluginClass == null)
            {
                throw new ArgumentException("Cannot load null plugin", nameof(pluginClass));
            }

            Loader.Load(pluginClass);

            return pluginClass.IsLoaded;
        }

        /// <summary>
        /// Notified when a plugin compilation request finishes
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="loaded"></param>
        private void OnPluginFinished(CompilePluginRequest plugin, bool loaded)
        {
            plugin.LoadRetries = 0;
            _compiledPlugins.Remove(plugin.PluginName);

            if (loaded)
            {
                _compiledPlugins.Add(new CompiledPlugin(plugin));
                if (Loader.LoadedPlugins.ContainsKey(plugin.PluginName))
                {
                    Loader.LoadedPlugins[plugin.PluginName] = plugin.Plugin;
                }
                else
                {
                    Loader.LoadedPlugins.Add(plugin.PluginName, plugin.Plugin);
                }
            }
        }

        #endregion Plugin handling

        /// <summary>
        /// Add pending compilation requests to compilation stack
        /// </summary>
        private void DoAdds()
        {
            List<CompileAction> temp = Pools.GetList<CompileAction>();

            lock (_pendingActions)
            {
                if (_pendingActions.Count > 0)
                {
                    temp.AddRange(_pendingActions);
                    _pendingActions.Clear();
                }
            }

            if (temp == null || temp.Count == 0)
            {
                return;
            }

            List<CompilePluginRequest> pluginRequests = Pools.GetList<CompilePluginRequest>();

            try
            {
                foreach (CompileAction action in temp)
                {
                    if (action.Action == CompileActionType.Load)
                    {
                        CompilePluginRequest request =
                            new CompilePluginRequest(_logger, action.Directory, action.FileName, action.PluginName);

                        string key = action.PluginName;

                        if (RecentRequests.ContainsKey(key))
                        {
                            RecentRequests[key] = request;
                        }
                        else
                        {
                            RecentRequests.Add(key, request);
                        }

                        pluginRequests.Add(request);
                    }
                }

                if (pluginRequests.Count > 0)
                {
                    lock (_workQueue)
                    {
#if DEBUG
                    _logger.Debug($"Adding {pluginRequests.Count} plugins to work queue: {string.Join(", ", pluginRequests.Select(x => x.Name).ToArray())}");
#endif
                        _workQueue.AddRange(pluginRequests);
                    }
                }
            }
            finally
            {
                Pools.FreeList(ref pluginRequests);
                Pools.FreeList(ref temp);
            }
        }

        #endregion Main Thread

        #region Compile Thread

        /// <summary>
        /// Determines if separate thread should exit
        /// </summary>
        /// <returns></returns>
        private bool ShouldExit()
        {
            return Interface.uMod.IsShuttingDown;
        }

        /// <summary>
        /// Determines if separate thread should wait or skip
        /// </summary>
        /// <returns></returns>
        private bool CheckWaitAndSkip()
        {
            if (Paused)
            {
                Thread.Sleep(25);
                return true;
            }
            bool sleep = false;
            lock (_workQueue)
            {
                DoAdds();

                if (_workQueue.Count == 0)
                {
                    sleep = true;
                }
            }

            if (sleep)
            {
                Thread.Sleep(250);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handle separate thread processing
        /// </summary>
        /// <param name="context"></param>
        private void ProcessSeperateThread(object context)
        {
            while (true)
            {
                if (ShouldExit())
                {
                    return;
                }

                if (CheckWaitAndSkip())
                {
                    continue;
                }

                CompilePluginRequest[] workQueueAll = _workQueue.All.ToArray();

                //Everything below operates on the work queue solely

                ProcessScriptsBeforeCompile(workQueueAll);

                ValidateDependencies(workQueueAll);

                //Order of send / recieve isn't that important, but may as well put recv first
                RecieveCompileRequests();

                TimeOutPurge(workQueueAll);

                SortCompileOrder(workQueueAll);

                SendCompileRequests(workQueueAll);

                CreatePluginInstances(workQueueAll);

                DoRemoves(workQueueAll);

                // 100ms delays between loops will be fine but can increase later if it's important to shave 100ms off total compile time (2000ms usually)
                if (ShouldExit())
                {
                    return;
                }
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Schedules recompilation of dependencies
        /// </summary>
        /// <param name="compiledPlugin"></param>
        /// <param name="workQueueAll"></param>
        /// <returns></returns>
        private bool RecompileTree(CompiledPlugin compiledPlugin, CompilePluginRequest[] workQueueAll)
        {
            bool restart = false;
            // recompile plugins referenced by supplied plugin
            foreach (PluginReference dependencyPlugin in compiledPlugin.PluginReferences)
            {
                if (dependencyPlugin.Optional == false &&
                    workQueueAll.All(x => x.PluginName != dependencyPlugin.PluginName))
                {
                    Compile(dependencyPlugin);
                    restart = true;

                    CompiledPlugin dependencyCompiledPlugin = _compiledPlugins.Find(dependencyPlugin.PluginName);
                    if (compiledPlugin?.IsLoaded ?? false)
                    {
                        RecompileTree(dependencyCompiledPlugin, workQueueAll);
                    }
                }
            }

            IEnumerable<CompiledPlugin> reverseReferences =
                _compiledPlugins.All.Where(x => x.PluginReferences.Any(y => y.PluginName == compiledPlugin.PluginName && y.Required));

            // recompile plugins that reference the supplied plugin
            foreach (CompiledPlugin reverseReference in reverseReferences)
            {
                if (workQueueAll.All(x => x.PluginName != reverseReference.PluginName))
                {
                    Compile(reverseReference.PluginName);
                    restart = true;
                }
            }

            return restart;
        }

        /// <summary>
        /// Preprocess scripts
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void ProcessScriptsBeforeCompile(CompilePluginRequest[] workQueueAll)
        {
            bool restart = false;

            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.CurrentStep == PluginCompileStage.Nothing)
                {
                    plugin.CurrentStep = PluginCompileStage.AwaitingUnload;
                }

                if (plugin.CurrentStep == PluginCompileStage.AwaitingUnload)
                {
                    CompiledPlugin compiledPlugin = _compiledPlugins.Find(plugin.PluginName);
                    if ((compiledPlugin?.IsLoaded ?? false) && RecompileTree(compiledPlugin, workQueueAll))
                    {
                        restart = true;
                    }
                }
            }

            if (restart)
            {
                RecompileAll(PluginCompileStage.AwaitingUnload);
                return;
            }

            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.CurrentStep == PluginCompileStage.AwaitingUnload)
                {
                    plugin.IsScriptLoaded = false;
                    bool any = false;
                    string message = string.Empty;
                    CompiledPlugin compiledPlugin = _compiledPlugins.Find(plugin.PluginName);
                    if ((compiledPlugin?.IsLoaded ?? false) && plugin.CanBeUnloaded)
                    {
                        Loader.PluginUnloaded(compiledPlugin.LastGoodPlugin);
                        any = true;
                        message += $" {compiledPlugin.PluginName}";
                    }

                    foreach (PluginReference refPlugin in plugin.ReferencedPlugins)
                    {
                        if (_workQueue.Contains(refPlugin.PluginName))
                        {
                            CompiledPlugin refPluginImpl = _compiledPlugins.Find(plugin.PluginName);
                            //IPlugin refPluginImpl = Utility.Plugins.Find(refPlugin.PluginName);
                            if (refPluginImpl != null && refPluginImpl.IsLoaded)
                            {
                                message += $" {refPluginImpl.PluginName}";
                                any = true;
                            }
                        }
                    }

                    if (!any)
                    {
                        plugin.NextStep();
                        plugin.UnloadAttempts = 0;
                    }
                    else
                    {
                        plugin.UnloadAttempts++;

                        if (plugin.UnloadAttempts > 5)
                        {
                            plugin.EncounteredError($"Could not load plugin {plugin.PluginName}.  Not unloaded {message}."); // TODO: Localization
                        }
                    }
                }

                if (plugin.IsScriptLoaded == false)
                {
                    lock (plugin)
                    {
                        plugin.LoadScript();
                    }
                }

                if (plugin.IsScriptLoaded && plugin.CurrentStep == PluginCompileStage.PreParseScript)
                {
                    plugin.TransformScript(_gameExtensionName, _gameExtensionBranch);

                    plugin.ParseScript();

                    plugin.NextStep();
                }
            }
        }

        /// <summary>
        /// Validate compilation stack dependencies
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void ValidateDependencies(CompilePluginRequest[] workQueueAll)
        {
            // We make sure all dependancies are queued up or already loaded on the server
            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.CurrentStep == PluginCompileStage.AddDefaultReferences)
                {
                    AddDefaultReferences(plugin);

                    // Don't think it can fail adding the built in references
                    plugin.NextStep();
                }

                if (plugin.CurrentStep == PluginCompileStage.ValidateAssemblyReferences)
                {
                    if (ValidateAssemblyReferences(plugin))
                    {
                        plugin.NextStep();
                    }
                }

                if (plugin.CurrentStep == PluginCompileStage.ValidatePluginReferences)
                {
                    if (ValidatePluginReferences(plugin))
                    {
                        plugin.NextStep();
                    }
                }
            }
        }

        /// <summary>
        /// Sort compilation order
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void SortCompileOrder(CompilePluginRequest[] workQueueAll)
        {
            // If a plugin becomes ready to compile, we need to check again if any other plugins are now ready, and so on
            bool recheckLoop = true;

            int loops = 0;

            while (recheckLoop)
            {
                recheckLoop = false;

                foreach (CompilePluginRequest plugin in workQueueAll)
                {
                    if (plugin.CurrentStep == PluginCompileStage.WaitingOnDependencies)
                    {
                        if (plugin.ReferencedPlugins.Any(x => x.IsReady(_workQueue, _compiledPlugins) == false))
                        {
                            continue;
                        }

                        foreach (PluginReference pluginReference in plugin.ReferencedPlugins)
                        {
                            CompilePluginRequest pendingWorkItem = _workQueue.Find(pluginReference.PluginName);
                            if (pendingWorkItem == null ||
                                pendingWorkItem.CurrentStep != PluginCompileStage.WaitingOnDependencies)
                            {
                                continue;
                            }

                            pendingWorkItem.NextStep();
                            pendingWorkItem.CompileOrder = _compileOrderIndex++;
                        }
                        //Need to apply sorting and such, or do it when creating request

                        //We just push plugins forward that need to be compiled, which means we don't need to store any list of valid plugins
#if DEBUG
                        _logger.Debug($"Pushing request: {plugin.Name}");
#endif

                        plugin.NextStep();
                        plugin.CompileOrder = _compileOrderIndex++;

                        recheckLoop = true;
                    }
                }

                if (loops++ > 300)
                {
#if DEBUG
                    _logger.Debug("Prevented infinite loop inside SortCompileOrder()!"); // TODO: Localization
#endif
                    break;
                }
            }

            _compileOrderIndex = 1;
        }

        /// <summary>
        /// Validate request plugin references
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        private bool ValidatePluginReferences(CompilePluginRequest plugin)
        {
            //Here we validate plugins referencing other plugins

            bool recompile = false;

            if (plugin.ReferencedPlugins.Count == 0)
            {
                return true;
            }

            foreach (PluginReference reference in plugin.ReferencedPlugins)
            {
                if (reference.Valid)
                {
                    continue;
                }

                CompiledPlugin loadedPlugin = _compiledPlugins.Find(reference.PluginName);

                // Plugin is already compiled and loaded, therefore is valid
                if (loadedPlugin != null && loadedPlugin.IsLoaded)
                {
                    reference.Valid = true;
                    continue;
                }

                CompilePluginRequest loadingPlugin = _workQueue.Find(reference.PluginName);

                // Plugin has been queued to be compiled, so it also is valid
                if (loadingPlugin != null)
                {
                    reference.Valid = true;
                    continue;
                }

                // Now we need to look for it on disk
                FileInfo dependencyFileInfo =
                    Loader.GetLoadableFile(Interface.uMod.PluginDirectory, reference.PluginName);

                if (dependencyFileInfo != null)
                {
                    continue;
                }

                if (File.Exists(Path.Combine(Interface.uMod.PluginDirectory, $"{reference.PluginName}.cs")))
                {
                    CompilePluginRequest cpr = new CompilePluginRequest(_logger, Interface.uMod.PluginDirectory, reference.PluginName, reference.PluginName);
                    _workQueue.Add(cpr);

                    recompile = true;

                    continue;
                }

                TypePromiseManager.AddPromise($"uMod.Plugins.{reference.PluginName}", plugin.Name + Loader.FileExtension);
                plugin.EncounteredError(Interface.uMod.Strings.Compiler.RequirementsMissingLog
                    .Interpolate(
                        ("plugin", plugin.Name),
                        ("requirements", reference.PluginName),
                        ("dependency", "plugin")
                    ));

                return false;
            }

            if (recompile)
            {
                RecompileAll();
                return false;
            }

            return plugin.ReferencedPlugins.All(x => x.Valid);
        }

        /// <summary>
        /// Validate request assembly references
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        private bool ValidateAssemblyReferences(CompilePluginRequest plugin)
        {
            // We also look for cached dll references in here

            #region Turn DLLs referenced by magic strings to DLLReference

            foreach (string referenceName in plugin.ReferencedAssemblyNames)
            {
                //We keep any previously loaded DLL references cached: they cant be hot reloaded anyways
                AssemblyReference reference = _assemblyCache.Find(referenceName) ?? new AssemblyReference(Path.Combine(Interface.uMod.ExtensionDirectory, referenceName));

                if (plugin.ReferencedAssemblies.ContainsKey(reference.Name))
                {
                    plugin.ReferencedAssemblies[reference.Name] = reference;
                }
                else
                {
                    plugin.ReferencedAssemblies.Add(reference.Name, reference);
                }
            }

            #endregion Turn DLLs referenced by magic strings to DLLReference

            #region Validate all DLLReference

            foreach (KeyValuePair<string, AssemblyReference> reference in plugin.ReferencedAssemblies)
            {
                if (reference.Value.IsValid)
                {
                    continue;
                }

                if (!reference.Value.Validate(plugin.Name))
                {
                    plugin.EncounteredError($"Failed to validate reference '{reference.Value.FileName}': " + reference.Value.Error); // TODO: Localization
                    return false;
                }

                if (reference.Value.IsCached == false)
                {
                    _assemblyCache.Add(reference.Value);
                }
            }

            #endregion Validate all DLLReference

            return true;
        }

        /// <summary>
        /// Add default request references
        /// </summary>
        /// <param name="plugin"></param>
        private void AddDefaultReferences(CompilePluginRequest plugin)
        {
            if (Interface.uMod.IsTesting)
            {
                #region Add references when running tests

                if (TestReferences == null)
                {
                    TestReferences = new List<AssemblyReference>();
                }

                if (TestReferences.Count == 0)
                {
                    string runtimePath = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();
                    TestReferences.Add(new AssemblyReference(typeof(object).Assembly.Location));
                    TestReferences.Add(new AssemblyReference(Path.Combine(runtimePath, "netstandard.dll")));
                    TestReferences.Add(new AssemblyReference(Path.Combine(runtimePath, "System.Collections.dll")));
                    TestReferences.Add(new AssemblyReference(Path.Combine(runtimePath, "System.Linq.dll")));
                    TestReferences.Add(new AssemblyReference(Path.Combine(runtimePath, "System.Runtime.dll")));
                    TestReferences.Add(new AssemblyReference(Path.Combine(runtimePath, "System.Data.Common.dll")));
                    TestReferences.Add(new AssemblyReference(Path.Combine(Interface.uMod.ExtensionDirectory, "uMod.Tests.dll")));
                    TestReferences.Add(new AssemblyReference(Path.Combine(Interface.uMod.ExtensionDirectory, "uMod.Tommy.dll")));

                    //uMod.Promise
                    TestReferences.Add(new AssemblyReference(typeof(Promise).Assembly.Location));

                    //uMod.Common
                    TestReferences.Add(new AssemblyReference(typeof(IContext).Assembly.Location));

                    //uMod.Core
                    TestReferences.Add(new AssemblyReference(typeof(Plugin).Assembly.Location));
                }

                foreach (AssemblyReference testReference in TestReferences)
                {
                    if (!plugin.ReferencedAssemblies.ContainsKey(testReference.Name))
                    {
                        plugin.ReferencedAssemblies.Add(testReference.Name, testReference);
                    }
                }

                #endregion Add references when running tests
            }
            else
            {
                AssemblyReference coreLib = new AssemblyReference(typeof(object).Assembly.Location);
                // Add mscorlib as a reference
                if (!plugin.ReferencedAssemblies.ContainsKey(coreLib.Name))
                {
                    plugin.ReferencedAssemblies.Add(coreLib.Name, coreLib);
                }
            }

            foreach (string filename in CompilablePluginLoader.PluginReferences)
            {
                string dllFileName = $"{filename}.dll";
                string dllFullPath = Path.Combine(Interface.uMod.ExtensionDirectory, dllFileName);
                string exeFileName = $"{filename}.exe";
                string exeFullPath = Path.Combine(Interface.uMod.ExtensionDirectory, exeFileName);
                if (!plugin.ReferencedAssemblies.ContainsKey(dllFileName) && File.Exists(dllFullPath))
                {
                    plugin.ReferencedAssemblies.Add(dllFileName, new AssemblyReference(dllFullPath));
                }
                if (!plugin.ReferencedAssemblies.ContainsKey(exeFileName) && File.Exists(exeFullPath))
                {
                    plugin.ReferencedAssemblies.Add(exeFileName, new AssemblyReference(exeFullPath));
                }
            }
        }

        /// <summary>
        /// Purge timed out plugins
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void TimeOutPurge(CompilePluginRequest[] workQueueAll)
        {
            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.TimeoutTime == DateTime.MinValue)
                {
                    continue;
                }

                if (DateTime.UtcNow > plugin.TimeoutTime)
                {
                    plugin.OnCompilationTimeout();
                }
            }
        }

        /// <summary>
        /// Send compilation requests to the compiler
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void SendCompileRequests(CompilePluginRequest[] workQueueAll)
        {
            //First we get a list of every plugin that has a dependency that is already loaded

            CompilePluginRequest[] validRequests = workQueueAll.Where(x => x.CurrentStep == PluginCompileStage.ReadyToCompile).ToArray();

            if (validRequests.Length == 0)
            {
                return;
            }

            if (validRequests.Length > 1)
            {
                validRequests = validRequests.OrderBy(x => x.CompileOrder).ToArray();
            }

#if DEBUG
            _logger.Debug($"Compiling {string.Join(",", validRequests.Select(x => x.Name).ToArray())}");
#endif
            CompilerRequest request = new CompilerRequest(this, _logger, validRequests);

            foreach (CompilePluginRequest plugin in validRequests)
            {
                plugin.CurrentStep = PluginCompileStage.CompilerProcessing;
                plugin.OnCompilationStarted();
            }

            _compiler.Compile(request);
        }

        private void RecieveCompileRequests()
        {
            while (_compiler.TryDequeueRequest(out CompilerRequest request))
            {
                if (request.Recompiling)
                {
                    continue;
                }

                if (request.ErrorMessages.Count > 0 || request.Plugins.Any(x => x.CurrentStep != PluginCompileStage.CompilerProcessing))
                {
                    ProcessFailedRequest(request);
                }
                
                ProcessSuccessfulRequest(request);
            }
        }

        private void ProcessFailedRequest(CompilerRequest request)
        {
            List<CompilePluginRequest> recompilePlugins = new List<CompilePluginRequest>();
            foreach (CompilePluginRequest plugin in request.Plugins)
            {
                if (request.ErrorMessages.ContainsKey(plugin.Name))
                {
                    plugin.CurrentStep = PluginCompileStage.CompileFailed;
                    plugin.OnCompilationFailed();

                    foreach (PluginReference pluginRef in plugin.ReferencedPlugins)
                    {
                        // If this reference is in the compilation queue because of this plugin...
                        // It was unloaded, so we should recompile it without this plugin to reload it
                        CompiledPlugin compiledPlugin = _compiledPlugins.Find(pluginRef.PluginName);
                        CompilePluginRequest compileRequest =
                            request.Plugins.FirstOrDefault(x => x.PluginName == pluginRef.PluginName);
                        if (compileRequest != null &&
                            compiledPlugin?.LastGoodPlugin != null)
                        {
                            recompilePlugins.Add(compileRequest);
                        }
                    }
                }
            }

            if (recompilePlugins.Count > 0)
            {
#if DEBUG
                _logger.Debug("Recompiling because of failed request");
#endif
                RecompileAll(PluginCompileStage.AwaitingUnload, recompilePlugins.ToArray());
            }
        }

        private void ProcessSuccessfulRequest(CompilerRequest request)
        {
            if (request.DLLBytes == null)
            {
                string[] names = request.Plugins.OrderBy(x => x.CompileOrder).Select(x => x.Name).ToArray();
                _logger.Error($"Request ({string.Join(",", names)} plugins) was successful but dll bytes missing!"); // TODO: Localization
                ProcessFailedRequest(request);
                return;
            }

            request.LoadedAssembly = Assembly.Load(request.DLLBytes);

            // After loading assembly, determine if types sought by promises have been fulfilled
            // If so, start over
            if (OnAssemblyResolved(request))
            {
                return;
            }

            //AssemblyRegister.Loaded(request.LoadedAssembly, request.DLLBytes);

            foreach (CompilePluginRequest plugin in request.Plugins)
            {
                if (plugin.CurrentStep == PluginCompileStage.CompilerProcessing)
                {
                    plugin.OnCompileRequestSuccess(request);
                }
            }
        }

        private void ProcessRecompileRequest(CompilerRequest request)
        {
            foreach (CompilePluginRequest plugin in request.Plugins)
            {
                plugin.OnCompileRequestRecompile(request);
            }
        }

        private bool OnAssemblyResolved(CompilerRequest request)
        {
            if (request.ResolvePromises(out List<string> promiseRemoval))
            {
                ProcessRecompileRequest(request);
                foreach (string promiseName in promiseRemoval)
                {
                    request.Plugins.Add(RecentRequests[promiseName]);
                }

#if DEBUG
                _logger.Debug("Recompiling due to explicit promise found");
#endif
                Recompile(request.Plugins.ToArray());

                request.OnRecompile();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Create instance of a plugin class
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void CreatePluginInstances(CompilePluginRequest[] workQueueAll)
        {
            List<CompilePluginRequest> delayedRequests = new List<CompilePluginRequest>();
            bool anyReferenceFails = false;
            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                //First we load the compiled bytes into memory as an assembly
                if (plugin.CurrentStep == PluginCompileStage.FindPluginType)
                {
                    // This isn't even named right, this finds the plugin type
                    if (plugin.FindPluginType())
                    {
                        plugin.NextStep();
                    }
                }

                // Check if requires attributes are available
                if (plugin.CurrentStep == PluginCompileStage.ValidateRequires)
                {
                    if (plugin.ValidateRequires(_workQueue.Requests.Keys))
                    {
                        plugin.ReferenceRetries = 0;
                        plugin.NextStep();
                    }
                }

                //Then we create the plugin
                if (plugin.CurrentStep == PluginCompileStage.CreatePluginInstance)
                {
                    if (plugin.IsLoaded)
                    {
                        continue;
                    }

                    foreach (PluginReference pluginReference in plugin.ReferencedPlugins)
                    {
                        // Fail-safe to prevent type mismatch
                        CompiledPlugin loadedPlugin = _compiledPlugins.Find(pluginReference.PluginName);
                        if (_workQueue.Contains(pluginReference.PluginName) && (loadedPlugin?.IsLoaded ?? false))
                        {
#if DEBUG
                            _logger.Debug("Recompiling to prevent type mismatch");
#endif
                            RecompileAll(PluginCompileStage.AwaitingUnload);
                            return;
                        }
                    }

                    // Assuming any plugin types may be resolved by queued assemblies, delay Assembly.Load until base assemblies are loaded
                    if (plugin.ReferencedPlugins.Any(ValidateReferenceInstances))
                    {
                        anyReferenceFails = true;
#if DEBUG
                        _logger.Debug($"Delaying plugin creation {plugin.PluginName}");
#endif
                        delayedRequests.Add(plugin);
                    }
                    else
                    {
                        plugin.CreatePlugin(Loader);
                        plugin.NextStep();
                    }
                }
            }

            int retries = 0;

            if (!anyReferenceFails)
            {
                // Load referenced assemblies
                List<CompilePluginRequest> newDelayedPlugins = new List<CompilePluginRequest>();
                while (delayedRequests.Count > 0)
                {
                    foreach (CompilePluginRequest plugin in delayedRequests)
                    {
                        if (plugin.CurrentStep == PluginCompileStage.CreatePluginInstance)
                        {
                            if (plugin.ReferencedPlugins.Any(ValidateReferenceInstances))
                            {
#if DEBUG
                                _logger.Debug($"Delaying plugin creation1 {plugin.PluginName}");
#endif
                                newDelayedPlugins.Add(plugin);
                                retries++;
                            }
                            else
                            {
                                plugin.CreatePlugin(Loader);
                                plugin.NextStep();
                            }
                        }
                    }

                    if (retries > 6)
                    {
                        foreach (CompilePluginRequest plugin in delayedRequests)
                        {
                            plugin.EncounteredError("Expected type not resolved.  Does dependency depth exceed 6?");
                        }

                        break;
                    }

                    delayedRequests = newDelayedPlugins;
                    Thread.Sleep(10);
                }
            }
        }

        /// <summary>
        /// Ensure instance references are properly resolved
        /// </summary>
        /// <param name="pluginReference"></param>
        /// <returns></returns>
        private bool ValidateReferenceInstances(PluginReference pluginReference)
        {
            if (pluginReference.Required)
            {
                CompilePluginRequest cpr = _workQueue.Find(pluginReference.PluginName);

                if (cpr == null)
                {
                    return false;
                }

                if (cpr.CurrentStep < PluginCompileStage.CreatePluginInstance)
                {
                    return true;
                }

                if (cpr.CurrentStep == PluginCompileStage.CreatePluginInstance)
                {
                    return false;
                }

                return !_compiledPlugins.Exists(cpr.PluginName);
            }

            return false;
        }

        /// <summary>
        /// Remove queued compilations from queue if they have failed or finished
        /// </summary>
        /// <param name="workQueueAll"></param>
        private void DoRemoves(CompilePluginRequest[] workQueueAll)
        {
            bool didRemove = false;
            //Ran on second thread so safe to loop through, only have to lock when accessing a main thread loop
            if (workQueueAll.Length == 0)
            {
                return;
            }

            foreach (CompilePluginRequest plugin in workQueueAll)
            {
                if (plugin.HasFailed)
                {
                    CancelPendingDependencies(plugin);
                }

                if (plugin.IsFinished)
                {
                    didRemove = true;
                    lock (_workQueue)
                    {
                        //Done working on it
                        _workQueue.Remove(plugin.PluginName);
                    }

                    lock (_completedQueue)
                    {
                        _completedQueue.Add(plugin);
                    }
                }
            }

            if (!didRemove)
            {
                return;
            }

            //Prevents us from running a linq query every time
            lock (_workQueue)
            {
                foreach (string name in _workQueue.Requests.Where(x => x.Value.IsFinished).Select(x => x.Key))
                {
                    _workQueue.Remove(name);
                }
            }
        }

        private void CancelPendingDependencies(CompilePluginRequest plugin)
        {
            HashSet<string> cancelled = new HashSet<string> { plugin.Name };
            Queue<CompilePluginRequest> cancelQueue = new Queue<CompilePluginRequest>();

            cancelQueue.Enqueue(plugin);

            while (cancelQueue.Count > 0)
            {
                CompilePluginRequest cancelling = cancelQueue.Dequeue();

                if (cancelling == null)
                {
                    break;
                }

                // Here is where we actually cancel it
                lock (_workQueue)
                {
                    _workQueue.Remove(cancelling.PluginName);
                }

                foreach (PluginReference dependancy in cancelling.ReferencedPlugins)
                {
                    // Only cancel pending load orders for plugins that are loaded
                    if (dependancy.Optional || TypePromiseManager.TypePromises.ContainsKey(dependancy.PluginName))
                    {
                        continue;
                    }

                    CompilePluginRequest dependancyPlugin = _workQueue.Find(dependancy.PluginName);

                    if (cancelled.Add(dependancy.PluginName))
                    {
                        cancelling.EncounteredError($"Canceling pending plugin '{dependancy.PluginName}': dependency '{cancelling.PluginName}' failed to load!"); // TODO: Localization
                        cancelQueue.Enqueue(dependancyPlugin);
                    }
                }
            }
        }

        #endregion Compile Thread

        private string FormatPluginName(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName)?.Replace("_", "");
        }
    }
}
