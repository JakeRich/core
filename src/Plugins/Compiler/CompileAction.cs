﻿namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents an action to be later fulfilled by the compiler
    /// </summary>
    public class CompileAction
    {
        /// <summary>
        /// The action to be performed
        /// </summary>
        public CompileActionType Action = CompileActionType.Load;

        /// <summary>
        /// The file associated with the action
        /// </summary>
        public string FileName;

        /// <summary>
        /// The directory of the action
        /// </summary>
        public string Directory;

        /// <summary>
        /// The plugin name (used to key actions)
        /// </summary>
        public string PluginName;
    }
}
