using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using uMod.Common;
using uMod.Pooling;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents the stages of compilation
    /// </summary>
    internal enum PluginCompileStage
    {
        //The order of these enums are important
        Nothing = 0,
        AwaitingUnload,
        PreParseScript,
        AddDefaultReferences,
        ValidateAssemblyReferences,
        ValidatePluginReferences,
        WaitingOnDependencies,
        ReadyToCompile,
        CompilerProcessing,
        CompileSucceeded,
        FindPluginType,
        ValidateRequires,
        CreatePluginInstance,

        Finished, //Keep everything here at end
        CompileFailed,
        Error,
    }

    /// <summary>
    /// Represents a compilation request
    /// </summary>
    internal class CompilePluginRequest : IContext
    {
        /// <summary>
        /// Gets the name of the context
        /// </summary>
        public string Name => PluginName;

        /// <summary>
        /// Gets the name of the plugin
        /// </summary>
        internal string PluginName { get; }

        /// <summary>
        /// Gets the directory location of the plugin
        /// </summary>
        internal string Directory { get; }

        /// <summary>
        /// Gets the filename of the plugin
        /// </summary>
        internal string FileName { get; }

        /// <summary>
        /// Gets the full path of the plugin file
        /// </summary>
        internal string FullPath { get; }

        /// <summary>
        /// Gets the script contents separated by newlines/carriage returns
        /// </summary>
        internal List<string> ScriptLines { get; private set; }

        /// <summary>
        /// Gets or sets the intended compilation order
        /// </summary>
        internal int CompileOrder { get; set; }

        /// <summary>
        /// Gets the number of times this plugin attempted to resolve references
        /// </summary>
        internal int ReferenceRetries = 0;

        /// <summary>
        /// Gets the number o fitmes this plugin attempted to load
        /// </summary>
        internal int LoadRetries = 0;

        /// <summary>
        /// Gets an array of bytes from the script contents
        /// </summary>
        internal byte[] ScriptBytes
        {
            get
            {
                if (_scriptBytes == null)
                {
                    if (ScriptLines == null)
                    {
                        return null;
                    }

                    _scriptBytes = System.Text.Encoding.UTF8.GetBytes(string.Join(Environment.NewLine, ScriptLines.ToArray()));
                }

                return _scriptBytes;
            }
        }

        /// <summary>
        /// Caches script bytes locally in memory
        /// </summary>
        private byte[] _scriptBytes;

        /// <summary>
        /// Gets a list of referenced assembly names
        /// </summary>
        internal HashSet<string> ReferencedAssemblyNames { get; } = new HashSet<string>();

        /// <summary>
        /// Gets a dictionary of assembly references
        /// </summary>
        internal Dictionary<string, AssemblyReference> ReferencedAssemblies { get; } = new Dictionary<string, AssemblyReference>();

        /// <summary>
        /// Gets a list of plugin references
        /// </summary>
        internal List<PluginReference> ReferencedPlugins { get; } = new List<PluginReference>();

        /// <summary>
        /// Gets the list of the errors associated with the last compilation
        /// </summary>
        internal List<string> Errors { get; } = new List<string>();

        /// <summary>
        /// Gets the list of the warnings associated with the last compilation
        /// </summary>
        internal List<string> Warnings { get; } = new List<string>();

        /// <summary>
        /// The last time the compilation was modified
        /// </summary>
        internal DateTime LastModifiedAt;

        /// <summary>
        /// The last time the script was cached
        /// </summary>
        internal DateTime LastCachedScriptAt;

        /// <summary>
        /// The last time the script was compiled
        /// </summary>
        internal DateTime LastCompiledAt;

        /// <summary>
        /// The time before a timeout will occur
        /// </summary>
        internal DateTime TimeoutTime;

        /// <summary>
        /// The compiled assembly from the response
        /// </summary>
        public Assembly CompiledAssembly;

        /// <summary>
        /// The type extracted from the response assembly
        /// </summary>
        public Type PluginType;

        /// <summary>
        /// The plugin instance created by the last response
        /// </summary>
        public IPlugin Plugin;

        /// <summary>
        /// The number of times the compiler has attempted to unload the plugin associated with this request
        /// </summary>
        public int UnloadAttempts;

        /// <summary>
        /// The amount of lines added to a script before compilation. Lets us offset line numbers
        /// </summary>
        public int LinesAddedToScript;

        /// <summary>
        /// The current stage of compilation
        /// </summary>
        private PluginCompileStage _stage = PluginCompileStage.Nothing;

        /// <summary>
        /// Gets the current stage of compilation
        /// </summary>
        public PluginCompileStage CurrentStep
        {
            get => _stage;
            internal set
            {
                _loadRetries = 0;
                _fileRetries = 0;
                //Interface.uMod.LogDebug($"{_stage} to {value}");
                _stage = value;
            }
        }

        /// <summary>
        /// Proceed to the next stage in the compilation workflow
        /// </summary>
        public void NextStep()
        {
            if (CurrentStep < PluginCompileStage.Finished)
            {
                CurrentStep++;
            }
        }

        /// <summary>
        /// Determines if compilation request is finished
        /// </summary>
        public bool IsFinished => HasFailed || CurrentStep >= PluginCompileStage.Finished;

        /// <summary>
        /// Determines if script is loaded
        /// </summary>
        public bool IsScriptLoaded
        {
            get
            {
                return ScriptLines != null;
            }
            set
            {
                if (value == false)
                {
                    ScriptLines = null;
                }
            }
        }

        /// <summary>
        /// Determines if compilation has failed
        /// </summary>
        internal bool HasFailed { get { return Errors.Count > 0 || CurrentStep == PluginCompileStage.CompileFailed; } }

        /// <summary>
        /// Determines if the compilation can be unloaded
        /// </summary>
        public bool CanBeUnloaded { get { return CurrentStep != PluginCompileStage.CompilerProcessing; } }

        /// <summary>
        /// Determines if the plugin is loaded into the server
        /// </summary>
        public bool IsLoaded { get { return Plugin != null && Plugin.IsLoaded; } }

        /// <summary>
        /// The logger used by this request
        /// </summary>
        protected ILogger Logger;

        private int _loadRetries;
        private int _fileRetries;

        /// <summary>
        /// Create a compilable file object belonging the specified plugin loader within the specified directory and using the specified name
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <param name="pluginName"></param>
        internal CompilePluginRequest(ILogger logger, string directory, string fileName, string pluginName)
        {
            Logger = logger;
            Directory = directory;
            FileName = Path.GetFileNameWithoutExtension(fileName);
            FullPath = Path.Combine(Directory, $"{fileName}.cs");
            PluginName = pluginName;

            CheckLastModificationTime();
        }

        /// <summary>
        /// Loads and caches in memory the script file contents
        /// </summary>
        /// <returns></returns>
        internal bool LoadScript()
        {
            string[] lines;
            try
            {
                lines = File.ReadAllLines(FullPath);

                LastCachedScriptAt = DateTime.Now;

                _loadRetries = 0;
                _fileRetries = 0;
            }
            catch (FileNotFoundException fnfEx) // Don't print entire filesystem path
            {
                if (_fileRetries < 10)
                {
                    //Interface.uMod.LogDebug($"Retrying file check: {PluginName}");
                    _fileRetries++;
                    return false;
                }
                EncounteredError($"Failed to read script '{FileName}'. File not found."); // TODO: Localization
                return false;
            }
            catch (IOException ex)
            {
                if (_loadRetries < 3)
                {
                    //Interface.uMod.LogDebug($"Retrying compilation: {PluginName}");
                    _loadRetries++;
                    return false;
                }

                EncounteredError($"Failed to read script '{FileName}':\n" + ex.ToString()); // TODO: Localization
                return false;
            }
            catch (Exception ex)
            {
                EncounteredError($"Failed to read script '{FileName}':\n" + ex.ToString()); // TODO: Localization
                return false;
            }

            LastCachedScriptAt = DateTime.Now;

            ScriptLines = new List<string>(lines);
            return true;
        }

        /// <summary>
        /// Notified when a compilation has started
        /// </summary>
        internal void OnCompilationStarted()
        {
            UnloadAttempts = 0;
            LastCompiledAt = LastModifiedAt;
            TimeoutTime = DateTime.UtcNow.AddSeconds(60);
        }

        /// <summary>
        /// Notified when this compilation has timed out
        /// </summary>
        internal void OnCompilationTimeout()
        {
            EncounteredError("Timed out waiting for plugin to be compiled: " + Name); // TODO: Localization
            UnloadAttempts = 0;
        }

        /// <summary>
        /// Notified when this compilation has failed
        /// </summary>
        internal void OnCompilationFailed()
        {
            LastCompiledAt = default;
            UnloadAttempts = 0;
        }

        /// <summary>
        /// Process recompilation request
        /// </summary>
        /// <param name="request"></param>
        internal void OnCompileRequestRecompile(CompilerRequest request)
        {
            UnloadAttempts = 0;
            CurrentStep = PluginCompileStage.ValidatePluginReferences;

            NextStep();
        }

        /// <summary>
        /// Process request success
        /// </summary>
        /// <param name="request"></param>
        internal void OnCompileRequestSuccess(CompilerRequest request)
        {
            if (CurrentStep != PluginCompileStage.CompilerProcessing)
            {
                return;
            }

            UnloadAttempts = 0;
            request.RecompileAttempts = 0;
            CompiledAssembly = request.LoadedAssembly;

            CurrentStep = PluginCompileStage.CompileSucceeded;

            NextStep();
        }

        /// <summary>
        /// Stores compilation errors and proceeds to error stage
        /// </summary>
        /// <param name="error"></param>
        internal void EncounteredError(string error)
        {
            Errors.Add(error);

            CurrentStep = PluginCompileStage.Error;
        }

        /// <summary>
        /// Stores compilation warnings
        /// </summary>
        /// <param name="warning"></param>
        internal void EncounteredWarning(string warning)
        {
            Warnings.Add(warning);
        }

        /// <summary>
        /// Determines if file has been modified
        /// </summary>
        /// <returns></returns>
        internal bool HasBeenModified()
        {
            DateTime lastModifiedAt = LastModifiedAt;
            CheckLastModificationTime();
            return LastModifiedAt != lastModifiedAt;
        }

        /// <summary>
        /// Check the last modification time of the plugin file
        /// </summary>
        private void CheckLastModificationTime()
        {
            if (!File.Exists(Directory))
            {
                LastModifiedAt = default;
                return;
            }

            DateTime modifiedTime = GetLastModificationTime();
            if (modifiedTime != default)
            {
                LastModifiedAt = modifiedTime;
            }
        }

        /// <summary>
        /// Gets the last modification time of the plugin file
        /// </summary>
        /// <returns></returns>
        internal DateTime GetLastModificationTime()
        {
            try
            {
                return File.GetLastWriteTime(FullPath);
            }
            catch (IOException ex)
            {
                Interface.uMod.LogError("IOException while checking plugin: {0} ({1})", FileName, ex.Message);
                return default;
            }
        }

        /// <summary>
        /// Validate plugin requirements
        /// </summary>
        /// <param name="plugins"></param>
        /// <returns></returns>
        internal bool ValidateRequires(IEnumerable<string> plugins)
        {
            foreach (FieldInfo field in PluginType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.DeclaringType != PluginType ||
                    !(field.GetCustomAttribute<PluginReferenceAttribute>() is PluginReferenceAttribute pluginReference))
                {
                    continue;
                }

                if (pluginReference is RequiresAttribute)
                {
                    bool anyFound = false;
                    
                    if (pluginReference.Names != null)
                    {
                        foreach (string refName in pluginReference.Names)
                        {
                            if (plugins.Any(x => x == refName) || Interface.uMod.Application.Resolved(refName))
                            {
                                anyFound = true;
                                TryAddPluginReference(refName,
                                    field.FieldType.IsAbstract
                                        ? PluginReferenceType.ImplicitRequires
                                        : PluginReferenceType.ExplicitRequires);
                            }
                        }
                    }
                    else
                    {
                        if (plugins.Any(x => x == field.Name) || Interface.uMod.Application.Resolved(field.Name))
                        {
                            anyFound = true;
                            TryAddPluginReference(field.Name,
                                field.FieldType.IsAbstract
                                    ? PluginReferenceType.ImplicitRequires
                                    : PluginReferenceType.ExplicitRequires);
                        }
                    }

                    if (!anyFound)
                    {
                        List<string> messagesList = Pools.GetList<string>();
                        try
                        {
                            if (pluginReference.Names != null)
                            {
                                messagesList.Add(string.Join(" or ", pluginReference.Names));
                            }
                            else
                            {
                                messagesList.Add(field.Name);
                            }

                            string dependencyNoun =
                                Interface.uMod.Strings.Compiler.Dependency.Choice(messagesList.Count);
                            string message = Interface.uMod.Strings.Compiler.RequirementsMissing.Interpolate(
                                ("dependency", dependencyNoun),
                                ("requirements", string.Join(", ", messagesList.ToArray()))
                            );
                            EncounteredError(Interface.uMod.Strings.Plugin.InitializeFailure.Interpolate(("name", Name),
                                                 ("version", Plugin?.Version.ToString() ?? "unknown")) +
                                             $": {message}");
                        }
                        finally
                        {
                            Pools.FreeList(ref messagesList);
                        }
                        
                        return false;
                    }
                }
                else if
                    (pluginReference is OptionalAttribute)
                {
                    if (pluginReference.Names != null)
                    {
                        foreach (string refName in pluginReference.Names)
                        {
                            TryAddPluginReference(refName,
                                field.FieldType.IsAbstract
                                    ? PluginReferenceType.ImplicitOptional
                                    : PluginReferenceType.ExplicitOptional);
                        }
                    }
                    else
                    {
                        TryAddPluginReference(field.Name,
                            field.FieldType.IsAbstract
                                ? PluginReferenceType.ImplicitOptional
                                : PluginReferenceType.ExplicitOptional);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Finds plugin type inside response assembly
        /// </summary>
        /// <returns></returns>
        internal bool FindPluginType()
        {
            PluginType = CompiledAssembly.GetType($"uMod.Plugins.{Name}") ??
                        CompiledAssembly.GetType($"uMod.Plugins.{Name}.{Name}"); //Putting plugin class inside namespace named of plugin
            if (PluginType == null)
            {
                EncounteredError(Interface.uMod.Strings.Compiler.MissingPluginClass.Interpolate("plugin", Name));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates plugin using the specified loader
        /// </summary>
        /// <param name="loader"></param>
        internal void CreatePlugin(IPluginLoader loader)
        {
            try
            {
#if DEBUG
                Interface.uMod.LogDebug($"Creating plugin {PluginType.Name}");
#endif
                Plugin = Activator.CreateInstance(PluginType) as IPlugin;
            }
            catch (MissingMethodException)
            {
                EncounteredError(Interface.uMod.Strings.Compiler.InvalidPluginConstructor.Interpolate("plugin", Name));
                return;
            }
            catch (TargetInvocationException invocationException)
            {
                Exception ex = invocationException.InnerException;
                EncounteredError(Interface.uMod.Strings.Compiler.CompiledAssemblyFailure.Interpolate(("script", FileName), ("errors", ex)));
                return;
            }
            catch (Exception ex)
            {
                EncounteredError(Interface.uMod.Strings.Compiler.CompiledAssemblyFailure.Interpolate(("script", FileName), ("errors", ex)));
                return;
            }

            if (Plugin == null)
            {
                EncounteredError(Interface.uMod.Strings.Compiler.AssemblyLoadFailure.Interpolate("script", FileName));
                return;
            }

            if (!Plugin.Resolve(loader, FileName, FullPath))
            {
                EncounteredError(Interface.uMod.Strings.Compiler.AnnotationMissing.Interpolate("script", FileName));
            }
        }

        /// <summary>
        /// Converts request into a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Adds required code and precompiler directeves
        /// </summary>
        /// <param name="gameName"></param>
        /// <param name="gameBranch"></param>
        internal void TransformScript(string gameName, string gameBranch)
        {
            List<string> appendAtStart = new List<string> { "#pragma warning disable 169, 649, 618, 436, 414" };

            // Disables "variable is never used" warning
            if (!string.IsNullOrEmpty(gameName))
            {
                if (!string.IsNullOrEmpty(gameBranch) && gameBranch != "public")
                {
                    appendAtStart.Add($"#define {gameName}{gameBranch}");
                }
                appendAtStart.Add($"#define {gameName}");
                appendAtStart.Add($"#define {Interface.uMod.GameEngine.ToString().ToUpper()}");
            }

            ScriptLines.InsertRange(0, appendAtStart);

            LinesAddedToScript = appendAtStart.Count;
            //Defines are counted later so we don't need to manually count define lines added here
        }

        #region Parsing Script

        /// <summary>
        /// Parses script
        /// </summary>
        internal void ParseScript()
        {
            // Try to provide at least some deprecation for the rename
            if (ScriptLines.Any(line => line.Contains("Oxide") || line.Contains("Covalence")))
            {
                OxideDeprecation.Deprecate(ScriptLines);
                Interface.uMod.LogWarning(Interface.uMod.Strings.Compiler.OxideNameWarning.Interpolate("plugin", FileName));
            }

            bool foundPluginClassName = false;
            bool haveReachedNamespace = false;
            for (int i = 0; i < ScriptLines.Count; i++)
            {
                // Skip empty files, they are not plugins
                string line = ScriptLines[i].Trim();
                if (line.Length < 1)
                {
                    continue;
                }

                //Check that class name matches .cs file name
                //Don't look for class name until namespace is encountered
                if (haveReachedNamespace)
                {
                    if (foundPluginClassName == false)
                    {
                        foundPluginClassName = LookForPluginClass(line);
                    }
                }

                if (haveReachedNamespace == false)
                {
                    //Magic references and using statements have to be used before the namespace

                    //Look for 'using uMod.Plugins.PluginNameHere'
                    LookForUsingPluginStatements(line);

                    // Looking for '//Requires:'
                    LookForMagicRequires(line);

                    // Looking for '//Reference:'
                    LookForMagicReference(line);

                    LookPrecompilerDirectives(line);

                    //Start parsing the uMod.Plugins namespace contents
                    haveReachedNamespace = LookForPluginNamespace(line);
                }
            }

            if (foundPluginClassName == false)
            {
                EncounteredError("Failed to find plugin class"); // TODO: Localization
            }
        }

        /// <summary>
        /// Searches for plugin namespaces
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private bool LookForPluginNamespace(string line)
        {
            Match match = Regex.Match(line, @"^\s*namespace (Oxide|uMod)\.Plugins\s*(\{\s*)?$", RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Searches for the plugin class
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private bool LookForPluginClass(string line)
        {
            // Skip blank lines and opening brace at the top of the namespace block
            Match match = Regex.Match(line, @"^\s*\{?\s*$", RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return false;
            }

            // Skip class custom attributes
            match = Regex.Match(line, @"^\s*\[", RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return false;
            }

            // Detect main plugin class name
            match = Regex.Match(line, @"^\s*(?:public|private|protected|internal)?\s*class\s+(\S+)\s+\:\s+\S*Plugin\s*", RegexOptions.IgnoreCase);
            if (!match.Success)
            {
                return false;
            }

            string className = match.Groups[1].Value;
            if (className != Name)
            {
                EncounteredError(Interface.uMod.Strings.Compiler.InvalidFileName.Interpolate(("script", FileName), ("class", className)));

                return false;
            }

            return true;
        }

        /// <summary>
        /// Searches for using statements that reference another plugin so we can add it as a dependancy
        /// </summary>
        /// <param name="line"></param>
        private void LookForUsingPluginStatements(string line)
        {
            Match match = Regex.Match(line, @"^\s*using (Oxide|uMod)\.Plugins\.(.+).", RegexOptions.IgnoreCase);

            if (match.Success)
            {
                //We have found a namespace that references another plugin

                //1st group is uMod/Oxide, 2nd group is plugin (only one capture)
                if (match.Groups.Count < 2)
                {
                    return;
                }

                Group group = match.Groups[1];

                string pluginName = group.Value;

                TryAddPluginReference(pluginName, PluginReferenceType.ExplicitRequires);
            }
        }

        /// <summary>
        /// Searches for hard requires comment annotations
        /// </summary>
        /// <param name="line"></param>
        private void LookForMagicRequires(string line)
        {
            Match match = Regex.Match(line, @"^//\s*Requires:\s*(\S+?)(\.cs)?\s*$", RegexOptions.IgnoreCase);
            if (match.Success)
            {
                string pluginName = match.Groups[1].Value;

                TryAddPluginReference(pluginName, PluginReferenceType.ExplicitRequires);
            }
        }

        /// <summary>
        /// Searches for hard reference comment annotations
        /// </summary>
        /// <param name="line"></param>
        private void LookForMagicReference(string line)
        {
            // Include explicit references defined by magic comments in script
            Match match = Regex.Match(line, @"^//\s*Reference:\s*(\S+)\s*$", RegexOptions.IgnoreCase); // TODO: Utilize option for plugin sandbox
            if (match.Success)
            {
                string result = match.Groups[1].Value;

                TryAddAssemblyReference(result);
            }
        }

        /// <summary>
        /// Searches for precompiler directives
        /// </summary>
        /// <param name="line"></param>
        private void LookPrecompilerDirectives(string line)
        {
            if (line.StartsWith("#"))
            {
                LinesAddedToScript++;
            }
        }

        /// <summary>
        /// Tries to add a plugin reference or reason to the compilation request
        /// </summary>
        /// <param name="referencedPluginName"></param>
        /// <param name="type"></param>
        internal bool TryAddPluginReference(string referencedPluginName, PluginReferenceType type)
        {
            Interface.uMod.LogDebug($"Adding plugin reference {referencedPluginName} on {PluginName}");
            //We validate the plugin references later
            PluginReference reference = ReferencedPlugins.FirstOrDefault(x => x.PluginName == referencedPluginName);

            bool found = true;
            if (reference == null)
            {
                found = false;
                reference = new PluginReference(referencedPluginName);
                ReferencedPlugins.Add(reference);
                
            }

            reference.AddReason(type);
            return found;
        }

        /// <summary>
        /// Tries to add assembly reference to the compilation request
        /// </summary>
        /// <param name="referencedDll"></param>
        private void TryAddAssemblyReference(string referencedDll)
        {
            AddReferenceByName(referencedDll.Replace(".dll", ""));
        }

        /// <summary>
        /// Tries to add reference to the compilation request
        /// </summary>
        /// <param name="assemblyName"></param>
        private void AddReferenceByName(string assemblyName)
        {
            //Hashset won't add if value already exists
            ReferencedAssemblyNames.Add(assemblyName);
        }

        #endregion Parsing Script
    }
}
