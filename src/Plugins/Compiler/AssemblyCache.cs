﻿using System.Collections.Generic;

namespace uMod.Plugins.Compiler
{
    internal class AssemblyCache
    {
        /// <summary>
        /// Cached assembly references
        /// </summary>
        private readonly Dictionary<string, AssemblyReference> _cachedDlls = new Dictionary<string, AssemblyReference>();

        /// <summary>
        /// Gets assembly reference by path
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        internal AssemblyReference Find(string fullPath)
        {
            _cachedDlls.TryGetValue(fullPath, out AssemblyReference dll);
            return dll;
        }

        /// <summary>
        /// Adds assembly reference
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        internal bool Add(AssemblyReference reference)
        {
            if (_cachedDlls.ContainsKey(reference.FullPath))
            {
                return false;
            }

            reference.IsCached = true;

            _cachedDlls[reference.FullPath] = reference;

            return true;
        }
    }
}
