﻿using System.Collections;
using System.Collections.Generic;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents a stack of plugins being processed by the compiler client
    /// </summary>
    internal class CompilationStack : IEnumerable<CompilePluginRequest>
    {
        /// <summary>
        /// Gets the number of requests currently being processed
        /// </summary>
        internal int Count { get { return Requests.Count; } }

        /// <summary>
        /// Gets a list of all requests
        /// </summary>
        internal readonly List<CompilePluginRequest> All = new List<CompilePluginRequest>();

        /// <summary>
        /// Gets a dictionary of requests keyed by plugin name
        /// </summary>
        internal readonly Dictionary<string, CompilePluginRequest> Requests = new Dictionary<string, CompilePluginRequest>();

        /// <summary>
        /// Determines if plugin request exists
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal bool Contains(string pluginName)
        {
            return Requests.ContainsKey(pluginName);
        }

        /// <summary>
        /// Gets a plugin request with the specified plugin name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal CompilePluginRequest Find(string pluginName)
        {
            Requests.TryGetValue(pluginName, out CompilePluginRequest plugin);

            return plugin;
        }

        /// <summary>
        /// Adds a plugin request
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        internal bool Add(CompilePluginRequest plugin)
        {
            if (Requests.ContainsKey(plugin.PluginName))
            {
                Requests.Remove(plugin.PluginName);
            }

            if (!All.Contains(plugin))
            {
                All.Add(plugin);
            }

            Requests.Add(plugin.PluginName, plugin);
            return true;
        }

        /// <summary>
        /// Adds a range of plugins
        /// </summary>
        /// <param name="plugins"></param>
        internal void AddRange(IEnumerable<CompilePluginRequest> plugins)
        {
            foreach (CompilePluginRequest plugin in plugins)
            {
                if (!Requests.ContainsKey(plugin.PluginName))
                {
                    Requests.Add(plugin.PluginName, plugin);
                }
                else
                {
                    Requests[plugin.PluginName].CurrentStep = PluginCompileStage.PreParseScript;
                }
            }

            foreach (CompilePluginRequest plugin in plugins)
            {
                if (!All.Contains(plugin))
                {
                    All.Add(plugin);
                }
            }
        }

        /// <summary>
        /// Remove a plugin with the specified plugin name
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal bool Remove(string pluginName)
        {
            if (Requests.TryGetValue(pluginName, out CompilePluginRequest request))
            {
                All.Remove(request);
            }
            return Requests.Remove(pluginName);
        }

        /// <summary>
        /// Clear stack
        /// </summary>
        internal void Clear()
        {
            All.Clear();
            Requests.Clear();
        }

        /// <summary>
        /// Gets an enumerator for the CompilationStack
        /// </summary>
        /// <returns></returns>
        public IEnumerator<CompilePluginRequest> GetEnumerator()
        {
            return All.GetEnumerator();
        }

        /// <summary>
        /// Gets an enumerator for the CompilationStack
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
