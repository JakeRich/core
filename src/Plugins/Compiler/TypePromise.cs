﻿using System.Collections.Generic;
using uMod.Common;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Manages type promises
    /// </summary>
    internal static class TypePromiseManager
    {
        public static Dictionary<string, ITypePromise> TypePromises = new Dictionary<string, ITypePromise>();

        /// <summary>
        /// Add a type promise
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="sourcePlugin"></param>
        public static void AddPromise(string typeName, string sourcePlugin)
        {
#if DEBUG
            Interface.uMod.LogDebug($"Adding promise {typeName} for {sourcePlugin}");
#endif
            if (!TypePromises.ContainsKey(sourcePlugin))
            {
                TypePromises.Add(sourcePlugin, new TypePromise()
                {
                    TypeName = typeName,
                    SourcePlugin = sourcePlugin
                });
            }
        }

        /// <summary>
        /// Remove a type promise
        /// </summary>
        /// <param name="sourcePlugin"></param>
        public static void RemovePromise(string sourcePlugin)
        {
#if DEBUG
            Interface.uMod.LogDebug($"Remove promises for {sourcePlugin}");
#endif
            TypePromises.Remove(sourcePlugin);
        }
    }

    /// <summary>
    /// Represents a type promise
    /// </summary>
    internal interface ITypePromise
    {
        string TypeName { get; }
        string SourcePlugin { get; }

        bool IsResolved(IPlugin plugin);
    }

    /// <summary>
    /// Implements a basic type promise
    /// </summary>
    internal class TypePromise : ITypePromise
    {
        public string TypeName { get; set; }
        public string SourcePlugin { get; set; }

        public bool IsResolved(IPlugin plugin)
        {
            if (Interface.uMod.Application.Resolved(TypeName))
            {
                return true;
            }

            return false;
        }
    }
}
