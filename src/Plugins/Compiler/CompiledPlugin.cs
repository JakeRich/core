﻿using System.Collections.Generic;
using System.Reflection;
using uMod.Common;

namespace uMod.Plugins.Compiler
{
    /// <summary>
    /// Represents a successfully compiled and loaded plugin
    /// </summary>
    internal class CompiledPlugin
    {
        /// <summary>
        /// The last successfully compiled assembly
        /// </summary>
        internal Assembly LastGoodAssembly;

        /// <summary>
        /// The last successfuly loaded plugin instance
        /// </summary>
        internal IPlugin LastGoodPlugin;

        /// <summary>
        /// The name of the plugin
        /// </summary>
        internal string PluginName;

        /// <summary>
        /// References associated with the plugin
        /// </summary>
        internal List<PluginReference> PluginReferences;

        /// <summary>
        /// Determines if plugin is successfully loaded into the server
        /// </summary>
        internal bool IsLoaded { get { return LastGoodPlugin != null && LastGoodPlugin.IsLoaded; } }

        /// <summary>
        /// Create a new instance of the CompiledPlugin class
        /// </summary>
        /// <param name="plugin"></param>
        internal CompiledPlugin(CompilePluginRequest plugin)
        {
            LastGoodPlugin = plugin.Plugin;
            LastGoodAssembly = plugin.CompiledAssembly;
            PluginName = plugin.PluginName;
            PluginReferences = plugin.ReferencedPlugins;
        }
    }
}
