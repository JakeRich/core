﻿using uMod.Libraries;
using uMod.Logging;

namespace uMod.Plugins.Decorators
{
    public class ServerDecorator : HookDecorator
    {
        private readonly Universal _universal;
        private bool _serverInitialized;

        public ServerDecorator(Plugin plugin, Universal universal) : base(plugin)
        {
            _universal = universal;
        }

        /// <summary>
        /// Called when the plugin is initializing
        /// </summary>
        [Hook("Init")]
        private void Init()
        {
            // Configure remote error logging
            SentryLogger.SetTag("game", _universal.Game.ToLowerInvariant());
            SentryLogger.SetTag("game version", _universal.Server.Version);
        }

        [Hook("InitLogging")]
        private void InitLogging()
        {
            Interface.uMod.OnLoggingInitialized.Invoke();
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("IOnServerInitialized")]
        private void IOnServerInitialized()
        {
            if (!_serverInitialized)
            {
                // Let plugins know server startup is complete
                Interface.CallHook("OnServerInitialized", _serverInitialized = true);

                Plugin.logger.Info(Interface.uMod.Strings.Server.VersionLog.Interpolate(("version", Module.Version), ("game", _universal.Game), ("serverVersion", _universal.Server.Version)));
                if (!Interface.uMod.IsTesting)
                {
                    Analytics.Collect();
                }
            }
        }

        [After("OnPluginLoaded")]
        private void OnPluginLoadedAfter(Plugin plugin)
        {
            if (_serverInitialized)
            {
                // Call OnServerInitialized for hotloaded plugins
                plugin.CallHook("OnServerInitialized", false);
            }
        }

        /// <summary>
        /// Called when the server is saved
        /// </summary>
        [Hook("OnServerSave")]
        private void OnServerSave()
        {
            // Trigger save process
            Interface.uMod.OnSave();
        }

        /// <summary>
        /// Called when the server is shutting down
        /// </summary>
        [Hook("OnServerShutdown")]
        private void OnServerShutdown()
        {
            // Trigger shutdown process
            Interface.uMod.OnShutdown();
        }
    }
}
