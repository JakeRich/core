﻿using System;
using uMod.Common;

namespace uMod.Plugins.Decorators
{
    internal class OnlinePlayersDecorator : HookDecorator
    {
        public OnlinePlayersDecorator(Plugin plugin) : base(plugin)
        {
        }

        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(IPlayer player) => AddOnlinePlayer(player);

        [Hook("OnPlayerDisconnected")]
        private void OnPlayerDisconnected(IPlayer player)
        {
            // Delay removing player until OnPlayerDisconnected has fired in plugin
            Interface.uMod.NextTick(() =>
            {
                foreach (PluginFieldInfo pluginField in Plugin.Meta.OnlinePlayerFields)
                {
                    if (pluginField.PlayerType == typeof(IPlayer))
                    {
                        pluginField.Call("Remove", player);
                    }
                    else if (player.Object != null)
                    {
                        pluginField.Call("Remove", player.Object);
                    }
                }
            });
        }

        private void AddOnlinePlayer(IPlayer player)
        {
            foreach (PluginFieldInfo pluginField in Plugin.Meta.OnlinePlayerFields)
            {
                Type type = pluginField.GenericArguments[1];
                object onlinePlayer = type.GetConstructor(new[] { pluginField.PlayerType }) == null ? Activator.CreateInstance(type) : Activator.CreateInstance(type, (object)player);
                type.GetField("Player").SetValue(onlinePlayer, player);
                pluginField.Call("Add", player, onlinePlayer);
            }
        }
    }
}
