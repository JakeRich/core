﻿using System;
using System.Collections.Generic;
using uMod.Common;

namespace uMod.Plugins
{
    /// <summary>
    /// Caches parsed hook names in memory
    /// </summary>
    internal class HookNameCache
    {
        internal object HookNameLock = new object();
        private readonly IDictionary<string, HookName> _dictionary = new Dictionary<string, HookName>();

        public HookName Get(string key)
        {
            if (!_dictionary.TryGetValue(key, out HookName result))
            {
                _dictionary.Add(key, result = new HookName(key));
            }

            return result;
        }
    }

    /// <summary>
    /// Represents a hook name
    /// </summary>
    public class HookName : IHookName, IEquatable<HookName>
    {
        internal static readonly HookNameCache Cache = new HookNameCache();
        private const char NamespaceSeparator = '.';
        private static readonly char[] NamespaceSeparatorArray = new[] { NamespaceSeparator };

        /// <summary>
        /// Fully qualified hook name
        /// </summary>
        internal readonly string fullyQualifiedHookName;

        /// <summary>
        /// Hook name that is invoked
        /// </summary>
        internal readonly string name;

        /// <summary>
        /// Base hook name
        /// </summary>
        internal readonly string baseName;

        /// <summary>
        /// Base hook name
        /// </summary>
        internal readonly string cachedName;

        /// <summary>
        /// Hook event name
        /// </summary>
        internal readonly string eventName;

        /// <summary>
        /// Hook namespace
        /// </summary>
        internal readonly string @namespace;

        /// <summary>
        /// Hook EventState
        /// </summary>
        internal readonly EventState eventState;

        internal readonly HookName StartedHookName;
        internal readonly HookName CanceledHookName;
        internal readonly HookName FailedHookName;
        internal readonly HookName CompletedHookName;
        internal readonly HookName AfterHookName;

        /// <summary>
        /// Determine if HookName is an event
        /// </summary>
        internal readonly bool isEvent;

        /// <summary>
        /// Determine if HookName is namespaced
        /// </summary>
        internal readonly bool isNamespaced;

        /// <summary>
        /// Determine if HookName is a base hook
        /// </summary>
        internal readonly bool isBaseHook;

        /// <summary>
        /// Determine if HookName is an internal hook
        /// </summary>
        internal readonly bool isInternal;

        private readonly int _hashCode;

        public string FullyQualifiedHookName => fullyQualifiedHookName;

        public string Name => name;

        public string BaseName => baseName;

        public string CachedName => cachedName;

        public string EventName => eventName;

        public string Namespace => @namespace;

        public EventState EventState => eventState;

        public bool IsEvent => isEvent;

        public bool IsNamespaced => isNamespaced;

        public bool IsBaseHook => isBaseHook;

        public bool IsInternal => isInternal;

        /// <summary>
        /// Create new HookName
        /// </summary>
        /// <param name="fullyQualifiedHookName"></param>
        public HookName(string fullyQualifiedHookName)
        {
            eventName = string.Empty;
            @namespace = string.Empty;

            this.fullyQualifiedHookName = fullyQualifiedHookName;
            if (this.fullyQualifiedHookName.IndexOf(NamespaceSeparator) > -1 && this.fullyQualifiedHookName.Split(NamespaceSeparatorArray) is string[] parts)
            {
                baseName = parts[0];
                for (int i = 1; i < parts.Length; i++)
                {
                    if (EventArgs.EventNames.Contains(parts[i]) || parts[i] == "After")
                    {
                        eventName = parts[i];
                        isEvent = true;
                    }
                    else
                    {
                        @namespace = parts[i];
                        isNamespaced = true;
                    }
                }

                if (isEvent)
                {
                    name = $"{baseName}.{eventName}";
                }
                else
                {
                    name = baseName;
                }

                if (name.StartsWith("I") && char.IsUpper(name[1]))
                {
                    isInternal = true;
                }

                if (name.StartsWith("base_"))
                {
                    isBaseHook = true;
                }
            }
            else
            {
                baseName = fullyQualifiedHookName;
                name = fullyQualifiedHookName;
                cachedName = fullyQualifiedHookName;
                if (name.StartsWith("I") && char.IsUpper(name[1]))
                {
                    isInternal = true;
                }

                if (name.StartsWith("base_"))
                {
                    isBaseHook = true;
                }
            }

            if (!isEvent)
            {
                StartedHookName = Cache.Get($"{fullyQualifiedHookName}.Started");
                CanceledHookName = Cache.Get($"{fullyQualifiedHookName}.Canceled");
                FailedHookName = Cache.Get($"{fullyQualifiedHookName}.Failed");
                CompletedHookName = Cache.Get($"{fullyQualifiedHookName}.Completed");
                AfterHookName = Cache.Get($"{fullyQualifiedHookName}.After");
                eventState = EventState.None;
            }
            else
            {
                StartedHookName = null;
                CanceledHookName = null;
                FailedHookName = null;
                CompletedHookName = null;
                AfterHookName = null;
                if (!eventName.Equals("After"))
                {
                    eventState = (EventState)Enum.Parse(typeof(EventState), eventName);
                }
                else
                {
                    eventState = EventState.None;
                }
            }

            _hashCode = 1680764015 + EqualityComparer<string>.Default.GetHashCode(this.fullyQualifiedHookName);
        }

        /// <summary>
        /// Convert HookName to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return fullyQualifiedHookName;
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return fullyQualifiedHookName.ToString(formatProvider);
        }

        public override bool Equals(object obj)
        {
            return obj is HookName name && fullyQualifiedHookName == name.fullyQualifiedHookName;
        }

        public bool Equals(HookName other)
        {
            return fullyQualifiedHookName == other?.fullyQualifiedHookName;
        }

        public bool Equals(IHookName other)
        {
            return fullyQualifiedHookName == other.FullyQualifiedHookName;
        }

        public override int GetHashCode()
        {
            return _hashCode;
        }

        public static bool operator ==(HookName left, HookName right)
        {
            return left != null && left.Equals(right);
        }

        public static bool operator !=(HookName left, HookName right)
        {
            return !(left == right);
        }
    }
}
