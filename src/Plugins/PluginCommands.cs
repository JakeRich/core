﻿using System;
using System.Collections.Generic;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using uMod.Common.Plugins;
using uMod.Libraries;
using uMod.Pooling;

namespace uMod.Plugins
{
    public struct CommandInfo : ICommandInfo
    {
        public ICommandDefinition Definition { get; }
        public string[] Permissions { get; }
        public CommandCallback Callback { get; }
        public IDictionary<string, string> Help { get; }
        public IDictionary<string, string> Descriptions { get; }

        public string GetHelp(string lang = null)
        {
            if (string.IsNullOrEmpty(lang))
            {
                lang = Lang.DefaultLang;
            }

            if (Help.TryGetValue(lang, out string result))
            {
                return result;
            }

            return Interface.uMod.Strings.Command.CommandHelpDefault;
        }

        public string GetDescription(string lang = null)
        {
            if (string.IsNullOrEmpty(lang))
            {
                lang = Lang.DefaultLang;
            }

            if (Descriptions.TryGetValue(lang, out string result))
            {
                return result;
            }

            return Interface.uMod.Strings.Command.CommandDescriptionDefault;
        }

        public CommandInfo(ICommandDefinition commandDefinition, CommandCallback callback, string[] permissions = null, IDictionary<string, string> descriptions = null, IDictionary<string, string> help = null)
        {
            Definition = commandDefinition;
            Callback = callback;
            Permissions = permissions;
            Descriptions = descriptions;
            Help = help;
        }
    }

    /// <summary>
    /// Plugin command handler
    /// </summary>
    public class PluginCommands : IPluginCommandHandler
    {
        /// <summary>
        /// Determine if command handler is already registered
        /// </summary>
        private bool _registered;

        /// <summary>
        /// Command plugin
        /// </summary>
        private readonly Plugin _plugin;

        /// <summary>
        /// Permission library
        /// </summary>
        protected Permission Permission = Interface.uMod.Libraries.Get<Permission>();

        /// <summary>
        /// Universal library
        /// </summary>
        protected Universal Universal = Interface.uMod.Libraries.Get<Universal>();

        /// <summary>
        /// Dictionary of registered commands
        /// </summary>
        private readonly IDictionary<string, ICommandInfo> _commandInfos;

        /// <summary>
        /// Gets command logger
        /// </summary>
        private readonly ILogger _logger;

        private readonly Module _module;

        /// <summary>
        /// Create a new plugin command handler
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="logger"></param>
        internal PluginCommands(Plugin plugin, ILogger logger, Module module)
        {
            _logger = logger;
            _module = module;
            _plugin = plugin;
            _commandInfos = new Dictionary<string, ICommandInfo>();
        }

        public CommandCallback CreateCallback(ICommandDefinition definition,
            Func<IPlayer, IArgs, CommandState> callback)
        {
            return delegate (IPlayer caller, string command, string fullCommand, object[] context, ICommandInfo cmdInfo)
            {
                Args args = Pools.CommandArgs.Get<Args>().ResetArgs(definition, caller, fullCommand);

                try
                {
                    return callback(caller, args);
                }
                finally
                {
                    Pools.CommandArgs.Free(args);
                }
            };
        }

        public CommandCallback CreateCallback(ICommandDefinition definition, string callback)
        {
            HookName hookName = HookName.Cache.Get(callback);

            return delegate (IPlayer caller, string command, string fullCommand, object[] context, ICommandInfo cmdInfo)
            {
                HookEvent hookEvent = Pools.HookEvents.Get();
                Args args = Pools.CommandArgs.Get<Args>().ResetArgs(definition, caller, fullCommand, context);

                try
                {
                    hookEvent.context.
                        AddContext(caller).
                        AddContext(typeof(IPlayer), caller).
                        AddContext(command).
                        AddContext(args).
                        AddContext(typeof(IArgs), args).
                        AddContext(args.GetStringValues()).
                        AddContext(cmdInfo);

                    if (context != null)
                    {
                        for (int i = 0; i < context.Length; i++)
                        {
                            hookEvent.context.AddContext(context[i]);
                        }
                    }

                    _plugin.Dispatcher.Dispatch(hookName, null, hookEvent);
                }
                finally
                {
                    Pools.CommandArgs.Free(args);
                    Pools.HookEvents.Free(hookEvent);
                }

                if (hookEvent.Canceled)
                {
                    return CommandState.Canceled;
                }

                return CommandState.Completed;
            };
        }

        public void Add(ICommandInfo command)
        {
            foreach (string cmdName in command.Definition.Names)
            {
                if (_commandInfos.ContainsKey(cmdName.ToLowerInvariant()))
                {
                    _logger.Warning(_module.Strings.Command.AliasAlreadyExists.Interpolate("command", cmdName));
                    continue;
                }

                _commandInfos.Add(cmdName, command);
            }

            if (command.Permissions != null)
            {
                foreach (string perm in command.Permissions)
                {
                    if (!Permission.PermissionExists(perm))
                    {
                        Permission.RegisterPermission(perm, _plugin);
                    }
                }
            }

            if (_registered)
            {
                foreach (string cmdName in command.Definition.Names)
                {
                    Universal.RegisterCommand(cmdName, _plugin, UniversalCommandCallback);
                }
            }
        }

        public void Remove(ICommandInfo command)
        {
            foreach (string cmdName in command.Definition.Names)
            {
                if (_commandInfos.ContainsKey(cmdName))
                {
                    _commandInfos.Remove(cmdName);
                }
            }
        }

        public void Remove(string command)
        {
            if (_commandInfos.TryGetValue(command, out ICommandInfo commandInfo))
            {
                Remove(commandInfo);
            }
        }

        /// <summary>
        /// Gets command syntax help
        /// </summary>
        /// <param name="command"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetHelp(string command, string lang = null)
        {
            return _commandInfos.TryGetValue(command, out ICommandInfo pluginCommand) ? pluginCommand.GetHelp(lang) : null;
        }

        /// <summary>
        /// Gets command description
        /// </summary>
        /// <param name="command"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetDescription(string command, string lang = null)
        {
            return _commandInfos.TryGetValue(command, out ICommandInfo pluginCommand) ? pluginCommand.GetDescription(lang) : null;
        }

        /// <summary>
        /// Register commands with universal provider
        /// </summary>
        internal void Register()
        {
            foreach (KeyValuePair<string, ICommandInfo> pair in _commandInfos)
            {
                Universal.RegisterCommand(pair.Key, _plugin, UniversalCommandCallback);
            }

            _registered = true;
        }

        /// <summary>
        /// Unregister commands from universal provider
        /// </summary>
        internal void Unregister()
        {
            foreach (KeyValuePair<string, ICommandInfo> pair in _commandInfos)
            {
                Universal.UnregisterCommand(pair.Key, _plugin);
            }

            _registered = false;
        }

        /// <summary>
        /// Command callback provider
        /// </summary>
        /// <param name="caller"></param>
        /// <param name="cmd"></param>
        /// <param name="fullCommand"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private CommandState UniversalCommandCallback(IPlayer caller, string cmd, string fullCommand, object[] context = null, ICommandInfo cmdInfo = null)
        {
            if (cmdInfo == null && !_commandInfos.TryGetValue(cmd, out cmdInfo))
            {
                return CommandState.Unrecognized;
            }

            if (caller == null)
            {
                _logger.Warning(_module.Strings.Command.NullCaller);
                return CommandState.Unrecognized;
            }

            if (cmdInfo.Permissions != null)
            {
                foreach (string perm in cmdInfo.Permissions)
                {
                    if (!caller.HasPermission(perm) && !caller.IsServer && (!caller.IsAdmin || !_plugin.IsCorePlugin))
                    {
                        if (_module.Plugins.Configuration.Commands.DeniedCommands)
                        {
                            caller.Message(_module.GetStrings(caller).Command.PermissionDenied
                                .Interpolate("command", cmd));
                        }

                        return CommandState.Completed;
                    }
                }
            }

            cmdInfo.Callback(caller, cmd, fullCommand, context, cmdInfo);

            return CommandState.Completed;
        }
    }
}
